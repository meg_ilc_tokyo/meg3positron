This is a repository for a new positron spectrometer for a new muegamma experiment

# Contact and contribute
Contact person: Yusuke Uchiyama (uchiyama [at] icepp.s.u-tokyo.ac.jp)  

If you want to push your development to this repository, send pull requests or send an e-mail to Yusuke for a request of write permission (tell your e-mail address registered for your BitBucket account).
(Users of UTokyo group don't need this; they are already in the 'developer' group.)



# Environmental variables
Set `$MEG3SYS`. You can use setenv.sh; source it in this directory:
```
source setenv.sh
```

# How to build
```
cd $MEG3SYS
make
```
this builds gem4 and analyzer.
```
cd $MEG3SYS
make gem4
```
buils gem4 only.
```
cd $MEG3SYS
make analyzer
```
or equivalently
```
cd $MEG3SYS/analyzer
./unix/install.sh
```
builds analyzer only.



# Test
```
./bin/$G4SYSTEM/gem4 -i macros/example.mac -n 10 --cout=STDOUT --cerr=STDERR -I
```

# Make geometry file
Geometry information is necessary in tracking with GenFit.

gem4 -> .gdml -> .root -> TGeoManager

To use GDML file, Geat4 and ROOT must be compiled with GDML.

## How to make a gdml file from gem4 geometry
Put the following line in a macro. (In example.mac, this line is commented out.)
```
# Write geometry to a GDML file
   /gem/geom/writeGDML gem4.gdml
```
Then, you will find `gem4.gdml` in `$MEG3SYS/gem4/`. If it's ok, move it to `$MEG3SYS/gem4/gdml/`
with a proper file name.

## How to convert to .root or .C file
```
$ cd $MEG3SYS/gem4
$ root
[] .L gdml/drawGDML.C
[] auto world = drawGDML()
[] world->Export("gem4.root", "world")
```
Then, you will find `gem4.root` in `$MEG3SYS/gem4/`. If it's ok, move it to `$MEG3SYS/gem4/gdml/`
with a proper file name.

If the geometry is useful and committed to the repository, write down the information in `gdml/README.md`.
