# $Id$
#
# User editable Makefile to compile MEG software
#
# usages:
#
# make              : same as offline
# make offlilne     : compile entire executables for offline
#
# make analyzer     : compile analyzer
# make bartender    : compile bartender
# make gem4         : compile gem4
# make getmeg2lib   : copy meg2lib from the offline cluster
# make getmeg2lib-light : copy meg2lib excluding heavy files for waveform processing
#
# make -k clean     : remove intermediate files
# make -k distclean : remove intermediate files and following directories and
#                     files in analyzer and bartender :
#                     src/generated include/generated obj dict Makefile
#
# make -k depclean  : remove dependency files (*.d) for analyzer and
#                     bartender
# make -k megclean  : remove MEG specific intermediate files for analyzer
#                     and bartender
#

########################### CONFIGURATION OPTIONS ############################
 
#
# Compiler options
#

export CFLAGS   := -pipe -Wall -W -Wstrict-aliasing=2 -Wcast-align \
                   -Wswitch-enum -D_FORTIFY_SOURCE=2 -Wpointer-arith \
                   -Winit-self -Wundef -Wbad-function-cast -Wwrite-strings \
                   -Wstrict-prototypes -Wmissing-prototypes -Wold-style-definition \
                   -Wmissing-format-attribute
export CXXFLAGS := -pipe -Wall -W -Wstrict-aliasing=2 -Wcast-align \
                   -Wswitch-enum -D_FORTIFY_SOURCE=2 -Wpointer-arith \
                   -Winit-self -Wundef -Woverloaded-virtual -Wwrite-strings \
                   -Wmissing-format-attribute -D_LIBCPP_UNROLL_LOOPS=0
export FFLAGS   := -pipe -Wall -W -fbounds-check
# export LDFLAGS  :=

#
# OPT : Additional flag(s) for compiler to build analyzer/bartender
#
export DEBUG ?= yes
export OPTIMIZE ?= yes
OPT = -g -O

#
# LIBROME                 : make rome class library.
# ROMEDEBUG, ROMEOPTIMIZE : turn on/off debugging and optimization flag for ROME
#
export LIBROME ?= yes
export ROMEDEBUG ?= yes
export ROMEOPTIMIZE ?= yes

#
# Flag if compiling several sources in parallel, default is yes.
#
# export PARALLEL_COMPILE = no

#
# Clobber MEG3SYS with current directory for convenience.
# Having MEG3SYS different from the current directory will be warned in checkenv-meg.
#
export CWD := $(shell pwd)
export MEG3SYS_COPY := `cd $(MEG3SYS); pwd -P`
ifneq ($(CWD),$(MEG3SYS_COPY))
	export MEG3SYS = $(CWD)
endif

#
# Set dummy MIDASSYS when not specified.
#
export MIDASSYS ?= /usr

####################### END OF CONFIGURATION OPTIONS #########################
#
# You should not change lines below if you do not know what you are doing
#
##############################################################################
# Architecture
PLATFORM = $(shell $(ROOTSYS)/bin/root-config --platform)
OSTYPE = $(shell uname |  tr '[A-Z]' '[a-z]')

export MEGOPT      ?= $(OPT)
export MEGFFLAGS   ?= $(FFLAGS)
export MEGCFLAGS   ?= $(CFLAGS)
export MEGCXXFLAGS ?= $(CXXFLAGS)

#
# check if $(CC) and $(FC) are GNU Compiler for parallel compilation
#
export GNU_FC  ?= $(strip $(shell LANG=C $(FC) -v 2>&1 | grep version | grep gcc))
export GNU_CC  ?= $(strip $(shell LANG=C $(CC) -v 2>&1 | grep version | grep gcc))
export GNU_CXX ?= $(strip $(shell LANG=C $(CXX) -v 2>&1 | grep version | grep gcc))
ifdef GNU_CC
#  ifdef GNU_CXX
    ifdef GNU_FC
      export GNU_COMPILER ?= yes
    endif
#  endif
endif

#
# Number of processors
#
ifeq ($(PARALLEL_COMPILE),no)
  NCPU = 1
  else
  ifeq ($(OSTYPE),darwin)
    ifeq ($(MACHTYPE),powerpc)
	NCPU := $(strip $(shell system_profiler SPHardwareDataType | grep "Number Of CPUs" | cut -d : -f 2))
    else
	NCPU := $(strip $(shell system_profiler SPHardwareDataType | grep "Number Of Cores" | cut -d : -f 2))
    endif
  else
    NCPU := $(shell grep 'processor[[:space:]]:[[:space:]][[:digit:]+]' /proc/cpuinfo | wc -l)
  endif
endif

###  Disabled automatic set of -j flag, because there are too large files
### in analyzer to compile in parallel. When the problem is solved,
### we can turn on following lines.
#ifeq ($(NCPU),1)
#  JOBSFLAG ?=
#else
#  JOBSFLAG ?= -j$(shell expr ${NCPU} + 1)
#endif


#
# Verbose or quiet 'make'
#
ifeq ($(MEGVERBOSEMAKE), 1)
  export ROMEVERBOSEMAKE ?= 1
else
  export ROMEVERBOSEMAKE ?= 0
endif

#
# Variables to be used for checking compatibility of ROOT
#

# Required ROOT version
ROOT_MAJOR_STD = 6
ROOT_MINOR_STD = 08
ROOT_PATCH_STD = 00

# Local ROOT version
ROOT_VERSION = $(shell $(ROOTSYS)/bin/root-config --version)
ROOT_MAJOR = $(shell $(ROOTSYS)/bin/root-config --version | cut -d . -f 1)
ROOT_MINOR = $(shell $(ROOTSYS)/bin/root-config --version | cut -d . -f 2 | cut -d / -f 1)
ROOT_PATCH = $(shell $(ROOTSYS)/bin/root-config --version | cut -d / -f 2 | cut -b 1,2)

ROOT_VERSION_ERROR := no
ifeq ($(shell expr $(ROOT_MAJOR) \< $(ROOT_MAJOR_STD)), 1)
  ROOT_VERSION_ERROR := yes
else
  ifeq ($(ROOT_MAJOR), $(ROOT_MAJOR_STD))
    ifeq ($(shell expr $(ROOT_MINOR) \< $(ROOT_MINOR_STD)), 1)
      ROOT_VERSION_ERROR := yes
    else
      ifeq ($(ROOT_MINOR), $(ROOT_MINOR_STD))
        ifeq ($(shell expr $(ROOT_PATCH) \< $(ROOT_PATCH_STD)), 1)
          ROOT_VERSION_ERROR := yes
        endif
      endif
    endif
  endif
endif

ROOT_VERSION_EXACTLY_SAME_WITH_STANDARD := no
ifeq ($(ROOT_MAJOR), $(ROOT_MAJOR_STD))
  ifeq ($(ROOT_MINOR), $(ROOT_MINOR_STD))
    ifeq ($(ROOT_PATCH), $(ROOT_PATCH_STD))
      ROOT_VERSION_EXACTLY_SAME_WITH_STANDARD := yes
    endif
  endif
endif

# Check if RooFit is available
MEGANAMAKEOPTIONS := -nl -pch -qm -a meg -mysql
ifeq ($(shell $(ROOTSYS)/bin/root-config --has-roofit), yes)
  MEGANAMAKEOPTIONS += -f HAVE_ROOFIT
  MEGANAMAKEOPTIONS += -a roofit
endif
ifneq ($(MEGNOBZIP2),yes)
  MEG3ANAOPTIONS += -bzip2
endif


#
# Variables to be used for check if standard
#

# Standard GCC version
GCC_MAJOR_STD3 = 6
GCC_MINOR_STD3 = 4
GCC_PATCH_STD3 = 0
GCC_MAJOR_STD4 = 8
GCC_MINOR_STD4 = 3
GCC_PATCH_STD4 = 0

# Local GCC version
GCC_VERSION := $(shell $(CC) -dumpversion)
GCC_MAJOR   := $(shell $(CC) -dumpversion | cut -d . -f 1)
GCC_MINOR   := $(shell $(CC) -dumpversion | cut -d . -f 2)
GCC_PATCH   := $(shell $(CC) -dumpversion | cut -d . -f 3)

GCC_VERSION_EXACTLY_SAME_WITH_STANDARD := no
ifeq ($(GCC_MAJOR), $(GCC_MAJOR_STD3))
  ifeq ($(GCC_MINOR), $(GCC_MINOR_STD3))
    ifeq ($(GCC_PATCH), $(GCC_PATCH_STD3))
      GCC_VERSION_EXACTLY_SAME_WITH_STANDARD := yes
    endif
  endif
endif
ifneq ($(GCC_VERSION_EXACTLY_SAME_WITH_STANDARD), yes)
  ifeq ($(GCC_MAJOR), $(GCC_MAJOR_STD4))
    ifeq ($(GCC_MINOR), $(GCC_MINOR_STD4))
      ifeq ($(GCC_PATCH), $(GCC_PATCH_STD4))
        GCC_VERSION_EXACTLY_SAME_WITH_STANDARD := yes
      endif
    endif
  endif
endif

PATH:=${PATH}:${CERN}/${CERN_LEVEL}/bin:${ROOTSYS}/bin:${ROMESYS}/bin

.PHONY: offline online getmeg2lib getmeg2lib-light meg2lib meg2db gem4 analyzer bartender rome common hooks \
        checkenv-rootversion-std checkenv-gccversion-std checkenv-platform-std \
        svn-update create-rz-for-check print-standards check-std \
        check-local-change

ifeq ($(MAKECMDGOALS), online)
  MEGONLINE = yes
endif

MEG3LIB ?= /meg/home/meg/offline/meg2lib

TARGET = hooks meg2lib

MEG3DB_EXIST    = $(shell find ../ -maxdepth 1 -type d -and -name meg2db)
COMMON_EXIST    = $(shell find  ./ -maxdepth 1 -type d -and -name common)
GEM4_EXIST      = $(shell find  ./ -maxdepth 1 -type d -and -name gem4)
BARTENDER_EXIST = $(shell find  ./ -maxdepth 1 -type d -and -name bartender)
ANALYZER_EXIST  = $(shell find  ./ -maxdepth 1 -type d -and -name analyzer)

ifeq (${MEG3DB_EXIST}, ../meg2db)
   TARGET += meg2db
endif
ifeq (${COMMON_EXIST}, ./common)
   TARGET += common
endif
ifeq (${GEM4_EXIST}, ./gem4)
   TARGET += gem4
endif
ifeq (${BARTENDER_EXIST}, ./bartender)
   TARGET += bartender
endif
ifeq (${ANALYZER_EXIST}, ./analyzer)
   TARGET += analyzer
endif

ifeq ($(MEGONLINE), yes)
  all: online
else
  all: offline
endif

offline: ${TARGET}
online: analyzer 

meg2lib:
	@if [ -d $(MEG3LIB) -a ! -e meg2lib ]; then \
	  ln -s $(MEG3LIB) .; \
	fi
	@if [ ! -e meg2lib ]; then \
	  echo You need download meg2lib.; \
	  echo Please read https://bitbucket.org/muegamma/offline/wiki/Installing%20Analysis%20and%20MC%20Software#markdown-header-meg2lib; \
          echo To just download full meg2lib from the offline cluster, \'make getmeg2lib\'; \
          echo If you do not need files for experimental waveform processing and want a light meg2lib, \'make getmeg2lib-light\'; \
	  exit 1; \
	fi

hooks:
	@if [ -d $(MEG3SYS) -a ! -e  $(MEG3SYS)/.git/hooks/pre-commit ]; then \
	  ln -s ../../common/etc/hooks/pre-commit $(MEG3SYS)/.git/hooks/pre-commit; \
	fi
	@if [ -d $(MEG3SYS) -a ! -e  $(MEG3SYS)/.git/hooks/commit-msg ]; then \
	  ln -s ../../common/etc/hooks/commit-msg $(MEG3SYS)/.git/hooks/commit-msg; \
	fi

so:
	@echo plase use "analyzer -I" or "bartender -I" instead of libmeganalyzer.so or libmegbartender.so
	@exit 1

bartender: hooks checkenv-bartender rome common
	$(ROMESYS)/bin/romebuilder.exe -nl -pch -qm -a meg -i bartender/MEGBartender.xml -o bartender -sqlite3 -mysql $(MEG3BAROPTIONS)
	$(MAKE) -C bartender pch
ifneq (,$(findstring distcc,$(CC) $(FC) $(CXX) $(CXXLD)))
	$(MAKE) $(JOBSFLAG) -C bartender
else
	@echo 0 > .build_success
	@if [ -t 1 ] && [ -t 2 ]; then \
	  ($(MAKE) $(JOBSFLAG) -C bartender \
	  || (echo $$? > .build_success; $(RM) .make_error.log)) \
	  3>&2 2>&1 1>&3 | tee .make_error.log; \
	else \
	  $(MAKE) $(JOBSFLAG) -C bartender || echo $$? > .build_success; \
	fi
	@if [ -s .make_error.log ]; then \
	  echo; \
	  echo === WARNINGS SUMMARY ===; \
	  cat .make_error.log; \
	  echo; \
	fi
	@export BUILD_SUCCESS=`cat .build_success`; \
	if [ $$BUILD_SUCCESS -ne 0 ]; then \
	  $(RM) .make_error.log .build_success; \
	  exit $$BUILD_SUCCESS; \
	fi
	@echo
	@echo "==> $@ is done."
	@echo
	@$(RM) .make_error.log .build_success
endif

analyzer: hooks checkenv-analyzer rome common
ifeq ($(MEGONLINE), yes)
	$(ROMESYS)/bin/romebuilder.exe -i analyzer/MEGAnalyzer.xml -o analyzer -midas -sqlite3 $(MEG3ANAOPTIONS) $(MEGANAMAKEOPTIONS)
else
	$(ROMESYS)/bin/romebuilder.exe -i analyzer/MEGAnalyzer.xml -o analyzer -sqlite3 $(MEG3ANAOPTIONS) $(MEGANAMAKEOPTIONS)
endif
	$(MAKE) -C analyzer pch
ifneq (,$(findstring distcc,$(CC) $(FC) $(CXX) $(CXXLD)))
	$(MAKE) $(JOBSFLAG) -C analyzer
else
	@echo 0 > .build_success
	@if [ -t 1 ] && [ -t 2 ]; then \
	  ($(MAKE) $(JOBSFLAG) -C analyzer \
	  || (echo $$? > .build_success; $(RM) .make_error.log)) \
	  3>&2 2>&1 1>&3 | tee .make_error.log; \
	else \
	  $(MAKE) $(JOBSFLAG) -C analyzer || echo $$? > .build_success; \
	fi
	@if [ -s .make_error.log ]; then \
	  echo; \
	  echo === WARNINGS SUMMARY ===; \
	  cat .make_error.log; \
	  echo; \
	fi
	@export BUILD_SUCCESS=`cat .build_success`; \
	if [ $$BUILD_SUCCESS -ne 0 ]; then \
	  $(RM) .make_error.log .build_success; \
	  exit $$BUILD_SUCCESS; \
	fi
	@echo
	@echo "==> $@ is done."
	@echo
	@$(RM) .make_error.log .build_success
endif

gem4: hooks rome
	(cd gem4/folders; ./build.sh)
	$(MAKE) -C gem4

rome: checkenv-rome
	$(MAKE) -C ${ROMESYS} dict
	$(MAKE) $(JOBSFLAG) -C ${ROMESYS}

common: hooks
	$(MAKE) -C $@

meg2db: hooks
	$(MAKE) -C ../$@
	@echo "==> $@ is done."

clean:
	-$(MAKE) clean -C bartender
	-$(MAKE) clean -C analyzer
	-$(MAKE) clean -C common
	-cd gem4; ./clean.sh

distclean:
	-$(MAKE) distclean -C bartender
	-$(MAKE) distclean -C analyzer
	-$(MAKE) distclean -C common
	-cd gem4; ./clean.sh

depclean:
	-$(MAKE) depclean -C bartender
	-$(MAKE) depclean -C analyzer
	-$(MAKE) depclean -C common

checkenv-meg:
ifneq ($(MAKECMDGOALS), check)
	@if [ -z ${MEG3SYS_COPY} ]; then \
	  echo Error: environment variable MEG3SYS not set.; \
	  exit 1; \
	fi
	@if [ ${CWD} != ${MEG3SYS_COPY} ]; then \
	  echo ; \
	  echo Warning: environment variable MEG3SYS does not match to your current working directory.; \
	  echo Makefile is about to override MEG3SYS as following:; \
	  echo MEG3SYS = ${MEG3SYS}; \
	  echo ; \
	  sleep 2; \
	fi
endif

checkenv-rome: checkenv-root checkenv-rootversion
	@if [ -z ${ROMESYS} ] ; then \
	  echo Error: environment variable ROMESYS not set.; \
	  exit 1; \
	fi
	@if [ ! -d ${ROMESYS} ] ; then \
	  echo Error: directory ROMESYS does not exist.; \
	  exit 1; \
	fi

checkenv-root:
	@if [ -z ${ROOTSYS} ]; then \
	  echo Error: environment variable ROOTSYS not set.; \
	  exit 1; \
	fi

checkenv-midas:
	@if [ -z ${MIDASSYS} ] ; then \
	  echo Error: environment variable MIDASSYS not set.; \
	  exit 1; \
	fi
	@if [ ! -d ${MIDASSYS} ] ; then \
	  echo Error: directlry MIDASSYS does not exist.; \
	  exit 1; \
	fi

checkenv-analyzer: checkenv-meg checkenv-root checkenv-rootversion

checkenv-bartender: checkenv-meg checkenv-root checkenv-rootversion

checkenv-rootversion:
ifeq ($(ROOT_VERSION_ERROR), yes)
	@echo ROOT $(ROOT_VERSION) is not supported.
	@echo Please update to $(ROOT_MAJOR_STD).$(ROOT_MINOR_STD)/$(ROOT_PATCH_STD) or later
	@exit 1
endif

checkenv-rootversion-std:
ifneq ($(ROOT_VERSION_EXACTLY_SAME_WITH_STANDARD), yes)
	@echo ROOT $(ROOT_VERSION) is not standard.
	@echo Please use $(ROOT_MAJOR_STD).$(ROOT_MINOR_STD)/$(ROOT_PATCH_STD).
	@exit 1
endif

checkenv-gccversion-std:
ifneq ($(GCC_VERSION_EXACTLY_SAME_WITH_STANDARD), yes)
	@echo GCC $(GCC_VERSION) is not standard.
	@echo Please use $(GCC_MAJOR_STD4).$(GCC_MINOR_STD4).$(GCC_PATCH_STD4) or $(GCC_MAJOR_STD3).$(GCC_MINOR_STD3).$(GCC_PATCH_STD3)
	@exit 1
endif

checkenv-platform-std:
ifneq ($(PLATFORM), linux)
	@echo Platform is not linux.
	@exit 1
endif

svn-update:
ifdef MAKECHECKREV
	svn update -r $(MAKECHECKREV) $(MEG3SYS)
else
	svn update $(MEG3SYS)
endif

print-standards:
	@if [ ! -s $(CHECKINGCOPY_FILENAME) ]; then \
	   echo "If you make here a checking copy, strict compile options will be always used."; \
	   echo -n "Do you use this directory as a checking copy ? "; \
	   read ANSWER; \
	   if [ $$ANSWER != "y" -a  $$ANSWER != "yes" -a $$ANSWER != "Y" -a  $$ANSWER != "YES" ]; then \
	      exit 1; \
	   fi; \
	fi
	@echo "This is a copy for checking. Compile flags will be changed automatically." > $(CHECKINGCOPY_FILENAME)
	@echo "Compile can run only in a standard environment." >> $(CHECKINGCOPY_FILENAME)
	@echo "You can disable this lock by removing a file '$(CHECKINGCOPY_FILENAME)'." >> $(CHECKINGCOPY_FILENAME)
	@echo "_________________________________________"
	@echo
	@echo "           MEG software check"
	@echo
	@echo " Standard ROOT version   .... $(ROOT_MAJOR_STD).$(ROOT_MINOR_STD)/$(ROOT_PATCH_STD)"
	@echo " Standard GCC  version   .... $(GCC_MAJOR_STD4).$(GCC_MINOR_STD4).$(GCC_PATCH_STD4) or $(GCC_MAJOR_STD3).$(GCC_MINOR_STD3).$(GCC_PATCH_STD3)"
#	@echo " Compile options for C   .... $(MEGOPT) $(MEGCFLAGS)"
#	@echo " Compile options for C++ .... $(MEGOPT) $(MEGCXXFLAGS)"
	@echo "_________________________________________"

check-std: checkenv-rootversion-std checkenv-gccversion-std  checkenv-platform-std

getmeg2lib:
	rsync -auvv meg-l-01.psi.ch:/meg/home/meg/offline/meg2lib ./
getmeg2lib-light:
	rsync -auvv --exclude='analyzer/noise' --exclude='common/drs' meg-l-01.psi.ch:/meg/home/meg/offline/meg2lib ./
