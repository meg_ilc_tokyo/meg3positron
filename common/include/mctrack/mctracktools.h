// $Id$
#ifndef MCTRACKTOOLS_H
#define MCTRACKTOOLS_H

#include <Rtypes.h>
#include <TClonesArray.h>

namespace MCTRACKTOOLS
{
Int_t TraceBack(const TClonesArray* tracks, const Int_t kParticleId, Int_t iTrackIndex);
}

#endif
