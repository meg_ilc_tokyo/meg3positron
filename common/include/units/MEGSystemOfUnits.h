// $Id$
// ----------------------------------------------------------------------
// MEG coherent system of Units
//
// This header was derived from CLHEP library and modified for MEG.
//
// The basic units are :
//              centimeter
//              second
//              Giga electron Volt
//              positron charge = 1e-9
//              degree Kelvin
//              amount of substance (mole)
//              luminous intensity (candela)
//              degree
//              steradian
//
// Below is a non exhaustive list of derived and pratical units
// (i.e. mostly the SI units).
// You can add your own units.
//
// The SI numerical value of the positron charge is defined here,
// as it is needed for conversion factor : positron charge = e_SI (coulomb)
//
// The others physical constants are defined in the header file :
//                        MEGPhysicalConstants.h
//
// Authors: S.Yamada, R.Sawada

#ifndef MEG_SYSTEM_OF_UNITS_H
#define MEG_SYSTEM_OF_UNITS_H

#ifdef __cplusplus
namespace MEG
{
#endif

//#ifndef HEP_SYSTEM_OF_UNITS_H

//-------------------------------------------------------------------------------
//
// Basic units
//
//-------------------------------------------------------------------------------


//
// Length [L]
//
static constexpr double centimeter  = 1.;
static constexpr double centimeter2 = centimeter * centimeter;
static constexpr double centimeter3 = centimeter * centimeter * centimeter;

static constexpr double millimeter  = 1.e-1 * centimeter;
static constexpr double millimeter2 = millimeter * millimeter;
static constexpr double millimeter3 = millimeter * millimeter * millimeter;

static constexpr double meter  = 100.*centimeter;
static constexpr double meter2 = meter * meter;
static constexpr double meter3 = meter * meter * meter;

static constexpr double kilometer  = 1000.*meter;
static constexpr double kilometer2 = kilometer * kilometer;
static constexpr double kilometer3 = kilometer * kilometer * kilometer;

static constexpr double parsec = 3.0856775807e+16 * meter;

static constexpr double micrometer = 1.e-6 * meter;
static constexpr double  nanometer = 1.e-9 * meter;
static constexpr double  angstrom  = 1.e-10 * meter;
static constexpr double  fermi     = 1.e-15 * meter;

static constexpr double      barn = 1.e-28 * meter2;
static constexpr double millibarn = 1.e-3 * barn;
static constexpr double microbarn = 1.e-6 * barn;
static constexpr double  nanobarn = 1.e-9 * barn;
static constexpr double  picobarn = 1.e-12 * barn;

// symbols
//static constexpr double nm  = nanometer;
//static constexpr double um  = micrometer;

//static constexpr double mm  = millimeter;
//static constexpr double mm2 = millimeter2;
//static constexpr double mm3 = millimeter3;

//static constexpr double cm  = centimeter;
//static constexpr double cm2 = centimeter2;
//static constexpr double cm3 = centimeter3;

//static constexpr double m  = meter;
//static constexpr double m2 = meter2;
//static constexpr double m3 = meter3;

//static constexpr double km  = kilometer;
//static constexpr double km2 = kilometer2;
//static constexpr double km3 = kilometer3;

//static constexpr double pc = parsec;

//
// Angle
//
static constexpr double degree      = 1.;
static constexpr double radian      = degree / (3.14159265358979323846 / 180.0);
static constexpr double milliradian = 1.e-3 * radian;

static constexpr double steradian = 1.;

// symbols
//static constexpr double rad  = radian;
//static constexpr double mrad = milliradian;
//static constexpr double sr   = steradian;
//static constexpr double deg  = degree;

//
// Time [T]
//
static constexpr double      second = 1.;
static constexpr double millisecond = 1.e-3 * second;
static constexpr double microsecond = 1.e-6 * second;
static constexpr double  nanosecond = 1.e-9 * second;
static constexpr double  picosecond = 1.e-12 * second;

static constexpr double     hertz = 1. / second;
static constexpr double kilohertz = 1.e+3 * hertz;
static constexpr double megahertz = 1.e+6 * hertz;

// symbols
//static constexpr double ns = nanosecond;
//static constexpr double  s = second;
//static constexpr double ms = millisecond;

//
// Electric charge [Q]
//
static constexpr double eplus       = 1.e-9;            // positron charge
static constexpr double e_SI        = 1.602176487e-19;  // positron charge in coulomb (PDG2008, CLHEP2.4&Geant4.10.04)
static constexpr double     coulomb = eplus / e_SI;     // coulomb = 6.24150 e+18 * eplus
// picocoulomb is defined later

//
// Energy [E]
//
static constexpr double gigaelectronvolt = 1.;
static constexpr double     electronvolt = 1.e-9 * gigaelectronvolt;
static constexpr double kiloelectronvolt = 1.e-6 * gigaelectronvolt;
static constexpr double megaelectronvolt = 1.e-3 * gigaelectronvolt;
static constexpr double teraelectronvolt = 1.e+3 * gigaelectronvolt;
static constexpr double petaelectronvolt = 1.e+6 * gigaelectronvolt;

static constexpr double joule = electronvolt / e_SI; // joule = 6.24150 e+12 * MeV

// symbols
static constexpr double MeV = megaelectronvolt;
static constexpr double  eV = electronvolt;
static constexpr double keV = kiloelectronvolt;
static constexpr double GeV = gigaelectronvolt;
static constexpr double TeV = teraelectronvolt;
static constexpr double PeV = petaelectronvolt;

//
// Temperature
//
static constexpr double kelvin = 1.;

//
// Amount of substance
//
static constexpr double mole = 1.;

//
// Luminous intensity [I]
//
static constexpr double candela = 1.;


//-------------------------------------------------------------------------------
//
// Derived units
//
//-------------------------------------------------------------------------------

//
// Mass [E][T^2][L^-2]
//
static constexpr double  kilogram = joule * second * second / (meter*meter);
static constexpr double      gram = 1.e-3 * kilogram;
static constexpr double milligram = 1.e-3 * gram;

// symbols
//static constexpr double  kg = kilogram;
//static constexpr double   g = gram;
//static constexpr double  mg = milligram;

//
// Power [E][T^-1]
//
static constexpr double watt = joule / second;         // watt = 6.24150 e+3 * MeV/ns

//
// Force [E][L^-1]
//
static constexpr double newton = joule / meter;        // newton = 6.24150 e+9 * MeV/mm

//
// Pressure [E][L^-3]
// (pascal is a C reserved word on Windows)
static constexpr double hep_pascal = newton / meter / meter; // pascal = 6.24150 e+3 * MeV/mm3
//static constexpr double bar        = 100000*hep_pascal;// bar    = 6.24150 e+8 * MeV/mm3
static constexpr double hep_bar    = 100000 * hep_pascal; // bar    = 6.24150 e+8 * MeV/mm3
static constexpr double atmosphere = 101325 * hep_pascal; // atm    = 6.32420 e+8 * MeV/mm3

//
// Electric current [Q][T^-1]
//
static constexpr double      ampere = coulomb / second; // ampere = 6.24150 e+9 * eplus/ns
static constexpr double milliampere = 1.e-3 * ampere;
static constexpr double microampere = 1.e-6 * ampere;
static constexpr double  nanoampere = 1.e-9 * ampere;

//
// Electric potential [E][Q^-1]
//
static constexpr double      volt = electronvolt / eplus;
static constexpr double  megavolt = 1.e6 * volt;
static constexpr double  kilovolt = 1.e3 * volt;
// millivolt is defined later
// microvolt is defined later

//
// Electric resistance [E][T][Q^-2]
//
static constexpr double ohm = volt / ampere;           // ohm = 1.60217e-16*(MeV/eplus)/(eplus/ns)

//
// Electric capacitance [Q^2][E^-1]
//
static constexpr double      farad = coulomb / volt;   // farad = 6.24150e+24 * eplus/Megavolt
static constexpr double millifarad = 1.e-3 * farad;
static constexpr double microfarad = 1.e-6 * farad;
static constexpr double  nanofarad = 1.e-9 * farad;
static constexpr double  picofarad = 1.e-12 * farad;

//
// Magnetic Flux [T][E][Q^-1]
//
static constexpr double weber = volt * second;         // weber = 1000*megavolt*ns

//
// Magnetic Field [T][E][Q^-1][L^-2]
//
static constexpr double     tesla = volt * second / meter2; // tesla =0.001*megavolt*ns/mm2
static constexpr double     gauss = 1.e-4 * tesla;
static constexpr double kilogauss = 1.e-1 * tesla;

//
// Inductance [T^2][E][Q^-2]
//
static constexpr double henry = weber / ampere;      // henry = 1.60217e-7*MeV*(ns/eplus)**2

//
// Activity [T^-1]
//
static constexpr double becquerel = 1. / second ;
static constexpr double     curie = 3.7e+10 * becquerel;

//
// Absorbed dose [L^2][T^-2]
//
static constexpr double gray = joule / kilogram ;


//
// Luminous flux [I]
//
static constexpr double lumen = candela * steradian;

//
// Illuminance [I][L^-2]
//
static constexpr double lux = lumen / meter2;

//
// Miscellaneous
//
static constexpr double perCent     = 0.01 ;
static constexpr double perThousand = 0.001;
static constexpr double perMillion  = 0.000001;
//#endif /* HEP_SYSTEM_OF_UNITS_H */

static constexpr double millivolt = 1.e-3 * volt;
static constexpr double microvolt = 1.e-6 * volt;
static constexpr double picocoulomb = 1.e-12 * coulomb;

#ifdef __cplusplus
} // namespace MEG
#endif

#endif /* MEG_SYSTEM_OF_UNITS_H */
