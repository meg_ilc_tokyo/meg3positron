// $Id$
// ----------------------------------------------------------------------
// MEG coherent Physical Constants
//
// This header was derived from CLHEP library and modified for MEG.
//
// The basic units are :
//              centimeter
//              second
//              Giga electron Volt
//              positon charge = 1e-9
//              degree Kelvin
//              amount of substance (mole)
//              luminous intensity (candela)
//              degree
//              steradian
//
// Below is a non exhaustive list of Physical CONSTANTS,
// computed in the Internal MEG System Of Units.
//
// Most of them are extracted from the Particle Data Book :
//        Phys. Rev. D  volume 50 3-1 (1994) page 1233
//
//        ...with a meaningful (?) name ...
//
// You can add your own constants.
//
// Author: S.Yamada, R.Sawada
//

#ifndef MEG_PHYSICAL_CONSTANTS_H
#define MEG_PHYSICAL_CONSTANTS_H

#include "units/MEGSystemOfUnits.h"

#ifdef __cplusplus
namespace MEG
{
#endif

//#ifndef HEP_PHYSICAL_CONSTANTS_H

static constexpr double     pi  = 3.14159265358979323846;
static constexpr double  twopi  = 2 * pi;
static constexpr double halfpi  = pi / 2;
static constexpr double     pi2 = pi * pi;

//
//
//
static constexpr double Avogadro = 6.02214179e+23 / mole;

//
// c   = 299.792458 mm/ns
// c^2 = 898.7404 (mm/ns)^2
//
static constexpr double c_light   = 2.99792458e+8 * meter / second;
static constexpr double c_squared = c_light * c_light;

//
// h     = 4.13566e-12 MeV*ns
// hbar  = 6.58212e-13 MeV*ns
// hbarc = 197.32705e-12 MeV*mm
//
static constexpr double h_Planck      = 6.62606896e-34 * joule * second;
static constexpr double hbar_Planck   = h_Planck / twopi;
static constexpr double hbarc         = hbar_Planck * c_light;
static constexpr double hbarc_squared = hbarc * hbarc;

//
//
//
static constexpr double electron_charge = -eplus; // see SystemOfUnits.h
static constexpr double e_squared = eplus * eplus;

//
// amu_c2 - atomic equivalent mass unit
// amu    - atomic mass unit
//
static constexpr double  electron_mass_c2 = 0.510998910 * MeV; // PDG 2008
static constexpr double    proton_mass_c2 = 938.272029  * MeV; // PDG 2007
static constexpr double   neutron_mass_c2 = 939.565360  * MeV; // PDG 2007
static constexpr double            amu_c2 = 931.494028  * MeV; // PDG 2007
static constexpr double               amu = amu_c2 / c_squared;
// muon_mass_c2 is defined later
// half_muon_mass_c2 is defined later

//
// permeability of free space mu0    = 2.01334e-16 Mev*(ns*eplus)^2/mm
// permittivity of free space epsil0 = 5.52636e+10 eplus^2/(MeV*mm)
//
static constexpr double mu0      = 4 * pi * 1.e-7 * henry / meter;
static constexpr double epsilon0 = 1. / (c_squared*mu0);

//
// electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
//
static constexpr double elm_coupling            = e_squared / (4 * pi*epsilon0);
static constexpr double fine_structure_const    = elm_coupling / hbarc;
static constexpr double classic_electr_radius   = elm_coupling / electron_mass_c2;
static constexpr double electron_Compton_length = hbarc / electron_mass_c2;
static constexpr double Bohr_radius             = electron_Compton_length / fine_structure_const;

static constexpr double alpha_rcl2 = fine_structure_const
                                     * classic_electr_radius
                                     * classic_electr_radius;

static constexpr double twopi_mc2_rcl2 = twopi * electron_mass_c2
                                         * classic_electr_radius
                                         * classic_electr_radius;
//
//
//
static constexpr double k_Boltzmann = 8.617343e-11 * MeV / kelvin;

//
//
//
static constexpr double STP_Temperature = 273.15 * kelvin;
static constexpr double STP_Pressure    = 1.*atmosphere;
static constexpr double kGasThreshold   = 10.*milligram / centimeter / centimeter / centimeter;

//
//
//
static constexpr double universe_mean_density = 1.e-25 * gram / centimeter / centimeter / centimeter;

//#endif /* HEP_PHYSICAL_CONSTANTS_H */

static constexpr double      muon_mass_c2 = 105.6583668 * MeV; // PDG 2008
static constexpr double half_muon_mass_c2 = muon_mass_c2 / 2;

#ifdef __cplusplus
} // namespace MEG
#endif

#endif /* MEG_PHYSICAL_CONSTANTS_H */
