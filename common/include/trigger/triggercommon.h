// $Id$
#ifndef TRIGGERCOMMON_H
#define TRIGGERCOMMON_H

//  The format of the TRGI Trigger Information BANK is a vector of DWORDs
//  00====RUNNUMBER
//  01==EVENTNUMBER
//  02====EVENTTYPE
//  03=TYPE1BITPTRN
#define TRGI_RUNNUMBER         0
#define TRGI_RUNNUMBER_MASK    0xFFFFFFFF
#define TRGI_EVENTNUMBER       1
#define TRGI_EVENTNUMBER_MASK  0xFFFFFFFF
#define TRGI_EVENTTYP          2
#define TRGI_EVENTTYP_MASK     0xFFFFFFFF
#define TRGI_BITPATTERN        3
#define TRGI_BITPATTERN_MASK   0xFFFFFFFF
#define TRGI_LIVETIME          4
#define TRGI_LIVETIME_MASK     0xFFFFFFFF
#define TRGI_TOTALTIME         5
#define TRGI_TOTALTIME_MASK    0xFFFFFFFF
#define TRGI_NUMBEROFBINS      6
#define TRGI_NUMBEROFBINS_MASK 0xFFFFFFFF
#define TRGI_DELAY             7
#define TRGI_DELAY_MASK        0xFFFFFFFF
#define TRGI_FIREDTRGS         8
#define TRGI_FIREDTRGS_MASK    0xFFFFFFFF
#define TRGI_PROTCURR          9
#define TRGI_PROTCURR_MASK     0xFFFFFFFF
#define TRGI_PCUR_NORM         10
#define TRGI_PCUR_NORM_MASK    0xFFFFFFFF

#define TRGNTCB_1       7
#define TRGNTCB_2       3
#define TRGTYPES       64
#define TRGTYPESTEMP    9


// Trigger descriptions
namespace TRIGGERCOMMON
{

// Trigger definitions
static constexpr Int_t  MEG =                        0;
static constexpr Int_t  MEG_LOW_Q =                  1;
static constexpr Int_t  MEG_WIDE_ANGLE =             2;
static constexpr Int_t  MEG_WIDE_TIME =              3;
static constexpr Int_t  RMD_NARROW_TIME =            4;
static constexpr Int_t  RMD_WIDE_TIME =              5;
static constexpr Int_t  MICHEL =                     6;
//static constexpr Int_t  UNUSED =                     7;
//static constexpr Int_t  UNUSED =                     8;
//static constexpr Int_t  UNUSED =                     9;
static constexpr Int_t  LXE_HIGH_Q =                10;
static constexpr Int_t  LXE_LOW_Q =                 11;
static constexpr Int_t  LXE_ALPHA =                 12;
static constexpr Int_t  LXE_LED_PM =                13;
static constexpr Int_t  LXE_LED_MPPC =              14;
static constexpr Int_t  LXE_COSMICS =               15;
//static constexpr Int_t  UNUSED =                    16;
//static constexpr Int_t  UNUSED =                    17;
//static constexpr Int_t  UNUSED =                    18;
//static constexpr Int_t  UNUSED =                    19;
static constexpr Int_t  TC_TRACK =                  20;
static constexpr Int_t  TC_COSMIC =                 21;
static constexpr Int_t  TC_SINGLE =                 22;
static constexpr Int_t  TC_LASER =                  23;
static constexpr Int_t  TC_LASER_DIODE =            24;
//static constexpr Int_t  UNUSED =                    25;
//static constexpr Int_t  UNUSED =                    26;
//static constexpr Int_t  UNUSED =                    27;
//static constexpr Int_t  UNUSED =                    28;
//static constexpr Int_t  UNUSED =                    29;
static constexpr Int_t  RDC_LXE =                   30;
static constexpr Int_t  RDC_COINC =                 31;
static constexpr Int_t  RDC_SCINT =                 32;
static constexpr Int_t  RDC_LYSO_SUM =              33;
static constexpr Int_t  RDC_LYSO_OR =               34;
//static constexpr Int_t  UNUSED =                    35;
//static constexpr Int_t  UNUSED =                    36;
//static constexpr Int_t  UNUSED =                    37;
//static constexpr Int_t  UNUSED =                    38;
//static constexpr Int_t  UNUSED =                    39;
static constexpr Int_t  DC_TRACK =                  40;
static constexpr Int_t  DC_CRC =                    41;
static constexpr Int_t  DC_COSMIC =                 42;
static constexpr Int_t  CRC_PAIR =                  43;
static constexpr Int_t  CRC_SINGLE =                44;
static constexpr Int_t  DC_SINGLE =                 45;
static constexpr Int_t  DC_MONITORING =             46;
static constexpr Int_t  DC_MON_SCINT =              47;
//static constexpr Int_t  UNUSED =                    48;
//static constexpr Int_t  UNUSED =                    49;
static constexpr Int_t  Pi0 =                       50;
static constexpr Int_t  Pi0_NO_PRESH =              51;
static constexpr Int_t  BGO_SINGLE =                52;
static constexpr Int_t  BGO_SUM =                   53;
static constexpr Int_t  BGO_COSMICS =               54;
static constexpr Int_t  PRESH_SINGLE =              55;
static constexpr Int_t  CW_BORON =                  56;
static constexpr Int_t  SCIFI =                     57;
static constexpr Int_t  NEUTRON_GENERATOR =         58;
static constexpr Int_t  RF_ACCEL =                  59;
//static constexpr Int_t  UNUSED =                    60;
//static constexpr Int_t  UNUSED =                    61;
//static constexpr Int_t  UNUSED =                    62;
static constexpr Int_t  PEDESTAL =                  63;

// Trigger counter normalisation"
// false -> live time
// true -> total time
   static constexpr Bool_t  TCOUMODE[64] = {false, false, false, false,
                                            false, false, false, false,
                                            false, false, false, false,
                                            false,  true,  true, false,
                                            false, false, false, false,
                                             true,  true,  true,  true,
                                             true,  true, false, false,
                                            false, false, false, false,
                                             true, false,  true, false,
                                            false, false, false, false,                                            
                                            false, false, false,  true,
                                             true, false, false,  true,
                                            false, false, false, false,
                                            false, false, false, false,
                                            false, false, false, false,
                                            false, false, false,  true};
                                            


extern const char *triggerString[];


typedef struct {
   UInt_t TrgType;
   Int_t TrgPattern[2];
   UInt_t TotalTime;
   UInt_t LiveTime;
   UInt_t EventCounter;
   UInt_t ProtonCurrent;
   struct {
      UInt_t TotalTime;
      UInt_t EventCounter;
   } BoardData[TRGNTCB_1 + TRGNTCB_2];
} TRGIFormat;

typedef struct {
   UInt_t TrgCounter[64];
} TRGCFormat;

const int XECnp = 128; //1024 before run 304617
typedef struct {
   UInt_t TriggerCell;
   UInt_t QSUMWf[XECnp];
   UInt_t XECInfo[XECnp];
} TXECFormat;

const int ALPnp = 128;
typedef struct {
   UInt_t TriggerCell;
   UInt_t AmplitudeWf[ALPnp];
   UInt_t ChargeWf[ALPnp];
} TALPFormat;

const int SPXnp = 128;
typedef struct {
   UInt_t TriggerCell;
   UInt_t LowBits[SPXnp];
   UInt_t HighBits[SPXnp];
} TSPXFormat;

const int BGOnp = 128;
typedef struct {
   UInt_t TriggerCell;
   UInt_t BGOWf[BGOnp];
} TBGOFormat;

const int RDCnp = 128;
typedef struct {
   UInt_t TriggerCell;
   UInt_t RDCLYSOWf[RDCnp];
   UInt_t RDCPlasticDiscr[RDCnp];
} TRDCFormat;

const int GENnp = 32;
typedef struct {
   UInt_t TriggerCell;
   UInt_t LowBits[GENnp];
   UInt_t HighBits[GENnp];
} TGENFormat;

const int TDCnp = 128;
//Alternative way to read TDC Memories
typedef struct {
   UInt_t TriggerCell;
   struct {
      UInt_t Down[TDCnp];
      UInt_t Up[TDCnp];
   } ChData[16];
} TTDCFormatAlt;

typedef struct {
   UInt_t TriggerCell;
   UInt_t TDCData[(TDCnp + TDCnp) * 16];
} TTDCFormat;

const int TIN1np = 128;
typedef struct {
   struct {
      UInt_t TriggerCell;
      struct {
         UInt_t Down[TIN1np];
         UInt_t Up[TIN1np];
      } ChData[16];
   } BoardData[TRGNTCB_1];
} TIN1Format;

const int TIN2np = 128;
typedef struct {
   UInt_t TriggerCell;
   struct {
      UInt_t Down[TIN2np];
      UInt_t Up[TIN2np];
   } ChData[4];
} TIN2Format;
}

#endif
