/********************************************************************\

   Name:         romecompat.h
   Created by:   Ryu Sawada

   Contents:     Header file for romecompat.c

   $Id$

\********************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

int romecompat();

#ifdef __cplusplus
}
#endif

