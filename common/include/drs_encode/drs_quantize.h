/* $Id$ */
#ifndef DRA_QUANTIZE_H
#define DRA_QUANTIZE_H
/* IMPORTANT !!!
   Do not change existing array.
   Otherwise encoded data can not be decoded any more
*/


/*
 * Quantize array for DCT transformed data
 * i-th coeffcient is compression factor of frequency component
 * where period is N/(i+1).
 * Factors should be larger than 1.
 */
static double dct_quantize_array[][1024] = {
   /*******************************************************************************
    * ID : 0
    * flat quantize
    * N = 64
    *******************************************************************************/
   {
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1
   },

   {0}
};

/*
 * Quantize array for wavelet transformed data
 * First element is compression factor for baseline followed by those for levels
 * i-th(i!=0) coeffcient is compression factor of frequency component
 * where period is (total bin number)/(2^(i-1)).
 * Factors should be larger than 1.
 */
static double wavelet_quantize_array[][11] = {
   /*******************************************************************************
    * ID : 0
    * flat quantize
    *******************************************************************************/
   {
      1
      , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
   },

   {0}
};

#endif
