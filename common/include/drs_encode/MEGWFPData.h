// $Id$
// Author : Ryu Sawada

#ifndef MEGWFPData_H
#define MEGWFPData_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// ROOT TObject to store WFP data
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "RConfig.h"
#include "TObject.h"
#include "TClass.h"
#include "wfp_encode.h"

class MEGWFPData : public TObject
{
protected:
   UShort_t    fFlags;          // 0-2:header type, 3-4:drs version, 5-9:data kind, 10-14:parameters, 15:1
   Int_t       fDsize;          // size of encoded WFP data
   char       *fData;           // [fDsize] encoded WFP data
   Double_t    fValue;          // ! decoded data
   Bool_t      fNotDeleteData;  // ! flag to avoid this instance to delete encoded data.

public:
   MEGWFPData(Int_t dsize = 0, UShort_t flags = 0);
   virtual ~MEGWFPData();
   MEGWFPData(const MEGWFPData &c);
   MEGWFPData& operator=(const MEGWFPData &c);

   void     Reset();
   void     Set(UShort_t headerFormat, UShort_t DRSVersion, UShort_t dataKind, UShort_t parameters);
   void     InitData(UShort_t flags);
   void     SetEventData(Int_t dsize, void *pdata);
   void     SetValue(Double_t value) { fValue = value; }

   UShort_t GetFlags() const           { return fFlags; }
   Int_t    GetDsize() const           { return fDsize; }
   char     GetDataAt(Int_t i) const   { return fData[i]; }
   char    *GetData() const            { return fData; }

   UShort_t GetHeaderFormat() const    { return WFP_ENCODE::wfp_data_extract_header_type(fFlags); }
   UShort_t GetDRSVersion() const      { return WFP_ENCODE::wfp_data_extract_drs_version(fFlags); }
   UShort_t GetDataKind() const        { return WFP_ENCODE::wfp_data_extract_data_kind(fFlags); }
   UShort_t GetParameters() const      { return WFP_ENCODE::wfp_data_extract_parameters(fFlags); }
   Double_t GetValue() const           { return fValue; }

   void SetFlags(UShort_t flags)        { fFlags    = flags; }
   void SetDsize(Int_t    dsize)        { fDsize    = dsize; }
   void SetDataAt(Int_t i, char data)    { fData[i]  = data; }
   void SetData(char *pdata);
   void DeleteData();

   void SetNotDeleteData(Bool_t  f)     { fNotDeleteData = f; }
   void SetHeaderFormat(UShort_t value) { WFP_ENCODE::wfp_data_insert_header_type(&fFlags, value); }
   void SetDRSVersion(UShort_t value) { WFP_ENCODE::wfp_data_insert_drs_version(&fFlags, value); }
   void SetDataKind(UShort_t value) { WFP_ENCODE::wfp_data_insert_data_kind(&fFlags, value); }
   void SetParameters(UShort_t value) { WFP_ENCODE::wfp_data_insert_parameters(&fFlags, value); }

   void AllocateData(Int_t size);

   void Encode(Double_t data);
   void Encode(UShort_t headerFormat, UShort_t DRSVersion,  UShort_t dataKind,
               UShort_t parameters, Double_t data);
   Int_t Decode();
   Int_t Encode();

   ClassDef(MEGWFPData, 1) // WFP raw data format
};

inline MEGWFPData::~MEGWFPData()
{
   DeleteData();
}

inline void MEGWFPData::Set(UShort_t headerFormat, UShort_t DRSVersion, UShort_t dataKind, UShort_t parameters)
{
   SetHeaderFormat(headerFormat);
   SetDRSVersion(DRSVersion);
   SetDataKind(dataKind);
   SetParameters(parameters);
}

inline void MEGWFPData::InitData(UShort_t flags)
{
   SetFlags(flags);
}

inline void MEGWFPData::SetEventData(Int_t dsize, void *pdata)
{
   SetDsize(dsize);
   SetData(static_cast<char*>(pdata));
}

inline void MEGWFPData::SetData(char *pdata)
{
   DeleteData();
   fData = pdata;
}

inline void MEGWFPData::DeleteData()
{
   if (fData && !fNotDeleteData) {
      delete [] fData; fData = 0;
   }
}

inline void MEGWFPData::AllocateData(Int_t size)
{
   if (size < 0) {
      return;
   } else if (size == 0) {
      DeleteData();
   } else {
      DeleteData();
      fData = new char[size];
   }
   return;
}

inline void MEGWFPData::Encode(UShort_t headerFormat, UShort_t DRSVersion, UShort_t dataKind,
                               UShort_t parameters, Double_t data)
{
   SetFlags(parameters | dataKind | DRSVersion | headerFormat);
   Encode(data);
}


#endif   // MEGWFPData_H
