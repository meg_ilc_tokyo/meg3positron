/* $Id$ */
/*
 * drived from gsl/wavelet/gsl_wavelet.h
 * http://www.gnu.org/software/gsl/
 */

#define WAVELET_FAILURE -1
#define WAVELET_SUCCESS 0
#ifndef M_SQRT1_2
#define M_SQRT1_2 0.70710678118654752440084436210       /* sqrt(1/2) */
#endif

#ifndef WAVELET_H
#define WAVELET_H
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
   wavelet_forward = 1, wavelet_backward = -1
} wavelet_direction;

typedef enum {
   wavelet_id_daubechies = 0,
   wavelet_id_daubechies_centered = 1,
   wavelet_id_haar = 2,
   wavelet_id_haar_centered = 3,
   wavelet_id_bspline = 4,
   wavelet_id_bspline_centered = 5
} wavelet_type_id;

typedef struct {
   const char *name;
   int (*init)(const double **h1, const double **g1, const double **h2, const double **g2, size_t *nc, size_t *offset, size_t member);
} wavelet_type;

typedef struct {
   const wavelet_type *type;
   const double *h1;
   const double *g1;
   const double *h2;
   const double *g2;
   size_t nc;
   size_t offset;
   short type_id;
   short member;
} wavelet;

typedef struct {
   double *scratch;
   size_t n;
} wavelet_workspace;

extern const wavelet_type *wavelet_daubechies;
extern const wavelet_type *wavelet_daubechies_centered;
extern const wavelet_type *wavelet_haar;
extern const wavelet_type *wavelet_haar_centered;
extern const wavelet_type *wavelet_bspline;
extern const wavelet_type *wavelet_bspline_centered;

int binary_logn(const size_t n);
wavelet *wavelet_alloc(const wavelet_type *T, short id, size_t k);
void wavelet_free(wavelet *w);
const char *wavelet_name(const wavelet *w);

wavelet_workspace *wavelet_workspace_alloc(size_t n);
void wavelet_workspace_free(wavelet_workspace *work);

int wavelet_transform(const wavelet *w, double *data, size_t stride, size_t n, wavelet_direction dir, wavelet_workspace *work);

int wavelet_transform_forward(const wavelet *w, double *data, size_t stride, size_t n, wavelet_workspace *work);

int wavelet_transform_inverse(const wavelet *w, double *data, size_t stride, size_t n, wavelet_workspace *work);

#ifdef __cplusplus
}
#endif
#endif                          /* WAVELET_H */
