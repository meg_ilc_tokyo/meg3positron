// Author : Ryu Sawada

#ifndef MEGDRSChipData_H
#define MEGDRSChipData_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// ROOT TObject to store DRS data
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "RConfig.h"
#include "TObject.h"
#include "TClass.h"
#include "TClonesArray.h"
#include "drs_encode/drs_encode.h"
#include "drs_encode/MEGDRSChannelData.h"
#include "waveform/MEGDRSWaveform.h"
#include "constants/drs/drsconst.h"
#include <vector>
#include <set>
#include <map>
#include <deque>
class TStyle;
class TObjArray;
class TH1F;
class TH1I;
class TH2S;
class TString;
class ROMEDataBase;
class MEGDRSChannelData;


class MEGDRSChipData : public TObject
{
public:

protected:
   Bool_t                  fHaveData;                   // Flag if it has event data
   UShort_t                fAddress;                    // see drs_encode.h
   Double_t                fMeasuredFrequency;          // Measured average value of sampling speed
   Float_t                 fOnBoardTemperature;         // Measured temperature in DAQ (K)
   Float_t                 fOnBoardFrequency;           // Measured frequency in DAQ (Hz)
   Float_t                 fStartCellDelay;             //! Delay time of startcell in DAQ (s)

   TObjArray              *fChannels;                   // MEGDRSChannelData objects
   std::vector<std::vector<Double_t> > fTimeCalibrations; //! Time calibration data for channels
   std::vector<EDRSTimeCalibrationType> fTimeCalibrationType;//! Time calibration type for channels
   static Int_t            fgNumberOfChips;             //! Total number of this class object
   Int_t                   fIndex;                      //! Internal index of this chip in MEGDRSChipData class


public:
   MEGDRSChipData(UShort_t haveData = kFALSE, UShort_t address = 0);
   virtual ~MEGDRSChipData();
   MEGDRSChipData(const MEGDRSChipData &c);
   MEGDRSChipData& operator=(const MEGDRSChipData &c);

   void     Reset();
   Bool_t   HaveData() const { return fHaveData; }
   void     InitData(Bool_t haveData, UShort_t address);
   void     SetEventData(Bool_t haveData, Float_t temperature, Float_t frequency);
   void     SetOnBoardTemperature(Float_t temp)  { fOnBoardTemperature = temp; }
   void     SetOnBoardFrequency(Float_t freq)    { fOnBoardFrequency = freq; }
   void     SetHaveData(Bool_t flg = kTRUE)  { fHaveData = flg; }
   void     SetStartCellDelay(Float_t delay) { fStartCellDelay = delay; }

   UShort_t GetAddress() const            { return fAddress; }
   Float_t  GetOnBoardTemperature() const { return fOnBoardTemperature; }
   Float_t  GetOnBoardFrequency() const   { return fOnBoardFrequency; }

   Int_t    GetNumberOfChannels() const { return kNumberOfChannelsOnDRSChipVer4; }

   MEGDRSWaveform*    GetWaveformAt(Int_t i) const
   {
      return fChannels->At(i) ?
             static_cast<MEGDRSChannelData*>(fChannels->At(i))->GetWaveform() : 0;
   }
   MEGDRSChannelData* GetChannelAt(Int_t i) const  { return static_cast<MEGDRSChannelData*>(fChannels->At(i)); }
   std::vector<Double_t> &GetTimeCalibrationAt(Int_t i) { return fTimeCalibrations[i]; }

   Float_t GetStartCellDelay() const { return fStartCellDelay;}

   void    SetAddress(UShort_t address)         { fAddress = address; }

   void    SetWaveformAt(Int_t i, MEGDRSWaveform* wf)
   {
      if (fChannels->At(i)) {
         static_cast<MEGDRSChannelData*>(fChannels->At(i))->SetWaveform(wf);
      }
   }

   void    DecodeWaves();
   void    ResetWaves();
   void    ZeroWaves();
   void    SetIndex();
   void    CalibrateWaves(Int_t method);
   void    ReadTimeCalibDatabase(ROMEDataBase *db, Int_t id, Long64_t runNumber, Long64_t eventNumber);

   ClassDef(MEGDRSChipData, 6) // DRS chip raw data format
};

inline MEGDRSChipData::~MEGDRSChipData()
{
   // destruct channels
   if (fChannels) {
      fChannels->Delete();
      delete fChannels;
   }
}

inline void MEGDRSChipData::InitData(Bool_t haveData, UShort_t address)
{
   SetHaveData(haveData);
   SetAddress(address);
}

inline void MEGDRSChipData::SetEventData(Bool_t haveData, Float_t temperature, Float_t frequency)
{
   SetHaveData(haveData);
   SetOnBoardTemperature(temperature);
   SetOnBoardFrequency(frequency);
}

#endif   // MEGDRSChipData_H
