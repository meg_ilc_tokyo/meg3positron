/********************************************************************
  DRSCalibration.h, S.Ritt, M. Schneebeli - PSI

  $Id$

********************************************************************/
#ifndef DRSCalibration_H
#define DRSCalibration_H

namespace DRSCalibration
{
const int kNumberOfBins = 1024;
int RotateWave(int startCell, float *waveform, float *time, float &rotatedStart, int &triggerStartBin);
int RotateWave(int startCell, double *waveform, double *time, float &rotatedStart, int &triggerStartBin);
int GetTriggerCell(float *waveform, bool responseCalib);
int GetTriggerCell(double *waveform, bool responseCalib);
bool GetCalibratedTime(const char *path, unsigned int boardNumber, unsigned int chipIndex, unsigned int frequency, double *time);
};

#endif                          // DRSCalibration_H
