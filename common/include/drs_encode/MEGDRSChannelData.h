// Author : Ryu Sawada

#ifndef MEGDRSChannelData_H
#define MEGDRSChannelData_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// ROOT TObject to store DRS data
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "RConfig.h"
#include "TObject.h"
#include "TClass.h"
#include "TClonesArray.h"
#include "drs_encode/drs_encode.h"
#include "waveform/MEGDRSWaveform.h"
class ROMEDataBase;

const Int_t kInitialParMode11Size = 16;
enum class EDRSTimeCalibrationType {kFixedInterval, kInData, kExternal, kNEDRSTimeCalibrationType};

class MEGDRSChannelData : public TObject
{
public:
   enum EReferenceTimeType {
      kTimeZero500,
      kTimeZero600,
      kTimeZero700,
      kTimeZero800,
      kTimeZero900,
      kTimeZero1000,
      kTimeZero1100,
      kTimeZero1200,
      kTimeZero1300,
      kTimeZero1400,
      kTimeZero1500,
      kTimeZero1600,
      kTimeZero1700,
      kTimeZero1800,
      kTimeZero1900,
      kTimeZero2000,
      kNReferenceTimeType
   };
   enum ECalibrationStatus {
      kNotApplied,
      kTimeZeroMethod,
      kShiftTimeApplied
   };

protected:
   UShort_t      fAddress;                     // see drs_encode.h
   UShort_t      fTriggerCell;                 // bin in which trigger occured

   UShort_t      fVFlags;                      // see drs_encode.h
   Int_t         fVDsize;                      // size of waveform in bytes
   UShort_t      fVFirstBin;                   // first bin which gets encoded, can be used to define ROI
   char         *fVData;                       // [fVDsize] encoded DRS data

   UShort_t      fTFlags;                      // see drs_encode.h
   Int_t         fTDsize;                      // size of timing waveform
   UShort_t      fTFirstBin;                   // first bin which gets encoded, can be used to define ROI
   UShort_t      fDt;                          // nominal bin width in (psec)
   char         *fTData;                       // [fTDsize] encoded DRS data

   Bool_t        fNotDeleteVData;              // ! flag to avoid this instance to delete encoded time data.
   Bool_t        fNotDeleteTData;              // ! flag to avoid this instance to delete encoded time data.

   Short_t       fMethodMode11;                // ! rebinning method for encode mode 11
   Short_t      *fParMode11;                   // ! parameter array for encode mode 11;
   Double_t     *fTimeCalibMode11;             // ! time calibration for encode mode 11;
   Float_t       fShiftTime;                   // shift time relative to t(last cell) = 0 applied (added)
   //                                             or to be applied (added) to data

   std::vector<Double_t>*fTimeCalibrationData; // ! Pointer to time calibration externally allocated
   EDRSTimeCalibrationType* fTimeCalibrationType;// ! Pointer to time calibration type (object is to be allocated in MEGDRSChipData and to be set to this)
   MEGDRSWaveform *fWaveform;                  // ! connected waveform class
   Bool_t          fWaveformAllocatedByMyself; // ! flag if a waveform is allocated by this instance
   Int_t           fRebinSize[kDRSBins];       // ! rebin size of each channels

   Short_t         fCalibrationStatus;         //  Status of calibration
   static std::vector<MEGDRSChannelData*> fgReferenceChannel; //! Reference time instance
   static std::vector<Double_t> fgReferenceTime;              //! Reference time for TimeZeroSync

public:
   MEGDRSChannelData(Int_t vdsize = 0, Int_t tdsize = 0, UShort_t vflags = 0, UShort_t tflags = 0, UShort_t address = 0);
   virtual ~MEGDRSChannelData();
   MEGDRSChannelData(const MEGDRSChannelData &c);
   MEGDRSChannelData& operator=(const MEGDRSChannelData &c);

   void          Reset();
   void          CalibrateWave(Int_t method);
   void          Set(UShort_t DRSVersionBit,
                     UChar_t crate, UChar_t slot, UChar_t chip, UChar_t channel,
                     UShort_t vHeaderFormat, UShort_t vEncodingMode,
                     UShort_t tHeaderFormat, UShort_t tEncodingMode);
   void          SetVEventData(Int_t vdsize, UShort_t vFirstBin, UShort_t vflags, void *vdata);
   void          SetTEventData(Int_t tdsize, UShort_t tFirstBin, UShort_t tflags, void *tdata,
                               UShort_t triggerCell, UShort_t dt);
   void          SetWaveform(MEGDRSWaveform* wf) { fWaveform = wf; if (wf) fWaveformAllocatedByMyself = kFALSE;}
   void          SetWaveformAllocatedByMyself(Bool_t flg) { fWaveformAllocatedByMyself = flg; }
   void          SetTimeCalibrationData(std::vector<Double_t> *cal, EDRSTimeCalibrationType* type)
   { fTimeCalibrationData = cal; fTimeCalibrationType = type; }
   void          CopyTimeCalibrationData(const std::vector<Double_t> &cal) { *fTimeCalibrationData = std::move(cal); }
   void          SetTimeCalibrationType(EDRSTimeCalibrationType* type) {fTimeCalibrationType = type;}

   UShort_t      GetAddress() const        { return fAddress; }
   UShort_t      GetVFlags() const         { return fVFlags; }
   Int_t         GetVDsize() const         { return fVDsize; }
   char          GetVDataAt(Int_t i) const { return fVData[i]; }
   UShort_t      GetVFirstBin() const      { return fVFirstBin; }
   char         *GetVData() const          { return fVData; }

   UShort_t      GetTFlags() const         { return fTFlags; }
   Int_t         GetTDsize() const         { return fTDsize; }
   char          GetTDataAt(Int_t i) const { return fTData[i]; }
   UShort_t      GetTFirstBin() const      { return fTFirstBin; }
   char         *GetTData() const          { return fTData; }
   UShort_t      GetDt() const             { return fDt; }

   UShort_t GetDRSVersion() const   { return DRS_ENCODE::drs_versionbit_to_version(DRS_ENCODE::drs_channelv_extract_drs_version(fVFlags)); }
   UChar_t  GetChannel() const      { return DRS_ENCODE::drs_channelv_extract_channel(fAddress, GetDRSVersion()); }
   UChar_t  GetChip() const         { return DRS_ENCODE::drs_channelv_extract_chip(fAddress, GetDRSVersion()); }
   UChar_t  GetSlot() const         { return DRS_ENCODE::drs_channelv_extract_slot(fAddress, GetDRSVersion()); }
   UChar_t  GetCrate() const        { return DRS_ENCODE::drs_channelv_extract_crate(fAddress, GetDRSVersion()); }

   UShort_t GetVHeaderFormat() const { return DRS_ENCODE::drs_channelv_extract_header_type(fVFlags); }
   UShort_t GetVEncodingMode() const { return DRS_ENCODE::drs_channelv_extract_encoding_mode(fVFlags); }
   UShort_t GetTHeaderFormat() const { return DRS_ENCODE::drs_channelt_extract_header_type(fTFlags); }
   UShort_t GetTEncodingMode() const { return DRS_ENCODE::drs_channelt_extract_encoding_mode(fTFlags); }

   Short_t   GetMethodMode11() const           { return fMethodMode11; }
   Short_t  *GetParMode11() const              { return fParMode11; }
   Double_t *GetTimeCalibMode11() const        { return fTimeCalibMode11; }

   Float_t         GetShiftTime() const                 { return fShiftTime;}
   UShort_t        GetTriggerCell() const               { return fTriggerCell;}
   MEGDRSWaveform *GetWaveform() const                  { return fWaveform; }
   const Int_t    *GetRebinSize() const                 { return fRebinSize; }
   Bool_t          GetWaveformAllocatedByMyself() const { return fWaveformAllocatedByMyself; }
   Short_t         GetCalibrationStatus() const         { return fCalibrationStatus;}
   std::vector<Double_t> *GetTimeCalibrationData() const { return fTimeCalibrationData; }
   // const std::vector<Double_t> &GetTimeCalibration() const {return fTimeCalibration;}

   static Double_t GetReferenceTimeAt(Int_t type)     { return fgReferenceTime[type];}
   static Bool_t   IsReferenceTimeReadyAt(Int_t type) { return fgReferenceChannel[type];}
   static void     ClearReferenceTimeAt(Int_t type)   { fgReferenceChannel[type] = 0;}
   static void     ClearReferenceTime()               { fgReferenceChannel.assign(kNReferenceTimeType, 0);}

   void SetAddress(UShort_t address)     { fAddress  = address; }
   void DeleteData();
   void DeleteTData();
   void DeleteVData();

   void SetVFlags(UShort_t flags)       { fVFlags    = flags; }
   void SetVDsize(Int_t    dsize)       { fVDsize    = dsize; }
   void SetVFirstBin(UShort_t bin)         { fVFirstBin = bin;; }
   void SetVDataAt(Int_t i, char data)   { fVData[i]  = data; }
   void SetVData(char *vdata);

   void SetTFlags(UShort_t flags)       { fTFlags    = flags; }
   void SetTDsize(Int_t    dsize)       { fTDsize    = dsize; }
   void SetTFirstBin(UShort_t bin)         { fTFirstBin = bin;; }
   void SetTDataAt(Int_t i, char data)   { fTData[i]  = data; }
   void SetTData(char *tdata);

   void DeleteWaveform() { if (fWaveformAllocatedByMyself) { SafeDelete(fWaveform); fWaveformAllocatedByMyself = kFALSE; }}

   void SetNotDeleteVData(Bool_t  f)     { fNotDeleteVData = f; }
   void SetNotDeleteTData(Bool_t  f)     { fNotDeleteTData = f; }
   void SetDRSVersion(UShort_t value) { DRS_ENCODE::drs_channelv_insert_drs_version(&fVFlags, value); }
   void SetChannel(UChar_t value, UShort_t version) { DRS_ENCODE::drs_channelv_insert_channel(&fAddress, value, version); }
   void SetChip(UChar_t value, UShort_t version) { DRS_ENCODE::drs_channelv_insert_chip(&fAddress, value, version); }
   void SetSlot(UChar_t value, UShort_t version) { DRS_ENCODE::drs_channelv_insert_slot(&fAddress, value, version); }
   void SetCrate(UChar_t value, UShort_t version) { DRS_ENCODE::drs_channelv_insert_crate(&fAddress, value, version); }

   void SetMethodMode11(Short_t value)         { fMethodMode11 = value; }
   void SetParMode11Size(Int_t n = kInitialParMode11Size) { if (fParMode11) delete [] fParMode11; fParMode11 = new Short_t[n]; }
   void SetParMode11At(Int_t i, Short_t value) { fParMode11[i] = value; }
   void SetTimeCalibMode11Size(Int_t n)   { if (fTimeCalibMode11) delete [] fTimeCalibMode11; fTimeCalibMode11 = new Double_t[n]; }
   void SetTimeCalibMode11At(Int_t i, Double_t value) { fTimeCalibMode11[i] = value; }

   void SetVHeaderFormat(UShort_t value) { DRS_ENCODE::drs_channelv_insert_header_type(&fVFlags, value); }
   void SetVEncodingMode(UShort_t value) { DRS_ENCODE::drs_channelv_insert_encoding_mode(&fVFlags, value); }
   void SetTHeaderFormat(UShort_t value) { DRS_ENCODE::drs_channelt_insert_header_type(&fTFlags, value); }
   void SetTEncodingMode(UShort_t value) { DRS_ENCODE::drs_channelt_insert_encoding_mode(&fTFlags, value); }

   void SetTriggerCell(UShort_t triggerCell) { fTriggerCell = triggerCell; }
   void SetDt(UShort_t dt)                   { fDt = dt; }
   void SetShiftTime(Float_t tshift)         { fShiftTime = tshift; }
   void AddShiftTime(Float_t tshift)         { fShiftTime += tshift; }
   void SetCalibrationStatus(Short_t status) {fCalibrationStatus = status;}

   void AllocateData(Int_t vsize, Int_t tsize);
   void AllocateVData(Int_t vsize);
   void AllocateTData(Int_t tsize);

   void EncodeV(UShort_t nbin, UShort_t firstBin, const Double_t *v);
   void EncodeV(UShort_t encoding, UShort_t DRSVersionBit, UShort_t headerFormat,
                UChar_t crate, UChar_t slot, UChar_t chip, UChar_t channel,
                UShort_t nbin, UShort_t firstBin, const Double_t *v);
   Int_t DecodeV(Double_t *v, Int_t *rebinSize = 0);

   void  EncodeT(UShort_t nbin, UShort_t dt, UShort_t timeFirstBin, UShort_t triggerCell, const Double_t *t);
   void  EncodeT(UShort_t encoding, UShort_t DRSVersionBit, UShort_t headerFormat,
                 UChar_t crate, UChar_t slot, UChar_t chip, UChar_t channel,
                 UShort_t nbin, UShort_t dt, UShort_t timeFirstBin, UShort_t triggerCell, const Double_t *t);
   Int_t DecodeT(Double_t *t);

   static Int_t GetMaxVDataSize(Int_t nbin, Int_t encode_mode);
   static Int_t GetMaxTDataSize(Int_t nbin, Int_t encode_mode);

   void SetData(char *vdata, char* tdata);

   void  DecodeWave();
   Int_t ApplyShiftTime();
   Int_t TimeZeroSync();
   Int_t GetTimeZeroEnum(Int_t dtPico);

   ClassDef(MEGDRSChannelData, 7) // DRS raw data format
};

inline MEGDRSChannelData::~MEGDRSChannelData()
{
   DeleteData();
   if (fParMode11) {
      delete [] fParMode11;
      fParMode11 = 0;
   }
   if (fTimeCalibMode11) {
      delete [] fTimeCalibMode11;
      fTimeCalibMode11 = 0;
   }
   if (fWaveform) {
      if (fWaveformAllocatedByMyself) {
         SafeDelete(fWaveform);
      }
   }
}

inline void MEGDRSChannelData::Set(UShort_t DRSVersionBit,
                                   UChar_t crate, UChar_t slot, UChar_t chip, UChar_t channel,
                                   UShort_t vHeaderFormat, UShort_t vEncodingMode,
                                   UShort_t tHeaderFormat, UShort_t tEncodingMode)
{
   SetDRSVersion(DRSVersionBit);
   UShort_t version = DRS_ENCODE::drs_versionbit_to_version(DRSVersionBit);
   SetCrate(crate, version);
   SetSlot(slot, version);
   SetChip(chip, version);
   SetChannel(channel, version);
   SetVHeaderFormat(vHeaderFormat);
   SetVEncodingMode(vEncodingMode);
   SetTHeaderFormat(tHeaderFormat);
   SetTEncodingMode(tEncodingMode);
}

inline void MEGDRSChannelData::SetVEventData(Int_t vdsize, UShort_t vFirstBin, UShort_t vflags, void *vdata)
{
   SetVDsize(vdsize);
   SetVFirstBin(vFirstBin);
   SetVFlags(vflags);
   SetVData(static_cast<char*>(vdata));
}

inline void MEGDRSChannelData::SetTEventData(Int_t tdsize, UShort_t tFirstBin, UShort_t tflags, void *tdata,
                                             UShort_t triggerCell, UShort_t dt)
{
   SetTDsize(tdsize);
   SetTFirstBin(tFirstBin);
   SetTFlags(tflags);
   SetTriggerCell(triggerCell);
   SetTData(static_cast<char*>(tdata));
   SetDt(dt);
}

inline void MEGDRSChannelData::SetData(char *vdata, char* tdata)
{
   DeleteData();
   fVData = vdata;
   fTData = tdata;
}

inline void MEGDRSChannelData::DeleteData()
{
   if (!fNotDeleteVData) {
      if (fVData) {
         delete [] fVData; fVData = 0;
      }
   }
   if (!fNotDeleteTData) {
      if (fTData) {
         delete [] fTData; fTData = 0;
      }
   }
}

inline void MEGDRSChannelData::DeleteVData()
{
   if (!fNotDeleteVData) {
      if (fVData) {
         delete [] fVData; fVData = 0;
      }
   }
}

inline void MEGDRSChannelData::DeleteTData()
{
   if (!fNotDeleteTData) {
      if (fTData) {
         delete [] fTData; fTData = 0;
      }
   }
}

inline void MEGDRSChannelData::AllocateData(Int_t vsize, Int_t tsize)
{
   AllocateVData(vsize);
   AllocateTData(tsize);
   return;
}

inline void MEGDRSChannelData::AllocateVData(Int_t vsize)
{
   DeleteVData();
   if (vsize > 0) {
      fVData = new char[vsize];
   }
   return;
}

inline void MEGDRSChannelData::AllocateTData(Int_t tsize)
{
   DeleteTData();
   if (tsize > 0) {
      fTData = new char[tsize];
   }
   return;
}

inline void MEGDRSChannelData::EncodeV(UShort_t encoding, UShort_t DRSVersionBit, UShort_t headerFormat,
                                       UChar_t crate, UChar_t slot, UChar_t chip, UChar_t channel,
                                       UShort_t nbin, UShort_t firstBin, const Double_t *v)
{
   SetVFlags(encoding | DRSVersionBit | headerFormat);
   UShort_t address = 0;
   UShort_t version = DRS_ENCODE::drs_versionbit_to_version(DRSVersionBit);
   DRS_ENCODE::drs_channelv_insert_crate(&address,   crate, version);
   DRS_ENCODE::drs_channelv_insert_crate(&address,    slot, version);
   DRS_ENCODE::drs_channelv_insert_crate(&address,    chip, version);
   DRS_ENCODE::drs_channelv_insert_crate(&address, channel, version);
   SetAddress(address);
   EncodeV(nbin, firstBin, v);
}

inline void MEGDRSChannelData::SetVData(char *vdata)
{
   DeleteVData();
   fVData = vdata;
}

inline void MEGDRSChannelData::SetTData(char *tdata)
{
   DeleteTData();
   fTData = tdata;
}

#endif   // MEGDRSChannelData_H
