// $Id$
#ifndef WFP_ENCODE_H
#define WFP_ENCODE_H
#include <math.h>
#include "drs_encode/drs_encode.h"
#include "units/MEGSystemOfUnits.h"

#if !defined(__GNUC__)
#   if defined(_MSC_VER)
#      define __inline__ __inline
#      define __restrict__
#   else
#      define __inline__ inline
#      define __restrict__
#   endif
#endif

// header produced by front-end for DRS waveform

// header type
#define WFPH_FLAG_DATA_HEADER_V0  (0<<0) /* current version is 0 */
#define WFPH_FLAG_DATA_HEADER_V1  (0<<0) /* not used */
#define WFPH_FLAG_DATA_HEADER_V2  (0<<0) /* not used */
#define WFPH_FLAG_DATA_HEADER_V3  (0<<0) /* not used */
#define WFPH_FLAG_DATA_HEADER_V4  (0<<0) /* not used */
#define WFPH_FLAG_DATA_HEADER_V5  (0<<0) /* not used */
#define WFPH_FLAG_DATA_HEADER_V6  (0<<0) /* not used */
#define WFPH_FLAG_DATA_HEADER_V7  (0<<0) /* not used */

// DRS version
#define WFPH_VERSION2             (0<<3) /* DRS2 */
#define WFPH_VERSION4             (1<<3) /* will it be used? */
#define WFPH_VERSION5             (2<<3)
#define WFPH_VERSION6             (3<<3)

// Data kind
#define WFPH_CHARGE_INTEGRATION   (0<<5) /* charge integration */
#define WFPH_BASELINE             (1<<5) /* baseline */
#if 0
#define WFPH_                     (2<<5) /* not used */
#define WFPH_                     (3<<5) /* not used */
#define WFPH_                     (4<<5) /* not used */
#define WFPH_                     (5<<5) /* not used */
#define WFPH_                     (6<<5) /* not used */
#define WFPH_                     (7<<5) /* not used */
#define WFPH_                     (8<<5) /* not used */
#define WFPH_                     (9<<5) /* not used */
#define WFPH_                    (10<<5) /* not used */
#define WFPH_                    (11<<5) /* not used */
#define WFPH_                    (12<<5) /* not used */
#define WFPH_                    (13<<5) /* not used */
#define WFPH_                    (14<<5) /* not used */
#define WFPH_                    (15<<5) /* not used */
#define WFPH_                    (16<<5) /* not used */
#define WFPH_                    (17<<5) /* not used */
#define WFPH_                    (18<<5) /* not used */
#define WFPH_                    (19<<5) /* not used */
#define WFPH_                    (20<<5) /* not used */
#define WFPH_                    (21<<5) /* not used */
#define WFPH_                    (22<<5) /* not used */
#define WFPH_                    (23<<5) /* not used */
#define WFPH_                    (24<<5) /* not used */
#define WFPH_                    (25<<5) /* not used */
#define WFPH_                    (26<<5) /* not used */
#define WFPH_                    (27<<5) /* not used */
#define WFPH_                    (28<<5) /* not used */
#define WFPH_                    (29<<5) /* not used */
#define WFPH_                    (30<<5) /* not used */
#define WFPH_                    (31<<5) /* not used */
#endif

#ifdef __cplusplus
extern "C" {
namespace WFP_ENCODE
{
#endif

const int kWFPMaxUShort = 65534;
const int kWFPMaxShort  = kWFPMaxUShort >> 1;
const int kWFPMinShort  = -kWFPMaxShort - 1;

typedef struct {
   unsigned short address;     //  0-3  : drs channel
   //  4    : drs chip
   //  5-8  : slot,
   //  9-14 : crate
   // 15    : header bit=0
} WFP_CHANNEL_HEADER;

typedef struct {
   unsigned short flags;       //  0- 2 : header type
   //  3- 4 : drs version
   //  5- 9 : data kind
   // 10-14 : parameters
   // 15    : header bit=1
} WFP_DATA_HEADER;

__inline__ unsigned char wfp_is_channel_header(const void *p)
{
   return static_cast<const WFP_CHANNEL_HEADER*>(p)->address & 0x8000 ? 0 : 1;
}
__inline__ unsigned char wfp_channel_extract_channel(unsigned short address, unsigned short /* version */)
{
   return (address & 0x000F) >> 0;
}
__inline__ unsigned char wfp_channel_extract_chip(unsigned short address, unsigned short /* version */)
{
   return (address & 0x0010) >> 4;
}
__inline__ unsigned char wfp_channel_extract_slot(unsigned short address, unsigned short /* version */)
{
   return (address & 0x01E0) >> 5;
}
__inline__ unsigned char wfp_channel_extract_crate(unsigned short address, unsigned short /* version */)
{
   return (address & 0xFE00) >> 9;
}

__inline__ void wfp_set_channel_header(void *p, unsigned char channel_header)
{
   ((WFP_CHANNEL_HEADER*)p)->address =
      (((WFP_CHANNEL_HEADER*)p)->address & (~0x8000)) |
      (((unsigned short)(channel_header ? 0 : 1)) << 15);
}

__inline__ void wfp_channel_insert_channel(unsigned short *address,
                                           unsigned char ch, unsigned short /* version */)
{
   *address = (*address & (~0x000F)) |
              ((((unsigned short) ch) & 0x000F) << 0);
}

__inline__ void wfp_channel_insert_chip(unsigned short *address,
                                        unsigned char chip, unsigned short /* version */)
{
   *address = (*address & (~0x0010)) |
              ((((unsigned short) chip)  & 0x0001) << 4);
}

__inline__ void wfp_channel_insert_slot(unsigned short *address,
                                        unsigned char slot, unsigned short /* version */)
{
   *address = (*address & (~0x01E0)) |
              ((((unsigned short) slot)  & 0x000F) << 5);
}

__inline__ void wfp_channel_insert_crate(unsigned short *address,
                                         unsigned char crate, unsigned short /* version */)
{
   *address = (*address & (~0xFE00)) |
              ((((unsigned short) crate) & 0x007F) << 9);
}

__inline__ unsigned short wfp_data_extract_header_type(unsigned short flags)
{ return flags & 0x0007; }
__inline__ unsigned short wfp_data_extract_drs_version(unsigned short flags)
{ return flags & 0x0018; }
__inline__ unsigned short wfp_data_extract_data_kind(unsigned short flags)
{ return flags & 0x03E0; }
__inline__ unsigned short wfp_data_extract_parameters(unsigned short flags)
{ return flags & 0x7C00; }
__inline__ unsigned short wfp_data_get_header_type(const void *header)
{ return wfp_data_extract_header_type(*static_cast<const unsigned short*>(header)); }

__inline__ void wfp_data_insert_header_type(unsigned short *flags,
                                            unsigned header)
{
   *flags = (*flags & (~0x0007)) | (header     & 0x0007);
}

__inline__ void wfp_data_insert_drs_version(unsigned short *flags,
                                            unsigned short version)
{
   *flags = (*flags & (~0x0018)) | (version    & 0x0018);
}

__inline__ void wfp_data_insert_data_kind(unsigned short *flags,
                                          unsigned short kind)
{
   *flags = (*flags & (~0x03E0)) | (kind       & 0x03E0);
}

__inline__ void wfp_data_insert_parameters(unsigned short *flags,
                                           unsigned short parameters)
{
   *flags = (*flags & (~0x7C00)) | (parameters & 0x7C00);
}

// headers
bool CheckWFPHeaderSize();
void wfp_compose_channel_header(void *pdata, unsigned char crate,
                                unsigned char slot, unsigned char wfp_chip,
                                unsigned char wfp_channel, unsigned short version);
void wfp_compose_data_header(void *pdata, unsigned short header_type,
                             unsigned short drs_version,
                             unsigned short data_kind,
                             unsigned short parameters);

// byte swap
int  byte_swap_wfp_header(void* header);
void byte_swap_wfp_bank(void* p, size_t length);

// read and write
size_t wfp_read_value(const void* p, double *data,
                      unsigned short *address,
                      void *data_header,
                      int &address_old);
size_t wfp_get_raw_data_pointer(void* p,
                                char **data,
                                unsigned short *address,
                                void *data_header,
                                int &address_old);
size_t wfp_write_value(void* p, double data, unsigned short address,
                       const void* data_header, int &address_old);


// For charge integration
#define WFPH_CHARGE_INTEGRATION_MODE00 (0<<10) /* 0.005 pC */
#define WFPH_CHARGE_INTEGRATION_MODE01 (1<<10) /* 0.025 pC */
#define WFPH_CHARGE_INTEGRATION_MODE02 (2<<10) /* 0.2   pC */
#define WFPH_CHARGE_INTEGRATION_MODE03 (3<<10) /* 1,0   pC */
const int    kChargeIntegrationOffset = 4096; // 6.25 % of kMaxUShort
const double WFPH_CHARGE_INTEGRATION_MODE00_MAX_CHARGE = 0.005 * MEG::picocoulomb * (kWFPMaxUShort - kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE00_MIN_CHARGE = 0.005 * MEG::picocoulomb * (-kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE01_MAX_CHARGE = 0.025 * MEG::picocoulomb * (kWFPMaxUShort - kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE01_MIN_CHARGE = 0.025 * MEG::picocoulomb * (-kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE02_MAX_CHARGE = 0.2   * MEG::picocoulomb * (kWFPMaxUShort - kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE02_MIN_CHARGE = 0.2   * MEG::picocoulomb * (-kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE03_MAX_CHARGE = 1.0   * MEG::picocoulomb * (kWFPMaxUShort - kChargeIntegrationOffset);
const double WFPH_CHARGE_INTEGRATION_MODE03_MIN_CHARGE = 1.0   * MEG::picocoulomb * (-kChargeIntegrationOffset);
__inline__ unsigned short wfp_extract_charge_integration_mode(
   unsigned short flags)
{ return flags & 0x1800; }
__inline__ void wfp_insert_charge_integration_mode(unsigned short *flags,
                                                   unsigned short mode)
{
   *flags = (*flags & (~0x1800)) | (mode & 0x1800);
}
size_t wfp_encode_charge_integration(void* p, double data,
                                     unsigned short flags);
size_t wfp_decode_charge_integration(double *data, const void *p,
                                     unsigned short flags);

// For baseline
#define WFPH_BASELINE_MODE00 (0<<10) /* 0.001 mV */
#define WFPH_BASELINE_MODE01 (1<<10) /* 0.01 mV */
const double WFPH_BASELINE_MODE00_MAX_VOLT = 0.001 * MEG::millivolt * kWFPMaxShort;
const double WFPH_BASELINE_MODE00_MIN_VOLT = -1 * WFPH_BASELINE_MODE00_MAX_VOLT;
const double WFPH_BASELINE_MODE01_MAX_VOLT = 0.01 * MEG::millivolt * kWFPMaxShort;
const double WFPH_BASELINE_MODE01_MIN_VOLT = -1 * WFPH_BASELINE_MODE00_MAX_VOLT;
__inline__ unsigned short wfp_extract_baseline_mode(unsigned short flags)
{ return flags & 0x1800; }
__inline__ void wfp_insert_baseline_mode(unsigned short *flags,
                                         unsigned short mode)
{
   *flags = (*flags & (~0x1800)) | (mode & 0x1800);
}
size_t wfp_encode_baseline(void* p, double data, unsigned short flags);
size_t wfp_decode_baseline(double *data, const void *p,
                           unsigned short flags);

#ifdef __cplusplus
};
}                               // extern "C"
#endif
#endif
