/* $Id$ */
#ifndef DCT_H
#define DCT_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
   short mcu_size;
   double *coeff;
} dct_parameters;

/* x : number of bin in data (integer type)
   y : mcu size (integer type)*/
#define DCT_SIZE(x,y) ((unsigned int)(x % y != 0 ? ((x / y) + 1) * y : x))

dct_parameters *dct_init_param(int mcu_size);
void dct_free_param(dct_parameters *param);
int dct_encode(const double *wf, double *dct, dct_parameters *dct_param, int nbin, int stride);
void dct_decode(double *wf, const double *dct, dct_parameters *dct_param, int nbin, int stride);

#ifdef __cplusplus
}
#endif
#endif
