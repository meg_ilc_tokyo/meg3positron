// $Id$
// Author: Yasuhiro Nishimura

#ifndef MEGHistory_H
#define MEGHistory_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Class MEGHistory                                                           //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

//#include <RConfig.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( push )
#   pragma warning( disable : 4800)
#endif // R__VISUAL_CPLUSPLUS
//#include <TNamed.h>
//#include <TAttLine.h>
//#include <TAttFill.h>
//#include <TAttMarker.h>
#include <TGraphErrors.h>
//#include <TH1.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS
//#include "ROMEPrint.h"

//MEGHIXTORYTOOLS
namespace MEGHISTORYTOOLS
{

// variables
//extern Double_t HISTORYTYPE;
// function MEGHISTORYTOOLS::

//inline void MEGHistory::CutoffGraph(TGraph *g, Double_t xmin, Double_t xmax) {
inline void CutoffGraph(TGraph *g, Double_t xmin, Double_t xmax)
{
   const Int_t n = g->GetN();
   Double_t *x = g->GetX();
   for (Int_t i = n - 1; i >= 0; i--) {
      if (x[i] < xmin || x[i] > xmax) {
         g->RemovePoint(i);
         x = g->GetX();
      }
   }
}

//inline void MEGHistory::MovingAverageGraph(TGraphErrors *g, Int_t meann) {
inline void MovingAverageGraph(TGraphErrors *g, Int_t meann)
{
   if (meann < 2) {
      return;
   }
   const Int_t n = g->GetN();
   if (meann > n / 2 - 1) {
      meann = n / 2 - 1;
   }
   if (!(meann % 2)) {
      meann++;
   }
   Int_t halfmeann = (meann - 1) / 2;
   g->Sort();
   Double_t *y  = g->GetY();
   Double_t *ye = g->GetEY();
   Double_t ytmp[n];
   Double_t yetmp[n];
   for (Int_t i = 0; i < n; i++) {
      ytmp[i]  = y[i];
      yetmp[i] = ye[i];
   }
   for (Int_t i = 0; i < n; i++) {
      if (i < halfmeann) {
         y[i]  *= (Double_t)(halfmeann - i + 1) / (Double_t)meann;
         ye[i] *= (Double_t)(halfmeann - i + 1) / (Double_t)meann;
      } else if (i >= n - halfmeann) {
         y[i]  *= (Double_t)(halfmeann - (n - 1 - i) + 1) / (Double_t)meann;
         ye[i] *= (Double_t)(halfmeann - (n - 1 - i) + 1) / (Double_t)meann;
      } else {
         y[i]  /= (Double_t)meann;
         ye[i] /= (Double_t)meann;
         /*for (Int_t j = 1; j <= halfmeann; j++) {
            y[i]  +=(ytmp[i-j] + ytmp[i+j])/(Double_t)meann;
            ye[i] +=(yetmp[i-j] + yetmp[i+j])/(Double_t)meann;
         }*/
      }
      for (Int_t j = 1; j <= halfmeann; j++) {
         y[i]   += (((i - j >= 0) ? ytmp[i - j] : 0) + ((i + j < n) ? ytmp[i + j] : 0)) / (Double_t)meann;
         ye[i]  += (((i - j >= 0) ? yetmp[i - j] : 0) + ((i + j < n) ? yetmp[i + j] : 0)) / (Double_t)meann;
      }
   }
}

inline Bool_t MovingAverageGraphByTime(TGraphErrors *g = 0, Int_t meann = 3)
{
   //inline Bool_t MEGHistory::MovingAverageGraphByTime(TGraphErrors *g = 0, Int_t meann = 3) {
   /* Moving average weighted by time, meann must be odd number
      meaned around x_i from jmin = i - (meann-1)/2 to jmax = i + (meann-1)/2
      weight_ij = 1 - | x_i - x_j | / (x_i_jmax - x_i_jmin)
      therefore weight_ii = 0, and calculate normalized factor
      N_ij = 1 / Sigma_jmin^jmax (weight_ij)
      result in x'_i = Sigma_jmin^jmax (N_ij * weight_ij * x_j)               */
   if (!g) {
      return kFALSE;
   }
   if (meann < 2) {
      return kFALSE;
   }
   const Int_t n = g->GetN();
   if (meann > n / 2 - 1) {
      meann = n / 2 - 1;
   }
   if (!(meann % 2)) {
      meann++;
   }
   Int_t halfmeann = (meann - 1) / 2;
   g->Sort();
   Double_t *x  = g->GetX();
   Double_t *y  = g->GetY();
   Double_t *xe = g->GetEX();
   Double_t *ye = g->GetEY();
   Double_t ytmp[n];
   Double_t xetmp[n];
   Double_t yetmp[n];
   Double_t normalized;
   Double_t deltaX;
   for (Int_t i = 0; i < n; i++) {
      ytmp[i]  = y[i];
      xetmp[i] = xe[i];
      yetmp[i] = ye[i];
   }
   for (Int_t i = 0; i < n; i++) {
      normalized = 1;
      deltaX = x[(i >= n - halfmeann) ? n - 1 : i + halfmeann] - x[(i < halfmeann) ? 0 : i - halfmeann];
      if (!deltaX) {
         return kFALSE;
      }
      for (Int_t j = 1; j <= halfmeann; j++) {
         if (i + j < n) {
            normalized += 1 - (x[i + j] - x[i]) / deltaX;
         }
         if (i - j >= 0) {
            normalized += 1 - (x[i] - x[i - j]) / deltaX;
         }
      }
      if (!normalized) {
         return kFALSE;
      } else {
         normalized = 1 / normalized;
      }
      y[i]  *= normalized;
      ye[i] *= normalized;
      for (Int_t j = 1; j <= halfmeann; j++) {
         y[i]   += (((i - j >= 0) ? ytmp[i - j] * (1 - (x[i] - x[i - j]) / deltaX) : 0)
                    + ((i + j < n) ? ytmp[i + j] * (1 - (x[i + j] - x[i]) / deltaX) : 0)) * normalized;
         ye[i]  += (((i - j >= 0) ? yetmp[i - j] * (1 - (x[i] - x[i - j]) / deltaX) : 0)
                    + ((i + j < n) ? yetmp[i + j] * (1 - (x[i + j] - x[i]) / deltaX) : 0)) * normalized;
      }
   }
   return kTRUE;
}

inline Bool_t InvertGraph(TGraph *g = 0)
{
   //inline Bool_t MEGHistory::InvertGraph(TGraph *g = 0) {
   if (!g) { return kFALSE; }
   Int_t n  = g->GetN();
   Double_t *y    = g->GetY();
   Double_t *yerr = g->GetEY();
   Double_t ymin  = g->GetHistogram()->GetMinimum();
   Double_t ymax  = g->GetHistogram()->GetMaximum();
   for (Int_t i = 0; i < n; i++) {
      if (y[i]) {
         y[i] = 1 / y[i];
         yerr[i] = yerr[i] * y[i] * y[i];
      } else {
         g->RemovePoint(i);
         y    = g->GetY();
         yerr = g->GetEY();
         n--;
      }
   }
   if (ymin) {
      g->SetMaximum(1 / ymin);
   }
   if (ymax) {
      g->SetMinimum(1 / ymax);
   }
   return kTRUE;
}

inline Bool_t NormalizeLGraphAtTime(TGraph *g = 0, Double_t basetime = -1, TSpline* spline = 0, Option_t* option = "")
{
   //inline Bool_t MEGHistory::NormalizeLGraphAtTime(TGraph *g = 0, Double_t basetime = -1, TSpline* spline = 0, Option_t* option = "") {
   if (!g) {
      return kFALSE;
   }
   g->Sort();
   const Int_t n  = g->GetN();
   Double_t *x    = g->GetX();
   Double_t *y    = g->GetY();
   Double_t *yerr = g->GetEY();
   Double_t ymin  = g->GetHistogram()->GetMinimum();
   Double_t ymax  = g->GetHistogram()->GetMaximum();
   Double_t y0;
   if (basetime == -1 || basetime > x[n - 1]) {
      y0 = y[n - 1];
   } else if (basetime == -2 || basetime < x[0]) {
      y0 = y[0];
   } else {
      y0 = g->Eval(basetime, spline, option);
   }
   if (!y0) {
      return kFALSE;
   }
   for (Int_t i = 0; i < n; i++) {
      y[i] = y[i] / y0;
      yerr[i] = yerr[i] / y0;
   }
   g->SetMaximum(ymax / y0);
   g->SetMinimum(ymin / y0);
   return kTRUE;
}

inline Bool_t NormalizeMeanGraphAtTime(TGraph *g = 0, Int_t starttime = -1, Int_t endtime = -1)
{
   //inline Bool_t MEGHistory::NormalizeMeanGraphAtTime(TGraph *g = 0, Int_t starttime = -1, Int_t endtime = -1) {
   if (!g) {
      return kFALSE;
   }
   g->Sort();
   Int_t count = 0;
   const Int_t n  = g->GetN();
   Double_t *x    = g->GetX();
   Double_t *y    = g->GetY();
   Double_t *xerr = g->GetEX();
   Double_t *yerr = g->GetEY();
   Double_t ymin  = g->GetHistogram()->GetMinimum();
   Double_t ymax  = g->GetHistogram()->GetMaximum();
   Double_t ymean = 0;
   if (starttime == -1) {
      starttime = (Int_t)(x[0] - xerr[0]);
   }
   if (endtime == -1) {
      endtime   = (Int_t)(x[n - 1] + xerr[n - 1]);
   }
   if (starttime > endtime || starttime > Int_t(x[n - 1] + xerr[n - 1]) || endtime < Int_t(x[0] - xerr[0])) {
      return kFALSE;
   }
   for (Int_t i = 0; i < n; i++) {
      if (x[i] >= Double_t(starttime) && x[i] <= (Double_t)(endtime)) {
         ymean += y[i];
         count++;
      }
   }
   if (count != 0 && ymean != 0) {
      ymean /= count;
   } else {
      return kFALSE;
   }
   for (Int_t i = 0; i < n; i++) {
      y[i] = y[i] / ymean;
      yerr[i] = yerr[i] / ymean;
   }
   g->SetMaximum(ymax / ymean);
   g->SetMinimum(ymin / ymean);
   return kTRUE;
}

inline Int_t RemoveDuplicate(TGraph *g = 0, Double_t thresholddx = 0)
{
   //inline Int_t MEGHistory::RemoveDuplicate(TGraph *g = 0, Double_t thresholddx = 0) {
   if (!g) {
      return 0;
   }
   g->Sort();
   const Int_t n  = g->GetN();
   if (n < 2) {
      return 0;
   }
   Int_t count = n;
   Double_t *x    = g->GetX();
   Double_t *y    = g->GetY();
   Double_t *xerr = g->GetEX();
   Double_t *yerr = g->GetEY();
   if (!thresholddx) {
      thresholddx = (x[n - 1] - x[0]) / (Double_t)(n - 1) * 1.5;
   }
   for (Int_t i = 1; i < count; i++) {
      if (x[i] - x[i - 1] < thresholddx) {
         x[i - 1] = (x[i] + x[i - 1]) * 0.5;
         y[i - 1] = (y[i] + y[i - 1]) * 0.5;
         xerr[i - 1] = TMath::Sqrt(xerr[i] * xerr[i] + xerr[i - 1] * xerr[i - 1]);
         yerr[i - 1] = TMath::Sqrt(yerr[i] * yerr[i] + yerr[i - 1] * yerr[i - 1]);
         g->RemovePoint(i);
         x    = g->GetX();
         y    = g->GetY();
         xerr = g->GetEX();
         yerr = g->GetEY();
         count--;
         i--;
      }
   }
   return n - count;
}

inline void CombineGraph(TGraph *g = 0, TGraph *g1 = 0, TGraph *g2 = 0)
{
   //inline void MEGHistory::CombineGraph(TGraph *g = 0, TGraph *g1 = 0, TGraph *g2 = 0) {
   if (!g) {
      g = new TGraph();
   }
   Int_t n1 = 0;
   Int_t n2 = 0;
   if (g1) {
      n1 = g1->GetN();
   }
   if (g2) {
      n2 = g2->GetN();
   }
   Double_t *xtmp = g1->GetX();
   Double_t *ytmp = g1->GetY();
   Double_t *xtmp2 = g2->GetX();
   Double_t *ytmp2 = g2->GetY();
   for (Int_t i = 0; i < n1; i++) {
      g->SetPoint(i, xtmp[i], ytmp[i]);
   }
   for (Int_t i = 0; i < n2; i++) {
      g->SetPoint(i + n1, xtmp2[i], ytmp2[i]);
   }
   g->Sort();
}

inline void CombineGraph(TGraphErrors *g = 0, TGraphErrors *g1 = 0, TGraphErrors *g2 = 0)
{
   //inline void MEGHistory::CombineGraph(TGraphErrors *g = 0, TGraphErrors *g1 = 0, TGraphErrors *g2 = 0) {
   if (!g) { g = new TGraphErrors(); }
   Int_t n1 = 0;
   Int_t n2 = 0;
   if (g1) { n1 = g1->GetN(); }
   if (g2) { n2 = g2->GetN(); }
   Double_t scale1 = 1;
   Double_t scale2 = 1;
   Double_t *xtmp = g1->GetX();
   Double_t *ytmp = g1->GetY();
   Double_t *xetmp = g1->GetEX();
   Double_t *yetmp = g1->GetEY();
   Double_t *xtmp2 = g2->GetX();
   Double_t *ytmp2 = g2->GetY();
   Double_t *xetmp2 = g2->GetEX();
   Double_t *yetmp2 = g2->GetEY();
   for (Int_t i = 0; i < n1; i++) {
      g->SetPoint(i, xtmp[i], ytmp[i]*scale1);
      g->SetPointError(i, xetmp[i], yetmp[i]*scale1);
   }
   for (Int_t i = 0; i < n2; i++) {
      g->SetPoint(i + n1, xtmp2[i], ytmp2[i]*scale2);
      g->SetPointError(i + n1, xetmp2[i], yetmp2[i]*scale2);
   }
   g->Sort();
}

inline void CombineGraph(const Int_t n = 0, TGraphErrors **g = 0, Double_t *scale = 0, Int_t *sort = 0, TGraphErrors *gout = 0)
{
   //inline void MEGHistory::CombineGraph(const Int_t n = 0, TGraphErrors **g = 0, Double_t *scale = 0, Int_t *sort = 0, TGraphErrors *gout = 0) {
   /* combine the number of n graphs with factor scale[] and create gout */
   if (!gout) {
      gout = new TGraphErrors();
   }
   Int_t ng;
   Int_t count = 0;
   Double_t *xtmp;
   Double_t *ytmp;
   Double_t *xetmp;
   Double_t *yetmp;

   for (Int_t i = 0; i < n; i++) {
      if (!g[i]) {
         continue;
      }
      ng    = g[sort[i]]->GetN();
      xtmp  = g[sort[i]]->GetX();
      ytmp  = g[sort[i]]->GetY();
      xetmp = g[sort[i]]->GetEX();
      yetmp = g[sort[i]]->GetEY();
      for (Int_t j = 0; j < ng; j++) {
         gout->SetPoint(count, xtmp[j], ytmp[j] * scale[sort[i]]);
         gout->SetPointError(count, xetmp[j], yetmp[j] * scale[sort[i]]);
         count++;
      }
   }
   gout->Sort();
}

inline Double_t MeanValuebyTime(TGraphErrors *g, Double_t starttime, Double_t endtime)
{
   //inline Double_t MEGHistory::MeanValuebyTime(TGraphErrors *g, Double_t starttime, Double_t endtime) {
   Double_t mean = 0;
   Double_t meannormalized = 0;
   Double_t timedif;
   g->Sort();
   const Int_t n = g->GetN();
   Double_t *xtmp = g->GetX();
   Double_t *ytmp = g->GetY();
   Double_t *yetmp = g->GetEY();
   for (Int_t i = 0; i < n; i++) {
      if (xtmp[i] >= starttime && xtmp[i] <= endtime) {
         if (!i) {
            timedif = xtmp[i + 1] - xtmp[i];
         } else if (i == n - 1) {
            timedif = xtmp[i] - xtmp[i - 1];
         } else {
            timedif = (xtmp[i + 1] - xtmp[i - 1]) * 0.5;
         }
         if (yetmp[i]) {
            mean += ytmp[i] / yetmp[i] * timedif;
            meannormalized += 1 / yetmp[i] * timedif;
         } else {
            mean += ytmp[i] * timedif;
            meannormalized += 1 * timedif;
         }
      }
   }
   if (meannormalized) {
      mean = mean / meannormalized;
   }

   return mean;
}

inline void CombineGraphSmooth(const Int_t n, TGraphErrors **g, Double_t **limittime, TGraphErrors *gout)
{
   //inline void MEGHistory::CombineGraphSmooth(const Int_t n, TGraphErrors **g, Double_t **limittime, TGraphErrors *gout) {
   /* use n graphs from limittime[][0] to limittime[][1] to combine as gout smoothly */

   Double_t duplicatetime[n];//interval time between 2 graphs
   Int_t sort[2][n];//id ordered by start time and end time
   Double_t scale[n];//scale factor of each graph
   Double_t value[n][2];//value of 1st and end points
   Int_t    avepoints = 10;//average of npoints about value[][]
   Int_t    npoints;//
   Double_t *x[n];
   Double_t *y[n];
   for (Int_t i = 0; i < n; i++) {
      g[i]->Sort();
      value[i][0] = 0;
      value[i][1] = 0;
      x[i] = g[i]->GetX();
      y[i] = g[i]->GetY();
      npoints = g[i]->GetN();
      //check if limittime is in the range of graph
      if (x[i][0] > limittime[i][0]) {
         limittime[i][0] = x[i][0];
      }
      if (x[i][npoints - 1] < limittime[i][1]) {
         limittime[i][1] = x[i][npoints - 1];
      }
      //MEGHISTORYTOOLS::CutoffGraph(g[i], limittime[i][0], limittime[i][1]);
      for (Int_t j = 0; j < npoints; j++) {
         if (x[i][npoints - j - 1] < limittime[i][0] || x[i][npoints - j - 1] > limittime[i][1]) {
            g[i]->RemovePoint(npoints - j - 1);
         }
         npoints = g[i]->GetN();
         x[i] = g[i]->GetX();
      }
      //Get value around start time and end time averaged by "avepoints" times
      y[i] = g[i]->GetY();
      for (Int_t j = 0; j < /*avepoints*/ ((avepoints > npoints) ? npoints : avepoints); j++) {
         value[i][0] += y[i][j] / TMath::Min((Double_t)avepoints, (Double_t)npoints);
         value[i][1] += y[i][npoints - j - 1] / TMath::Min((Double_t)avepoints, (Double_t)npoints);
      }
   }
   //sort by limittime
   Int_t MinId[2];
   for (Int_t i = 0; i < n; i++) {
      MinId[0] = i;
      MinId[1] = i;
      for (Int_t j = i + 1; j < n; j++) {
         if (limittime[i][0] > limittime[j][0]) {
            MinId[0] = j;
         }
         if (limittime[i][0] == limittime[j][0] && limittime[i][1] > limittime[j][1]) {
            MinId[0] = j;
         }
         if (limittime[i][0] == limittime[j][0] && limittime[i][1] < limittime[j][1]) {
            MinId[0] = i;
         }
         if (limittime[i][1] >  limittime[j][1]) {
            MinId[1] = j;
         }
         if (limittime[i][1] == limittime[j][1] && limittime[i][0] > limittime[j][0]) {
            MinId[1] = j;
         }
         if (limittime[i][1] == limittime[j][1] && limittime[i][0] < limittime[j][0]) {
            MinId[1] = i;
         }
      }
      sort[0][i] = MinId[0];
      sort[1][i] = MinId[1];
   }
   scale[sort[0][0]] = 1;
   for (Int_t i = 0; i < n - 1; i++) {
      //change scale of g[sort[0][i+1]]
      duplicatetime[sort[0][i]] = limittime[sort[0][i]][1] - limittime[sort[0][i + 1]][0];
      if (duplicatetime[sort[0][i]] < 0) {//not duplicate
         if (value[sort[0][i + 1]][0] && scale[sort[0][0]]) {
            scale[sort[0][i + 1]] = value[sort[0][i]][1] / value[sort[0][i + 1]][0] / scale[sort[0][0]];
         }
      } else { // duplicate
         scale[sort[0][i + 1]] = MeanValuebyTime(g[sort[0][i]], limittime[sort[0][i + 1]][0], limittime[sort[0][i]][1])
                                 / MeanValuebyTime(g[sort[0][i + 1]], limittime[sort[0][i + 1]][0], limittime[sort[0][i]][1])
                                 / scale[sort[0][0]];
      }
      for (Int_t j = 0; j < i; j++) {
         scale[sort[0][i + 1]] *= scale[sort[0][j + 1]];
      }
   }
   //#ifdef  XECHistory_Debug
   //   for (Int_t i = 0; i < n; i++) {
   //      cout << "Scale " << sort[0][i] << "\t" << scale[i] << endl;
   //   }
   //#endif
   CombineGraph(n, g, scale, sort[0], gout);
}

inline void CombineGraphSmooth(const Int_t n, TGraphErrors **g, Double_t *limittimestart, Double_t *limittimeend, TGraphErrors *gout)
{
   //inline void MEGHistory::CombineGraphSmooth(const Int_t n, TGraphErrors **g, Double_t *limittimestart, Double_t *limittimeend, TGraphErrors *gout) {
   Double_t **limittime;
   limittime = new Double_t*[n];
   for (Int_t i = 0; i < n; i++) {
      limittime[i]    = new Double_t[2];
      limittime[i][0] = limittimestart[i];
      limittime[i][1] = limittimeend[i];
   }
   CombineGraphSmooth(n, g, limittime, gout);
   for (Int_t i = 0; i < n; i++) {
      delete [] limittime[i];
   }
   delete [] limittime;
}

inline void CombineGraphSmooth(const Int_t n, TGraphErrors **g, Int_t *limittimestart, Int_t *limittimeend, TGraphErrors *gout)
{
   //inline void MEGHistory::CombineGraphSmooth(const Int_t n, TGraphErrors **g, Int_t *limittimestart, Int_t *limittimeend, TGraphErrors *gout) {
   Double_t **limittime;
   limittime = new Double_t*[n];
   for (Int_t i = 0; i < n; i++) {
      limittime[i]    = new Double_t[2];
      limittime[i][0] = (Double_t)limittimestart[i];
      limittime[i][1] = (Double_t)limittimeend[i];
   }
   CombineGraphSmooth(n, g, limittime, gout);
   for (Int_t i = 0; i < n; i++) { delete [] limittime[i]; }
   delete [] limittime;
}

inline Double_t GetNearestD(TGraphErrors *g, Double_t x, Double_t y)
{
   /* return nearest distance from x, y point */
   const Int_t npoints = g->GetN();
   if (npoints == 0) {
      return 0;
   }
   Double_t * graphx = g->GetX();
   Double_t * graphy = g->GetY();
   Double_t nearestdistance2 = (graphx[0] - x) * (graphx[0] - x) + (graphy[0] - y) * (graphy[0] - y);
   Double_t distance2 = 0;
   for (Int_t i = 1; i < npoints; i++) {
      distance2 = (graphx[i] - x) * (graphx[i] - x) + (graphy[i] - y) * (graphy[i] - y);
      if (distance2 < nearestdistance2) {
         nearestdistance2 = distance2;
      }
   }
   return TMath::Sqrt(nearestdistance2);
}

inline Int_t GetNearestN(TGraphErrors *g, Double_t x, Double_t y)
{
   /* return nearest ith of graph from x, y point */
   /* return the largest ith if graph has some points of the distance from x, y */
   const Int_t npoints = g->GetN();
   if (npoints == 0) {
      return 0;
   }
   Double_t * graphx = g->GetX();
   Double_t * graphy = g->GetY();
   Int_t    nearestith = 0;
   Double_t nearestdistance2 = (graphx[0] - x) * (graphx[0] - x) + (graphy[0] - y) * (graphy[0] - y);
   Double_t distance2 = 0;
   for (Int_t i = 1; i < npoints; i++) {
      distance2 = (graphx[i] - x) * (graphx[i] - x) + (graphy[i] - y) * (graphy[i] - y);
      if (distance2 < nearestdistance2) {
         nearestdistance2 = distance2;
         nearestith = i;
      }
   }
   return nearestith;
}

inline Int_t GetNearestxN(TGraphErrors *g, Double_t x)
{
   g->Sort();
   const Int_t npoints = g->GetN();
   if (npoints == 0) {
      return 0;
   }
   Double_t * graphx = g->GetX();
   if (graphx[0] > x) {
      return 0;
   }
   for (Int_t i = 1; i < npoints; i++) {
      if (graphx[i] > x) {
         if ((graphx[i] + graphx[i - 1]) * 0.5 > x) {
            return i - 1;
         } else {
            return i;
         }
      }
   }
   return npoints - 1;
}

inline Int_t GetNearestyN(TGraphErrors *g, Double_t y)
{
   //g->Sort();
   const Int_t npoints = g->GetN();
   if (npoints == 0) {
      return 0;
   }
   Double_t * graphy = g->GetY();
   Int_t    nearestith = 0;
   Double_t nearestdistancey = TMath::Abs(graphy[0] - y);
   Double_t distancey = 0;
   for (Int_t i = 1; i < npoints; i++) {
      distancey = TMath::Abs(graphy[i] - y);
      if (distancey < nearestdistancey) {
         nearestdistancey = distancey;
         nearestith = i;
      }
   }
   return nearestith;
}

}
//END OF MEGHIXTORYTOOLS

class TGraph;
class TF1;
class TH1;

class MEGHistoryNode : public TGraphErrors
{

protected:
   Int_t           fType;                     //History Type of fGraph
   Double_t        fScale;                    //The fGraph is obtained at the scale of fScaleAt, for example MeV, V, gain, mA, etc.
   Double_t        fWeight;                   //The fGraph is weighted at the scale by fWeightAt
   Double_t        fStartTime;                //Start time to use for correction
   Double_t        fEndTime;                  //End time to use for correction
   //TGraphErrors   *fGraph;                    //!Pointer to graph used for fitting

public:
   MEGHistoryNode();
   MEGHistoryNode(const char* name = "", const char* title = "");
   MEGHistoryNode(const MEGHistoryNode &history);
   MEGHistoryNode(const TGraphErrors* graph = 0, const char* name = "", const char* title = "", Int_t type = -1, Double_t scale = -1, Double_t weight = 0, Double_t starttime = 0, Double_t endtime = -1);
   virtual ~MEGHistoryNode();

   /*virtual void          Import(const TGraphErrors *graph, Int_t Type = -1);
   virtual void          ImportAt(Int_t nhistory = 0, const TGraphErrors *graph = 0, Int_t Type = -1);
   virtual void          ImportHistory(const TGraphErrors *graph, Int_t Type = -1);
   virtual void          ReplaceAt(Int_t nhistory = 0, const TGraphErrors *graph = 0, Int_t Type = -1);
   virtual void          ReplaceHistory(const TGraphErrors *graph, Int_t Type = -1);
   virtual TGraphErrors *GetGraph();
   virtual TGraphErrors *ExportAt(Int_t nhistory = 0, Int_t Type = 0);
   virtual TGraphErrors *ExportCombined(Int_t Type = 0);
   virtual TGraphErrors *ExportHistory(Int_t Type = 0);
   virtual void          Draw(Option_t *chopt = "");
   virtual void          DrawAt(Option_t *chopt = "", Int_t nhistory = 0);
   virtual void          DrawAll(Option_t *chopt = "");
   virtual void          DrawHistory(Option_t *chopt = "");
   virtual void          Add(const TGraphErrors *graph, Int_t Type = -1);
   virtual void          Add(const MEGHistory *history);
   //virtual Int_t         Add(TGraphErrors *graph, Int_t Type = -1);
   virtual void          Reset();
   virtual void          ResetHistory();
   virtual Char_t        GetTypeString(Int_t Type = -1);
   virtual Char_t        GetTypeStringAt(Int_t NHistories = -1) const { return GetTypeString(fTypeAt[NHistories]); }
   virtual void          SetNPoints(Int_t npoints);
   *///virtual void          SetTitle(const char *title);
   //virtual void          SetTimeMin(Double_t timemin);

   //virtual void          SetTime(const MEGWaveform *wfin);
   //virtual void          SetTime(Double_t* ptime);
   //virtual void          SetTime(Double_t tmin,Double_t tmax);
   //virtual void          SetTimeAt(Int_t pnt,Double_t t)         { fTime[pnt] = t; }

   virtual void          SetType(Int_t type = -1)                   { fType = type;}
   virtual void          SetScale(Double_t scale = 1)               { fScale = scale;}
   virtual void          SetWeight(Double_t weight = 1)             { fWeight = weight;}
   virtual void          SetStartTime(Double_t time)           { fStartTime = time;}
   virtual void          SetEndTime(Double_t time)             { fEndTime = time;}
   virtual Int_t         GetType()                   const { return fType; }
   //virtual Int_t         GetNPoints()              const { return fGraph->GetN(); }
   virtual Double_t      GetScale()          const { return fScale; }
   virtual Double_t      GetWeight()         const { return fWeight; }
   virtual Double_t      GetStartTime()      const { return fStartTime; }
   virtual Double_t      GetEndTime()        const { return fEndTime; }
   //virtual TGraphErrors *GetGraph()          const { return fGraph; }

};

class MEGHistory : public TNamed
{

protected:
   Int_t           fType;                       //History Type
   Int_t           fInterpolateType;            //History Type
   Int_t           fNPoints;                    //!Maximum number of data points. Array size of fAmplitude.
   Int_t           fMVPoints;                   //!Averaging points
   Int_t           fMVTime;                     //!Averaging if in the range of fMVTime
   Int_t           fTime;                       //!Normalized at fTime
   Int_t           fMaxTime;                    //!
   Int_t           fMinTime;                    //!
   Double_t        fStartTime;                  //Start time to use for correction
   Double_t        fEndTime;                    //End time to use for correction
   Double_t       *fMaxIntervalTime;            //Maximum interval time between points
   Double_t       *fMinIntervalTime;            //Minimum interval time between points

   //each History
   Int_t           fNHistories;                 //!the number of fGraph.
   Double_t       *fIntervalTime;               //Interval time between each point[fNHistories+1]
   Double_t       *fNormalizedFactorAt;         //Normalized factor for each fGraph[fNHistories]
   std::vector<MEGHistoryNode*>  fHistory;      //!Pointer to graph used for fitting[fNHistories]

   /*Int_t          *fTypeAt;                     //History Type of fGraph[fNHistories]
   Double_t       *fScaleAt;                    //The fGraph is obtained at the scale of fScaleAt[fNHistories], for example MeV, V, gain, mA, etc.
   Double_t       *fWeightAt;                   //The fGraph is weighted at the scale by fWeightAt[fNHistories]
   Double_t       *fStartTimeAt;                //Start time to use for correction[fNHistories]
   Double_t       *fEndTimeAt;                  //End time to use for correction[fNHistories]
   //TGraphErrors  **fGraph;                      //!Pointer to graph used for fitting[fNHistories]
   std::vector<TGraphErrors*>  fGraph;          //!Pointer to graph used for fitting[fNHistories]*/

   //Combined History
   TGraphErrors   *fGraphCombined;              //!Pointer to combined graph of all fGraph after normalized
   TGraphErrors   *fGraphHistory;               //!Pointer to graph to use correction after smoothing
   TSpline        *fSplineHistory;              //!Pointer to spline for correction
   TF1            *fMultipliedFunction;         //!
   TF1            *fFunction;                   //!
   Int_t           fNA;                         //!The number of fA
   Double_t       *fA;                          //!changed scale by polinominal function for the different scale of fScaleAt[] to be factored

public:

   MEGHistory();
   MEGHistory(const char* name = "", const char* title = "");
   MEGHistory(const MEGHistory &history);
   MEGHistory(const TGraphErrors* graph, const char* name = "", const char* title = "", Int_t type = -1, Int_t interpolatetype = 0, Int_t mvpoints = 0, Double_t normalizedtime = -1, Double_t starttime = 0, Double_t endtime = -1);
   virtual ~MEGHistory();

   virtual void          Import(const TGraphErrors *graph, Int_t Type = -1);
   virtual void          ImportAt(Int_t nhistory = 0, const TGraphErrors* graph = 0, Int_t Type = -1);
   virtual void          ImportHistory(const TGraphErrors *graph, Int_t Type = -1);
   virtual void          ReplaceAt(Int_t nhistory = 0, const TGraphErrors* graph = 0, Int_t Type = -1);
   virtual void          ReplaceHistory(const TGraphErrors* graph, Int_t Type = -1);
   virtual TGraphErrors *Export(Int_t Type = 0);
   virtual TGraphErrors *ExportAt(Int_t nhistory = 0, Int_t Type = 0);
   virtual TGraphErrors *ExportCombined(Int_t Type = 0);
   virtual TGraphErrors *ExportHistory(Int_t Type = 0);
   virtual void          Draw(Option_t *chopt = "");
   virtual void          DrawAt(Option_t *chopt = "", Int_t nhistory = 0);
   virtual void          DrawAll(Option_t *chopt = "");
   virtual void          DrawHistory(Option_t *chopt = "");
   virtual void          Add(const TGraphErrors *graph, Int_t Type = -1);
   virtual void          Add(const MEGHistory *history);
   //virtual Int_t         Add(TGraphErrors *graph, Int_t Type = -1);
   virtual void          Remove(Int_t nhistory = -1);
   virtual void          AddAt(Int_t nhistory = 0, const TGraphErrors *graph = 0, Int_t Type = -1);
   virtual void          Reset();
   virtual void          ResetHistory();
   virtual Char_t        GetTypeString(Int_t Type = -1);
   //virtual Char_t        GetTypeStringAt(Int_t NHistories = -1) const { return GetTypeString(fTypeAt[NHistories]); }
   virtual void          SetNPoints(Int_t npoints);
   virtual void          SetStartTime(Double_t time)           { fStartTime = time;}
   virtual void          SetEndTime(Double_t time)             { fEndTime = time;}
   virtual void          SetTitle(const char *title);
   virtual void          SetTimeMin(Double_t timemin);
   //virtual void Double_t GetTimeAt(Int_t pnt);

   //virtual void          SetTime(const MEGWaveform *wfin);
   //virtual void          SetTime(Double_t* ptime);
   //virtual void          SetTime(Double_t tmin,Double_t tmax);
   //virtual void          SetTimeAt(Int_t pnt,Double_t t)         { fTime[pnt] = t; }

   virtual Int_t         GetType()                   const { return fType; }
   virtual Int_t         GetNPoints()              const { return fNPoints; }
   virtual Double_t      GetStartTime()      const { return fStartTime; }
   virtual Double_t      GetEndTime()        const { return fEndTime; }
   //virtual Double_t      GetTimeMin()              const { return GetTimeAt(0); }
   //virtual Double_t      GetTimeMax()              const { return GetTimeAt(fNPoints-1); }

   virtual void          InvertHistory();
   virtual void          InvertAt(Int_t nhistory = 0);
   virtual void          InvertAll();

   virtual Double_t      Integral();
   virtual Double_t      GetRMS();
   //virtual void        MinimumPeak(Double_t &peak,Int_t &peakpoint,Double_t tstart=0,Double_t tend=0) const;
   //virtual void        MaximumPeak(Double_t &peak,Int_t &peakpoint,Double_t tstart=0,Double_t tend=0) const;
   //virtual void        TimeShift(Int_t dpht,MEGWaveform* wfout=0);

   MEGHistory&       operator+=(const MEGHistory &history);
   MEGHistory&       operator-=(const MEGHistory &history);
   MEGHistory&       operator+=(Double_t c1);
   MEGHistory&       operator-=(Double_t c1);
   MEGHistory&       operator*=(Double_t c1);
   MEGHistory&       operator/=(Double_t c1);
   MEGHistory&       operator=(const MEGHistory &history);
   //MEGHistory        operator+(const MEGHistory &history);
   //MEGHistory        operator-(const MEGHistory &history);
   //friend MEGHistory operator*(Double_t c1,const MEGHistory &history);
   //friend MEGHistory operator*(const MEGHistory &history, Double_t c1);
   //friend MEGHistory operator/(Double_t c1,const MEGHistory &history);
   //friend MEGHistory operator/(const MEGHistory &history, Double_t c1);

   //ClassDef(MEGHistory,1); //History class for MEG experiment
};
//MEGHistory operator*(Double_t c1,const MEGHistory &history);
//MEGHistory operator/(Double_t c1,const MEGHistory &history);
//inline MEGHistory operator*(const MEGHistory &history, Double_t c1) { return operator*(c1, history); }
//inline MEGHistory operator/(const MEGHistory &history, Double_t c1) { return operator/(c1, history); }

////////////////////////////////////////////////////////////////

#endif   // MEGHistory_H
