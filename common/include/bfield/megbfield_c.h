// $Id$
// Author: Matthias Schneebeli

#ifndef MEGBField_C_H
#define MEGBField_C_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// C functions for class MEGBField                                            //
// Used by megmc                                                              //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C" {
#endif

enum MagnetTypes {
   kAll,
   kCobra,
   kBTS,
   kBypass
};

enum FieldTypes {
   kBDefault = -1,
   kMeasured,
   kCalculated,
   kReconstructed1, // reconstructed b-field with offset correction
   kReconstructed2, // reconstructed b-field without offset correction
   kReconstructed3, // reconstructed b-field with offset correction, 0.5 mm more shifted for z-axis
   kReconstructed4, // reconstructed b-field with offset correction, 1.0 mm more shifted for z-axis
   kCalcScaled,     // calculated b-field already normalized by 0.9975
   kReconstructed5, // reconstructed b-field with offset correction, 0.75 mm more shifted for z-axis
   kReconstructed6, // reconstructed b-field with offset correction, 1.5 mm more shifted for z-axis
   kCalcScaled2     // calculated b-field normalized by 0.9975 * 0.998, *** JUST FOR TEST ***
};

int  initbfield_(const int *magnet, const int *fieldType,
                 const double *cobraScale, const double *btsScale, const double *bypassScale);
void freebfield_(void);

double getbz_(const double *z, const double *r, const double *phi);
double getbr_(const double *z, const double *r, const double *phi);
double getbphi_(const double *z, const double *r, const double *phi);

double getb_(const double *z, const double *r, const double *phi,
             const int *im, const int *it, const int *id);

void setfieldtype_(const int *fieldType);
int  getfieldtype_(void);
void setmagnettype_(const int *magnetType);
int  getmagnettype_(void);

//unsigned int FRead(void *buf, unsigned int size, unsigned int n, FILE *fp);

#ifdef __cplusplus
}
#endif

#endif   // MEGBField_C_H
