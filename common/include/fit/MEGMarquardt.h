// $Id$
#ifndef MEGMarquardt_h
#define MEGMarquardt_h

const int kMaxPar    = 10;// maxmaum number of parameters

class MEGMarquardt
{
public:
   MEGMarquardt() {};
   MEGMarquardt(double x[], double y[], double weight[],
                int npoints, int npar, double yfit[]);
   ~MEGMarquardt();
   void      InitFit(double x[], double y[], double weight[],
                     int npoints, int npar, double yfit[]);
   void      Delete();
   void      SetFitFunction(double (*pfunc)(double x, double y, int nparam, double par[]));
   void      SetParameter(double par[]);
   void      SetParameter(int parnum, double parval);
   int       GetCalls();
   double  GetChisquare();
   void      GetParameter(double par[]);
   void      GetParameter(double par[], double parerror[]);
   void      GetParameter(int parnum, double& paradd, double& parerradd);
   double  GetParameter(int parnum);
   double  GetParError(int parnum);
   void      Fderiv(double x, double y, int nparam, double param[],
                    double (*pfunc)(double x, double y, int nparam, double par[]),
                    int nTerms, double deriv[]);
   double  CalcChisqr(double y[], double yfit[], double weight[],
                      int nPoints, int nFree);
   int       MatInv(double array[kMaxPar][kMaxPar], const int nOrder);
   int       MarquardtFit(double x[], double y[], double weight[],
                          int nPoints, int nTerms,
                          double a[], double sigmaa[], double& flamda,
                          double yfit[], double& chisqr);
   double    Fit(int maxcalls = 100, double tolerance = 1);

   static double Fgaussian(double x, double /*y*/, int /*nparam*/, double param[]);
   static double Fcircle(double x, double y, int /*nparam*/, double param[]);
   static double Fpolynom(double x, double /*y*/, int nparam, double param[]);
   static double Fsinus(double x, double /*y*/, int /*nparam*/, double param[]);
   static double Ftriangle(double x, double /*y*/, int /*nparam*/, double param[]);

   static double Triangle(double x);

private:
   double (*fPfunc)(double x, double y, int nparam, double par[]);   // pointer of user definition function
   int        fNpoints;
   int        fNpar;
   double    *fX;
   double    *fY;
   double    *fWeight;
   double    *fYfit;
   double     fPar[kMaxPar];// parameters
   double     fSigmaPar[kMaxPar];// parameter errors
   double     fChisquare;
   int        fCalls;
   int        fMaxCalls;

};

#endif
