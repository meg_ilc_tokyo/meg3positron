// $Id: MEGSVTGeometry.h 19717 2012-07-05 19:56:05Z uchiyama $
#ifndef MEGSVTGeometry_H
#define MEGSVTGeometry_H

#include <vector>
#include <TObject.h>
#include <TMatrixD.h>
#include <TVector3.h>
#include <TPolyLine.h>
#include "generated/MEGSVTSensorRunHeader.h"
//#include "geom/MEGDCHGeometryBase.h"

class TObjArray;
class MEGSVTSensorRunHeader;
class MEGSVTRunHeader;
class MEGMCSVTRunHeader;
class MEGSVTXYView;
class MEGSVTGeometry: /*public MEGDCHGeometryBase,*/ public TObject
{
private:
   MEGSVTGeometry(MEGSVTRunHeader *anaRH=nullptr, TClonesArray *sensorRHs=nullptr);
   MEGSVTGeometry(const MEGSVTGeometry& cc): /*MEGDCHGeometryBase(),*/ TObject(cc) {};
   virtual ~MEGSVTGeometry();
   static MEGSVTGeometry* instance;

   void Dummy() {};

public:
   static void Destroy();
   static MEGSVTGeometry* GetInstance(MEGSVTRunHeader *anaRH=nullptr, TClonesArray *sensorRHs=nullptr);

   virtual void GetLocalFrame(Int_t wireNumber, Double_t w, TVector3 &p0, TVector3 &uaxis, TVector3 &vaxis, TVector3 &waxis);

   Int_t    GetNLayers() const { return fNLayers; }
   Int_t    GetNSensors() const { return fNSensors; }
   Int_t    GetNSensorsInZAt(Int_t layer) const { return fNSensorsInZ[layer]; }
   Int_t    GetNSensorsInPhiAt(Int_t layer) const { return fNSensorsInPhi[layer]; }
   Double_t GetLayerRAt(Int_t layer) const { return fLayerR[layer]; }
   MEGSVTSensorRunHeader *GetSensorAt(Int_t index);
   void     GetVirtualPlane(Int_t chipid, TVector3 &p0,TVector3 &v,TVector3 &w);

   static void  CopyRunHeaders(MEGMCSVTRunHeader *mcRH, MEGSVTRunHeader *anaRH, TClonesArray *sensorRHs);
   Int_t LayerFromChip(Int_t chipid);
   Int_t SegmentInLayerFromChip(Int_t chipid);
   Int_t SegmentFromChip(Int_t chipid);

   void     DrawXYView(Option_t* option="",Color_t color=0,Int_t width=1, Bool_t update=kFALSE);
   TObjArray *GetXYViewObjArray() { return fXYViewObjArray; }
protected:
   void LoadDBWireInfo() {;}

private:
   Int_t            fNLayers {0};         // Number of layers
   Int_t            fNSensors {0};        // Total number of sensor chips
   std::vector<Int_t>    fNSensorsInZ;    // Number of sensor in z direction for each layer
   std::vector<Int_t>    fNSensorsInPhi;  // Number of sensor in phi direction for each layer
   std::vector<Double_t> fLayerR;         // Radius of each layer
   Double_t         fSlantAngle {0.};     // Sensor slant angle in phi from tangent 
   Double_t         fOverlapWidth {0.};   // Overlap of sensors b/w ladders
   MEGSVTRunHeader *fRunHeader {nullptr}; // Pointer to run header
   TClonesArray    *fSensorRunHeaders {nullptr}; // Pointer to sensor run header array
   
   TObjArray       *fXYViewObjArray {nullptr};

   ClassDef(MEGSVTGeometry,0)  //parameter  object for set:SVT
};

class MEGSVTXYView : public TObject {
public:
   MEGSVTXYView():TObject() {}
   MEGSVTXYView(Int_t layerID,Int_t ladderID,Int_t npoint,
                Double_t x[], Double_t y[]);
   virtual ~MEGSVTXYView();
   
   virtual void   Translate(Double_t x, Double_t y);
   virtual void   Rotate(Double_t phi);
   virtual void   SetBit(UInt_t f, Bool_t flg);
   virtual void   SetLineColor(Color_t color);
   virtual void   SetLineWidth(Int_t width);
   virtual void   Draw(Option_t* option="");

   static void Setup(MEGSVTGeometry* geom, TObjArray* figure);

private:
   Int_t  fLayerID;
   Int_t  fLadderID;
   Int_t  fNPoint;
   TPolyLine *fLadderLine;

};


#endif
