// $Id$

#ifndef NewDelete_H
#define NewDelete_H

#include <new>

void UsingMEGNewOperator();
void MEGNewHandler();
void MEGNewReserveEmergencyMemory();
#endif
