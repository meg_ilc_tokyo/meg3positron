#if 0
   GEANT mechanisms code
   $Id$
#endif

#ifndef GEANT3MECHANISM
#define GEANT3MECHANISM

#define kG3NEXT        (1)
#define kG3MULS        (2)
#define kG3LOSS        (3)
#define kG3FIEL        (4)
#define kG3DCAY        (5)
#define kG3PAIR        (6)
#define kG3COMP        (7)
#define kG3PHOT        (8)
#define kG3BREM        (9)
#define kG3DRAY        (10)
#define kG3ANNI        (11)
#define kG3HADR        (12)
#define kG3ECOH        (13)
#define kG3EVAP        (14)
#define kG3FISS        (15)
#define kG3ABSO        (16)
#define kG3ANNH        (17)
#define kG3CAPT        (18)
#define kG3EINC        (19)
#define kG3INHE        (20)
#define kG3MUNU        (21)
#define kG3TOFM        (22)
#define kG3PFIS        (23)
#define kG3SCUT        (24)
#define kG3RAYL        (25)
#define kG3PARA        (26)
#define kG3PRED        (27)
#define kG3LOOP        (28)
#define kG3NULL        (29)
#define kG3STOP        (30)

#if 0
   Define mechanisms for our own annihilation in flight
  for positrons above threshold defined in common/gemcaifcard/
#endif

#define kG3AIF         (0)

#endif
