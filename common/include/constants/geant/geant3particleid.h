#if 0
   GEANT particle code
   $Id$
#endif
#ifndef GEANT3PARTICLEID
#define GEANT3PARTICLEID

#define kG3Gamma           (1)
#define kG3Positron        (2)
#define kG3Electron        (3)
#define kG3Neutrino        (4)
#define kG3MuonPlus        (5)
#define kG3MuonMinus       (6)
#define kG3PionZero        (7)
#define kG3PionPlus        (8)
#define kG3PionMinus       (9)
#define kG3KaonZeroLong   (10)
#define kG3KaonPlus       (11)
#define kG3KaonMinus      (12)
#define kG3Neutron        (13)
#define kG3Proton         (14)
#define kG3Antiproton     (15)
#define kG3KaonZeroShort  (16)
#define kG3Eta            (17)
#define kG3Lambda         (18)
#define kG3SigmaPlus      (19)
#define kG3SigmaZero      (20)
#define kG3SigmaMinus     (21)
#define kG3XiZero         (22)
#define kG3XiMinus        (23)
#define kG3Omega          (24)
#define kG3Antineutron    (25)
#define kG3Antilambda     (26)
#define kG3AntisigmaMinus (27)
#define kG3AntisigmaZero  (28)
#define kG3AntisigmaPlus  (29)
#define kG3AntixiZero     (30)
#define kG3AntixiPlus     (31)
#define kG3AntiomegaPlus  (32)
#define kG3Deuteron       (45)
#define kG3Tritium        (46)
#define kG3Alpha          (47)
#define kG3Geantino       (48)
#define kG3He3            (49)
#define kG3Cerenkov       (50)

#endif
