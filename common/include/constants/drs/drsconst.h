#ifndef DRSCONST_H
#define DRSCONST_H

#include <Rtypes.h>
#include "units/MEGSystemOfUnits.h"

static constexpr Int_t    kDRSBins                       = 1024;
static constexpr Int_t    kNumberOfChannelsOnDRSChip     = 9;
static constexpr Int_t    kNumberOfChannelsOnDRSChipVer4 = 9;
static constexpr Double_t kDRSInputImpedance             = 50. * MEG::ohm; // input impedance

#endif
