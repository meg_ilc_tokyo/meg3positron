// $Id$
#ifndef TRGCONST_H
#define TRGCONST_H

#include <Rtypes.h>

// simulation
const Int_t kNumberOfTriggerTypes = 33;
const Long64_t kMaxSimulationTimePico = kMaxLong64 / 2;

#endif                          //TRGCONST_H
