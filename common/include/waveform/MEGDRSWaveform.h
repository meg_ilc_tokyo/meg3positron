// $Id$
// Author: Yusuke Uchiyama

#ifndef MEGDRSWaveform_H
#define MEGDRSWaveform_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Class MEGDRSWaveform                                                       //
//                                                                            //
// Class for waveform from DRS                                                //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "waveform/MEGWaveform.h"
#include "constants/drs/drsconst.h"
class MEGDRSChipData;
class MEGDRSChannelData;
struct MEGWaveformTemplateFit;

class MEGDRSWaveform : public MEGWaveform
{

public:
   enum {
      kDRSMaxCells = kDRSBins         //Maximum number of DRS cells
   };
protected:
   Int_t           fNErrorPoints;     //Number of error points (0 or fNPoints)
   Int_t           fStartCell;        //DRS cell number of the first point
   Double_t       *fError;            //[fNErrorPoints]Pointer to an array contains error
   void (*fFCN)(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);    //!Function pointer
   MEGDRSChipData    *fDRSChipData;   //!Pointer to related DRS chip
   MEGDRSChannelData *fDRSChannelData;//!Pointer to related DRS channel

public:
   MEGDRSWaveform();
   MEGDRSWaveform(Int_t npoints, Double_t binsize = 0, Double_t* ptime = 0, const char* title = "Waveform");
   MEGDRSWaveform(Int_t npoints, Double_t binsize, Double_t tmin, const char* title = "Waveform");
   MEGDRSWaveform(const MEGDRSWaveform &wf);
   MEGDRSWaveform(const TH1 *h, Double_t* ptime = 0);
   virtual ~MEGDRSWaveform();

   virtual void        Reset();
   virtual void        SetStartCell(Int_t startcell)           { fStartCell = startcell; }
   virtual void        SetError(Double_t* error)               { fError = error; fNErrorPoints = fNPoints; }
   virtual void        SetErrorAt(Int_t pnt, Double_t error)    { fError[pnt] = error; }
   virtual void        SetNPoints(Int_t npoints);
   using MEGWaveform::SetTime;
   virtual void        SetTime(const MEGWaveform *wfin);
   virtual void        SetTime(Double_t* ptime);
   //virtual void        SetTime(Double_t tmin,Double_t tmax)    { MEGWaveform::SetTime(tmin,tmax); }
   virtual void        SetFixBinSize(Bool_t flag = true, Double_t* ptime = 0);
   virtual void        SetVariableBinSize(Bool_t flag = true, Double_t* ptime = 0);
   virtual void        SetDRSChipData(MEGDRSChipData *chip)          { fDRSChipData = chip; }
   virtual void        SetDRSChannelData(MEGDRSChannelData *channel) { fDRSChannelData = channel; }
   Int_t               GetStartCell()            const { return fStartCell; }
   Double_t           *GetError()                const { return fError; }
   virtual Double_t    GetErrorAt(Int_t pnt)     const;
   MEGDRSChipData     *GetDRSChipData()     const { return fDRSChipData; }
   MEGDRSChannelData  *GetDRSChannelData()  const { return fDRSChannelData; }
   virtual void        Add(const MEGWaveform *waveform, Double_t baseline = 0, Option_t* opt = "",
                           Double_t syncrotime = 0);
   virtual void        Add(const MEGWaveform *waveform, Double_t baseline, Option_t* opt, Double_t syncrotime,
                           Double_t scale);
   virtual void        DrawByCell(Option_t *chopt = "");
   virtual Int_t       FindStartCell(Double_t threshold);
   virtual Double_t GetCellTimeAt(Int_t cell);
   virtual void        Rearrange();
   static  Int_t       PrepareTemplate(MEGWaveformTemplateFit *templatefit[], const char* file,
                                       const char* templatename, Bool_t useEachTemplate = false, Int_t nchannel = 0, Int_t maxpeak = 0,
                                       Double_t ratiothre = 0, Double_t localwidth = 0.);
   virtual void        CalculateError(Double_t noiselevel, Double_t baseline, Double_t factor,
                                      Double_t lowerthre = -1e9, Double_t upperthre = 1e9);
#ifndef __MAKECINT__ // quick fix of dictionary generation error.
   virtual Int_t       MinuitTemplateFit(MEGWaveformTemplateFit *templatefit, const Double_t par[][5],
                                         Double_t result[], MEGDRSWaveform* wfout = 0, Option_t* opt = "FCN1");
#endif
   virtual Int_t       Fit(const char *formula, Option_t *option = "", Option_t *goption = "", Axis_t xmin = 0,
                           Axis_t xmax = 0); // *MENU*
   virtual Int_t       Fit(TF1 *f1, Option_t *option = "", Option_t *goption = "", Axis_t xmin = 0,
                           Axis_t xmax = 0); // *MENU*

   MEGDRSWaveform&       operator+=(const MEGDRSWaveform &wf1);
   MEGDRSWaveform&       operator-=(const MEGDRSWaveform &wf1);
   MEGDRSWaveform&       operator+=(Double_t c1);
   MEGDRSWaveform&       operator-=(Double_t c1);
   MEGDRSWaveform&       operator*=(Double_t c1);
   MEGDRSWaveform&       operator/=(Double_t c1);
   MEGDRSWaveform&       operator=(const MEGDRSWaveform &wf1);
   MEGDRSWaveform        operator+(const MEGDRSWaveform &wf1);
   MEGDRSWaveform        operator-(const MEGDRSWaveform &wf1);
   friend MEGDRSWaveform operator*(Double_t c1, const MEGDRSWaveform &wf1);
   friend MEGDRSWaveform operator*(const MEGDRSWaveform &wf1, Double_t c1);
   friend MEGDRSWaveform operator/(Double_t c1, const MEGDRSWaveform &wf1);
   friend MEGDRSWaveform operator/(const MEGDRSWaveform &wf1, Double_t c1);

   ClassDef(MEGDRSWaveform, 2) //DRS waveform base class
};

inline Double_t MEGDRSWaveform::GetErrorAt(Int_t pnt) const
{
   if (pnt < 0 || pnt >= fNPoints) { return 0.; }
   else { return fError[pnt]; }
}
MEGDRSWaveform operator*(Double_t c1, const MEGDRSWaveform &wf1);
MEGDRSWaveform operator/(Double_t c1, const MEGDRSWaveform &wf1);
inline MEGDRSWaveform operator*(const MEGDRSWaveform &wf1, Double_t c1) { return operator*(c1, wf1); }
inline MEGDRSWaveform operator/(const MEGDRSWaveform &wf1, Double_t c1) { return operator/(c1, wf1); }



struct MEGWaveformTemplateFit {
   MEGDRSWaveform *fWaveform;    //!Pointer to fitted waveform
   Int_t           fNDF;         //Number of degrees of freedom
   Double_t        fTL;          //Lower edge of fitting region (sec)
   Double_t        fTU;          //Upper edge of fitting region (sec)
   MEGWaveform    *fTemplateWaveform;//MEGWaveform of this template.
   Int_t           fNPoints;     //Number of template points
   Double_t       *fAmplitude;   //!Pointer to an array of template waveform amplitude
   Double_t        fBinSize;     //Bin width of template waveform
   Double_t        fLowerEdge;   //Lower edge of averaged waveform histogram (sec)
   Double_t        fArea;        //Area of temlate waveform (V*sec)
   Double_t        fPeakToPeak;  //Peak to peak amplitude of template waveform (V)
   Double_t        fPeakTime;    //Peak time (pulse starts from time 0.)
   Double_t        fCFTime;      //CF time (pulse starts from time 0.)
   Int_t           fNPeak;       //Number of peaks
   Double_t       *fTimeDif;     //[fNPeak-1]Time difference from the 1st peak
   Double_t       *fRatio;       //[fNPeak-1]Ratio of peak height to that of 1st peak
   MEGWaveformTemplateFit()
      : fWaveform(0),
        fTemplateWaveform(0),
        fNPoints(0),
        fAmplitude(0),
        fTimeDif(0),
        fRatio(0)
   {};
   ~MEGWaveformTemplateFit()
   {
      delete fTemplateWaveform;
      delete fTimeDif;
      delete fRatio;
   }
};


namespace MEGWAVEFORM
{
extern MEGWaveformTemplateFit *gTemplateFit;
extern void FCN1(Int_t &npar, Double_t * /*gin*/, Double_t &f, Double_t *par, Int_t /*iflag*/);
extern void FCN2(Int_t &npar, Double_t * /*gin*/, Double_t &f, Double_t *par, Int_t /*iflag*/);
extern void FCN3(Int_t &npar, Double_t * /*gin*/, Double_t &f, Double_t *par, Int_t /*iflag*/);
}
#endif   // MEGDRSWaveform_H
