// $Id$
// Author: Yusuke Uchiyama

#ifndef MEGWaveform_H
#define MEGWaveform_H

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Class MEGWaveform                                                          //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <RConfig.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( push )
#   pragma warning( disable : 4800)
#endif // R__VISUAL_CPLUSPLUS
#include <TNamed.h>
#include <TAttLine.h>
#include <TAttFill.h>
#include <TAttMarker.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS
#include "ROMEPrint.h"

class TGraph;
class TF1;
class TH1;
class TH1F;
class TAxis;
class TVirtualFFT;
class MEGFrequencyResponse;

class MEGWaveform : public TNamed, public TAttLine, public TAttFill, public TAttMarker
{

protected:
   Int_t           fMaxNPoints;   //!Maximum number of data points. Array size of fAmplitude.
   Int_t           fNPoints;      //Number of data points <= fMaxNPoints
   Double_t        fBinSize;      //Average time interval between cells
   Bool_t          fFixBinSize;   //Flag for fix time interval
   Double_t       *fAmplitude;    //[fNPoints]Amplitude allocated automatically
   Double_t       *fTime;         //[fNPoints]Just a pointer to time for variable bin
   Int_t           fID;           //ID number for this object. channel#, pmt#, etc.
   Bool_t          fOverflow;     //If this waveform is saturated, it is true
   Double_t        fTimeMin;      //Time of the first (0) point used for fix bin
   Double_t        fTimeMax;      //Time of the end (fNPoints-1) point used for fix bin
   TH1F           *fHistogram;    //Pointer to histogram used for drawing axis
   TGraph         *fGraph;        //!Pointer to graph used for fitting
   Double_t        fMinimum;      //Minimum value for plotting along Y
   Double_t        fMaximum;      //Maximum value for plotting along Y
   Double_t        fDisplayAmplUnit;//If you want to display by mV, set millivolt(=1e-3).
   Double_t        fDisplayTimeUnit;//If you want to display by micro sec, set microsecond(=1e-6).
   Bool_t          fEditable;     //If true, aveform can be modified with the mouse (default false).
   Double_t        fAttenuation;  //Attenuation factor between detector and readout.
   TObject        *fDrawnObject;  //! Pointer to the object last drawn

public:
   static const Double_t kInvalidTime;
   static const Double_t kMinimumStep;

   MEGWaveform();
   MEGWaveform(Int_t npoints, Double_t binsize = 0, Double_t* ptime = 0, const char* title = "Waveform");
   MEGWaveform(Int_t npoints, Double_t binsize, Double_t tmin, const char* title = "Waveform");
   MEGWaveform(const MEGWaveform &wf);
   MEGWaveform(const TH1 *h, Double_t* ptime = 0);
   virtual ~MEGWaveform();

   virtual void        Import(const TH1 *h, Bool_t fixbin = true);
   virtual TH1F       *Export(Bool_t unitconvert = false) const;
   virtual void        Draw(Option_t *chopt = "")                { Draw(chopt, 0., 0.); }
   virtual void        Draw(Option_t *chopt, Double_t tstart, Double_t tend);
   virtual void        Reset();
   virtual void        ResetAmplitude()                        { if (fAmplitude) memset(fAmplitude, 0, fNPoints * sizeof(Double_t)); }
   virtual void        SetNPoints(Int_t npoints);
   virtual void        SetBinSize(Double_t binsize)            { fBinSize = binsize; fTimeMax = fBinSize * (fNPoints - 1) + fTimeMin; }
   virtual void        SetYMaximum(Double_t maximum)           { fMaximum = maximum;}
   virtual void        SetTitle(const char *title);
   virtual void        SetID(Int_t id)                         { fID = id; }
   virtual void        SetOverflow(Bool_t flag)                { fOverflow = flag; }
   virtual void        SetFixBinSize(Bool_t flag = true, Double_t* ptime = 0);
   virtual void        SetAmplitude(Double_t* amplitude);
   virtual void        SetAmplitudeAt(Int_t pnt, Double_t ampl) { fAmplitude[pnt] = ampl; }
   virtual void        AddAmplitudeAt(Int_t pnt, Double_t ampl) { fAmplitude[pnt] += ampl; }
   virtual void        AddAmplitudeAt(Int_t pnt, Int_t ampl)    { fAmplitude[pnt] += static_cast<Double_t>(ampl); }
   virtual void        SetTimeMin(Double_t timemin);
   virtual void        SetTime(const MEGWaveform *wfin);
   virtual void        SetTime(Double_t* ptime);
   virtual void        SetTime(Double_t tmin, Double_t tmax);
   virtual void        SetTimeAt(Int_t pnt, Double_t t)         { fTime[pnt] = t; }
   virtual void        SetDisplayAmplUnit(Double_t amplunit)   { fDisplayAmplUnit = amplunit; }
   virtual void        SetDisplayTimeUnit(Double_t timeunit)   { fDisplayTimeUnit = timeunit; }
   virtual void        SetEditable(Bool_t editable)            { fEditable = editable; }
   virtual void        SetAttenuation(Double_t attenuation)    { fAttenuation = attenuation;}
   virtual void        SetMinimum(Double_t min)                { fMinimum = min; }
   virtual void        SetMaximum(Double_t max)                { fMaximum = max; }

   Int_t               GetID()                   const { return fID; }
   Int_t               GetNPoints()              const { return fNPoints; }
   Double_t            GetBinSize()              const { return fBinSize; }
   Double_t            GetDisplayAmplUnit()      const { return fDisplayAmplUnit; }
   Double_t            GetDisplayTimeUnit()      const { return fDisplayTimeUnit; }
   TAxis              *GetXaxis();
   TAxis              *GetYaxis();
   Double_t           *GetAmplitude()            const { return fAmplitude; }
   Double_t           *GetTime()                 const { return fTime; }
   Bool_t              IsFixBinSize()            const { return fFixBinSize; }
   Bool_t              IsOverflow()              const { return fOverflow; }
   Double_t            GetAttenuation()          const { return fAttenuation;}
   virtual Double_t    GetAmplitudeAt(Int_t pnt) const;
   virtual Double_t    GetTimeAt(Int_t pnt)      const;
   virtual Double_t    GetTimeMin()              const { return GetTimeAt(0); }
   virtual Double_t    GetTimeMax()              const { return GetTimeAt(fNPoints - 1); }
   virtual Int_t       FindPoint(Double_t x, Int_t startpoint = 0, Int_t lastpoint = 10000000) const;
   virtual Int_t       FindPointIncrement(Double_t x, Int_t startpoint = 0) const;
   virtual Int_t       FindPointDecrement(Double_t x, Int_t startpoint = 10000000) const;
   virtual void        Invert();
   virtual Double_t    InterpolateAmplitude(Double_t x, Int_t n = 2) const;
   virtual Double_t    InterpolateTime(Double_t y, Int_t point, Double_t xini,
                                       Double_t xacc, Int_t imax) const;
   virtual Double_t    MinimumPeak(Double_t tstart = 0, Double_t tend = 0) const;
   virtual void        MinimumPeak(Double_t &peak, Int_t &peakpoint, Double_t tstart = 0,
                                   Double_t tend = 0) const;
   virtual Double_t    MaximumPeak(Double_t tstart = 0, Double_t tend = 0) const;
   virtual void        MaximumPeak(Double_t &peak, Int_t &peakpoint, Double_t tstart = 0,
                                   Double_t tend = 0) const;
   virtual void        GetBothPeak(Double_t *peak) const;
   virtual Double_t    PeakToPeak(Double_t tstart = 0, Double_t tend = 0) const;
   virtual Int_t       PeakSearch(Double_t *peak, Int_t *peakpoint, Int_t nmax, Double_t tstart,
                                  Double_t tend, Double_t threshold, Int_t local, Int_t polarity = -1) const;
   virtual Int_t       PeakSearch(Double_t *peak, Int_t *peakpoint, Int_t nmax, Double_t tstart,
                                  Double_t tend, Double_t threshold, Double_t localwidth, Int_t polarity = -1,
                                  Int_t nveto = 0, const Double_t *timedif = 0, const Double_t *ratio = 0) const;
   virtual Double_t    CalculateBaseline(Double_t tstart, Double_t tend) const;
   virtual Double_t    CalculateBaseline(Double_t tstart, Double_t tend, Double_t lowthre,
                                         Double_t upthre) const;
   virtual Double_t    CalculateBaseline(Double_t tstart, Double_t tend, Double_t low, Double_t up,
                                         Double_t &rms, TH1* hist, Option_t* opt = "") const;
   virtual Double_t    CalculateBaseline(Double_t tstart, Double_t tend, Double_t low, Double_t up,
                                         Double_t binwidth, Option_t* opt = "") const;
   virtual Double_t    CalculateBaseline(Double_t tstart, Double_t tend, Double_t lowthre, Double_t upthre,
                                         Double_t low, Double_t up, Double_t binwidth, Option_t* opt = "") const;
   virtual Double_t    ChargeIntegration(Double_t tstart, Double_t tend, Double_t baseline) const;
   virtual Double_t    ChargeIntegration(Double_t tstart, Double_t tend, Double_t baseline, Double_t jitter,
                                         Int_t polarity) const;
   virtual Double_t    IntegrateHeightSquaredWF(Double_t tstart, Double_t tend, Double_t baseline) const;
   virtual Double_t    CrossingTime(Double_t threshold, Int_t startpnt, Int_t endpnt,
                                    Int_t polarity, Option_t *opt = "", Double_t tacc = 1e-12, Int_t imax = 10) const;
   virtual Double_t    CrossingTime(Double_t threshold, Double_t tstart, Double_t tend,
                                    Int_t polarity, Option_t *opt = "", Double_t tacc = 1e-12, Int_t imax = 10) const;
   virtual Double_t    PulseWidth(Double_t peak, Int_t peakpoint, Double_t baseline = 0, Double_t height = 0.5,
                                  Int_t polarity = 0) const;
   virtual Double_t    TimeOverThreshold(Double_t threshold, Int_t peakpoint, Double_t baseline = 0) const;
   virtual Double_t    RiseTime(Double_t peak, Int_t peakpoint, Double_t baseline = 0,
                                Int_t polarity = 0, Double_t low = 0.1, Double_t high = 0.9, Option_t *opt = "") const;
   virtual Double_t    FallTime(Double_t peak, Int_t peakpoint, Double_t baseline = 0,
                                Int_t polarity = 0, Double_t low = 0.1, Double_t high = 0.9, Option_t *opt = "") const;
   virtual Double_t    LeadingEdge(Double_t threshold, Double_t tstart, Double_t tend, Int_t polarity = 0) const;
   virtual Double_t    LeadingEdge(Double_t threshold) const;
   virtual Double_t    DoubleThreshold(Double_t threlow, Double_t threhigh, Double_t tstart, Double_t tend,
                                       Int_t polarity = 0, Option_t *opt = "LINEAR", Double_t tacc = 1e-12, Int_t imax = 10) const;
   virtual Double_t    ConstantFraction(Double_t cfamplitude, Double_t threshold, Double_t tstart, Double_t tend,
                                        Int_t polarity = 0, Option_t *opt = "LINEAR", Double_t tacc = 1e-12, Int_t imax = 10) const;
   virtual Double_t    ConstantFraction(Double_t fraction, Double_t delay, Double_t baseline, Double_t threshold,
                                        Double_t tstart, Double_t tend, Int_t polarity = 0, MEGWaveform* wfout = 0,
                                        Option_t *opt = "LINEAR", Double_t tacc = 1e-12, Int_t imax = 10) const;
   virtual void        Rotate(Int_t dpnt, MEGWaveform* wfout = 0);
   virtual void        TimeShift(Int_t dpht, MEGWaveform* wfout = 0);
   virtual void        TimeShift(Double_t dt, MEGWaveform *wfout = 0);
   virtual void        Reverse(MEGWaveform* wfout = 0);
   virtual void        Sampling(Int_t npnt, Int_t startpnt, Int_t dpnt, MEGWaveform* wfout) const;
   virtual void        Sampling(Int_t npnt, Double_t starttime, Double_t dt, MEGWaveform* wfout,
                                Option_t* opt = "") const;
   virtual void        Sampling(MEGWaveform* wfout, Option_t* opt = "") const;
   virtual void        Digitize(Int_t n, const Double_t* xbins, const Double_t* xout = 0,
                                MEGWaveform* wfout = 0);
   virtual void        Rebin(Int_t rebinsize, MEGWaveform* wfout) const;
   virtual void        Rebin(Int_t npoints, const Int_t* rebinsize, MEGWaveform* wfout) const;
   virtual void        Clip(Double_t threshold, MEGWaveform* wfout = 0, Int_t polarity = 0,
                            Double_t noiserms = 0);
   virtual void        Reflection(Double_t delay, Double_t ratio, MEGWaveform* wfout, Int_t startpnt = 0) const;
   virtual void        FIR(Int_t nkernel, const Double_t h[], MEGWaveform* wfout, Int_t type = 0) const;
   virtual void        FFTConvolution(Int_t nkernel, const Double_t h[], MEGWaveform* wfout,
                                      Int_t planlevel = 0) const;
   virtual void        FFTConvolution(MEGFrequencyResponse* freqres, MEGWaveform* wfout) const;
   virtual void        MovingAverage(Int_t avepnts, Int_t startpnt = 0, Int_t endpnt = 0
                                     , Bool_t causal = false, Bool_t lowpass = true);
   virtual void        MovingAverage(Int_t avepnts, MEGWaveform* wfout, Int_t startpnt = 0, Int_t endpnt = 0
                                     , Bool_t causal = false, Bool_t lowpass = true) const;
   virtual void        IntegrationCircuit(Double_t rc, MEGWaveform* wfout = 0, Int_t startpnt = 0);
   virtual void        DifferentiationCircuit(Double_t cr, MEGWaveform* wfout = 0, Int_t startpnt = 0);
   virtual void        Differentiate(Int_t diffpnts, MEGWaveform* wfout) const;
   virtual void        Differentiate(Int_t diffpnts, MEGWaveform* wfout, Int_t nDiff) const;
   virtual void        MedianFilter(MEGWaveform* wfout = 0, Double_t threshold = 0);
   virtual void        ChebyshevFilter(Double_t a[], Double_t b[], Int_t pole, MEGWaveform* wfout = 0,
                                       Int_t startpnt = 0, Int_t endpnt = 0);
   static void         ChebyshevCoefficient(Double_t *a, Double_t *b, Double_t fc, Int_t ripple, Int_t pole,
                                            Bool_t lowpass = true);
   static void         FormMatchedFilterKernel(const MEGWaveform* input, MEGWaveform* kernel
                                               , Int_t npoints, Double_t binsize, Double_t tstart);
   static void         FormFilterKernel(const MEGWaveform* input, MEGWaveform* output, MEGWaveform* kernel,
                                        Int_t npoints);
   static void         BlackmanWindow(Int_t nkernel, Double_t h[]);
   virtual TVirtualFFT*FFT(TVirtualFFT* fft, Option_t* option = "K") const;
   virtual MEGWaveform*FFT(MEGWaveform* wfout, Option_t* option = "") const;
   virtual MEGWaveform*FFT(TVirtualFFT* fft, MEGWaveform* wfout, Option_t* option = "") const;
   static MEGWaveform *TransformWaveform(TVirtualFFT* fft, MEGWaveform* wfout, Option_t* option = "");

   static  Double_t    PinkFilter(Double_t in);
   virtual void        AddPinkNoise(Double_t sigma, Int_t startpnt = 0, Int_t endpnt = 0,
                                    MEGWaveform* wfout = 0);
   virtual void        AddFilteredRandomNoise(Double_t sigma, Double_t fc,
                                              Int_t startpnt = 0, Int_t endpnt = 0, MEGWaveform* wfout = 0);
   virtual void        AddSineNoise(Double_t amp, Double_t frequency, Double_t phase = 0);
   virtual void        AddFunction(TF1 *func);
   virtual void        AddTemplate(const MEGWaveform* wfin, Double_t mixtime, Option_t* option = "");
   virtual void        AddTemplate(const MEGWaveform* wfin, Double_t mixtime, Option_t* option,
                                   MEGWaveform* wfout, Double_t scale);

   virtual void        Attenuate(Double_t factor = -1);
   virtual void        CorrectAttenuation(Double_t factor = -1);

   static  Double_t    NewtonRaphson(void (*func)(Double_t *, Double_t, Double_t &, Double_t &), Double_t *coef,
                                     Double_t xini, Double_t xmin, Double_t xmax, Double_t xacc, Int_t imax = 10);
   static  Int_t       PolInt(const Double_t xa[], const Double_t ya[],
                              Int_t n, Double_t x, Double_t &y, Double_t &dy);
   static  void        PolCoe(const Double_t xa[], const Double_t ya[], Int_t n, Double_t coef[]);
   static  void        Pol3(Double_t *coef, Double_t x, Double_t &y, Double_t &dy);
   virtual void        Pop();
   Double_t    Quad(Double_t a, Double_t b, Double_t p, Double_t q, Double_t r) const { return b - (a * (r - q)) / (2 * (r + q - 2 * p)); };
   Bool_t      RootsCubic(const Double_t coef[4], Double_t &a, Double_t &b, Double_t &c) const;

   MEGWaveform&       operator+=(const MEGWaveform &wf1);
   MEGWaveform&       operator-=(const MEGWaveform &wf1);
   MEGWaveform&       operator+=(Double_t c1);
   MEGWaveform&       operator-=(Double_t c1);
   MEGWaveform&       operator*=(Double_t c1);
   MEGWaveform&       operator/=(Double_t c1);
   MEGWaveform&       operator=(const MEGWaveform &wf1);
   MEGWaveform        operator+(const MEGWaveform &wf1);
   MEGWaveform        operator-(const MEGWaveform &wf1);
   friend MEGWaveform operator*(Double_t c1, const MEGWaveform &wf1);
   friend MEGWaveform operator*(const MEGWaveform &wf1, Double_t c1);
   friend MEGWaveform operator/(Double_t c1, const MEGWaveform &wf1);
   friend MEGWaveform operator/(const MEGWaveform &wf1, Double_t c1);

   ClassDef(MEGWaveform, 1) //Waveform class for MEG experiment
};

inline Double_t MEGWaveform::GetAmplitudeAt(Int_t pnt) const
{
   if (pnt < 0) {
      Report(R_VERBOSE, "negative point is invalid. Return 0.");
      return 0.;
   } else if (pnt >= fNPoints) { return 0.; }
   else { return fAmplitude[pnt]; }
}
inline Double_t MEGWaveform::GetTimeAt(Int_t pnt) const
{
   if (pnt < 0) {
      Report(R_VERBOSE, "negative point is invalid. Return -1e10.");
      return -1e10;
   } else if (pnt >= fNPoints) {
      Report(R_VERBOSE, "Point over fNPoints is invalid. Return 1e10.");
      return 1e10;
   }
   if (fFixBinSize) {
      return pnt * fBinSize + fTimeMin;
   } else {
      return fTime[pnt];
   }
}
MEGWaveform operator*(Double_t c1, const MEGWaveform &wf1);
MEGWaveform operator/(Double_t c1, const MEGWaveform &wf1);
inline MEGWaveform operator*(const MEGWaveform &wf1, Double_t c1) { return operator*(c1, wf1); }
inline MEGWaveform operator/(const MEGWaveform &wf1, Double_t c1) { return operator/(c1, wf1); }

class MEGFrequencyResponse
{
   friend class MEGWaveform;
protected:
   Int_t             fNKernel; // length of original impulse response
//    vector<Double_t>  fFreqRes; // the frequency response. [2n]:Real part, [2n+1]:Imaginary part
   TVirtualFFT      *fFFTR2C;  // real number DFT
   TVirtualFFT      *fFFTC2R;  // Inverse DFT
public:
   std::vector<Double_t>  fFreqRes; // the frequency response. [2n]:Real part, [2n+1]:Imaginary part

   MEGFrequencyResponse();
   virtual ~MEGFrequencyResponse();
   void         FormFrequencyResponse(Int_t nkernel, const Double_t h[], Int_t planlevel = 0);
   void         SetNKernel(Int_t n) { fNKernel = n; }
   void         SetFFTR2C(TVirtualFFT *fft) { fFFTR2C = fft; }
   void         SetFFTC2R(TVirtualFFT *ifft) { fFFTC2R = ifft; }
   TVirtualFFT *GetFFTR2C() { return fFFTR2C; }
   TVirtualFFT *GetFFTC2R() { return fFFTC2R; }
   Int_t        GetNKernel() { return fNKernel; }
   std::vector<Double_t>::iterator GetFreqRes() { return fFreqRes.begin(); }
//    ClassDef(MEGFrequencyResponse, 1)  //class for frequency response
};
#endif   // MEGWaveform_H
