// $Id$
#ifndef MEGSTATS_H
#define MEGSTATS_H

#include <math.h>
#include "stddef.h"

// weighted mean
double stats_wmean(double *w, int wstride, double *data, int stride, int n);
double stats_wsd_m(double *w, int wstride, double *data, int stride, int n, double wmean);
double stats_wvariance(double *w, int wstride, double *data, int stride, int n, double wmean);
double stats_factor(double *w, int wstride, int n);

inline double stats_wvariance_m(double *w, int wstride, double *data, int stride, int n, double wmean)
{
   return stats_factor(w, wstride, n) * stats_wvariance(w, wstride, data, stride, n, wmean);
}

inline double stats_wvariance(double *w, int wstride, double *data, int stride, int n)
{
   return stats_wvariance_m(w, wstride, data, stride, n, stats_wmean(w, wstride, data, stride, n));
}

inline double stats_wsd_m(double *w, int wstride, double *data, int stride, int n, double wmean)
{
   return sqrt(stats_factor(w, wstride, n) * stats_wvariance(w, wstride, data, stride, n, wmean));
}

inline double stats_wsd(double *w, int wstride, double *data, int stride, int n)
{
   return stats_wsd_m(w, wstride, data, stride, n, stats_wmean(w, wstride, data, stride, n));
}

// fitting
int fit_linear(const double *x, const size_t xstride,
               const double *y, const size_t ystride,
               const size_t n,
               double *c0, double *c1,
               double *cov_00, double *cov_01, double *cov_11, double *sumsq);
#endif
