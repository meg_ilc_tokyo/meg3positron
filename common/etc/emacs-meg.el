;;; $Id: emacs-meg.el 19536 2012-06-12 20:45:28Z uchiyama $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Coding style and automatic formatting
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun meg-c-mode-common-hook ()
  ;;; Use MEG C coding style
  (c-add-style "MEG" meg-c-style t)
  ;;; Indentation can insert tabs if this is non-nil.
  (setq indent-tabs-mode nil)
  ;;; No new line after semicolon or comma
  (setq c-hanging-semi&comma-criteria nil)
  ;;; Automatic new line
;  (c-toggle-auto-state t)
  ;;; Automatic hungry deletion of whitespace
;  (c-toggle-hungry-state t)
  )

;;; Use MEG style for C and C++ source code.
(add-hook 'c-mode-common-hook 'meg-c-mode-common-hook)
(add-hook 'c++-mode-common-hook 'meg-c-mode-common-hook)

;;; Auto Mode Loading
(setq auto-mode-alist
      (nconc '(("\\.c$" . c-mode)
               ("\\.h$" . c++-mode)
               ("\\.hh$" . c++-mode)
               ("\\.C$" . c++-mode)
               ("\\.cc$" . c++-mode)
               ("\\.cpp$" . c++-mode)
               ("\\.cxx$" . c++-mode))
             auto-mode-alist
             ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Definition of MEG programing style.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;http://cc-mode.sourceforge.net/html-manual/Syntactic-Symbols.html
;;http://cc-mode.sourceforge.net/html-manual/Hanging-Braces.html
;;http://cc-mode.sourceforge.net/html-manual/Hanging-Colons.html
;;http://cc-mode.sourceforge.net/html-manual/Clean_002dups.html
;;http://cc-mode.sourceforge.net/html-manual/c_002doffsets_002dalist.html
(defconst meg-c-style
  '(
    (c-basic-offset             . 3) ;  Basic indent offset
    (c-tab-always-indent        . t) ;  <TAB> indents the current line
    (c-comment-only-line-offset . 0) ;  Extra offset for comment line
    (c-hanging-braces-alist ; Auto newlines for braces
     . (
        (class-open before after)    ; Opens a class definition
        (class-close after)          ; Closes a class definition
        (defun-open before after)    ; Opens a top-level function definition
        (defun-close after)          ; Closes a top-level function definition
        (inline-open after)          ; Opens an in-class inline method
        (inline-close after)         ; Closes an in-class inline method
        (brace-list-open after)      ; Open brace of an enum or list
        (brace-list-close before after) ; Close brace of an enum or list.
        (block-open after)           ; Statement block open brace
        (block-close after)          ; Statement block close brace
        (substatement-open after)    ; Opens a substatement block
        (statement-case-open after)  ; The first line in a case block
        (extern-lang-open after)     ; Opens an extern block
        (extern-lang-close before)   ; Closes an extern block
        ))
    (c-hanging-colons-alist ; Auto newlines for colons
     . (
        (case-label after)           ; A label in a switch block
        (access-label after)         ; C++ access control label
        (label after)                ; Any other label.
        (member-init-intro after)    ; First line in a member initialization
        (inher-intro before)         ; First line of an inheritance list
        ))
    (c-cleanup-list ; Remove (or add) whitespace in specific circumstances
     . (
        brace-else-brace             ; `} else {'
        brace-elseif-brace           ; `} else if (...) {'
        defun-close-semi             ; Terminating semicolon on function
        list-close-comma             ; Aarray and aggregate initializers
        scope-operator               ; Double colons of C++ scope operator
        ))
    (c-offsets-alist ; Indent width
     . (
        (arglist-intro          . ++); The first line in an argument list
        (arglist-close          . c-lineup-arglist) ;Close paren of an arglist
        (substatement-open      . 0) ; The brace opens a substatement block
        (statement-cont         . ++); A continuation of a statement
        (case-label             . 0) ; A label in a switch block
        (label                  . 0) ; Any other label
        (block-open             . 0) ; Statement block open brace
        (member-init-intro      . 0) ; First line in a member initializations
        (inline-open            . 0) ; Opens an in-class inline method
        (inline-close           . 0) ; Closes an in-class inline method
        ))
    (c-echo-syntactic-information-p . nil) ; If showing the syntactic analysis
    )
  "MEG C and C++ Programming Style")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Definition of MEG XML style.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq auto-mode-alist
      (cons '("\\.\\(xml\\|xsl\\|rng\\|xhtml\\)\\'" . nxml-mode)
	    auto-mode-alist))

(add-hook 'nxml-mode-hook
        (lambda ()
        (setq nxml-slash-auto-complete-flag t)
        (setq nxml-child-indent 3)
	(setq nxml-attribute-indent 3)
        (setq indent-tabs-mode nil)
        (setq tab-width 3)
        (define-key nxml-mode-map "\r" 'newline-and-indent)
        )
)
