" $Id: meg.vimrc 13514 2009-06-03 22:14:04Z sawada@PSI.CH $
"
" To use this file, run following command
"   touch ~/.vimrc
"   echo 'source $MEG2SYS/common/etc/meg.vimrc' >> ~/.vimrc
"   echo 'set smartindent'                     >> ~/.vimrc
"

set expandtab
set shiftwidth=3
set tabstop=3
set	cinoptions=>s,e0,n0,f0,{0,}0,^0,:0,=s,l0,b0,g0,hs,ps,t0,i0,+s,c1,C0,/0,(0,us,U0,w0,W0,m0,j0,)20,*30,#0
