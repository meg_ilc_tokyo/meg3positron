// $Id$
#include "mctrack/mctracktools.h"
#include "generated/MEGMCTrackEvent.h"

//_____________________________________________________________________________
Int_t MCTRACKTOOLS::TraceBack(const TClonesArray* tracks, const Int_t kParticleId, Int_t iTrackIndex)
{
// Trace back the shower and find a oldest generation of track with
// given particle id.
//
// Returns array index of MCTrackEvent if the track was
// found. Otherwise returns -1.
//
// Parameters:
//  kParticleId : GEANT3 particle id of the track to find
//  iTrackIndex : array Index of MCTrackEvent where track back starts from.

   if (iTrackIndex < 0) {
      return -1;
   }

   Int_t iCandidate   = -1;
   Int_t iParentIndex = -9999; // Avoid -1 for initial value, which
   // has special meaning for ParentIndex
   Int_t iParticleId;
   MEGMCTrackEvent* MCTrack = 0;

   while (iParentIndex != -1) {
      // Track back until GEANT's primary particle
      MCTrack = static_cast<MEGMCTrackEvent*>(tracks->At(iTrackIndex));
      iParentIndex = MCTrack->GetParentIndex();
      iParticleId  = MCTrack->Getparticleid();

      if (iParticleId == kParticleId) {
         iCandidate = iTrackIndex;
      }

      iTrackIndex = iParentIndex;
   }

   return iCandidate;
}
