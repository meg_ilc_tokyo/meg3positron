// $Id$
#include <math.h>
#include "fit/MEGMarquardt.h"

#define PI 3.1415926535897932384626433832795

MEGMarquardt::MEGMarquardt(double x[], double y[], double weight[],
                           int npoints, int npar, double yfit[])
{
   //
   //  fit a function "fPfunc(x, param[])" to a given data set x[]/y[]
   //  with poisson weighted data points.
   //
   //  Input : x[]  array containing x-values of data points
   //          y[]  array containing value of data points
   //          weight[]  array with data point weights
   //          npoints  number of data points to fit
   //          npar  number of parameters
   //
   //  Output : yfit[]  new fit
   //

   fNpar      = npar;
   fNpoints   = npoints;
   fX         = x;
   fY         = y;
   fWeight    = weight;
   fYfit      = yfit;
}

MEGMarquardt::~MEGMarquardt() {}

void MEGMarquardt::InitFit(double x[], double y[], double weight[],
                           int npoints, int npar, double yfit[])
{
   //
   //  fit a function "fPfunc(x, param[])" to a given data set x[]/y[]
   //  with poisson weighted data points.
   //
   //  Input : x[]  array containing x-values of data points
   //          y[]  array containing value of data points
   //          weight[]  array with data point weights
   //          npoints  number of data points to fit
   //          npar  number of parameters
   //
   //  Output : yfit[]  new fit
   //

   fNpar      = npar;
   fNpoints   = npoints;
   fX         = x;
   fY         = y;
   fWeight    = weight;
   fYfit      = yfit;
}

void MEGMarquardt::Delete()
{
   delete this;
}

void MEGMarquardt::SetFitFunction(double (*pfunc)(double x, double y, int nparam, double par[]))
{
   //
   // Set fit function used over this method
   //
   // Input pointer of function ( function name is function pointer ).
   // Note function arguments.
   //

   fPfunc = pfunc;

}

void MEGMarquardt::SetParameter(double par[])
{
   // Set paramters
   for (int i = 0 ; i < fNpar ; i++) {
      fPar[i] = par[i];
   }
}

void MEGMarquardt::SetParameter(int parnum, double parval)
{
   // Set one parameter
   fPar[parnum] = parval;
}

int MEGMarquardt::GetCalls()
{
   return fCalls;
}

double MEGMarquardt::GetChisquare()
{
   return fChisquare;
}

void MEGMarquardt::GetParameter(double par[], double parerror[])
{
   // Get parameters and errors
   for (int i = 0 ; i < fNpar ; i++) {
      par[i] = fPar[i];
      parerror[i] = fSigmaPar[i];
   }
}

void MEGMarquardt::GetParameter(double par[])
{
   // Get parameters
   for (int i = 0 ; i < fNpar ; i++) {
      par[i] = fPar[i];
   }
}

void MEGMarquardt::GetParameter(int parnum, double &paradd, double &parerradd)
{
   // Get one set of parameter and error
   paradd = fPar[parnum];
   parerradd = fSigmaPar[parnum];
}

double MEGMarquardt::GetParameter(int parnum)
{
   // Get one parameter
   return fPar[parnum];
}
double MEGMarquardt::GetParError(int parnum)
{
   // Get one parameter error
   return fSigmaPar[parnum];
}


void MEGMarquardt::Fderiv(double x, double y, int nparam, double param[],
                          double (*pfunc)(double x, double y, int nparam, double param[]),
                          int nTerms, double deriv[])
{
   //
   //  calculation of derivations of a function "fPfunc" at point x
   //  in respect to its paranters param[]
   //  nTerms : number of parameters
   //  output  deriv[]
   //

   int     i;
   double  delta, yf;

   yf = pfunc(x, y, nparam, param);

   for (i = 0 ; i < nTerms ; i++) {
      delta = fabs(param[i]) / 1000;
      if (delta < 1e-12) {
         delta = 1e-12;
      }

      param[i] += delta;
      deriv[i] = (pfunc(x, y, nparam, param) - yf) / delta;
      param[i] -= delta;
   }
}

double MEGMarquardt::CalcChisqr(double y[], double yfit[], double weight[],
                                int nPoints, int nFree)
{
   //
   //  calculates chiszuare for a fit yfit[] in respect to asample
   //  distribution y[]
   //
   //  Input parameters: y[]       sample
   //                    yfit[]    fit
   //                    weight[]  statistical weights
   //                    nPoints   dimension of y, yfit
   //                    nFree     degree of freedom
   //
   //  Return value:     chisqure
   //

   double chisq;
   int    i;

   if (nFree == 0) {
      return -1;
   }

   for (i = 0, chisq = 0 ; i < nPoints ; i++) {
      chisq += weight[i] * (y[i] - yfit[i]) * (y[i] - yfit[i]);
   }
   return chisq / (double)nFree;
}


int MEGMarquardt::MatInv(double array[kMaxPar][kMaxPar], const int nOrder)
{
   //
   //  inverts matrix array of order nOrder
   //  returns 1 if successful, -1 if matrix is singular
   //

   double amax, save;
   int    ik[kMaxPar], jk[kMaxPar], k, i, j, l;

   for (k = 0 ; k < nOrder ; k++) {
      amax = 0;

label:
      for (i = k ; i < nOrder ; i++) {
         for (j = k ; j < nOrder ; j++) {
            if (fabs(amax) <= fabs(array[i][j])) {
               amax = array[i][j];
               ik[k] = i;
               jk[k] = j;
            }
         }
      }
      if (amax == 0) {
         return -1;
      }

      i = ik[k];
      if ((i - k) < 0) {
         goto label;
      } else if ((i - k) > 0) {
         for (j = 0 ; j < nOrder ; j++) {
            save = array[k][j];
            array[k][j] = array[i][j];
            array[i][j] = -save;
         }
      }
      j = jk[k];
      if ((j - k) < 0) {
         goto label;
      } else if ((j - k) > 0) {
         for (i = 0 ; i < nOrder ; i++) {
            save = array[i][k];
            array[i][k] = array[i][j];
            array[i][j] = -save;
         }
      }

      for (i = 0 ; i < nOrder ; i++)
         if (i != k) {
            array[i][k] /= -amax;
         }

      for (i = 0 ; i < nOrder ; i++)
         for (j = 0 ; j < nOrder ; j++)
            if (i != k && j != k) {
               array[i][j] += array[i][k] * array[k][j];
            }

      for (j = 0 ; j < nOrder ; j++)
         if (j != k) {
            array[k][j] /= amax;
         }
      array[k][k] = 1 / amax;

   } // end of k loop

   for (l = 0 ; l < nOrder ; l++) {
      k = nOrder - l - 1;
      j = ik[k];
      if (j > k)
         for (i = 0 ; i < nOrder ; i++) {
            save = array[i][k];
            array[i][k] = - array[i][j];
            array[i][j] = save;
         }

      i = jk[k];
      if (i > k)
         for (j = 0 ; j < nOrder ; j++) {
            save = array[k][j];
            array[k][j] = - array[i][j];
            array[i][j] = save;
         }
   } // end of l loop

   return 1;
}


int MEGMarquardt::MarquardtFit(double x[], double y[], double weight[],
                               int nPoints, int nTerms,
                               double a[], double sigmaa[], double &flamda,
                               double yfit[], double &chisqr)
{
   //
   //  calculates a new fit contained in yfit[] to a sample
   //  distribution y[] by variation of the parameters a[]
   //  of the fit function; it also calculates the new
   //  parameters a[] and the error of the parameters sigmaa[]
   //
   //  Input : x[]  array containing x-values of sample
   //          y[]  array containing sample
   //          weight[]  array containing weights
   //          nPoints  number of sample points to fit
   //          nTerms  number of free parameters
   //          a[]  array containing paramter values
   //          flamda  marquardt factor
   //
   //  Output : a[]  new parameters
   //           sigmaa[]  errors in parameters
   //           yfit[]  new fit
   //           chisqr  new chisquare
   //

   double alpha[kMaxPar][kMaxPar], beta[kMaxPar], deriv[kMaxPar];
   double array[kMaxPar][kMaxPar], b[kMaxPar], chisq1, buf;
   int    i, j, k;

   // evaluate alpha and beta matreces
   for (i = 0 ; i < nTerms ; i++) {
      beta[i] = 0;
      for (j = 0 ; j <= i ; j++) {
         alpha[i][j] = 0;
      }
   }

   for (i = 0 ; i < nPoints ; i++) {
      Fderiv(x[i], y[i], fNpar, a, fPfunc, nTerms, deriv);

      for (j = 0 ; j < nTerms ; j++) {
         beta[j] += weight[i] * (y[i] - yfit[i]) * deriv[j];
         for (k = 0 ; k <= j ; k++) {
            alpha[j][k] += weight[i] * deriv[j] * deriv[k];
         }
      }
   }

   for (i = 0 ; i < nTerms ; i++)
      for (j = 0 ; j < i ; j++) {
         alpha[j][i] = alpha[i][j];
      }

   chisq1 = chisqr;

   // invert modified curvature matrix to find new paramters
   do {
      for (j = 0 ; j < nTerms ; j++) {
         for (k = 0 ; k < nTerms ; k++) {
            buf = sqrt(alpha[j][j] * alpha[k][k]);
            if (buf == 0) {
               return -1;
            }

            array[j][k] = alpha[j][k] / buf;
         }
         array[j][j] = 1 + flamda;
      }

      if (MatInv(array, nTerms) == -1) {
         return -1;
      }


      for (j = 0 ; j < nTerms ; j++) {
         b[j] = a[j];
         for (k = 0 ; k < nTerms ; k++) {
            b[j] += beta[k] * array[j][k] / sqrt(alpha[j][j] * alpha[k][k]);
         }
      }

      for (i = 0 ; i < nPoints ; i++) {
         yfit[i] = fPfunc(x[i], y[i], fNpar, b);
      }

      chisqr = CalcChisqr(y, yfit, weight, nPoints, nPoints - nTerms);

      if (chisqr > chisq1) {
         flamda *= 10;
      }

      fCalls++;

   } while (chisqr - chisq1 > 0 && fCalls < fMaxCalls);

   // evaluate parameters and uncertainties
   for (j = 0 ; j < nTerms ; j++) {
      a[j] = b[j];
      sigmaa[j] = sqrt(array[j][j] / alpha[j][j]);
   }

   flamda /= 10;

   return 0;
}


double MEGMarquardt::Fit(int maxcalls, double tolerance)
{
   //
   //  Returns : hisqr  new chisquare
   //            -1  if fit doesn't converge. A reason for this could
   //            be that the variation of a parameter doesn't affect
   //            the chisquare value. This happens if the starting
   //            parameters are wrong or the fit function is independent
   //            of a given parameter.
   //

   double chiold, flamda;
   int    i, status;

   // maximum number of iteration
   fMaxCalls = maxcalls;

   // lamda parameter for MarquardtFit
   flamda = 0.001;

   // calculate initial fit with starting parameters
   for (i = 0 ; i < fNpoints ; i++) {
      fYfit[i] = fPfunc(fX[i], fY[i], fNpar, fPar);
   }

   // calculate starting chisquare
   fChisquare = CalcChisqr(fY, fYfit, fWeight, fNpoints, fNpoints - fNpar);

   fCalls = 0;
   // main iteration loop
   do {
      chiold = fChisquare;
      status = MarquardtFit(fX, fY, fWeight, fNpoints, fNpar,
                            fPar, fSigmaPar, flamda, fYfit, fChisquare);
      if (status == -1) {
         return -1;
      }
      if (fabs(fChisquare) < 1E-20) {
         return fChisquare;
      }

   } while (fabs(chiold - fChisquare) / fChisquare > 0.001 * tolerance && fCalls < fMaxCalls);

   return fChisquare;
}


double MEGMarquardt::Fgaussian(double x, double /*y*/, int /*nparam*/, double param[])
{
   // parameter 0 : peak height, 1 : center, 2 : FWHM

   const double kFWHMRatio = 2.35482;

   double x1;
   double sigma;

   // supress division by zero
   if (param[2] == 0) {
      return 0;
   }

   sigma = param[2] / kFWHMRatio;

   x1 = (x - param[1]) / sigma;

   // supress overflow
   if ((x1 > 4) || (x1 < -4)) {
      return 0;
   }

   // sqare argument
   x1 *= x1;

   return param[0] * exp(-0.5 * x1);
}


double MEGMarquardt::Fcircle(double x, double y, int /*nparam*/, double param[])
{
   // parameter 0 : x, 1 : y, 2 : r

   double yres;
   double xd2 = pow(x - param[0], 2);
   double r2 = pow(param[2], 2);
   if (0 > r2 - xd2) {
      yres = param[1];
   } else {
      yres = sqrt(r2 - xd2);
      if (fabs(param[1] + yres - y) < fabs(param[1] - yres - y)) {
         yres = param[1] + yres;
      } else {
         yres = param[1] - yres;
      }
   }
   return yres;
}
double MEGMarquardt::Fpolynom(double x, double /*y*/, int nparam, double param[])
{
   int i;
   double yres = 0;

   for (i = 0; i < nparam; i++) {
      yres += param[i] * pow(x, i);
   }
   return yres;
}
double MEGMarquardt::Fsinus(double x, double /*y*/, int /*nparam*/, double param[])
{
   return param[0] + param[1] * sin(2 * PI * param[2] * x + param[3]);
}
double MEGMarquardt::Triangle(double x)
{
   double pi2 = PI / 2;
   double pi = PI;
   x = x - ((int)(x / (2 * pi))) * 2 * pi;
   if (x <= pi2) {
      return x / pi2;
   } else if (x <= 3 * pi2) {
      return (pi - x) / pi2;
   } else {
      return (x - 2 * pi) / pi2;
   }
}
double MEGMarquardt::Ftriangle(double x, double /*y*/, int /*nparam*/, double param[])
{
   return param[0] + param[1] * MEGMarquardt::Triangle(2 * PI * param[2] * x + param[3]);
}
