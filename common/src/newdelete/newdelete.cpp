// $Id$
// Override new/delete operator

#include <Rtypes.h>
#if defined( R__VISUAL_CPLUSPLUS )
#include <malloc.h>
#endif // R__VISUAL_CPLUSPLUS

#if defined( R__VISUAL_CPLUSPLUS )
#pragma warning( push )
#pragma warning( disable : 4800 )
#endif // R__VISUAL_CPLUSPLUS
#include <TSystem.h>
#if defined( R__VISUAL_CPLUSPLUS )
#pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS
#include <stdlib.h>
#include <Riostream.h>
#if defined(MEG_ANALYZER) || defined(MEG_BARTENDER) || defined(MEG_DB2CARDS)
#   include <ROMEPrint.h>
#endif
#include <iomanip>
#include <newdelete/newdelete.h>

//  When MEG_NEW_MARGIN is 0, this source code does not override new/delete.
//
//  In other case, it replaces new/delte with special version for
//  boundary check. The new operator puts (MEG_NEW_MARGIN * 8) bytes margin
//  after requested memory.
//  Memory map after allocation will be
//
//  |----|--------------------------------|----|
//     A                  B                  C
//
//   A: 8 bytes to remember allocated size
//   B: Requested size
//   C: (MEG_NEW_MARGIN * 8) bytes margin

#ifndef MEG_NEW_MARGIN
#   define MEG_NEW_MARGIN 1
#endif

#if defined (R__UNIX)
const ULong64_t kMEGNewGuard      = 0x7ff7dead7fb7beefULL;
const ULong64_t kMEGNewGuardArray = 0x7ff7cafe7fb7babeULL;
const ULong64_t kSNaNMask         = 0xffff000000000000ULL;
#else
const ULong64_t kMEGNewGuard      = 0x7ff7dead7fb7beef;
const ULong64_t kMEGNewGuardArray = 0x7ff7cafe7fb7babe;
const ULong64_t kSNaNMask         = 0xffff000000000000;
#endif

#if 1
const size_t    kMaxAllocationSize = (size_t) -1;
#else
const size_t    kMaxAllocationSize = 1 << 28;
#endif

const char* const kMEGTooBigMemoryMsg =
   " *** Break *** too large memory request";

const char* const kMEGBadAllocMsg =
   " *** Break *** memory allocation error";

const char* const kMEGNewOverrunMsg =
   " *** Break *** delete uninitialized pointer, or buffer overrun";

const char* const kMEGNewMismatchMsg =
   " *** Break *** mismatched delete / delete []";

/*
Signaling NaN (double) One of x must be 1, s is 0 or 1.
s111 1111 1111 0xxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx

Signaling NaN (float) One of x must be 1, s is 0 or 1.
s111 1111 10xx xxxx xxxx xxxx xxxx xxxx
*/

using namespace std;

#if (MEG_NEW_MARGIN != 0)

void UsingMEGNewOperator()
{
#if defined(MEG_ANALYZER) || defined(MEG_BARTENDER) || defined(MEG_DB2CARDS)
   Report(R_VERBOSE, "Using MEG new/delete operators. (%d bytes margin)",
          static_cast<Int_t>(MEG_NEW_MARGIN * sizeof(ULong64_t)));
#else
   cout << "Using MEG new/delete operators. (" << MEG_NEW_MARGIN * sizeof(ULong64_t)
        << " bytes margin)" << endl;
#endif
}

namespace MEG_NEWDELETE
{
UChar_t* Malloc(size_t size);
void     Free(UChar_t* p);
UChar_t* AllocateMemory(size_t size);
void     WriteMagicNumber(UChar_t *p, size_t size, ULong64_t magic);
void     CheckMagicNumber(UChar_t *p, ULong64_t correctMagic,
                          ULong64_t wrongMagic);
}

using namespace MEG_NEWDELETE;

//______________________________________________________________________________
inline UChar_t* MEG_NEWDELETE::Malloc(size_t size)
{
#if defined (R__UNIX)
   return static_cast<UChar_t*>(malloc(size));
#else
   return static_cast<UChar_t*>(_aligned_malloc(size, sizeof(ULong64_t)));
#endif
}

//______________________________________________________________________________
inline void MEG_NEWDELETE::Free(UChar_t* p)
{
#if defined (R__UNIX)
   free(p);
#else
   _aligned_free(p);
#endif
}

//______________________________________________________________________________
inline UChar_t* MEG_NEWDELETE::AllocateMemory(size_t size)
{
   if (size == 0) {
      size = 8;
   } else if ((size & 7) != 0) {
      // align
      size = 8 * (((size & ~7) >> 3)  + 1);
   }

   size += sizeof(ULong64_t) + MEG_NEW_MARGIN * sizeof(ULong64_t);

   if (size > kMaxAllocationSize) {
      cerr << kMEGTooBigMemoryMsg << " (" << size << " bytes)" << endl;
      gSystem->StackTrace();
      gSystem->Abort();
   }

   UChar_t *p = Malloc(size);

   while (p == 0) {
      new_handler handler = set_new_handler(0);
      set_new_handler(handler);
      if (!handler) {
         throw bad_alloc();
      } else {
         (*handler)();
         p = Malloc(size);
      }
   }

   return p;
}

//______________________________________________________________________________
inline void MEG_NEWDELETE::WriteMagicNumber(UChar_t *p, size_t size,
                                            ULong64_t magic)
{
   ULong64_t val;

   // size must be less than 0x0000ffffffffffff
   val = (magic & kSNaNMask) | size;
   memcpy(p, &val, sizeof(ULong64_t));

   // put SNaN to detect uninitialized access
   val = magic;
   memcpy(p + sizeof(ULong64_t), &val, sizeof(ULong64_t));

   UChar_t *pi = p + sizeof(ULong64_t) + size;
#if (MEG_NEW_MARGIN == 1)
   memcpy(pi, &val, sizeof(ULong64_t));
#else
   Int_t i;
   for (i = (MEG_NEW_MARGIN - 1) * sizeof(ULong64_t);
        i >= 0 ;
        i -= sizeof(ULong64_t)) {
      memcpy(pi + i, &val, sizeof(ULong64_t));
   }
#endif
}

//______________________________________________________________________________
inline void MEG_NEWDELETE::CheckMagicNumber(UChar_t *p, ULong64_t correctMagic,
                                            ULong64_t wrongMagic)
{
   ULong64_t size;
   ULong64_t val;

   memcpy(&size, p, sizeof(ULong64_t));
   size = size & ~kSNaNMask;
   UChar_t *pi = p + sizeof(ULong64_t) + size;

#if (MEG_NEW_MARGIN == 1)
   memcpy(&val, pi, sizeof(ULong64_t));
   if (val != correctMagic) {
      if (val == wrongMagic) {
         cerr << kMEGNewMismatchMsg << endl;
      } else {
         cerr << kMEGNewOverrunMsg << endl;
      }
      gSystem->StackTrace();
      gSystem->Abort();
   }
#else
   Int_t i;
   for (i = (MEG_NEW_MARGIN - 1) * sizeof(ULong64_t);
        i >= 0;
        i -= sizeof(ULong64_t)) {
      memcpy(&val, pi + i, sizeof(ULong64_t));
      if (val != correctMagic) {
         if (val == wrongMagic) {
            cerr << kMEGNewMismatchMsg << endl;
         } else {
            cerr << kMEGNewOverrunMsg << endl;
         }
         gSystem->StackTrace();
         gSystem->Abort();
      }
   }
#endif
}

//--- New/Delete single ---//

//______________________________________________________________________________
#if defined (R__UNIX)
void* operator new (size_t size) throw (bad_alloc)
#else
void* operator new (size_t size)
#endif
{
   UChar_t *p = AllocateMemory(size);
   WriteMagicNumber(p, size, kMEGNewGuard);
   return p + sizeof(ULong64_t);
}

//______________________________________________________________________________
void operator delete (void* ptr) throw ()
{
   if (!ptr) {
      return;
   }

   UChar_t *p = static_cast<UChar_t*>(ptr) - sizeof(ULong64_t);
   CheckMagicNumber(p, kMEGNewGuard, kMEGNewGuardArray);
   Free(p);
}

//--- New/Delete single without throw ---//

//______________________________________________________________________________
void* operator new (size_t size, const nothrow_t&) throw()
{
   void *p = 0;
   try {
      p = ::operator new (size);
   } catch (bad_alloc &) {
      return 0;
   }
   return p;
}

//______________________________________________________________________________
void operator delete (void *ptr, const nothrow_t&) throw ()
{
   ::operator delete (ptr);
}

//--- New/Delete array ---//

//______________________________________________________________________________
#if defined (R__UNIX)
void* operator new[](size_t size) throw (bad_alloc)
#else
void* operator new[](size_t size)
#endif
{
   UChar_t *p = AllocateMemory(size);
   WriteMagicNumber(p, size, kMEGNewGuardArray);
   return p + sizeof(ULong64_t);
}

//______________________________________________________________________________
void operator delete[](void* ptr) throw ()
{
   if (!ptr) {
      return;
   }

   UChar_t *p = static_cast<UChar_t*>(ptr) - sizeof(ULong64_t);
   CheckMagicNumber(p, kMEGNewGuardArray, kMEGNewGuard);
   Free(p);
}

//--- New/Delete array without throw ---//

//______________________________________________________________________________
void* operator new[](size_t size, const nothrow_t&) throw()
{
   void *p = 0;
   try {
      p = ::operator new[](size);
   } catch (bad_alloc &) {
      return 0;
   }
   return p;
}

//______________________________________________________________________________
void operator delete[](void *ptr, const nothrow_t&) throw ()
{
   ::operator delete[](ptr);
}

#else // MEG_NEW_MARGIN == 0
//______________________________________________________________________________
void UsingMEGNewOperator() {}
#endif

namespace
{
char* memoryPool = 0;
void MEGNewFreeEmergencyMemory()
{
   if (memoryPool) {
      delete [] memoryPool;
      memoryPool = 0;
   }
}
}

//______________________________________________________________________________
void MEGNewReserveEmergencyMemory()
{
   if (!memoryPool) {
      memoryPool = new char[2048000]; // 2MB. Maybe enough for stack trace.
      atexit(MEGNewFreeEmergencyMemory);
   }
}

//______________________________________________________________________________
void MEGNewHandler()
{
   MEGNewFreeEmergencyMemory();

   MemInfo_t mem;
   gSystem->GetMemInfo(&mem);
   cerr << kMEGBadAllocMsg << endl << endl;
   cerr << "Mem:"
        << "     " << setw(5) << mem.fMemTotal << "M total,"
        << "    " << setw(5) << mem.fMemUsed << "M used,"
        << "    " << setw(5) << mem.fMemFree << "M free" << endl;
   cerr << "Swap:"
        << "    " << setw(5) << mem.fSwapTotal << "M total,"
        << "    " << setw(5) << mem.fSwapUsed << "M used,"
        << "    " << setw(5) << mem.fSwapFree << "M free" << endl;
   cerr << endl;
//   gSystem->StackTrace();
   gSystem->Abort();
}
