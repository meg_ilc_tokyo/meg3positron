// $Id: MEGSVTGeometry.cpp 19717 2012-07-05 19:56:05Z uchiyama $
#include <RConfig.h>
#if defined( R__VISUAL_CPLUSPLUS )
#pragma warning( push )
#pragma warning( disable : 4244)
#endif // R__VISUAL_CPLUSPLUS
#include <TMath.h>
#include <TMatrixD.h>
#include <TObject.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TVectorD.h>
#include <TMatrixD.h>
#if defined( R__VISUAL_CPLUSPLUS )
#pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS
#include "generated/MEGMEGParameters.h"
#include "generated/MEGGlobalSteering.h"
#include "generated/MEGMCSVTRunHeader.h"
#include "generated/MEGSVTRunHeader.h"
#include "generated/MEGSVTSensorRunHeader.h"
#include "geom/MEGSVTGeometry.h"
#include "units/MEGSystemOfUnits.h"
#include "MEGAnalyzer.h"
using namespace std;

ClassImp(MEGSVTGeometry)

//__________________________________________________________
MEGSVTGeometry* MEGSVTGeometry::instance = nullptr;

//__________________________________________________________
MEGSVTGeometry::MEGSVTGeometry(MEGSVTRunHeader *anaRH, TClonesArray *sensorRHs)
: fRunHeader(anaRH), fSensorRunHeaders(sensorRHs)
{
   if (!anaRH) {
      fRunHeader = gAnalyzer->GetSVTRunHeader();
   }
   if (!sensorRHs) {
      fSensorRunHeaders = gAnalyzer->GetSVTSensorRunHeaders();
   }
   
   fNLayers = fRunHeader->GetNLayers();
   fNSensors = fRunHeader->GetNSensors();
   fSlantAngle = fRunHeader->GetSlantAngle();
   fOverlapWidth = fRunHeader->GetOverlapWidth();

   fNSensorsInZ.resize(fNLayers);
   fNSensorsInPhi.resize(fNLayers);
   fLayerR.resize(fNLayers);
   for (Int_t iLayer = 0; iLayer < fNLayers; iLayer++) {
      fNSensorsInZ[iLayer] = fRunHeader->GetNSensorsInZAt(iLayer);
      fNSensorsInPhi[iLayer] = fRunHeader->GetNSensorsInPhiAt(iLayer);
      fLayerR[iLayer] = fRunHeader->GetLayerRAt(iLayer);
   }

}

//__________________________________________________________
MEGSVTGeometry::~MEGSVTGeometry()
{
   if (fXYViewObjArray) {
      fXYViewObjArray->Delete();
      delete fXYViewObjArray;
   }
}

//__________________________________________________________
MEGSVTGeometry* MEGSVTGeometry::GetInstance(MEGSVTRunHeader *anaRH, TClonesArray *sensorRHs)
{
   if (!instance)
      instance = new MEGSVTGeometry(anaRH, sensorRHs);
   return instance;
}

//__________________________________________________________
void MEGSVTGeometry::Destroy()
{
   SafeDelete(instance);
}

//______________________________________________________________________________
void  MEGSVTGeometry::CopyRunHeaders(MEGMCSVTRunHeader *mcRH,
                                     MEGSVTRunHeader *anaRH, TClonesArray *sensorRHs)
{
   // Copy the information in MC run header to run headers for analyzer

   if (!mcRH) {
      mcRH = gAnalyzer->GetMCSVTRunHeader();
   }
   if (!anaRH) {
      anaRH = gAnalyzer->GetSVTRunHeader();
   }
   if (!sensorRHs) {
      sensorRHs = gAnalyzer->GetSVTSensorRunHeaders();
   }
   if (!mcRH || !anaRH || !sensorRHs) {
      Report(R_ERROR, "Input run header pointer(s) is null.");
      return;
   }
   
   anaRH->SetNLayers(mcRH->GetNLayers());
   anaRH->SetNSensors(mcRH->GetNSensors());
   anaRH->SetSlantAngle(mcRH->GetSlantAngle());
   anaRH->SetOverlapWidth(mcRH->GetOverlapWidth());
   anaRH->SetNSensorsInZSize(anaRH->GetNLayers());
   anaRH->SetNSensorsInPhiSize(anaRH->GetNLayers());
   anaRH->SetLayerRSize(anaRH->GetNLayers());
   for (Int_t iLayer = 0; iLayer < anaRH->GetNLayers(); iLayer++) {
      anaRH->SetNSensorsInZAt(iLayer, mcRH->GetNSensorsInZAt(iLayer));
      anaRH->SetNSensorsInPhiAt(iLayer, mcRH->GetNSensorsInPhiAt(iLayer));
      anaRH->SetLayerRAt(iLayer, mcRH->GetLayerRAt(iLayer));
   }

   const Int_t nSensor = anaRH->GetNSensors();
   sensorRHs->ExpandCreate(nSensor);
   for (Int_t iSensor = 0; iSensor < nSensor; iSensor++) {
      auto sensorRH = static_cast<MEGSVTSensorRunHeader*>(sensorRHs->At(iSensor));
      sensorRH->SetLayer(mcRH->GetSensorLayerAt(iSensor));
      sensorRH->SetXYZAt(0, mcRH->GetSensorXAt(iSensor));
      sensorRH->SetXYZAt(1, mcRH->GetSensorYAt(iSensor));
      sensorRH->SetXYZAt(2, mcRH->GetSensorZAt(iSensor));
      sensorRH->SetDirectionAt(0, mcRH->GetSensorDirXAt(iSensor));
      sensorRH->SetDirectionAt(1, mcRH->GetSensorDirYAt(iSensor));
      sensorRH->SetDirectionAt(2, mcRH->GetSensorDirZAt(iSensor));
      sensorRH->SetSensorSizeCopy(3, mcRH->GetSensorSize());
      sensorRH->SetNPixelsCopy(2, mcRH->GetNPixels());
      sensorRH->SetPixelSizeCopy(2, mcRH->GetPixelSize());
   }   
   
}

//______________________________________________________________________________
void MEGSVTGeometry::GetLocalFrame(Int_t /*wireNumber*/, Double_t /*w*/,
                                   TVector3 &p0, TVector3 &uaxis, TVector3 &vaxis, TVector3 &waxis)
{

  p0 = TVector3(0.,0.,0.);
  uaxis = TVector3(1.,0.,0.);
  vaxis = TVector3(0.,1.,0.);
  waxis = TVector3(0.,0.,1.);

}


//______________________________________________________________________________
MEGSVTSensorRunHeader *MEGSVTGeometry::GetSensorAt(Int_t index)
{
   return static_cast<MEGSVTSensorRunHeader*>(fSensorRunHeaders->At(index));
}

//______________________________________________________________________________
void MEGSVTGeometry::GetVirtualPlane(Int_t chipID, TVector3 &p0, TVector3 &v, TVector3 &w)
{
   // Get plane of the chip (not virtual but real plane of the chip)
   
   // Get the position (center) of the chip
   p0.SetXYZ(GetSensorAt(chipID)->GetXYZAt(0),
             GetSensorAt(chipID)->GetXYZAt(1),
             GetSensorAt(chipID)->GetXYZAt(2));

   // Get the plane of the SVT sensor
   TVector3 norm(GetSensorAt(chipID)->GetDirectionAt(0),
                 GetSensorAt(chipID)->GetDirectionAt(1),
                 GetSensorAt(chipID)->GetDirectionAt(2));
   w.SetXYZ(0, 0, 1.); // now assuming z-axis as one of the direction
   v = w.Cross(norm);
   v = v.Unit();
}


//______________________________________________________________________________
Int_t MEGSVTGeometry::LayerFromChip(Int_t chipID)
{
   if (chipID < 0) return -1;
   Int_t nLayers = fRunHeader->GetNLayers();
   Int_t nChip(0);
   for (Int_t iLayer = 0; iLayer < nLayers; iLayer++) {
      Int_t nZ = fRunHeader->GetNSensorsInZAt(iLayer);
      Int_t nPhi = fRunHeader->GetNSensorsInPhiAt(iLayer);
      nChip += nZ * nPhi;
      if (nChip > chipID) {
         return iLayer;
      }
   }
   return -1;
}

//______________________________________________________________________________
Int_t MEGSVTGeometry::SegmentFromChip(Int_t chipID)
{
   if (chipID < 0) return -1;
   Int_t nLayers = fRunHeader->GetNLayers();
   Int_t nChip(0);
   Int_t nSegment(0);
   for (Int_t iLayer = 0; iLayer < nLayers; iLayer++) {
      Int_t nZ = fRunHeader->GetNSensorsInZAt(iLayer);
      Int_t nPhi = fRunHeader->GetNSensorsInPhiAt(iLayer);
      for (Int_t iPhi = 0; iPhi < nPhi; iPhi++) {
         nChip += nZ;
         if (nChip > chipID) {
            return nSegment;
         }
         nSegment++;
      }
   }
   return -1;

}

//______________________________________________________________________________
Int_t MEGSVTGeometry::SegmentInLayerFromChip(Int_t chipID)
{
   if (chipID < 0) return -1;
   Int_t nLayers = fRunHeader->GetNLayers();
   Int_t layerID = LayerFromChip(chipID);
   Int_t nChip(0);
   Int_t nSegment(0);
   for (Int_t iLayer = 0; iLayer < nLayers; iLayer++) {
      Int_t nZ = fRunHeader->GetNSensorsInZAt(iLayer);
      Int_t nPhi = fRunHeader->GetNSensorsInPhiAt(iLayer);
      if (iLayer < layerID) {
         nChip += nZ * nPhi;
         continue;
      }

      for (Int_t iPhi = 0; iPhi < nPhi; iPhi++) {
         nChip += nZ;
         if (nChip > chipID) {
            return nSegment;
         }
         nSegment++;
      }
   }
   return -1;
}

//______________________________________________________________________________
void MEGSVTGeometry::DrawXYView(Option_t* option,Color_t color,Int_t width,Bool_t update)
{
   if (!fXYViewObjArray || update) {
      // create 
      if (!fXYViewObjArray) {
         fXYViewObjArray = new TObjArray(1);
      }
      fXYViewObjArray->Delete();
      
      MEGSVTXYView::Setup(this, fXYViewObjArray);
   }
   for (Int_t iObj = 0; iObj < fXYViewObjArray->GetEntriesFast(); iObj++) {
      static_cast<MEGSVTXYView*>(fXYViewObjArray->At(iObj))->SetLineColor(color);
      static_cast<MEGSVTXYView*>(fXYViewObjArray->At(iObj))->SetLineWidth(width);
   }
   fXYViewObjArray->Draw(option);
}


//______________________________________________________________________________
// class MEGSVTXYView
//______________________________________________________________________________


//______________________________________________________________________________
MEGSVTXYView::MEGSVTXYView(Int_t layerID, Int_t ladderID, Int_t npoint, Double_t x[], Double_t y[])
:TObject()   
,fLayerID(layerID)
,fLadderID(ladderID)
,fNPoint(npoint)
,fLadderLine(new TPolyLine(npoint, x, y))
{
   // Normal constructor
   fLadderLine->SetBit(kCannotPick, kTRUE);
}

//__________________________________________________________
MEGSVTXYView::~MEGSVTXYView()
{
   // Destructor
   delete fLadderLine;
}

//__________________________________________________________
void MEGSVTXYView::Translate(Double_t xtrans, Double_t ytrans)
{
   if (fLadderLine) {
      for (Int_t iPnt = 0; iPnt < fNPoint; iPnt++) {
         fLadderLine->GetX()[iPnt] += xtrans;
         fLadderLine->GetY()[iPnt] += ytrans;
      }
   }
}

//__________________________________________________________
void MEGSVTXYView::Rotate(Double_t phi)
{
   // rotate in phi-dir for phi radian

   if (fLadderLine) {
      Double_t xtemp, ytemp;
      Double_t *xpnt, *ypnt;
      xpnt = fLadderLine->GetX();
      ypnt = fLadderLine->GetY();
      Int_t iPnt;
       for (iPnt = 0; iPnt < fNPoint; iPnt++) {
          xtemp = xpnt[iPnt]*TMath::Cos(phi) - ypnt[iPnt]*TMath::Sin(phi);
          ytemp = xpnt[iPnt]*TMath::Sin(phi) + ypnt[iPnt]*TMath::Cos(phi);
          xpnt[iPnt] = xtemp;
          ypnt[iPnt] = ytemp;
       }
   }
}

//______________________________________________________________________________
void MEGSVTXYView::Draw(Option_t* option)
{
   // Draw
   fLadderLine->Draw(option);
}

//______________________________________________________________________________
void MEGSVTXYView::SetBit(UInt_t f, Bool_t flg)
{
   fLadderLine->SetBit(f, flg);
}

//______________________________________________________________________________
void MEGSVTXYView::SetLineColor(Color_t color)
{
   fLadderLine->SetLineColor(color);
}

//______________________________________________________________________________
void MEGSVTXYView::SetLineWidth(Int_t width)
{
   fLadderLine->SetLineWidth(width);
}

//______________________________________________________________________________
void MEGSVTXYView::Setup(MEGSVTGeometry *geom, TObjArray *figObj)
{
   
   Int_t nLayers = geom->GetNLayers();
   Int_t currentSensorIndex(0);
   const TVector3 norm0(1., 0., 0.);
   for (Int_t iLayer = 0; iLayer < nLayers; iLayer++) {
      Int_t nLadders = geom->GetNSensorsInPhiAt(iLayer);
      Int_t nSensorInLadder = geom->GetNSensorsInZAt(iLayer);
      for (Int_t iLadder = 0; iLadder < nLadders; iLadder++) {
         MEGSVTSensorRunHeader *sensor = geom->GetSensorAt(currentSensorIndex);
         
         Double_t height = sensor->GetSensorSizeAt(1);
         const Int_t kNPoint = 2; // just a line
         Double_t x[kNPoint] = {0, 0};
         Double_t y[kNPoint] = {-height * 0.5, height * 0.5};
         MEGSVTXYView *xyView = new MEGSVTXYView(iLayer, iLadder, kNPoint, x, y);
                                                 

         TVector3 norm(sensor->GetDirection());
         Double_t phi = norm.Phi();// in rad
         xyView->Rotate(phi);
         xyView->Translate(sensor->GetXYZAt(0), sensor->GetXYZAt(1));

         figObj->Add(xyView);
         currentSensorIndex += nSensorInLadder;
      }

   }
}
