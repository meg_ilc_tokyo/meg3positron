// Author : Ryu Sawada

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// MEGDRSChipData                                                             //
//                                                                            //
// DRS raw data format.
//                                                                            //
//                                                                            //
/////////////////////////////////////----///////////////////////////////////////

#include <RConfig.h>
#include <zlib.h>
#include <TMath.h>
#include <vector>
#include <algorithm>
#include <numeric>
#include <functional>
#include "generated/MEGGlobalSteering.h"
#include "drs_encode/MEGDRSChipData.h"
#include "drs_encode/MEGDRSChannelData.h"
#include "drs_encode/drs_encode.h"
#include "constants/drs/drsconst.h"
#include "units/MEGSystemOfUnits.h"
#include "stats/megstats.h"
#include "drs_encode/DRSCalibration.h"
#include "ROMEAnalyzer.h"
#include "ROMEPrint.h"
#include "trigger/triggercommon.h"

using namespace std;
using namespace MEG;
namespace
{
}

ClassImp(MEGDRSChipData)

Int_t MEGDRSChipData::fgNumberOfChips = 0;

//______________________________________________________________________________
MEGDRSChipData::MEGDRSChipData(const MEGDRSChipData & c)
   : TObject(c)
   , fHaveData(kFALSE)
   , fAddress(0)
   , fMeasuredFrequency(0)
   , fOnBoardTemperature(0)
   , fOnBoardFrequency(0)
   , fStartCellDelay(0)
   , fChannels(new TObjArray())
   , fIndex(-1)
{
   Report(R_ERROR, "MEGDRSChipData::MEGDRSChipData(const MEGDRSChipData&) is not implemented");
}

//______________________________________________________________________________
MEGDRSChipData & MEGDRSChipData::operator=(const MEGDRSChipData & /* c */)
{
   Report(R_ERROR, "MEGDRSChipData::operator=(const MEGDRSChipData&) is not implemented");
   return *this;
}

//______________________________________________________________________________
MEGDRSChipData::MEGDRSChipData(UShort_t haveData, UShort_t address)
   : TObject()
   , fHaveData(haveData)
   , fAddress(address)
   , fMeasuredFrequency(0)
   , fOnBoardTemperature(0)
   , fOnBoardFrequency(0)
   , fStartCellDelay(0)
   , fChannels(new TObjArray())
   , fIndex(-1)
{
   MEGDRSChipData::Class()->IgnoreTObjectStreamer();

   fTimeCalibrations.resize(kNumberOfChannelsOnDRSChip);
   fTimeCalibrationType.resize(kNumberOfChannelsOnDRSChip, EDRSTimeCalibrationType::kFixedInterval);

   fChannels->SetOwner(kTRUE);
   // construct channels
   Int_t i;
   for (i = 0; i < kNumberOfChannelsOnDRSChip; i++) {
      fChannels->AddAtAndExpand(new MEGDRSChannelData(), i);
   }

}

//______________________________________________________________________________
void MEGDRSChipData::Reset()
{
   fHaveData = kFALSE;
   fAddress = 0;

   // reset channels
   Int_t i;
   MEGDRSChannelData *ch;
   for (i = 0; i < kNumberOfChannelsOnDRSChip; i++) {
      if ((ch = static_cast<MEGDRSChannelData*>(fChannels->At(i)))) {
         ch->Reset();
      }
   }

   fOnBoardTemperature = 0;
   fOnBoardFrequency = 0;
   fStartCellDelay = 0;
}

//______________________________________________________________________________
void MEGDRSChipData::DecodeWaves()
{
   Int_t iChannel;
   for (iChannel = 0; iChannel < kNumberOfChannelsOnDRSChip; iChannel++) {
      static_cast<MEGDRSChannelData*>(fChannels->At(iChannel))->SetTimeCalibrationData(&fTimeCalibrations[iChannel],
            &fTimeCalibrationType[iChannel]);
      static_cast<MEGDRSChannelData*>(fChannels->At(iChannel))->DecodeWave();
   }
}

//______________________________________________________________________________
void MEGDRSChipData::ZeroWaves()
{
   Int_t iChannel;
   for (iChannel = 0; iChannel < kNumberOfChannelsOnDRSChip; iChannel++) {
      MEGDRSWaveform *wf;
      if ((wf = static_cast<MEGDRSChannelData*>(fChannels->At(iChannel))->GetWaveform())) {
         wf->SetNPoints(0);
      }
   }
}

//______________________________________________________________________________
void MEGDRSChipData::ResetWaves()
{
   Int_t iChannel;
   MEGDRSChannelData *ch;
   for (iChannel = 0; iChannel < kNumberOfChannelsOnDRSChip; iChannel++) {
      if ((ch = static_cast<MEGDRSChannelData*>(fChannels->At(iChannel)))) {
         ch->DeleteWaveform();
      }
   }
}

//______________________________________________________________________________
void MEGDRSChipData::SetIndex()
{
   fIndex = fgNumberOfChips;
   fgNumberOfChips++;
}

//______________________________________________________________________________
void MEGDRSChipData::CalibrateWaves(Int_t method)
{
   for (Int_t iChannel = 0; iChannel < kNumberOfChannelsOnDRSChip; iChannel++) {
      MEGDRSChannelData *ch;
      if ((ch = static_cast<MEGDRSChannelData*>(fChannels->At(iChannel)))) {
         ch->CalibrateWave(method);
      }
   }
}

//______________________________________________________________________________
void MEGDRSChipData::ReadTimeCalibDatabase(ROMEDataBase *db, Int_t id, Long64_t runNumber,
                                           Long64_t eventNumber)
{

   UShort_t dt(0);
   for (auto&& ch : *fChannels) {
      if (ch) {
         dt = static_cast<MEGDRSChannelData*>(ch)->GetDt();
      }
   }

   if (!dt) {
      return;
   }

   Int_t    frequency = static_cast<Int_t>((1000000 / dt) / 100. + 0.5) * 100;
   ROMEString      path;
   ROMEStr2DArray  values(1, kDRSBins + 1);
   path.SetFormatted("%d/drs_timecalib_%05d_%03dMSPS.db/width",
                     id, fAddress, frequency);
   Report(R_DEBUG, "Reading database %s(type=%s, path=%s)", "drs", "text", path.Data());
   if (!db->Read(&values, path, runNumber, eventNumber)) {
      Report(R_ERROR, "Error in Folder 'DRSChip' Value 'TimeCalibration'. (address=%d)", fAddress);
      return;
   }


   if (!values.GetEntriesFast()) {
      // Calibration file is not prepared for this chip
      return;
   }


   for (Int_t idx = 0; idx < values.GetEntriesFast(); ++idx) {
      if (values.GetEntriesFastAt(idx) != kDRSBins + 1 || !values.At2(idx, 0).Length()) {
         Report(R_ERROR, "Database entry is wrong for DRSChip TimeCalibration (address=%d, %d)", fAddress, idx);
         continue;
      }

      Int_t iChannel =  values.At2(idx, 0).Atoi();

      if (iChannel < 0 || iChannel >= kNumberOfChannelsOnDRSChip) {
         continue;
      }
      auto ch = static_cast<MEGDRSChannelData*>(fChannels->At(iChannel));
      if (ch) {

         fTimeCalibrations[iChannel].resize(2 * kDRSBins);
         for (Int_t iBin = 1; iBin < kDRSBins + 1; iBin++) {
            if (values.At2(idx, iBin).Length() != 0) {
               fTimeCalibrations[iChannel][iBin - 1] = strtod(values.At2(idx, iBin).Data(), 0); // cell width
            } else {
               Report(R_WARNING, "Error in Folder 'DRSChip' Value 'TimeCalibration'.\n"
                      "Missing DB value %s, bin=%d\n"
                      "DB time calibration will not be used.", path.Data(), iBin);
               return;
            }
         }

         // second half is used for rotation
         std::copy(fTimeCalibrations[iChannel].begin(), fTimeCalibrations[iChannel].begin() + kDRSBins,
                   fTimeCalibrations[iChannel].begin() + kDRSBins);

         fTimeCalibrationType[iChannel] = EDRSTimeCalibrationType::kExternal;
         ch->SetTimeCalibrationData(&fTimeCalibrations[iChannel], &fTimeCalibrationType[iChannel]);
      }
   }
}
