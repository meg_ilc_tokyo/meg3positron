/* $Id$ */
/*
 * drived from gsl/wavelet/dwt.c
 * http://www.gnu.org/software/gsl/
 */

/* function dwt_step is based on the public domain function pwt.c
 * available from http://www.numerical-recipes.com
 */

#include <drs_encode/wavelet.h>
#include <stdio.h>

#define ELEMENT(a,stride,i) ((a)[(stride)*(i)])

static void dwt_step(const wavelet *w, double *a, size_t stride, size_t n, wavelet_direction dir,
                     wavelet_workspace *work);

int binary_logn(const size_t n)
{
   size_t ntest;
   size_t logn = 0;
   size_t k = 1;

   while (k < n) {
      k *= 2;
      logn++;
   }

   ntest = (1 << logn);

   if (n != ntest) {
      return -1;                /* n is not a power of 2 */
   }

   return logn;
}

static void dwt_step(const wavelet *w, double *a, size_t stride, size_t n, wavelet_direction dir,
                     wavelet_workspace *work)
{
   double ai, ai1;
   size_t i, ii;
   size_t jf;
   size_t k;
   size_t n1, ni, nh, nmod;

   for (i = 0; i < work->n; i++) {
      work->scratch[i] = 0.0;
   }

   nmod = w->nc * n;
   nmod -= w->offset;           /* center support */

   n1 = n - 1;
   nh = n >> 1;

   if (dir == wavelet_forward) {
      for (ii = 0, i = 0; i < n; i += 2, ii++) {
         ni = i + nmod;
         for (k = 0; k < w->nc; k++) {
            jf = n1 & (ni + k);
            work->scratch[ii] += w->h1[k] * ELEMENT(a, stride, jf);
            work->scratch[ii + nh] += w->g1[k] * ELEMENT(a, stride, jf);
         }
      }
   } else {
      for (ii = 0, i = 0; i < n; i += 2, ii++) {
         ai = ELEMENT(a, stride, ii);
         ai1 = ELEMENT(a, stride, ii + nh);
         ni = i + nmod;
         for (k = 0; k < w->nc; k++) {
            jf = (n1 & (ni + k));
            work->scratch[jf] += (w->h2[k] * ai + w->g2[k] * ai1);
         }
      }
   }

   for (i = 0; i < n; i++) {
      ELEMENT(a, stride, i) = work->scratch[i];
   }
}

int wavelet_transform(const wavelet *w, double *data, size_t stride, size_t n, wavelet_direction dir,
                      wavelet_workspace *work)
{
   size_t i;

   if (work->n < n) {
      printf("not enough workspace provided\n");
   }

   if (binary_logn(n) == -1) {
      printf("n is not a power of 2\n");
   }

   if (n < 2) {
      return WAVELET_SUCCESS;
   }

   if (dir == wavelet_forward) {
      for (i = n; i >= 2; i >>= 1) {
         dwt_step(w, data, stride, i, dir, work);
      }
   } else {
      for (i = 2; i <= n; i <<= 1) {
         dwt_step(w, data, stride, i, dir, work);
      }
   }

   return WAVELET_SUCCESS;
}

int wavelet_transform_forward(const wavelet *w, double *data, size_t stride, size_t n,
                              wavelet_workspace *work)
{
   return wavelet_transform(w, data, stride, n, wavelet_forward, work);
}

int wavelet_transform_inverse(const wavelet *w, double *data, size_t stride, size_t n,
                              wavelet_workspace *work)
{
   return wavelet_transform(w, data, stride, n, wavelet_backward, work);
}
