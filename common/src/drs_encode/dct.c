/* $Id$ */
/* -- Example
   double a[1024];
   double *b;

   ... fill waveform into a

   // initialization
   dct_parameters *param = dct_init_param(64);
   b = new double[DCT_SIZE(1024,64)];

   // DCT transform
   dct_encode(a,b,param,1024,1); // b is filled with transformed data

   ... process b.

   // IDCT transform
   dct_decode(a,b,param,1024,1); // a is filled with reconstructed waveform

   delete [] b;
   dct_free(param);
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "drs_encode/dct.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/*
   allocate memory for "param", then
   initialize parameters from given mcu_size
 */
dct_parameters *dct_init_param(int mcu_size)
{
   int i, j;
   int k = 0;
   dct_parameters *param = (dct_parameters *) malloc(sizeof(dct_parameters));
   param->mcu_size = mcu_size;
   param->coeff = (double *) malloc(mcu_size * mcu_size * sizeof(double));

   for (i = 0; i < mcu_size; i++) {
      for (j = 0; j < mcu_size; j++) {
         param->coeff[k] = sqrt(2. / mcu_size) * cos(((2 * j + 1) * i * M_PI) / (2 * mcu_size));
         if (i == 0) {
            param->coeff[k] /= sqrt(2.);
         }
         k++;
      }
   }
   return param;
}

/* free memory for "param" */
void dct_free_param(dct_parameters *param)
{
   free(param->coeff);
   free(param);
}

/*
  DCT
  double* wf            : waveform
  double* dct           : data after dct
  dct_parameters *param : DCT parameter
  int nbin              : number of bin of waveform
 (int stride            : reserved for future)
  return                : number of bins in dct data
*/
int dct_encode(const double *wf, double *dct, dct_parameters *param, int nbin, int stride)
{
   int n_mcu;
   int i_mcu;
   int i, j, k;
   int mcu_pos = 0;

   n_mcu = nbin / param->mcu_size;
   if (nbin % param->mcu_size != 0) {
      n_mcu++;
   }

   for (i_mcu = 0; i_mcu < n_mcu; i_mcu++) {
      k = 0;
      for (i = 0; i < param->mcu_size; i++) {
         dct[mcu_pos + i] = 0;
         for (j = 0; j < param->mcu_size; j++) {
            if ((mcu_pos + j) < nbin) {
               dct[mcu_pos + i] += wf[mcu_pos + j] * param->coeff[k];
            }
            k++;
         }
      }
      mcu_pos += param->mcu_size;
   }
   return n_mcu * param->mcu_size;
   if (stride) {
      k++;   /* just for suppressing compiler warning */
   }
}

/*
  IDCT
  double* wf  : decoded waveform
  double* dct : encoded dct data
  int nbin    : # of bins in waveform
 (int stride  : reserved for future)
*/
void dct_decode(double *wf, const double *dct, dct_parameters *param, int nbin, int stride)
{
   int n_mcu;
   int i_mcu;
   int i, j;
   int mcu_pos = 0;

   n_mcu = nbin / param->mcu_size;
   if (nbin % param->mcu_size != 0) {
      n_mcu++;
   }

   for (i_mcu = 0; i_mcu < n_mcu; i_mcu++) {
      for (i = 0; i < param->mcu_size; i++) {
         if ((mcu_pos + i) < nbin) {
            wf[mcu_pos + i] = 0;
            for (j = 0; j < param->mcu_size; j++) {
               wf[mcu_pos + i] += dct[mcu_pos + j] * param->coeff[j * param->mcu_size + i];
            }
         }
      }
      mcu_pos += param->mcu_size;
   }
   return;
   if (stride) {
      i++;   /* just for suppressing compiler warning */
   }
}
