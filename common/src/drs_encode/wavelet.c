/* $Id$ */
/*
 * drived from gsl/wavelet/wavelet.c
 * http://www.gnu.org/software/gsl/
 */

/* -- Example
   double a[1024];

   ... fill waveform into a

   // initialization
   wavelet *w = wavelet_alloc(wavelet_haar, wavelet_id_haar, 2);
   wavelet_workspace *work = wavelet_workspace_alloc(1024);

   // transform
   wavelet_transform_forward(w, a, 1, 1024, work);

   ... process b.

   // inverse transform
   wavelet_transform_inverse(w, a, 1, 1024, work);

   wavelet_workspace_free(work);
   wavelet_free(w);
*/

#include <stdlib.h>
#include <stdio.h>
#include "drs_encode/wavelet.h"

wavelet *wavelet_alloc(const wavelet_type *T, short id, size_t k)
{
   int status;

   wavelet *w = (wavelet *) malloc(sizeof(wavelet));

   w->type_id = id;
   w->member = k;

   if (w == NULL) {
      printf("failed to allocate space for wavelet struct\n");
   };

   w->type = T;

   status = (T->init)(&(w->h1), &(w->g1), &(w->h2), &(w->g2), &(w->nc), &(w->offset), k);

   if (status) {
      free(w);
      printf("invalid wavelet member\n");
   }

   return w;
}

void wavelet_free(wavelet *w)
{
   free(w);
}

const char *wavelet_name(const wavelet *w)
{
   return w->type->name;
}

/* Let's not export this for now (BJG) */
#if 0
void wavelet_print(const wavelet *w)
{
   size_t n = w->nc;
   size_t i;

   printf("Wavelet type: %s\n", w->type->name);

   printf(" h1(%d):%12.8f   g1(%d):%12.8f       h2(%d):%12.8f   g2(%d):%12.8f\n", 0, w->h1[0], 0, w->g1[0], 0,
          w->h2[0], 0, w->g2[0]);

   for (i = 1; i < (n < 10 ? n : 10); i++) {
      printf(" h1(%d):%12.8f   g1(%d):%12.8f       h2(%d):%12.8f   g2(%d):%12.8f\n", i, w->h1[i], i, w->g1[i], i,
             w->h2[i], i, w->g2[i]);
   }

   for (; i < n; i++) {
      printf("h1(%d):%12.8f  g1(%d):%12.8f      h2(%d):%12.8f  g2(%d):%12.8f\n", i, w->h1[i], i, w->g1[i], i,
             w->h2[i], i, w->g2[i]);
   }
}
#endif

wavelet_workspace *wavelet_workspace_alloc(size_t n)
{
   wavelet_workspace *work;

   if (n == 0) {
      printf("length n must be positive integer\n");
   }

   work = (wavelet_workspace *) malloc(sizeof(wavelet_workspace));

   if (work == NULL) {
      printf("failed to allocate struct\n");
   }

   work->n = n;
   work->scratch = (double *) malloc(n * sizeof(double));

   if (work->scratch == NULL) {
      /* error in constructor, prevent memory leak */
      free(work);
      printf("failed to allocate scratch space\n");
   }

   return work;
}

void wavelet_workspace_free(wavelet_workspace *work)
{
   /* release scratch space */
   free(work->scratch);
   work->scratch = NULL;
   free(work);
}
