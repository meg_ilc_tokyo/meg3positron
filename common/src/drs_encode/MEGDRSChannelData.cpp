// Author : Ryu Sawada

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// MEGDRSChannelData                                                          //
//                                                                            //
// DRS raw data format.
//                                                                            //
//                                                                            //
/////////////////////////////////////----///////////////////////////////////////

#include <RConfig.h>
#include <zlib.h>
#include <numeric>
#include "drs_encode/MEGDRSChannelData.h"
#include "drs_encode/MEGDRSChipData.h"
#include "drs_encode/drs_encode.h"
#include "ROMEiostream.h"
#include "ROMEDataBase.h"
#include "ROMEStr2DArray.h"
#include "ROMEPrint.h"

ClassImp(MEGDRSChannelData)

using namespace std;
using namespace MEG;
using namespace DRS_ENCODE;
vector<MEGDRSChannelData*> MEGDRSChannelData::fgReferenceChannel(kNReferenceTimeType, 0);
vector<Double_t>           MEGDRSChannelData::fgReferenceTime(kNReferenceTimeType, 0);

namespace
{
const Int_t kRebinEncodeMode = DRSH_ENC_MODE11;
}

//______________________________________________________________________________
MEGDRSChannelData::MEGDRSChannelData(const MEGDRSChannelData &c)
   : TObject(c)
   , fAddress(0)
   , fTriggerCell(0)
   , fVFlags(0)
   , fVDsize(0)
   , fVFirstBin(0)
   , fVData(0)
   , fTFlags(0)
   , fTDsize(0)
   , fTFirstBin(0)
   , fDt(0)
   , fTData(0)
   , fNotDeleteVData(kFALSE)
   , fNotDeleteTData(kFALSE)
   , fMethodMode11(0)
   , fParMode11(new Short_t[kInitialParMode11Size])
   , fTimeCalibMode11(0)
   , fShiftTime(0)
   , fTimeCalibrationData(nullptr)
   , fWaveform(0)
   , fWaveformAllocatedByMyself(kFALSE)
   , fCalibrationStatus(kNotApplied)
{
   MEGDRSChannelData::Class()->IgnoreTObjectStreamer();
#if 0
   if (!dsize) {
      fData = 0;
   } else {
      fData = new char[dsize];
   }
   memset(fParMode11, 0, kInitialParMode11Size * sizeof(Short_t));
#endif
   Report(R_ERROR, "MEGDRSChannelData::MEGDRSChannelData(const MEGDRSChannelData&) is not implemented");
}

//______________________________________________________________________________
MEGDRSChannelData& MEGDRSChannelData::operator=(const MEGDRSChannelData &/* c */)
{
   Report(R_ERROR, "MEGDRSChannelData::operator=(const MEGDRSChannelData&) is not implemented");
   return *this;
}

//______________________________________________________________________________
MEGDRSChannelData::MEGDRSChannelData(Int_t vdsize, Int_t tdsize,
                                     UShort_t vflags, UShort_t tflags,
                                     UShort_t address)
   : TObject()
   , fAddress(address)
   , fTriggerCell(0)
   , fVFlags(vflags)
   , fVDsize(vdsize)
   , fVFirstBin(0)
   , fVData(0)
   , fTFlags(tflags)
   , fTDsize(tdsize)
   , fTFirstBin(0)
   , fDt(0)
   , fTData(0)
   , fNotDeleteVData(kFALSE)
   , fNotDeleteTData(kFALSE)
   , fMethodMode11(0)
   , fParMode11(new Short_t[kInitialParMode11Size])
   , fTimeCalibMode11(0)
   , fShiftTime(0)
   , fTimeCalibrationData(nullptr)
   , fWaveform(0)
   , fWaveformAllocatedByMyself(kFALSE)
   , fCalibrationStatus(kNotApplied)
{
   MEGDRSChannelData::Class()->IgnoreTObjectStreamer();
   if (!vdsize) {
      fVData = 0;
   } else {
      fVData = new char[vdsize];
   }
   if (!tdsize) {
      fTData = 0;
   } else {
      fTData = new char[tdsize];
   }
   memset(fParMode11, 0, kInitialParMode11Size * sizeof(Short_t));
}

//______________________________________________________________________________
void MEGDRSChannelData::Reset()
{
   fAddress                   =  0;
   fTriggerCell               =  0;
   fVFlags                    =  0;
   fVDsize                    =  0;
   fVFirstBin                 =  0;
   fTFlags                    =  0;
   fTDsize                    =  0;
   fTFirstBin                 =  0;
   fDt                        =  500;
   fNotDeleteVData            =  kFALSE;
   fNotDeleteTData            =  kFALSE;
   fMethodMode11              =  0;
   fShiftTime                 =  0;
   fWaveform                  =  0;
   fWaveformAllocatedByMyself =  kFALSE;
   fCalibrationStatus         =  kNotApplied;

   delete [] fVData;
   fVData = 0;
   delete [] fTData;
   fTData = 0;

   // defulat values of arguments for encoding
   if (fParMode11) {
      delete [] fParMode11;
      fParMode11 = 0;
   }
   if (fTimeCalibMode11) {
      delete [] fTimeCalibMode11;
      fTimeCalibMode11 = 0;
   }
}

//______________________________________________________________________________
void MEGDRSChannelData::EncodeV(UShort_t nbin, UShort_t firstBin, const Double_t *v)
{
// Encode "v" with given data and fill "Data" with encoded data.
// Return number of bytes of encoded data
// Return -1 when encode error.
//
// Example.
//   UShort_t encodeMode   = DRSH_ENC_MODE00;           // encoding mode
//   UShort_t DRSVersionBit= DRSH_VERSION4;             // DRS version
//   UShort_t headerFormat = DRSH_FLAG_CHANNEL_HEADER2; // header format
//   UChar_t  crate        = 0;                         // crate number
//   UChar_t  slot         = 0;                         // slot number
//   UChar_t  chip         = 0;                         // chip id on a board
//   UChar_t  channel      = 0;                         // channel on a chip
//   UShort_t nBin         = 1024;                      // number of bins to encode
//   UShort_t firstBin     = 0;                         // index of t to start encoding
//   Double   v[1024];                                  // Voltage array. It must be an array with 1024 elements.
//
//   MEGDRSChannelData *ch = new MEGDRSChannelData(MEGDRSChannelData::GetMaxDataSize(nBin, encodeMode));
//   // fill t [sec] and other parameters(crate, slot....)
//   .
//   .
//   .
//   // encode data.
//   ch->Set(headerFormat, DRSVersionBit, encodeMode, crate, slot, chip, channel);
//   ch->Encode(nBin, firstBin, v);
//
//
// Encode mode specific options have to be set before calling "Encode" method.

   fVFirstBin = firstBin;

   // encode data
   switch (GetVEncodingMode()) {
   case DRSH_ENC_MODE00:
      fVDsize = drs_encode_voltage_mode00(nbin, firstBin, v, fVData);
      break;
   case DRSH_ENC_MODE11:
      fVDsize = drs_encode_voltage_mode11(nbin, firstBin, v, fVData, fMethodMode11, fParMode11, fTimeCalibMode11);
      break;
   default:
      Error("Encode", "unknown encode mode %d", GetVEncodingMode());
   };
   return;
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::DecodeV(Double_t *v, Int_t *rebinSize)
{
// Decode "Data" and fill the result to "v".
// Parameters of encoded data (like encode mode, firstBin...) are read from data member of this object.
// Return number of bins which "v" has.
// Return -1 when decode error.
//
// v must be an array with 1024 elements.
// v will be filled with 0 before firstBin.
// v will be filled with decoded data from firstBin to firstBin+nBin
// v will be filled with 0 after firstBin+nBin.
// nBin is read from encoded data
//
// |0000.......00000XXXXXXXXXXXXXXXXXXXXXXX000000.....00000|
//  ^               ^                      ^              ^
//  |               |                      |              |
//  0               firstBin               firstBin+nBin  1023
//
// Example.
// assuming pointer of MEGDRSChannelData "wf" has already encoded data and header.
//   UShort_t firstBin = 0;             // index of v which corresponds to the first encoded data. This must be in chip header
//   Double v[1024];
//
//   wf->Decode(v);

   switch (GetVEncodingMode()) {
   case DRSH_ENC_MODE00:
      return drs_decode_voltage_mode00(fVDsize, fVFirstBin, fVData, v);
   case DRSH_ENC_MODE11:
      return drs_decode_voltage_mode11(fVDsize, fVFirstBin, fVData, v, rebinSize);
   default:
      Error("Decode", "unknown encode mode %d", GetVEncodingMode());
   };

   return -1;
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::GetMaxVDataSize(Int_t nbin, Int_t encoding_mode)
{
// Return maximum data size with specified encoding mode.
// Concerning each encoding mode, please see drs_encode.c
   switch (encoding_mode) {
   case DRSH_ENC_MODE00:
      return DRS_MAX_ENCODE_VOLTAGE_SIZE_MODE00(nbin);
   case DRSH_ENC_MODE11:
      return DRS_MAX_ENCODE_VOLTAGE_SIZE_MODE11(nbin);
   default:
      cerr << "Error from MEGDRSChannelData::GetMaxDataSize : unknown encode mode " << encoding_mode << endl;
   };
   return 0;
}

//______________________________________________________________________________
void MEGDRSChannelData::EncodeT(UShort_t encoding, UShort_t DRSVersionBit, UShort_t headerFormat,
                                UChar_t crate, UChar_t slot, UChar_t chip, UChar_t channel,
                                UShort_t nbin, UShort_t dt, UShort_t timeFirstBin, UShort_t triggerCell, const Double_t *t)
{
   SetTFlags(encoding | DRSVersionBit | headerFormat);
   UShort_t address = 0;
   drs_channelt_insert_crate(&address, crate, drs_versionbit_to_version(DRSVersionBit));
   drs_channelt_insert_slot(&address, slot, drs_versionbit_to_version(DRSVersionBit));
   drs_channelt_insert_chip(&address, chip, drs_versionbit_to_version(DRSVersionBit));
   drs_channelt_insert_channel(&address, channel, drs_versionbit_to_version(DRSVersionBit));
   SetAddress(address);
   EncodeT(nbin, dt, timeFirstBin, triggerCell, t);
}

//______________________________________________________________________________
void MEGDRSChannelData::EncodeT(UShort_t nbin, UShort_t dt, UShort_t firstBin, UShort_t triggerCell,
                                const Double_t *t)
{
// Encode "t" with given data and fill "TimeData" with encoded data.
// Return number of bytes of encoded data
// Return -1 when encode error.
//
// Example.
//   UShort_t encodeMode   = DRSH_ENC_MODE00;       // encoding mode
//   UShort_t DRSVersion   = DRSH_VERSION4;         // DRS version
//   UShort_t headerFormat = DRSH_FLAG_CHIP_HEADER; // header format
//   UChar_t  crate        = 0;                     // crate number
//   UChar_t  slot         = 0;                     // slot number
//   UChar_t  chip         = 0;                     // chip id on a mezzanine card
//   UShort_t nBin         = 1024;                  // number of bins to encode
//   UShort_t dt           = 500;                   // nominal bin width in [psec]
//   UShort_t firstBin     = 0;                     // index of t to start encoding
//   UShort_t triggerCell  = 0;                     // DRS cell number corresponds to 0th bin
//   Double   t[1024];                              // Time array. It must be an array with 1024 elements.
//
//   MEGDRSChipData *chip = new MEGDRSChipData(MEGDRSChipData::GetMaxDataSize(nBin, encodeMode));
//   // fill t [sec] and other parameters(crate, slot....)
//   .
//   .
//   .
//   // encode data.
//   chip->Set(headerFormat, DRSVersion, encodeMode, crate, slot, chip);
//   chip->EncodeTime(nBin, dt, firstBin, triggerCell, t);
//
//
// Encode mode specific options have to be set before calling "EncodeT" method.

   // set headers
   SetDt(dt);
   SetTFirstBin(firstBin);
   SetTriggerCell(triggerCell);

   // encode data
   switch (GetTEncodingMode()) {
   case DRSH_ENC_MODE00:
      fTDsize = drs_encode_time_mode00(nbin, dt, firstBin, t, fTData);
      break;
   case DRSH_ENC_MODE08:
      fTDsize = drs_encode_time_mode08(nbin, firstBin, t, fTData);
      break;
   case DRSH_ENC_MODE09:
      fTDsize = drs_encode_time_mode09(nbin, firstBin, t, fTData);
      break;
   default:
      Error("EncodeTime", "unknown encode mode %d", GetTEncodingMode());
   };
   return;
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::DecodeT(Double_t *t)
{
// Decode "TimeData" and fill the result to "t".
// Parameters of encoded data (like encode mode, firstBin...) are read from data member of this object.
// Return number of bins which "t" has.
// Return -1 when decode error.
//
// t must be an array with 1024 elements.
// t will be filled with 0 before firstBin.
// t will be filled with decoded data from firstBin to firstBin+nBin
// t will be filled with 0 after firstBin+nBin.
// nBin is read from encoded data
//
// |0000.......00000XXXXXXXXXXXXXXXXXXXXXXX000000.....00000|
//  ^               ^                      ^              ^
//  |               |                      |              |
//  0               firstBin               firstBin+nBin  1023
//
// Example.
// assuming pointer of MEGDRSChipData "wf" has already encoded data and header.
//   Double t[1024];   // Time array. It must be an array with 1024 elements.
//   wf->DecodeT(t);

   switch (GetTEncodingMode()) {
   case DRSH_ENC_MODE00:
      return drs_decode_time_mode00(fTDsize, fDt, fTFirstBin, fTData, t);
   case DRSH_ENC_MODE08:
      return drs_decode_time_mode08(fTDsize, fTFirstBin, fTData, t);
   case DRSH_ENC_MODE09:
      return drs_decode_time_mode09(fTDsize, fTFirstBin, fTData, t);
   default:
      Error("DecodeT", "unknown encode mode %d", GetTEncodingMode());
   };

   return -1;
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::GetMaxTDataSize(Int_t nbin, Int_t encode_mode)
{
// Return maximum data size with specified encoding mode.
// Concerning each encoding mode, please see drs_encode.c
   switch (encode_mode) {
   case DRSH_ENC_MODE00:
      return DRS_MAX_ENCODE_TIME_SIZE_MODE00(nbin);
   case DRSH_ENC_MODE08:
      return DRS_MAX_ENCODE_TIME_SIZE_MODE08(nbin);
   case DRSH_ENC_MODE09:
      return DRS_MAX_ENCODE_TIME_SIZE_MODE09(nbin);
   default:
      cerr << "Error from MEGDRSChipData::GetMaxTimeDataSize : unknown encode mode " << encode_mode << endl;
   };
   return 0;
}

//______________________________________________________________________________
void MEGDRSChannelData::DecodeWave()
{
   Int_t           iBin;
   Double_t        drsTime[kDRSBins];
   Double_t        drsVoltage[kDRSBins];
   Double_t        drsTimeForRebin[kDRSBins];
   Double_t        drsVoltageForRebin[kDRSBins];
   MEGDRSWaveform *wf;

   fRebinSize[0]   = 0;
   Int_t nVoltagePoints   = DecodeV(drsVoltage, fRebinSize);
   Int_t channelVFirstBin = GetVFirstBin();
   Int_t nTimePoints      = DecodeT(drsTime);

   Bool_t hasDatabaseTimeCalibration = fTimeCalibrationData && fTimeCalibrationData->size()
                                       && (*fTimeCalibrationType == EDRSTimeCalibrationType::kExternal);
   if ((hasDatabaseTimeCalibration || !nTimePoints) && nVoltagePoints) {
      if (!fTimeCalibrationData->size()) {
         // set default calibration
         fTimeCalibrationData->resize(2 * kDRSBins);
         fill(fTimeCalibrationData->begin(), fTimeCalibrationData->end(), fDt * picosecond);
         *fTimeCalibrationType = EDRSTimeCalibrationType::kFixedInterval;
      }
      // use own calibration
      drsTime[kDRSBins - 1] = 0;
      for (iBin = 1; iBin < kDRSBins; iBin++) {
         drsTime[kDRSBins - 1 - iBin] = drsTime[kDRSBins - iBin] - (*fTimeCalibrationData)[fTriggerCell + kDRSBins - 1
                                        - iBin];
      }
   } else if (!hasDatabaseTimeCalibration && nTimePoints) {
      // check which is written in the array, time array or calibration (=cell widths)
      Bool_t isCalibration = kFALSE;
      for (iBin = 1; iBin < nTimePoints; iBin++) {
         if (drsTime[iBin] - drsTime[iBin - 1] < 0) {
            isCalibration = kTRUE;
            break;
         }
      }

      if (isCalibration) {
         if (nTimePoints != kDRSBins) {
            Report(R_ERROR, "Time calibration requires %d numbers", kDRSBins);
            abort();
         }
         // copy the time calibration of the full time window
         fTimeCalibrationData->resize(2 * kDRSBins);
         copy(drsTime, drsTime + kDRSBins, fTimeCalibrationData->begin());
         copy(drsTime, drsTime + kDRSBins, fTimeCalibrationData->begin() + kDRSBins);

         drsTime[kDRSBins - 1] = 0;
         for (iBin = 1; iBin < kDRSBins; iBin++) {
            drsTime[kDRSBins - 1 - iBin] = drsTime[kDRSBins - iBin] - (*fTimeCalibrationData)[fTriggerCell + kDRSBins - 1
                                           - iBin];
         }
      } else {
         // decoded time is used
#if 0
         // Align at the last time
         Double_t endTime = drsTime[kDRSBins - 1];
         for (iBin = 0; iBin < kDRSBins; iBin++) {
            drsTime[iBin] -= endTime;
         }
#endif
      }
      *fTimeCalibrationType = EDRSTimeCalibrationType::kInData;
   }
   if (!fWaveform) {
      fWaveform = new MEGDRSWaveform(kDRSBins, fDt * picosecond);
      fWaveformAllocatedByMyself = kTRUE;
   }
   wf = fWaveform;

   if (TMath::Abs(wf->GetBinSize() - fDt * picosecond) > 0.5 * picosecond) {
      // bin size of the waveform has not been set, or there is a difference with raw data.
      wf->SetBinSize(fDt * picosecond);
      wf->SetFixBinSize(kFALSE);
   }

   if (!nVoltagePoints) {
      wf->SetNPoints(0);
   } else {
      // fill waveform
      if (GetVEncodingMode() == kRebinEncodeMode) {
         // decode re-binning
         nVoltagePoints = decode_rebin(fRebinSize, drsTime + channelVFirstBin, drsTimeForRebin,
                                       drsVoltage + channelVFirstBin, drsVoltageForRebin);
         wf->SetNPoints(nVoltagePoints);
         wf->SetTime(drsTimeForRebin);
         wf->SetAmplitude(drsVoltageForRebin);
      } else {
         wf->SetNPoints(nVoltagePoints);
         wf->SetTime(drsTime + channelVFirstBin);
         wf->SetAmplitude(drsVoltage + channelVFirstBin);
      }
   }

   if (fTriggerCell >= kDRSBins) {
      Report(R_WARNING, "Wrong trigger cell (%d). Set to 0. (address=%d)", fTriggerCell, fAddress);
      fTriggerCell = 0;
   }
   wf->SetStartCell(fTriggerCell);
}



//______________________________________________________________________________
void MEGDRSChannelData::CalibrateWave(Int_t method)
{
   // Initialize
   fCalibrationStatus = kNotApplied;

   if (!fWaveform->GetNPoints()) {
      return;
   }

   switch (method) {
   case 0:                     // do nothing
      fShiftTime = 0;
      break;
   case 1:
      TimeZeroSync();
      break;
   case 2:
      ApplyShiftTime();
      break;
   default:
      Error("CalibrateWave", "unknown method %d", method);
   }
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::ApplyShiftTime()
{
   // Shift waveforms by fShiftTime
   // This method is to be used eg. in the second process in which the input data (raw.root)
   // have already calculated fShiftTime.

   if (!fWaveform->GetNPoints()) {
      return kNotApplied;
   }
   // Set time
   transform(fWaveform->GetTime(), fWaveform->GetTime() + fWaveform->GetNPoints(),
   fWaveform->GetTime(), [&](Double_t &t) {
      return t + fShiftTime;
   });

   fCalibrationStatus = kShiftTimeApplied;

   return kShiftTimeApplied;
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::TimeZeroSync()
{
// For DRS4
// Time zero is the time of the first cell.
// Shift waveforms respect to time reference.
// Time reference is determined with the time zero of first chip in a event.

   if (!fWaveform->GetNPoints()) {
      return kNotApplied;
   }

   Int_t referenceTimeType = GetTimeZeroEnum(fDt);

   // Get time of the first cell
   Double_t timeZero;
   Double_t halfWindow, fullWindow;
   if (fTimeCalibrationData->empty()) {
      if (fTriggerCell == 0) {
         timeZero = -(kDRSBins - 1) * (fDt * picosecond);
      } else {
         timeZero = -(fTriggerCell - 1) * (fDt * picosecond);
      }
      halfWindow = (kDRSBins / 2) * fDt * picosecond;
      fullWindow = (kDRSBins) * fDt * picosecond;
   } else {
      vector<Double_t>::iterator tcbegin = fTimeCalibrationData->begin();
      if (fTriggerCell == 0) {
         timeZero = -accumulate(tcbegin,            tcbegin + (kDRSBins - 1),                0.);
      } else {
         timeZero = -accumulate(tcbegin + kDRSBins, tcbegin + (kDRSBins - 1 + fTriggerCell), 0.);
      }

      halfWindow = accumulate(tcbegin, tcbegin + kDRSBins / 2, 0.);
      fullWindow = accumulate(tcbegin, tcbegin + kDRSBins,   0.);
   }

   static Int_t ich(0);
   Double_t shiftTime(0);
   if (!fgReferenceChannel[referenceTimeType]) {      // 1st chip in an event
      // Set time reference of whole experiment
      fgReferenceTime[referenceTimeType]    = timeZero;
      fgReferenceChannel[referenceTimeType] = this;

      ich = 0;
   } else if (fgReferenceChannel[referenceTimeType] != this) {
      ich++;
      Int_t refLastCell = (fgReferenceChannel[referenceTimeType]->GetTriggerCell() - 1 + kDRSBins) % kDRSBins;
      Int_t thisLastCell = (fTriggerCell - 1 + kDRSBins) % kDRSBins;

      if (thisLastCell - refLastCell > kDRSBins / 4) {
         if (fTimeCalibrationData->empty()) {
            timeZero += kDRSBins * (fDt * picosecond);
         } else {
            vector<Double_t>::iterator tcbegin = fTimeCalibrationData->begin();
            //timeZero += accumulate(tcbegin,  tcbegin + kDRSBins, 0.);
            timeZero = accumulate(tcbegin + thisLastCell, tcbegin + kDRSBins, 0.);
         }
      } else if (thisLastCell - refLastCell < -kDRSBins / 4) {
         if (fTimeCalibrationData->empty()) {
            timeZero -= kDRSBins * (fDt * picosecond);
         } else {
            vector<Double_t>::iterator tcbegin = fTimeCalibrationData->begin();
            timeZero -= accumulate(tcbegin, tcbegin + kDRSBins, 0.);
         }
      }

      shiftTime = timeZero - fgReferenceTime[referenceTimeType];

      // Set time
      transform(fWaveform->GetTime(), fWaveform->GetTime() + fWaveform->GetNPoints(),
      fWaveform->GetTime(), [&](Double_t &t) {
         return t - shiftTime;
      });
   }

   fCalibrationStatus = kTimeZeroMethod;
   fShiftTime = -shiftTime;
   return kTimeZeroMethod;
}

//______________________________________________________________________________
Int_t MEGDRSChannelData::GetTimeZeroEnum(Int_t dtPico)
{
   if (dtPico == 1250) {
      return kTimeZero800;
   }
   if (dtPico ==  625) {
      return kTimeZero1600;
   }
   if (dtPico ==  833) {
      return kTimeZero1200;
   }
   if (dtPico ==  500) {
      return kTimeZero2000;
   }

   if (dtPico > 1850) {
      return kTimeZero500;
   } else if (dtPico > 1450) {
      return kTimeZero600;
   } else if (dtPico > 1350) {
      return kTimeZero700;
   } else if (dtPico > 1180) {
      return kTimeZero800;
   } else if (dtPico > 1050) {
      return kTimeZero900;
   } else if (dtPico >  950) {
      return kTimeZero1000;
   } else if (dtPico >  870) {
      return kTimeZero1100;
   } else if (dtPico >  800) {
      return kTimeZero1200;
   } else if (dtPico >  745) {
      return kTimeZero1300;
   } else if (dtPico >  690) {
      return kTimeZero1400;
   } else if (dtPico >  645) {
      return kTimeZero1500;
   } else if (dtPico >  605) {
      return kTimeZero1600;
   } else if (dtPico >  560) {
      return kTimeZero1700;
   } else if (dtPico >  540) {
      return kTimeZero1800;
   } else if (dtPico >  513) {
      return kTimeZero1900;
   } else                    {
      return kTimeZero2000;
   }
}
