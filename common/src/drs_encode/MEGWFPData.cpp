// $Id$
// Author : Ryu Sawada

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// MEGWFPData                                                                 //
//                                                                            //
// WFP raw data format.
//                                                                            //
//                                                                            //
/////////////////////////////////////----///////////////////////////////////////

#include <RConfig.h>
#include <zlib.h>
#include "drs_encode/MEGWFPData.h"
#include "drs_encode/wfp_encode.h"
#include "ROMEiostream.h"
#include "ROMEAnalyzer.h"
#include "ROMEPrint.h"

ClassImp(MEGWFPData)

//______________________________________________________________________________
MEGWFPData::MEGWFPData(const MEGWFPData &c)
   : TObject(c)
   , fFlags(0)
   , fDsize(0)
   , fData(0)
   , fValue(0)
#if defined( R__VISUAL_CPLUSPLUS )
   , fNotDeleteData(kTRUE)
#else
   , fNotDeleteData(kFALSE)
#endif
{
   MEGWFPData::Class()->IgnoreTObjectStreamer();
#if 0
   if (!dsize) {
      fData = 0;
   } else {
      fData = new char[dsize];
   }
#endif
   Report(R_ERROR, "MEGWFPData::MEGWFPData(const MEGWFPData&) is not implemented");
}

//______________________________________________________________________________
MEGWFPData& MEGWFPData::operator=(const MEGWFPData &/* c */)
{
   Report(R_ERROR, "MEGWFPData::operator=(const MEGWFPData&) is not implemented");
   return *this;
}

//______________________________________________________________________________
MEGWFPData::MEGWFPData(Int_t dsize, UShort_t flags)
   : TObject()
   , fFlags(flags)
   , fDsize(dsize)
   , fData(0)
   , fValue(0)
#if defined( R__VISUAL_CPLUSPLUS )
   , fNotDeleteData(kTRUE)
#else
   , fNotDeleteData(kFALSE)
#endif
{
   MEGWFPData::Class()->IgnoreTObjectStreamer();
   if (!dsize) {
      fData = 0;
   } else {
      fData = new char[dsize];
   }
}

//______________________________________________________________________________
void MEGWFPData::Reset()
{
   fFlags = 0;
   fDsize = 0;
   DeleteData();
   fData = 0;
   fValue = 0;
}

//______________________________________________________________________________
void MEGWFPData::Encode(Double_t data)
{
   // encode data
   switch (GetDataKind()) {
   case WFPH_CHARGE_INTEGRATION:
      fDsize = WFP_ENCODE::wfp_encode_charge_integration(fData, data, fFlags);
      break;
   case WFPH_BASELINE:
      fDsize = WFP_ENCODE::wfp_encode_baseline(fData, data, fFlags);
      break;
   default:
      fDsize = 0;
      Error("Encode", "unknown encode mode %d", GetDataKind());
   };
   return;
}

//______________________________________________________________________________
Int_t MEGWFPData::Decode()
{
   switch (GetDataKind()) {
   case WFPH_CHARGE_INTEGRATION:
      return WFP_ENCODE::wfp_decode_charge_integration(&fValue, fData, fFlags);
   case WFPH_BASELINE:
      return WFP_ENCODE::wfp_decode_baseline(&fValue, fData, fFlags);
   default:
      Error("Decode", "unknown encode mode %d", GetDataKind());
   };

   return -1;
}

//______________________________________________________________________________
Int_t MEGWFPData::Encode()
{
   // encode fValue and write fData
   // fData must be allocated before calling this function
   switch (GetDataKind()) {
   case WFPH_CHARGE_INTEGRATION:
      return WFP_ENCODE::wfp_encode_charge_integration(fData, fValue, fFlags);
   case WFPH_BASELINE:
      return WFP_ENCODE::wfp_encode_baseline(fData, fValue, fFlags);
   default:
      Error("Decode", "unknown encode mode %d", GetDataKind());
   };

   return -1;
}
