// $Id$
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <zlib.h>
#include <string.h>
#include <iostream>
#include <algorithm>

#include "drs_encode/wfp_encode.h"
#include "constants/drs/drsconst.h"

namespace WFP_ENCODE
{
void byte_swap_wfp_channel_header(WFP_CHANNEL_HEADER *header);
void byte_swap_wfp_data_header(WFP_DATA_HEADER *header);
}

using namespace std;
using namespace MEG;
using namespace DRS_ENCODE;
using namespace WFP_ENCODE;

//______________________________________________________________________________
bool WFP_ENCODE::CheckWFPHeaderSize()
{
   if (sizeof(WFP_CHANNEL_HEADER) != 2 ||
       sizeof(WFP_DATA_HEADER)    != 2) {
      cerr << "WFP header size is not correct, probably due to padding." << endl <<
           "Please replace compiler or use proper compile option." << endl;
      return false;
   }
   return true;
}

/*******************************************************************************
 * Compose waveform channel header
 *
 * Input
 *   crate       VME crate number
 *   slot        VME location 2..41
 *               (slot/2=VME slot, slot%2=mezzanine number 0(upper)/1(lower)
 *   drs_chip    Number of WFP chip on mezzanine 0..1
 *   drs_channel Number of WFP channel 0..8 (8=clock channel)
 * Ouput
 *   header at address "pdata"
 *******************************************************************************/

//______________________________________________________________________________
void WFP_ENCODE::wfp_compose_channel_header(void *pdata,
                                            unsigned char crate,
                                            unsigned char slot,
                                            unsigned char wfp_chip,
                                            unsigned char wfp_channel,
                                            unsigned short version)
{
   WFP_CHANNEL_HEADER *ph = static_cast<WFP_CHANNEL_HEADER*>(pdata);
   unsigned short address = 0;
   wfp_channel_insert_crate(&address, crate, version);
   wfp_channel_insert_slot(&address, slot, version);
   wfp_channel_insert_chip(&address, wfp_chip, version);
   wfp_channel_insert_channel(&address, wfp_channel, version);
   ph->address = address;
   wfp_set_channel_header(pdata, 1);
}

/*******************************************************************************
 * Compose waveform data header
 *
 * Input
 *   flags         0-2: header fmt, 3-4: drs version, 5-10: data kind,
 *                 11-14: parameters, 15: header big=1
 * Ouput
 *   header at address "pdata"
 *******************************************************************************/

//______________________________________________________________________________
void WFP_ENCODE::wfp_compose_data_header(void *pdata,
                                         unsigned short header_type,
                                         unsigned short drs_versionbit,
                                         unsigned short data_kind,
                                         unsigned short parameters)
{
   WFP_DATA_HEADER *ph = static_cast<WFP_DATA_HEADER*>(pdata);

   ph->flags = parameters | data_kind | drs_versionbit | header_type;
   wfp_set_channel_header(pdata, 0);
}

/*******************************************************************************
 * Byte swap WFP header
 *
 * Input
 *   header      pointer to WFP header
 *******************************************************************************/
//______________________________________________________________________________
void WFP_ENCODE::byte_swap_wfp_channel_header(WFP_CHANNEL_HEADER *header)
{
   drs_word_swap(&(header->address));
}

//______________________________________________________________________________
void WFP_ENCODE::byte_swap_wfp_data_header(WFP_DATA_HEADER *header)
{
   drs_word_swap(&(header->flags));
}

//______________________________________________________________________________
int WFP_ENCODE::byte_swap_wfp_header(void *header)
{
   // swap WFP channel or data header.
   // return -1, when channel header
   // return header type, when data header
   unsigned short address = static_cast<WFP_CHANNEL_HEADER*>(header)->address;
   drs_word_swap(&address);

   if (wfp_is_channel_header(&address)) {
      byte_swap_wfp_channel_header(static_cast<WFP_CHANNEL_HEADER*>(header));
      return -1;
   } else {
      byte_swap_wfp_data_header(static_cast<WFP_DATA_HEADER*>(header));
      return wfp_data_get_header_type(header);
   }
}

//______________________________________________________________________________
void WFP_ENCODE::byte_swap_wfp_bank(void* p, size_t length)
{
   size_t i = 0;
   char *pc = static_cast<char*>(p);

   while (i < length) {
      if (byte_swap_wfp_header(pc + i) == -1) {
         // channel header
         i += sizeof(WFP_CHANNEL_HEADER);
      } else {
         // data header
         i += sizeof(WFP_DATA_HEADER);
         // data (currently, all data is 2 byte)
         if (1) {
            drs_word_swap(pc + i);
            i += 2;
         } else {
            drs_dword_swap(pc + i);
            i += 4;
         }
      }
   }
}

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_read_value(const void* p,
                                  double *data,
                                  unsigned short *address,
                                  void *data_header,
                                  int &address_old)
{
// Return number of bytes successfully read.
// Return -1 when error
//
// When the data start from data header. This function assumes, address is not
// changed from the previous call.

   if (!p) {
      address_old = -1;
      return 0;
   }

   size_t n = 0;
   union {
      // use union not to break strict-aliasing rule
      const char               *un_char;
      const WFP_CHANNEL_HEADER *un_ch_header;
      const WFP_DATA_HEADER    *un_data_header;
   };
   un_char = static_cast<const char*>(p);

   // read channel header if it exists
   while (wfp_is_channel_header(un_char)) {
      // channel header
      address_old = un_ch_header->address;
      n += sizeof(WFP_CHANNEL_HEADER);
      un_char +=  sizeof(WFP_CHANNEL_HEADER);
   }

   if (address_old == -1) {
      cerr << "Error from WFP_ENCODE::wfp_read_value : missing channel header" <<
           endl;
      return 0;
   }

   // read data header
   if (address) {
      *address = address_old;
   }
   if (data_header) {
      switch (wfp_data_get_header_type(un_data_header)) {
      case WFPH_FLAG_DATA_HEADER_V0:
         memcpy(data_header, un_data_header, sizeof(WFP_DATA_HEADER));
         break;
      default:
         cerr << "Error from 'WFP_ENCODE::wfp_read_value' : unknown data header type " <<
              wfp_data_get_header_type(un_data_header) << endl;
         return 0;
      }
   }
   n += sizeof(WFP_DATA_HEADER);

   // decode data
   switch (wfp_data_extract_data_kind(un_data_header->flags)) {
   case WFPH_CHARGE_INTEGRATION:
      n += wfp_decode_charge_integration(data,
                                         un_char + sizeof(WFP_DATA_HEADER),
                                         un_data_header->flags);
      break;
   case WFPH_BASELINE:
      n += wfp_decode_baseline(data, un_char + sizeof(WFP_DATA_HEADER),
                               un_data_header->flags);
      break;
   default:
      cerr << "Error from 'WFP_ENCODE::wfp_next_value' : unknown data_kind " <<
           wfp_data_extract_data_kind(un_data_header->flags) << endl;
      return 0;
   }

   return n;
}

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_get_raw_data_pointer(void* p,
                                            char **data,
                                            unsigned short *address,
                                            void *data_header,
                                            int &address_old)
{
// Return number of bytes successfully read.
// Return -1 when error
//
// When the data start from data header. This function assumes, address is not
// changed from the previous call.

   if (!p) {
      address_old = -1;
      return 0;
   }

   size_t n = 0;
   union {
      // use union not to break strict-aliasing rule
      char               *un_char;
      WFP_CHANNEL_HEADER *un_ch_header;
      WFP_DATA_HEADER    *un_data_header;
   };
   un_char = static_cast<char*>(p);

   // read channel header if it exists
   while (wfp_is_channel_header(un_char)) {
      // channel header
      address_old = un_ch_header->address;
      n += sizeof(WFP_CHANNEL_HEADER);
      un_char +=  sizeof(WFP_CHANNEL_HEADER);
   }

   if (address_old == -1) {
      cerr << "Error from WFP_ENCODE::wfp_read_value : missing channel header" <<
           endl;
      return 0;
   }

   // read data header
   if (address) {
      *address = address_old;
   }
   if (data_header) {
      switch (wfp_data_get_header_type(un_data_header)) {
      case WFPH_FLAG_DATA_HEADER_V0:
         memcpy(data_header, un_data_header, sizeof(WFP_DATA_HEADER));
         break;
      default:
         cerr << "Error from 'WFP_ENCODE::wfp_read_value' : unknown data header type " <<
              wfp_data_get_header_type(un_data_header) << endl;
         return 0;
      }
   }
   n += sizeof(WFP_DATA_HEADER);

   // set raw data pointer
   switch (wfp_data_extract_data_kind(un_data_header->flags)) {
   case WFPH_CHARGE_INTEGRATION:
      *data = un_char + sizeof(WFP_DATA_HEADER);
      n += 2;
      break;
   case WFPH_BASELINE:
      *data = un_char + sizeof(WFP_DATA_HEADER);
      n += 2;
      break;
   default:
      cerr << "Error from 'WFP_ENCODE::wfp_next_value' : unknown data_kind " <<
           wfp_data_extract_data_kind(un_data_header->flags) << endl;
      return 0;
   }

   return n;
}

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_write_value(void* p,
                                   double data,
                                   unsigned short address,
                                   const void* data_header,
                                   int &address_old)
{
// return number of bytes successfully write.
// return -1 when error
//
// When an argument 'address' is same as the previous call, this does not write
// channel header.

   if (!p) {
      address_old = -1;
      return 0;
   }

   size_t n = 0;
   union {
      // use union not to break strict-aliasing rule
      char               *un_char;
      WFP_CHANNEL_HEADER *un_ch_header;
      WFP_DATA_HEADER    *un_data_header;
   };
   un_char = static_cast<char*>(p);

   // write channel header if 'address' is different from previous call.
   if (address != address_old) {
      address_old = address;
      un_ch_header->address = address & 0x7FFF;
      n += sizeof(WFP_CHANNEL_HEADER);
      un_char +=  sizeof(WFP_CHANNEL_HEADER);
   }

   // write data header
   unsigned short flags;
   switch (wfp_data_get_header_type(data_header)) {
   case WFPH_FLAG_DATA_HEADER_V0:
      memcpy(un_data_header, data_header, sizeof(WFP_DATA_HEADER));
      un_char +=  sizeof(WFP_DATA_HEADER);
      n += sizeof(WFP_DATA_HEADER);
      flags = static_cast<const WFP_DATA_HEADER*>(data_header)->flags;
      break;
   default:
      cerr << "Error from 'WFP_ENCODE::wfp_write_value' : unknown data header type " <<
           wfp_data_get_header_type(data_header) << endl;
      return 0;
   }

   // write data
   switch (wfp_data_extract_data_kind(flags)) {
   case WFPH_CHARGE_INTEGRATION:
      n += wfp_encode_charge_integration(un_char, data, flags);
      break;
   case WFPH_BASELINE:
      n += wfp_encode_baseline(un_char, data, flags);
      break;
   default:
      cerr << "Error from 'WFP_ENCODE::wfp_write_value' : unknown data_kind " <<
           wfp_data_extract_data_kind(flags) << endl;
      return 0;
   }

   return n;
}

/*******************************************************************************
 * WFP Data Kind - 00 (Charge integration)
 * charge is stored in short int
 *
 * Pricisions
 *
 * --------------------------------------------------------------------------
 * precision | dynamic rage(-6.25 to 93.75%) | maximum allowed base shift
 * --------------------------------------------------------------------------
 * 0.005 pQ  | ~ 33 mV(2GHz), ~  8 mV(.5GHz) | ~  2 mV(2GHz), ~0.5 mV(.5GHz)
 * 0.025 pQ  | ~160 mV(2GGz), ~ 40 mV(.5GHz) | ~ 10 mV(2GHz), ~2.5 mV(.5GHz)
 * 0.2   pQ  | ~1.3  V(2GHz), ~330 mV(.5GHz) | ~ 80 mV(2GHz), ~ 20 mV(.5GHz)
 * 1     pQ  | ~6.5  V(2GHz), ~1.6  V(.5GHz) | ~0.4  V(2GHz), ~0.1  V(.5GHz)
 * --------------------------------------------------------------------------
 *
 * Parameters
 * 10-11 bit in 'flags' : Mode (precision)
 *
 *******************************************************************************/
const double kChargeIntegrationCFMode00 = 0.005 * picocoulomb;
const double kChargeIntegrationCFMode01 = 0.025 * picocoulomb;
const double kChargeIntegrationCFMode02 = 0.2   * picocoulomb;
const double kChargeIntegrationCFMode03 = 1.0   * picocoulomb;

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_encode_charge_integration(void* p, double data,
                                                 unsigned short flags)
{
   if (!p) {
      return 0;
   }

   switch (wfp_extract_charge_integration_mode(flags)) {
   case WFPH_CHARGE_INTEGRATION_MODE00:
      data *= (1 / kChargeIntegrationCFMode00);
      break;
   case WFPH_CHARGE_INTEGRATION_MODE01:
      data *= (1 / kChargeIntegrationCFMode01);
      break;
   case WFPH_CHARGE_INTEGRATION_MODE02:
      data *= (1 / kChargeIntegrationCFMode02);
      break;
   case WFPH_CHARGE_INTEGRATION_MODE03:
      data *= (1 / kChargeIntegrationCFMode03);
      break;
   default:
      cerr << "Error from WFP_ENCODE::wfp_encode_charge_integration : " <<
           "unknown precision mode " <<
           wfp_extract_charge_integration_mode(flags) << endl;
      return 0;
   }

   // offset
   data += kChargeIntegrationOffset;

   short sdata = max(0, min(kWFPMaxUShort, DRS_NINT(data)));
   char *pc = static_cast<char*>(p);
   pc[0] = sdata & 0x00FF;
   pc[1] = (sdata & 0xFF00) >> 8;

   return 2;
}

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_decode_charge_integration(double *data, const void *p,
                                                 unsigned short flags)
{
   if (!p || !data) {
      return 0;
   }

   const char* pc = static_cast<const char*>(p);
   short sdata;
   memcpy(&sdata, pc, sizeof(short));
#ifndef DRS_LITTLE_ENDIAN
   drs_word_swap(&sdata);
#endif

   // offset
   *data = sdata - kChargeIntegrationOffset;

   switch (wfp_extract_charge_integration_mode(flags)) {
   case WFPH_CHARGE_INTEGRATION_MODE00:
      *data *= kChargeIntegrationCFMode00;
      break;
   case WFPH_CHARGE_INTEGRATION_MODE01:
      *data *= kChargeIntegrationCFMode01;
      break;
   case WFPH_CHARGE_INTEGRATION_MODE02:
      *data *= kChargeIntegrationCFMode02;
      break;
   case WFPH_CHARGE_INTEGRATION_MODE03:
      *data *= kChargeIntegrationCFMode03;
      break;
   default:
      cerr << "Error from WFP_ENCODE::wfp_encode_charge_integration : " <<
           "unknown precision mode " <<
           wfp_extract_charge_integration_mode(flags) << endl;
      return 0;
   }

   return 2;
}

/*******************************************************************************
 * WFP Data Kind - 01 (Baseline)
 * voltage is stored in short int
 *
 * Pricisions
 *
 * --------------------------------------------------------------------------
 * precision | dynamic rage(-50 to +50%)
 * --------------------------------------------------------------------------
 * 0.001 mV  | - 32.767 mV, 32.767 mV
 * --------------------------------------------------------------------------
 * 0.01 mV   | - 327.67 mV, 327.67 mV
 * --------------------------------------------------------------------------
 *
 * Parameters
 * 10-11 bit in 'flags' : Mode (precision)
 *
 *******************************************************************************/
const double kBaselineCFMode00 = 0.001 * millivolt;
const double kBaselineCFMode01 = 0.01  * millivolt;

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_encode_baseline(void* p, double data,
                                       unsigned short flags)
{
   if (!p) {
      return 0;
   }

   switch (wfp_extract_baseline_mode(flags)) {
   case WFPH_BASELINE_MODE00:
      data *= (1 / kBaselineCFMode00);
      break;
   case WFPH_BASELINE_MODE01:
      data *= (1 / kBaselineCFMode01);
      break;
   default:
      cerr << "Error from WFP_ENCODE::wfp_encode_baseline : " <<
           "unknown precision mode " <<
           wfp_extract_baseline_mode(flags) << endl;
      return 0;
   }

   short sdata = max(kWFPMinShort, min(kWFPMaxShort, DRS_NINT(data)));
   char *pc = static_cast<char*>(p);
   pc[0] = sdata & 0x00FF;
   pc[1] = (sdata & 0xFF00) >> 8;

   return 2;
}

//______________________________________________________________________________
size_t WFP_ENCODE::wfp_decode_baseline(double *data, const void *p,
                                       unsigned short flags)
{
   if (!p || !data) {
      return 0;
   }

   const char* pc = static_cast<const char*>(p);
   short sdata;
   memcpy(&sdata, pc, sizeof(short));
#ifndef DRS_LITTLE_ENDIAN
   drs_word_swap(&sdata);
#endif

   switch (wfp_extract_baseline_mode(flags)) {
   case WFPH_BASELINE_MODE00:
      *data *= kBaselineCFMode00;
      break;
   case WFPH_BASELINE_MODE01:
      *data *= kBaselineCFMode01;
      break;
   default:
      cerr << "Error from WFP_ENCODE::wfp_encode_baseline : " <<
           "unknown precision mode " <<
           wfp_extract_baseline_mode(flags) << endl;
      return 0;
   }

   return 2;
}
