/********************************************************************

  Name:         DRSCalibration.cpp
  Created by:   Stefan Ritt, Matthias Schneebeli

  Contents:     Library functions for DRSCalibration

  $Id$

\********************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <mxml.h>
#include "drs_encode/DRSCalibration.h"

int DRSCalibration::GetTriggerCell(float *waveform, const bool responseCalib)
{
   int j;
   int istart = -1;
   const float threshold1 = (float)0.6;
   const float threshold2 = (float)2000;

   if (responseCalib) {
      for (j = 0; j < kNumberOfBins - 1; j++) {
         if (waveform[j] <= threshold1 &&
             waveform[j + 1] > threshold1) {
            istart = j;
            break;
         }
      }
      if (istart == -1 &&
          waveform[kNumberOfBins - 1] <= threshold1 &&
          waveform[0] > threshold1) {
         istart = kNumberOfBins - 1;
      }
   } else {
      for (j = 0; j < kNumberOfBins - 1; j++) {
         if (waveform[j] >= threshold2
             && waveform[j + 1] < threshold2) {
            istart = j;
            break;
         }
      }
      if (istart == -1 &&
          waveform[kNumberOfBins - 1] >= threshold2 &&
          waveform[0] < threshold2) {
         istart = kNumberOfBins - 1;
      }
   }

   return istart;
}

int DRSCalibration::GetTriggerCell(double *waveform, const bool responseCalib)
{
   int ret;
   float v[kNumberOfBins];
   int i;

   if (!waveform) {
      ret = -1;
   } else {
      for (i = 0; i < kNumberOfBins; i++) {
         v[i] = (float)waveform[i];
      }
      ret = GetTriggerCell(v, responseCalib);
   }

   return ret;
}

int DRSCalibration::RotateWave(const int startCell, float *waveform, float *time, float &rotatedStart,
                               int &triggerStartBin)
{
   float tmp[kNumberOfBins];
   float startTime;
   float timeOffset;
   int j;

   if (startCell > 0) {
      if (waveform) {
         memcpy(tmp, waveform + startCell, (kNumberOfBins - startCell) * sizeof(float));
         memcpy(tmp + (kNumberOfBins - startCell), waveform, startCell * sizeof(float));
         memcpy(waveform, tmp, kNumberOfBins * sizeof(float));
      }

      if (time) {
         startTime = time[startCell];
         timeOffset = time[kNumberOfBins - 1] - startTime - time[0] + time[1] - time[0];
         memcpy(tmp, time + startCell, (kNumberOfBins - startCell) * sizeof(float));
         memcpy(tmp + (kNumberOfBins - startCell), time, startCell * sizeof(float));
         memcpy(time, tmp, kNumberOfBins * sizeof(float));

         for (j = 0; j < kNumberOfBins - startCell; j++) {
            time[j] -= startTime;
         }
         for (j = kNumberOfBins - startCell; j < kNumberOfBins; j++) {
            time[j] += timeOffset;
         }
      }
   }

   rotatedStart = time ? time[kNumberOfBins - startCell] : 0;

   triggerStartBin = startCell;

   return 1;
}

int DRSCalibration::RotateWave(const int startCell, double *waveform, double *time, float &rotatedStart,
                               int &triggerStartBin)
{
   int j;
   float *v = 0;
   float *t = 0;
   int ret;

   if (waveform) {
      v = new float[kNumberOfBins];
      for (j = 0; j < kNumberOfBins; j++) {
         v[j] = (float)waveform[j];
      }
   }
   if (time) {
      t = new float[kNumberOfBins];
      for (j = 0; j < kNumberOfBins; j++) {
         t[j] = (float)time[j];
      }
   }

   ret = RotateWave(startCell, v, t, rotatedStart, triggerStartBin);

   if (waveform) {
      for (j = 0; j < kNumberOfBins; j++) {
         waveform[j] = v[j];
      }
   }
   if (time) {
      for (j = 0; j < kNumberOfBins; j++) {
         time[j] = t[j];
      }
   }

   if (v) {
      delete [] v;
   }
   if (t) {
      delete [] t;
   }

   return ret;
}

bool DRSCalibration::GetCalibratedTime(const char *path, unsigned int boardNumber, unsigned int chipIndex,
                                       unsigned int frequency, double *time)
{
   int l;
   char *cstop;
   char fileName[500];
   char error[240];
   PMXML_NODE node, rootNode, mainNode;

   sprintf(fileName, "%s/board%d/TimeCalib_board%d_chip%d_%dMHz.xml", path, boardNumber, boardNumber, chipIndex,
           frequency);
   rootNode = mxml_parse_file(fileName, error, sizeof(error), 0);
   if (rootNode == NULL) {
      return false;
   }

   mainNode = mxml_find_node(rootNode, const_cast<char*>("/DRSTimeCalibration"));

   for (l = 0; l < kNumberOfBins; l++) {
      node = mxml_subnode(mainNode, l + 2);
      time[l] = strtod(mxml_get_value(node), &cstop);
   }
   mxml_free_tree(rootNode);

   return true;
}
