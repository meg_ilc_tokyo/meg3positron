/* $Id$ */
/*
 * drived from gsl/wavelet/haar.c
 * http://www.gnu.org/software/gsl/
 */

#include <drs_encode/wavelet.h>

static const double ch_2[2] = { M_SQRT1_2, M_SQRT1_2 };
static const double cg_2[2] = { M_SQRT1_2, -(M_SQRT1_2) };

static int haar_init(const double **h1, const double **g1, const double **h2, const double **g2, size_t * nc,
                     size_t * offset, const size_t member)
{
   if (member != 2) {
      return WAVELET_FAILURE;
   }

   *h1 = ch_2;
   *g1 = cg_2;
   *h2 = ch_2;
   *g2 = cg_2;

   *nc = 2;
   *offset = 0;

   return WAVELET_SUCCESS;
}

static int haar_centered_init(const double **h1, const double **g1, const double **h2, const double **g2,
                              size_t * nc, size_t * offset, const size_t member)
{
   if (member != 2) {
      return WAVELET_FAILURE;
   }

   *h1 = ch_2;
   *g1 = cg_2;
   *h2 = ch_2;
   *g2 = cg_2;

   *nc = 2;
   *offset = 1;

   return WAVELET_SUCCESS;
}

static const wavelet_type haar_type = {
   "haar",
   &haar_init
};

static const wavelet_type haar_centered_type = {
   "haar-centered",
   &haar_centered_init
};

const wavelet_type *wavelet_haar = &haar_type;
const wavelet_type *wavelet_haar_centered = &haar_centered_type;
