// $Id$
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <getopt.h>
#include <Riostream.h>
#include <Rtypes.h>
#include <TMath.h>
#include <TRandom.h>
#include <TStopwatch.h>
#include "drs_encode/drs_encode.h"
#include "units/MEGSystemOfUnits.h"

using namespace std;
const Double_t kMaxAbsVoltage   = 1.5 * volt;
const Int_t    kNominalInterval = 500; // [psec]
const Int_t    kDRSBins         = 1024;

//______________________________________________________________________________
Bool_t check_values(Int_t nbin, const Double_t *val1, const Double_t *val2,
                    Double_t precision, const char *name,
                    Double_t maximum, Double_t minimum)
{
   Int_t i;
   Bool_t ok = kTRUE;
   for (i = 0; i < nbin; i++) {
      if (val1[i] < maximum && val1[i] > minimum &&
          TMath::Abs(val1[i] - val2[i]) > precision) {
         cerr << name << "\t" << setw(5) << i << "\t"
              << setw(10) << setprecision(6) << val1[i] << "\t"
              << setw(10) << setprecision(6) << val2[i] << "\t"
              << setw(10) << setprecision(6) << val1[i] - val2[i] << endl;
         ok = kFALSE;
      }
   }
   return ok;
}

//______________________________________________________________________________
Bool_t check_rebin_values(Int_t nbin, const Double_t *val1,
                          const Double_t *val2, Double_t precision,
                          const char *name, Double_t maximum, Double_t minimum,
                          const Int_t *rebinSize)
{
   Int_t i, j;
   Bool_t ok = kTRUE;
   Double_t mean1;
   Double_t mean2;
   for (i = 0, j = 0; i < nbin && rebinSize[i] != 0; j += rebinSize[i++]) {
      mean1 = TMath::Mean(rebinSize[i], val1 + j);
      mean2 = val2[j];
      if (mean1 < maximum && mean1 > minimum &&
          TMath::Abs(mean1 - mean2) > precision) {
         if (mean1 < maximum && mean1 > minimum &&
             TMath::Abs(mean1 - mean2) > precision) {
            cerr << name << "\t" << setw(5) << i << "\t" << setw(5) << rebinSize[i] << "\t"
                 << setw(10) << setprecision(6) << mean1 << "\t"
                 << setw(10) << setprecision(6) << mean2 << "\t"
                 << setw(10) << setprecision(6) << mean1 - mean2 << endl;
         }
         ok = kFALSE;
      }
   }
   return ok;
}

//______________________________________________________________________________
Int_t main(int argc, char* argv[])
{
   Int_t firstBin   = 0;
   Int_t nEncodeBin = kDRSBins;
   Int_t opt;
   while ((opt = getopt(argc, argv, "f:n:")) != -1) {
      switch (opt) {
      case 'f':
         firstBin = atoi(optarg);
         break;
      case 'n':
         nEncodeBin = atoi(optarg);
         break;
      default: /* '?' */
         fprintf(stderr, "Usage: %s [-f first_bin] [-n nBin]\n",
                 argv[0]);
         exit(EXIT_FAILURE);
      }
   }

   Double_t org[kDRSBins];
   char     encoded[kDRSBins * 32];
   Double_t decoded[kDRSBins];
   Int_t    iBin;
   Int_t    dsize;
   Int_t    nbin;
   Int_t    iMode;
   Int_t    mode;
   char     name[100];
   Double_t precision;
   Double_t maximum, minimum;
   Int_t iLoop;
   const Int_t kNLoop = 1000;

   //------------ Voltage ------------//
   Int_t    voltageModes[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
   Double_t voltagePrecisions[] = { 0.1  * millivolt,
                                    0.1  * millivolt,
                                    0.1  * millivolt,
                                    0.1  * millivolt,
                                    0.25 * millivolt,
                                    0.25 * millivolt,
                                    1.5  * millivolt,
                                    1.5  * millivolt,
                                    0.01 * millivolt,
                                    0.01 * millivolt,
                                    0.1  * millivolt,
                                    0.1  * millivolt
                                  };
   Double_t voltageMaximums[]   = { 3.0  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt,
                                    0.9  * millivolt,
                                    0.9  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt,
                                    3.0  * millivolt
                                  };
   Double_t voltageMinimums[]   = {-3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -0.1  * millivolt,
                                   -0.1  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt,
                                   -3.0  * millivolt
                                  };
   const Int_t kNVoltageModes = sizeof(voltageModes) / sizeof(Int_t);
   Int_t       iVoltageTest;
   const Int_t kNumberOfVoltageTest = 3;
   TStopwatch *voltageWatch[kNVoltageModes];
   Int_t       voltageEncodedSize[kNVoltageModes];
   Int_t       rebinSize[kDRSBins];
   short       rebinOpt[8];

   memset(voltageEncodedSize, 0, sizeof(voltageEncodedSize));
   for (iMode = 0; iMode < kNVoltageModes; iMode++) {
      voltageWatch[iMode] = new TStopwatch();
      voltageWatch[iMode]->Stop();
   }

   for (iVoltageTest = 0; iVoltageTest < kNumberOfVoltageTest; iVoltageTest++) {
      for (iLoop = 0; iLoop < kNLoop; iLoop++) {
         switch (iVoltageTest) {
         case 0:
            // Flat voltage test
            org[0] = kMaxAbsVoltage * (1. - 2 * gRandom->Rndm());
            for (iBin = 1; iBin < kDRSBins; iBin++) {
               org[iBin] = org[0];
            }
            break;
         case 1:
            // Sin wave test
            for (iBin = 0; iBin < kDRSBins; iBin++) {
               org[iBin] = kMaxAbsVoltage *
                           TMath::Sin(2. * TMath::Pi() *
                                      static_cast<Double_t>(iBin +
                                                            iVoltageTest * 10) / 512);
            }
            break;
         default:
            // Random test
            for (iBin = 0; iBin < kDRSBins; iBin++) {
               org[iBin] = kMaxAbsVoltage * (1. - 2 * gRandom->Rndm());
            }
            break;
         };

         for (iMode = 0; iMode < kNVoltageModes; iMode++) {
            mode = voltageModes[iMode];
            precision = voltagePrecisions[iMode];
            maximum = voltageMaximums[iMode];
            minimum = voltageMinimums[iMode];
            switch (mode) {
            case 0:
               strcpy(name, "volt mode 00");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode00(nEncodeBin, firstBin, org,
                                                 encoded);
               nbin  = drs_decode_voltage_mode00(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 1:
               strcpy(name, "volt mode 01");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode01(nEncodeBin, firstBin, org,
                                                 encoded);
               nbin  = drs_decode_voltage_mode01(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 2:
               strcpy(name, "volt mode 02");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode02(nEncodeBin, firstBin, org,
                                                 encoded, Z_BEST_SPEED);
               nbin  = drs_decode_voltage_mode02(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 3:
               strcpy(name, "volt mode 03");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode03(nEncodeBin, firstBin, org,
                                                 encoded, Z_BEST_SPEED);
               nbin  = drs_decode_voltage_mode03(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 4:
               strcpy(name, "volt mode 04");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode04(nEncodeBin, firstBin, org,
                                                 encoded, -100 * millivolt);
               nbin  = drs_decode_voltage_mode04(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 5:
               strcpy(name, "volt mode 05");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode05(nEncodeBin, firstBin, org,
                                                 encoded, -100 * millivolt,
                                                 Z_BEST_SPEED);
               nbin  = drs_decode_voltage_mode05(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 6:
               strcpy(name, "volt mode 06");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode06(nEncodeBin, firstBin, org,
                                                 encoded, 0, 1);
               nbin  = drs_decode_voltage_mode06(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 7:
               strcpy(name, "volt mode 07");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode07(nEncodeBin, firstBin, org,
                                                 encoded, 0, 0, 1);
               nbin  = drs_decode_voltage_mode07(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 8:
               strcpy(name, "volt mode 08");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode08(nEncodeBin, firstBin, org,
                                                 encoded);
               nbin  = drs_decode_voltage_mode08(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 9:
               strcpy(name, "volt mode 09");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode09(nEncodeBin, firstBin, org,
                                                 encoded);
               nbin  = drs_decode_voltage_mode09(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 10:
               strcpy(name, "volt mode 10");
               voltageWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_voltage_mode10(nEncodeBin, firstBin, org,
                                                 encoded);
               nbin  = drs_decode_voltage_mode10(dsize, firstBin, encoded,
                                                 decoded);
               voltageWatch[iMode]->Stop();
               break;
            case 11:
               strcpy(name, "volt mode 11");
               voltageWatch[iMode]->Start(kFALSE);
               rebinOpt[0] = iLoop % 6; // 0, 2, 4, 8, 16, 32.
               if (nEncodeBin % (1 << rebinOpt[0]) == 0) {
                  dsize = drs_encode_voltage_mode11(nEncodeBin, firstBin, org,
                                                    encoded, 0, rebinOpt, 0);
                  nbin  = drs_decode_voltage_mode11(dsize, firstBin, encoded,
                                                    decoded, rebinSize);
               } else {
                  dsize = 0;
                  nbin = 0;
               }
               voltageWatch[iMode]->Stop();
               break;
            default:
               strcpy(name, "unknown");
               dsize = 0;
               nbin  = 0;
               break;
            };
            voltageEncodedSize[iMode] += dsize;

            if (mode != 11) {
               if (nbin != nEncodeBin) {
                  cerr << name << endl;
               } else {
                  check_values(nbin, org + firstBin, decoded + firstBin,
                               precision, name, maximum, minimum);
               }
            } else if (nEncodeBin % (1 << rebinOpt[0]) == 0) {
               if (nbin != nEncodeBin) {
                  cerr << name << endl;
               } else {
                  check_rebin_values(nbin, org + firstBin, decoded + firstBin,
                                     precision, name, maximum, minimum,
                                     rebinSize);
               }
            }
         }
      }
   }

   for (iMode = 0; iMode < kNVoltageModes; iMode++) {
      cout << "volt mode " << setw(2) << voltageModes[iMode] << " : "
           << setw(12) << voltageEncodedSize[iMode] << " : ";
      voltageWatch[iMode]->Print();
      delete voltageWatch[iMode];
   }

   //------------ Time ------------//
   Int_t    timeModes[] = {0, 2, 8, 9, 10};
   Double_t timePrecisions[] = { 5   * picosecond,
                                 5   * picosecond,
                                 0.1 * picosecond,
                                 0.1 * picosecond,
                                 1   * picosecond
                               };
   Double_t timeMaximums[]   = { 550 * nanosecond,
                                 550 * nanosecond,
                                 550 * nanosecond,
                                 550 * nanosecond,
                                 550 * nanosecond
                               };
   Double_t timeMinimums[]   = {   0 * nanosecond,
                                   0 * nanosecond,
                                   0 * nanosecond,
                                   0 * nanosecond,
                                   0 * nanosecond
                               };
   const Int_t kNTimeModes = sizeof(timeModes) / sizeof(Int_t);
   Int_t       iTimeTest;
   const Int_t kNumberOfTimeTest = 2;
   TStopwatch *timeWatch[kNTimeModes];
   Int_t       timeEncodedSize[kNTimeModes];

   memset(timeEncodedSize, 0, sizeof(timeEncodedSize));
   for (iMode = 0; iMode < kNTimeModes; iMode++) {
      timeWatch[iMode] = new TStopwatch();
      timeWatch[iMode]->Stop();
   }

   for (iTimeTest = 0; iTimeTest < kNumberOfTimeTest; iTimeTest++) {
      for (iLoop = 0; iLoop < kNLoop; iLoop++) {
         switch (iTimeTest) {
         case 0:
            // Fixed interval
            for (iBin = 0; iBin < kDRSBins; iBin++) {
               org[iBin] = kNominalInterval * picosecond * iBin;
            }
            break;
         default:
            // Random test (5 % max fluctuation)
            org[0] = 0;
            for (iBin = 1; iBin < kDRSBins; iBin++) {
               org[iBin] = org[iBin - 1] + kNominalInterval * picosecond *
                           (1 + 0.05 * (2 * gRandom->Rndm() - 1));
            }
            break;
         };

         for (iMode = 0; iMode < kNTimeModes; iMode++) {
            mode      = timeModes[iMode];
            precision = timePrecisions[iMode];
            maximum   = timeMaximums[iMode];
            minimum   = timeMinimums[iMode];
            switch (mode) {
            case 0:
               strcpy(name, "time mode 00");
               timeWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_time_mode00(nEncodeBin, kNominalInterval,
                                              firstBin, org, encoded);
               nbin  = drs_decode_time_mode00(dsize, kNominalInterval,
                                              firstBin, encoded, decoded);
               timeWatch[iMode]->Stop();
               break;
            case 2:
               strcpy(name, "time mode 02");
               timeWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_time_mode02(nEncodeBin, kNominalInterval,
                                              firstBin, org, encoded,
                                              Z_BEST_SPEED);
               nbin  = drs_decode_time_mode02(dsize, kNominalInterval,
                                              firstBin, encoded, decoded);
               timeWatch[iMode]->Stop();
               break;
            case 8:
               strcpy(name, "time mode 08");
               timeWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_time_mode08(nEncodeBin, firstBin, org,
                                              encoded);
               nbin  = drs_decode_time_mode08(dsize, firstBin, encoded,
                                              decoded);
               timeWatch[iMode]->Stop();
               break;
            case 9:
               strcpy(name, "time mode 09");
               timeWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_time_mode09(nEncodeBin, firstBin, org,
                                              encoded);
               nbin  = drs_decode_time_mode09(dsize, firstBin, encoded,
                                              decoded);
               timeWatch[iMode]->Stop();
               break;
            case 10:
               strcpy(name, "time mode 10");
               timeWatch[iMode]->Start(kFALSE);
               dsize = drs_encode_time_mode10(nEncodeBin, firstBin, org,
                                              encoded);
               nbin  = drs_decode_time_mode10(dsize, firstBin, encoded,
                                              decoded);
               timeWatch[iMode]->Stop();
               break;
            default:
               strcpy(name, "unknown");
               dsize = 0;
               nbin  = 0;
               break;
            };
            timeEncodedSize[iMode] += dsize;
            if (nbin != nEncodeBin) {
               cerr << name << endl;
            } else {
               check_values(nbin, org + firstBin, decoded + firstBin,
                            precision, name, maximum, minimum);
            }
         }
      }
   }

   for (iMode = 0; iMode < kNTimeModes; iMode++) {
      cout << "time mode " << setw(2) << timeModes[iMode] << " : "
           << setw(12) << timeEncodedSize[iMode] << " : ";
      timeWatch[iMode]->Print();
      delete timeWatch[iMode];
   }

   return 0;
}
