// $Id$
// Author: Matthias Schneebeli

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// C functions for class MEGBField                                            //
// Used by megmc                                                              //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#include <io.h>
#else
#include <unistd.h>
#endif
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "bfield/megbfield_c.h"

#ifdef _MSC_VER
#   define O_RDONLY_BINARY O_RDONLY | O_BINARY
#else
#   define O_RDONLY_BINARY O_RDONLY
#endif
//#define USE_LOWLEVEL_IO

//#define kNumberOfField     2
#define kNumberOfField    10
#define kNumberOfDirection 3
#define kNumberOfMagnet    3
#define kMaxNumberOfPhi   36

/* Byte and Word swapping big endian <-> little endian */
/* Copied from midas/include/msystem.h */
/**
SWAP WORD macro */
#define WORD_SWAP(x) { unsigned char _tmp;                                        \
                       _tmp                        = *( (unsigned char *)(x));    \
                       *( (unsigned char *)(x))    = *(((unsigned char *)(x))+1); \
                       *(((unsigned char *)(x))+1) = _tmp; }

/**
SWAP DWORD macro */
#define DWORD_SWAP(x) { unsigned char _tmp;                                       \
                       _tmp                        = *( (unsigned char *)(x));    \
                       *( (unsigned char *)(x))    = *(((unsigned char *)(x))+3); \
                       *(((unsigned char *)(x))+3) = _tmp;                        \
                       _tmp                        = *(((unsigned char *)(x))+1); \
                       *(((unsigned char *)(x))+1) = *(((unsigned char *)(x))+2); \
                       *(((unsigned char *)(x))+2) = _tmp; }

/**
SWAP QWORD macro */
#define QWORD_SWAP(x) { unsigned char _tmp;                                       \
                       _tmp                        = *( (unsigned char *)(x));    \
                       *( (unsigned char *)(x))    = *(((unsigned char *)(x))+7); \
                       *(((unsigned char *)(x))+7) = _tmp;                        \
                       _tmp                        = *(((unsigned char *)(x))+1); \
                       *(((unsigned char *)(x))+1) = *(((unsigned char *)(x))+6); \
                       *(((unsigned char *)(x))+6) = _tmp;                        \
                       _tmp                        = *(((unsigned char *)(x))+2); \
                       *(((unsigned char *)(x))+2) = *(((unsigned char *)(x))+5); \
                       *(((unsigned char *)(x))+5) = _tmp;                        \
                       _tmp                        = *(((unsigned char *)(x))+3); \
                       *(((unsigned char *)(x))+3) = *(((unsigned char *)(x))+4); \
                       *(((unsigned char *)(x))+4) = _tmp; }

static int     fNu       [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static int     fNv       [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fZmin     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fZmax     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fZrange   [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fZrangeInv[kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fRmin     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fRmax     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fRrange   [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double  fRrangeInv[kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];
static double *fGrid     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];  //!
static int     fNPhi     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection];
static double  fDPhi     [kNumberOfMagnet][kNumberOfField][kNumberOfDirection];
static double  fDPhiI    [kNumberOfMagnet][kNumberOfField][kNumberOfDirection];

static char    fType     [kNumberOfField]    [20] = {"Measured", "Calculated", "Measured", "Measured", "Measured"
                                                     , "Measured", "Calculated", "Measured", "Measured", "Calculated"};
static char    fMagnet   [kNumberOfMagnet]   [30] = {"Cobra", "BTS", "Bypass"};
static char    fMagnetDir[kNumberOfMagnet]   [20] = {"cobra", "bts", "bypass"};
static char    fDirection[kNumberOfDirection][5]  = {"Bz", "Br", "Bphi"};
static char    fDirection2[kNumberOfDirection][4] = {"Z", "R", "Phi"};
static char    fCalculatedSegment[3][3]           = {"DS", "C", "US"};

static int     fInitialized[kNumberOfMagnet][kNumberOfField][kNumberOfDirection][kMaxNumberOfPhi];

static int fFieldType = 0;
static int fMagnetType = 0;

static const int kMagnetTypeCobra  = 0;
static const int kMagnetTypeBTS    = 1;
static const int kMagnetTypeBypass = 2;

static const int kDirectionZ   = 0;
static const int kDirectionR   = 1;
static const int kDirectionPhi = 2;


#ifdef _MSC_VER
static double GetValue(double* coefficients, double u, double v, int nu, int nv);
#else
static inline double GetValue(double* coefficients, double u, double v, int nu, int nv);
#endif

int initbfield_(const int *magnet, const int *fieldType, const double *cobraScale, const double *btsScale, const double *bypassScale)
{
   // Constructor for the Magnetic Field class
   // 
   // magnet    : The type of the magnet
   //             0 for all magnets
   //             1 for Cobra
   //             2 for BTS
   //             3 for Bypass Field
   // 
   // fieldType : 0 for measured field data
   //             1 for calculated field data
   //
   // cobraScale, btsScale, bypassScale : scale factors for the different magnets
   //
   // return value: 0 for error
   //               1 for success
   int   i, j, k, ii, jj, im;
   char  fileName[1024];
   char  numString[10];
   char  segString[4];
#ifdef USE_LOWLEVEL_IO
   int   fileHandle;
#else
   FILE  *fp;
#endif
   char *meg2sys;
   int phi;

   double scale[kNumberOfMagnet] = {*cobraScale, *btsScale, *bypassScale};

   // initializations
   fMagnetType = *magnet;
   fFieldType  = *fieldType;

   for (im=0; im<kNumberOfMagnet; im++) {
      for (i=0; i<kNumberOfField; i++) {
         for (j=0; j<kNumberOfDirection; j++) {
            for (k=0; k<kMaxNumberOfPhi; k++) {
               fInitialized[im][i][j][k] = 0;
            }
         }
      }
   }

   meg2sys = getenv("MEG2SYS");
   if (meg2sys==0) {
      printf(__FILE__ ": Please set the environment variable MEG2SYS to the MEG root-directory.\n");
      exit(EXIT_FAILURE);
   }

   // loop over possible configurations
   for (im=0; im<kNumberOfMagnet; im++) {
      for (i=0; i<kNumberOfField; i++) {
         if (im==kMagnetTypeBypass && (i!=kCalculated&&i!=kCalcScaled&&i!=kCalcScaled2)) {
            continue;
         }
         for (j=0; j<kNumberOfDirection; j++) {
            fNPhi[im][i][j] = kMaxNumberOfPhi;
            if ( (i==kCalculated||i==kCalcScaled||i==kCalcScaled2) && j>1) {
               continue;
            }
            for (k=0; ((i!=kCalculated&&i!=kCalcScaled&&i!=kCalcScaled2)&&
                        k<kMaxNumberOfPhi&&k<fNPhi[im][i][j]) || ((i==kCalculated||i==kCalcScaled||i==kCalcScaled2)&&k<3); k++) {
               if (i!=kCalculated&&i!=kCalcScaled&&i!=kCalcScaled2) {
                  sprintf(numString, "phi%d_", k);
                  segString[0] = '\0';
               } else if (i==kCalculated||i==kCalcScaled||i==kCalcScaled2) {
                  numString[0] = '\0';
                  sprintf(segString, "%s_", fCalculatedSegment[k]);
               }

               if (i==kMeasured||i==kCalculated)
                  sprintf(fileName, "%s/meg2lib/common/bfield/%s/bfield_%s_%s_%s%s_%sSurface.bin"
                          , meg2sys
                          , fMagnetDir[im]
                          , fMagnet[im]
                          , fDirection[j]
                          , numString
                          , fType[i]
                          , segString );

               if (i==kReconstructed1||i==kReconstructed3||i==kReconstructed4||i==kReconstructed5||i==kReconstructed6)
                  sprintf(fileName, "%s/meg2lib/common/bfield_rec1/%s/bfield_%s_%s_%s%s_%sSurface.bin"
                          , meg2sys
                          , fMagnetDir[im]
                          , fMagnet[im]
                          , fDirection[j]
                          , numString
                          , fType[i]
                          , segString );

               if (i==kReconstructed2)
                  sprintf(fileName, "%s/meg2lib/common/bfield_rec2/%s/bfield_%s_%s_%s%s_%sSurface.bin"
                          , meg2sys
                          , fMagnetDir[im]
                          , fMagnet[im]
                          , fDirection[j]
                          , numString
                          , fType[i]
                          , segString );

               if (i==kCalcScaled)
                  sprintf(fileName, "%s/meg2lib/common/bfield_cals/%s/bfield_%s_%s_%s%s_%sSurface.bin"
                          , meg2sys
                          , fMagnetDir[im]
                          , fMagnet[im]
                          , fDirection[j]
                          , numString
                          , fType[i]
                          , segString );

               if (i==kCalcScaled2)
                  sprintf(fileName, "%s/meg2lib/common/bfield_calc_test1/%s/bfield_%s_%s_%s%s_%sSurface.bin"
                          , meg2sys
                          , fMagnetDir[im]
                          , fMagnet[im]
                          , fDirection[j]
                          , numString
                          , fType[i]
                          , segString );

#ifdef USE_LOWLEVEL_IO
               fileHandle = open(fileName, O_RDONLY_BINARY);
               if (fileHandle==-1) {
                  printf(__FILE__ ": %s : %s\n", fileName, strerror(errno));
                  exit(EXIT_FAILURE);
               }

               // read parameters
               if(read(fileHandle, &fNPhi[im][i][j]     , 4) != 4) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &phi                 , 4) != 4) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &fNu[im][i][j][k]    , 4) != 4) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &fNv[im][i][j][k]    , 4) != 4) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &fZmin[im][i][j][k]  , 8) != 8) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &fZrange[im][i][j][k], 8) != 8) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &fRmin[im][i][j][k]  , 8) != 8) {exit(EXIT_FAILURE);}
               if(read(fileHandle, &fRrange[im][i][j][k], 8) != 8) {exit(EXIT_FAILURE);}
#else
               fp = fopen(fileName, "rb");
               if (fp == NULL) {
                  printf(__FILE__ ": %s : %s\n", fileName, strerror(errno));
                  exit(EXIT_FAILURE);
               }

               // read parameters
               if (fread(&fNPhi[im][i][j],   4, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&phi            ,   4, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&fNu[im][i][j][k]  ,   4, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&fNv[im][i][j][k]  ,   4, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&fZmin[im][i][j][k],   8, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&fZrange[im][i][j][k], 8, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&fRmin[im][i][j][k],   8, 1, fp) < 1) {exit(EXIT_FAILURE);}
               if (fread(&fRrange[im][i][j][k], 8, 1, fp) < 1) {exit(EXIT_FAILURE);}
#endif

#if defined(__BIG_ENDIAN__) || defined(_BIG_ENDIAN)
               DWORD_SWAP(&fNPhi[im][i][j]);
               DWORD_SWAP(&phi);
               DWORD_SWAP(&fNu[im][i][j][k]);
               DWORD_SWAP(&fNv[im][i][j][k]);
               QWORD_SWAP(&fZmin[im][i][j][k]);
               QWORD_SWAP(&fZrange[im][i][j][k]);
               QWORD_SWAP(&fRmin[im][i][j][k]);
               QWORD_SWAP(&fRrange[im][i][j][k]);
#endif

               // m to cm
               fZmin[im][i][j][k] *= 100;
               fZrange[im][i][j][k] *= 100;
               fRmin[im][i][j][k] *= 100;
               fRrange[im][i][j][k] *= 100;

               // pre-calculation
               fZrangeInv[im][i][j][k] = 1/fZrange[im][i][j][k];
               fZmax[im][i][j][k] = fZmin[im][i][j][k]+fZrange[im][i][j][k];
               fRrangeInv[im][i][j][k] = 1/fRrange[im][i][j][k];
               fRmax[im][i][j][k] = fRmin[im][i][j][k]+fRrange[im][i][j][k];

               // read grid
               fGrid[im][i][j][k] = (double*)malloc(8*fNu[im][i][j][k]*fNv[im][i][j][k]);
               for (ii=0; ii<fNu[im][i][j][k]; ii++) {
                  for (jj=0; jj<fNv[im][i][j][k]; jj++) {
#ifdef USE_LOWLEVEL_IO
                     if(read(fileHandle, &fGrid[im][i][j][k][ii*fNv[im][i][j][k]+jj], 8) != 8) {
                        exit(EXIT_FAILURE);
                     }
#else
                     if (fread(&fGrid[im][i][j][k][ii*fNv[im][i][j][k]+jj], 8, 1, fp) < 1) {exit(EXIT_FAILURE);}
#endif

#if defined(__BIG_ENDIAN__) || defined(_BIG_ENDIAN)
                     QWORD_SWAP(&fGrid[im][i][j][k][ii*fNv[im][i][j][k]+jj]);
#endif
                     // scale field
                     fGrid[im][i][j][k][ii*fNv[im][i][j][k]+jj] *= scale[im];
                     // convert to gauss
                     fGrid[im][i][j][k][ii*fNv[im][i][j][k]+jj] *= 10000;
                  }
               }
#ifdef USE_LOWLEVEL_IO
               close(fileHandle);
#else
               fclose(fp);
#endif
               // check number of phi
               if (fNPhi[im][i][j]>kMaxNumberOfPhi ) {
                  printf(__FILE__ " : Number of Phi overflow for %s %s in %s direction : %d\n"
                          , fType[i]
                          , fMagnet[im]
                          , fDirection2[j],
                          fNPhi[im][i][j]);
                  exit(0);
               }
               // set initailized flag
               fInitialized[im][i][j][k] = 1;
            }
            // pre-calculation
            if (fNPhi[im][i][j]>0) {
               fDPhiI[im][i][j] = fNPhi[im][i][j]/360.;
               fDPhi[im][i][j] = 1/fDPhiI[im][i][j];
            }
         }
      }
   }
   return 1;
}

void freebfield_(void)
{
   int i, j, im, k;
   for (im=0; im<kNumberOfMagnet; im++) {
      for (i=0; i<kNumberOfField; i++) {
         for (j=0; j<kNumberOfDirection; j++) {
            if (i>0 && j>1)
               continue;
            for (k=0; k<kMaxNumberOfPhi; k++) {
               if (fInitialized[im][i][j][k]) {
                  free(fGrid[im][i][j][k]);
               }
            }
         }
      }
   }
}

double getb_(const double *z, const double *r, const double *phi, 
             const int *im, const int *it, const int *id)
{
   // Returns one component of the BField
   // 
   // z    : z coordinate
   // r    : r coordinate
   // phi  : phi coordinate
   // im   : magnet type
   // it   : field type
   // id   : direction
   //
   // return value: Bfield in gauss
   //
   int itI = *it;
   double bfield=0;
   int iphi=0;
   double zz, rr, pp;
   int outOfBound = 0;

   zz = *z;
   rr = *r;
   pp = *phi;

   if (itI==kReconstructed3)
      zz+=0.05;

   if (itI==kReconstructed4)
      zz+=0.1;

   if (itI==kReconstructed5)
      zz+=0.075;

   if (itI==kReconstructed6)
      zz+=0.15;

   // accept -360<phi<720
   if(pp<0) {
      pp += 360.;
   }
   else if(pp>360.){
      pp -= 360.;
   }

   // no phi direction for calculated and measured bypass field
   if (*id==kDirectionPhi &&
       ((*im==2 && (itI!=kCalculated&&itI!=kCalcScaled&&itI!=kCalcScaled2)) ||
         itI==kCalculated || itI==kCalcScaled || itI==kCalcScaled2))
      return 0;

   // no measured bypass field
   if (*im==kMagnetTypeBypass && itI==kMeasured)
      itI = kCalculated;

   if (*im==kMagnetTypeBypass && (itI!=kMeasured&&itI!=kCalculated))
      itI = kCalcScaled;

   // check for a valid field
   if (!fInitialized[*im][itI][*id][0]) {
      printf("%s %s B-Field in %s direction is not initialized\n"
             , fType[itI]
             , fMagnet[*im]
             , fDirection2[*id] );
      return 0;
   }

   // get phi index of measured fields & check boundary
   if (itI==kMeasured) {
      if (fNPhi[*im][itI][*id]<2)
         return 0;
      iphi = (int)(pp*fDPhiI[*im][itI][*id]+0.5);
      if (iphi==fNPhi[*im][itI][*id])
         iphi = 0;

      if (zz<fZmin[*im][itI][*id][iphi] ||
          zz>fZmax[*im][itI][*id][iphi] ||
          rr<fRmin[*im][itI][*id][iphi] ||
          rr>fRmax[*im][itI][*id][iphi])
         outOfBound = 1;
   } else if (itI!=kCalculated&&itI!=kCalcScaled&&itI!=kCalcScaled2) {
      if (fNPhi[*im][itI][*id]<2)
         return 0;
      iphi = (int)(pp*fDPhiI[*im][itI][*id]+0.5);
      if (iphi==fNPhi[*im][itI][*id])
         iphi = 0;

      if (zz<fZmin[*im][itI][*id][iphi] ||
          zz>fZmax[*im][itI][*id][iphi] ||
          rr<fRmin[*im][itI][*id][iphi] ||
          rr>fRmax[*im][itI][*id][iphi] ||
          rr>26. ||
          fabs(rr)<2.) // in |R| < 2cm region, reconstructed data not exist
         outOfBound = 1;
   }

   // use calculated field if out of bounds
   if (outOfBound) {
      if (itI==kMeasured)
         itI = kCalculated;
      if (itI==kReconstructed1||itI==kReconstructed2||itI==kReconstructed3||itI==kReconstructed4||itI==kReconstructed5||itI==kReconstructed6)
         itI = kCalcScaled;
      if (*id==kDirectionPhi)
         return 0;
   }

   // get measured bfield
   if (itI!=kCalculated&&itI!=kCalcScaled&&itI!=kCalcScaled2) {
      bfield = GetValue(fGrid[*im][itI][*id][iphi]
                       , (zz-fZmin[*im][itI][*id][iphi])*fZrangeInv[*im][itI][*id][iphi]
                       , (rr-fRmin[*im][itI][*id][iphi])*fRrangeInv[*im][itI][*id][iphi]
                       , fNu[*im][itI][*id][iphi]
                       , fNv[*im][itI][*id][iphi]);
   }
   // get calculated bfield
   else if (itI==kCalculated||itI==kCalcScaled||itI==kCalcScaled2) {
      if (zz<=-50) {
         if (zz<fZmin[*im][itI][*id][0] ||
             rr<fRmin[*im][itI][*id][0] ||
             rr>fRmax[*im][itI][*id][0])
            return 0;
         bfield = GetValue(fGrid[*im][itI][*id][0]
                           , (zz-fZmin[*im][itI][*id][0])*fZrangeInv[*im][itI][*id][0]
                           , (rr-fRmin[*im][itI][*id][0])*fRrangeInv[*im][itI][*id][0]
                           , fNu[*im][itI][*id][0]
                           , fNv[*im][itI][*id][0]);
      }
      else if (-50<zz && zz<=50) {
         if (rr<fRmin[*im][itI][*id][1] ||
             rr>fRmax[*im][itI][*id][1])
            return 0;
         bfield = GetValue(fGrid[*im][itI][*id][1]
                           , (zz-fZmin[*im][itI][*id][1])*fZrangeInv[*im][itI][*id][1]
                           , (rr-fRmin[*im][itI][*id][1])*fRrangeInv[*im][itI][*id][1]
                           , fNu[*im][itI][*id][1]
                           , fNv[*im][itI][*id][1]);
      }
      else if (50<zz) {
         if (zz>fZmax[*im][itI][*id][2] ||
             rr<fRmin[*im][itI][*id][2] ||
             rr>fRmax[*im][itI][*id][2])
            return 0;
         bfield = GetValue(fGrid[*im][itI][*id][2]
                           , (zz-fZmin[*im][itI][*id][2])*fZrangeInv[*im][itI][*id][2]
                           , (rr-fRmin[*im][itI][*id][2])*fRrangeInv[*im][itI][*id][2]
                           , fNu[*im][itI][*id][2]
                           , fNv[*im][itI][*id][2]);
      }
   }

   return bfield;
}

double getbz_(const double *z, const double *r, const double *phi)
{
   // Returns z component of the BField
   // 
   // z    : z coordinate
   // r    : r coordinate
   // phi  : phi coordinate
   //
   // return value: Bfield in gauss
   //
   int im = fMagnetType-1;
   if (fMagnetType==kAll) {
      return getb_(z, r, phi, &kMagnetTypeCobra,  &fFieldType, &kDirectionZ)
            +getb_(z, r, phi, &kMagnetTypeBTS,    &fFieldType, &kDirectionZ)
            +getb_(z, r, phi, &kMagnetTypeBypass, &fFieldType, &kDirectionZ);
   } else {
      return getb_(z, r, phi, &im, &fFieldType, &kDirectionZ);
   }
}

double getbr_(const double *z, const double *r, const double *phi)
{
   // Returns r component of the BField
   // 
   // z    : z coordinate
   // r    : r coordinate
   // phi  : phi coordinate
   //
   // return value: Bfield in gauss
   //
   int im = fMagnetType-1;
   if (fMagnetType==0) {
      return getb_(z, r, phi, &kMagnetTypeCobra,  &fFieldType, &kDirectionR)
            +getb_(z, r, phi, &kMagnetTypeBTS,    &fFieldType, &kDirectionR)
            +getb_(z, r, phi, &kMagnetTypeBypass, &fFieldType, &kDirectionR);
   } else {
      return getb_(z, r, phi, &im, &fFieldType, &kDirectionR);
   }
}

double getbphi_(const double *z, const double *r, const double *phi)
{
   // Returns phi component of the BField
   // 
   // z    : z coordinate
   // r    : r coordinate
   // phi  : phi coordinate
   //
   // return value: Bfield in gauss
   //
   int im = fMagnetType-1;
   if (fMagnetType==0) {
      return getb_(z, r, phi, &kMagnetTypeCobra, &fFieldType, &kDirectionPhi)
            +getb_(z, r, phi, &kMagnetTypeBTS,   &fFieldType, &kDirectionPhi);
   } else {
      return getb_(z, r, phi, &im, &fFieldType, &kDirectionPhi);
   }
}

void setfieldtype_(const int *fieldType)
{
   fFieldType = *fieldType;
}

int getfieldtype_(void)
{
   return fFieldType;
}

void setmagnettype_(const int *magnetType)
{
   fMagnetType = *magnetType;
}

int getmagnettype_(void)
{
   return fMagnetType;
}




#ifdef _MSC_VER
double GetValue(double* coefficients, double u, double v, int nu, int nv)
#else
inline double GetValue(double* coefficients, double u, double v, int nu, int nv)
#endif
{
   int j, k, nk, iu, iv;
   double s=0;
   double s2=0;
   int ind, ind0;
   double  fBSplinesU[2];
   double  fBSplinesV[2];

   if (u>=1) {
      fBSplinesU[0] = 0;
      fBSplinesU[1] = 1;
      iu = nu-2;
   }
   else if (u <= 0) {
      fBSplinesU[0] = 1;
      fBSplinesU[1] = 0;
      iu = 0;
   }
   else {
      nk = (nu-1);
      iu = (int)(u*nk);

      fBSplinesU[1] = u*nk - iu;
      fBSplinesU[0] = 1-fBSplinesU[1];
   }
   if (v>=1) {
      fBSplinesV[0] = 0;
      fBSplinesV[1] = 1;
      iv = nv-2;
   }
   else if (v <= 0) {
      fBSplinesV[0] = 1;
      fBSplinesV[1] = 0;
      iv = 0;
   }
   else {
      nk = (nv-1);
      iv = (int)(v*nk);

      fBSplinesV[1] = v*nk - iv;
      fBSplinesV[0] = 1-fBSplinesV[1];
   }

   ind0 = iv+iu*nv;
   for (j=0; j<2; j++) {
      s2=0;
      for (k=0; k<2; k++) {
         ind = ind0+j+k*nv;
         s2 += coefficients[ind]*fBSplinesU[k];
      }
      s += s2*fBSplinesV[j];
   }
   return s;
}
