// $Id$

// Author: Yusuke Uchiyama

//__________________________________________________________________________________
//
// MEGWaveform
//
// Basic class to treat waveforms for the MEG experiment.
// It can treat DRS and trigger FADC data, and it can be used both for analysis and
// simulation.
//
// It has two modes, one for fix bin size that is the sampling interval is constant,
// and the other is variable bin size that is the sampling speed has fluctuation.
// For both case you have to specify the number of points and (average) sampling
// interval through constructor MEGWaveform(npoints,binsize) or SetNPoints()
// and SetBinSize().
// * In case of fix bin size,
// set time information through SetTimeMin(tmin) or SetTime(tmin,tmax). "tmin" is
// the time of first point and "tmax" is that of the final point.
// * In case of variable bin size,
// set time through SetTime(ptime). "ptime" is a pointer or array name which contains
// time of all points.
// Notice that this class does not allocate fTime, not like fAmplitude and assure that
// ptime has reserved storage memory.
//
// For Discrete Fourier Transforms(DFT) FFTW(www.fftw.org) library is used.
// To use FFT() or TransformWaveform(), you need:
//  * Install the FFTW library, version 3.0.1 and higher
//

#include <map>
//#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <functional>
#include <typeinfo>
#include <RConfig.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( push )
#   pragma warning( disable : 4800)
#endif // R__VISUAL_CPLUSPLUS
#include <Riostream.h>
#include <TMath.h>
#include <TH1.h>
#include <TF1.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TVirtualFFT.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS


#include "waveform/MEGWaveform.h"
#include "units/MEGSystemOfUnits.h"
#include "units/MEGPhysicalConstants.h"
#include "constants/drs/drsconst.h"

ClassImp(MEGWaveform)
// ClassImp(MEGFrequencyResponse)

using namespace std;
using namespace MEG;

const Double_t MEGWaveform::kInvalidTime = 1e10 * second;
const Double_t MEGWaveform::kMinimumStep = 1e-6 * volt;

//__________________________________________________________________________________
MEGWaveform::MEGWaveform()
   : TNamed(), TAttLine(), TAttFill(0, 0), TAttMarker()
   , fMaxNPoints(0)
   , fNPoints(0)
   , fBinSize(0)
   , fFixBinSize(true)
   , fAmplitude(0)
   , fTime(0)
   , fID(0)
   , fOverflow(false)
   , fTimeMin(0)
   , fTimeMax(0)
   , fHistogram(0)
   , fGraph(0)
   , fMinimum(-1111)
   , fMaximum(-1111)
   , fDisplayAmplUnit(volt)
   , fDisplayTimeUnit(second)
   , fEditable(false)
   , fAttenuation(1)
   , fDrawnObject(nullptr)
{
   // Waveform default constructor.
}

//__________________________________________________________________________________
MEGWaveform::MEGWaveform(Int_t npoints, Double_t binsize, Double_t *ptime, const char *title)
   : TNamed("Waveform", title), TAttLine(), TAttFill(0, 0), TAttMarker()
   , fMaxNPoints(npoints)
   , fNPoints(npoints)
   , fBinSize(binsize)
   , fTime(ptime)
   , fID(0)
   , fOverflow(false)
   , fTimeMin(0)
   , fHistogram(0)
   , fGraph(0)
   , fMinimum(-1111)
   , fMaximum(-1111)
   , fDisplayAmplUnit(volt)
   , fDisplayTimeUnit(second)
   , fEditable(false)
   , fAttenuation(1)
   , fDrawnObject(nullptr)
{
   // Waveform normal constructor.

   fFixBinSize = ptime ? false : true;
   fTimeMax   = binsize * (npoints - 1);
   // allocate
   fAmplitude = new Double_t[npoints];
   memset(fAmplitude, 0, sizeof(Double_t)*npoints);
}

//__________________________________________________________________________________
MEGWaveform::MEGWaveform(Int_t npoints, Double_t binsize, Double_t tmin, const char *title)
   : TNamed("Waveform", title), TAttLine(), TAttFill(0, 0), TAttMarker()
   , fMaxNPoints(npoints)
   , fNPoints(npoints)
   , fBinSize(binsize)
   , fFixBinSize(true)
   , fTime(0)
   , fID(0)
   , fOverflow(false)
   , fTimeMin(tmin)
   , fHistogram(0)
   , fGraph(0)
   , fMinimum(-1111)
   , fMaximum(-1111)
   , fDisplayAmplUnit(volt)
   , fDisplayTimeUnit(second)
   , fEditable(false)
   , fAttenuation(1)
   , fDrawnObject(nullptr)
{
   // Waveform normal constructor for fix bin.

   fTimeMax   = binsize * (npoints - 1) + tmin;
   // allocate
   fAmplitude = new Double_t[npoints];
   memset(fAmplitude, 0, sizeof(Double_t)*npoints);
}

//__________________________________________________________________________________
MEGWaveform::MEGWaveform(const MEGWaveform &wf)
   : TNamed(wf), TAttLine(wf), TAttFill(wf), TAttMarker(wf)
   , fMaxNPoints(wf.fMaxNPoints)
   , fNPoints(wf.fNPoints)
   , fBinSize(wf.fBinSize)
   , fFixBinSize(wf.fFixBinSize)
   , fTime(wf.fTime)
   , fID(wf.fID)
   , fOverflow(wf.fOverflow)
   , fTimeMin(wf.fTimeMin)
   , fTimeMax(wf.fTimeMax)
   , fHistogram(0)
   , fGraph(0)
   , fMinimum(wf.fMinimum)
   , fMaximum(wf.fMaximum)
   , fDisplayAmplUnit(wf.fDisplayAmplUnit)
   , fDisplayTimeUnit(wf.fDisplayTimeUnit)
   , fEditable(wf.fEditable)
   , fAttenuation(wf.fAttenuation)
   , fDrawnObject(nullptr)
{
   // Copy constructor for this waveform

   fAmplitude = new Double_t[fMaxNPoints];
   memcpy(fAmplitude, wf.fAmplitude, sizeof(Double_t)*fMaxNPoints);
}

//__________________________________________________________________________________
MEGWaveform::MEGWaveform(const TH1 *h, Double_t *ptime)
   : TNamed(), TAttLine(), TAttFill(), TAttMarker()
   , fTime(ptime)
   , fID(0)
   , fOverflow(false)
   , fHistogram(0)
   , fGraph(0)
   , fMinimum(-1111)
   , fMaximum(-1111)
   , fDisplayAmplUnit(volt)
   , fDisplayTimeUnit(second)
   , fEditable(false)
   , fAttenuation(1)
   , fDrawnObject(nullptr)
{
   // Constructor importing from histogram
   // if ptime=0 (default), waveform is made with fix bin size

   fNPoints   = ((TH1*)h)->GetXaxis()->GetNbins();
   fMaxNPoints = fNPoints;
   fAmplitude = new Double_t[fMaxNPoints];

   Int_t iPnt;
   TAxis *xaxis = ((TH1*)h)->GetXaxis();
   fTimeMin   = xaxis->GetBinCenter(1);
   fTimeMax   = xaxis->GetBinCenter(fNPoints);
   if (ptime) {  // variable bin
      fFixBinSize = false;
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         ptime[iPnt] = xaxis->GetBinCenter(iPnt + 1);
         fAmplitude[iPnt] = h->GetBinContent(iPnt + 1);
      }
   } else {     // fix bin
      fFixBinSize = true;
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         fAmplitude[iPnt] = h->GetBinContent(iPnt + 1);
      }
   }

   if (fNPoints > 1) {
      fBinSize   = (fTimeMax - fTimeMin) / (fNPoints - 1);
   } else {
      fBinSize = 0;
   }

   h->TAttLine::Copy(*this);
   h->TAttFill::Copy(*this);
   h->TAttMarker::Copy(*this);
   SetName(h->GetName());
   SetTitle(h->GetTitle());
}

//__________________________________________________________________________________
MEGWaveform::~MEGWaveform()
{
   // Waveform default destructor

   delete [] fAmplitude;
   fAmplitude = nullptr;

   delete fHistogram;
   fHistogram = nullptr;
   delete fGraph;
   fGraph = nullptr;

   // delete fDrawnObjects;
   // fDrawnObjects = nullptr;
}

//__________________________________________________________________________________
void MEGWaveform::Import(const TH1 *h, Bool_t fixbin)
{
   // Make waveform from histogram

   SetNPoints(((TH1*)h)->GetXaxis()->GetNbins());

   Int_t iPnt;
   TAxis *xaxis = ((TH1*)h)->GetXaxis();
   fTimeMin   = xaxis->GetBinCenter(1);
   fTimeMax   = xaxis->GetBinCenter(fNPoints);
   if (fTime && !fixbin) {  // variable bin
      fFixBinSize = false;
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         fTime[iPnt] = xaxis->GetBinCenter(iPnt + 1);
         fAmplitude[iPnt] = h->GetBinContent(iPnt + 1);
      }
   } else {     // fix bin
      fFixBinSize = true;
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         fAmplitude[iPnt] = h->GetBinContent(iPnt + 1);
      }
   }

   if (fNPoints > 1) {
      fBinSize   = (fTimeMax - fTimeMin) / (fNPoints - 1);
   } else {
      fBinSize = 0;
   }
   fOverflow = false;

   h->TAttLine::Copy(*this);
   h->TAttFill::Copy(*this);
   h->TAttMarker::Copy(*this);
   SetName(h->GetName());
   SetTitle(h->GetTitle());
}

//__________________________________________________________________________________
TH1F *MEGWaveform::Export(Bool_t unitconvert) const
{
   // Make histogram from waveform
   // TH1F object is newly created. User has responsibility of deleting this histogram.

   Double_t *xbins;
   Int_t iPnt;
   TH1F *hnew;
   if (unitconvert) {
      Double_t invTimeUnit = 1. / fDisplayTimeUnit;
      Double_t invAmplUnit = 1. / fDisplayAmplUnit;
      if (!fFixBinSize) {  // variable bin
         xbins = new Double_t[fNPoints + 1];
         xbins[0] = (fTime[0] - fBinSize / 2) * invTimeUnit;
         for (iPnt = 1; iPnt < fNPoints; iPnt++) {
            xbins[iPnt] = (fTime[iPnt - 1] + fTime[iPnt]) / 2 * invTimeUnit;
         }
         xbins[fNPoints] = (fTime[fNPoints - 1] + fBinSize / 2) * invTimeUnit;
         hnew = new TH1F(GetName(), GetTitle(), fNPoints, xbins);
         for (iPnt = 0; iPnt < fNPoints;  iPnt++) {
            hnew->Fill(fTime[iPnt] * invTimeUnit, fAmplitude[iPnt] * invAmplUnit);
         }
         delete [] xbins;
      } else {              // fix bin
         hnew = new TH1F(GetName(), GetTitle(), fNPoints
                         , (fTimeMin - fBinSize / 2) * invTimeUnit, (fTimeMax + fBinSize / 2) * invTimeUnit);
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            hnew->SetBinContent(iPnt + 1, fAmplitude[iPnt] * invAmplUnit);
         }
      }
   } else {
      if (!fFixBinSize) {  // variable bin
         xbins = new Double_t[fNPoints + 1];
         xbins[0] = fTime[0] - fBinSize / 2;
         for (iPnt = 1; iPnt < fNPoints; iPnt++) {
            xbins[iPnt] = (fTime[iPnt - 1] + fTime[iPnt]) / 2;
         }
         xbins[fNPoints] = fTime[fNPoints - 1] + fBinSize / 2;
         hnew = new TH1F(GetName(), GetTitle(), fNPoints, xbins);
         for (iPnt = 0; iPnt < fNPoints;  iPnt++) {
            hnew->Fill(fTime[iPnt], fAmplitude[iPnt]);
         }
         delete [] xbins;
      } else {              // fix bin
         hnew = new TH1F(GetName(), GetTitle(), fNPoints
                         , fTimeMin - fBinSize / 2, fTimeMax + fBinSize / 2);
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            hnew->SetBinContent(iPnt + 1, fAmplitude[iPnt]);
         }
      }
   }
   this->TAttLine::Copy(*hnew);
   this->TAttFill::Copy(*hnew);
   this->TAttMarker::Copy(*hnew);

   if (fHistogram) {
      TString title = GetTitle();
      switch (title.CountChar(';')) {
      case 0:
         title = title + ";" + fHistogram->GetXaxis()->GetTitle() + ";" + fHistogram->GetYaxis()->GetTitle();
         break;
      case 1:
         title = title + ";" + fHistogram->GetYaxis()->GetTitle();
         break;
      case 2:
      default:
         break;
      }
      hnew->SetTitle(title);
   }

   return hnew;
}

//__________________________________________________________________________________
void MEGWaveform::SetNPoints(Int_t npoints)
{
   // Set the number of points

   if (!fAmplitude) {
      fAmplitude = new Double_t[npoints];
      memset(fAmplitude, 0, sizeof(Double_t)*npoints);
      fMaxNPoints = npoints;
   } else if (fNPoints == npoints) {
      return;
   } else if (fMaxNPoints < npoints) {
      Double_t *temp = new Double_t[fNPoints];
      memcpy(temp, fAmplitude, sizeof(Double_t)*fNPoints);
      delete [] fAmplitude;
      fAmplitude = new Double_t[npoints];
      memset(fAmplitude, 0, sizeof(Double_t)*npoints);
      memcpy(fAmplitude, temp, sizeof(Double_t)*fNPoints);
      delete [] temp;
      fMaxNPoints = npoints;
   } else if (fNPoints < npoints) {
      memset(fAmplitude + fNPoints, 0, sizeof(Double_t) * (npoints - fNPoints));
   }
//    else {
//       Int_t startpnt = (fNPoints <  npoints) ? fNPoints : npoints;
//       memset(fAmplitude+startpnt, 0, sizeof(Double_t)*(fMaxNPoints-fNPoints));
//    }
   fNPoints = npoints;
   fTimeMax = fBinSize * (npoints - 1) + fTimeMin;
}

//__________________________________________________________________________________
void MEGWaveform::SetFixBinSize(Bool_t flag, Double_t* ptime)
{
   // Set the bin type of this waveform

   if (flag == fFixBinSize) {
      return;
   }
   Int_t iPnt;
   if (flag) {
      fTimeMin = GetTimeAt(0);
      fTimeMax = fBinSize * (fNPoints - 1) + fTimeMin;
      fFixBinSize = true;
   } else {
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         ptime[iPnt] = iPnt * fBinSize + fTimeMin;
      }
      fTime = ptime;
      fFixBinSize = false;
   }
}

//__________________________________________________________________________________
void MEGWaveform::SetTitle(const char *title)
{
   // Set title of this object

   fTitle = title;
   if (fHistogram) {
      fHistogram->SetTitle(title);
   }
}

//__________________________________________________________________________________
void MEGWaveform::SetTime(const MEGWaveform *wfin)
{
   // Set time information from input waveform object

   SetNPoints(wfin->fNPoints);
   fBinSize    = wfin->fBinSize;
   fFixBinSize = wfin->fFixBinSize;
   fTimeMin    = wfin->fTimeMin;
   fTimeMax    = wfin->fTimeMax;
   fTime       = wfin->fTime;
}

//__________________________________________________________________________________
void MEGWaveform::SetTime(Double_t *ptime)
{
   // Set time information of each point (variable bin size mode)

   fTime = ptime;
   fFixBinSize = false;
}

//__________________________________________________________________________________
void MEGWaveform::SetTime(Double_t tmin, Double_t tmax)
{
   // Set time information for fix bin size mode

   fFixBinSize = true;
   fTimeMin = tmin;
   if (fNPoints > 1) {
      fTimeMax = tmax;
      fBinSize = (tmax - tmin) / (fNPoints - 1);
   } else {
      fTimeMax = tmin;
      fBinSize = 0;
   }
}

//__________________________________________________________________________________
void MEGWaveform::SetTimeMin(Double_t timemin)
{
   // Set time of first point (fix bin size mode)

   fTimeMin = timemin;
   fTimeMax = fBinSize * (fNPoints - 1) + fTimeMin;
   fFixBinSize = true;
}

//__________________________________________________________________________________
void MEGWaveform::SetAmplitude(Double_t* amplitude)
{
   // Copy amplitude

   if (amplitude != fAmplitude) {
      memcpy(fAmplitude, amplitude, sizeof(Double_t)*fNPoints);
   }
}

//__________________________________________________________________________________
void MEGWaveform::Draw(Option_t *option, Double_t tstart, Double_t tend)
{
   // Draw this waveform with its current attributes.

   TString opt = option;
   opt.ToLower();

   if (opt.Contains("zoom") || !opt.Contains("hist")) {
      Int_t iPnt;
      Double_t *ptime = 0;
      Double_t *amplitude = 0;
      if (fDisplayAmplUnit != 1.) {
         amplitude = new Double_t[fNPoints];
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            amplitude[iPnt] = fAmplitude[iPnt] / fDisplayAmplUnit;
         }
      } else {
         amplitude = fAmplitude;
      }
      if (fFixBinSize) {
         ptime = new Double_t[fNPoints];
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            ptime[iPnt] = (iPnt * fBinSize + fTimeMin) / fDisplayTimeUnit;
         }
      } else if (fDisplayTimeUnit != 1.) {
         ptime = new Double_t[fNPoints];
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            ptime[iPnt] = fTime[iPnt] / fDisplayTimeUnit;
         }
      } else {
         ptime = fTime;
      }


      Int_t npt;
      Double_t uxmin, uxmax;
      Double_t rwxmin, rwxmax, rwymin, rwymax, maximum, minimum, dy;
      // Draw the Axis.
      if (!opt.Contains("same")) {
         if (opt.Contains("zoom")) {
            rwxmin = 999999;
            rwxmax = -999999;
         } else {
            rwxmin = rwxmax = ptime[0];
         }
         rwymin = rwymax = amplitude[0];
         for (iPnt = 1; iPnt < fNPoints; iPnt++) {
            if (opt.Contains("zoom")) {
               if (ptime[iPnt] < rwxmin && amplitude[iPnt] != 0.) {
                  rwxmin = ptime[iPnt] - fBinSize;
               }
               if (ptime[iPnt] > rwxmax && TMath::Abs(amplitude[iPnt]) > rwymax * 1e-6) {
                  rwxmax = ptime[iPnt] + fBinSize;
               }
            } else if (tend > tstart + fBinSize) {
               rwxmin = tstart / fDisplayTimeUnit;
               rwxmax = tend / fDisplayTimeUnit;
            } else {
               if (ptime[iPnt] < rwxmin) {
                  rwxmin = ptime[iPnt] - fBinSize;
               }
               if (ptime[iPnt] > rwxmax) {
                  rwxmax = ptime[iPnt] + fBinSize;
               }
            }
            if (amplitude[iPnt] < rwymin) {
               rwymin = amplitude[iPnt];
            }
            if (amplitude[iPnt] > rwymax) {
               rwymax = amplitude[iPnt];
            }
         }
         if (rwxmax < rwxmin) {
            rwxmin = 0;
            rwxmax = 1;
         }
         if (rwxmin == rwxmax) {
            rwxmax += 1.;
         }
         if (rwymin == rwymax) {
            rwymax += 1.;
         }
         //dx = 0.1*(rwxmax-rwxmin);
         dy = 0.1 * (rwymax - rwymin);
         uxmin    = rwxmin;
         uxmax    = rwxmax;
         minimum  = rwymin - dy;
         maximum  = rwymax + dy;


         if (fMinimum != -1111) {
            rwymin = minimum = fMinimum;
         }
         if (fMaximum != -1111) {
            rwymax = maximum = fMaximum;
         }
         if (!gPad) {
            gPad = new TCanvas;
         }
         if (minimum < 0 && rwymin >= 0) {
            if (gPad->GetLogy()) {
               minimum = 0.9 * rwymin;
            } else {
               minimum = 0;
            }
         }
         if (minimum <= 0 && gPad->GetLogy()) {
            minimum = 0.001 * maximum;
         }
         if (uxmin <= 0 && gPad->GetLogx()) {
            if (uxmax > 1000) {
               uxmin = 1;
            } else {
               uxmin = 0.001 * uxmax;
            }
         }
         rwymin = minimum;
         rwymax = maximum;
         npt = 100;
         // Create a temporary histogram
         char chopth[8] = "";
         if (strstr(option, "x+")) {
            strcat(chopth, "x+");
         }
         if (strstr(option, "y+")) {
            strcat(chopth, "y+");
         }
         if (!fHistogram) {
            // the graph is created with at least as many channels as there are
            // points to permit zooming on the full range.
            rwxmin = uxmin;
            rwxmax = uxmax;
            fHistogram = new TH1F(Form("%s_h", GetName()), GetTitle(), npt, rwxmin, rwxmax);
            if (!fHistogram) {
               return;
            }
            fHistogram->SetMinimum(rwymin);
            fHistogram->SetMaximum(rwymax);
            fHistogram->GetYaxis()->SetLimits(rwymin, rwymax);
            fHistogram->SetBit(TH1::kNoStats);
            fHistogram->SetDirectory(0);
//         fHistogram->Paint(chopth); // Draw histogram axis, title and grid
            fHistogram->Draw(chopth); // Draw histogram axis, title and grid
         } else {
//         if (gPad->GetLogy()) {
            fHistogram->SetBins(npt, rwxmin, rwxmax);
            fHistogram->SetMinimum(rwymin);
            fHistogram->SetMaximum(rwymax);
            fHistogram->GetYaxis()->SetLimits(rwymin, rwymax);
//         }
//         fHistogram->Paint(chopth); // Draw histogram axis, title and grid
            fHistogram->Draw(chopth); // Draw histogram axis, title and grid
         }
      }

      TGraph *newgraph(nullptr);
      if (!newgraph) {
         newgraph = new TGraph(fNPoints, ptime, amplitude);
         fDrawnObject = newgraph;
         newgraph->SetTitle(GetTitle());
         newgraph->SetName(Form("%s_gr", GetName()));
      }
      TAttLine::Copy(*newgraph);
      TAttFill::Copy(*newgraph);
      TAttMarker::Copy(*newgraph);
      newgraph->SetBit(kCanDelete);
      newgraph->SetEditable(fEditable);
      if (opt.Contains("hist")) {
         newgraph->AppendPad("B");
      } else {
         newgraph->AppendPad("L");
      }

      if (fDisplayAmplUnit != 1.) {
         delete [] amplitude;
      }
      if (fFixBinSize || fDisplayTimeUnit != 1.) {
         delete [] ptime;
      }
   } else {
      Bool_t unitConvert = false;
      if (fDisplayAmplUnit != 1. || fDisplayTimeUnit != 1.) {
         unitConvert = true;
      }
      TH1F *newhist = Export(unitConvert);
      TH1F *hist(nullptr);
      if (!hist) {
         hist = newhist;
         hist->SetName(Form("%sHist", hist->GetName()));
         fDrawnObject = hist;
      }
      hist->SetBit(kCanDelete);
      hist->Draw(opt.Data());
   }
}

//__________________________________________________________________________________
void MEGWaveform::Pop()
{
   // Pop on object drawn in a pad to the top of the display list. I.e. it
   // will be drawn last and on top of all other primitives.
   
   if (fDrawnObject) {
      fDrawnObject->Pop();
   }
}

//__________________________________________________________________________________
TAxis *MEGWaveform::GetXaxis()
{
   // return a pointer to the X axis of frame histogram

   if (!fHistogram) {
      fHistogram = new TH1F();
      fHistogram->SetName(Form("%s_h", GetName()));
      fHistogram->SetTitle(GetTitle());
   }
   return fHistogram->GetXaxis();
}

//__________________________________________________________________________________
TAxis *MEGWaveform::GetYaxis()
{
   // return a pointer to the Y axis of frame histogram

   if (!fHistogram) {
      fHistogram = new TH1F();
      fHistogram->SetName(Form("%s_h", GetName()));
      fHistogram->SetTitle(GetTitle());
   }
   return fHistogram->GetYaxis();
}

//__________________________________________________________________________________
void MEGWaveform::Reset()
{
   // Reset this waveform

   if (fAmplitude) {
      memset(fAmplitude, 0, fNPoints * sizeof(Double_t));
   }
   if (fTime) {
      memset(fTime, 0, fNPoints * sizeof(Double_t));
   }
}

//__________________________________________________________________________________
Int_t MEGWaveform::FindPoint(Double_t x, Int_t startpoint, Int_t lastpoint) const
{
   // Find point which is maxmum one under x
   // -1 : underflow
   //  0 : first point
   // ,,,
   // fNPoints - 1 : last bin (overflow)
   //
   // cell#     0   1   2   3   4           fNPoints-1
   //         --*---*---*---*---*------*---*---*--
   // return -1   0   1   2   3   4             fNPoints-1
   //
   // In case of variable bin size, use a binary search algorithm.

   Int_t point;
   if (fFixBinSize) {        // fix bins
      if (x < fTimeMin) {
         point = -1;
      } else  if (x > fTimeMax) {
         point = fNPoints - 1;
      } else {
         point = static_cast<int>((fNPoints - 1) * (x - fTimeMin) / (fTimeMax - fTimeMin));
      }
   } else {         //Binary search
      if (startpoint < 0) {
         startpoint = 0;
      } else if (startpoint > fNPoints - 1) {
         startpoint = fNPoints - 1;
      }
      if (lastpoint < startpoint) {
         lastpoint = startpoint;
      } else if (lastpoint > fNPoints - 1) {
         lastpoint = fNPoints - 1;
      }
      if (x < fTime[startpoint]) {
         point = -1;
      } else {
         point = static_cast<int>(TMath::BinarySearch(lastpoint - startpoint + 1, fTime + startpoint, x)) + startpoint;
      }
   }
   return point;
}

//__________________________________________________________________________________
Int_t MEGWaveform::FindPointIncrement(Double_t x, Int_t startpoint) const
{
   // Find point which is maxmum one under x 
   // by incrementing the point from 'startpoint' (variable bin size case)
   //
   // -1 : underflow
   //  0 : first point
   // ,,,
   // fNPoints - 1: last bin (overflow)
   //
   // cell#     0   1   2   3   4           fNPoints-1
   //         --*---*---*---*---*------*---*---*--
   // return -1   0   1   2   3   4             fNPoints-1
   //

   Int_t point;
   if (fFixBinSize) {
      return FindPoint(x, startpoint);
   }

   if (startpoint < 0) {
      startpoint = 0;
   } else if (startpoint > fNPoints - 1) {
      startpoint = fNPoints - 1;
   }

   if (x < fTime[startpoint]) {
      point = -1;
   } else if (x > fTimeMax) {
      point = fNPoints - 1;
   } else {
      for (point = startpoint + 1; point < fNPoints; point++) {
         if (x < fTime[point]) {
            break;
         }
      }
      point--;
   }
   return point;
}

//__________________________________________________________________________________
Int_t MEGWaveform::FindPointDecrement(Double_t x, Int_t startpoint) const
{
   // Find point which is maxmum one under x 
   // by decrementing the point from 'startpoint' (variable bin size case)
   //
   // -1 : underflow
   //  0 : first point
   // ,,,
   // fNPoints - 1 : last bin (overflow)
   //
   // cell#     0   1   2   3   4           fNPoints-1
   //         --*---*---*---*---*------*---*---*--
   // return -1   0   1   2   3   4             fNPoints-1
   //

   Int_t point;
   if (fFixBinSize) {
      return FindPoint(x, startpoint);
   }

   if (startpoint < 0) {
      startpoint = 0;
   } else if (startpoint > fNPoints - 1) {
      startpoint = fNPoints - 1;
   }
   
   if (x >= fTime[startpoint]) {
      point = startpoint;
   } else if (x < fTimeMin) {
      point = -1;
   } else {
      for (point = startpoint - 1; point >= 0; point--) {
         if (x >= fTime[point]) {
            break;
         }
      }
   }
   return point;
}

//__________________________________________________________________________________
Double_t MEGWaveform::NewtonRaphson(void (*func)(Double_t*, Double_t, Double_t&, Double_t&), Double_t* coef,
                                    Double_t xini, Double_t xmin, Double_t xmax, Double_t xacc, Int_t imax)
{
   // Root finding method by Newton-Raphson method
   // "func" whose root is known to lie between xmin and xmax returns both function value
   //  and the first derivative of the function at x.

   Int_t iIt;
   Double_t df, dx, f, rtn;

   rtn = xini; // initial guess
   for (iIt = 0; iIt < imax; iIt++) { // iterate until converge within xacc
      (*func)(coef, rtn, f, df);
      dx = f / df;
//       cout<<" NN "<<rtn<<" "<<f<<" "<<df<<" "<<dx<<" "<<rtn-dx<<endl;
      rtn -= dx;
//       if ((xmin - rtn) * (rtn - xmax) < 0.)
//          cerr<<"MEGWaveform::NewtonRaphson : Jumped out of limit"<<endl;
      if (TMath::Abs(dx) < xacc) {
         return rtn;
      }
   }
   Report(R_WARNING, "MEGWaveform::NewtonRaphson : Maximum number of iterations exceeded.");
   return ((xmin - rtn) * (rtn - xmax) < 0.) ? xini : rtn;
}

//__________________________________________________________________________________
Int_t MEGWaveform::PolInt(const Double_t xa[], const Double_t ya[], Int_t n, Double_t x, Double_t &y,
                          Double_t &dy)
{
   // Calculate interpolated value and its error by polynomial interpolation
   // Using Neville's algorithm.
   // Input:
   //       "n"  : Number of points. Interpolating polynomial degree n-1.
   //   "xa","ya": n points interpolating polynomial through

   Int_t iPnt, iDeg, cPoint = 0;
   Double_t den, dif, difTemp, ho, hp, w;
   Double_t *c = 0, *d = 0;

   dif = TMath::Abs(x - xa[0]);
   c = new Double_t[n];
   d = new Double_t[n];
   for (iPnt = 0; iPnt < n; iPnt++) {
      if ((difTemp = TMath::Abs(x - xa[iPnt])) < dif) {
         cPoint = iPnt;                        // find closest point
         dif = difTemp;
      }
      c[iPnt] = ya[iPnt];                      // initialize the tableau of c's and d's
      d[iPnt] = ya[iPnt];
   }

   y = ya[cPoint--];                           // initial approximation to y value

   for (iDeg = 1; iDeg < n; iDeg++) {
      for (iPnt = 0; iPnt < n - iDeg; iPnt++) {
         ho = xa[iPnt] - x;
         hp = xa[iPnt + iDeg] - x;
         w = c[iPnt + 1] - d[iPnt];
         if ((den = ho - hp) == 0.0) {
            cerr << "MEGWaveform::PolInt()" << endl; // error when two input xa are identical
            return -1;
         }
         den = w / den;
         d[iPnt] = hp * den;
         c[iPnt] = ho * den;
      }

      y += (dy = (2 * (cPoint + 1) < (n - iDeg) ? c[cPoint + 1] : d[cPoint--]));
   }

   delete [] c;
   delete [] d;

   return 1;
}

//__________________________________________________________________________________
void MEGWaveform::PolCoe(const Double_t xa[], const Double_t ya[], Int_t n, Double_t coef[])
{
   // Calculate coefficients of interpolating polynomial
   //    ya[i] = Sigma (coef[j] * xa[i]^j)
   //
   // *** Temporary version (Only for n=4) ***
   //
   // Input:
   //       "n"  : Number of points. Interpolating polynomial degree n-1.
   //   "xa","ya": n points interpolating polynomial through

//    Int_t iDeg , jDeg, iPnt;
//    Double_t phi, ff, b, *coefmaster = 0;

//    coefmaster = new Double_t[n];

//    for (iPnt = 0; iPnt < n; iPnt++)
//       coefmaster[iPnt] = coef[iPnt] = 0.;
//    coefmaster[n-1] = -xa[0];
//    for (iPnt = 1; iPnt < n; iPnt++) {
//       for (jDeg = n-iPnt; jDeg < n-1; jDeg++)
//          coefmaster[jDeg] -= xa[iPnt] * coefmaster[jDeg+1];
//       coefmaster[n-1] -= xa[iPnt];
//    }
//    for (jDeg = 0; jDeg < n; jDeg++) {
//       phi = n;
//       for (iDeg = n-1; iDeg > 0; iDeg--)
//          phi = iDeg * coefmaster[iDeg] + xa[jDeg] * phi;
//       ff = ya[jDeg] / phi;
//       b = 1.0;
//       for (iDeg = n-1; iDeg > 0; iDeg--) {
//          coef[iDeg] += b * ff;
//          b = coefmaster[iDeg] + xa[jDeg] * b;
//       }
//    }
//    delete [] coefmaster;

   Int_t iDeg, jPnt, lPnt;
   vector<Double_t> wa(n, 0.);
   vector<Double_t> ua(n, 0.);
   memset(coef, 0, sizeof(Double_t) * n);
   for (iDeg = 0; iDeg < n; iDeg++) {
      wa[iDeg] = ya[iDeg];

      for (jPnt = 0; jPnt < n; jPnt++) {
         if (jPnt != iDeg) {
            wa[iDeg] /= xa[iDeg] - xa[jPnt];
         }
      }
   }

   // 0
   for (iDeg = 0; iDeg < n; iDeg++) {
      ua[iDeg] = wa[iDeg];
      for (jPnt = 0; jPnt < n; jPnt++) {
         if (jPnt != iDeg) {
            ua[iDeg] *= xa[jPnt];
         }
      }
      coef[0] -= ua[iDeg];
   }

   // 1
   memset(&ua[0], 0, sizeof(Double_t) * n);
   for (iDeg = 0; iDeg < n; iDeg++) {
      for (jPnt = 0; jPnt < n; jPnt++) {
         for (lPnt = jPnt + 1; lPnt < n; lPnt++) {
            if (jPnt != iDeg && lPnt != iDeg) {
               ua[iDeg] += xa[jPnt] * xa[lPnt];
            }
         }
      }
      coef[1] += ua[iDeg] * wa[iDeg];
   }

   // 2
   memset(&ua[0], 0, sizeof(Double_t) * n);
   for (iDeg = 0; iDeg < n; iDeg++) {
      for (jPnt = 0; jPnt < n; jPnt++) {
         if (jPnt != iDeg) {
            ua[iDeg] += xa[jPnt];
         }
      }
      coef[2] -= ua[iDeg] * wa[iDeg];
   }

   // 3
   for (iDeg = 0; iDeg < n; iDeg++) {
      coef[3] += wa[iDeg];
   }

   return;
}

//__________________________________________________________________________________
Double_t MEGWaveform::InterpolateAmplitude(Double_t x, Int_t n) const
{
   // Interpolate amplitude (currently only by polynomial)
   // "n" is number of points used for interpolation. n-1 degree of polynomial.
   //    2, default:   linear interpolation
   //    3         :   parabolic interpolation
   //    4         :   cubic interpolation

   Int_t startpnt = FindPoint(x);

   if (n % 2 == 0) { // even
      startpnt -= (n - 1) / 2;
   } else {        // odd
      if (GetTimeAt(startpnt + (n + 1) / 2) - x > x - GetTimeAt(startpnt - (n - 1) / 2)) {
         startpnt -= (n - 1) / 2;
      } else {
         startpnt -= (n - 2) / 2;
      }
   }
   if (startpnt < 0) {
      startpnt = 0;
   }
   if (startpnt + n > fNPoints) {
      startpnt = fNPoints - n;
   }

   Int_t iPnt;
   Double_t amplint, error;
   Double_t *time = 0, *amplitude = 0;
   if (fFixBinSize) {
      time = new Double_t[n];
      for (iPnt = 0; iPnt < n; iPnt++) {
         time[iPnt] = GetTimeAt(startpnt + iPnt);
      }
   } else {
      time = fTime + startpnt;
   }
   amplitude = fAmplitude + startpnt;

   Int_t result = PolInt(time, amplitude, n, x, amplint, error);

   if (result < 0) { // in case of failure
      startpnt = FindPoint(x);
      if (startpnt < 0) {
         startpnt = 0;
      } else if (startpnt > fNPoints - 1) {
         startpnt = fNPoints - 1;
      }
      amplint = fAmplitude[startpnt];
   }

   if (fFixBinSize) {
      delete [] time;
   }

   return amplint;
}


//__________________________________________________________________________________
Double_t MEGWaveform::InterpolateTime(Double_t y, Int_t point, Double_t xini,
                                      Double_t /*xacc*/, Int_t /*imax*/) const
{
   // Cubic interpolation of time crossing threshold "y".

   // Obtain the coefficients of interpolating polynomial
   Double_t coef[4];
   Int_t startpnt = point;
   startpnt -= 1;
   if (startpnt < 0) {
      startpnt = 0;
   }
   if (startpnt + 4 > fNPoints) {
      startpnt = fNPoints - 4;
   }
   Int_t iPnt;
   // Double_t *time = 0, *amplitude = 0;
   // time = new Double_t[4];
   Double_t *amplitude(0);
   vector<Double_t> time(4, 0);
   Double_t time0 = GetTimeAt(startpnt + 1);
   for (iPnt = 0; iPnt < 4; iPnt++) {
      time[iPnt] = GetTimeAt(startpnt + iPnt) - time0;
   }

   // check time[] is proper
   for (iPnt = 1; iPnt < 4; iPnt++) {
      if (time[iPnt] <= time[iPnt - 1]) {
         // Bad time for the sample points
         throw 1;
      }
   }

#define MY_ROOTSCUBIC
#ifdef MY_ROOTSCUBIC
   Double_t timeScale = TMath::Power(10, -static_cast<Int_t>(TMath::Log10(time[3])));
   time0 *= timeScale;
   for (iPnt = 0; iPnt < 4; iPnt++) {
      time[iPnt] *= timeScale;
   }
   xini *= timeScale;
#else
   Double_t timeScale = 1;
#endif

   amplitude = fAmplitude + startpnt;
   PolCoe(&time[0], amplitude, 4, coef);
   // Solve the equation, y = f(x)
   coef[0] -= y;
#if 0
   Double_t xmin = (GetTimeAt(point) - time0) * timeScale;
   Double_t xmax = (GetTimeAt(point + 1) - time0) * timeScale;
   // cout<<"COEF "<<coef[0]<<" "<<coef[1]<<" "<<coef[2]<<" "<<coef[3]<<endl;

//    Double_t test, dte;Pol3(coef, xmin, test, dte);
//    cout<<" AFTERER1 "<<test<<" "<<xmin<<endl;
//    Pol3(coef, xmax, test, dte);
//    cout<<" AFTERER2 "<<test<<" "<<xmax<<endl;
//    Pol3(coef, xini-time0, test, dte);
//    cout<<" AFTERER3 "<<test<<" "<<xini-time0<<endl;

   Double_t troot = NewtonRaphson(Pol3, coef, xini - time0, xmin, xmax, xacc, imax);
   return (troot + time0) / timeScale;
#endif

   /* If coef[3] is zero function RootsCubic will crash.
   If it is too small, result of RootsCubic becomes non-reliable.*/
   // if (coef[3] == 0)
#ifdef MY_ROOTSCUBIC
   if (TMath::Abs(coef[3]) < 1e-7) {
      return xini / timeScale;
   }
#else
   if (TMath::Abs(coef[3]) < 1e20) {
      return xini / timeScale;
   }
#endif

   /*If coef[1] / coef[3] and pow(coef[2] / coef[3],2.0) /3. takes similar value,
   RootsCubic will crash by calculating Log(0) (maybe by underflow).*/
   //cout<<TMath::Abs(coef[1] / coef[3] - pow(coef[2] / coef[3],2.0) /3.)<<endl;
#ifdef MY_ROOTSCUBIC
   if (TMath::Abs(coef[1] / coef[3] - pow(coef[2] / coef[3], 2.0) / 3.) < 1e-12) {
      return xini / timeScale;
   }
#else
   if (TMath::Abs(coef[1] / coef[3] - pow(coef[2] / coef[3], 2.0) / 3.) < 1e-30) {
      return xini / timeScale;
   }
#endif
   Double_t troot[3];
#if 0
// #ifdef MY_ROOTSCUBIC
   Bool_t complexroot = TMath::RootsCubic(coef, troot[0], troot[1], troot[2]);
#else
   Bool_t complexroot = RootsCubic(coef, troot[0], troot[1], troot[2]);
#endif
   if (complexroot) { // one real root
      // check timing
      if (troot[0] < time[0] || troot[0] > time[3]) {
//          Report(R_WARNING, "Cubic interpolation seems bad");
         return xini / timeScale;
      }
      return (troot[0] + time0) / timeScale;

   } else { // three real roots
      Double_t delta = kInvalidTime;
      Int_t nearestRoot = 0;
      for (Int_t iRoot = 0; iRoot < 3; iRoot++) {
         if (TMath::Abs(xini - time0 - troot[iRoot]) < delta) {
            delta = TMath::Abs(xini - time0 - troot[iRoot]);
            nearestRoot = iRoot;
         }
      }
      return (troot[nearestRoot] + time0) / timeScale;
   }
}

//__________________________________________________________________________________
Bool_t MEGWaveform::RootsCubic(const Double_t coef[4], Double_t &a, Double_t &b, Double_t &c) const
{
   // Calculates roots of polynomial of 3rd order a*x^3 + b*x^2 + c*x + d, where
   // a == coef[3], b == coef[2], c == coef[1], d == coef[0]
   //coef[3] must be different from 0
   // If the boolean returned by the method is false:
   //    ==> there are 3 real roots a,b,c
   // If the boolean returned by the method is true:
   //    ==> there is one real root a and 2 complex conjugates roots (b+i*c,b-i*c)
   // Author: Francois-Xavier Gentit

   Bool_t complex = kFALSE;
   Double_t r, s, t, p, q, d, ps3, ps33, qs2, u, v, tmp, lnu, lnv, su, sv, y1, y2, y3;
   a    = 0;
   b    = 0;
   c    = 0;
   if (coef[3] == 0) {
      return complex;
   }
   r    = coef[2] / coef[3];
   s    = coef[1] / coef[3];
   t    = coef[0] / coef[3];
   p    = s - (r * r) / 3;
   ps3  = p / 3;
   q    = (2 * r * r * r) / 27.0 - (r * s) / 3 + t;
   qs2  = q / 2;
   ps33 = ps3 * ps3 * ps3;
   d    = ps33 + qs2 * qs2;
   if (d >= 0) {
      complex = kTRUE;
      d   = TMath::Sqrt(d);
      u   = -qs2 + d;
      v   = -qs2 - d;
      tmp = 1. / 3.;
      // Avoid 0 due to rounding
      if (u == 0) {
         u = 1e-100;
      }
      if (v == 0) {
         v = 1e-100;
      }
      lnu = TMath::Log(TMath::Abs(u));
      lnv = TMath::Log(TMath::Abs(v));
      su  = TMath::Sign(1., u);
      sv  = TMath::Sign(1., v);
      u   = su * TMath::Exp(tmp * lnu);
      v   = sv * TMath::Exp(tmp * lnv);
      y1  = u + v;
      y2  = -y1 / 2;
      y3  = ((u - v) * TMath::Sqrt(3.)) / 2;
      tmp = r / 3;
      a   = y1 - tmp;
      b   = y2 - tmp;
      c   = y3;
   } else {
      Double_t phi, cphi, phis3, c1, c2, c3, pis3;
      ps3   = -ps3;
      ps33  = -ps33;
      cphi  = -qs2 / TMath::Sqrt(ps33);
      phi   = TMath::ACos(cphi);
      phis3 = phi / 3;
      pis3  = TMath::Pi() / 3;
      c1    = TMath::Cos(phis3);
      c2    = TMath::Cos(pis3 + phis3);
      c3    = TMath::Cos(pis3 - phis3);
      tmp   = TMath::Sqrt(ps3);
      y1    = 2 * tmp * c1;
      y2    = -2 * tmp * c2;
      y3    = -2 * tmp * c3;
      tmp = r / 3;
      a   = y1 - tmp;
      b   = y2 - tmp;
      c   = y3 - tmp;
   }
   return complex;
}

//__________________________________________________________________________________
Double_t MEGWaveform::MinimumPeak(Double_t tstart, Double_t tend) const
{
   Double_t peak = 0.;
   Int_t peakpoint = 0;
   this->MinimumPeak(peak, peakpoint, tstart, tend);
   return peak;
}

//__________________________________________________________________________________
void MEGWaveform::MinimumPeak(Double_t &peak, Int_t &peakpoint, Double_t tstart, Double_t tend) const
{
   // Search for peak for negative pulse
   // Output :
   //   "peak"        the peak amplitude value,
   //   "peakpoint"   the peak position (point number).

   Int_t startpnt, endpnt;
   if (tstart == tend) {
      startpnt = 0;
      endpnt = fNPoints;
   } else {
      startpnt = FindPoint(tstart) + 1;
      endpnt = FindPoint(tend) + 1;
      if (startpnt >= endpnt) {
         endpnt = startpnt;
      }
   }
   Double_t *peakPointer = min_element(fAmplitude + startpnt, fAmplitude + endpnt);
   peak = *peakPointer;
   peakpoint = peakPointer - fAmplitude;
}

//__________________________________________________________________________________
Double_t MEGWaveform::MaximumPeak(Double_t tstart, Double_t tend) const
{
   Double_t peak = 0.;
   Int_t peakpoint = 0;
   this->MaximumPeak(peak, peakpoint, tstart, tend);
   return peak;
}

//__________________________________________________________________________________
void MEGWaveform::MaximumPeak(Double_t &peak, Int_t &peakpoint, Double_t tstart, Double_t tend) const
{
   // Search for peak for positive pulse
   // Output :
   //   "peak"        the peak amplitude value,
   //   "peakpoint"   the peak position (point number).

   Int_t startpnt, endpnt;
   if (tstart == tend) {
      startpnt = 0;
      endpnt = fNPoints;
   } else {
      startpnt = FindPoint(tstart) + 1;
      endpnt = FindPoint(tend) + 1;
      if (startpnt >= endpnt) {
         endpnt = startpnt;
      }
   }

   Double_t *peakPointer = max_element(fAmplitude + startpnt, fAmplitude + endpnt);
   peak = *peakPointer;
   peakpoint = peakPointer - fAmplitude;
}

//__________________________________________________________________________________
void MEGWaveform::GetBothPeak(Double_t* peak) const
{
   Int_t pnt;
   this->MinimumPeak(peak[0], pnt, this->GetTimeMin(), this->GetTimeMax());
   this->MaximumPeak(peak[1], pnt, this->GetTimeMin(), this->GetTimeMax());
   return;
}

//__________________________________________________________________________________
Double_t MEGWaveform::PeakToPeak(Double_t tstart, Double_t tend) const
{
   // Return the peak to peak amplitude.

   Int_t startpnt;
   Int_t endpnt;
   if (tstart == tend) {
      startpnt = 0;
      endpnt = fNPoints;
   } else {
      startpnt = FindPoint(tstart) + 1;
      endpnt = FindPoint(tend) + 1;
      if (startpnt >= endpnt) {
         return 0.;
      }
   }
   Double_t p2p = *max_element(fAmplitude + startpnt, fAmplitude + endpnt)
                  - *min_element(fAmplitude + startpnt, fAmplitude + endpnt);
   return p2p;
}

//__________________________________________________________________________________
Int_t MEGWaveform::PeakSearch(Double_t *peak, Int_t *peakpoint, Int_t nmax, Double_t tstart, Double_t tend,
                              Double_t threshold, Int_t local, Int_t polarity) const
{
   // Search for peaks
   // Return the number of peaks.
   // Output :
   //  "peak"       peak values.
   //  "peakpoint"  peak positions (point number)
   // Input :
   //  "nmax"         maximum number of peaks to search
   //  "tstart""tend" search region
   //  "threshold"    threshold for peak
   //  "local"        judge as peak if the point is an extremal value in the local region (half width)

   Int_t  iPnt, jPnt;
   Int_t  npeak = 0;
   Int_t  startpoint = FindPoint(tstart) + 1;
//    Int_t  endpoint   = FindPoint(tend);
   Int_t  endpoint   = FindPoint(tend) + 1;
   startpoint = (startpoint - local < 0) ? local : startpoint;
//    endpoint = (endpoint+local > fNPoints-1) ? fNPoints-1-local : endpoint;
   endpoint = (endpoint + local > fNPoints) ? fNPoints - local : endpoint;
   Int_t  flag;
   iPnt = startpoint;

   if (polarity > 0) { // local maximum
      while (iPnt < endpoint && fAmplitude[iPnt] < threshold) {
         iPnt++;
      }
      if (iPnt == endpoint) {
         return npeak;
      }

      while (iPnt < startpoint + local) {
         flag = 1;
         for (jPnt = 0; jPnt < local; jPnt++) {
            if (fAmplitude[iPnt + jPnt + 1] > fAmplitude[iPnt]) {
               flag = 0;
               iPnt += jPnt + 1;
               break;
            }
         }
         if (flag) {
            if (fAmplitude[iPnt] >= threshold) {
               npeak++;
               if (npeak <= nmax) {
                  peak[npeak - 1] = fAmplitude[iPnt];
                  peakpoint[npeak - 1] = iPnt;
               }
            }
            iPnt = iPnt + local + 1;
         }
      }
      while (iPnt < endpoint - local) {
         flag = 1;
         for (jPnt = 0; jPnt < local; jPnt++) {
            if (fAmplitude[iPnt + jPnt + 1] > fAmplitude[iPnt]) {
               flag = 0;
               iPnt += jPnt + 1;
               break;
            }
            if (fAmplitude[iPnt - local + jPnt] > fAmplitude[iPnt]) {
               flag = 0;
               iPnt++;
               break;
            }
         }
         if (flag) {
            if (fAmplitude[iPnt] >= threshold) {
               npeak++;
               if (npeak <= nmax) {
                  peak[npeak - 1] = fAmplitude[iPnt];
                  peakpoint[npeak - 1] = iPnt;
               }
               flag = 1;
            }
            iPnt = iPnt + local + 1;
         }
      }
      if (iPnt > endpoint) {
         iPnt = endpoint;
      }

      flag = 1;
      Double_t *peakPointer = max_element(fAmplitude + iPnt, fAmplitude + endpoint);
      iPnt = peakPointer - fAmplitude;
      for (jPnt = iPnt - local; jPnt < iPnt; jPnt++) {
         if (fAmplitude[jPnt] > fAmplitude[iPnt]) {
            flag = 0;
            break;
         }
      }

      if (flag && fAmplitude[iPnt] >= threshold) {
         npeak++;
         if (npeak <= nmax) {
            peak[npeak - 1] = fAmplitude[iPnt];
            peakpoint[npeak - 1] = iPnt;
         }
      }


   } else {          // local minimum

      while (iPnt < endpoint && fAmplitude[iPnt] > threshold) {
         iPnt++;
      }
      if (iPnt == endpoint) {
         return npeak;
      }

      while (iPnt < startpoint + local) {
         flag = 1;
         for (jPnt = 0; jPnt < local; jPnt++) {
            if (fAmplitude[iPnt + jPnt + 1] < fAmplitude[iPnt]) {
               flag = 0;
               iPnt += jPnt + 1;
               break;
            }
         }
         if (flag) {
            if (fAmplitude[iPnt] <= threshold) {
               npeak++;
               if (npeak <= nmax) {
                  peak[npeak - 1] = fAmplitude[iPnt];
                  peakpoint[npeak - 1] = iPnt;
               }
            }
            iPnt = iPnt + local + 1;
         }
      }
      while (iPnt < endpoint - local) {
         flag = 1;
         for (jPnt = 0; jPnt < local; jPnt++) {
            if (fAmplitude[iPnt + jPnt + 1] < fAmplitude[iPnt]) {
               flag = 0;
               iPnt += jPnt + 1;
               break;
            }
            if (fAmplitude[iPnt - local + jPnt] < fAmplitude[iPnt]) {
               flag = 0;
               iPnt++;
               break;
            }
         }
         if (flag) {
            if (fAmplitude[iPnt] <= threshold) {
               npeak++;
               if (npeak <= nmax) {
                  peak[npeak - 1] = fAmplitude[iPnt];
                  peakpoint[npeak - 1] = iPnt;
               }
               flag = 1;
            }
            iPnt = iPnt + local + 1;
         }
      }
      if (iPnt > endpoint) {
         iPnt = endpoint;
      }

      flag = 1;

      Double_t *peakPointer = min_element(fAmplitude + iPnt, fAmplitude + endpoint);
      iPnt = peakPointer - fAmplitude;
      for (jPnt = iPnt - local; jPnt < iPnt; jPnt++) {
         if (fAmplitude[jPnt] < fAmplitude[iPnt]) {
            flag = 0;
            break;
         }
      }

      if (flag && fAmplitude[iPnt] <= threshold) {
         npeak++;
         if (npeak <= nmax) {
            peak[npeak - 1] = fAmplitude[iPnt];
            peakpoint[npeak - 1] = iPnt;
         }
      }

//       while (iPnt < endpoint) {

//          flag = 1;
//          for (jPnt = 0; jPnt < local; jPnt++) {
//             if (fAmplitude[iPnt-local+jPnt] < fAmplitude[iPnt]) {
//                flag = 0;
//                iPnt++;
//                break;
//             }
//          }
//          if (flag) {
//             for (jPnt = 0; jPnt < local && iPnt + jPnt + 1 < endpoint; jPnt++) {
//                if (fAmplitude[iPnt+jPnt+1] < fAmplitude[iPnt]) {
//                   flag = 0;
//                   iPnt += jPnt + 1;
//                }
//             }
//          }
//          if (flag) {
//             if (fAmplitude[iPnt] <= threshold) {
//                npeak++;
//                if (npeak <= nmax) {
//                   peak[npeak-1] = fAmplitude[iPnt];
//                   peakpoint[npeak-1] = iPnt;
//                   dbgcout<<fID<<" @@@ "<<endpoint<<" "<<npeak-1<<" "<<iPnt<<" "<<fAmplitude[iPnt]<<endl;
//                }
//                flag = 1;
//             }
//             iPnt = iPnt + local;
//          }
//       }


//       Int_t localEnd;
//       while (iPnt < endpoint-1) {
//          while (fAmplitude[iPnt] > threshold) {
//             iPnt++;
//             if (iPnt == endpoint-1)
//                return npeak;
//          }
//          flag = 0;
//          while (!flag) {
//             while (fAmplitude[iPnt] > fAmplitude[iPnt+1]) {
//                iPnt++;
//                if (iPnt == endpoint-1) {
//                   localEnd = TMath::Max(startpoint, iPnt - local);
//                   //localEnd = (startpoint-local < 0) ? local : startpoint;
//                   flag = 1;
//                   for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
//                      if (fAmplitude[iPnt] > fAmplitude[jPnt] ) {
//                         flag = 0;
//                         break;
//                      }
//                   }
//                   if (flag && fAmplitude[iPnt] <= threshold) {
//                      npeak++;
//                      if (npeak <= nmax) {
//                         peak[npeak-1] = fAmplitude[iPnt];
//                         peakpoint[npeak-1] = iPnt;
//                         dbgcout<<fID<<" ! "<<endpoint<<" "<<npeak-1<<" "<<iPnt<<" "<<fAmplitude[iPnt]<<endl;
//                      }
//                   }
//                   return npeak;
//                }
//             }
//             flag = 1;
//             localEnd = TMath::Max(startpoint, iPnt - local);
//             for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
//                if (fAmplitude[iPnt] > fAmplitude[jPnt] ) {
//                   flag = 0;
//                   iPnt++;
//                   if (iPnt == endpoint-1)
//                      return npeak;
//                   break;
//                }
//             }
//             if (flag) {
//                localEnd = TMath::Min(iPnt + local, endpoint-1);
//                for (jPnt = iPnt + 1; jPnt < localEnd + 1; jPnt++) {
//                   if (fAmplitude[iPnt] > fAmplitude[jPnt]) {
//                      flag = 0;
//                      iPnt = jPnt;
//                      if (iPnt >= endpoint-1)
//                         return npeak;
//                      break;
//                   }
//                }
//             }
//             if (fAmplitude[iPnt] > threshold) {
//                flag = 0;
//                break;
//             }
//          }

//          if (flag && fAmplitude[iPnt] <= threshold) {
//             npeak++;
//             if (npeak <= nmax) {
//                peak[npeak-1] = fAmplitude[iPnt];
//                peakpoint[npeak-1] = iPnt;
//                dbgcout<<fID<<" !! "<<endpoint<<" "<<npeak-1<<" "<<iPnt<<" "<<fAmplitude[iPnt]<<endl;
//             }
// //          iPnt = iPnt + 2*local;
//          iPnt = iPnt + local + 1;
//          }
//       }



   }
   return npeak;
}

//__________________________________________________________________________________
Int_t MEGWaveform::PeakSearch(Double_t *peak, Int_t *peakpoint, Int_t nmax, Double_t tstart, Double_t tend,
                              Double_t threshold, Double_t localwidth, Int_t polarity,
                              Int_t nveto, const Double_t *timedif, const Double_t *ratio) const
{
   // Search for peaks
   // Return the number of peaks.
   // Output :
   //  "peak"       peak values.
   //  "peakpoint"  peak positions (point number)
   // Input :
   //  "nmax"         maximum number of peaks to search
   //  "tstart""tend" search region
   //  "threshold"    threshold for peak
   //  "localwidth"   judge as peak if the point is an extremal value in the local region (half width)
   //  "nveto"        number of peaks to be considered for veto
   //  "timedif"      time difference of peak to be veto from main peak
   //  "ratio"        ratio of peak height to be veto to main peak

   Int_t  iPnt, jPnt, localEnd;
   Int_t  npeak = 0;
   Int_t  startpoint = FindPoint(tstart) + 1;
//    Int_t  endpoint   = FindPoint(tend);
   Int_t  endpoint   = FindPoint(tend) + 1;
   Int_t  flag;
   iPnt = startpoint;
   if (polarity > 0) { // local maximum
      while (iPnt < endpoint - 1) {
         while (fAmplitude[iPnt] < threshold) {
            iPnt++;
            if (iPnt == endpoint - 1) {
               return npeak;
            }
         }
         flag = 0;
         while (!flag) {
            while (fAmplitude[iPnt] < fAmplitude[iPnt + 1]) {
               iPnt++;
               if (iPnt == endpoint - 1) {
                  localEnd = TMath::Max(startpoint, FindPoint(GetTimeAt(iPnt) - localwidth) + 1);
                  flag = 1;
                  for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
                     if (fAmplitude[iPnt] < fAmplitude[jPnt]) {
                        flag = 0;
                        break;
                     }
                  }
                  if (flag && fAmplitude[iPnt] >= threshold) {
                     for (Int_t iPeak = 0; iPeak < npeak; iPeak++) {
                        for (Int_t iVeto = 0; iVeto < nveto; iVeto++) {
                           if ((TMath::Abs(GetTimeAt(iPnt) - GetTimeAt(peakpoint[iPeak]) - timedif[iVeto]) < localwidth / 2)
                               && (fAmplitude[iPnt] - peak[iPeak] * ratio[iVeto] < threshold
                                   || peak[iPeak] * ratio[iVeto] > threshold)) {
                              flag = 0;
                           }
                        }
                     }

                     if (flag) {
                        npeak++;
                        if (npeak <= nmax) {
                           peak[npeak - 1] = fAmplitude[iPnt];
                           peakpoint[npeak - 1] = iPnt;
                        }
                     }
                     return npeak;
                  } else {
                     return npeak;
                  }
               }
            }
            flag = 1;
            localEnd = TMath::Max(startpoint, FindPoint(GetTimeAt(iPnt) - localwidth) + 1);
            for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
               if (fAmplitude[iPnt] < fAmplitude[jPnt]) {
                  flag = 0;
                  iPnt++;
                  if (iPnt == endpoint - 1) {
                     return npeak;
                  }
                  break;
               }
            }
            if (flag) {
               localEnd = TMath::Min(FindPoint(GetTimeAt(iPnt) + localwidth), endpoint - 1);
               for (jPnt = iPnt + 1; jPnt < localEnd + 1; jPnt++) {
                  if (fAmplitude[iPnt] < fAmplitude[jPnt]) {
                     flag = 0;
                     iPnt = jPnt;
                     if (iPnt >= endpoint - 1) {
                        return npeak;
                     }
                     break;
                  }
               }
            }
            if (fAmplitude[iPnt] < threshold) {
               flag = 0;
               break;
            }
         }

         for (Int_t iPeak = 0; iPeak < npeak; iPeak++) {
            for (Int_t iVeto = 0; iVeto < nveto; iVeto++) {
               if ((TMath::Abs(GetTimeAt(iPnt) - GetTimeAt(peakpoint[iPeak]) - timedif[iVeto]) < localwidth / 2)
                   && (fAmplitude[iPnt] - peak[iPeak] * ratio[iVeto] < threshold
                       || peak[iPeak] * ratio[iVeto] > threshold)) {
                  flag = 0;
               }
            }
         }

         if (flag) {
            npeak++;
            if (npeak <= nmax) {
               peak[npeak - 1] = fAmplitude[iPnt];
               peakpoint[npeak - 1] = iPnt;
            }
         }
//          iPnt = FindPoint(GetTimeAt(iPnt) + 2*localwidth);
         iPnt = FindPoint(GetTimeAt(iPnt) + localwidth) + 1;
      }
   } else {          // local minimum
//       while (iPnt < endpoint-1) {
//          while (fAmplitude[iPnt] > threshold) {
//             iPnt++;
//             if (iPnt == endpoint-1)
//                return npeak;
//          }
//          flag = 0;
//          while (!flag) {
//             while (fAmplitude[iPnt] > fAmplitude[iPnt+1]) {
//                iPnt++;
//                if (iPnt == endpoint-1) {
//                   npeak++;
//                   if (npeak <= nmax) {
//                      peak[npeak-1] = fAmplitude[iPnt];
//                      peakpoint[npeak-1] = iPnt;
//                   }
//                   return npeak;
//                }
//             }
//             flag = 1;
//             localEnd = TMath::Max(startpoint, FindPoint(GetTimeAt(iPnt) - localwidth) + 1);
//             for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
//                if (fAmplitude[iPnt] > fAmplitude[jPnt] ) {
//                   flag = 0;
//                   iPnt++;
//                   if (iPnt == endpoint-1)
//                      return npeak;
//                  break;
//                }
//             }
//             if (flag) {
//                localEnd = TMath::Min(FindPoint(GetTimeAt(iPnt) + localwidth), endpoint-1);
//                for (jPnt = iPnt + 1; jPnt < localEnd + 1; jPnt++) {
//                   if (fAmplitude[iPnt] > fAmplitude[jPnt]) {
//                      flag = 0;
//                      iPnt = jPnt;
//                      if (iPnt >= endpoint-1)
//                         return npeak;
//                      break;
//                   }
//                }
//             }
//          }
//          for (Int_t iPeak = 0; iPeak < npeak; iPeak++) {
//             for (Int_t iVeto = 0; iVeto < nveto; iVeto++) {
//                if ((TMath::Abs(GetTimeAt(iPnt) - GetTimeAt(peakpoint[iPeak]) - timedif[iVeto]) < localwidth/2)
//                    && (fAmplitude[iPnt] - peak[iPeak] * ratio[iVeto] > threshold
//                        || peak[iPeak] * ratio[iVeto] < threshold))
//                   flag = 0;
//             }
//          }
//
//          if (flag) {
//             npeak++;
//             if (npeak <= nmax) {
//                peak[npeak-1] = fAmplitude[iPnt];
//                peakpoint[npeak-1] = iPnt;
//             }
//          }
//          iPnt = FindPoint(GetTimeAt(iPnt) + 2*localwidth);
//       }



////////////
      while (iPnt < endpoint - 1) {
         while (fAmplitude[iPnt] > threshold) {
            iPnt++;
            if (iPnt == endpoint - 1) {
               return npeak;
            }
         }
         flag = 0;
         while (!flag) {
            while (fAmplitude[iPnt] > fAmplitude[iPnt + 1]) {
               iPnt++;
               if (iPnt == endpoint - 1) {

                  localEnd = TMath::Max(startpoint, FindPoint(GetTimeAt(iPnt) - localwidth) + 1);
                  //localEnd = TMath::Max(startpoint, iPnt - local);
                  //localEnd = (startpoint-local < 0) ? local : startpoint;
                  flag = 1;
                  for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
                     if (fAmplitude[iPnt] > fAmplitude[jPnt]) {
                        flag = 0;
                        break;
                     }
                  }

                  if (flag && fAmplitude[iPnt] <= threshold) {
                     for (Int_t iPeak = 0; iPeak < npeak; iPeak++) {
                        for (Int_t iVeto = 0; iVeto < nveto; iVeto++) {
                           if ((TMath::Abs(GetTimeAt(iPnt) - GetTimeAt(peakpoint[iPeak]) - timedif[iVeto]) < localwidth / 2)
                               && (fAmplitude[iPnt] - peak[iPeak] * ratio[iVeto] > threshold
                                   || peak[iPeak] * ratio[iVeto] < threshold)) {
                              flag = 0;
                           }
                        }
                     }

                     if (flag) {
                        npeak++;
                        if (npeak <= nmax) {
                           peak[npeak - 1] = fAmplitude[iPnt];
                           peakpoint[npeak - 1] = iPnt;
                        }
                     }
                     return npeak;
                  } else {
                     return npeak;
                  }
               }
            }
            flag = 1;
            localEnd = TMath::Max(startpoint, FindPoint(GetTimeAt(iPnt) - localwidth) + 1);
            for (jPnt = iPnt - 1; jPnt > localEnd - 1; jPnt--) {
               if (fAmplitude[iPnt] > fAmplitude[jPnt]) {
                  flag = 0;
                  iPnt++;
                  if (iPnt == endpoint - 1) {
                     return npeak;
                  }
                  break;
               }
            }
            if (flag) {
               localEnd = TMath::Min(FindPoint(GetTimeAt(iPnt) + localwidth), endpoint - 1);
               for (jPnt = iPnt + 1; jPnt < localEnd + 1; jPnt++) {
                  if (fAmplitude[iPnt] > fAmplitude[jPnt]) {
                     flag = 0;
                     iPnt = jPnt;
                     if (iPnt >= endpoint - 1) {
                        return npeak;
                     }
                     break;
                  }
               }
            }
            if (fAmplitude[iPnt] > threshold) {
               flag = 0;
               break;
            }
         }
         for (Int_t iPeak = 0; iPeak < npeak; iPeak++) {
            for (Int_t iVeto = 0; iVeto < nveto; iVeto++) {
               if ((TMath::Abs(GetTimeAt(iPnt) - GetTimeAt(peakpoint[iPeak]) - timedif[iVeto]) < localwidth / 2)
                   && (fAmplitude[iPnt] - peak[iPeak] * ratio[iVeto] > threshold
                       || peak[iPeak] * ratio[iVeto] < threshold)) {
                  flag = 0;
               }
            }
         }

         if (flag) {
            npeak++;
            if (npeak <= nmax) {
               peak[npeak - 1] = fAmplitude[iPnt];
               peakpoint[npeak - 1] = iPnt;
            }
         }
         iPnt = FindPoint(GetTimeAt(iPnt) + localwidth) + 1;
      }

   }
   return npeak;
}

//__________________________________________________________________________________
Double_t MEGWaveform::CalculateBaseline(Double_t tstart, Double_t tend) const
{
   // Calculate baseline between tstart and tend

   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   Int_t    npoints = endpoint - startpoint;
   Double_t baseline = 0;
   Int_t    iPnt;

   if (npoints <= 0) {
      return 0;
   }

   for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
      baseline += fAmplitude[iPnt];
   }
   baseline /= npoints;

   return baseline;
}

//__________________________________________________________________________________
Double_t MEGWaveform::CalculateBaseline(Double_t tstart, Double_t tend, Double_t lowthre,
                                        Double_t upthre) const
{
   // Calculate baseline between tstart and tend
   // Not use the points outside the range specified by "lowthre" and "upthre".

   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   Int_t    npoints = 0;
   Double_t baseline = 0;
   Int_t    iPnt;

   for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
      if (lowthre < fAmplitude[iPnt] && fAmplitude[iPnt] < upthre) {
         baseline += fAmplitude[iPnt];
         npoints++;
      }
   }

   if (npoints <= 0) {
      return 0;
   }

   baseline /= npoints;

   return baseline;
}

//__________________________________________________________________________________
Double_t MEGWaveform::CalculateBaseline(Double_t tstart, Double_t tend, Double_t low, Double_t up,
                                        Double_t &rms, TH1 *hist, Option_t *opt) const
{
   // Calculate baseline between tstart and tend
   // Make histogram and use its mode.
   // By default histogram with 0.2mV binning is created and used. If you want to use
   // another histogram, input in the argument 'hist'.
   // 'range' is a parameter which specifies the ragne of histogram to be used from mode.

   TString option = opt;
   option.ToLower();

   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   Double_t baseline = 0;
   Int_t    iPnt;

   if (!hist) {
      static TH1I *histnew = new TH1I("WaveformBaseline", "WaveformBaseline", 3000, -300 * millivolt,
                                      300 * millivolt);
      hist = histnew;
   }
   hist->Reset();
   hist->GetXaxis()->SetRange(0, 0);
   for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
      hist->Fill(fAmplitude[iPnt]);
   }

   Int_t maxBin = hist->GetMaximumBin();
   Double_t mode = hist->GetBinCenter(maxBin);
   rms = hist->GetRMS();
   static TF1 *gaus = 0;
   Int_t fitResult = 0;

   if (up < 0 || low < 0 || (!option.Contains("m") && !option.Contains("f"))) {
      baseline = mode;
   } else if (option.Contains("f")) {
      if (!gaus) {
         gaus = new TF1("gaus", "gaus");
         gaus->SetLineWidth(1);
         gaus->SetLineColor(2);
      }
      gaus->SetParameter(1, mode);
      fitResult = hist->Fit(gaus, "Q", "", mode - low, mode + up);
      baseline = gaus->GetParameter(1);
   }
   if (option.Contains("m") || fitResult) {
      Int_t xmin = hist->FindBin(mode - low);
      Int_t xmax = hist->FindBin(mode + up);
      Double_t entry = 0;
      baseline = 0;
      for (Int_t iBin = xmin; iBin < xmax + 1; iBin++) {
         baseline += hist->GetBinContent(iBin) * hist->GetBinCenter(iBin);
         entry += hist->GetBinContent(iBin);
      }
      if (entry > 0) {
         baseline /= entry;
      }
   }

   if (option.Contains("draw")) {
      hist->GetXaxis()->SetRangeUser(mode - rms * 10, mode + rms * 10);
      if (option.Contains("copy")) {
         hist->DrawCopy();
      } else {
         hist->Draw();
      }
   }

   return baseline;
}

//__________________________________________________________________________________
Double_t MEGWaveform::CalculateBaseline(Double_t tstart, Double_t tend, Double_t low, Double_t up,
                                        Double_t binwidth, Option_t *opt) const
{
   // Calculate baseline between tstart and tend
   // Make histogram and use its mode.

   return CalculateBaseline(tstart, tend, -1e10, 1e10, low, up, binwidth, opt);
}


//__________________________________________________________________________________
Double_t MEGWaveform::CalculateBaseline(Double_t tstart, Double_t tend, Double_t lowthre, Double_t upthre,
                                        Double_t low, Double_t up, Double_t binwidth, Option_t *opt) const
{
   // Calculate baseline between tstart and tend
   // Make histogram and use its mode.

   TString option = opt;
   option.ToLower();

   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   Double_t baseline = 0;
   Int_t    iPnt, amplitude;
   Double_t halfwidth = binwidth / 2.;
   Double_t binwidthInv = 1. / binwidth;
//#define UNORDERED_MAP
#if defined (UNORDERED_MAP)
   unordered_map<Int_t, Int_t> baselineMap;
   for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
      if (fAmplitude[iPnt] < lowthre || fAmplitude[iPnt] > upthre) {
         continue;
      }
      if (fAmplitude[iPnt] >= 0) {
         amplitude = static_cast<Int_t>((fAmplitude[iPnt] + halfwidth) * binwidthInv);
      } else {
         amplitude = static_cast<Int_t>((fAmplitude[iPnt] - halfwidth) * binwidthInv);
      }
      baselineMap[amplitude]++;
   }

   Int_t maxCount = 0;
   Double_t mode;
   unordered_map<Int_t, Int_t>::iterator iter = baselineMap.begin();
   unordered_map<Int_t, Int_t>::iterator itMax = baselineMap.begin();
   while (iter != baselineMap.end()) {
      if ((*iter).second > maxCount) {
         maxCount = (*iter).second;
         itMax = iter;
      }
      ++iter;
   }

   mode = binwidth * (*itMax).first;
   Int_t modeBin = (*itMax).first;
   Int_t upBin = static_cast<Int_t>((up + halfwidth) * binwidthInv);
   Int_t lowBin = static_cast<Int_t>((low + halfwidth) * binwidthInv);
   if (up <= 0 || low <= 0 || (!option.Contains("m"))) {
      baseline = mode;
   } else if (option.Contains("m")) {
      Int_t entry = 0;
      for (Int_t bin = modeBin - lowBin; bin <= modeBin + upBin; bin++) {
         baseline += bin * baselineMap[bin];
         entry += baselineMap[bin];
      }
      if (entry > 0) {
         baseline *= binwidth / entry;
      }
   }

#else
   map<Int_t, Int_t> baselineMap;
   for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
      if (fAmplitude[iPnt] < lowthre || fAmplitude[iPnt] > upthre) {
         continue;
      }
      if (fAmplitude[iPnt] >= 0) {
         amplitude = static_cast<Int_t>((fAmplitude[iPnt] + halfwidth) * binwidthInv);
      } else {
         amplitude = static_cast<Int_t>((fAmplitude[iPnt] - halfwidth) * binwidthInv);
      }
      baselineMap[amplitude]++;
   }

   Int_t maxCount = 0;
   Double_t mode;
   map<Int_t, Int_t>::iterator iter = baselineMap.begin();
   map<Int_t, Int_t>::iterator itMax = baselineMap.begin();
   while (iter != baselineMap.end()) {
      if ((*iter).second > maxCount) {
         maxCount = (*iter).second;
         itMax = iter;
      }
      ++iter;
   }

   mode = binwidth * (*itMax).first;
   Int_t modeBin = (*itMax).first;
   Int_t upBin = static_cast<Int_t>((up + halfwidth) * binwidthInv);
   Int_t lowBin = static_cast<Int_t>((low + halfwidth) * binwidthInv);
   if (up <= 0 || low <= 0 || (!option.Contains("m"))) {
      baseline = mode;
   }
   if (option.Contains("m")) {
      Int_t entry = 0;
      map<Int_t, Int_t>::iterator iterBegin = baselineMap.begin();
      map<Int_t, Int_t>::iterator iterEnd   = baselineMap.end();
      iter = itMax;
      while (iter != iterEnd && (*iter).first <= modeBin + upBin) {
         baseline += (*iter).first * (*iter).second;
         entry += (*iter).second;
         ++iter;
      }
      if (itMax != iterBegin) {
         iter = itMax;
         --iter;
         while ((*iter).first >= modeBin - lowBin) {
            baseline += (*iter).first * (*iter).second;
            entry += (*iter).second;
            if (iter == iterBegin) {
               break;
            }
            --iter;
         }
      }
      if (entry > 0) {
         baseline *= binwidth / entry;
      }
   }
#endif
   // static Int_t count(0);
   // dbgcout<<count++<<"  "<<baseline<<endl;

   return baseline;
}

//__________________________________________________________________________________
Double_t MEGWaveform::ChargeIntegration(Double_t tstart, Double_t tend, Double_t baseline) const
{
   // Calculate charge by integrating this pulse between tstart and tend.

   Int_t    iPnt;
   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   if (startpoint >= endpoint) {
      return 0;
   }
   Double_t charge = 0;
   Double_t factor = 1. / kDRSInputImpedance; // conversion factor from area(V*sec) to charge(eplus);
   if (fFixBinSize) {
      charge = accumulate(fAmplitude + startpoint + 1, fAmplitude + endpoint - 1, fAmplitude[startpoint] / 2.);
      charge += fAmplitude[endpoint - 1] / 2.;
      charge *= fBinSize;
      charge -= (endpoint - startpoint) * fBinSize * baseline;
   } else {
      charge = fAmplitude[startpoint] * (fTime[startpoint + 1] - fTime[startpoint]);
      for (iPnt = startpoint + 1; iPnt < endpoint - 1; iPnt++) {
         charge += fAmplitude[iPnt] * (fTime[iPnt + 1] - fTime[iPnt - 1]);
      }
      charge += fAmplitude[endpoint - 1] * (fTime[endpoint - 1] - fTime[endpoint - 2]);
      charge /= 2.;
      charge -= (fTime[endpoint - 1] - fTime[startpoint]) * baseline;
   }
   charge *= factor;

   return charge;
}


//__________________________________________________________________________________
Double_t MEGWaveform::ChargeIntegration(Double_t tstart, Double_t tend, Double_t baseline, Double_t jitter,
                                        Int_t polarity) const
{
   // Calculate charge by integrating this pulse between tstart and tend.
   // Calculate charge with fix range with changing start time within jitter,
   // and adopt maximum value.
   // Currently only for fix bin mode

   Int_t    iPnt;
   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   if (startpoint >= endpoint) {
      return 0;
   }
   Double_t charge = 0;
   Double_t factor = 1. / kDRSInputImpedance; // conversion factor from area(V*sec) to charge(eplus);
   if (fFixBinSize) {
      Int_t startIt = FindPoint(tstart - jitter / 2) + 1;
      Int_t endIt = FindPoint(tstart + jitter / 2) + 1;
      Double_t maxvalue = -1e15;
      endpoint -= startpoint - startIt;
      vector<Double_t> integral(endIt - startIt + 1, 0.);
      Int_t maxIt = 0;
      for (Int_t iIt = 0; iIt < endIt - startIt + 1; iIt++) {
         startpoint = startIt + iIt;
         endpoint = TMath::Min(fNPoints, endpoint++);
         charge = accumulate(fAmplitude + startpoint + 1 + iIt, fAmplitude + endpoint - 1,
                             fAmplitude[startpoint] / 2.);
         charge += fAmplitude[endpoint - 1] / 2.;
         integral[iIt] = charge;
         if (charge * polarity > maxvalue) {
            maxvalue = charge * polarity;
            maxIt = iIt;
         }
      }
      if (endIt - startIt == 0)
         ;
      else if (maxIt == 0) {
         maxvalue = (integral[maxIt] + integral[maxIt + 1]) / 2. * polarity;
      } else if (maxIt == endIt - startIt) {
         maxvalue = (integral[maxIt] + integral[maxIt - 1]) / 2. * polarity;
      } else {
         maxvalue = (integral[maxIt - 1] + integral[maxIt] + integral[maxIt + 1]) / 3. * polarity;
      }
      charge = maxvalue * polarity * fBinSize;
      charge -= (endpoint - startpoint) * fBinSize * baseline;
   } else {
      charge = fAmplitude[startpoint] * (fTime[startpoint + 1] - fTime[startpoint]);
      for (iPnt = startpoint + 1; iPnt < endpoint - 1; iPnt++) {
         charge += fAmplitude[iPnt] * (fTime[iPnt + 1] - fTime[iPnt - 1]);
      }
      charge += fAmplitude[endpoint - 1] * (fTime[endpoint - 1] - fTime[endpoint - 2]);
      charge /= 2.;
      charge -= (fTime[endpoint - 1] - fTime[startpoint]) * baseline;
   }
   charge *= factor;

   return charge;
}

//__________________________________________________________________________________
Double_t MEGWaveform::IntegrateHeightSquaredWF(Double_t tstart, Double_t tend, Double_t baseline) const
{
   // Calculate area of waveform whose height is squared
   // between tstart and tend.

   Int_t    iPnt;
   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend) + 1;
   if (startpoint >= endpoint) {
      return 0;
   }
   Double_t noisevar = 0;
   Double_t factor = 1. / kDRSInputImpedance; // conversion factor from area(V*sec) to charge(eplus);
   if (fFixBinSize) {
      for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
         noisevar += TMath::Power(fAmplitude[iPnt] - baseline, 2) * fBinSize;
      }
   } else {
      noisevar = TMath::Power(fAmplitude[startpoint] - baseline, 2) *
                 (fTime[startpoint + 1] - fTime[startpoint]);
      for (iPnt = startpoint + 1; iPnt < endpoint - 1; iPnt++) {
         noisevar += TMath::Power(fAmplitude[iPnt] - baseline, 2) *
                     (fTime[iPnt + 1] - fTime[iPnt - 1]);
      }
      noisevar += TMath::Power(fAmplitude[endpoint - 1] - baseline, 2) *
                  (fTime[endpoint - 1] - fTime[endpoint - 2]);
   }
   noisevar /= 2.;
   noisevar *= factor * factor;

   return noisevar;
}

//__________________________________________________________________________________
Double_t MEGWaveform::CrossingTime(Double_t threshold, Int_t startpoint, Int_t endpoint
                                   , Int_t polarity, Option_t *opt, Double_t tacc, Int_t imax) const
{
   // Calculate time of first crossing "threshold".
   // If "endpoint" < "startpoint", search for the crossing point backward from "startpoint" down to "endpoint".
   // If the pulse didn't cross the "threshold", return kInvalidTime.
   //   "polarity"     in case of positive value low-to-high crossing time,
   //                  negaive value, high-to-low crossing time.

   if (!fNPoints) {
      return 0;
   }
   TString option = opt;
   option.ToUpper();

   Double_t ratio;// for linear interpolation
   Double_t crossTime = kInvalidTime;
   Int_t    direction = (endpoint > startpoint) ? 1 : -1;
//    Int_t    startpoint = FindPoint(tstart) + 1;
//    Int_t    endpoint   = FindPoint(tend);
   if (direction < 0) {
//       startpoint--;
//       endpoint++;
      crossTime *= -1;
      if (startpoint <= endpoint) {
         return crossTime;   // failure
      }
   } else {
      if (startpoint >= endpoint) {
         return crossTime;   // failure
      }
   }
   Int_t    iPnt = startpoint;

   if (polarity > 0 && direction > 0) { // search for low-to-high crossing
      if (fAmplitude[startpoint] >= threshold) {
         iPnt = startpoint;
         while (fAmplitude[iPnt] >= threshold) {
            iPnt++;
            if (iPnt >= endpoint) {
               return crossTime;   // failure
            }
         }
      }
      while (iPnt < endpoint) {
         if (fAmplitude[iPnt] >= threshold) {
            if (fAmplitude[iPnt - 1] != fAmplitude[iPnt]) {
               ratio = (threshold - fAmplitude[iPnt]) / (fAmplitude[iPnt - 1] - fAmplitude[iPnt]);
            } else {
               ratio = 0.5;
            }
            crossTime = GetTimeAt(iPnt) - ratio * (GetTimeAt(iPnt) - GetTimeAt(iPnt - 1));
            break;
         }
         iPnt++;
      }
   } else if (polarity > 0 && direction < 0) {
      if (fAmplitude[startpoint] >= threshold) {
         iPnt = startpoint;
         while (fAmplitude[iPnt] >= threshold) {
            iPnt--;
            if (iPnt <= endpoint) {
               return crossTime;   // failure
            }
         }
      }
      while (iPnt > endpoint) {
         if (fAmplitude[iPnt] >= threshold) {
            if (fAmplitude[iPnt + 1] != fAmplitude[iPnt]) {
               ratio = (threshold - fAmplitude[iPnt]) / (fAmplitude[iPnt + 1] - fAmplitude[iPnt]);
            } else {
               ratio = 0.5;
            }
            crossTime = GetTimeAt(iPnt) + ratio * (GetTimeAt(iPnt + 1) - GetTimeAt(iPnt));
            break;
         }
         iPnt--;
      }
   } else if (polarity < 0 && direction > 0) { // search for high-to-low crossing
      if (fAmplitude[startpoint] <= threshold) {
         iPnt = startpoint;
         while (fAmplitude[iPnt] <= threshold) {
            iPnt++;
            if (iPnt >= endpoint) {
               return crossTime;   // failure
            }
         }
      }
      while (iPnt < endpoint) {
         if (fAmplitude[iPnt] <= threshold) {
            if (fAmplitude[iPnt - 1] != fAmplitude[iPnt]) {
               ratio = (threshold - fAmplitude[iPnt]) / (fAmplitude[iPnt - 1] - fAmplitude[iPnt]);
            } else {
               ratio = 0.5;
            }
            crossTime = GetTimeAt(iPnt) - ratio * (GetTimeAt(iPnt) - GetTimeAt(iPnt - 1));
            break;
         }
         iPnt++;
      }
   } else if (polarity < 0 && direction < 0) {
      if (fAmplitude[startpoint] <= threshold) {
         iPnt = startpoint;
         while (fAmplitude[iPnt] <= threshold) {
            iPnt--;
            if (iPnt <= endpoint) {
               return crossTime;   // failure
            }
         }
      }
      while (iPnt > endpoint) {
         if (fAmplitude[iPnt] <= threshold) {
            if (fAmplitude[iPnt + 1] != fAmplitude[iPnt]) {
               ratio = (threshold - fAmplitude[iPnt]) / (fAmplitude[iPnt + 1] - fAmplitude[iPnt]);
            } else {
               ratio = 0.5;
            }
            crossTime = GetTimeAt(iPnt) + ratio * (GetTimeAt(iPnt + 1) - GetTimeAt(iPnt));
            break;
         }
         iPnt--;
      }
   } else {
      return crossTime; // failure
   }

   // cubic interpolation
   if (option.Contains("CUB")) {
      Double_t crossTimeCUB = crossTime;
      try {
         crossTimeCUB = InterpolateTime(threshold, iPnt, crossTime, tacc, imax);
      } catch (...) {
         crossTimeCUB = crossTime;
      }
      crossTime = crossTimeCUB;
   }

   return crossTime;
}

//__________________________________________________________________________________
Double_t MEGWaveform::CrossingTime(Double_t threshold, Double_t tstart, Double_t tend
                                   , Int_t polarity, Option_t *opt, Double_t tacc, Int_t imax) const
{
   // Calculate time of first crossing "threshold".
   // If "tend" < "tstart", search for the crossing point backward from "tstart" down to "tend".
   // If the pulse didn't cross the "threshold", return kInvalidTime.
   //   "polarity"     in case of positive value low-to-high crossing time,
   //                  negaive value, high-to-low crossing time.

   if (!fNPoints) {
      return 0;
   }

   Double_t crossTime = kInvalidTime;
   Int_t    direction = (tend > tstart) ? 1 : -1;
   Int_t    startpoint = FindPoint(tstart) + 1;
   Int_t    endpoint   = FindPoint(tend);
   if (direction < 0) {
      startpoint--;
      endpoint++;
      crossTime *= -1;
      if (startpoint <= endpoint) {
         return crossTime;   // failure
      }
   } else {
      if (startpoint >= endpoint) {
         return crossTime;   // failure
      }
   }

   return CrossingTime(threshold, startpoint, endpoint, polarity, opt, tacc, imax);
}

//__________________________________________________________________________________
Double_t MEGWaveform::PulseWidth(Double_t peak, Int_t peakpoint, Double_t baseline, Double_t height,
                                 Int_t polarity) const
{
   // Calculate pulse width at the "height" fraction (FWHM by default)

   if (!fNPoints) {
      return 0;
   }

   if (!polarity) {
      polarity = (peak < baseline) ? -1 : 1;
   }
   Double_t atheight = (height * (peak - baseline) + baseline);
   Double_t widthStart, widthEnd;
   Double_t width = kInvalidTime;

   if (fAmplitude[peakpoint] * polarity <= atheight * polarity) {
      return width;
   }

   Double_t tstart = GetTimeAt(peakpoint);
   Double_t tend = GetTimeMin();

   widthStart = CrossingTime(atheight, tstart, tend, -polarity);
   if (widthStart > tend) {
      tend = GetTimeMax();
      widthEnd = CrossingTime(atheight, tstart, tend, -polarity);
      if (widthEnd < tend) {
         width = widthEnd - widthStart;
      }
   }
   return width;
}
//__________________________________________________________________________________
Double_t MEGWaveform::TimeOverThreshold(Double_t threshold, Int_t peakpoint, Double_t baseline) const
{
   // Calculate time length pulse is over threshlod

   if (!fNPoints) {
      return 0;
   }

   Double_t widthStart, widthEnd;
   Double_t width = kInvalidTime;
   Double_t tstart = GetTimeAt(peakpoint);
   Double_t tend = GetTimeMin();

   widthStart = CrossingTime(baseline + threshold, tstart, tend, 1);

   if (widthStart > tend) {
      tend = GetTimeMax();

      widthEnd = CrossingTime(baseline + threshold, tstart, tend, 1);
      if (widthEnd < tend) {
         width = widthEnd - widthStart;
      }
   }
   return width;
}
//__________________________________________________________________________________
Double_t MEGWaveform::RiseTime(Double_t peak, Int_t peakpoint, Double_t baseline,
                               Int_t polarity, Double_t low, Double_t high, Option_t *opt) const
{
   // Calculate rise time
   // Time length from fraction "low" to "high" (by default 0.1 - 0.9)

   if (!fNPoints) {
      return 0;
   }

   TString option = opt;
   option.ToUpper();

   if (!polarity) {
      polarity = (peak < baseline) ? -1 : 1;
   }
   Double_t lowthre = (low * (peak - baseline) + baseline);
   Double_t highthre = (high * (peak - baseline) + baseline);
   Double_t widthStart, widthEnd;
   Double_t width = kInvalidTime;
   Double_t tstart = GetTimeAt(peakpoint);
   Double_t tend;
   Int_t rise = 1;
   if (option.Contains("FALL")) {
      tend = GetTimeMax();
      rise = -1;
   } else {
      tend = GetTimeMin();
   }

   if (fAmplitude[peakpoint] * polarity <= highthre * polarity) {
      return width;
   }

   widthEnd = CrossingTime(highthre, tstart, tend, -polarity, opt);
   if (widthEnd * rise > tend * rise) {
      widthStart = CrossingTime(lowthre, widthEnd, tend, -polarity, opt);
      if (widthStart * rise > tend * rise) {
         width = (widthEnd - widthStart) * rise;
      }
   }
   return width;
}

//__________________________________________________________________________________
Double_t MEGWaveform::FallTime(Double_t peak, Int_t peakpoint, Double_t baseline,
                               Int_t polarity, Double_t low, Double_t high, Option_t *opt) const
{
   // Calculate fall time
   // Time length from fraction "high" to "low" (by default 0.9 - 0.1)

   TString option = opt;
   option.Append("FALL");
   return RiseTime(peak, peakpoint, baseline, polarity, low, high, option.Data());
}

//__________________________________________________________________________________
Double_t MEGWaveform::LeadingEdge(Double_t threshold) const
{
   // Time pick-off with Leading Edge method

   return LeadingEdge(threshold, GetTimeMin(), GetTimeMax());
}

//__________________________________________________________________________________
Double_t MEGWaveform::LeadingEdge(Double_t threshold, Double_t tstart, Double_t tend, Int_t polarity) const
{
   // Time pick-off with Leading Edge method
   // Return the timing of the "threshold" crossing.
   // If the pulse didn't cross the "threshold", return kInvalidTime.
   // Input :
   //   "threshold"    pulse height threshold.
   //   "tstart""tend" region
   //   "polarity"     in case of  positive value low-to-high crossing time,
   //                  negaive value, high-to-low crossing time.
   //                  0 (default), threshold polarity is used.

   if (tstart >= tend) {
      return kInvalidTime;
   }
   if (!polarity) {
      polarity = (threshold < 0) ? -1 : 1;
   }

   return CrossingTime(threshold, tstart, tend, polarity);
}

//__________________________________________________________________________________
Double_t MEGWaveform::DoubleThreshold(Double_t threlow, Double_t threhigh, Double_t tstart, Double_t tend,
                                      Int_t polarity, Option_t* opt, Double_t tacc, Int_t imax) const
{
   // Time pick-off with double thresholds method
   // Return the timing of the first "threlow" crossing going back from "threhigh" crossing.
   // If the pulse didn't cross the "threshigh", return kInvalidTime.
   // Input :
   //   "tstart""tend" region
   //   "polarity"     in case of  positive value low-to-high crossing time,
   //                  negaive value, high-to-low crossing time.
   //                  0 (default), threshold polarity is used.
   //   "opt"          set "CUBIC" for cubic interpolation. Otherwise linear interpolation.
   //   "tacc","imax"  convergence criterion and maximum number of iteration for cubic interpolation.

   TString option = opt;
   option.ToUpper();

   Double_t tLE = kInvalidTime;
   Int_t direction = (tstart < tend) ? 1 : -1;
   // if (tstart >= tend)
   //    return tLE;
   if (!polarity) {
      polarity = (threhigh < 0) ? -1 : 1;
   }

   if (direction > 0) {
      tLE = CrossingTime(threhigh, tstart, tend, polarity);
      if (tLE < tend) { // crossing high threshold
         Int_t startpnt = FindPoint(tLE) + 1;
         Int_t endpnt = FindPoint(tstart) + 1;
         tLE = CrossingTime(threlow, startpnt, endpnt, -polarity, opt, tacc, imax);
         if (tLE < tstart) {
            tLE = kInvalidTime;
         }
      }
   } else {
      tLE = CrossingTime(threhigh, tstart, tend, -polarity);
      if (tLE < tstart) { // crossing high threshold
         Int_t startpnt = FindPoint(tLE) + 1;
         Int_t endpnt = FindPoint(tend) + 1;
         tLE = CrossingTime(threlow, startpnt, endpnt, -polarity, opt, tacc, imax);
         if (tLE < tend) {
            tLE = kInvalidTime;
         }
      }
   }
   return tLE;
}

//__________________________________________________________________________________
Double_t MEGWaveform::ConstantFraction(Double_t cfamplitude, Double_t threshold, Double_t tstart,
                                       Double_t tend,
                                       Int_t polarity, Option_t* opt, Double_t tacc, Int_t imax) const
{
   // Time pickoff with (digital) Constant Fraction method
   // Return the timing at which the signal reaches a fraction of the full amplitude,
   // If the pulse is under the "threshold", return kInvalidTime.
   // Set "opt" "CUBIC" for cubic interpolation. Otherwise linear interpolation.
   // "tacc" is convergence criterion and "imax" is maximum number of iteration for cubic interpolation.

   // Check highthre is larger than lowthre
   if (polarity < 0) {
      if (cfamplitude < threshold) {
         threshold = cfamplitude;
      }
   } else {
      if (cfamplitude > threshold) {
         threshold = cfamplitude;
      }
   }

   return DoubleThreshold(cfamplitude, threshold, tstart, tend, polarity, opt, tacc, imax);
}

//__________________________________________________________________________________
Double_t MEGWaveform::ConstantFraction(Double_t fraction, Double_t delay, Double_t baseline,
                                       Double_t threshold,
                                       Double_t tstart, Double_t tend, Int_t polarity, MEGWaveform *wfout,
                                       Option_t* opt, Double_t tacc, Int_t imax) const
{
   // Time pickoff with Constant Fraction method also known as Amplitude and Rise time Compensated method
   // Return the zero cross time of,
   //      f_cf(t) = "fraction"*f(t) - f(t-"delay")
   // If the pulse is under the "threshold", return kInvalidTime.
   // Set "opt" "CUBIC" for cubic interpolation. Otherwise linear interpolation.
   // "tacc" is convergence criterion and "imax" is maximum number of iteration for cubic interpolation.
   // "wfout" is fix bin size.

   Int_t     iPnt;
   Double_t  tCF = kInvalidTime;
   Int_t     tempbin;
   Double_t  tempampl, temptime;
   Double_t  ratio;// for linear interpolation
   Double_t  timemin = GetTimeMin();
   Double_t *amplitudeCF = 0;
   MEGWaveform *wfCF = 0;
   if (wfout) {
      Int_t npointsOut = static_cast<Int_t>((GetTimeMax() - GetTimeMin()) / fBinSize);
      wfout->SetNPoints(npointsOut);
      wfout->ResetAmplitude();
      wfout->SetBinSize(fBinSize);
      wfout->SetTimeMin(timemin);
      wfCF = wfout;
   } else {
      Int_t npointsOut = static_cast<Int_t>((GetTimeMax() - GetTimeMin()) / fBinSize);
      wfCF = new MEGWaveform(npointsOut, fBinSize, timemin);
   }
   Int_t     startpoint = wfCF->FindPoint(tstart) + 1;
   Int_t     endpoint   = wfCF->FindPoint(tend);
   if (startpoint >= endpoint) {
      return tCF;
   }
   Int_t     delayInt = static_cast<Int_t>(delay / fBinSize);

   amplitudeCF = wfCF->GetAmplitude();

   // Make Amplitude and Rise time Compensated waveform
   if (fFixBinSize) {
      for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
         if (iPnt - delayInt > 0) {
            temptime = iPnt * fBinSize + timemin;

            // Attenuated pulse
            tempbin  = FindPoint(temptime);
            if (tempbin < 0 || tempbin >= fNPoints - 1) {
               amplitudeCF[iPnt] = 0;
               continue;
            }
            ratio = (temptime - (tempbin * fBinSize + fTimeMin)) / fBinSize;
            tempampl = (1 - ratio) * fAmplitude[tempbin] + ratio * fAmplitude[tempbin + 1] - baseline;
            amplitudeCF[iPnt] = fraction * tempampl;

            // Delayed and inverted pulse
            tempbin = FindPoint(temptime - delay);
            if (tempbin < 0 || tempbin >= fNPoints - 1) {
               amplitudeCF[iPnt] = 0;
               continue;
            }
            ratio = (temptime - delay - (tempbin * fBinSize + fTimeMin)) / fBinSize;
            tempampl = (1 - ratio) * fAmplitude[tempbin] + ratio * fAmplitude[tempbin + 1] - baseline;
            amplitudeCF[iPnt] += -1 * tempampl;
         } else {
            amplitudeCF[iPnt] = 0;
         }
      }

   } else { // variable bin size
      for (iPnt = startpoint; iPnt < endpoint; iPnt++) {
         if (iPnt - delayInt > 0) {
            temptime = iPnt * fBinSize + timemin;

            // Attenuated pulse
            tempbin  = FindPoint(temptime);
            if (tempbin < 0 || tempbin >= fNPoints - 1) {
               amplitudeCF[iPnt] = 0;
               continue;
            }
            ratio = (temptime - fTime[tempbin]) / (fTime[tempbin + 1] - fTime[tempbin]);
            tempampl = (1 - ratio) * fAmplitude[tempbin] + ratio * fAmplitude[tempbin + 1] - baseline;
            amplitudeCF[iPnt] = fraction * tempampl;

            // Delayed and inverted pulse
            tempbin = FindPoint(temptime - delay);
            if (tempbin < 0 || tempbin >= fNPoints - 1) {
               amplitudeCF[iPnt] = 0;
               continue;
            }
            ratio = (temptime - delay - fTime[tempbin]) / (fTime[tempbin + 1] - fTime[tempbin]);
            tempampl = (1 - ratio) * fAmplitude[tempbin] + ratio * fAmplitude[tempbin + 1] - baseline;
            amplitudeCF[iPnt] += -1 * tempampl;
         } else {
            amplitudeCF[iPnt] = 0;
         }
      }
   }

   if (!polarity) {
      polarity = (threshold < 0) ? -1 : 1;
   }

   // Get the zero crossing time
   tCF = wfCF->DoubleThreshold(0., -threshold, tstart, tend, -polarity, opt, tacc, imax);

   if (!wfout) {
      delete wfCF;
   }

   return tCF;
}

//__________________________________________________________________________________
void MEGWaveform::Rotate(Int_t dpnt, MEGWaveform *wfout)
{
   // Rotate the amplitude array
   // Use for fix bin size waveform.
   // If output waveform is not specified, this waveform is changed.
   // If "dpnt" is positive, rotate right (delay)
   // If "dpnt" is negative, rotate left (ahead)

   if (dpnt < 0) {
      dpnt = -dpnt;
   } else {
      dpnt = fNPoints - dpnt;
   }

   if (!wfout) {
      rotate(fAmplitude, fAmplitude + dpnt, fAmplitude + fNPoints);
   } else {
      wfout->SetTime(this);
      rotate_copy(fAmplitude, fAmplitude + dpnt, fAmplitude + fNPoints, wfout->fAmplitude);
   }
}

//__________________________________________________________________________________
void MEGWaveform::TimeShift(Int_t dpnt, MEGWaveform *wfout)
{
   // Shift waveform
   // Use for fix bin size waveform.
   // If output waveform is not specified, this waveform is changed.
   // If "dpnt" is positive, shift right (delay)
   // If "dpnt" is negative, shift left (ahead)

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }

   if (dpnt > 0) {
      memmove(wfout->GetAmplitude() + dpnt, wfout->GetAmplitude(), sizeof(Double_t) * (fNPoints - dpnt));
      memset(wfout->GetAmplitude(), 0, sizeof(Double_t)*dpnt);
   } else {
      memmove(wfout->GetAmplitude(), wfout->GetAmplitude() - dpnt, sizeof(Double_t) * (fNPoints + dpnt));
      memset(wfout->GetAmplitude() + (fNPoints + dpnt), 0, sizeof(Double_t) * (-dpnt));
   }
}

//__________________________________________________________________________________
void MEGWaveform::TimeShift(Double_t dt, MEGWaveform *wfout)
{
   // Shift waveform by dt
   // Can be used for both fixed and variable bin waveform.
   // If output waveform is not specified, this waveform is changed.
   // If "dt" is positive, shift right (delay)
   // If "dt" is negative, shift left (ahead)

   if (wfout) {
      wfout->SetTime(this);
      wfout->SetAmplitude(fAmplitude);
   } else {
      wfout = this;
   }

   if (wfout->fFixBinSize) {
      wfout->SetTime(wfout->fTimeMin + dt, wfout->fTimeMax + dt);
   } else {
      Double_t *ptime = wfout->GetTime();
      // for (int i = 0; i < wfout->fNPoints; i++) cout<<i<<" "<<ptime[i]<<endl;
      for (int i = 0; i < wfout->fNPoints; i++) {
         ptime[i] += dt;
      }
   }
   return;
}

//__________________________________________________________________________________
void MEGWaveform::Reverse(MEGWaveform *wfout)
{
   // Time reverse of this waveform (flip left-right).
   // Use for fix bin size waveform.
   // If output waveform is not specified, this waveform is changed.

   if (!wfout) {
      reverse(fAmplitude, fAmplitude + fNPoints);
   } else {
      wfout->SetTime(this);
      reverse_copy(fAmplitude, fAmplitude + fNPoints, wfout->fAmplitude);
   }
}

//__________________________________________________________________________________
void MEGWaveform::Sampling(Int_t npnt, Int_t startpnt, Int_t dpnt, MEGWaveform *wfout) const
{
   // Sampling "npnt" from "startpnt" of this and at "dpnt" interval.
   // Use for fix bin size waveform and output waveform is fix bin size.

   wfout->SetNPoints(npnt);
   wfout->SetBinSize(fBinSize * dpnt);

   Int_t iPnt;
   wfout->SetTime(GetTimeAt(startpnt), GetTimeAt(startpnt) + (npnt - 1)*wfout->fBinSize);
   for (iPnt = 0; iPnt < npnt; iPnt++) {
      wfout->fAmplitude[iPnt] = GetAmplitudeAt(startpnt + iPnt * dpnt);
   }
}

//__________________________________________________________________________________
void MEGWaveform::Sampling(Int_t npnt, Double_t starttime, Double_t dtime, MEGWaveform *wfout,
                           Option_t *opt) const
{
   // Sampling "npnt" from "starttime" of this and at "dtime" interval.
   // Output waveform is fix bin size.
   // Option :
   //    "precise"  linear interpolation by the both side of points.
   //     default   find the corresponding point for every sampling.

   TString option = opt;
   option.ToUpper();

   wfout->SetNPoints(npnt);
   wfout->SetBinSize(dtime);

   Int_t iPnt;
   Int_t pnt = 0;
   Double_t ratio, width, tmax;
   wfout->SetTime(starttime, starttime + (npnt - 1)*dtime);
   if (fFixBinSize) {
      if (option == "PRECISE") {
         for (iPnt = 0; iPnt < npnt; iPnt++) {
            pnt = FindPoint(starttime + dtime * iPnt, pnt);
            if (pnt < 0) {
               pnt = 0;
               wfout->fAmplitude[iPnt] = 0;
            } else if (pnt >= fNPoints - 1) {
               memset(wfout->GetAmplitude() + iPnt, 0, sizeof(Double_t) * (wfout->fNPoints - iPnt));
               break;
            } else {
               width = fBinSize;
               ratio = ((pnt + 1) * fBinSize + fTimeMin - (starttime + dtime * iPnt)) / width;
               wfout->fAmplitude[iPnt] = (1 - ratio) * fAmplitude[pnt + 1] + ratio * fAmplitude[pnt];
            }
         }
      } else {
         tmax = (fNPoints - 1) * fBinSize + fTimeMin + fBinSize / 2;
         for (iPnt = 0; iPnt < npnt; iPnt++) {
            pnt = FindPoint(starttime + dtime * iPnt);
            if (pnt < 0) {
               wfout->fAmplitude[iPnt] = 0;
            } else if (pnt >= fNPoints - 1) {
               memset(wfout->GetAmplitude() + iPnt, 0, sizeof(Double_t) * (wfout->fNPoints - iPnt));
               break;
            } else {
               wfout->fAmplitude[iPnt] = GetAmplitudeAt(pnt);
            }
         }
      }
   } else { // variable bin size
      if (option == "PRECISE") {
         for (iPnt = 0; iPnt < npnt; iPnt++) {
            pnt = FindPoint(starttime + dtime * iPnt, pnt);
            if (pnt < 0) {
               pnt = 0;
               wfout->fAmplitude[iPnt] = 0;
            } else if (pnt >= fNPoints - 1) {
               memset(wfout->GetAmplitude() + iPnt, 0, sizeof(Double_t) * (wfout->fNPoints - iPnt));
               break;
            } else {
               width = fTime[pnt + 1] - fTime[pnt];
               ratio = (fTime[pnt + 1] - (starttime + dtime * iPnt)) / width;
               wfout->fAmplitude[iPnt] = (1 - ratio) * fAmplitude[pnt + 1] + ratio * fAmplitude[pnt];
            }
         }
      } else {
         tmax = fTime[fNPoints - 1] + fBinSize / 2;
         for (iPnt = 0; iPnt < npnt; iPnt++) {
            pnt = FindPoint(starttime + dtime * iPnt, pnt);
            if (pnt < 0) {
               pnt = 0;
               wfout->fAmplitude[iPnt] = 0;
            } else if (pnt >= fNPoints - 1) {
               memset(wfout->GetAmplitude() + iPnt, 0, sizeof(Double_t) * (wfout->fNPoints - iPnt));
               break;
            } else {
               wfout->fAmplitude[iPnt] = GetAmplitudeAt(pnt);
            }
         }
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::Sampling(MEGWaveform *wfout, Option_t *opt) const
{
   // Sampling according to fTime of wfout.
   // Option :
   //    "precise"  linear interpolation by the both side of points.
   //     default   find the corresponding point for every sampling.

   TString option = opt;
   option.ToUpper();

   Int_t iPnt;
   Int_t pnt;
   Double_t ratio, width, tmax;
   if (fFixBinSize && wfout->fFixBinSize) {
      if (option == "PRECISE") {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(iPnt * wfout->fBinSize + wfout->fTimeMin);
            if (pnt < 0 || pnt >= fNPoints - 1) {
               wfout->fAmplitude[iPnt] = 0;
            } else {
               width = fBinSize;
               ratio = ((pnt + 1) * fBinSize + fTimeMin - iPnt * wfout->fBinSize + wfout->fTimeMin) / width;
               wfout->fAmplitude[iPnt] = (1 - ratio) * fAmplitude[pnt + 1] + ratio * fAmplitude[pnt];
            }
         }
      } else {
         tmax = (fNPoints - 1) * fBinSize + fTimeMin + fBinSize / 2;
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(iPnt * wfout->fBinSize + wfout->fTimeMin);
            if (iPnt * wfout->fBinSize + wfout->fTimeMin > tmax) {
               pnt = fNPoints;
            }
            wfout->fAmplitude[iPnt] = GetAmplitudeAt(pnt);
         }
      }
   } else if (fFixBinSize && !wfout->fFixBinSize) {
      if (option == "PRECISE") {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(wfout->fTime[iPnt]);
            if (pnt < 0 || pnt >= fNPoints - 1) {
               wfout->fAmplitude[iPnt] = 0;
            } else {
               width = fBinSize;
               ratio = ((pnt + 1) * fBinSize + fTimeMin - wfout->fTime[iPnt]) / width;
               wfout->fAmplitude[iPnt] = (1 - ratio) * fAmplitude[pnt + 1] + ratio * fAmplitude[pnt];
            }
         }
      } else {
         tmax = (fNPoints - 1) * fBinSize + fTimeMin + fBinSize / 2;
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(wfout->fTime[iPnt]);
            if (wfout->fTime[iPnt] > tmax) {
               pnt = fNPoints;
            }
            wfout->fAmplitude[iPnt] = GetAmplitudeAt(pnt);
         }
      }
   } else if (!fFixBinSize && wfout->fFixBinSize) {
      if (option == "PRECISE") {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(iPnt * wfout->fBinSize + wfout->fTimeMin);
            if (pnt < 0 || pnt >= fNPoints - 1) {
               wfout->fAmplitude[iPnt] = 0;
            } else {
               width = fTime[pnt + 1] - fTime[pnt];
               ratio = (fTime[pnt + 1] - iPnt * wfout->fBinSize + wfout->fTimeMin) / width;
               wfout->fAmplitude[iPnt] = (1 - ratio) * fAmplitude[pnt + 1] + ratio * fAmplitude[pnt];
            }
         }
      } else {
         tmax = fTime[fNPoints - 1] + fBinSize / 2;
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(iPnt * wfout->fBinSize + wfout->fTimeMin);
            if (iPnt * wfout->fBinSize + wfout->fTimeMin > tmax) {
               pnt = fNPoints;
            }
            wfout->fAmplitude[iPnt] = GetAmplitudeAt(pnt);
         }
      }
   } else {
      if (option == "PRECISE") {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(wfout->fTime[iPnt]);
            if (pnt < 0 || pnt >= fNPoints - 1) {
               wfout->fAmplitude[iPnt] = 0;
            } else {
               width = fTime[pnt + 1] - fTime[pnt];
               ratio = (fTime[pnt + 1] - wfout->fTime[iPnt]) / width;
               wfout->fAmplitude[iPnt] = (1 - ratio) * fAmplitude[pnt + 1] + ratio * fAmplitude[pnt];
            }
         }
      } else {
         tmax = fTime[fNPoints - 1] + fBinSize / 2;
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            pnt = FindPoint(wfout->fTime[iPnt]);
            if (wfout->fTime[iPnt] > tmax) {
               pnt = fNPoints;
            }
            wfout->fAmplitude[iPnt] = GetAmplitudeAt(pnt);
         }
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::Digitize(Int_t n, const Double_t *xbins, const Double_t *xout, MEGWaveform *wfout)
{
   // Digitize this with resolution "n" according to "xbins""xout".
   // If output waveform is not specified, this waveform is changed.
   //
   //  xbins    0   1   2   3        n-2
   //         --|---|---|---|---------|-------
   //  bin#   0   1   2   3            n-1
   //   0 : underflow
   //  n-1: overflow
   //
   //  If input analog value x is between xbins[i] and xbins[i+1],
   //  then output digitized value is xout[i+1]

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }

   Int_t iPnt;
   Int_t bin;
   for (iPnt = 0; iPnt < fNPoints; iPnt++) {
      // Find bin
      if (fAmplitude[iPnt] < xbins[0]) {              // underflow
         bin = 0;
      } else  if (!(fAmplitude[iPnt] < xbins[n - 2])) { // overflow
         bin = n - 1;
      } else {
         bin = 1 + (Int_t)TMath::BinarySearch(n - 2, xbins, fAmplitude[iPnt]);
      }

      wfout->fAmplitude[iPnt] = xout[bin];
   }
}


//__________________________________________________________________________________
void MEGWaveform::Rebin(Int_t rebinsize, MEGWaveform *wfout) const
{
   // Rebin the waveform according to the 'rebinsize'
   // Use for fix bin size waveform and output waveform is fix bin size.

   if (rebinsize <= 1) { // No rebin
      wfout->SetNPoints(fNPoints);
      wfout->SetAmplitude(fAmplitude);
      return;
   }

   wfout->ResetAmplitude();
   Int_t iPnt(0), iBin(0), jPnt;
   while (iPnt < fNPoints) {
      for (jPnt = 0; jPnt < rebinsize; jPnt++) {
         wfout->fAmplitude[iBin] += fAmplitude[iPnt];
         iPnt++;
      }
      wfout->fAmplitude[iBin] /= rebinsize;
      iBin++;
   }
   wfout->SetNPoints(iBin);
}

//__________________________________________________________________________________
void MEGWaveform::Rebin(Int_t npoints, const Int_t *rebinsize, MEGWaveform *wfout) const
{
   // Rebin the waveform according to the 'rebinsize'
   // Use for fix bin size waveform and output waveform is fix bin size.

   if (rebinsize[0] == 0) { // No rebin
      wfout->SetNPoints(fNPoints);
      wfout->SetAmplitude(fAmplitude);
      return;
   }

   wfout->SetNPoints(npoints);
   wfout->ResetAmplitude();
   Int_t iPnt(0), iBin(0), jPnt;
   while (iBin < npoints && iPnt < fNPoints) {
      if (rebinsize[iBin] <= 1) {
         wfout->fAmplitude[iBin] = fAmplitude[iPnt];
         iPnt++;
         iBin++;
      } else {
         for (jPnt = 0; jPnt < rebinsize[iBin]; jPnt++) {
            wfout->fAmplitude[iBin] += fAmplitude[iPnt];
            iPnt++;
         }
         wfout->fAmplitude[iBin] /= rebinsize[iBin];
         iBin++;
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::Clip(Double_t threshold, MEGWaveform *wfout, Int_t polarity, Double_t noiserms)
{
   // Clip signal over(under) threshold when polarity is positive (negative).

   if (wfout) {
      wfout->SetTime(this);
      wfout->SetAmplitude(fAmplitude);
   } else {
      wfout = this;
   }

   if (polarity == 0) {
      polarity = (threshold < 0) ? -1 : 1;
   }

   Int_t iPnt;
   if (noiserms > 0) {
      static TRandom* random = new TRandom();
      if (polarity > 0) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (fAmplitude[iPnt] > threshold) {
               wfout->fAmplitude[iPnt] = threshold;
               wfout->fAmplitude[iPnt] += random->Gaus(0, noiserms);
            }
         }
      } else {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (fAmplitude[iPnt] < threshold) {
               wfout->fAmplitude[iPnt] = threshold;
               wfout->fAmplitude[iPnt] += random->Gaus(0, noiserms);
            }
         }
      }

   } else {
      if (polarity > 0) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (fAmplitude[iPnt] > threshold) {
               wfout->fAmplitude[iPnt] = threshold;
            }
         }
      } else {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (fAmplitude[iPnt] < threshold) {
               wfout->fAmplitude[iPnt] = threshold;
            }
         }
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::Reflection(Double_t delay, Double_t ratio, MEGWaveform *wfout, Int_t startpnt) const
{
   // Reflection appear after "delay" at the rate of "ratio"
   // Assume fix bin size.

   if (startpnt >= fNPoints) {
      return;
   }
   if (startpnt < 0) {
      startpnt = 0;
   }
   Int_t    iPnt;
   Int_t    pnt;
   Double_t temp;
   Int_t    delaypnt = static_cast<Int_t>(delay / fBinSize);
   wfout->SetTime(this);
   wfout->ResetAmplitude();
   for (iPnt = startpnt; iPnt < fNPoints; iPnt++) {
      pnt = iPnt;
      temp = fAmplitude[iPnt];
      if (temp) {
         while (pnt < fNPoints) {
            wfout->fAmplitude[pnt] += (1 - ratio) * temp;
            temp *= ratio;
            pnt += delaypnt;
         }
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::FIR(Int_t nkernel, const Double_t h[], MEGWaveform *wfout, Int_t type) const
{
   // FIR (Finite Impulse Response) filter
   // "h[]" is the filter kernel and "nkernel" is its length.
   //  Output of this filter y[i] is expressed as:
   //          y[i] = h[0]*x[i] + h[1]*x[i-1] + .... + h[n]*x[i-n]
   //               = Sigma( h[j]*x[i-j] )
   //
   // "type" specifies the algorithm to be used,
   //           0 : the Output Side Algorithm (default)
   //           1 : the Input Side Algorithm

   Int_t iPnt, jPnt;
   wfout->SetTime(this);
   wfout->ResetAmplitude();

   switch (type) {
   case 0:
   default:
      // Output side algorithm
      for (iPnt = 0; iPnt < nkernel; iPnt++) {
         for (jPnt = 0; jPnt < iPnt + 1; jPnt++) {
            wfout->fAmplitude[iPnt] += h[jPnt] * fAmplitude[iPnt - jPnt];
         }
      }
      for (iPnt = nkernel; iPnt < fNPoints; iPnt++) {
         for (jPnt = 0; jPnt < nkernel; jPnt++) {
            wfout->fAmplitude[iPnt] += h[jPnt] * fAmplitude[iPnt - jPnt];
         }
      }
      break;

   case 1:
      // Input side algorithm
      for (iPnt = 0; iPnt < fNPoints - (nkernel - 1); iPnt++) {
         if (fAmplitude[iPnt] == 0.) {
            continue;
         }
         for (jPnt = 0; jPnt < nkernel; jPnt++) {
            wfout->fAmplitude[iPnt + jPnt] += fAmplitude[iPnt] * h[jPnt];
         }
      }
      for (iPnt = fNPoints - (nkernel - 1); iPnt < fNPoints; iPnt++) {
         if (fAmplitude[iPnt] == 0.) {
            continue;
         }
         for (jPnt = 0; jPnt < fNPoints - iPnt; jPnt++) {
            wfout->fAmplitude[iPnt + jPnt] += fAmplitude[iPnt] * h[jPnt];
         }
      }
      break;
   }
}

//__________________________________________________________________________________
void MEGWaveform::FFTConvolution(Int_t nkernel, const Double_t h[], MEGWaveform *wfout, Int_t planlevel) const
{
   // FFT Convolution
   // Compute convolution via frequency domain.
   // Result is identical to FIR filter.
   // "h[]" is the filter kernel (impulse response) and "nkernel" is its length.
   // "planlevel" : 0-3,  how much time to be spent to search for FFT engine.

   // Create FFT objects and frequency response
   MEGFrequencyResponse freqres;
   freqres.FormFrequencyResponse(nkernel, h, planlevel);

   FFTConvolution(&freqres, wfout);
}

//__________________________________________________________________________________
void MEGWaveform::FFTConvolution(MEGFrequencyResponse *freqres, MEGWaveform *wfout) const
{
   // FFT Convolution
   // Compute convolution via frequency domain.
   // Result is identical to FIR filter.
   // "freqres->fNKernel" is the length of original impulse response.
   // "freqres->fFreqRes[]" is the frequency response. [2n]:Real part, [2n+1]:Imaginary part.
   //                       Its size is the fft transform size, not freqres->fNKernel.
   // "freqres->fFFTR2C" is a TVirtualFFT object which calculate R2C DFT.
   // "freqres->fFFTC2R" is a TVirtualFFT object which calculate C2R DFT.

   Int_t *extendedN = freqres->fFFTR2C->GetN();// transform size
   Int_t signalSegN = extendedN[0] - freqres->fNKernel + 1;// length (# of non-zero point) of segment
   Int_t nSignalSeg = fNPoints / signalSegN + 1; // number of segment

   vector<Double_t> signalSegIn(2 * ((extendedN[0] + 1) / 2 + 1));
   vector<Double_t> signalSegOut(2 * ((extendedN[0] + 1) / 2 + 1));
   wfout->SetTime(this);
   wfout->ResetAmplitude();

   Int_t iSeg, iPnt;
   for (iSeg = 0; iSeg < nSignalSeg; iSeg++) {
      memset(&signalSegIn[0], 0, sizeof(Double_t) * 2 * ((extendedN[0] + 1) / 2 + 1));
      if (iSeg == nSignalSeg - 1) {
         Int_t remain = fNPoints - iSeg * signalSegN;
         memcpy(&signalSegIn[0], fAmplitude + iSeg * signalSegN, sizeof(Double_t)*remain);
      } else {
         memcpy(&signalSegIn[0], fAmplitude + iSeg * signalSegN, sizeof(Double_t)*signalSegN);
      }

      // DFT
      freqres->fFFTR2C->SetPoints(&signalSegIn[0]);
      freqres->fFFTR2C->Transform();
      freqres->fFFTR2C->GetPoints(&signalSegIn[0]);

      // Convolution in Frequency domain (multiplication of complex numbers)
      for (iPnt = 0; iPnt < (extendedN[0] + 1) / 2 + 1; iPnt++) {
         //Real part
         signalSegOut[iPnt * 2] = signalSegIn[iPnt * 2] * freqres->fFreqRes[iPnt * 2] - signalSegIn[iPnt * 2 + 1] *
                                  freqres->fFreqRes[iPnt * 2 + 1];
         //Imaginal part
         signalSegOut[iPnt * 2 + 1] = signalSegIn[iPnt * 2 + 1] * freqres->fFreqRes[iPnt * 2] + signalSegIn[iPnt * 2] *
                                      freqres->fFreqRes[iPnt * 2 + 1];
      }

      // Inverse DFT
      freqres->fFFTC2R->SetPoints(&signalSegOut[0]);
      freqres->fFFTC2R->Transform();
      freqres->fFFTC2R->GetPoints(&signalSegOut[0]);

      for (iPnt = 0; iPnt < extendedN[0] && iPnt + iSeg * signalSegN < fNPoints; iPnt++) {
         wfout->fAmplitude[iPnt + iSeg * signalSegN] += signalSegOut[iPnt] /
                                                        extendedN[0]; // normalization factor of IDFT
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::MovingAverage(Int_t avepnts, Int_t startpnt, Int_t endpnt, Bool_t causal, Bool_t lowpass)
{
   // Moving Average this waveform
   // this waveform is overwritten by filterd one.
   MEGWaveform* wftmp = new MEGWaveform(*this);
   wftmp->MovingAverage(avepnts, this, startpnt, endpnt, causal, lowpass);
   delete wftmp;
}

//__________________________________________________________________________________
void MEGWaveform::MovingAverage(Int_t avepnts, MEGWaveform *wfout, Int_t startpnt, Int_t endpnt,
                                Bool_t causal, Bool_t lowpass) const
{
   // Moving Average this waveform
   // Average points are chosen symmetrically and
   // "avepnts" must be an ODD number.
   // It is one of FIR filter but implemented using recursive algorithm.
   // Moving Average is the best smoothing filter, but the worst low-pass filter.
   // Cut off frequency : f = 0.443 / (avepnts*fBinSize) .


   if (avepnts <= 1) {
      wfout->SetTime(this);
      memcpy(wfout->fAmplitude, fAmplitude, sizeof(Double_t)*fNPoints);
      return;
   }

   Int_t halfpnts = avepnts / 2;
   Double_t avepntsInv = 1. / (halfpnts * 2 + 1);
   if (!causal) {
      if (startpnt == endpnt) {
         startpnt = halfpnts;
         endpnt = fNPoints - 1 - halfpnts;
      } else {
         startpnt = (startpnt > halfpnts) ? startpnt : halfpnts;
         endpnt = (endpnt < fNPoints - halfpnts) ? endpnt : fNPoints - 1 - halfpnts;
      }
      if (startpnt >= endpnt) {
         return;
      }

      Int_t iPnt;
      wfout->SetTime(this);
      wfout->ResetAmplitude();
      wfout->fAmplitude[startpnt] = accumulate(fAmplitude + (startpnt - halfpnts)
                                               , fAmplitude + (startpnt + halfpnts + 1), 0.);
      for (iPnt = startpnt + 1; iPnt < endpnt; iPnt++) {
         wfout->fAmplitude[iPnt] = wfout->fAmplitude[iPnt - 1]
                                   - fAmplitude[iPnt - 1 - halfpnts]
                                   + fAmplitude[iPnt + halfpnts];
      }
   } else {
      if (startpnt == endpnt) {
         startpnt = halfpnts * 2;
         endpnt = fNPoints - 1;
      } else {
         startpnt = (startpnt > 2 * halfpnts) ? startpnt : 2 * halfpnts;
      }
      if (startpnt >= endpnt) {
         return;
      }

      Int_t iPnt;
      wfout->SetTime(this);
      wfout->ResetAmplitude();
      wfout->fAmplitude[startpnt] = accumulate(fAmplitude + (startpnt - 2 * halfpnts)
                                               , fAmplitude + (startpnt + 1), 0.);
      for (iPnt = startpnt + 1; iPnt < endpnt; iPnt++) {
         wfout->fAmplitude[iPnt] = wfout->fAmplitude[iPnt - 1]
                                   - fAmplitude[iPnt - 1 - 2 * halfpnts]
                                   + fAmplitude[iPnt];
      }
   }
   transform(wfout->fAmplitude + startpnt, wfout->fAmplitude + endpnt, wfout->fAmplitude + startpnt,
             //[&](Double_t &a){ return a * avepntsInv;});
             bind(multiplies<Double_t>(), placeholders::_1, avepntsInv));

   // High-pass filter
   if (!lowpass) {
      transform(fAmplitude + startpnt, fAmplitude + endpnt
                , wfout->fAmplitude + startpnt, wfout->fAmplitude + startpnt, minus<Double_t>());
   }

}

//__________________________________________________________________________________
void MEGWaveform::IntegrationCircuit(Double_t rc, MEGWaveform *wfout, Int_t startpnt)
{
   // RC shaping of integration circuit with time constant t=rc.
   // It works as a low-pass filter (IIR).
   // Cut off frequency : f = 1 / (2Pi*rc)
   // If output waveform is not specified, this waveform is changed.

   if (rc <= 0) {
      return;
   }
   if (startpnt >= fNPoints) {
      return;
   }

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }

   if (startpnt < 0) {
      startpnt = 0;
   }
   Double_t temp = 0;
   Int_t iPnt;
   Double_t factor = fBinSize / rc;
   memset(wfout->fAmplitude, 0, sizeof(Double_t)*startpnt);
   for (iPnt = startpnt; iPnt < fNPoints; iPnt++) {
      temp += (fAmplitude[iPnt] - temp) * factor;
      wfout->fAmplitude[iPnt] = temp;
   }
}

//__________________________________________________________________________________
void MEGWaveform::DifferentiationCircuit(Double_t cr, MEGWaveform *wfout, Int_t startpnt)
{
   // CR shaping of differentiation circuit with time constant t=cr.
   // It works as a high-pass filter (IIR).
   // If output waveform is not specified, this waveform is changed.

   if (cr <= 0) {
      return;
   }
   if (startpnt >= fNPoints) {
      return;
   }

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }

   if (startpnt < 0) {
      startpnt = 0;
   }
   Int_t iPnt;
   Double_t temp, templast;
   memset(wfout->fAmplitude, 0, sizeof(Double_t) * (startpnt + 1));
   temp = fAmplitude[startpnt];
   for (iPnt = startpnt + 1; iPnt < fNPoints; iPnt++) {
      templast = temp;
      temp = fAmplitude[iPnt];
      wfout->fAmplitude[iPnt] = (1 - fBinSize / cr)
                                * (wfout->fAmplitude[iPnt - 1] + temp - templast);
   }
}

//__________________________________________________________________________________
void MEGWaveform::Differentiate(Int_t diffpnts, MEGWaveform *wfout) const
{
   // Differentiate this waveform.
   // Differential coefficient is calculated as a slope between two points removed "diffpnts"

   wfout->SetTime(this);
   Int_t iPnt;
   for (iPnt = 0; iPnt < diffpnts / 2; iPnt++) {
      wfout->fAmplitude[iPnt] = 0;
   }

   if (fFixBinSize) {
      for (iPnt = diffpnts / 2; iPnt < fNPoints - diffpnts / 2; iPnt++) {
         wfout->fAmplitude[iPnt] = (fAmplitude[iPnt + diffpnts / 2] - fAmplitude[iPnt - diffpnts / 2])
                                   / ((diffpnts / 2 + diffpnts / 2) * fBinSize);
      }
   } else {
      for (iPnt = diffpnts / 2; iPnt < fNPoints - diffpnts / 2; iPnt++) {
         wfout->fAmplitude[iPnt] = (fAmplitude[iPnt + diffpnts / 2] - fAmplitude[iPnt - diffpnts / 2])
                                   / (fTime[iPnt + diffpnts / 2] - fTime[iPnt - diffpnts / 2]);
      }
   }

   for (iPnt = fNPoints - diffpnts / 2; iPnt < fNPoints; iPnt++) {
      wfout->fAmplitude[iPnt] = 0;
   }
}

//__________________________________________________________________________________
void MEGWaveform::Differentiate(Int_t diffpnts, MEGWaveform *wfout, Int_t ndiff) const
{
   // Differentiate this waveform.
   // Differential coefficient is calculated as a slope between two points removed "diffpnts"
   wfout->SetTime(this);
   Double_t* tempWF = new Double_t[fNPoints];
   Double_t* diffWF = new Double_t[fNPoints];
   for (Int_t iPnt = 0; iPnt < fNPoints; iPnt++) {
      tempWF[iPnt] = fAmplitude[iPnt];
   }

   for (Int_t iDiff = 0; iDiff < ndiff; iDiff++) {
      Int_t iPnt;
      for (iPnt = 0; iPnt < diffpnts / 2; iPnt++) {
         diffWF[iPnt] = 0;
      }

      if (fFixBinSize) {
         for (iPnt = diffpnts / 2; iPnt < fNPoints - diffpnts / 2; iPnt++) {
            diffWF[iPnt] = (tempWF[iPnt + diffpnts / 2] - tempWF[iPnt - diffpnts / 2]) /
                           ((diffpnts / 2 + diffpnts / 2) * fBinSize);
         }
      } else {
         for (iPnt = diffpnts / 2; iPnt < fNPoints - diffpnts / 2; iPnt++) {
            if (fTime[iPnt + diffpnts / 2] != fTime[iPnt - diffpnts / 2]) {
               diffWF[iPnt] = (tempWF[iPnt + diffpnts / 2] -
                               tempWF[iPnt - diffpnts / 2]) /
                              (fTime[iPnt + diffpnts / 2] - fTime[iPnt - diffpnts / 2]);
            } else {
               diffWF[iPnt] = diffWF[iPnt - 1];
               Report(R_WARNING, "fTime:[%d]: %lf\n", iPnt + diffpnts / 2, fTime[iPnt + diffpnts / 2]);
               // cout << "fTime:[" << iPnt+diffpnts/2 << "]: " << fTime[iPnt+diffpnts/2] << endl;
            }
         }
      }

      for (iPnt = fNPoints - diffpnts / 2; iPnt < fNPoints; iPnt++) {
         diffWF[iPnt] = 0;
      }

      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         tempWF[iPnt] = diffWF[iPnt];
      }
   }
   wfout->SetAmplitude(tempWF);
   delete [] tempWF;
   delete [] diffWF;
}

//__________________________________________________________________________________
void MEGWaveform::MedianFilter(MEGWaveform *wfout, Double_t threshold)
{
   // Apply median filter
   // When wfout is null, this waveform is changed.

   if (!wfout) {
      wfout = this;
   } else {
      wfout->SetNPoints(fNPoints);
   }

   vector<Double_t> amp(fNPoints);
   vector<Double_t> window(3);

   Int_t iPnt;
   amp[0] = fAmplitude[0];
   amp[fNPoints - 1] = fAmplitude[fNPoints - 1];
   for (iPnt = 1; iPnt < fNPoints - 1; iPnt++) {
      window[0] = fAmplitude[iPnt - 1];
      window[1] = fAmplitude[iPnt];
      window[2] = fAmplitude[iPnt + 1];
      sort(window.begin(), window.end());
      if (TMath::Abs(window[1] - fAmplitude[iPnt]) > threshold) {
         amp[iPnt] = window[1];
      } else {
         amp[iPnt] = fAmplitude[iPnt];
      }
   }
   wfout->SetAmplitude(&amp[0]);
}

//__________________________________________________________________________________
void MEGWaveform::ChebyshevFilter(Double_t a[], Double_t b[], Int_t pole, MEGWaveform *wfout, Int_t startpnt,
                                  Int_t endpnt)
{
   // Chebyshev filter with 2 , 4 or 6 poles.
   // It works as a low-pass filter or a high-pass filter according to the coefficients (IIR).
   // If output waveform is not specified, this waveform is changed.

   if (startpnt >= fNPoints) {
      return;
   }
   if (endpnt == 0) {
      endpnt = fNPoints;
   }
   startpnt -= pole;
   if (startpnt < 0) {
      startpnt = 0;
   }
   if (endpnt > fNPoints) {
      endpnt = fNPoints;
   }

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }

   memset(wfout->fAmplitude, 0, sizeof(Double_t) * (startpnt + pole));
   memset(wfout->fAmplitude + endpnt, 0, sizeof(Double_t) * (fNPoints - endpnt));
   switch (pole) {
   case 2:
      for (Int_t iPnt = startpnt + pole; iPnt < endpnt; iPnt++) {
         wfout->fAmplitude[iPnt] =
            a[0] * fAmplitude[iPnt] + a[1] * fAmplitude[iPnt - 1] + a[2] * fAmplitude[iPnt - 2]
            + b[1] * wfout->fAmplitude[iPnt - 1] + b[2] * wfout->fAmplitude[iPnt - 2]; // not use b[0]
         if (iPnt > 100 && iPnt < 300) {
            continue;
         }
      }
      break;
   case 4:
      for (Int_t iPnt = startpnt + pole; iPnt < endpnt; iPnt++) {
         wfout->fAmplitude[iPnt] =
            a[0] * fAmplitude[iPnt] + a[1] * fAmplitude[iPnt - 1] + a[2] * fAmplitude[iPnt - 2]
            + a[3] * fAmplitude[iPnt - 3] + a[4] * fAmplitude[iPnt - 4]
            + b[1] * wfout->fAmplitude[iPnt - 1] + b[2] * wfout->fAmplitude[iPnt - 2]
            + b[3] * wfout->fAmplitude[iPnt - 3] + b[4] * wfout->fAmplitude[iPnt - 4]; // not use b[0]
      }
      break;
   case 6:
      for (Int_t iPnt = startpnt + pole; iPnt < endpnt; iPnt++) {
         wfout->fAmplitude[iPnt] =
            a[0] * fAmplitude[iPnt] + a[1] * fAmplitude[iPnt - 1] + a[2] * fAmplitude[iPnt - 2]
            + a[3] * fAmplitude[iPnt - 3] + a[4] * fAmplitude[iPnt - 4]
            + a[5] * fAmplitude[iPnt - 5] + a[6] * fAmplitude[iPnt - 6]
            + b[1] * wfout->fAmplitude[iPnt - 1] + b[2] * wfout->fAmplitude[iPnt - 2]
            + b[3] * wfout->fAmplitude[iPnt - 3] + b[4] * wfout->fAmplitude[iPnt - 4]
            + b[5] * wfout->fAmplitude[iPnt - 5] + b[6] * wfout->fAmplitude[iPnt - 6]; // not use b[0]
      }
      break;
   default:
      break;
   }
}

//__________________________________________________________________________________
void MEGWaveform::ChebyshevCoefficient(Double_t *a, Double_t *b, Double_t fc, Int_t ripple, Int_t pole,
                                       Bool_t lowpass)
{
   // Calculate coefficients for Chebyshev filer.
   // Input :
   //   'fc'      : cut off frequency (0 to 0.5).
   //   'ripple'  : percent ripple (0 to 29).
   //   'pole'    : number of poles (2,4,...,20).
   //   'lowpass' : true for low-pass filter, false for hight-pass filter.
   // Output :
   //   'a','b'   : Chebyshev coefficients

   Double_t ta[23], tb[23];
   memset(a, 0, sizeof(Double_t) * 23);
   memset(b, 0, sizeof(Double_t) * 23);
   a[2] = 1;
   b[2] = 1;
   for (Int_t iPole = 0; iPole < pole / 2; iPole++) {

      // Calculate the pole location on the unit circle
      Double_t rp = -TMath::Cos(pi / (pole * 2) + iPole * pi / pole);
      Double_t ip = TMath::Sin(pi / (pole * 2) + iPole * pi / pole);

      // Warp from a circle to an ellipse
      if (ripple) {
         Double_t es = TMath::Sqrt((100. / (100 - ripple)) * (100. / (100 - ripple)) - 1);
         Double_t vx = (1. / pole) * TMath::Log((1 / es) + TMath::Sqrt(1 / (es * es) + 1));
         Double_t kx = (1. / pole) * TMath::Log((1 / es) + TMath::Sqrt(1 / (es * es) - 1));
         kx = (TMath::Exp(kx) + TMath::Exp(-kx)) / 2;
         rp = rp * ((TMath::Exp(vx) - TMath::Exp(-vx)) / 2) / kx;
         ip = ip * ((TMath::Exp(vx) + TMath::Exp(-vx)) / 2) / kx;
      }

      // s-domain to z-domain conversion
      Double_t t = 2 * TMath::Tan(0.5);
      Double_t w = 2 * pi * fc;
      Double_t m = rp * rp + ip * ip;
      Double_t d = 4 - 4 * rp * t + m * t * t;
      Double_t x0 = t * t / d;
      Double_t x1 = 2 * t * t / d;
      Double_t x2 = t * t / d;
      Double_t y1 = (8 - 2 * m * t * t) / d;
      Double_t y2 = (-4 - 4 * rp * t - m * t * t) / d;

      // low-pass to low-pass, or low-pass to high-pass transform
      Double_t k;
      if (!lowpass) {
         k = -TMath::Cos(w / 2 + 0.5) / TMath::Cos(w / 2 - 0.5);
      } else {
         k = TMath::Sin(0.5 - w / 2) / TMath::Sin(0.5 + w / 2);
      }
      d = 1 + y1 * k - y2 * k * k;
      Double_t a0 = (x0 - x1 * k + x2 * k * k) / d;
      Double_t a1 = (-2 * x0 * k + x1 + x1 * k * k - 2 * x2 * k) / d;
      Double_t a2 = (x0 * k * k - x1 * k + x2) / d;
      Double_t b1 = (2 * k + y1 + y1 * k * k - 2 * y2 * k) / d;
      Double_t b2 = (-k * k - y1 * k + y2) / d;
      if (!lowpass) {
         a1 *= -1;
         b1 *= -1;
      }

      // Add coefficients to the cascade
      memcpy(ta, a, sizeof(Double_t) * 23);
      memcpy(tb, b, sizeof(Double_t) * 23);
      for (Int_t iCoe = 2; iCoe < 23; iCoe++) {
         a[iCoe] = a0 * ta[iCoe] + a1 * ta[iCoe - 1] + a2 * ta[iCoe - 2];
         b[iCoe] = tb[iCoe] - b1 * tb[iCoe - 1] - b2 * tb[iCoe - 2];
      }
   }

   b[2] = 0;
   for (Int_t iCoe = 0; iCoe < 21; iCoe++) {
      a[iCoe] = a[iCoe + 2];
      b[iCoe] = -b[iCoe + 2];
   }

   // Normalize the gain
   Double_t sa = 0, sb = 0;
   for (Int_t iCoe = 0; iCoe < 21; iCoe++) {
      if (lowpass) {
         sa += a[iCoe];
         sb += b[iCoe];
      } else {
         sa += a[iCoe] * TMath::Power(-1, iCoe);
         sb += b[iCoe] * TMath::Power(-1, iCoe);
      }
   }
   Double_t gain = sa / (1 - sb);
   for (Int_t iCoe = 0; iCoe < 21; iCoe++) {
      a[iCoe] /= gain;
   }
}

//__________________________________________________________________________________
void MEGWaveform::FormMatchedFilterKernel(const MEGWaveform *wfin, MEGWaveform *kernel, Int_t npoints,
                                          Double_t binsize, Double_t tstart)
{
   // Form a kernel for matched filter (reverse of template signal)
   // Input template waveform.

   // Sampling
   wfin->Sampling(npoints, tstart, binsize, kernel, "precise");
   // Set gain = 1
   Double_t scale = accumulate(kernel->fAmplitude, kernel->fAmplitude + npoints, 0.);
   *kernel *= 1. / scale;
   // Reverse
   kernel->Reverse();
}

//__________________________________________________________________________________
void MEGWaveform::FormFilterKernel(const MEGWaveform *wfin, MEGWaveform *wfout, MEGWaveform* kernel,
                                   Int_t npoints)
{
   // Form a kernel using FFT

   // Time domain signals


   // Frequency domain signals

   vector<Double_t>outSp(2 * ((npoints + 1) / 2 + 1), 0);
   vector<Double_t>inSp(2 * ((npoints + 1) / 2 + 1), 0);
   vector<Double_t>filterSp(2 * ((npoints + 1) / 2 + 1), 0);

   // Fourier Transform of input signal
   Int_t nsize = npoints;
   TVirtualFFT *fft = TVirtualFFT::FFT(1, &nsize, "R2C ES K");
   fft->SetPoints(wfin->GetAmplitude());
   fft->Transform();
   fft->GetPoints(&inSp[0]);

   // Fourier Transform of output signal
   fft->SetPoints(wfout->GetAmplitude());
   fft->Transform();
   fft->GetPoints(&outSp[0]);

   // Spectrum of filter
   for (Int_t iBin = 0; iBin < npoints / 2 + 1; iBin++) {
      // Real part
      filterSp[iBin * 2] = (outSp[iBin * 2] * inSp[iBin * 2] + outSp[iBin * 2 + 1] * inSp[iBin * 2 + 1])
                           / (inSp[iBin * 2] * inSp[iBin * 2] + inSp[iBin * 2 + 1] * inSp[iBin * 2 + 1]);
      // Imaginary part
      filterSp[iBin * 2 + 1] = (outSp[iBin * 2 + 1] * inSp[iBin * 2] - outSp[iBin * 2] * inSp[iBin * 2 + 1])
                               / (inSp[iBin * 2] * inSp[iBin * 2] + inSp[iBin * 2 + 1] * inSp[iBin * 2 + 1]);
   }

   // Inverse Fourier Transform of response spectrum -> Get impulse response
   TVirtualFFT *fftInv = TVirtualFFT::FFT(1, &nsize, "C2R ES K");
   fftInv->SetPoints(&filterSp[0]);
   fftInv->Transform();

   kernel->SetNPoints(npoints);
   MEGWaveform::TransformWaveform(fftInv, kernel, "RE");
   *kernel /= npoints;// normalization factor of InversDFT

   delete fft;
   delete fftInv;
}

//__________________________________________________________________________________
void MEGWaveform::BlackmanWindow(Int_t nkernel, Double_t h[])
{
   // Multiplying the filter kernel (impulse response) by the Blackman window

   for (Int_t iPnt = 0; iPnt < nkernel; iPnt++) {
      h[iPnt] *= 0.42 - 0.5 * TMath::Cos(twopi * iPnt / (nkernel + 1)) + 0.08 * TMath::Cos(2 * twopi * iPnt /
                 (nkernel + 1));
   }
}

//__________________________________________________________________________________
TVirtualFFT* MEGWaveform::FFT(TVirtualFFT *fft, Option_t *option) const
{
   // Fast discrete Fourier transforms using TVirtualFFT,
   // interface to use FFTW (www.fftw.org) in ROOT
   //
   // Parameters:
   //  "fft" - transform object to be used.
   //          If a null pointer is passed, a new fft is created
   //          and returned, otherwise, the provided fft is used.
   //
   //  "options" option parameters consists of 3 parts:
   //    1) option of transform type
   //      "R2C"  - real to complex transforms - default
   //      "R2HC" - real to halfcomplex (special format of storing output data,
   //               results the same as for R2C)
   //      "DHT"  - discrete Hartley transform
   //               real to real transforms (sine and cosine):
   //      "R2R_0", "R2R_1", "R2R_2", "R2R_3" - discrete cosine transforms of types I-IV
   //      "R2R_4", "R2R_5", "R2R_6", "R2R_7" - discrete sine transforms of types I-IV
   //
   //    2) option of transform flag: choosing how much time should be spent in planning the transform
   //      "ES" (from "estimate")   - no time in preparing the transform, but probably sub-optimal
   //                                 performance
   //      "M" (from "measure")     - some time spend in finding the optimal way to do the transform
   //      "P" (from "patient")     - more time spend in finding the optimal way to do the transform
   //      "EX" (from "exhaustive") - the most optimal way is found
   //      This option should be chosen depending on how many transforms of the same size and
   //      type are going to be done. Planning is only done once, for the first transform of this
   //      size and type. Default is "ES".
   //    3) option allowing to choose between the global fgFFT and a new TVirtualFFT object
   //        ""                - changes and returns the global fgFFT variable
   //        "K" (from "keep") - without touching the global fgFFT,
   //        creates and returns a new TVirtualFFT*. User is then responsible for deleting it.
   //   Examples of valid options: "R2C ES K", "DHT P K", etc.

   TString opt = option;
   opt.ToUpper();

   // Create a new transform object adequate for this waveform
   if (!fft) {
      Int_t npoints = fNPoints;
      if (!opt.Contains("2R")) {
         if (!opt.Contains("2C") && !opt.Contains("2HC") && !opt.Contains("DHT")) {
            //no type specified, "R2C" by default
            opt.Append("R2C");
         }
         fft = TVirtualFFT::FFT(1, &npoints, opt.Data());
      } else {
         //find the kind of transform
         Int_t ind = opt.Index("R2R", 3);
         Int_t kind;
         Char_t t;
         t = opt[ind + 4];
         kind = atoi(&t);
         fft = TVirtualFFT::SineCosine(1, &npoints, &kind, option);
      }
   }

   // Transform this waveform
   fft->SetPoints(fAmplitude);
   fft->Transform();

   return fft;
}

//__________________________________________________________________________________
MEGWaveform* MEGWaveform::FFT(MEGWaveform *wfout, Option_t *option) const
{
   // Fast discrete Fourier transforms using TVirtualFFT,
   // interface to use FFTW (www.fftw.org) in ROOT
   //
   // To extract more information about the transform, use the function
   //  TVirtualFFT::GetCurrentTransform() to get a pointer to the current
   //  transform object.
   //
   // Parameters:
   //  1st - waveform for the output. If a null pointer is passed, a new wavefirn is created
   //  and returned, otherwise, the provided waveform is used and should be big enough
   //
   //  Options: option parameters consists of 3 parts:
   //    - option on what to return
   //      "RE" - returns a histogram of the real part of the output
   //      "IM" - returns a histogram of the imaginary part of the output
   //      "MAG"- returns a histogram of the magnitude of the output
   //      "PH" - returns a histogram of the phase of the output
   //    - option of transform type
   //    - option of transform flag
   //   Examples of valid options: "Mag R2C M" "Re R2R_11" "Im R2C ES" "PH R2HC EX"

   TString opt = option;
   opt.ToUpper();

   TVirtualFFT *fft;

   Int_t npoints = fNPoints;

   if (!opt.Contains("2R")) {
      if (!opt.Contains("2C") && !opt.Contains("2HC") && !opt.Contains("DHT")) {
         //no type specified, "R2C" by default
         opt.Append("R2C");
      }
      fft = TVirtualFFT::FFT(1, &npoints, opt.Data());
   } else {
      //find the kind of transform
      Int_t ind = opt.Index("R2R", 3);
      Int_t kind;
      Char_t t;
      t = opt[ind + 4];
      kind = atoi(&t);
      fft = TVirtualFFT::SineCosine(1, &npoints, &kind, option);
   }

   if (!fft) {
      return 0;
   }
   fft->SetPoints(fAmplitude);
   fft->Transform();
   wfout = TransformWaveform(fft, wfout, option);
   return wfout;
}

//__________________________________________________________________________________
MEGWaveform* MEGWaveform::FFT(TVirtualFFT *fft, MEGWaveform *wfout, Option_t *option) const
{
   // Fast discrete Fourier transforms using TVirtualFFT,
   // interface to use FFTW (www.fftw.org) in ROOT
   // Calculate DFT from a given transform (first parameter).
   // "option" is on what to return ("RE""IM""MAG""PH")

   if (!fft) {
      return 0;
   }
   fft->SetPoints(fAmplitude);
   fft->Transform();
   wfout = TransformWaveform(fft, wfout, option);
   return wfout;
}

//__________________________________________________________________________________
MEGWaveform*  MEGWaveform::TransformWaveform(TVirtualFFT* fft, MEGWaveform *wf_out, Option_t *option)
{
   // For a given transform (first parameter), fill the waveform (second parameter)
   // with the transform output data, specified in the third parameter
   // If the 2nd paramter "wfout" is NULL, a new waveform (MEGWaveform) is created
   // with fix time bin, and the user is responsible for deleting it.
   // Available options:
   //   "RE"  - real part of the output
   //   "IM"  - imaginary part of the output
   //   "MAG" - magnitude of the output
   //   "POW" - power of the output
   //   "PH"  - phase of the output

   TString opt = option;
   opt.ToUpper();

   Int_t *n = fft->GetN();
   TString type = fft->GetType();
   MEGWaveform *wfout = 0;
   if (wf_out) {
      wfout = wf_out;
   } else {
      Char_t name[10];
      sprintf(name, "out_%s", opt.Data());
      if (type.Contains("2C") || type.Contains("2HC")) {
         wfout = new MEGWaveform((n[0] + 1) / 2 + 1, 1);
      } else {
         wfout = new MEGWaveform(n[0], 1);
      }

      wfout->SetTitle(name);
   }

   Int_t iPnt;
   Int_t ind;
   Double_t re, im;
   if (opt.Contains("RE")) {
      if (type.Contains("2C") || type.Contains("2HC")) {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            fft->GetPointComplex(&ind, re, im);
            wfout->fAmplitude[iPnt] = re;
         }
      } else {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            wfout->fAmplitude[iPnt] = fft->GetPointReal(&ind);
         }
      }
   }
   if (opt.Contains("IM")) {
      if (type.Contains("2C") || type.Contains("2HC")) {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            fft->GetPointComplex(&ind, re, im);
            wfout->fAmplitude[iPnt] = im;
         }
      } else {
         printf("No complex numbers in the output");
         return 0;
      }
   }
   if (opt.Contains("MA")) {
      if (type.Contains("2C") || type.Contains("2HC")) {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            fft->GetPointComplex(&ind, re, im);
            wfout->fAmplitude[iPnt] = TMath::Sqrt(re * re + im * im);
         }
      } else {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            wfout->fAmplitude[iPnt] = TMath::Abs(fft->GetPointReal(&ind));
         }
      }
   }
   if (opt.Contains("POW")) {
      if (type.Contains("2C") || type.Contains("2HC")) {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            fft->GetPointComplex(&ind, re, im);
            wfout->fAmplitude[iPnt] = re * re + im * im;
         }
      } else {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            wfout->fAmplitude[iPnt] = TMath::Abs(fft->GetPointReal(&ind));
            wfout->fAmplitude[iPnt] *= wfout->fAmplitude[iPnt];
         }
      }
   }
   if (opt.Contains("PH")) {
      if (type.Contains("2C") || type.Contains("2HC")) {
         for (iPnt = 0; iPnt < wfout->fNPoints; iPnt++) {
            ind = iPnt;
            fft->GetPointComplex(&ind, re, im);
            if (re != 0) {
               wfout->fAmplitude[iPnt] = TMath::ATan2(im, re);
            }
         }
      } else {
         printf("Pure real output, no phase");
         return 0;
      }
   }

   return wfout;
}

//__________________________________________________________________________________
Double_t MEGWaveform::PinkFilter(Double_t in)
{
   // IIR implementation of 3dB/oct filter (pink (1/f) filter).

   const Int_t kMaxZ = 16;
   static Double_t z[kMaxZ] = {0,};
   static Double_t k[kMaxZ] = {0,};
   static Double_t t = 0.0;
   static Bool_t initialized = false;
   Double_t q;
   Int_t i;

   // init
   if (!initialized) {
      for (i = 0; i < kMaxZ; i++) {
         z[i] = 0;
      }
      k[kMaxZ - 1] = 0.5;
      for (i = kMaxZ - 1; i > 0; i--) {
         k[i - 1] = k[i] * 0.25;
      }
      initialized = true;
   }

   q = in;
   for (i = 0; i < kMaxZ; i++) {
      z[i] = (q * k[i] + z[i] * (1.0 - k[i]));
      q = (q + z[i]) * 0.5;
   }
   return (t = 0.75 * q + 0.25 * t); // add 1st order LPF
}
//__________________________________________________________________________________
void MEGWaveform::AddPinkNoise(Double_t scale, Int_t startpnt, Int_t endpnt, MEGWaveform *wfout)
{
   // Add approximate pink (1/f) noise.

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }
   if (!endpnt) {
      endpnt = fNPoints;
   }

   Int_t iPnt;
   Double_t noise;
   for (iPnt = startpnt; iPnt < endpnt; iPnt++) {
//          noise = PinkFilter(random() & 1 ? 1.0 : -1.0);
      noise = PinkFilter(gRandom->Rndm() > 0.5 ? 1.0 : -1.0) * scale;
      wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + noise;
   }
}

//__________________________________________________________________________________
void MEGWaveform::AddFilteredRandomNoise(Double_t sigma, Double_t fc, Int_t startpnt, Int_t endpnt,
                                         MEGWaveform *wfout)
{
   // Add filtered random noise.
   // Noise source is gaussian with "sigma".
   // Then pass a filter which is Integration-circuit and whose cut-off frequency is "fc".
   // If output waveform is not specified, this waveform is changed.

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }

   if (!endpnt) {
      endpnt = fNPoints;
   }

   // cut off frequency Fc = 1/(2pi*RC)
   if (fc > 0) {
      Double_t temp = 0;
      Int_t iPnt;
      Double_t noise;
      Double_t factor = twopi * fc * fBinSize;
      for (iPnt = startpnt; iPnt < endpnt; iPnt++) {
         noise = gRandom->Gaus(0., sigma);
         temp += (noise - temp) * factor;
         wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + temp;
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::AddSineNoise(Double_t amp, Double_t frequency, Double_t phase)
{
   // Add sine function to all points
   // This waveform is changed.

   TF1 fsin("fsin", "[0]*sin([1]*x-[2])", GetTimeMin(), GetTimeMax());
   fsin.SetParameters(amp, twopi * frequency, phase);
   AddFunction(&fsin);
}

//__________________________________________________________________________________
void MEGWaveform::AddFunction(TF1 *func)
{
   // Add amplitude from function "func" to all points
   // This waveform is changed.

   Int_t iPnt;
   if (fFixBinSize) {
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         fAmplitude[iPnt] += func->Eval(iPnt * fBinSize + fTimeMin);
      }
   } else {
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         fAmplitude[iPnt] += func->Eval(fTime[iPnt]);
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::AddTemplate(const MEGWaveform *wfin, Double_t mixtime, Option_t *opt)
{
   // Add a template waveform to this
   // This waveform is changed.
   // wfin->fBinSize is not necessarily equal to fBinSize.
   // t=0 of wfin is synchronized with t="mixtime" of this.

   TString option = opt;
   option.ToUpper();

   Double_t lowertime = wfin->GetTimeMin();

   Int_t iPnt;
   Int_t pntIn;
   Double_t iTime;
   Double_t ratio;

   iPnt = FindPoint(mixtime + lowertime) + 1;

   if (iPnt >= fNPoints - 1) {
      return;
   }

   iTime = GetTimeAt(iPnt) - mixtime;
   pntIn = wfin->FindPoint(iTime);

   if (option.Contains("PRECISE")) {

      if (fFixBinSize && wfin->fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1 && iPnt < fNPoints) {
            ratio = (iTime - (pntIn * wfin->fBinSize + wfin->fTimeMin)) / wfin->fBinSize;
            fAmplitude[iPnt] += wfin->fAmplitude[pntIn]
                                + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]);

            iPnt++;
            iTime += fBinSize;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else if (!fFixBinSize && wfin->fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1) {
            ratio = (iTime - (pntIn * wfin->fBinSize + wfin->fTimeMin)) / wfin->fBinSize;
            fAmplitude[iPnt] += wfin->fAmplitude[pntIn]
                                + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]);

            iPnt++;
            if (iPnt > fNPoints - 1) {
               break;
            }
            iTime = fTime[iPnt] - mixtime;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else if (fFixBinSize && !wfin->fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1 && iPnt < fNPoints) {
            ratio = (iTime - wfin->fTime[pntIn]) / (wfin->fTime[pntIn + 1] - wfin->fTime[pntIn]);
            fAmplitude[iPnt] += wfin->fAmplitude[pntIn]
                                + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]);

            iPnt++;
            iTime += fBinSize;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else {
         while (pntIn < wfin->fNPoints - 1) {
            ratio = (iTime - wfin->fTime[pntIn]) / (wfin->fTime[pntIn + 1] - wfin->fTime[pntIn]);
            fAmplitude[iPnt] += wfin->fAmplitude[pntIn]
                                + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]);

            iPnt++;
            if (iPnt > fNPoints - 1) {
               break;
            }
            iTime = fTime[iPnt] - mixtime;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      }

   } else {

      if (fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1 && iPnt < fNPoints) {
            fAmplitude[iPnt] += wfin->fAmplitude[pntIn];

            iPnt++;
            iTime += fBinSize;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else {
         while (pntIn < wfin->fNPoints - 1) {
            fAmplitude[iPnt] += wfin->fAmplitude[pntIn];

            iPnt++;
            if (iPnt > fNPoints - 1) {
               break;
            }
            iTime = fTime[iPnt] - mixtime;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::AddTemplate(const MEGWaveform *wfin, Double_t mixtime, Option_t *opt, MEGWaveform *wfout,
                              Double_t scale)
{
   // Add a template waveform to this
   // This waveform is changed.
   // wfin->fBinSize is not necessarily equal to fBinSize.
   // t=0 of wfin is synchronized with t="mixtime" of this.

   TString option = opt;
   option.ToUpper();

   if (wfout) {
      wfout->SetTime(this);
   } else {
      wfout = this;
   }


   Double_t lowertime = wfin->GetTimeMin();

   Int_t iPnt;
   Int_t pntIn;
   Double_t iTime;
   Double_t ratio;

   iPnt = FindPoint(mixtime + lowertime) + 1;

   if (iPnt >= fNPoints - 1) {
      return;
   }

   iTime = GetTimeAt(iPnt) - mixtime;
   pntIn = wfin->FindPoint(iTime);

   if (option.Contains("PRECISE")) {

      if (fFixBinSize && wfin->fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1 && iPnt < fNPoints) {
            ratio = (iTime - (pntIn * wfin->fBinSize + wfin->fTimeMin)) / wfin->fBinSize;
            wfout->fAmplitude[iPnt] = fAmplitude[iPnt] +  scale * (wfin->fAmplitude[pntIn]
                                                                   + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]));

            iPnt++;
            iTime += fBinSize;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else if (!fFixBinSize && wfin->fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1) {
            ratio = (iTime - (pntIn * wfin->fBinSize + wfin->fTimeMin)) / wfin->fBinSize;
            wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + scale * (wfin->fAmplitude[pntIn]
                                                                  + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]));

            iPnt++;
            if (iPnt > fNPoints - 1) {
               break;
            }
            iTime = fTime[iPnt] - mixtime;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else if (fFixBinSize && !wfin->fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1 && iPnt < fNPoints) {
            ratio = (iTime - wfin->fTime[pntIn]) / (wfin->fTime[pntIn + 1] - wfin->fTime[pntIn]);
            wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + scale * (wfin->fAmplitude[pntIn]
                                                                  + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]));

            iPnt++;
            iTime += fBinSize;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else {
         while (pntIn < wfin->fNPoints - 1) {
            ratio = (iTime - wfin->fTime[pntIn]) / (wfin->fTime[pntIn + 1] - wfin->fTime[pntIn]);
            wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + scale * (wfin->fAmplitude[pntIn]
                                                                  + ratio * (wfin->fAmplitude[pntIn + 1] - wfin->fAmplitude[pntIn]));

            iPnt++;
            if (iPnt > fNPoints - 1) {
               break;
            }
            iTime = fTime[iPnt] - mixtime;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      }

   } else {

      if (fFixBinSize) {
         while (pntIn < wfin->fNPoints - 1 && iPnt < fNPoints) {
            wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + scale * wfin->fAmplitude[pntIn];

            iPnt++;
            iTime += fBinSize;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      } else {
         while (pntIn < wfin->fNPoints - 1) {
            wfout->fAmplitude[iPnt] = fAmplitude[iPnt] + scale * wfin->fAmplitude[pntIn];

            iPnt++;
            if (iPnt > fNPoints - 1) {
               break;
            }
            iTime = fTime[iPnt] - mixtime;
            pntIn = wfin->FindPointIncrement(iTime, pntIn);
         }
      }
   }
}

//__________________________________________________________________________________
void MEGWaveform::Attenuate(Double_t factor)
{
   // Attenuate this waveform
   // When 'factor' is negative, a data member 'fAttenuation' is used.
   // When 'factor' is positive, a data member 'fAttenuation' is overwritten
   // by the number.
   if (factor > 0) {
      SetAttenuation(factor);
   }
   if (TMath::Abs(fAttenuation - 1.) > 0.0001) {
      *this *= fAttenuation;
   }
}

//__________________________________________________________________________________
void MEGWaveform::CorrectAttenuation(Double_t factor)
{
   // Attenuate this waveform
   // When 'factor' is negative, a data member 'fAttenuation' is used.
   // When 'factor' is positive, a data member 'fAttenuation' is overwritten
   // by the number.
   if (factor > 0) {
      SetAttenuation(factor);
   }
   if (TMath::Abs(fAttenuation - 1.) > 0.0001) {
      *this *= 1 / fAttenuation;
   }
}

//__________________________________________________________________________________
void MEGWaveform::Pol3(Double_t *coef, Double_t x, Double_t &y, Double_t &dy)
{
   // 3rd degree polynomial function

   y = coef[0] + coef[1] * x + coef[2] * x * x + coef[3] * x * x * x;
   dy = coef[1] + coef[2] * 2 * x + coef[3] * 3 * x * x;
}

//__________________________________________________________________________________
void MEGWaveform::Invert()
{
   // Invert signal

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude, negate<Double_t>());
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator+=(const MEGWaveform &wf1)
{
   // Operator +=
   // Add waveform to this just simply

   transform(fAmplitude, fAmplitude + fNPoints, wf1.fAmplitude, fAmplitude, plus<Double_t>());
   return *this;
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator-=(const MEGWaveform &wf1)
{
   // Operator -=
   // Sabtruct waveform from this just simply

   transform(fAmplitude, fAmplitude + fNPoints, wf1.fAmplitude, fAmplitude, minus<Double_t>());
   return *this;
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator+=(Double_t c1)
{
   // Operator +=  (Constant shift)
   // Add constant for all points

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a + c1;});
             bind(plus<Double_t>(), placeholders::_1, c1));
   return *this;
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator-=(Double_t c1)
{
   // Operator -=  (Constant shift)
   // Sabtruct constant from all points

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a - c1;});
             bind(minus<Double_t>(), placeholders::_1, c1));
   return *this;
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator*=(Double_t c1)
{
   // Operator *=  (Scale)
   // Multiply c1 for all points

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a * c1;});
             bind(multiplies<Double_t>(), placeholders::_1, c1));
   return *this;
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator/=(Double_t c1)
{
   // Operator /=  (Scale)
   // Divide all points by c1

   Double_t c = 1 / c1;
   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a * c;});
             bind(multiplies<Double_t>(), placeholders::_1, c));
   return *this;
}

//__________________________________________________________________________________
MEGWaveform& MEGWaveform::operator=(const MEGWaveform &wf)
{
   // Operator =

   if (this != &wf) {
      wf.TNamed::Copy(*this);
      wf.TAttLine::Copy(*this);
      wf.TAttFill::Copy(*this);
      wf.TAttMarker::Copy(*this);
      //fNPoints   = wf.fNPoints;
      SetNPoints(wf.fNPoints);
      fBinSize   = wf.fBinSize;
      fFixBinSize = wf.fFixBinSize;
      fTime      = wf.fTime;
      fID        = wf.fID;
      fOverflow  = wf.fOverflow;
      fTimeMin   = wf.fTimeMin;
      fTimeMax   = wf.fTimeMax;
      fHistogram = 0;
      fGraph     = 0;
      fMinimum   = wf.fMinimum;
      fMaximum   = wf.fMaximum;
      fDisplayAmplUnit = wf.fDisplayAmplUnit;
      fDisplayTimeUnit = wf.fDisplayAmplUnit;
      fEditable  = wf.fEditable;
      fAttenuation = wf.fAttenuation;
      memcpy(fAmplitude, wf.fAmplitude, sizeof(Double_t)*fNPoints);
   }
   return *this;
}

//__________________________________________________________________________________
MEGWaveform MEGWaveform::operator+(const MEGWaveform &wf1)
{
   // Operator +

   MEGWaveform wfnew = *this;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wf1.fAmplitude, wfnew.fAmplitude,
             plus<Double_t>());
   return wfnew;
}

//__________________________________________________________________________________
MEGWaveform MEGWaveform::operator-(const MEGWaveform &wf1)
{
   // Operator -

   MEGWaveform wfnew = *this;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wf1.fAmplitude, wfnew.fAmplitude,
             minus<Double_t>());
   return wfnew;
}

//__________________________________________________________________________________
MEGWaveform operator*(Double_t c1, const MEGWaveform &wf1)
{
   // Operator *

   MEGWaveform wfnew = wf1;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wfnew.fAmplitude,
             //[&](Double_t &a){ return a * c1;});
             bind(multiplies<Double_t>(), placeholders::_1, c1));
   return wfnew;
}

//__________________________________________________________________________________
MEGWaveform operator/(Double_t c1, const MEGWaveform &wf1)
{
   // Operator /

   MEGWaveform wfnew = wf1;
   Double_t c = 1 / c1;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wfnew.fAmplitude,
             //[&](Double_t &a){ return a * c;});
             bind(multiplies<Double_t>(), placeholders::_1, c));
   return wfnew;
}

//__________________________________________________________________________________
//
// MEGFrequencyResponse
//

//__________________________________________________________________________________
MEGFrequencyResponse::MEGFrequencyResponse()
   : fFFTR2C(0), fFFTC2R(0)
{
}

//__________________________________________________________________________________
MEGFrequencyResponse::~MEGFrequencyResponse()
{
   delete fFFTR2C;
   delete fFFTC2R;
}

//__________________________________________________________________________________
void MEGFrequencyResponse::FormFrequencyResponse(Int_t nkernel, const Double_t h[], Int_t planlevel)
{
   // Form a frequency response from impulse response using FFT.
   // "h[]" is the filter kernel (impulse response) and "nkernel" is its length.
   // "planlevel" : 0-3,  how much time to be spent to search for FFT engine.
   // "fNKernel" is the length of original impulse response.
   // "fFreqRes[]" is the frequency response. [2n]:Real part, [2n+1]:Imaginary part.
   //              Its size is the fft transform size, not freqres->fNKernel.
   // "fFFTR2C" is a TVirtualFFT object which calculate R2C DFT.
   // "fFFTC2R" is a TVirtualFFT object which calculate C2R DFT.

   Int_t extendedN; // DFT of this length is used (transform size)

   Int_t iPower = 5;
   while (1) {
      if (nkernel * 2 < TMath::Power(2, iPower)) {
         extendedN = static_cast<Int_t>(TMath::Power(2, iPower));
         break;
      }
      iPower++;
   }

   // Create FFT objects
   TString option;
   option = "R2C ";
   TString plan[4] = {"ES ", "M ", "P ", "EX "};
   option += plan[planlevel];
   option += "K";
   fFFTR2C = TVirtualFFT::FFT(1, &extendedN, option);
   option = "C2R ";
   option += plan[planlevel];
   option += "K";
   fFFTC2R = TVirtualFFT::FFT(1, &extendedN, option);

   // Calculate frequency response i.e. spectrum of impulse response
   fNKernel = nkernel;
   fFreqRes.assign(h, h + nkernel);
   fFreqRes.resize(2 * ((extendedN + 1) / 2 + 1), 0);

   fFFTR2C->SetPoints(&fFreqRes[0]);
   fFFTR2C->Transform();
   fFFTR2C->GetPoints(&fFreqRes[0]);
}
