// $Id: MEGDRSWaveform.cpp 21343 2014-05-28 18:35:53Z uchiyama $
// Author: Yusuke Uchiyama

//__________________________________________________________________________________
//
// MEGDRSWaveform
//
// Basic class to treat DRS waveforms for the MEG experiment.
// This class object always has allocated memory to house time information.
//

#include <algorithm>
#include <functional>
#include <RConfig.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( push )
#   pragma warning( disable : 4800)
#endif // R__VISUAL_CPLUSPLUS
#include <TROOT.h>
#include <TFrame.h>
#include <TVirtualPad.h>
#include <TF1.h>
#include <TH1.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TVirtualFitter.h>
#include <TMinuit.h>
#if defined( R__VISUAL_CPLUSPLUS )
#   pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS
#include <Riostream.h>

#include "units/MEGSystemOfUnits.h"
#include "waveform/MEGDRSWaveform.h"

using namespace std;

namespace MEGWAVEFORM
{
MEGWaveformTemplateFit *gTemplateFit;

// FCN for MINUIT fitting
//__________________________________________________________________________________
void FCN1(Int_t &npar, Double_t * /*gin*/, Double_t &f, Double_t *par, Int_t /*iflag*/)
{
   // FCN for variable bin size waveform
   // Fit with linear interporation of template waveform

   // calculate chisquare
   Double_t chisq = 0;
   Int_t    startBinData = gTemplateFit->fWaveform->FindPoint(par[2] + gTemplateFit->fTL) + 1; // >= 0
   Int_t    endBinData   = gTemplateFit->fWaveform->FindPoint(par[2] + gTemplateFit->fTU);  // <= fNPoints-1
   Int_t    iBinData;
   Int_t    iBinTemplate;
   Double_t amplTemplate;
   Double_t ratio;
   Double_t error, weight2;
   Double_t binsizeinv = 1. / gTemplateFit->fBinSize;
   Double_t *wfAmplitude = gTemplateFit->fWaveform->GetAmplitude();
   Double_t *wfError     = gTemplateFit->fWaveform->GetError();
   Double_t *wfTime      = gTemplateFit->fWaveform->GetTime();
   gTemplateFit->fNDF = 0;

   for (iBinData = startBinData; iBinData < endBinData; iBinData++) {
      error = wfError[iBinData];
      if (error <= 0) {
         continue;
      }
      weight2 = 1. / (error * error);

      iBinTemplate = static_cast<Int_t>(((wfTime[iBinData] - par[2])
                                         - gTemplateFit->fLowerEdge) * binsizeinv);
      ratio = ((wfTime[iBinData] - par[2])
               - (iBinTemplate * gTemplateFit->fBinSize + gTemplateFit->fLowerEdge)) * binsizeinv;
      amplTemplate = (1 - ratio) * gTemplateFit->fAmplitude[iBinTemplate] + ratio *
                     gTemplateFit->fAmplitude[iBinTemplate + 1];

      chisq += weight2 *
               (wfAmplitude[iBinData] - par[1] * amplTemplate - par[0]) *
               (wfAmplitude[iBinData] - par[1] * amplTemplate - par[0]);
      gTemplateFit->fNDF++;
   }
   gTemplateFit->fNDF -= npar;

   f = chisq;
}

//__________________________________________________________________________________
void FCN2(Int_t &npar, Double_t * /*gin*/, Double_t &f, Double_t *par, Int_t /*iflag*/)
{
   // FCN for variable bin size waveform
   // Fit with 2nd order interpolation of template waveform

   // calculate chisquare
   Double_t chisq = 0;
   Int_t    startBinData = gTemplateFit->fWaveform->FindPoint(par[2] + gTemplateFit->fTL) + 1; // >= 0
   Int_t    endBinData   = gTemplateFit->fWaveform->FindPoint(par[2] + gTemplateFit->fTU);  // <= fNPoints-1
   Int_t    iBinData;
   Double_t timeData;
   Int_t    iBinTemplate;
   Double_t amplTemplate;
   Double_t ratio;
   Double_t error, weight2;
   Double_t binsizeinv = 1. / gTemplateFit->fBinSize;
   Double_t *wfAmplitude = gTemplateFit->fWaveform->GetAmplitude();
   Double_t *wfError     = gTemplateFit->fWaveform->GetError();
   Double_t *wfTime      = gTemplateFit->fWaveform->GetTime();
   gTemplateFit->fNDF = 0;

   for (iBinData = startBinData; iBinData < endBinData; iBinData++) {
      error = wfError[iBinData];
      if (error <= 0) {
         continue;
      }
      weight2 = 1. / (error * error);

      timeData = (wfTime[iBinData] - par[2]) - gTemplateFit->fLowerEdge;
      iBinTemplate = static_cast<Int_t>(timeData * binsizeinv);

      ratio = (timeData - iBinTemplate * gTemplateFit->fBinSize) * binsizeinv;
      if (ratio > 0.5) {
         iBinTemplate++;
         ratio -= 1;
      }

      amplTemplate = 0.5 * ratio * ((ratio - 1) * gTemplateFit->fAmplitude[iBinTemplate - 1]
                                    + (ratio + 1) * gTemplateFit->fAmplitude[iBinTemplate + 1])
                     - (ratio - 1) * (ratio + 1) * gTemplateFit->fAmplitude[iBinTemplate];

      chisq += weight2 *
               (wfAmplitude[iBinData] - par[1] * amplTemplate - par[0]) *
               (wfAmplitude[iBinData] - par[1] * amplTemplate - par[0]);
      gTemplateFit->fNDF++;
   }
   gTemplateFit->fNDF -= npar;

   f = chisq;
}

//__________________________________________________________________________________
void FCN3(Int_t &npar, Double_t * /*gin*/, Double_t &f, Double_t *par, Int_t /*iflag*/)
{
   // FCN for variable bin size waveform
   // Fit with linear interporation of template waveform
   // Not use the leading edge (around t0)

   // calculate chisquare
   Double_t chisq = 0;
   Int_t    startBinData = gTemplateFit->fWaveform->FindPoint(par[2] + gTemplateFit->fTL) + 1; // >= 0
   Int_t    endBinData   = gTemplateFit->fWaveform->FindPoint(par[2] + gTemplateFit->fTU);  // <= fNPoints-1
   Int_t    iBinData;
   Int_t    iBinTemplate;
   Double_t amplTemplate;
   Double_t ratio;
   Double_t error, weight2;
   Double_t binsizeinv = 1. / gTemplateFit->fBinSize;
   Double_t *wfAmplitude = gTemplateFit->fWaveform->GetAmplitude();
   Double_t *wfError     = gTemplateFit->fWaveform->GetError();
   Double_t *wfTime      = gTemplateFit->fWaveform->GetTime();
   gTemplateFit->fNDF = 0;

   for (iBinData = startBinData; iBinData < endBinData; iBinData++) {
      error = wfError[iBinData];
      if (error <= 0) {
         continue;
      }
      weight2 = 1. / (error * error);

      if (-5e-9 < wfTime[iBinData] - par[2] && wfTime[iBinData] - par[2] < 3e-9) {
         continue;
      }

      iBinTemplate = static_cast<Int_t>(((wfTime[iBinData] - par[2])
                                         - gTemplateFit->fLowerEdge) * binsizeinv);
      ratio = ((wfTime[iBinData] - par[2])
               - (iBinTemplate * gTemplateFit->fBinSize + gTemplateFit->fLowerEdge)) * binsizeinv;
      amplTemplate = (1 - ratio) * gTemplateFit->fAmplitude[iBinTemplate] + ratio *
                     gTemplateFit->fAmplitude[iBinTemplate + 1];

      chisq += weight2 *
               (wfAmplitude[iBinData] - par[1] * amplTemplate - par[0]) *
               (wfAmplitude[iBinData] - par[1] * amplTemplate - par[0]);
      gTemplateFit->fNDF++;
   }
   gTemplateFit->fNDF -= npar;

   f = chisq;
}
}

using namespace MEGWAVEFORM;

extern void H1LeastSquareSeqnd(Int_t n, Double_t *a, Int_t idim, Int_t &ifail, Int_t k, Double_t *b);

ClassImp(MEGDRSWaveform)

//__________________________________________________________________________________
MEGDRSWaveform::MEGDRSWaveform()
   : MEGWaveform()
   , fNErrorPoints(0)
   , fError(0)
   , fFCN(0)
   , fDRSChipData(0)
   , fDRSChannelData(0)
{
   // Waveform default constructor.

   fFixBinSize = false;
}

//__________________________________________________________________________________
MEGDRSWaveform::MEGDRSWaveform(Int_t npoints, Double_t binsize, Double_t *ptime, const char *title)
   : MEGWaveform(npoints, binsize, ptime, title)
   , fNErrorPoints(0)
   , fError(0)
   , fFCN(0)
   , fDRSChipData(0)
   , fDRSChannelData(0)
{
   // Waveform normal constructor for variable bin size.

   fFixBinSize = false;

   // allocate
   fTime = new Double_t[npoints];
   if (ptime) {
      memcpy(fTime, ptime, sizeof(Double_t)*npoints);
   } else if (binsize != 0) {
      Int_t iPnt;
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         fTime[iPnt] = iPnt * fBinSize + fTimeMin;
      }
   } else {
      memset(fTime, 0, sizeof(Double_t)*npoints);
   }
}

//__________________________________________________________________________________
MEGDRSWaveform::MEGDRSWaveform(Int_t npoints, Double_t binsize, Double_t tmin, const char *title)
   : MEGWaveform(npoints, binsize, tmin, title)
   , fNErrorPoints(0)
   , fError(0)
   , fFCN(0)
   , fDRSChipData(0)
   , fDRSChannelData(0)
{
   // Waveform normal constructor for fix bin size.

   // allocate
   fTime = new Double_t[npoints];
   Int_t iPnt;
   for (iPnt = 0; iPnt < fNPoints; iPnt++) {
      fTime[iPnt] = iPnt * fBinSize + tmin;
   }
}

//__________________________________________________________________________________
MEGDRSWaveform::MEGDRSWaveform(const MEGDRSWaveform &wf)
   : MEGWaveform(wf)
   , fNErrorPoints(wf.fNErrorPoints)
   , fError(wf.fError)
   , fFCN(wf.fFCN)
   , fDRSChipData(0)
   , fDRSChannelData(0)
{
   // Copy constructor for this waveform

   // allocate
   fTime = new Double_t[fMaxNPoints];
   if (wf.fTime) {
      memcpy(fTime, wf.fTime, sizeof(Double_t)*fMaxNPoints);
   } else {
      memset(fTime, 0, sizeof(Double_t)*fMaxNPoints);
   }
}

//__________________________________________________________________________________
MEGDRSWaveform::MEGDRSWaveform(const TH1 *h, Double_t *ptime)
   : MEGWaveform(h, ptime)
   , fNErrorPoints(0)
   , fError(0)
   , fFCN(0)
   , fDRSChipData(0)
   , fDRSChannelData(0)
{
   // Constructor importing from histogram
   // if ptime=0 (default), waveform is made with fix bin size

   fTime = new Double_t[fMaxNPoints];
   if (ptime) {
      memcpy(fTime, ptime, sizeof(Double_t)*fMaxNPoints);
   } else {
      memset(fTime, 0, sizeof(Double_t)*fMaxNPoints);
   }
}

//__________________________________________________________________________________
MEGDRSWaveform::~MEGDRSWaveform()
{
   // Waveform default destructor

   delete [] fTime;
   fDRSChipData    = 0;
   fDRSChannelData = 0;
}

//__________________________________________________________________________________
void MEGDRSWaveform::SetNPoints(Int_t npoints)
{
   // Set the number of points

   if (!fAmplitude) {
      fAmplitude = new Double_t[npoints];
      fTime      = new Double_t[npoints];
      memset(fAmplitude, 0, sizeof(Double_t)*npoints);
      memset(fTime, 0, sizeof(Double_t)*npoints);
      fMaxNPoints = npoints;
   } else if (fNPoints == npoints) {
      return;
   } else if (fMaxNPoints < npoints) {
      Double_t *temp = new Double_t[fNPoints];
      memcpy(temp, fAmplitude, sizeof(Double_t)*fNPoints);
      delete [] fAmplitude;
      fAmplitude = new Double_t[npoints];
      memset(fAmplitude, 0, sizeof(Double_t)*npoints);
      memcpy(fAmplitude, temp, sizeof(Double_t)*fNPoints);

      memcpy(temp, fTime, sizeof(Double_t)*fNPoints);
      delete [] fTime;
      fTime = new Double_t[npoints];
      memset(fTime, 0, sizeof(Double_t)*npoints);
      memcpy(fTime, temp, sizeof(Double_t)*fNPoints);
      delete [] temp;
      fMaxNPoints = npoints;
//    } else {
//       Int_t startpnt = (fNPoints <  npoints) ? fNPoints : npoints;
//       memset(fAmplitude + startpnt, 0, sizeof(Double_t)*(fMaxNPoints - fNPoints));
//       memset(fTime + startpnt, 0, sizeof(Double_t)*(fMaxNPoints - fNPoints));
   } else if (fNPoints < npoints) {
      memset(fAmplitude + fNPoints, 0, sizeof(Double_t) * (npoints - fNPoints));
   }
   fNPoints = npoints;
   fTimeMax = fBinSize * (npoints - 1) + fTimeMin;
   if (fError) {
      fNErrorPoints = npoints;
   }
}

//__________________________________________________________________________________
void MEGDRSWaveform::SetVariableBinSize(Bool_t flag, Double_t* ptime)
{
   // Set the bin type of this waveform

   if (((flag == false) && fFixBinSize) || ((flag != false) && !fFixBinSize)) {
      return;
   }

   if (!flag) {
      fTimeMin = GetTimeAt(0);
      fTimeMax = fBinSize * (fNPoints - 1) + fTimeMin;
      fFixBinSize = true;
   } else {
      if (ptime) {
         memcpy(fTime, ptime, sizeof(Double_t)*fNPoints);
      }
      fFixBinSize = false;
   }
}

//__________________________________________________________________________________
void MEGDRSWaveform::SetFixBinSize(Bool_t flag, Double_t* ptime)
{
   // Set the bin type of this waveform

   if (flag == fFixBinSize) {
      return;
   }
   Int_t iPnt;
   if (flag) {
      fTimeMin = GetTimeAt(0);
      fTimeMax = fBinSize * (fNPoints - 1) + fTimeMin;
      fFixBinSize = true;
   } else {
      if (ptime) {
         memcpy(fTime, ptime, sizeof(Double_t)*fNPoints);
      } else {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            fTime[iPnt] = iPnt * fBinSize + fTimeMin;
         }
      }
      fFixBinSize = false;
   }
}

//__________________________________________________________________________________
void MEGDRSWaveform::SetTime(Double_t* ptime)
{
   // Set time information of each point (variable bin size mode)

   if (fTime != ptime) {
      memcpy(fTime, ptime, sizeof(Double_t)*fNPoints);
   }
   fFixBinSize = false;
}

//__________________________________________________________________________________
void MEGDRSWaveform::SetTime(const MEGWaveform *wfin)
{
   // Set time information from input waveform object

   SetNPoints(wfin->GetNPoints());
   fBinSize    = wfin->GetBinSize();
   fFixBinSize = wfin->IsFixBinSize();
   fTimeMin    = wfin->GetTimeMin();
   fTimeMax    = wfin->GetTimeMax();
   if (!fFixBinSize && fTime != wfin->GetTime()) {
      memcpy(fTime, wfin->GetTime(), sizeof(Double_t)*fNPoints);
   }
}

//__________________________________________________________________________________
void MEGDRSWaveform::Reset()
{
   // Reset this waveform

   MEGWaveform::Reset();
   if (fError) {
      memset(fError, 0, fNPoints * sizeof(Double_t));
   }
}

//__________________________________________________________________________________
void MEGDRSWaveform::DrawByCell(Option_t *option)
{
   MEGDRSWaveform *wf = new MEGDRSWaveform(*this);
   Double_t ptime[fNPoints], amplitude[fNPoints];
   Int_t iCell = 0;
   for (int i = 0; i < fNPoints; i++) {
      iCell = ((i + fStartCell) < kDRSBins) ? i + fStartCell : i + fStartCell - kDRSBins;
      if (iCell < 0 || iCell > kDRSBins) {
         return;
      }
      amplitude[iCell] = wf->GetAmplitudeAt(i);
      ptime[iCell] = iCell;
   }
   wf->SetTime(ptime);
   wf->SetAmplitude(amplitude);
   wf->SetDisplayTimeUnit(1);
   wf->GetXaxis()->SetTitle("cell");
   wf->Draw(option);
   delete wf;
   return;
}

//__________________________________________________________________________________
Int_t MEGDRSWaveform::FindStartCell(Double_t threshold)
{
   // Find start cell which is the next cell on which domino wave stopped.

   // temporary simple version

   Int_t iCell;
   for (iCell = 0; iCell < fNPoints; iCell++) {
      if (fAmplitude[iCell] > threshold) {
         fStartCell = (iCell + 1) % fNPoints;
         return fStartCell;
      }
   }
   Warning("FindStartCell", "Failed to find startcell.");
   fStartCell = 0;
   return -1;
}

//__________________________________________________________________________________
Double_t MEGDRSWaveform::GetCellTimeAt(Int_t cell)
{
   Int_t pnt = this->GetNPoints() - this->GetStartCell() + cell;
   pnt = pnt % (this->GetNPoints());
   return this->GetTimeAt(pnt);
}

//__________________________________________________________________________________
void MEGDRSWaveform::Rearrange()
{
   // Rearrange this waveform from the order of DRS cell to the order of time.

   Int_t iCell;
   Double_t *ampltemp = new Double_t[fNPoints];// working space
   Double_t *timetemp = new Double_t[fNPoints];// working space
   Double_t starttime = fTime[fStartCell];
   Double_t lasttime  = fTime[fNPoints - 1];
   Double_t lasttofirstint = (fTime[1] + fTime[fNPoints - 1] - fTime[fNPoints - 2]) / 2.;
   for (iCell = 0; iCell < fNPoints; iCell++) {
      if (fStartCell + iCell < fNPoints) {
         timetemp[iCell] = fTime[fStartCell + iCell] - starttime;
      } else {
         timetemp[iCell] = fTime[(fStartCell + iCell) % fNPoints] - starttime
                           + lasttime + lasttofirstint;
      }
      ampltemp[iCell] = fAmplitude[(fStartCell + iCell) % fNPoints];
   }
   memcpy(fTime, timetemp, sizeof(Double_t)*fNPoints);
   memcpy(fAmplitude, ampltemp, sizeof(Double_t)*fNPoints);
   delete [] ampltemp;
   delete [] timetemp;
}

//__________________________________________________________________________________
Int_t MEGDRSWaveform::PrepareTemplate(MEGWaveformTemplateFit *templatefit[], const char *filename,
                                      const char *templatename, Bool_t useEachTemplate, Int_t nchannel, Int_t maxpeak, Double_t ratiothre,
                                      Double_t localwidth)
{
   // Prepare template waveforms for minuit fit

   // Open template file
   TFile *templateFile = 0;
   if ((templateFile = TFile::Open(filename)) == 0) {
      Report(R_ERROR, "PrepareTemplate : Failed to open waveform template file (%s).", filename);
      return -1;
   }

   // Fill template pulse array & calculate some property of the template
   Int_t    iPnt, iCh;
   TH1F    *templateHist = 0;
   Double_t *peak = 0;
   Int_t *peakpoint = 0;
   Int_t polarity;
   Double_t threshold;
   MEGWaveform *wf = 0;
   if (maxpeak) {
      peak = new Double_t[maxpeak];
      peakpoint = new Int_t[maxpeak];
   } else {
      peak = new Double_t[1];
      peakpoint = new Int_t[1];
   }

   if (!useEachTemplate) {  // use one template over all channels
//       memset(templatefit[0]->fAmplitude, 0, sizeof(Double_t)*templatefit[0]->fNPoints);
      templateHist = (TH1F*)templateFile->Get(templatename);
      if (!templateHist) {
         Report(R_ERROR, "PrepareTemplate : Failed to get template histogram (%s).", templatename);
         delete [] peak;
         delete [] peakpoint;
         templatefit[0]->fTemplateWaveform = 0;
         return -1;
      }
      templatefit[0]->fTemplateWaveform = new MEGWaveform(templateHist->GetNbinsX()
                                                          , templateHist->GetBinWidth(1));
      wf = templatefit[0]->fTemplateWaveform;//->Import(templateHist, true);
      templatefit[0]->fAmplitude = wf->GetAmplitude();
      for (iPnt = 0; iPnt < templateHist->GetNbinsX(); iPnt++) {
         templatefit[0]->fAmplitude[iPnt] = templateHist->GetBinContent(iPnt + 1);
      }
      templatefit[0]->fNPoints = templateHist->GetNbinsX();
      templatefit[0]->fBinSize = templateHist->GetBinWidth(1);
      templatefit[0]->fLowerEdge = -1 * (templateHist->GetXaxis()->FindBin(0.) - 1) * templatefit[0]->fBinSize;
      templatefit[0]->fArea = templateHist->Integral("width");//[Vsec]
      templatefit[0]->fPeakToPeak = wf->PeakToPeak();
      wf->SetTimeMin(templatefit[0]->fLowerEdge);
      polarity = (templatefit[0]->fArea > 0) ? 1 : -1;
      if (polarity > 0) {
         wf->MaximumPeak(peak[0], peakpoint[0]);
      } else {
         wf->MinimumPeak(peak[0], peakpoint[0]);
      }
      templatefit[0]->fPeakTime = wf->GetTimeAt(peakpoint[0]);
      if (maxpeak) {
         threshold = templatefit[0]->fPeakToPeak * ratiothre * polarity;
         templatefit[0]->fNPeak = wf->PeakSearch(peak, peakpoint, maxpeak
                                                 , wf->GetTimeMin(), wf->GetTimeMax()
                                                 , threshold, localwidth, polarity);
         templatefit[0]->fNPeak = TMath::Min(templatefit[0]->fNPeak, maxpeak);
         for (Int_t iPeak = 1; iPeak < templatefit[0]->fNPeak; iPeak++) {
            templatefit[0]->fTimeDif[iPeak - 1] = wf->GetTimeAt(peakpoint[iPeak]) - wf->GetTimeAt(peakpoint[0]);
            templatefit[0]->fRatio[iPeak - 1] = peak[iPeak] / peak[0];
         }
      }

   } else {                  // use each template for each channel
      TString histname;
      for (iCh = 0; iCh < nchannel; iCh++) {
//          memset(templatefit[iCh]->fAmplitude, 0, sizeof(Double_t)*templatefit[iCh]->fNPoints);
         histname.Form("%s%04d", templatename, iCh);
         templateHist = (TH1F*)templateFile->Get(histname);

         if (templateHist) {
            templatefit[iCh]->fTemplateWaveform = new MEGWaveform(templateHist->GetNbinsX()
                                                                  , templateHist->GetBinWidth(1));
            wf = templatefit[iCh]->fTemplateWaveform;
            templatefit[iCh]->fAmplitude = wf->GetAmplitude();
            for (iPnt = 0; iPnt < templateHist->GetNbinsX(); iPnt++) {
               templatefit[iCh]->fAmplitude[iPnt] = templateHist->GetBinContent(iPnt + 1);
            }
            templatefit[iCh]->fNPoints = templateHist->GetNbinsX();
            templatefit[iCh]->fBinSize = templateHist->GetBinWidth(1);
            templatefit[iCh]->fLowerEdge = -1 * (templateHist->GetXaxis()->FindBin(0.) - 1) * templatefit[iCh]->fBinSize;
            templatefit[iCh]->fArea = templateHist->Integral("width");//[Vsec]
         }

         // if histogram is empty, use template over all channels
         if (!templateHist || templatefit[iCh]->fArea == 0) {
            templateHist = (TH1F*)templateFile->Get(templatename);
            if (!templateHist) {
               Report(R_ERROR, "PrepareTemplate : Failed to get template histogram (%s).", templatename);
               delete [] peak;
               delete [] peakpoint;
               templatefit[iCh]->fTemplateWaveform = 0;
               return -1;
            }
            templatefit[iCh]->fTemplateWaveform = new MEGWaveform(templateHist->GetNbinsX()
                                                                  , templateHist->GetBinWidth(1));
            templatefit[iCh]->fAmplitude = templatefit[iCh]->fTemplateWaveform->GetAmplitude();
            for (iPnt = 0; iPnt < templateHist->GetNbinsX(); iPnt++) {
               templatefit[iCh]->fAmplitude[iPnt] = templateHist->GetBinContent(iPnt + 1);
            }
            templatefit[iCh]->fNPoints = templateHist->GetNbinsX();
            templatefit[iCh]->fBinSize = templateHist->GetBinWidth(1);
            templatefit[iCh]->fLowerEdge = -1 * (templateHist->GetXaxis()->FindBin(0.) - 1) * templatefit[0]->fBinSize;
            templatefit[iCh]->fArea = templateHist->Integral("width");//[Vsec]
         }
         wf = templatefit[iCh]->fTemplateWaveform;//->Import(templateHist, true);
         templatefit[iCh]->fPeakToPeak = wf->PeakToPeak();
         wf->SetTimeMin(templatefit[iCh]->fLowerEdge);
         polarity = (templatefit[iCh]->fArea > 0) ? 1 : -1;
         if (polarity > 0) {
            wf->MaximumPeak(peak[0], peakpoint[0]);
         } else {
            wf->MinimumPeak(peak[0], peakpoint[0]);
         }
         templatefit[iCh]->fPeakTime = wf->GetTimeAt(peakpoint[0]);

         if (maxpeak) {
            threshold = templatefit[iCh]->fPeakToPeak * ratiothre * polarity;
            templatefit[iCh]->fNPeak = wf->PeakSearch(peak, peakpoint, maxpeak
                                                      , 0, wf->GetTimeMax()
                                                      , threshold, localwidth, polarity);
            templatefit[iCh]->fNPeak = TMath::Min(templatefit[iCh]->fNPeak, maxpeak);
            templatefit[iCh]->fPeakTime = wf->GetTimeAt(peakpoint[0]);
            for (Int_t iPeak = 1; iPeak < templatefit[iCh]->fNPeak; iPeak++) {
               templatefit[iCh]->fTimeDif[iPeak - 1] = wf->GetTimeAt(peakpoint[iPeak]) - wf->GetTimeAt(peakpoint[0]);
               templatefit[iCh]->fRatio[iPeak - 1] = peak[iPeak] / peak[0];
            }
         }
      }
   }
   templateFile->Close();
   delete templateFile;
   delete [] peak;
   delete [] peakpoint;
   return 1;
}

//__________________________________________________________________________________
void MEGDRSWaveform::CalculateError(Double_t noiselevel, Double_t baseline, Double_t factor,
                                    Double_t lowerthre, Double_t upperthre)
{
   // Calculate error
   // factor = 1/VoltToPe/filterN

   if (!fError) {
      Report(R_ERROR, "memory for error is not specified.");
      return;
   }

   Int_t iPnt;
   Double_t sigmaStat;// statistical error
   for (iPnt = 0; iPnt < fNPoints; iPnt++) {
      if (fAmplitude[iPnt] < lowerthre || upperthre < fAmplitude[iPnt]) {
         fError[iPnt] = -1;// over flow
      } else {
         sigmaStat = (TMath::Abs(fAmplitude[iPnt] - baseline) < noiselevel) ?
                     0 : TMath::Sqrt(TMath::Abs(fAmplitude[iPnt] - baseline) * factor);
         fError[iPnt] = TMath::Sqrt(sigmaStat * sigmaStat + noiselevel * noiselevel);
      }
   }
}

//__________________________________________________________________________________
Int_t MEGDRSWaveform::MinuitTemplateFit(MEGWaveformTemplateFit *templatefit, const Double_t par[][5],
                                        Double_t result[], MEGDRSWaveform *wfout, Option_t* opt)
{
   // Fitting waveform with template using MINUIT
   // Parameters :
   //   templatefit     global values used in FCN
   //     {
   //       MEGDRSWaveform *fWaveform;    //Fitted waveform
   //       Int_t           fNDF;         //Number of degrees of freedom
   //       Double_t        fTL;          //Lower edge of fitting region [sec]
   //       Double_t        fTU;          //Upper edge of fitting region [sec]
   //       Double_t       *fAmplitude;   //Pointer to an array of template waveform amplitude
   //       Double_t        fBinSize;     //Bin width of template waveform
   //       Double_t        fLowerEdge;   //Lower edge of averaged waveform histogram [sec]
   //       Double_t        fArea;        //Area of temlate waveform [V*sec]
   //     }
   //
   //   par[][5]        input arrays of fit parameters
   //                   0:initial value, 1:step size,
   //                   2:lower limit(0 means unlimit), 3:upper limit(0 means unlimit)
   //                   4:fix parameter
   //     {
   //       par[0][]      // for baseline
   //       par[1][]      // for scale
   //       par[2][]      // for t0
   //     }
   //
   //   result[]        output array of fitting results
   //                   0:baseline, 1:scale, 2:t0, 3:chisquare

   TString option = opt;
   option.ToUpper();

   gTemplateFit = templatefit;

   // for Fitting result
   Double_t    base, ebase;
   Double_t    scale, escale;
   Double_t    t0, et0;
   Double_t    fcn;

   // for MINUIT
   static TMinuit *minuit = 0;
   Double_t arglist[10];
   Int_t    ierflg = 0;
   Double_t edm, errdef;
   Int_t    nvpar, nparx, icstat;

   // Set up fitter "MINUIT"
   TVirtualFitter::SetDefaultFitter("Minuit");
   if (!minuit) {
      minuit = new TMinuit(3); // initialize TMinuit with 3 parameters

      minuit->SetPrintLevel(-1);
      minuit->mnexcm("SET NOW", arglist, 0, ierflg);
      arglist[0] = 1;
      minuit->mnexcm("SET ERR", arglist, 1, ierflg); // UP = 1 : normal chisquare fit

      arglist[0] = 1;
      minuit->mnexcm("SET STR", arglist, 1, ierflg); // normal
   }

   // Set FCN
   if (option.Contains("FCN2")) {
      fFCN = FCN2;
   } else if (option.Contains("FCN3")) {
      fFCN = FCN3;
   } else {
      fFCN = FCN1;
   }
   minuit->SetFCN(fFCN); // set chisquare function

   // Release parameters
   for (Int_t iPar = 0; iPar < 3; iPar++) {
      minuit->Release(iPar);
   }

   // Set initial values and step size for parameters
//    minuit->mnparm(0, "baseline", par[0][0], 1e-7, par[0][1], par[0][2], ierflg);
//    minuit->mnparm(1, "scale", par[1][0], 1.e-4, par[1][1], par[1][2], ierflg);
//    minuit->mnparm(2, "t0", par[2][0], 1.e-17, par[2][1], par[2][2], ierflg);
   minuit->mnparm(0, "baseline", par[0][0], par[0][1], par[0][2], par[0][3], ierflg);
   minuit->mnparm(1, "scale", par[1][0], par[1][1], par[1][2], par[1][3], ierflg);
   minuit->mnparm(2, "t0", par[2][0], par[2][1], par[2][2], par[2][3], ierflg);

   // Fix parameters if specified
   for (Int_t iPar = 0; iPar < 3; iPar++) {
      if (par[iPar][4]) {
         minuit->FixParameter(iPar);
      }
   }

   arglist[0] = 500;
   arglist[1] = 0.1;
   minuit->mnexcm("MIGRAD", arglist, 2, ierflg); // execute MIGRAD minimization

   // Print results
   minuit->mnstat(fcn, edm, errdef, nvpar, nparx, icstat);
   //minuit->mnprin(3, fcn);

   // Get Parameters
   minuit->GetParameter(0, base, ebase);
   minuit->GetParameter(1, scale, escale);
   minuit->GetParameter(2, t0, et0);
//   delete minuit;
   result[0] = base;
   result[1] = scale; //scale*gTemplateFit->fArea / kDRSInputImpedance;
   result[2] = t0;
   result[3] = fcn;

   if (wfout) {
      Int_t startbin = static_cast<Int_t>((gTemplateFit->fTL - gTemplateFit->fLowerEdge) / gTemplateFit->fBinSize);
      Int_t endbin = static_cast<Int_t>((gTemplateFit->fTU - gTemplateFit->fLowerEdge) / gTemplateFit->fBinSize);
      Int_t iPnt;
      Int_t nbin = endbin - startbin;
      wfout->SetNPoints(nbin);
      wfout->SetBinSize(gTemplateFit->fBinSize);
      for (iPnt = 0; iPnt < nbin; iPnt++) {
         wfout->SetAmplitudeAt(iPnt, scale * gTemplateFit->fAmplitude[startbin + iPnt] + base);
         wfout->SetTimeAt(iPnt, (iPnt + startbin)*gTemplateFit->fBinSize + gTemplateFit->fLowerEdge + t0);
      }
   }

   return ierflg;// Error return code: 0 if no error, >0 if request failed.
}

//______________________________________________________________________________
Int_t MEGDRSWaveform::Fit(const char *fname, Option_t *option, Option_t *, Axis_t xmin, Axis_t xmax)
{
   // Fit this graph with function with name fname.
   //
   //  interface to TGraph::Fit(TF1 *f1...
   //
   //      fname is the name of an already predefined function created by TF1 or TF2
   //      Predefined functions such as gaus, expo and poln are automatically
   //      created by ROOT.
   //      fname can also be a formula, accepted by the linear fitter (linear parts divided
   //      by "++" sign), for example "x++sin(x)" for fitting "[0]*x+[1]*sin(x)"

   char *linear;
   linear = (char*) strstr(fname, "++");
   TF1 *f1 = 0;
   if (linear) {
      f1 = new TF1(fname, fname, xmin, xmax);
   } else {
      f1 = (TF1*)gROOT->GetFunction(fname);
      if (!f1) {
         Printf("Unknown function: %s", fname);
         return -1;
      }
   }
   return Fit(f1, option, "", xmin, xmax);
}

//__________________________________________________________________________________
Int_t MEGDRSWaveform::Fit(TF1 *f1, Option_t *option, Option_t *, Axis_t rxmin, Axis_t rxmax)
{
   // Fit this graph with function f1.
   //
   //   f1 is an already predefined function created by TF1.
   //   Predefined functions such as gaus, expo and poln are automatically
   //   created by ROOT.
   //
   //   The list of fit options is given in parameter option.
   //      option = "W" Set all weights to 1; ignore error bars
   //             = "U" Use a User specified fitting algorithm (via SetFCN)
   //             = "Q" Quiet mode (minimum printing)
   //             = "V" Verbose mode (default is between Q and V)
   //             = "B" Use this option when you want to fix one or more parameters
   //                   and the fitting function is like "gaus","expo","poln","landau".
   //             = "R" Use the Range specified in the function range
   //             = "N" Do not store the graphics function, do not draw
   //             = "0" Do not plot the result of the fit. By default the fitted function
   //                   is drawn unless the option"N" above is specified.
   //             = "+" Add this new fitted function to the list of fitted functions
   //                   (by default, any previous function is deleted)
   //             = "C" In case of linear fitting, not calculate the chisquare
   //                    (saves time)
   //             = "F" If fitting a polN, switch to minuit fitter
   //             = "ROB" In case of linear fitting, compute the LTS regression
   //                     coefficients (robust(resistant) regression), using
   //                     the default fraction of good points
   //               "ROB=0.x" - compute the LTS regression coefficients, using
   //                           0.x as a fraction of good points
   //
   //   When the fit is drawn (by default), the parameter goption may be used
   //   to specify a list of graphics options. See TGraph::Paint for a complete
   //   list of these options.
   //
   //   In order to use the Range option, one must first create a function
   //   with the expression to be fitted. For example, if your graph
   //   has a defined range between -4 and 4 and you want to fit a gaussian
   //   only in the interval 1 to 3, you can do:
   //        TF1 *f1 = new TF1("f1","gaus",1,3);
   //        graph->Fit("f1","R");
   //
   //
   //   who is calling this function
   //   ============================
   //   Note that this function is called when calling TGraphErrors::Fit
   //   or TGraphAsymmErrors::Fit ot TGraphBentErrors::Fit
   //   see the discussion below on the errors calulation.
   //
   //   Linear fitting
   //   ============================
   //   When the fitting function is linear (contains the "++" sign) or the fitting
   //   function is a polynomial, a linear fitter is initialised.
   //   To create a linear function, use the following syntaxis: linear parts
   //   separated by "++" sign.
   //   Example: to fit the parameters of "[0]*x + [1]*sin(x)", create a
   //    TF1 *f1=new TF1("f1", "x++sin(x)", xmin, xmax);
   //   For such a TF1 you don't have to set the initial conditions
   //   Going via the linear fitter for functions, linear in parameters, gives a considerable
   //   advantage in speed.
   //
   //   Setting initial conditions
   //   ==========================
   //   Parameters must be initialized before invoking the Fit function.
   //   The setting of the parameter initial values is automatic for the
   //   predefined functions : poln, expo, gaus, landau. One can however disable
   //   this automatic computation by specifying the option "B".
   //   You can specify boundary limits for some or all parameters via
   //        f1->SetParLimits(p_number, parmin, parmax);
   //   if parmin>=parmax, the parameter is fixed
   //   Note that you are not forced to fix the limits for all parameters.
   //   For example, if you fit a function with 6 parameters, you can do:
   //     func->SetParameters(0,3.1,1.e-6,0.1,-8,100);
   //     func->SetParLimits(4,-10,-4);
   //     func->SetParLimits(5, 1,1);
   //   With this setup, parameters 0->3 can vary freely
   //   Parameter 4 has boundaries [-10,-4] with initial value -8
   //   Parameter 5 is fixed to 100.
   //
   //  Fit range
   //  =========
   //  The fit range can be specified in two ways:
   //    - specify rxmax > rxmin (default is rxmin=rxmax=0)
   //    - specify the option "R". In this case, the function will be taken
   //      instead of the full graph range.
   //
   //   Changing the fitting function
   //   =============================
   //  By default the fitting function GraphFitChisquare is used.
   //  To specify a User defined fitting function, specify option "U" and
   //  call the following functions:
   //    TVirtualFitter::Fitter(mygraph)->SetFCN(MyFittingFunction)
   //  where MyFittingFunction is of type:
   //  extern void MyFittingFunction(Int_t &npar, Double_t *gin, Double_t &f, Double_t *u, Int_t flag);
   //
   //  How errors are used in the chisquare function (see TFitter GraphFitChisquare)//   Access to the fit results
   //   ============================================
   // In case of a TGraphErrors object, ex, the error along x,  is projected
   // along the y-direction by calculating the function at the points x-exlow and
   // x+exhigh.
   //
   // The chisquare is computed as the sum of the quantity below at each point:
   //
   //                     (y - f(x))**2
   //         -----------------------------------
   //         ey**2 + (0.5*(exl + exh)*f'(x))**2
   //
   // where x and y are the point coordinates, and f'(x) is the derivative of function f(x).
   //
   // In case the function lies below (above) the data point, ey is ey_low (ey_high).
   //
   //  thanks to Andy Haas (haas@yahoo.com) for adding the case with TGraphasymmerrors
   //            University of Washington
   //
   // The approach used to approximate the uncertainty in y because of the
   // errors in x, is to make it equal the error in x times the slope of the line.
   // The improvement, compared to the first method (f(x+ exhigh) - f(x-exlow))/2
   // is of (error of x)**2 order. This approach is called "effective variance method".
   // This improvement has been made in version 4.00/08 by Anna Kreshuk.
   //
   //  NOTE:
   //  1) By using the "effective variance" method a simple linear regression
   //      becomes a non-linear case, which takes several iterations
   //      instead of 0 as in the linear case .
   //
   //  2) The effective variance technique assumes that there is no correlation
   //      between the x and y coordinate .
   //
   // Note, that the linear fitter doesn't take into account the errors in x. If errors
   // in x are important, go through minuit (use option "F" for polynomial fitting).
   //
   //   Associated functions
   //   ====================
   //  One or more object (typically a TF1*) can be added to the list
   //  of functions (fFunctions) associated to each graph.
   //  When TGraph::Fit is invoked, the fitted function is added to this list.
   //  Given a graph gr, one can retrieve an associated function
   //  with:  TF1 *myfunc = gr->GetFunction("myfunc");
   //
   //  If the graph is made persistent, the list of
   //  associated functions is also persistent. Given a pointer (see above)
   //  to an associated function myfunc, one can retrieve the function/fit
   //  parameters with calls such as:
   //    Double_t chi2 = myfunc->GetChisquare();
   //    Double_t par0 = myfunc->GetParameter(0); //value of 1st parameter
   //    Double_t err0 = myfunc->GetParError(0);  //error on first parameter
   //
   //   Fit Statistics
   //   ==============
   //  You can change the statistics box to display the fit parameters with
   //  the TStyle::SetOptFit(mode) method. This mode has four digits.
   //  mode = pcev  (default = 0111)
   //    v = 1;  print name/values of parameters
   //    e = 1;  print errors (if e=1, v must be 1)
   //    c = 1;  print Chisquare/Number of degress of freedom
   //    p = 1;  print Probability
   //
   //  For example: gStyle->SetOptFit(1011);
   //  prints the fit probability, parameter names/values, and errors.
   //  You can change the position of the statistics box with these lines
   //  (where g is a pointer to the TGraph):
   //
   //  Root > TPaveStats *st = (TPaveStats*)g->GetListOfFunctions()->FindObject("stats")
   //  Root > st->SetX1NDC(newx1); //new x start position
   //  Root > st->SetX2NDC(newx2); //new x end position

   Int_t iPnt;
   Double_t *ptime = 0;
   Double_t *amplitude = 0;
   amplitude = fAmplitude;
   if (fFixBinSize) {
      ptime = new Double_t[fNPoints];
      for (iPnt = 0; iPnt < fNPoints; iPnt++) {
         ptime[iPnt] = iPnt * fBinSize + fTimeMin;
      }
   } else {
      ptime = fTime;
   }

   if (!fGraph) {
      fGraph = new TGraphErrors(fNPoints);
   } else {
      fGraph->Set(fNPoints);
   }
   memcpy(fGraph->GetX(), ptime, sizeof(Double_t)*fNPoints);
   memcpy(fGraph->GetY(), amplitude, sizeof(Double_t)*fNPoints);

   if (fError) {
      memcpy(fGraph->GetEY(), fError, sizeof(Double_t)*fNPoints);
   }

   return fGraph->Fit(f1, option, "", rxmin, rxmax);
}

//__________________________________________________________________________________
void MEGDRSWaveform::Add(const MEGWaveform *wfin, Double_t baseline, Option_t *opt, Double_t toption)
{
   // Add a waveform to this
   // Option :
   //    "rough"    Synchronize the point specified as "toption" then add amplitude of each points.
   //    "precise"  Add amplitude which is linear interpolated by the both side of points.
   //               If "toption" is specified, synchronize the phase
   //               i.e. wfin is time-shifted by "toption" then added.
   //     default   Find the corresponding point of input waveform for every point.
   //               In terms of both precision and time consumption, this mode has performance between
   //               that of "rough" and "precise".

   TString option = opt;
   option.ToUpper();
   Bool_t UseFindPointInc = option.Contains("INC") ? kTRUE : kFALSE;

   Int_t iPnt;
   Int_t startpointA;
   Int_t startpointB;
   Int_t temppnt;
   Int_t previouspnt = 0;
   Double_t tempamp, ratio;
   Int_t     npointsIn = wfin->GetNPoints();
   if (!npointsIn) {
      return;
   }
   Double_t  binSizeIn = wfin->GetBinSize();
   Double_t  timeMinIn = wfin->GetTimeMin();
   Double_t *amplIn    = wfin->GetAmplitude();
   Double_t *timeIn    = wfin->GetTime();
   if (option.Contains("ROUGH")) {
      startpointA = FindPoint(toption);
      startpointB = wfin->FindPoint(toption);
      for (iPnt = startpointA; iPnt > 0; iPnt--) {
         temppnt = startpointB + iPnt - startpointA;
         if (temppnt < 0) {
            continue;
         } else if (temppnt >= npointsIn - 1) {
            break;
         }
         fAmplitude[iPnt] += amplIn[temppnt] - baseline;
      }
      for (iPnt = startpointA + 1; iPnt < fNPoints; iPnt++) {
         temppnt = startpointB + iPnt - startpointA;
         if (temppnt < 0) {
            continue;
         } else if (temppnt >= npointsIn - 1) {
            break;
         }
         fAmplitude[iPnt] += amplIn[temppnt] - baseline;
      }
   } else if (option.Contains("PRECISE")) {
      if (fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            ratio = (fBinSize * iPnt + fTimeMin - toption - (binSizeIn * temppnt + timeMinIn))
                    / (binSizeIn * (temppnt + 1) - binSizeIn * (temppnt));
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += tempamp - baseline;
         }
      } else if (fFixBinSize && !wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            ratio = (fBinSize * iPnt + fTimeMin - toption - timeIn[temppnt])
                    / (timeIn[temppnt + 1] - timeIn[temppnt]);
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += tempamp - baseline;
         }
      } else if (!fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            ratio = (fTime[iPnt] - toption - (binSizeIn * temppnt + timeMinIn))
                    / (binSizeIn * (temppnt + 1) - binSizeIn * (temppnt));
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += tempamp - baseline;
         }
      } else {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            ratio = (fTime[iPnt] - toption - timeIn[temppnt])
                    / (timeIn[temppnt + 1] - timeIn[temppnt]);
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += tempamp - baseline;
         }
      }

   } else {
      if (fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fBinSize * iPnt + fTimeMin - toption - (binSizeIn * temppnt + timeMinIn)
                > binSizeIn * (temppnt + 1) + timeMinIn - (fBinSize * iPnt + fTimeMin - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += amplIn[temppnt] - baseline;
         }
      } else if (fFixBinSize && !wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fBinSize * iPnt + fTimeMin - toption - (timeIn[temppnt])
                > timeIn[temppnt + 1] - (fBinSize * iPnt + fTimeMin - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += amplIn[temppnt] - baseline;
         }
      } else if (!fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fTime[iPnt] - toption - (binSizeIn * temppnt + timeMinIn)
                > binSizeIn * (temppnt + 1) + timeMinIn - (fTime[iPnt] - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += amplIn[temppnt] - baseline;
         }
      } else { // variable bin size
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fTime[iPnt] - toption - timeIn[temppnt] > timeIn[temppnt + 1] - (fTime[iPnt] - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += amplIn[temppnt] - baseline;
         }
      }
   }
}

//__________________________________________________________________________________
void MEGDRSWaveform::Add(const MEGWaveform *wfin, Double_t baseline, Option_t *opt, Double_t toption,
                         Double_t scale)
{
   // Add a waveform to this with scaling factor
   // Option :
   //    "rough"    Synchronize the point specified as "toption" then add amplitude of each points.
   //    "precise"  Add amplitude which is linear interpolated by the both side of points.
   //               If "toption" is specified, synchronize the phase
   //               i.e. wfin is time-shifted by "toption" then added.
   //     default   Find the corresponding point of input waveform for every point.
   //               In terms of both precision and time consumption, this mode has performance between
   //               that of "rough" and "precise".

   if (TMath::Abs(scale) < 1e-6) {
      return;
   }
   if (TMath::Abs(scale - 1.) < 0.0001) {
      Add(wfin, baseline, opt, toption);
      return;
   }

   TString option = opt;
   option.ToUpper();
   Bool_t UseFindPointInc = option.Contains("INC") ? kTRUE : kFALSE;

   Int_t iPnt;
   Int_t startpointA;
   Int_t startpointB;
   Int_t temppnt;
   Int_t previouspnt = 0;
   Double_t tempamp, ratio;
   Int_t     npointsIn = wfin->GetNPoints();
   if (!npointsIn) {
      return;
   }
   Double_t  binSizeIn = wfin->GetBinSize();
   Double_t  timeMinIn = wfin->GetTimeMin();
   Double_t *amplIn    = wfin->GetAmplitude();
   Double_t *timeIn    = wfin->GetTime();
   if (option.Contains("ROUGH")) {
      startpointA = FindPoint(toption);
      startpointB = wfin->FindPoint(toption);
      for (iPnt = startpointA; iPnt > 0; iPnt--) {
         temppnt = startpointB + iPnt - startpointA;
         if (temppnt < 0) {
            continue;
         } else if (temppnt >= npointsIn - 1) {
            break;
         }
         fAmplitude[iPnt] += (amplIn[temppnt] - baseline) * scale;
      }
      for (iPnt = startpointA + 1; iPnt < fNPoints; iPnt++) {
         temppnt = startpointB + iPnt - startpointA;
         if (temppnt < 0) {
            continue;
         } else if (temppnt >= npointsIn - 1) {
            break;
         }
         fAmplitude[iPnt] += (amplIn[temppnt] - baseline) * scale;
      }
   } else if (option.Contains("PRECISE")) {
      if (fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            ratio = (fBinSize * iPnt + fTimeMin - toption - (binSizeIn * temppnt + timeMinIn))
                    / (binSizeIn * (temppnt + 1) - binSizeIn * (temppnt));
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += (tempamp - baseline) * scale;
         }
      } else if (fFixBinSize && !wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fBinSize * iPnt + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            ratio = (fBinSize * iPnt + fTimeMin - toption - timeIn[temppnt])
                    / (timeIn[temppnt + 1] - timeIn[temppnt]);
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += (tempamp - baseline) * scale;
         }
      } else if (!fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (temppnt < 0 || temppnt >= npointsIn - 1) {
               continue;
            }
            ratio = (fTime[iPnt] - toption - (binSizeIn * temppnt + timeMinIn))
                    / (binSizeIn * (temppnt + 1) - binSizeIn * (temppnt));
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += (tempamp - baseline) * scale;
         }
      } else {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (timeIn[temppnt + 1] - timeIn[temppnt] == 0) {
               ratio = 0;
            } else {
               ratio = (fTime[iPnt] - toption - timeIn[temppnt])
                       / (timeIn[temppnt + 1] - timeIn[temppnt]);
            }
            tempamp = amplIn[temppnt] + ratio * (amplIn[temppnt + 1] - amplIn[temppnt]);
            fAmplitude[iPnt] += (tempamp - baseline) * scale;
         }
      }

   } else {
      if (fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fBinSize * iPnt + fTimeMin - toption - (binSizeIn * temppnt + timeMinIn)
                > binSizeIn * (temppnt + 1) + timeMinIn - (fBinSize * iPnt + fTimeMin - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += (amplIn[temppnt] - baseline) * scale;
         }
      } else if (fFixBinSize && !wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(iPnt * fBinSize + fTimeMin - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fBinSize * iPnt + fTimeMin - toption - (timeIn[temppnt])
                > timeIn[temppnt + 1] - (fBinSize * iPnt + fTimeMin - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += (amplIn[temppnt] - baseline) * scale;
         }
      } else if (!fFixBinSize && wfin->IsFixBinSize()) {
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fTime[iPnt] - toption - (binSizeIn * temppnt + timeMinIn)
                > binSizeIn * (temppnt + 1) + timeMinIn - (fTime[iPnt] - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += (amplIn[temppnt] - baseline) * scale;
         }
      } else { // variable bin size
         for (iPnt = 0; iPnt < fNPoints; iPnt++) {
            if (iPnt == 0 && !UseFindPointInc) {
               previouspnt = temppnt = wfin->FindPoint(fTime[iPnt] - toption, previouspnt);
            } else {
               previouspnt = temppnt = wfin->FindPointIncrement(fTime[iPnt] - toption, previouspnt);
            }
            if (temppnt < 0) {
               previouspnt = 0;
               continue;
            } else if (temppnt >= npointsIn - 1) {
               break;
            }
            if (fTime[iPnt] - toption - timeIn[temppnt] > timeIn[temppnt + 1] - (fTime[iPnt] - toption)) {
               temppnt += 1;
            }

            fAmplitude[iPnt] += (amplIn[temppnt] - baseline) * scale;
         }
      }
   }
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator+=(const MEGDRSWaveform &wf1)
{
   // Operator +=
   // Add waveform to this just simply

   transform(fAmplitude, fAmplitude + fNPoints, wf1.fAmplitude, fAmplitude,  plus<Double_t>());
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator-=(const MEGDRSWaveform &wf1)
{
   // Operator -=
   // Sabtruct waveform from this just simply

   transform(fAmplitude, fAmplitude + fNPoints, wf1.fAmplitude, fAmplitude,  minus<Double_t>());
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator+=(Double_t c1)
{
   // Operator +=  (Constant shift)
   // Add constant for all points

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a + c1;});
             bind(plus<Double_t>(), placeholders::_1, c1));
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator-=(Double_t c1)
{
   // Operator -=  (Constant shift)
   // Sabtruct constant from all points

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a - c1;});
             bind(minus<Double_t>(), placeholders::_1, c1));
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator*=(Double_t c1)
{
   // Operator *=  (Scale)
   // Multiply c1 for all points

   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a * c1;});
             bind(multiplies<Double_t>(), placeholders::_1, c1));
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator/=(Double_t c1)
{
   // Operator /=  (Scale)
   // Divide all points by c1

   Double_t c = 1 / c1;
   transform(fAmplitude, fAmplitude + fNPoints, fAmplitude,
             //[&](Double_t &a){ return a * c;});
             bind(multiplies<Double_t>(), placeholders::_1, c));
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform& MEGDRSWaveform::operator=(const MEGDRSWaveform &wf)
{
   // Operator =

   if (this != &wf) {
      wf.TNamed::Copy(*this);
      wf.TAttLine::Copy(*this);
      wf.TAttFill::Copy(*this);
      wf.TAttMarker::Copy(*this);
      //fNPoints   = wf.fNPoints;
      SetNPoints(wf.fNPoints);
      fBinSize   = wf.fBinSize;
      fFixBinSize = wf.fFixBinSize;
      fError     = wf.fError;
      fID        = wf.fID;
      fOverflow  = wf.fOverflow;
      fTimeMin   = wf.fTimeMin;
      fTimeMax   = wf.fTimeMax;
      fStartCell = wf.fStartCell;
      fHistogram = 0;
      fGraph     = 0;
      fMinimum   = wf.fMinimum;
      fMaximum   = wf.fMaximum;
      fDisplayAmplUnit = wf.fDisplayAmplUnit;
      fDisplayTimeUnit = wf.fDisplayAmplUnit;
      fEditable  = wf.fEditable;
      fNErrorPoints = wf.fNErrorPoints;
      fFCN       = wf.fFCN;
      memcpy(fTime, wf.fTime, sizeof(Double_t)*fNPoints);
      memcpy(fAmplitude, wf.fAmplitude, sizeof(Double_t)*fNPoints);
   }
   return *this;
}

//__________________________________________________________________________________
MEGDRSWaveform MEGDRSWaveform::operator+(const MEGDRSWaveform &wf1)
{
   // Operator +

   MEGDRSWaveform wfnew = *this;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wf1.fAmplitude, wfnew.fAmplitude,
             plus<Double_t>());
   return wfnew;
}

//__________________________________________________________________________________
MEGDRSWaveform MEGDRSWaveform::operator-(const MEGDRSWaveform &wf1)
{
   // Operator -

   MEGDRSWaveform wfnew = *this;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wf1.fAmplitude, wfnew.fAmplitude,
             minus<Double_t>());
   return wfnew;
}

//__________________________________________________________________________________
MEGDRSWaveform operator*(Double_t c1, const MEGDRSWaveform &wf1)
{
   // Operator *

   MEGDRSWaveform wfnew = wf1;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wfnew.fAmplitude,
             //[&](Double_t &a){ return a * c1;});
             bind(multiplies<Double_t>(), placeholders::_1, c1));
   return wfnew;
}

//__________________________________________________________________________________
MEGDRSWaveform operator/(Double_t c1, const MEGDRSWaveform &wf1)
{
   // Operator *

   MEGDRSWaveform wfnew = wf1;
   Double_t c = 1 / c1;
   transform(wfnew.fAmplitude, wfnew.fAmplitude + wfnew.fNPoints, wfnew.fAmplitude,
             //[&](Double_t &a){ return a * c;});
             bind(multiplies<Double_t>(), placeholders::_1, c));
   return wfnew;
}
