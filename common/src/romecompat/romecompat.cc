/********************************************************************\

   Name:         romecompat.c
   Created by:   Ryu Sawada

   Contents:     check if rome and meg revision is compatible.

\********************************************************************/

#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include "romecompat/romecompat.h"

using namespace std;

const string rome_sha = "17c5b337ec4316dfadf11f5b9dbcdb004076e4a9";
const string meg_sha  = "29cbd0f17e52cca6bb10b6081cd1d119bfa7c0b3";

/*------------------------------------------------------------------*/
/*
  return 1 when compatible.
  return 0 when incompatible.
*/
int romecompat()
{
   string command = "cd ";
   const char *romesys = getenv("ROMESYS");
   if (!romesys) {
      return -1;
   }
   command += romesys;
   command += "; git ls-tree --name-only ";
   command += rome_sha;
   command += " > /dev/null 2>&1";

   int result = system(command.c_str());

   // cout<<command<<" "<<result<<" "<<WEXITSTATUS(result)<<endl;

   return WEXITSTATUS(result);
}

/*------------------------------------------------------------------*/
int main(int /* argc */, char ** /* argv */)
{

   if (romecompat() == 0) {
      return 0;
   } else {
      cout << endl;
      cout << "From meg2 (" << meg_sha << "), rome (" << rome_sha << ") is required." << endl;
      cout << "Please update rome (i.e. 'cd $ROMESYS; git pull')." << endl;
      cout << endl;
      cout << "If your rome is not the git version, please donwload it." << endl;
      cout << "See https://bitbucket.org/muegamma/meg2/wiki/Installing\%20Analysis\%20and\%20MC\%20Software#markdown-header-rome"
           << endl;
      cout << endl;
      return 1;
   }

   return 0;
}
