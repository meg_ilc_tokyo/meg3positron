// $Id$
// Author: Shuei YAMADA

#ifndef MEGColors_H
#define MEGColors_H

#include "constants/geant/geant3particleid.h"

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// MEGColors                                                                  //
//                                                                            //
// Define common color scheme for various event displays                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace MEGColors
{
enum {kDark = 100, kBright = 150};

// color for tracks
int MCTrack(int g3pid);
int RecTrack(int g3pid = kG3Positron);

// color for hits
inline int XECFirstConversion() { return kRed; }
inline int XECFirstInteraction() { return kGreen; }
inline int XECRecPosition() { return kWhite; }

inline int TICPMCHit() { return 52; }
inline int TICZMCHit() { return 65; }
}

#endif
