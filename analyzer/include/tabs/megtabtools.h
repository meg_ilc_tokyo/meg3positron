// Author: Shuei YAMADA

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Collection of helper routines to access UserInfoFrame and display
// information which is common to all the tabs
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
// $Id$

#ifndef MEGTCOMMON_H
#define MEGTCOMMON_H

class TGVerticalFrame;
class TObjArray;

namespace MEGTABTOOLS
{
Int_t Init(TGVerticalFrame *userInfoFrame, TObjArray *userInfoObjects);
void EventHandler(TObjArray *userInfoObjects, Int_t mask);
const Int_t nLabel = 2;
}


#endif
