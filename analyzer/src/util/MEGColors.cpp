// $Id$
// Author: Shuei YAMADA

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Class MEGColors                                                            //
//                                                                            //
// Define common color scheme for various event displays                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <Rtypes.h>
#include <Gtypes.h>
#include "util/MEGColors.h"
#include "constants/geant/geant3particleid.h"

// For the moment, ROME cannot handle NamespaceImp()
// NamespaceImp(MEGColors)

int MEGColors::MCTrack(int g3pid)
{
   // Defines color of a MC track accoring to its GEANT3 particle code.

   switch (g3pid) {
   case kG3Gamma:
      return kYellow;
//      return kBlue;
   case kG3Positron:
      return kCyan;
   case kG3Electron:
      return kMagenta;
   case kG3MuonPlus:
   case kG3MuonMinus:
      return kGreen;
   case kG3Alpha:
      return kRed;
   default:
      return 14; // dark gray
//      return kYellow;
   }
}

int MEGColors::RecTrack(int g3pid)
{
   // Defines color of a reconstructed track accoring to its GEANT3 particle code.
   // For the time being, only positrons and electrons are considered.

   switch (g3pid) {
   case kG3Electron:
      return kRed;
   case kG3Positron:
   default:
      return kBlue;
   }
}
