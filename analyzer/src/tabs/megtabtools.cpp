// Author: Shuei YAMADA

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Collection of helper routines to access UserInfoFrame and display
// information which is common to all the tabs
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
// $Id$

#include <RConfig.h>
#include <Rtypes.h>
#if defined( R__VISUAL_CPLUSPLUS )
#pragma warning( push )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4800 )
#endif // R__VISUAL_CPLUSPLUS

#include "tabs/megtabtools.h"
//#include "trigger/triggercommon.h"
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGWidget.h>
#include <TObjArray.h>

#if defined( R__VISUAL_CPLUSPLUS )
#pragma warning( pop )
#endif // R__VISUAL_CPLUSPLUS

Int_t MEGTABTOOLS::Init(TGVerticalFrame *userInfoFrame, TObjArray *userInfoObjects)
{
   Int_t nsize = userInfoObjects->GetEntriesFast();
   if (nsize > 0) {
      return -1;
   }

   Int_t iLabel;
   const char labelStr[nLabel][32] = {
      "Event Type : ",
      "",
   };
   userInfoObjects->Expand(nLabel);

   TGHorizontalFrame *triggerFrame;
   TGLabel           *triggerLabel;
   TGLabel           *triggerValue;

   for (iLabel = 0; iLabel < nLabel; iLabel++) {
      triggerFrame = new TGHorizontalFrame(userInfoFrame);
      userInfoFrame->AddFrame(triggerFrame, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 15, 10, 2, 2));

      triggerLabel = new TGLabel(triggerFrame, labelStr[iLabel]);
      triggerLabel->SetTextJustify(kTextLeft | kTextCenterY);
      triggerFrame->AddFrame(triggerLabel, new TGLayoutHints(kLHintsLeft, 0, 0, 0, 0));

      triggerValue = new TGLabel(triggerFrame, "");
      triggerValue->SetTextJustify(kTextRight | kTextCenterY);
      triggerFrame->AddFrame(triggerValue, new TGLayoutHints(kLHintsRight, 5, 0, 0, 0));

      userInfoObjects->AddAt(triggerValue, iLabel);
   }

   return nLabel;
}


void MEGTABTOOLS::EventHandler(TObjArray *userInfoObjects, Int_t mask)
{
   if (!userInfoObjects) {
      return;
   }

   Int_t nsize = userInfoObjects->GetEntriesFast();
   if (nsize < nLabel) {
      return;
   }

   char  str[32];

   TGLabel *label;
   label = static_cast<TGLabel*>(userInfoObjects->At(0));
   sprintf(str, "%4d", mask);
   label->SetText(str);

   // label = static_cast<TGLabel*>(userInfoObjects->At(1));
   // label->SetText(TRIGGERCOMMON::triggerString[mask]);

   // Those labels are updated when layout() is called in ArgusWindow::TriggerEventHandler.
   return;
}
