#!/bin/bash
###############################################################################
# A sample SLURM script to run gem4 on 1 node
#
#  usage:
#        (0) Edit this script
#        (1) Submit the job.
#            % cd $MEG3SYS/gem4
#            % sbatch example/gem4.sl
#
###############################################################################

#SBATCH --partition=all
#SBATCH --job-name=gem4
#SBATCH --output="gem4-%j.out"
#SBATCH --error="gem4-%j.err"

MY_HOST=`hostname`
MY_DATE=`date`
#
# Disable core dump. (Remove this line if you want to create core files)
#
ulimit -c 0

echo "Running on $MY_HOST at $MY_DATE"
echo "================================================================"

#
# Print envirionment
#
#echo "Running environment":
#env | sort
#echo "================================================================"

#
# Replace configuration file by yours
#
${MEG3SYS}/gem4/bin/Linux-g++/gem4 -i macros/example.mac -n 10 -r 1 --cout=STDOUT --cerr=STDERR


RET=$?
echo "Exit status: $RET"
echo "================================================================"
END_DATE=`date`
echo "End of run is $END_DATE"
