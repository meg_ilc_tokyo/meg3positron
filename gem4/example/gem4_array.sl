#!/bin/bash
###############################################################################
# A sample SLURM script to run gem4 on 1 node
#
#  usage:
#        (0) Edit this script
#        (1) Submit an array job.
#            % cd $MEG3SYS/gem4
#            % sbatch -a 0-20%10 example/gem4_array.sl
#
###############################################################################

#SBATCH --partition=all
#SBATCH --job-name=gem4
#SBATCH --array=0
#SBATCH --output="gem4-%A_%a.out"
#SBATCH --error="gem4-%A_%a.err"



# Output directory of the root files
OUTPUT_DIR=./

# Replace configuration file by yours
INPUT_FILE=macros/example.mac

# Run number (start run number for array job)
START_RUNNUM=1

# Sleep time between subjobs in array
SLEEP_SEC=1

MY_HOST=`hostname`
MY_DATE=`date`
#
# Disable core dump. (Remove this line if you want to create core files)
#
ulimit -c 0

echo "Running on $MY_HOST at $MY_DATE"
echo "================================================================"

#
# Print envirionment
#
#echo "Running environment":
#env | sort
#echo "================================================================"

sleep $(( SLEEP_SEC * SLURM_ARRAY_TASK_ID ))

mkdir -p $OUTPUT_DIR

# Customize macro
TEMP_FILE=${OUTPUT_DIR}/${INPUT_FILE##*/}
sed -e "s|sim#.root|$OUTPUT_DIR/sim#.root|g" \
    -e "s|sev#.root|$OUTPUT_DIR/sev#.root|g" \
    ${INPUT_FILE} \
    > ${TEMP_FILE} 

# Increment run number for array job
RUNNUM=$(( START_RUNNUM + SLURM_ARRAY_TASK_ID ))

${MEG3SYS}/gem4/bin/Linux-g++/gem4 -i ${TEMP_FILE} -n 10 --cout=STDOUT --cerr=STDERR -r $RUNNUM

RET=$?
echo "Exit status: $RET"
echo "================================================================"
END_DATE=`date`
END_DATE_S=`date +%s`
echo "End of run is $END_DATE ($SECONDS sec)"

