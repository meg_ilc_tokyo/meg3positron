//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <vector>
#include <set>
#include <list>
#include <string>
#include <getopt.h>
#include <TUUID.h>

#include "GEMRootIO.hh"
#include "MAGModule.hh"
#include "TARModule.hh"
//#include "ATARModule.hh"
#include "BMUModule.hh"
#include "TMPModule.hh"
#include "SVTModule.hh"
#include "GCSModule.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4UImanager.hh"
#include "G4UIdirectory.hh"

#include "GEMUIsession.hh"
#include "GEMPhysicsList.hh"
#include "GEMPrimaryGeneratorAction.hh"
#include "GEMDetectorConstruction.hh"
#include "GEMSteppingVerbose.hh"

#include "GEMRunAction.hh"
#include "GEMEventAction.hh"
#include "GEMStackingAction.hh"
#include "GEMTrackingAction.hh"
#include "GEMSteppingAction.hh"



#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#endif

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void tokenize(const char *org, const char *delim, set<string> &arr)
{
   list<G4int> splitIndex;
   string orgStr   = org;
   string delibStr = delim;

   G4int start, nrDiff = 0;
   start = 0;
   while (start < static_cast<G4int>(orgStr.size())) {
      size_t pos = orgStr.find(delim, start);
      if (pos == string::npos) break;
      splitIndex.push_back(pos);
      start = pos + 1;
   }
   if (start > 0) nrDiff++;
   splitIndex.push_back(orgStr.size());

   if (nrDiff > 1) {
      splitIndex.sort();
   }

   arr.clear();

   start = -1;
   list<G4int>::iterator it;
   for (it = splitIndex.begin(); it != splitIndex.end(); it++) {
      G4int stop = *it;
      if (stop - 1 >= start + 1) {
         string tok = orgStr.substr(start + 1, stop - start - 1);
         arr.insert(tok);
      }
      start = stop;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void help()
{
   G4cerr<<"Usage:"<<G4endl<<
         "  (batch mode)       gem4 [OPTIONS] -r 1 -n 10 -i macros/example.mac"<<G4endl<<
         "  (interactive mode) gem4 [OPTIONS] -I -i macros/vis.mac"<<G4endl<<
         G4endl<<
         "Options"<<G4endl<<
         "  -i, --input=NAME    A macro file executed before starting a run"<<G4endl<<
         "  -r, --run=INT       Run number"<<G4endl<<
         "  -n, --nevent=INT    Number of events"<<G4endl<<
         "                      When this is zero, a run should started"<<G4endl<<
         "                      in a macro with /run/beamOn command."<<G4endl<<
         "  -s, --seed=INT      Random seed"<<G4endl<<
         "                      When this option is omitted, a random number is used. "<<G4endl<<
         "  -v, --vis=on/off    Turn on/off visualization"<<G4endl<<
         "                      When this option is omitted, the visualization is turned on in"<<G4endl<<
         "                      intractive sessions, otherwise it is off."<<G4endl<<
         "  -d, --disable=NAMES Disable modules"<<G4endl<<
         "                      (e.g. --disable=xec,spx)"<<G4endl<<
         "                      modules are"<<G4endl<<
         "                       mag : Cobra magnet"<<G4endl<<
         "                       tar : Target"<<G4endl<<
         "                       bmu : BTS and US-endcap"<<G4endl<<
         "                       svt : Silicon vertex tracker"<<G4endl<<         
         "                       gcs : Gamma converter spectrometer"<<G4endl<<
         "  -e, --enable=NAMES  Enable optional modules"<<G4endl<<
         "                      (e.g. --enable=tmp)"<<G4endl<<
         "                      modules are"<<G4endl<<
         "                       atar: Active target"<<G4endl<<
         "                       tmp : Template module"<<G4endl<<
         "  -I, --interactive   Start an interactive session."<<G4endl<<
         "                      UI type depends on environment variables."<<G4endl<<
         "  -T, --tcsh          Start an interactive session on terminal."<<G4endl<<
         "  -O, --cout=FILE     Set filename to redirect cout."<<G4endl<<
         "                      When FILE ends with +, it is opened in append-mode."<<G4endl<<
         "                      Use STDOUT to print on screen."<<G4endl<<
         "                      Default is gem4.out."<<G4endl<<
         "                      In visualize-mode, it is set to be STDOUT."<<G4endl<<
         "  -E, --cerr=FILE     Set filename to redirect cerr."<<G4endl<<
         "                      Use STDERR to print on screen."<<G4endl<<
         "                      Default is STDERR."<<G4endl<<
         "                      In visualize-mode, it is set to be STDERR."<<G4endl<<
         G4endl<<
         "      --help          Display this help and exit."<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc, char** argv)
{
   G4bool   opt_interactive = false;
   G4bool   opt_tcsh        = false;
   G4String opt_input       = "";
   G4int    opt_run         = 1;
   G4int    opt_nevent      = 0;
   G4long   opt_seed        = 0;
   G4String opt_vis         = "";
   G4bool   opt_disable_mag = false;
   G4bool   opt_disable_tar = false;
   G4bool   opt_disable_bmu = false;
   G4bool   opt_disable_svt = false;   
   G4bool   opt_disable_gcs = false;
   G4bool   opt_enable_tmp  = false;
   G4String opt_coutfile = "";
   G4String opt_cerrfile = "";

   int argcTmp = 0;
   char **argvTmp = new char*[argc];

   // parse options
   int c;
   while (1) {
      int optionIndex = 0;
      static struct option longOptions[] = {
         {"interactive", required_argument, 0, 'I'},
         {"tcsh"       , required_argument, 0, 'T'},
         {"input"      , required_argument, 0, 'i'},
         {"run"        , required_argument, 0, 'r'},
         {"nevent"     , required_argument, 0, 'n'},
         {"seed"       , required_argument, 0, 's'},
         {"visual"     , required_argument, 0, 'v'},
         {"disable"    , required_argument, 0, 'd'},
         {"enable"     , required_argument, 0, 'e'},
         {"cout"       , required_argument, 0, 'O'},
         {"cerr"       , required_argument, 0, 'E'},
         {"help"       , no_argument      , 0,  0},
         {0, 0, 0, 0}
      };

      c = getopt_long(argc, argv, "ITi:r:n:s:v:d:e:O:E:h", longOptions, &optionIndex);
      if (c == -1) {
         break;
      }

      switch (c) {
      case 0: // long option
         if (!strncmp(longOptions[optionIndex].name, "help", sizeof("help"))) {
            help();
            return 0;
         }
         break;
      case 'I':
         opt_interactive = true;
         break;
      case 'T':
         opt_tcsh        = true;
         break;
      case 'i':
         if (optarg) {
            opt_input = optarg;
         }
         break;
      case 'r':
         if (optarg) {
            opt_run = strtol(optarg, 0, 10);
         }
         break;
      case 'n':
         if (optarg) {
            opt_nevent = strtol(optarg, 0, 10);
         }
         break;
      case 's':
         if (optarg) {
            opt_seed = strtol(optarg, 0, 10);
         }
         break;
      case 'v':
         if (optarg) {
            opt_vis = optarg;
         }
         break;
      case 'd':
         if (optarg) {
            set<string> disablemodes;
            tokenize(optarg, ",", disablemodes);
            if (disablemodes.find("mag") != disablemodes.end()) {
               opt_disable_mag = true;
            }
            if (disablemodes.find("tar") != disablemodes.end()) {
               opt_disable_tar = true;
            }
            if (disablemodes.find("bmu") != disablemodes.end()) {
               opt_disable_bmu = true;
            }
            if (disablemodes.find("svt") != disablemodes.end()) {
               opt_disable_svt = true;
            }
            if (disablemodes.find("gcs") != disablemodes.end()) {
               opt_disable_gcs = true;
            }
         }
         break;
      case 'e':
         if (optarg) {
            set<string> enablemodes;
            tokenize(optarg, ",", enablemodes);
            if (enablemodes.find("tmp") != enablemodes.end()) {
               opt_enable_tmp = true;
            }
          }
         break;
      case 'O':
         if (optarg) {
            opt_coutfile = optarg;
         }
         break;
      case 'E':
         if (optarg) {
            opt_cerrfile = optarg;
         }
         break;
      case '?':
         help();
         return 1;
         break;
      default:
         G4cerr<<__FILE__<<":"<<__LINE__<<" "<<"?? getopt returned character code 0"<<c<<" ??"<<G4endl;
      }
   }
   if (optind < argc) {
      G4cout<<__FILE__<<":"<<__LINE__<<" "<<"non-option ARGV-elements: "<<G4endl;
      while (optind < argc) {
         G4cerr<<argv[optind++]<<G4endl;
      }
      G4cerr<<G4endl;
      help();
      return 1;
   }

   G4bool visualization;
   if (opt_vis == "") {
      visualization = opt_interactive;
   } else {
      visualization =
         !G4StrUtil::icompare(opt_vis, "on")    ||
         !G4StrUtil::icompare(opt_vis, "1")     ||
         !G4StrUtil::icompare(opt_vis, "true")  ||
         !G4StrUtil::icompare(opt_vis, "active");
   }

   if (visualization) {
      // disable redirection
      opt_coutfile = "STDOUT";
      opt_cerrfile = "STDERR";
   }

   if (!opt_tcsh && !opt_interactive) {
      G4UImanager* UImanager = G4UImanager::GetUIpointer();
      GEMUIsession *gemUISession = new GEMUIsession();
      gemUISession->OpenOutFile(opt_coutfile);
      gemUISession->OpenErrFile(opt_cerrfile);
      UImanager->SetCoutDestination(gemUISession);
      // uiSession->SessionStart(); // not required in this case
   }

   // Seed the random number generator manually
   if (opt_seed == 0) {
      TUUID uid;
      UChar_t uuid[16];
      uid.GetUUID(uuid);
      opt_seed = (uuid[0]&0x7F)*(1<<24) + uuid[1]*(1<<16) + uuid[2]*(1<<8) + uuid[3]*(1<<0);
   }
   G4cout<<"Seed: "<<opt_seed<<endl;
   CLHEP::HepRandom::setTheSeed(opt_seed);

   // User Verbose output class
   G4VSteppingVerbose* verbosity = new GEMSteppingVerbose();
   G4VSteppingVerbose::SetInstance(verbosity);

   // Run manager
   G4RunManager* runManager = new G4RunManager();

   // Make gem directory
   (new G4UIdirectory("/gem/"))->SetGuidance("gem control commands.");

   // UserInitialization classes - mandatory
   G4VUserPhysicsList* physics = new GEMPhysicsList;
   runManager->SetUserInitialization(physics);

   // Register modules
   vector<GEMAbsModule*> modules;
   vector<GEMAbsModule*>::iterator modIter;
   GEMAbsModule         *tarmodule = 0;
   if (!opt_disable_mag) { modules.push_back(new MAGModule("MAG")); } // MAG should be the first
   if (!opt_disable_tar) { modules.push_back((tarmodule = new TARModule("TAR", static_cast<GEMPhysicsList*>(physics), modules.size()))); }
   if (!opt_disable_bmu) { modules.push_back(new BMUModule("BMU", modules.size())); }
   if (!opt_disable_svt) { modules.push_back(new SVTModule("SVT", static_cast<GEMPhysicsList*>(physics), modules.size())); }   
   if (!opt_disable_gcs) { modules.push_back(new GCSModule("GCS", modules.size())); }
   if ( opt_enable_tmp)  { modules.push_back(new TMPModule("TMP", modules.size())); }

   G4cout<<"Active modules : ";
   for (modIter = modules.begin(); modIter != modules.end(); ++modIter) {
      if (modIter != modules.begin()) {
         G4cout<<", ";
      }
      G4cout<<(*modIter)->GetName();
   }
   G4cout<<endl;

   GEMPrimaryGeneratorAction *gen_action =
      new GEMPrimaryGeneratorAction(modules, static_cast<GEMPhysicsList*>(physics));
   runManager->SetUserAction(static_cast<G4VUserPrimaryGeneratorAction*>(gen_action));

   // choose the target volume used in the event generator.
   //  so far, we have only one target module.
   if (tarmodule) {
      gen_action->SetTargetModule(tarmodule);
   }




   GEMDetectorConstruction *detector = new GEMDetectorConstruction(opt_input);
   detector->SetGeneratorAction(gen_action);

   detector->AddModules(modules);
   runManager-> SetUserInitialization(detector);

#ifdef G4VIS_USE
   // visualization manager
   G4VisManager* visManager = 0;
   if (visualization) {
      visManager = new G4VisExecutive("quiet");
      visManager->Initialize();
      visManager->SetVerboseLevel("warnings");
   }
#endif


   // UserAction classes
   G4UserRunAction      *runAct = new GEMRunAction();
   G4UserEventAction    *eveAct = new GEMEventAction();
   G4UserStackingAction *staAct = new GEMStackingAction();
   G4UserTrackingAction *traAct = new GEMTrackingAction();
   G4UserSteppingAction *steAct = new GEMSteppingAction();

   runManager->SetUserAction(runAct);
   runManager->SetUserAction(eveAct);
   runManager->SetUserAction(staAct);
   runManager->SetUserAction(traAct);
   runManager->SetUserAction(steAct);

   static_cast<GEMRunAction*>(runAct)->AddModules(modules);
   static_cast<GEMEventAction*>(eveAct)->AddModules(modules);
   static_cast<GEMStackingAction*>(staAct)->AddModules(modules);
   static_cast<GEMTrackingAction*>(traAct)->AddModules(modules);
   static_cast<GEMSteppingAction*>(steAct)->AddModules(modules);

   detector->SetRunAction(runAct);
   detector->SetEventAction(eveAct);
   detector->SetStackingAction(staAct);
   detector->SetTrackingAction(traAct);
   detector->SetSteppingAction(steAct);

   // Initialize G4 kernel
   //runManager->Initialize();
   // Don't initialize here, but do it in macro
   // after setting phyiscs

   // Get the pointer to the User Interface manager
   G4UImanager* UImanager = G4UImanager::GetUIpointer();

   // Construct ROOT I/O
   GEMRootIO::GetInstance()->SetRunManager(runManager);
   GEMRootIO::GetInstance()->SetDetector(detector);

#ifdef G4UI_USE
   G4UIExecutive *ui = 0;
   G4UIterminal  *tcsh = 0;
   if (opt_interactive || opt_tcsh) { // Define UI session for interactive mode
      G4String tmpArg;
      G4bool copyArg = false;
      G4int iArg = 0;
      argvTmp[iArg] = argv[iArg];
      argcTmp++;
      for (iArg = 1; iArg < argc; iArg++) {
         tmpArg = argv[iArg];
         if (tmpArg == "-I") {
//            copyArg = true;
         } else if (copyArg) {
            argvTmp[argcTmp] = argv[iArg];
            argcTmp++;
         }
      }
      if (opt_tcsh) {
         tcsh = new G4UIterminal(new G4UItcsh());
      } else if (opt_interactive) {
         ui = new G4UIExecutive(argcTmp, argvTmp);
      }
   }
#endif

   runManager->SetRunIDCounter(opt_run);


   G4String command;
   if (opt_input != "") {
      command = "/control/execute ";
      G4String fileName = opt_input;
      UImanager->ApplyCommand(command+fileName);
   }

   //GEMRootIO::GetInstance()->SetRunManager(runManager);


   //runManager->SetRunIDCounter(opt_run);


   if (opt_nevent > 0) {
      runManager->BeamOn(opt_nevent);
   }

#ifdef G4UI_USE
   // Define UI session for interactive mode
   if (tcsh) {
      tcsh->SessionStart();
      delete tcsh;
   } else if (ui) {
      ui->SessionStart();
      delete ui;
   }
#endif

   // Job termination
   // Free the store: user actions, physics_list and detector_description are
   //                 owned and deleted by the run manager, so they should not
   //                 be deleted in the main() program !

#ifdef G4VIS_USE
   delete visManager;
#endif
   delete runManager;
   delete verbosity;

   delete [] argvTmp;

   return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
