//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
// Author: Yusuke Uchiyama
////////////////////////////////////////////////////
// 
// Class SVTSiROGeometry
//
//  This class describes GEANT hit in SVT Si sensor.
//
///////////////////////////////////////////////////

#ifndef SVTSiROGeometry_h
#define SVTSiROGeometry_h 1

#include "G4VReadOutGeometry.hh"
class G4Material;
class SVTModule;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTSiROGeometry : public G4VReadOutGeometry
{
public:
   SVTSiROGeometry();
   SVTSiROGeometry(G4String, SVTModule *module);
   ~SVTSiROGeometry();

private:
   G4VPhysicalVolume* Build();

   SVTModule   *fModule;
   G4Material  *fDummyMat;
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
