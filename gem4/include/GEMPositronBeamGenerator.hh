//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMPositronBeamGeneratorAction_h
#define GEMPositronBeamGeneratorAction_h 1

#include <vector>
#include <list>
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4Event;
class G4VPhysicalVolume;
class GEMPrimaryGeneratorAction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMPositronBeamGenerator
{
   enum MOMSHAPE {
      MOMSHAPE_GAUSS           = 0,
      MOMSHAPE_TRUNCATED_GAUSS = 1
   };

protected:
   GEMPrimaryGeneratorAction         *generatorAction;
   std::list<std::vector<G4double> > *phaseSpacePool;
   G4double                           positronPolarization;
   G4bool                             phaseSpaceReadFromFile;
   G4String                           phaseSpaceFileName;
   G4double                           phaseSpaceMeanX;
   G4double                           phaseSpaceSigmaX;
   G4double                           phaseSpaceMeanXp;
   G4double                           phaseSpaceSigmaXp;
   G4double                           phaseSpaceCorrX;
   G4double                           phaseSpaceMeanY;
   G4double                           phaseSpaceSigmaY;
   G4double                           phaseSpaceMeanYp;
   G4double                           phaseSpaceSigmaYp;
   G4double                           phaseSpaceCorrY;
   G4double                           phaseSpaceMeanZ;
   G4double                           phaseSpaceSigmaZ;
   G4int                              phaseSpaceMomentumShape;
   G4double                           phaseSpaceMomentumMean;
   G4double                           phaseSpaceMomentumSigma;
   G4double                           phaseSpaceMomentumWidth;

public:
   GEMPositronBeamGenerator();
   GEMPositronBeamGenerator(GEMPrimaryGeneratorAction *act);
   ~GEMPositronBeamGenerator();

public:
   void GeneratePrimaries(G4Event *anEvent);

   void SetPositronPolarization(G4double v)    { positronPolarization = v; }
   void SetPhaseSpaceReadFromFile(G4bool &b)   { phaseSpaceReadFromFile = b; }
   void SetPhaseSpaceFileName(G4String &str)   { phaseSpaceFileName = str; }
   void SetPhaseSpaceMeanX(G4double v)         { phaseSpaceMeanX = v; }
   void SetPhaseSpaceSigmaX(G4double v)        { phaseSpaceSigmaX = v; }
   void SetPhaseSpaceMeanXp(G4double v)        { phaseSpaceMeanXp = v; }
   void SetPhaseSpaceSigmaXp(G4double v)       { phaseSpaceSigmaXp = v; }
   void SetPhaseSpaceCorrX(G4double v)         { phaseSpaceCorrX = v; }
   void SetPhaseSpaceMeanY(G4double v)         { phaseSpaceMeanY = v; }
   void SetPhaseSpaceSigmaY(G4double v)        { phaseSpaceSigmaY = v; }
   void SetPhaseSpaceMeanYp(G4double v)        { phaseSpaceMeanYp = v; }
   void SetPhaseSpaceSigmaYp(G4double v)       { phaseSpaceSigmaYp = v; }
   void SetPhaseSpaceCorrY(G4double v)         { phaseSpaceCorrY = v; }
   void SetPhaseSpaceMeanZ(G4double v)         { phaseSpaceMeanZ = v; }
   void SetPhaseSpaceSigmaZ(G4double v)        { phaseSpaceSigmaZ = v; }
   void SetPhaseSpaceMomentumShape(G4int i)    { phaseSpaceMomentumShape = i; }
   void SetPhaseSpaceMomentumMean(G4double v)  { phaseSpaceMomentumMean = v; }
   void SetPhaseSpaceMomentumSigma(G4double v) { phaseSpaceMomentumSigma = v; }
   void SetPhaseSpaceMomentumWidth(G4double v) { phaseSpaceMomentumWidth = v; }

private:
   void ReadPhaseSpaceFile(const char* filename);
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
