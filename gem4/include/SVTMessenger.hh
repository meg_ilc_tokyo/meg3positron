//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef SVTMessenger_h
#define SVTMessenger_h 1

#include "G4UImessenger.hh"
#include "G4OpticalSurface.hh"
#include "globals.hh"

class SVTModule;

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAString;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTMessenger: public G4UImessenger
{
private:
   static SVTModule    *fgModule;

   G4UIdirectory *fGeomDir;
   G4UIdirectory *fTrackingDir;
   G4UIdirectory *fMaterialDir;
   G4UIdirectory *fSensorDir;
   G4UIdirectory *fSupportDir;
   G4UIdirectory *fDigiDir;
   G4UIdirectory *fVisDir;

   G4UIcmdWithAnInteger      *fVerboseCmd;
   G4UIcmdWithABool          *fCheckOverlapCmd;
   G4UIcmdWithADoubleAndUnit *fLayerRadius0Cmd;
   G4UIcmdWithADoubleAndUnit *fLayerRadius1Cmd;
   G4UIcmdWithAnInteger      *fNSensorInZ0Cmd;
   G4UIcmdWithAnInteger      *fNSensorInZ1Cmd;
   G4UIcmdWithADoubleAndUnit *fSensorSlantAngleCmd;
   G4UIcmdWithADoubleAndUnit *fSensorOverlapCmd;
   G4UIcmdWithADoubleAndUnit *fSensorThicknessCmd;
   G4UIcmdWithADoubleAndUnit *fDepThicknessCmd;
   G4UIcmdWithADoubleAndUnit *fPixelSizeZCmd;
   G4UIcmdWithADoubleAndUnit *fPixelSizeYCmd;
   

public:
   SVTMessenger(SVTModule*);
   ~SVTMessenger();

   void SetNewValue(G4UIcommand*, G4String);

private:

};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
