//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMPrimaryGeneratorAction_h
#define GEMPrimaryGeneratorAction_h 1

#include <vector>
#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "GEMAbsModule.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "GEMStoppedMuonDecayGenerator.hh"
#include "GEMMuonBeamGenerator.hh"


class GEMPhysicsList;
class G4ParticleGun;
class G4GeneralParticleSource;
class G4Event;
class GEMPrimaryGeneratorMessenger;
class G4VPhysicalVolume;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
enum EGEMEventTypeID {
   // =================================================================
   ISIGNAL          =  1,
   // =================================================================
   IRADIATIVE       =  2,
   IAIFAIF          =  3,
   IPOSITRON2GAMMA  =  4,
   //                  1 =  5-9  : free for pair events
   // =================================================================
   ISIGNALPOSITRON  = 11,
   IMICHEL          = 12,
   INOISEE          = 13,
   //                  1 = 14..19: free for single e+ events
   // =================================================================
   ISIGNALPHOTON    = 21,
   IRADIATIVEPHOTON = 22,
   IAFGAMA          = 23,
   IFLGAMA          = 24,
   IAFGAMATAB       = 29,
   // =================================================================
   IMUBEAM          = 30,
   //                  1 = 31..39: free for muon beam
   // =================================================================
   IPOSBEAM         = 40,
   // =================================================================
   ICR1DIR          = 50,
   ICRZANG          = 51,
   //                  1 = 52..55: free for cosmic ray
   // =================================================================
   IEVELED          = 61,
   INICKGA          = 62,
   IALPHSW          = 63,
   IPROTCA          = 64,
   IPIZERODALITZ    = 65,
   IEPEMGA          = 66,
   IPIZERO2GAMMA    = 67,
   IPROCON          = 68,
   IPROTBO          = 69,
   IEVMOTT          = 70,
   INEUCA           = 71,
   IXRAY            = 72,
   ICOSMIC          = 73,
   IPIBEAM          = 74,
   IPIRADCAP        = 75,
   // =================================================================
   IBMFILE          = 80,
   IBMCARD          = 81,
   IGSP             = 82,
   // =================================================================
   IEVTYPEIDEND
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMEventType
{
protected:
   G4int    fId;
   G4String fDescription;

public:
   GEMEventType(): fId(-1), fDescription("none") {}
   GEMEventType(G4int i, const char *d): fId(i), fDescription(d) {}
   ~GEMEventType() {}
   G4int     GetId()          { return fId; }
   G4String &GetDescription() { return fDescription; }
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
protected:
   G4ParticleGun                 *fParticleGun;
   G4GeneralParticleSource       *fParticleSource; // pointer to the G4GeneralParticleSource, needed for radioactive samples
   GEMPrimaryGeneratorMessenger  *fGunMessenger;
   std::vector<GEMEventType*>     fEventTypeList;
   G4int                          fEventType;
   std::vector<GEMAbsModule*>     fModules;
   GEMPhysicsList                *fPhysics;
   GEMStoppedMuonDecayGenerator  *fMuonDecayGenerator;
   GEMMuonBeamGenerator          *fMuonBeamGenerator;

   GEMAbsModule                  *fTargetModule;
   G4int                          fTGTGeometryId;
   G4RotationMatrix              *fTargetRotation;
   G4ThreeVector                  fTargetEulerAngles;
public:
   GEMPrimaryGeneratorAction();
   GEMPrimaryGeneratorAction(std::vector<GEMAbsModule*> &m,
                             GEMPhysicsList *p);
   ~GEMPrimaryGeneratorAction();

   GEMPhysicsList *GetPhysicsList() { return fPhysics; }

   void           GeneratePrimaries(G4Event*);
   G4ParticleGun *GetParticleGun() { return fParticleGun; }
   void           DumpEventTypes();
   void           SetEventType(G4int evtype) { fEventType = evtype; }
   void           AddModules(std::vector<GEMAbsModule*> &mod)
   { fModules.insert(fModules.end(), mod.begin(), mod.end()); }

   void SetMuonPolarization(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetMuonPolarization(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetMuonPolarization(v); }
   }
   void SetMuonRadiativeBranchingRatio(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetMuonRadiativeBranchingRatio(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetMuonRadiativeBranchingRatio(v); }
   }
   void SetMuonRadiativeMaxFuncValue(G4double v);
   void SetMuonRadiativeScanNBin(G4int n);
   void SetMuonRadiativeGammaSpectrumLowerEdge(G4double th);
   void SetMuonRadiativeSafetyFactor(G4double v);
   void SetStoppedMuonCenterX(G4double v)    { if (fMuonDecayGenerator) fMuonDecayGenerator->SetMuonCenterX(v); }
   void SetStoppedMuonSigmaX(G4double v)     { if (fMuonDecayGenerator) fMuonDecayGenerator->SetMuonSigmaX(v); }
   void SetStoppedMuonCenterY(G4double v)    { if (fMuonDecayGenerator) fMuonDecayGenerator->SetMuonCenterY(v); }
   void SetStoppedMuonSigmaY(G4double v)     { if (fMuonDecayGenerator) fMuonDecayGenerator->SetMuonSigmaY(v); }
   void SetStoppedMuonDepth(G4double v)      { if (fMuonDecayGenerator) fMuonDecayGenerator->SetMuonDepth(v); }
   void SetStoppedMuonDepthSigma(G4double v) { if (fMuonDecayGenerator) fMuonDecayGenerator->SetMuonDepthSigma(v); }
   void SetTargetModule(GEMAbsModule *mod)   { fTargetModule = mod; }
   void SetTGTGeometryId(G4int);
   void SetTargetSize(G4ThreeVector);
   void SetTargetAngle(G4ThreeVector);
   void SetTargetEulerAngles(G4ThreeVector);
   G4ThreeVector GetTargetEulerAngles();

   void CalculateTargetRotationMatrix(const G4ThreeVector);
   void SetTargetPosition(G4ThreeVector);
   void SetTargetMaterial(G4int);

   void SetStoppedMuonCosThetaGammaCenter(G4double v)    { if (fMuonDecayGenerator) fMuonDecayGenerator->SetCosThetaGammaCenter(v); }
   void SetStoppedMuonCosThetaGammaWidth(G4double v)     { if (fMuonDecayGenerator) fMuonDecayGenerator->SetCosThetaGammaWidth(v); }
   void SetStoppedMuonPhiGammaCenter(G4double v)         { if (fMuonDecayGenerator) fMuonDecayGenerator->SetPhiGammaCenter(v); }
   void SetStoppedMuonPhiGammaWidth(G4double v)          { if (fMuonDecayGenerator) fMuonDecayGenerator->SetPhiGammaWidth(v); }
   void SetStoppedMuonCosThetaPositronCenter(G4double v) { if (fMuonDecayGenerator) fMuonDecayGenerator->SetCosThetaPositronCenter(v); }
   void SetStoppedMuonCosThetaPositronWidth(G4double v)  { if (fMuonDecayGenerator) fMuonDecayGenerator->SetCosThetaPositronWidth(v); }
   void SetStoppedMuonPhiPositronCenter(G4double v)      { if (fMuonDecayGenerator) fMuonDecayGenerator->SetPhiPositronCenter(v); }
   void SetStoppedMuonPhiPositronWidth(G4double v)       { if (fMuonDecayGenerator) fMuonDecayGenerator->SetPhiPositronWidth(v); }
   void SetXRangeMin(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetXRangeMin(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetXRangeMin(v); }
   }
   void SetXRangeMax(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetXRangeMax(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetXRangeMax(v); }
   }
   void SetYRangeMin(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetYRangeMin(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetYRangeMin(v); }
   }
   void SetYRangeMax(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetYRangeMax(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetYRangeMax(v); }
   }
   void SetZRangeMin(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetZRangeMin(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetZRangeMin(v); }
   }
   void SetZRangeMax(G4double v)
   {
      if (fMuonDecayGenerator) { fMuonDecayGenerator->SetZRangeMax(v); }
      if (fMuonBeamGenerator) { fMuonBeamGenerator->SetZRangeMax(v); }
   }

   void SetAlphaSourceZShift(G4int i, G4double s);

   void SetMuonBeamMuonDecayMode(G4int i)              { if (fMuonBeamGenerator) fMuonBeamGenerator->SetMuonDecayMode(i); }
   void SetMuonBeamPhaseSpaceReadFromFile(G4bool b)    { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceReadFromFile(b); }
   void SetMuonBeamPhaseSpaceFileName(G4String &str)   { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceFileName(str); }
   void SetMuonBeamPhaseSpaceMeanX(G4double v)         { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMeanX(v); }
   void SetMuonBeamPhaseSpaceSigmaX(G4double v)        { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceSigmaX(v); }
   void SetMuonBeamPhaseSpaceMeanXp(G4double v)        { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMeanXp(v); }
   void SetMuonBeamPhaseSpaceSigmaXp(G4double v)       { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceSigmaXp(v); }
   void SetMuonBeamPhaseSpaceCorrX(G4double v)         { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceCorrX(v); }
   void SetMuonBeamPhaseSpaceMeanY(G4double v)         { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMeanY(v); }
   void SetMuonBeamPhaseSpaceSigmaY(G4double v)        { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceSigmaY(v); }
   void SetMuonBeamPhaseSpaceMeanYp(G4double v)        { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMeanYp(v); }
   void SetMuonBeamPhaseSpaceSigmaYp(G4double v)       { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceSigmaYp(v); }
   void SetMuonBeamPhaseSpaceCorrY(G4double v)         { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceCorrY(v); }
   void SetMuonBeamPhaseSpaceMeanZ(G4double v)         { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMeanZ(v); }
   void SetMuonBeamPhaseSpaceSigmaZ(G4double v)        { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceSigmaZ(v); }
   void SetMuonBeamPhaseSpaceMomentumShape(G4int i)    { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMomentumShape(i); }
   void SetMuonBeamPhaseSpaceMomentumMean(G4double v)  { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMomentumMean(v); }
   void SetMuonBeamPhaseSpaceMomentumSigma(G4double v) { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMomentumSigma(v); }
   void SetMuonBeamPhaseSpaceMomentumWidth(G4double v) { if (fMuonBeamGenerator) fMuonBeamGenerator->SetPhaseSpaceMomentumWidth(v); }

   GEMAbsModule  *GetModule(const char* name);
   G4int GetTGTGeometryId() const                      { return fTGTGeometryId; }

private:
   void Init();
   void GenerateSignalGammaAndPositron(G4Event*);
   void GenerateSignalPositron(G4Event*);
   void GenerateSignalGamma(G4Event*);
   void GenerateMuonBeam(G4Event*);
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
