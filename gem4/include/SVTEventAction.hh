//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef SVTEventAction_h
#define SVTEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

class G4Event;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTEventAction : public G4UserEventAction
{

public:
   SVTEventAction();
   ~SVTEventAction();

public:
   void  BeginOfEventAction(const G4Event*);
   void  EndOfEventAction(const G4Event*);
   void  SetIndex(G4int i) { idx = i; }
   void  SetEventVerbose(G4int v) {fVerbose = v;}
   G4int GetEventVerbose()        {return fVerbose;}

protected:

private:
   G4int idx;
   G4int   fSiHitsCollectionID;
   G4int   fPixelHitsCollectionID;
   G4int   fVerbose;
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
