//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
// Author: Yusuke Uchiyama
////////////////////////////////////////////////////
// 
// Class SVTPixelHit
//
//  This class describes SVT pixel hit.
//  Pixel information is simply simulated from SVTSiHit.
//  More detail simulation of pixel will be done in bartender
//
///////////////////////////////////////////////////

#ifndef SVTPixelHit_h
#define SVTPixelHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4LogicalVolume.hh"
#include <vector>
#include <set>


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTPixelHit : public G4VHit
{
public:
   SVTPixelHit();
   //SVTPixelHit(G4LogicalVolume* logVol);
   ~SVTPixelHit();
   SVTPixelHit(const SVTPixelHit &right);
   const SVTPixelHit& operator=(const SVTPixelHit &right);
   G4int operator==(const SVTPixelHit &right) const;

   inline void *operator new(size_t);
   inline void operator delete(void *aHit);

   void Draw();
   void Print();

   G4bool   FindTrack(G4int trackID) const;
   void     InsertTrackIDSet(G4int trackID){fTrackIDSet.insert(trackID);} 
   void     SetTrackID(G4int id)           {fTrackID = id;}
   void     SetLayerID(G4int id)           {fLayerID = id;}
   void     SetTime(G4double time)         {fTime = time;}
   void     SetXYZMean(G4ThreeVector xyz)  {fXYZMean = xyz;}
   void     SetEloss(G4double eloss)       {fEloss = eloss;}
   void     SetEdepTot(G4double edep)      {fEdepTot = edep;}
   void     SetIncAngle(G4double incangle) {fIncAngle = incangle;}
   void     SetPathLength(G4double length) {fPathLength = length;}
   void     AddXYZMean(G4ThreeVector xyz, G4double w)  {fXYZMean += xyz*w; 
                                                     fXYZMeanWeight += w;}
   void     AddEloss(G4double eloss)       {fEloss += eloss;}
   void     AddEdepTot(G4double edep)      {fEdepTot += edep;}
   void     AddPathLength(G4double length) {fPathLength += length;}
   void     CalculateXYZMean()             {if (fXYZMeanWeight > 0) {
                                            fXYZMean /= fXYZMeanWeight; 
                                            fXYZMeanWeight = 0;}}
   G4int    GetTrackID() const             {return fTrackID;}
   G4int    GetLayerID() const             {return fLayerID;}
   G4double GetTime() const                {return fTime;}
   G4double GetXYZMeanWeight() const       {return fXYZMeanWeight;}
   G4double GetXMean() const {return fXYZMean.x();}
   G4double GetYMean() const {return fXYZMean.y();}
   G4double GetZMean() const {return fXYZMean.z();}
   G4double GetEloss() const {return fEloss;}
   G4double GetEdepTot() const {return fEdepTot;}
   G4double GetIncAngle() const {return fIncAngle;}
   G4double GetPathLength() const {return fPathLength;}


   std::vector<G4int> &GetChipIDVec()      {return fChipIDVec;}
   std::vector<G4int> &GetPixelIndexVec()  {return fPixelIndexVec;}
   std::vector<G4double> &GetEdepVec()     {return fEdepVec;}
   std::vector<G4ThreeVector> &GetXYZPixelVec() {return fXYZPixelVec;}
   void     SetEdepAt(G4int i,G4double edep) {fEdepVec[i] = edep;}
   void     AddEdepAt(G4int i,G4double edep) {fEdepVec[i] += edep;}
   

   void     SetRot(G4RotationMatrix rot)   {fRot = rot;}
   static void SetLogVol(G4LogicalVolume* log){fgLogVol = log;}


private:
   
   std::set<G4int>          fTrackIDSet;
   G4int                    fTrackID;    // Global track ID
   G4int                    fLayerID;
   G4double                 fTime;       // Time of this hit
   G4ThreeVector            fXYZMean;    // Center position of energy deposit
   G4double                 fXYZMeanWeight;
   G4double                 fEloss;      // Energy loss in silicon
   G4double                 fEdepTot;    // Total energy deposit in silicon sensitive rgion
   G4double                 fIncAngle;   // Incident angle (angle b/w particle mom and norm of sensor)
   G4double                 fPathLength; // Path length in silicon 

   std::vector<G4int>       fChipIDVec;
   std::vector<G4int>       fPixelIndexVec; // Pixel index in the chip
   std::vector<G4ThreeVector> fXYZPixelVec; // Center position of the pixel
   std::vector<G4double>    fEdepVec;       // Energy deposit in the sensitive region


   G4RotationMatrix         fRot;        // Rotation matrix of the pixel 
   static G4LogicalVolume  *fgLogVol;    // Logical volume of pixel for drawing

};
   
typedef G4THitsCollection<SVTPixelHit> SVTPixelHitsCollection;

extern G4Allocator<SVTPixelHit> SVTPixelHitAllocator;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

inline void* SVTPixelHit::operator new(size_t)
{
   void *aHit;
   aHit = (void *) SVTPixelHitAllocator.MallocSingle();
   return aHit;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

inline void SVTPixelHit::operator delete(void *aHit)
{
   SVTPixelHitAllocator.FreeSingle((SVTPixelHit*) aHit);
}

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
