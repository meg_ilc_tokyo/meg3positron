//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMStoppedMuonDecayGeneratorAction_h
#define GEMStoppedMuonDecayGeneratorAction_h 1

#include <vector>
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"

class G4Event;
class G4VPhysicalVolume;
class GEMPrimaryGeneratorAction;
class G4DynamicParticle;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMStoppedMuonDecayGenerator
{
public:
   enum MUDECAY {
      MUDECAY_SM            = 0,
      MUDECAY_SIGNAL_EGAMMA = 1
   };

protected:
   GEMPrimaryGeneratorAction *fGeneratorAction;
   //   GEMAbsModule                  *fTargetModule;

   G4double              fMuonCenterX;
   G4double              fMuonSigmaX;
   G4double              fMuonCenterY;
   G4double              fMuonSigmaY;
   G4double              fMuonDepth;
   G4double              fMuonDepthSigma;
   double param_vec[6];
   std::vector<double> x_tot;
   std::vector<double> y_tot;
   std::vector<double> z_tot;
   double lol;
   int count;
   int row_count = 0;
   G4ThreeVector tgt_angle;
   double init_grid_zT;
   double init_grid_yT;
   double grid_zT_width;
   double grid_yT_width;
   int csv_size_z;
   int csv_size_y;
   G4ThreeVector         fTargetSize;
   G4ThreeVector         fTargetPosition;
   G4VPhysicalVolume    *fTargetVol;
   G4RotationMatrix      fTargetRotation;
   G4ThreeVector in_target_flat_x;
   G4ThreeVector in_target_flat_y;
   G4double fCosThetaGammaCenter;
   G4double fCosThetaGammaWidth;
   G4double fPhiGammaCenter;
   G4double fPhiGammaWidth;

   G4double fCosThetaPositronCenter;
   G4double fCosThetaPositronWidth;
   G4double fPhiPositronCenter;
   G4double fPhiPositronWidth;

   G4double fXRangeMin;
   G4double fXRangeMax;
   G4double fYRangeMin;
   G4double fYRangeMax;
   G4double fZRangeMin;
   G4double fZRangeMax;
   G4String fDEFORM_FILE;
   G4double fMuonPolarization;
   G4double fRadiativeBR;

public:
   GEMStoppedMuonDecayGenerator();
   GEMStoppedMuonDecayGenerator(GEMPrimaryGeneratorAction *act);
   ~GEMStoppedMuonDecayGenerator();
   //   void   ReadDatabase(const char* connection);

   void SetMuonCenterX(G4double v)    { fMuonCenterX    = v; }
   void SetMuonSigmaX(G4double v)     { fMuonSigmaX     = v; }
   void SetMuonCenterY(G4double v)    { fMuonCenterY    = v; }
   void SetMuonSigmaY(G4double v)     { fMuonSigmaY     = v; }
   void SetMuonDepth(G4double v)      { fMuonDepth      = v; }
   void SetMuonDepthSigma(G4double v) { fMuonDepthSigma = v; }
   void SetTargetSize(G4ThreeVector &v)       { fTargetSize     = v; }
   void SetTargetRotation(G4RotationMatrix *rot) { fTargetRotation = *rot; }
   void SetTargetPosition(G4ThreeVector &v)   { fTargetPosition = v; }
   void SetTargetVolume(G4VPhysicalVolume *v) { fTargetVol = v; }
   //  void SetDEFORM_FILE(G4String &t){fDEFORM_FILE=t;}
   void SetCosThetaGammaCenter(G4double v)    { fCosThetaGammaCenter    = v; }
   void SetCosThetaGammaWidth(G4double v)     { fCosThetaGammaWidth     = v; }
   void SetPhiGammaCenter(G4double v)         { fPhiGammaCenter         = v; }
   void SetPhiGammaWidth(G4double v)          { fPhiGammaWidth          = v; }
   void SetCosThetaPositronCenter(G4double v) { fCosThetaPositronCenter = v; }
   void SetCosThetaPositronWidth(G4double v)  { fCosThetaPositronWidth  = v; }
   void SetPhiPositronCenter(G4double v)      { fPhiPositronCenter      = v; }
   void SetPhiPositronWidth(G4double v)       { fPhiPositronWidth       = v; }
   void SetXRangeMin(G4double v)              { fXRangeMin              = v; }
   void SetXRangeMax(G4double v)              { fXRangeMax              = v; }
   void SetYRangeMin(G4double v)              { fYRangeMin              = v; }
   void SetYRangeMax(G4double v)              { fYRangeMax              = v; }
   void SetZRangeMin(G4double v)              { fZRangeMin              = v; }
   void SetZRangeMax(G4double v)              { fZRangeMax              = v; }
   void SetMuonPolarization(G4double v)       { fMuonPolarization = v; }
   void SetMuonRadiativeBranchingRatio(G4double v) { fRadiativeBR = v; }
   //void SetDEFORM_FILE(G4String &v);
   void SetDEFORM_FILE(G4String &v);

   void GeneratePrimaries(G4Event *anEvent, G4int decayMode,
                          G4bool positronOn = true, G4bool gammaOn = true);

   void GenerateAnSMDecay(G4Event *anEvent,
                          G4bool positronOn, G4bool gammaOn);

   G4ThreeVector MEG_to_TGT(G4ThreeVector pos, G4ThreeVector tgt_pos, G4ThreeVector tgt_angle);


protected:
   void Init();
   void GenerateASignal(G4Event *anEvent,
                        G4bool positronOn, G4bool gammaOn);

   G4ThreeVector GeneratePositionOnTheTarget();
   std::vector<std::vector<std::string> > getData(std::string fileName);

   G4bool CheckKinematics(G4DynamicParticle *positron,
                          G4DynamicParticle *gamma,
                          G4bool positronOn, G4bool gammaOn);
   G4bool GammaRequired();
   G4bool PositronRequired();
   void   GenerateGammaDirection(G4ThreeVector &dir);
   void   GeneratePositronDirection(G4ThreeVector &dir);
   void   CheckRangeCondition(G4bool positronOn, G4bool gammaOn);
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
