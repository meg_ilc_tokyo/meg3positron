//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef INCLUDE_GEMROOTIO_HH
#define INCLUDE_GEMROOTIO_HH 1

// Include files
#include "globals.hh"
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TSystem.h>

// #include "ROMETreeInfo.h"

// #include "generated/MEGUnits.h"


class G4RunManager;
class GEMRootIOMessenger;
class GEMDetectorConstruction;
class ROMETreeInfo;
class MEGUnits;
class MEGMCSVTRunHeader;
class MEGMCMixtureInfoEvent;
class MEGMCKineEvent;
class MEGMCTrackEvent;
class MEGMCTargetEvent;
class MEGMCSVTEvent;
class MEGMCSVTPixelEvent;
class MEGMCGCSWireEvent;
class MEGMCGCSConverterEvent;
class MEGMCKineSubevent;
class MEGMCTrackSubevent;
class MEGMCTargetSubevent;
class MEGMCSVTSubevent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMRootIO
{
private:
   G4RunManager           *fRunManager {nullptr};
   GEMDetectorConstruction *fDetector {nullptr};
   G4bool                  fSimFileActive {false};
   G4String                fSimFileName {"sim#.root"};
   TFile                  *fSimFile {nullptr};
   TTree                  *fSimTree {nullptr};
   G4int                   fSimCompressionLevel {1};

   G4bool                  overwrite {true};
   G4bool                  openFileFailed {false};
   G4bool                  fFillThisEvent {true};

   G4bool                  mcmixeventBranchActive {true};
   G4bool                  mckineBranchActive {true};
   G4bool                  mctrackBranchActive {true};
   G4bool                  mctargetBranchActive {true};
   G4bool                  mcsvtBranchActive {true};
   G4bool                  mcsvtpixelBranchActive {true};   
   G4bool                  mcgcswireBranchActive {true};
   G4bool                  mcgcsconverterBranchActive {true};
   G4bool                  fSaveTrajectory {true};

   G4bool                  fSevFileActive {false};
   G4String                fSevFileName {"sev#.root"};
   TFile                  *fSevFile {nullptr};
   TTree                  *fSevTree {nullptr};
   G4int                   fSevCompressionLevel {1};

   G4bool                  sevkineBranchActive {true};
   G4bool                  sevtrackBranchActive {true};
   G4bool                  sevtargetBranchActive {true};
   G4bool                  sevsvtBranchActive {true};  

   ROMETreeInfo           *fInfo;
   MEGUnits               *fUnits;
   MEGMCSVTRunHeader      *fMCSVTRunHeader;

   MEGMCMixtureInfoEvent  *fMCMixtureInfoEvent;
   MEGMCKineEvent         *fMCKineEvent;
   TClonesArray           *fMCTrackEvents;
//   MEGMCTargetEvent       *fMCTargetEvent;
   TClonesArray           *fMCTargetEvents;
   MEGMCSVTEvent          *fMCSVTEvent;
   TClonesArray           *fMCSVTPixelEvents;
   TClonesArray           *fMCGCSWireEvents;
   TClonesArray           *fMCGCSConverterEvents;
   TClonesArray           *fMCKineSubevents;
   TClonesArray           *fMCTrackSubevents;
   TClonesArray           *fMCTargetSubevents;
   TClonesArray           *fMCSVTSubevents;
   
   GEMRootIOMessenger*     fMessenger;

public:
   virtual ~GEMRootIO();

   static GEMRootIO* GetInstance();
   void SetRunManager(G4RunManager *p) { fRunManager = p; }
   void SetDetector(GEMDetectorConstruction *d) { fDetector = d; }
   G4bool Open();
   void Fill();
   void Close();
   void WriteRunHeaders();

   void SetSimFileActive(G4bool act)       { fSimFileActive = act; }
   void SetSimFileName(G4String &filename) { fSimFileName = filename; }
   void SetSimFileCompressionLevel(G4int level) { fSimCompressionLevel = level; }

   void SetSevFileActive(G4bool act)       { fSevFileActive = act; }
   void SetSevFileName(G4String &filename) { fSevFileName = filename; }
   void SetSevFileCompressionLevel(G4int level) { fSevCompressionLevel = level; }

   TTree*                 GetSimTree()            { return fSimTree; }
   TTree*                 GetSevTree()            { return fSevTree; }
   MEGUnits*              GetUnits()              { return fUnits; }
   MEGMCSVTRunHeader*     GetMCSVTRunHeader()     { return fMCSVTRunHeader; }
   MEGMCMixtureInfoEvent* GetMCMixtureInfoEvent() { return fMCMixtureInfoEvent; }
   MEGMCKineEvent*        GetMCKineEvent()        { return fMCKineEvent; }
   TClonesArray*          GetMCTrackEvents()      { return fMCTrackEvents; }
   TClonesArray*          GetMCKineSubevents()    { return fMCKineSubevents; }
   TClonesArray*          GetMCTrackSubevents()   { return fMCTrackSubevents; }
//   MEGMCTargetEvent*      GetMCTargetEvent()      { return fMCTargetEvent; }
   TClonesArray*          GetMCTargetEvents()     { return fMCTargetEvents; }
   TClonesArray*          GetMCTargetSubevents()  { return fMCTargetSubevents; }
   TClonesArray*          GetMCSVTSubevents()     { return fMCSVTSubevents; }
   MEGMCSVTEvent*         GetMCSVTEvent()         { return fMCSVTEvent; }
   TClonesArray*          GetMCSVTPixelEvents()   { return fMCSVTPixelEvents; }
   TClonesArray*          GetMCGCSWireEvents()    { return fMCGCSWireEvents; }
   TClonesArray*          GetMCGCSConverterEvents()    { return fMCGCSConverterEvents; }
   
   void SetOverwrite(G4bool a)   { overwrite              = a; }
   G4bool GetOpenFileFailed() const { return openFileFailed; }
   G4bool GetFillThisEvent() const  { return fFillThisEvent; }
   void SetFillThisEvent(G4bool a)   { fFillThisEvent           = a; }
   G4bool GetSaveTrajectory() const { return fSaveTrajectory; }
   void SetSaveTrajectory(G4bool a)   { fSaveTrajectory          = a; }


   void SetmcmixeventBranchActive(G4bool a)   { mcmixeventBranchActive   = a; }
   void SetmckineBranchActive(G4bool a)   { mckineBranchActive       = a; }
   void SetmctrackBranchActive(G4bool a)   { mctrackBranchActive      = a; }
   void SetmctargetBranchActive(G4bool a)   { mctargetBranchActive     = a; }
   void SetmcsvtBranchActive(G4bool a)      { mcsvtBranchActive        = a; }
   void SetmcsvtpixelBranchActive(G4bool a) { mcsvtpixelBranchActive   = a; }
   void SetmcgcswireBranchActive(G4bool a) { mcgcswireBranchActive   = a; }
   void SetmcgcsconverterBranchActive(G4bool a) { mcgcsconverterBranchActive   = a; }

   void SetsevkineBranchActive(G4bool a)   { sevkineBranchActive      = a; }
   void SetsevtrackBranchActive(G4bool a)   { sevtrackBranchActive     = a; }
   void SetsevtargetBranchActive(G4bool a)   { sevtargetBranchActive    = a; }
   void SetsevsvtBranchActive(G4bool a)      { sevsvtBranchActive    = a; }   

protected:
   GEMRootIO();
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
