//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMPhysicsList_h
#define GEMPhysicsList_h 1

#include "globals.hh"
#include "G4Version.hh"
#include "G4VModularPhysicsList.hh"
#include "G4VUserPhysicsList.hh"
#include "G4Cerenkov.hh"
#include "G4Scintillation.hh"
#include "G4OpAbsorption.hh"
#include "G4OpRayleigh.hh"
#include "G4OpMieHG.hh"
#include "G4OpBoundaryProcess.hh"

class G4VDecayChannel;
class GEMMuonDecayChannelWithSpin;
class GEMMuonRadiativeDecayChannelWithSpin;
class GEMPhysicsListMessenger;
class G4VPhysicsConstructor;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMPhysicsList : public G4VUserPhysicsList
{
private:
   G4Cerenkov              *fCerenkovProcess;
   G4Scintillation         *fScintillationProcess;
   G4OpAbsorption          *fAbsorptionProcess;
   G4OpRayleigh            *fRayleighScatteringProcess;
   G4OpMieHG               *fMieHGScatteringProcess;
   G4OpBoundaryProcess     *fBoundaryProcess;

   G4VDecayChannel                      *fMuonMichelChannel;
   GEMMuonDecayChannelWithSpin          *fMuonChannelWithSpin;
   GEMMuonRadiativeDecayChannelWithSpin *fMuonRadiativeChannelWithSpin;
   G4VDecayChannel                      *fMuegammaChannel;

   GEMPhysicsListMessenger *fMessenger;
   G4VPhysicsConstructor   *fParticleList;
   G4VPhysicsConstructor   *fEMPhysicsList;
   G4String                 fEmName;
   std::vector<G4VPhysicsConstructor*>  fHadronPhys;

   G4bool   fFiniteRiseTime;
public:
   GEMPhysicsList();
   ~GEMPhysicsList();

public:
   void ConstructParticle();
   void ConstructProcess();

   void SetCuts();

   //these methods Construct physics processes and register them
   void ConstructEM();
   void ConstructOp();

   void AddPhysicsList(const G4String& name);

   void AddPAIModel(const G4String& modname);
   void NewPAIModel(const G4ParticleDefinition* part,
                    const G4String& modname,
                    const G4String& procname);
   void SetBuilderList0(G4bool flagHP = false);
   void SetBuilderList1(G4bool flagHP = false);
   void SetBuilderList2();

   void Update();

   // Muon decay control
   G4VDecayChannel                      *GetMuonMichelChannel() { return fMuonMichelChannel; }
   GEMMuonDecayChannelWithSpin          *GetMuonChannelWithSpin()          { return fMuonChannelWithSpin; }
   GEMMuonRadiativeDecayChannelWithSpin *GetMuonRadiativeChannelWithSpin() { return fMuonRadiativeChannelWithSpin; }
   G4VDecayChannel                      *GetMuegammaChannel() { return fMuegammaChannel; }
   void                                  SetMuonDecayBR(G4double signalBR, G4double michelBR, G4double rmdBR);

   //for the Messenger
   void SetVerbose(G4int);

   void SetFiniteScintillationRiseTime(G4bool v) { fFiniteRiseTime = v; }
   void UpdateFiniteScintillationRiseTime()
   {
      if (fScintillationProcess) {
         fScintillationProcess->SetFiniteRiseTime(fFiniteRiseTime);
      }
   }
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
