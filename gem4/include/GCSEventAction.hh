#ifndef GCSEventAction_h
#define GCSEventAction_h 1
#include "G4UserEventAction.hh"

class GCSEventAction : public G4UserEventAction
{
   friend class GCSModule;

public:
   GCSEventAction();
   ~GCSEventAction();
   void BeginOfEventAction(const G4Event*);
   void EndOfEventAction(const G4Event*);


};

#endif
