// $
//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
//

#ifndef GEMMagneticField_H
#define GEMMagneticField_H 1

#include "globals.hh"
#include "G4MagneticField.hh"
#include "GEMMagneticFieldMessenger.hh"
class GEMMagneticFieldMessenger;
class G4UIdirectory;

class GEMMagneticField : public G4MagneticField
{
   friend class GEMMagneticFieldMessenger;

public:
   GEMMagneticField();
   ~GEMMagneticField();

   virtual void GetFieldValue(const G4double Point[3], double *Bfield) const;

private:
   GEMMagneticFieldMessenger* messenger;
   G4double By;
   G4int magnetType;
   G4int fieldType;
   G4double cobraScale;
   G4double btsScale;
   G4double bypassScale;

public:
   void     InitMEGBField();
   inline void SetField(G4double val) { By = val; }
   inline G4double GetField() const { return By; }
   G4double GetBz(G4double Point[3]);

protected:
   void     SetMagnetType(G4int v) { magnetType = v; };
   G4int    GetMagnetType() { return magnetType; };
   void     SetFieldType(G4int v) { fieldType = v; };
   G4int    GetFieldType() { return fieldType; };
   void     SetCobraScale(G4double v) { cobraScale = v; };
   G4double GetCobraScale() { return cobraScale; };
   void     SetBTSScale(G4double v) { btsScale = v; };
   G4double GetBTSScale() { return btsScale; };
   void     SetBypassScale(G4double v) { bypassScale = v; };
   G4double GetBypassScale() { return bypassScale; };
};

#endif

