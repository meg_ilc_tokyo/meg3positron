//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMSteppingAction_H
#define GEMSteppingACtion_H 1

#include <vector>
#include "globals.hh"
#include "G4UserSteppingAction.hh"
#include "GEMAbsModule.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMSteppingAction : public G4UserSteppingAction
{
private:
   std::vector<GEMAbsModule*> modules;

public:
   GEMSteppingAction();
   ~GEMSteppingAction();
   virtual void UserSteppingAction(const G4Step*);
   void AddModules(std::vector<GEMAbsModule*> &mo)
   {
      modules.insert(modules.end(), mo.begin(), mo.end());
      std::vector<GEMAbsModule*>::iterator mod;
      G4UserSteppingAction *act;
      for (mod = modules.begin(); mod != modules.end(); ++mod) {
         if ((act = (*mod)->GetSteppingAction())) {
            act->SetSteppingManagerPointer(fpSteppingManager);
         }
      }
   }
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
