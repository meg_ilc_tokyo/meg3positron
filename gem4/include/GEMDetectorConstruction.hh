//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMDetectorConstruction_h
#define GEMDetectorConstruction_h 1

#include <vector>
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "GEMAbsModule.hh"

#include "G4Region.hh"
#include "G4MagneticField.hh"
#include "G4FieldManager.hh"
#ifdef GEM4_USE_GDML
#   include "G4GDMLParser.hh"
#endif

class G4LogicalVolume;
class G4VPhysicalVolume;
class GEMDetectorMessenger;
class GEMPrimaryGeneratorAction;
class ROMESQLDataBase;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMDetectorConstruction : public G4VUserDetectorConstruction
{
private:
   std::vector<GEMAbsModule*>  fModules;
   G4UserRunAction            *fRunAct;
   G4UserEventAction          *fEveAct;
   G4UserStackingAction       *fStaAct;
   G4UserTrackingAction       *fTraAct;
   G4UserSteppingAction       *fSteAct;
   GEMDetectorMessenger       *fMessenger;
   G4MagneticField            *fMagneticField;
   G4FieldManager             *fFieldMgr;
   G4VPhysicalVolume          *fExpHall_phys;
#ifdef GEM4_USE_GDML
   G4GDMLParser               *fParser;
#endif
   G4int                       fCustomRunNumber;
   GEMPrimaryGeneratorAction  *fGeneratorAction;
   G4int                       fVerbose;
   G4bool                      fSetField;
   G4int                       fMagnetType;
   G4double                    fMagneticFieldValue;
   static G4String             fgInputFileName;
   ROMESQLDataBase            *fDB;
   G4Region                   *fFGASRegion;

public:
   GEMDetectorConstruction(G4String inputFileName);
   ~GEMDetectorConstruction();

   G4VPhysicalVolume* Construct();
   void               UpdateGeometry();
   void AddModules(std::vector<GEMAbsModule*> &mo)
   { fModules.insert(fModules.end(), mo.begin(), mo.end()); }
   G4bool FindModule(G4String name);
   void   SetRunAction(G4UserRunAction      *a) { fRunAct = a; }
   void   SetEventAction(G4UserEventAction    *a) { fEveAct = a; }
   void   SetStackingAction(G4UserStackingAction *a) { fStaAct = a; }
   void   SetTrackingAction(G4UserTrackingAction *a) { fTraAct = a; }
   void   SetSteppingAction(G4UserSteppingAction *a) { fSteAct = a; }
   G4UserRunAction      *GetRunAction() const { return fRunAct; }
   G4UserEventAction    *GetEventAction() const { return fEveAct; }
   G4UserStackingAction *GetStackingAction() const { return fStaAct; }
   G4UserTrackingAction *GetTrackingAction() const { return fTraAct; }
   G4UserSteppingAction *GetSteppingAction() const { return fSteAct; }
   G4bool WriteGDML(const char* filename);
   void   ReadDatabase(const char* connection);
   void   DisconnectDatabase();
   void   SetCustomRunNumber(G4int run) { fCustomRunNumber = run; }
   void   SetGeneratorAction(GEMPrimaryGeneratorAction *p) { fGeneratorAction = p; }
   void   SetVerbose(G4int v) { fVerbose = v; }
   void   SetBField(G4bool v) { fSetField = v; }
   void   SetMagnetType(G4int v) { fMagnetType = v; }
   void   SetMagneticFieldValue(G4double v) { fMagneticFieldValue = v; }
   void   CreateFGASRegion();
   void   SetFGASRegionCuts();

   static G4String GetInputFileName() { return fgInputFileName; }
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
