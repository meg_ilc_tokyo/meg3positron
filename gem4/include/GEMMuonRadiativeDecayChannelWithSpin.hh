//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#ifndef GEMMuonRadiativeDecayChannelWithSpin_h
#define GEMMuonRadiativeDecayChannelWithSpin_h 1

#include <vector>
#include <map>
#include "globals.hh"
#include "Randomize.hh"
#include "G4ThreeVector.hh"
#include "G4VDecayChannel.hh"

class GEMMuonRadiativeDecayChannelWithSpin : public G4VDecayChannel
{
   // Class Decription
   // This class describes radiative muon decay kinemtics, but
   // gives incorrect energy spectrum for neutrinos

public:  // With Description

   //Constructors
   GEMMuonRadiativeDecayChannelWithSpin(const G4String& theParentName,
                                        G4double        theBR);
   //  Destructor
   virtual ~GEMMuonRadiativeDecayChannelWithSpin();

public:  // With Description

   virtual G4DecayProducts *DecayIt(G4double);

   void SetPolarization(G4ThreeVector);
   const G4ThreeVector& GetPolarization() const;
   void SetRange(G4double x0, G4double x1,
                 G4double y0, G4double y1,
                 G4double z0, G4double z1);
   void SetMaxSom0(G4double v)               { maxSom0       = v; };
   void SetMaxSom0ByHand(G4bool b)           { maxSom0ByHand = b; };
   void SetScanNBin(G4int n)                 { scanNBin      = n; };
   void SetGammaSpectrumLowerEdge(G4double th) { gammaSpectrumLowerEdge = th; };
   void SetSafetyFactor(G4double v)          { safetyFactor  = v; };

   G4double fron(G4double Pmu, G4double x, G4double y,
                 G4double cthetaE, G4double cthetaG, G4double cthetaEG);

private:
   // rn3dim generates random vectors, uniformly distributed over the surface
   // of a sphere of given radius.
   // See http://wwwinfo.cern.ch/asdoc/shortwrupsdir/v131/top.html

   void rn3dim(G4double& x, G4double& y, G4double& z, G4double xlong);

   G4double atan4(G4double x, G4double y);

   G4double qsqr(G4double x, G4double y, G4double cthetaGE);

private:
   G4ThreeVector parent_polarization;
   G4double EMMU;
   G4double EMASS;

   G4double xMin;
   G4double xMax;
   G4double yMin;
   G4double yMax;
   G4double cthetaGEMin;
   G4double cthetaGEMax;

   G4double maxSom0;
   G4bool   maxSom0ByHand;
   G4int    scanNBin;
   G4double gammaSpectrumLowerEdge;
   G4double safetyFactor;

   std::map<std::vector<G4double>, G4double> maxSom0Cache;
};

inline void GEMMuonRadiativeDecayChannelWithSpin::
SetPolarization(G4ThreeVector polar)
{
   parent_polarization = polar;
}

inline const G4ThreeVector& GEMMuonRadiativeDecayChannelWithSpin::
GetPolarization() const
{
   return parent_polarization;
}

inline void GEMMuonRadiativeDecayChannelWithSpin::rn3dim(G4double& x,
                                                         G4double& y,
                                                         G4double& z,
                                                         G4double xlong)
{
   G4double a = 0.; G4double b = 0.; G4double c = 0.; G4double r = 0.;

   do {
      a = G4UniformRand() - 0.5;
      b = G4UniformRand() - 0.5;
      c = G4UniformRand() - 0.5;
      r = a * a + b * b + c * c;
   } while (r > 0.25);

   G4double rinv = xlong / (std::sqrt(r));
   x = a * rinv;
   y = b * rinv;
   z = c * rinv;

   return;
}

inline G4double GEMMuonRadiativeDecayChannelWithSpin::atan4(G4double x,
                                                            G4double y)
{
   G4double phi = 0.;

   G4double pi_l = 4.*std::atan(1.);

   if (x == 0. && y > 0.) {
      phi = 0.5 * pi_l;
   } else if (x == 0. && y < 0.) {
      phi = 1.5 * pi_l;
   } else if (y == 0. && x > 0.) {
      phi = 0.;
   } else if (y == 0. && x < 0.) {
      phi = pi_l;
   } else if (x > 0.) {
      phi = std::atan(y / x);
   } else if (x < 0.) {
      phi = std::atan(y / x) + pi_l;
   }

   return phi;
}

inline G4double GEMMuonRadiativeDecayChannelWithSpin::qsqr(G4double x,
                                                           G4double y,
                                                           G4double cthetaGE)
{
   if (EMASS == 0 || EMMU == 0) {
      std::ostringstream o;
      o << "EMASS or EMMU is not initialized";
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }
   G4double eps = EMASS / EMMU;
   G4double term0 = eps * eps;
   G4double term1 = x * ((1.0 - eps) * (1.0 - eps)) + 2.0 * eps;
   G4double beta  = std::sqrt(x * ((1.0 - eps) * (1.0 - eps)) *
                              (x * ((1.0 - eps) * (1.0 - eps)) + 4.0 * eps)) / term1;
   G4double delta = 1.0 - beta * cthetaGE;
   G4double term3 = y * (1.0 - (eps * eps));
   G4double term6 = term1 * delta * term3;
   G4double Qsqr  = (1.0 - term1 - term3 + term0 + 0.5 * term6) / ((1.0 - eps) * (1.0 - eps));
   return Qsqr;
}

#endif
