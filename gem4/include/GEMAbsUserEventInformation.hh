//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4VUserEventInformation.hh"
#include "globals.hh"

#ifndef GEMAbsUserEventInformation_h
#define GEMAbsUserEventInformation_h 1

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMAbsUserEventInformation : public G4VUserEventInformation
{
public:
   GEMAbsUserEventInformation() {}
   ~GEMAbsUserEventInformation() {}

   virtual const char *GetName() const = 0;
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
