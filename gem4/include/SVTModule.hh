//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef SVTModule_h
#define SVTModule_h 1

#include "globals.hh"
#include "GEMAbsModule.hh"
#include "G4ThreeVector.hh"
#include <vector>

class GEMPhysicsList;
class G4LogicalVolume;
class G4Material;
class G4Region;
class G4UserLimits;
class G4UIdirectory;
class SVTSiSD;
class SVTMessenger;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTModule : public GEMAbsModule
{
// Methods
public:
   SVTModule(const char* n, GEMPhysicsList *p, G4int i);
   ~SVTModule();
   
private:
   SVTModule() {}
   
public:
   void Construct(G4LogicalVolume *parent);
   void BuildAndAssemble(G4LogicalVolume* parent, 
                         std::vector<G4LogicalVolume*> &logVec, G4bool readout=false);
   GEMAbsUserEventInformation* MakeEventInformation(const G4Event* anEvent);
   GEMAbsUserTrackInformation* MakeTrackInformation(const G4Track* anTrack);
   G4bool    IsInsideFGAS() const             {return true;} // this is in FGAS
   G4int     GetNLayers() const               {return fNLayers;}
   G4double  GetLayerRAt(G4int i) const       {return fLayerR[i];}
   void      SetLayerRAt(G4int i,G4double r)  {fLayerR[i] = r; fGeometryCalculated = false;}
   G4int     GetNSensorInZAt(G4int i) const   {return fNSensorInZ[i];}
   void      SetNSensorInZAt(G4int i,G4int n) {fNSensorInZ[i] = n; fGeometryCalculated = false;}
   G4int     GetNSensorInPhiAt(G4int i) const {return fNSensorInPhi[i];}
   G4int     GetNSensors() const              {return fNSensors;}
   void      SetSlantAngle(G4double an)       {fSlantAngle = an; fGeometryCalculated = false;}
   G4double  GetSlantAngle() const            {return fSlantAngle;}
   void      SetOverlapWidth(G4double w)      {fOverlapWidth = w; fGeometryCalculated = false;}
   G4double  GetOverlapWidth() const          {return fOverlapWidth;}
   G4int     GetNPixelInZ() const             {return fNPixelInZ;}
   G4int     GetNPixelInY() const             {return fNPixelInY;}
   void      SetPixelSizeAt(G4int i, G4double v){fPixelSize[i] = v;}
   G4double  GetPixelSizeAt(G4int i)          {return fPixelSize[i];}
   void      SetSensorThickness(G4double t)   {fSensorThickness = t; /*fGeometryCalculated = false;*/}
   G4double  GetSensorThickness() const       {return fSensorThickness;}
   void      SetDepThickness(G4double t)      {fDepThickness = t; /*fGeometryCalculated = false;*/}
   G4double  GetDepThickness() const          {return fDepThickness;}

   void      SetVerboseLevel(G4int v);
   G4int     GetVerboseLevel() const          {return fVerbose;}
   void      SetCheckVolumeOverlap(G4bool v)  {fCheckVolumeOverlap = v;}
private:
   void ConstructMaterials();
   void CalculatePhiCoverage();
   void SetVisAttributes();
   void FillRunHeader();

// Data members
private:
   G4int idx;
   GEMPhysicsList       *fPhysics;
   G4UIdirectory        *fDir;
   SVTMessenger         *fMessenger;

   G4bool                fMaterialConstructed;
   G4bool                fGeometryCalculated;
   G4bool                fCheckVolumeOverlap;

   // Material
   G4Material           *fSilicon;
   G4Material           *fKapton;
   G4Material           *fAluminum;
   G4Material           *fEndRingMat;

   // Logical volumes
   std::vector<G4LogicalVolume*> fSensorArrayLog; // sensor chip array in a segment
   G4LogicalVolume      *fSensorLog;    // Si sensitive (depleted) region
   G4LogicalVolume      *fWaferLog;     // Si wafer of one chip
   G4LogicalVolume      *fPixelLog;     // Pixel (currently only used to draw)

   std::vector<G4LogicalVolume*>  fFrameLogs;     // Kapton frame for a segment
   std::vector<G4LogicalVolume*>  fFlexMotherLogs;// Flex print dummy (mother) volume
   std::vector<G4LogicalVolume*>  fFlexLogs;      // Flex print main volume
   std::vector<G4LogicalVolume*>  fTraceLogs;     // Al trace in flex print
   std::vector<G4LogicalVolume*>  fEndRingLogs; // support ring at end. log volume of a segment for each layer

   // Sensitive detectors
   SVTSiSD              *fSVTSiSD;

   // Region
   G4Region             *fSVTRegion;
   G4UserLimits         *fStepLimit;
   
   // Geometry
   G4int                 fNLayers;      // number of layers
   std::vector<G4double> fLayerR;       // radius of each layer
   std::vector<G4int>    fNSensorInZ;   // number of sensors in z direction
   std::vector<G4double> fPhiCoverage;  // phi coverage for each layer
   std::vector<G4double> fPhiCenter;    // phi center value for each layer
   std::vector<G4double> fPhiShift;     // phi shift value of each sensor position
   std::vector<G4int>    fNSensorInPhi; // number of sensors in phi direction 
   std::vector<G4double> fPhiPerSensor; // phi coverage by one sensor at each layer
   G4int                 fNSensors;     // total number of sensor
   G4double              fSlantAngle;   // sensor slant angle in phi from tangent of circle
   G4double              fOverlapWidth; // overlap of sensors b/w ladders
   G4double              fSensorThickness;// thickness of silicon wafer
   G4double              fDepThickness; // thickness of sensitive (depleted) region
   G4int                 fNPixelInZ;    // number of pixels in z direction
   G4int                 fNPixelInY;    // number of pixels in y (phi) direction
   G4double              fPixelSize[2]; // full width of pixel size ([0]: z, [1]:y)
   std::vector<G4int>    fSensorLayer;  // layer id where the sensor chip belongs to
   std::vector<G4ThreeVector> fSensorPos;// XYZ position of each sensor chip
   std::vector<G4ThreeVector> fSensorDir;// direction of normal vector of each sensor chip

   std::vector<G4double> fEndRingRMin;  // Min radius of end ring
   std::vector<G4double> fEndRingRMax;  // Max radius of end ring
   std::vector<G4double> fEndRingGrooveAngle;
   std::vector<G4double> fEndRingGrooveDepth;
   std::vector<G4double> fEndRingGrooveHeight;
   G4double              fEndRingThickness; // thickness in z
   G4double              fEndRingWidth; // width in r

   G4int                 fVerbose;      // verbose level for SVT
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
