//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef TMPModule_h
#define TMPModule_h 1

#include "globals.hh"
#include "GEMAbsModule.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TMPModule : public GEMAbsModule
{
private:
   G4int       idx;
   G4bool      materialConstructed;
   G4Material *Silicon;

public:
   TMPModule(const char* n, G4int i);
   ~TMPModule();

private:
   TMPModule() {}

public:
   void Construct(G4LogicalVolume *parent);
   GEMAbsUserEventInformation* MakeEventInformation(const G4Event* anEvent);
   GEMAbsUserTrackInformation* MakeTrackInformation(const G4Track* anTrack);
   G4bool IsInsideFGAS() const { return true; } // this is in FGAS (MACONHE) volume

private:
   void ConstructMaterials();
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
