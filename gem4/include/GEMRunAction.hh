//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMRunAction_h
#define GEMRunAction_h 1

#include <vector>
#include "globals.hh"
#include "G4UserRunAction.hh"
#include "GEMAbsModule.hh"

class G4Timer;
class G4Run;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMRunAction : public G4UserRunAction
{
private:
   std::vector<GEMAbsModule*> modules;
   G4Timer *timer;

public:
   GEMRunAction();
   ~GEMRunAction();
   void AddModules(std::vector<GEMAbsModule*> &mo)
   { modules.insert(modules.end(), mo.begin(), mo.end()); }

   void BeginOfRunAction(const G4Run* aRun);
   void EndOfRunAction(const G4Run* aRun);

protected:
   void  FillUnits();
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
