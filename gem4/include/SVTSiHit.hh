//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
// Author: Yusuke Uchiyama
////////////////////////////////////////////////////
// 
// Class SVTSiHit
//
//  This class describes GEANT hit in SVT Si sensor.
//
///////////////////////////////////////////////////

#ifndef SVTSiHit_h
#define SVTSiHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTSiHit : public G4VHit
{
public:
   SVTSiHit();
   ~SVTSiHit();
   SVTSiHit(const SVTSiHit &right);
   const SVTSiHit& operator=(const SVTSiHit &right);
   G4int operator==(const SVTSiHit &right) const;

   inline void *operator new(size_t);
   inline void operator delete(void *aHit);

   void Draw();
   void Print();
   
   void  SetIndex(G4int id)             {fIndex = id;}
   void  SetTrackID(G4int id)           {fTrackID = id;}
   void  SetParticleID(G4int id)        {fParticleID = id;}
   void  SetChipID(G4int id)            {fChipID = id;}
   void  SetTime(G4double time)         {fTime = time;}
   void  SetXYZPre(G4ThreeVector xyz)   {fXYZPre = xyz;}
   void  SetXYZPost(G4ThreeVector xyz)  {fXYZPost = xyz;}
   void  SetMomentum(G4ThreeVector mom) {fMomentum = mom;}
   void  SetEloss(G4double eloss)       {fEloss = eloss;}
   void  SetStepLength(G4double stepl)  {fStepLength = stepl;}
//    inline G4ThreeVector GetPos() { return pos; }

   G4int    GetIndex() const            {return fIndex;}
   G4int    GetTrackID() const          {return fTrackID;}
   G4int    GetParticleID() const       {return fParticleID;}
   G4int    GetChipID() const           {return fChipID;}
   G4double GetTime() const             {return fTime;}
   G4ThreeVector GetXYZPre() const      {return fXYZPre;}
   G4double GetXPre() const             {return fXYZPre.x();}
   G4double GetYPre() const             {return fXYZPre.y();}
   G4double GetZPre() const             {return fXYZPre.z();}
   G4ThreeVector GetXYZPost() const     {return fXYZPost;}
   G4double GetXPost() const            {return fXYZPost.x();}
   G4double GetYPost() const            {return fXYZPost.y();}
   G4double GetZPost() const            {return fXYZPost.z();}
   G4ThreeVector GetMomentum() const    {return fMomentum;}
   G4double GetMomentumX() const        {return fMomentum.x();}
   G4double GetMomentumY() const        {return fMomentum.y();}
   G4double GetMomentumZ() const        {return fMomentum.z();}
   G4double GetEloss() const            {return fEloss;}
   G4double GetStepLength() const       {return fStepLength;}

private:
   G4int                    fIndex;      // index of this hit in hitscollection
   G4int                    fTrackID;    // Global track ID
   G4int                    fParticleID; // ID of incident particle (Geant3 ID)
   G4int                    fChipID;
   G4double                 fTime;       // Time of this hit
   G4ThreeVector            fXYZPre;     // Position at pre-step-point
   G4ThreeVector            fXYZPost;    // Position at post-step-point 
   G4ThreeVector            fMomentum;   // Momentum at pre-step-point
//    G4ThreeVector            fMomOut;     // Momentum at exit
   G4double                 fEloss;      // Energy loss
   G4double                 fStepLength; // Step length
};

typedef G4THitsCollection<SVTSiHit> SVTSiHitsCollection;

extern G4Allocator<SVTSiHit> SVTSiHitAllocator;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

inline void* SVTSiHit::operator new(size_t)
{
   void *aHit;
   aHit = (void *) SVTSiHitAllocator.MallocSingle();
   return aHit;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

inline void SVTSiHit::operator delete(void *aHit)
{
   SVTSiHitAllocator.FreeSingle((SVTSiHit*) aHit);
}

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
