//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMEventAction_h
#define GEMEventAction_h 1

#include <vector>
//#include "GEMEventMessenger.hh"
#include "G4UserEventAction.hh"
#include "globals.hh"
#include "GEMAbsModule.hh"

class G4Event;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMEventAction : public G4UserEventAction
{
private:
   std::vector<GEMAbsModule*> modules;
   G4bool                     useGEMTrajectoryDrawing;
   G4bool                     drawOpticalPhotons;
   G4double                   drawEnergyMin;

public:
   GEMEventAction();
   ~GEMEventAction();
   void AddModules(std::vector<GEMAbsModule*> &mo)
   { modules.insert(modules.end(), mo.begin(), mo.end()); }

   void BeginOfEventAction(const G4Event*);
   void EndOfEventAction(const G4Event*);

   void SetUseGEMTrajectoryDrawing(G4bool b) { useGEMTrajectoryDrawing = b; }
   void SetDrawOpticalPhotons(G4bool b) { drawOpticalPhotons = b; }
   void SetDrawEnergyMin(G4double th)   { drawEnergyMin = th; }
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
