#ifndef MEGVPVPARAMETERISATION_HH
#define MEGVPVPARAMETERISATION_HH

#include "G4VPVParameterisation.hh"

class G4TwistedTubs;

class GEMVPVParameterisation : public G4VPVParameterisation
{
public:

   GEMVPVParameterisation() {}
   virtual ~GEMVPVParameterisation() {}

public:  // with description

   virtual void ComputeDimensions(G4TwistedTubs &,
                                  const G4int,
                                  const G4VPhysicalVolume *) const {}
};

#endif
