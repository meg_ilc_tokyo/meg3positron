#ifndef GCSStackingAction_h
#define GCSStackingAction_h 1
#include "G4UserStackingAction.hh"

class GCSStackingAction : public G4UserStackingAction
{
   friend class GCSModule;
public:
   GCSStackingAction();
   virtual ~GCSStackingAction();
   G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
   void NewStage();
   void PrepareNewEvent();

};

#endif
