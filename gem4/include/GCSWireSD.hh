//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef GCSWireSD_h
#define GCSWireSD_h 1

#include <vector>
#include "G4VSensitiveDetector.hh"

#include <TClonesArray.h>

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class GCSWireSD : public G4VSensitiveDetector
{
public:

   GCSWireSD(G4String name);
   ~GCSWireSD(){};

   G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*);
   void Initialize(G4HCofThisEvent* HCE);

private:
   TClonesArray *pWireEvent_arr;
   std::map<G4String, G4int> map_hitID;
   G4int hitID;

};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
