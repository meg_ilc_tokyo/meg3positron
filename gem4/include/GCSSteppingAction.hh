#ifndef GCSSteppingAction_h
#define GCSSteppingAction_h 1
#include "globals.hh"
#include "G4UserSteppingAction.hh"

class GCSSteppingAction : public G4UserSteppingAction
{
   friend class GCSModule;

public:
   GCSSteppingAction();
   ~GCSSteppingAction();
   void UserSteppingAction(const G4Step*);


};

#endif
