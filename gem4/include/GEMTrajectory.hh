//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMTrajectory_h
#define GEMTrajectory_h 1

#include "G4Trajectory.hh"
#include "G4Allocator.hh"
#include "G4ios.hh"
#include "globals.hh"
#include "G4ParticleDefinition.hh"
#include "G4TrajectoryPoint.hh"
#include "G4Track.hh"
#include "G4Step.hh"

class G4Polyline;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMTrajectory : public G4Trajectory
{
public:
   GEMTrajectory();
   GEMTrajectory(const G4Track* aTrack);
   GEMTrajectory(GEMTrajectory &);
   virtual ~GEMTrajectory();

   virtual void DrawTrajectory() const;
   virtual void DrawTrajectory(G4int i_mode = 0) const;

   inline void* operator new (size_t);
   inline void  operator delete (void*);

   void SetDrawTrajectory(G4bool b) {drawit = b;}

private:
   G4bool drawit;
   G4ParticleDefinition* particleDefinition;
};

extern G4Allocator<GEMTrajectory> GEMTrajectoryAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* GEMTrajectory::operator new (size_t)
{
   void* aTrajectory;
   aTrajectory = (void*)GEMTrajectoryAllocator.MallocSingle();
   return aTrajectory;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void GEMTrajectory::operator delete (void* aTrajectory)
{
   GEMTrajectoryAllocator.FreeSingle((GEMTrajectory*)aTrajectory);
}

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
