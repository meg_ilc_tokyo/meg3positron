//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef BMUModule_h
#define BMUModule_h 1

#include "globals.hh"
#include "GEMAbsModule.hh"
#include "BMUMessenger.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class BMUModule : public GEMAbsModule
{
   friend class BMUMessenger;

private:
   G4int         idx;
   G4bool        materialConstructed;
   BMUMessenger *messenger;
   G4Material   *Air;
   G4Material   *Vacuum;
   G4Material   *Lead;
   G4Material   *mabmumy1; // Mylar for degrader
   G4Material   *mabmumy2; // Mylar for vac.window
   G4Material   *mabmupe;  // Polyethylene
   G4Material   *MAAL;
   G4Material   *MASUS;
   G4double      TDGR1;

public:
   BMUModule(const char* n, G4int i);
   ~BMUModule();

private:
   BMUModule() {}

public:
   void Construct(G4LogicalVolume *parent);
   GEMAbsUserEventInformation* MakeEventInformation(const G4Event* anEvent);
   GEMAbsUserTrackInformation* MakeTrackInformation(const G4Track* anTrack);
   void ReadDatabase(ROMESQLDataBase *db, G4int run);

   void SetDegraderThickness(G4double d) { TDGR1 = d; }

private:
   void ConstructMaterials();
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
