//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "GEMAbsUserTrackInformation.hh"
#include "globals.hh"

#ifndef TMPUserTrackInformation_h
#define TMPUserTrackInformation_h 1


class TMPUserTrackInformation : public GEMAbsUserTrackInformation
{
public:
   G4double energyDeposit;

public:
   TMPUserTrackInformation();
   ~TMPUserTrackInformation();

   const char *GetName() const { return "TMP"; }

   inline void Print()const {};

   GEMAbsUserTrackInformation *Clone() const { return 0; }
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
