//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMTrackingAction_h
#define GEMTrackingAction_h 1

#include <vector>
#include "globals.hh"
#include "G4UserTrackingAction.hh"
#include "GEMUserEventInformation.hh"
#include "GEMAbsModule.hh"

class G4VProcess;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMTrackingAction : public G4UserTrackingAction
{
private:
   std::vector<GEMAbsModule*> modules;

public:
   GEMTrackingAction();
   ~GEMTrackingAction() {};
   void AddModules(std::vector<GEMAbsModule*> &mo)
   {
      modules.insert(modules.end(), mo.begin(), mo.end());
      std::vector<GEMAbsModule*>::iterator mod;
      G4UserTrackingAction *act;
      for (mod = modules.begin(); mod != modules.end(); ++mod) {
         if ((act = (*mod)->GetTrackingAction())) {
            act->SetTrackingManagerPointer(fpTrackingManager);
         }
      }
   }

   void PreUserTrackingAction(const G4Track*);
   void PostUserTrackingAction(const G4Track*);

private:
   G4bool IsDecayProcess(const G4VProcess* process);
   G4bool IsPionZeroDecayProcess(GEMUserEventInformation* eveInfo, const G4Track* aTrack);
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
