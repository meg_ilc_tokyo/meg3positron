//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef TARMessenger_h
#define TARMessenger_h 1

#include "G4UImessenger.hh"
#include "G4OpticalSurface.hh"
#include "globals.hh"

class TARModule;

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAString;

// Messenger class for 'passive' target
// See also GEMPrimaryGeneratorMessenger for some common parameters

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TARMessenger: public G4UImessenger
{
private:
   TARModule            *fModule;
   G4UIdirectory        *fDir;
   G4UIdirectory        *fGeomDir;

   G4UIcmdWithAnInteger      *fNumberOfHolesCmd;
   G4UIcmdWithADoubleAndUnit *fHoleRadiusCmd;
   G4UIcmdWithAnInteger      *fTGTGeometryIdCmd;
   G4UIcmdWithAnInteger      *fVerboseCmd; // command to set verbose level

public:
   TARMessenger(TARModule*);
   ~TARMessenger();

   void SetNewValue(G4UIcommand*, G4String);
   G4String GetCurrentValue(G4UIcommand* command);

private:
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
