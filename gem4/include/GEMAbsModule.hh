//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
// Abstract flass of sub-systems in gem.

#ifndef GEMAbsModule_h
#define GEMAbsModule_h 1

#include "globals.hh"
#include "G4String.hh"

class G4Event;
class G4Track;
class G4UserRunAction;
class G4UserEventAction;
class G4UserStackingAction;
class G4UserSteppingAction;
class G4UserTrackingAction;
class G4LogicalVolume;
class GEMAbsUserEventInformation;
class GEMAbsUserTrackInformation;
class G4Material;
class ROMESQLDataBase;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMAbsModule
{
public:
   GEMAbsModule(const char* n)
      : fModuleName(n)
      , fRunAction(nullptr)
      , fEventAction(nullptr)
      , fStackingAction(nullptr)
      , fSteppingAction(nullptr)
      , fTrackingAction(nullptr)
      , fMACONHE(nullptr)
   {}
   virtual ~GEMAbsModule() {}

protected:
   GEMAbsModule()
      : fModuleName("nobody")
      , fRunAction(nullptr)
      , fEventAction(nullptr)
      , fStackingAction(nullptr)
      , fSteppingAction(nullptr)
      , fTrackingAction(nullptr)
      , fMACONHE(nullptr)
   {}

public:
   virtual void Construct(G4LogicalVolume *parent) = 0;
   G4String              &GetName()             { return fModuleName; }
   G4UserRunAction       *GetRunAction()        { return fRunAction; }
   G4UserEventAction     *GetEventAction()      { return fEventAction; }
   G4UserStackingAction  *GetStackingAction()   { return fStackingAction; }
   G4UserSteppingAction  *GetSteppingAction()   { return fSteppingAction; }
   G4UserTrackingAction  *GetTrackingAction()   { return fTrackingAction; }
   virtual GEMAbsUserEventInformation *MakeEventInformation(const G4Event* anEvent) = 0;
   virtual GEMAbsUserTrackInformation *MakeTrackInformation(const G4Track* anTrack) = 0;
   virtual G4bool         IsInsideFGAS() const { return false; }
   virtual G4LogicalVolume *ConstructFGas() { return nullptr; } // MAG module makes FGAS volume
   void                   SetMACONHE(G4Material *ma) { fMACONHE = ma; }
   virtual void           ReadDatabase(ROMESQLDataBase * /*db*/, G4int /*run*/) {}

protected:
   G4String              fModuleName;
   G4UserRunAction      *fRunAction;
   G4UserEventAction    *fEventAction;
   G4UserStackingAction *fStackingAction;
   G4UserSteppingAction *fSteppingAction;
   G4UserTrackingAction *fTrackingAction;

   // shared materials
   G4Material           *fMACONHE; // He gas with air admixture
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
