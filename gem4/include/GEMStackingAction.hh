//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMStackingAction_H
#define GEMStackingAction_H 1

#include <vector>
#include "globals.hh"
#include "G4UserStackingAction.hh"
#include "GEMAbsModule.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMStackingAction : public G4UserStackingAction
{
private:
   std::vector<GEMAbsModule*> modules;
   static G4bool fgReClassificationRequested;
public:
   GEMStackingAction();
   ~GEMStackingAction();
   void AddModules(std::vector<GEMAbsModule*> &mo)
   { modules.insert(modules.end(), mo.begin(), mo.end()); }

   G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
   void NewStage();
   void PrepareNewEvent();

   static void RequestReClassification(G4bool v) {fgReClassificationRequested = v;}
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
