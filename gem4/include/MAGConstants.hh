#ifndef MAGConstants_h
#define MAGConstants_h 1

#include "G4SystemOfUnits.hh"
#include "globals.hh"

namespace
{
const G4double ALMAG1 = 64.5 * cm;
const G4double ALMAG2 = 43.5 * cm;
const G4double ALMAG3 = 17.0 * cm;
const G4double ALMAG4 = 34.0 * cm;
const G4double kCOBRAVolumeRDCExtension = 80 * mm; // COBRA volume downstream extension

const G4double R1MAG1 = 41.0 * centimeter;
const G4double R2MAG1 = 57.5 * centimeter;
const G4double R1MAG3 = 35.5 * centimeter;
const G4double R1MAG4 = 30.0 * centimeter;
}

#endif
