//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include <vector>
#include <map>
#include "GEMAbsUserEventInformation.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

#ifndef TARUserEventInformation_h
#define TARUserEventInformation_h 1

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TARUserEventInformation : public GEMAbsUserEventInformation
{
public:
   TARUserEventInformation();
   ~TARUserEventInformation();

   const char *GetName() const { return "TAR"; }

   inline void Print() const {};

   G4ThreeVector vecIn;  // vector when the primary positron enter the target
   G4ThreeVector vecOut; // vector when the primary positron exit  the target
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
