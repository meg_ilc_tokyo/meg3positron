//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef GCSMessenger_h
#define GCSMessenger_h 1

#include "G4UImessenger.hh"
#include "G4OpticalSurface.hh"
#include "globals.hh"

class GCSModule;

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAString;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class GCSMessenger: public G4UImessenger
{
private:
   static GCSModule    *fgModule;

   G4UIdirectory *fGeomDir;
   G4UIdirectory *fTrackingDir;
   G4UIdirectory *fMaterialDir;
   G4UIdirectory *fSensorDir;
   G4UIdirectory *fSupportDir;
   G4UIdirectory *fDigiDir;
   G4UIdirectory *fVisDir;

   G4UIcmdWithAnInteger      *fVerboseCmd;
   G4UIcmdWithABool          *fCheckOverlapCmd;
   G4UIcmdWithADoubleAndUnit *fGasRMinCmd;
   G4UIcmdWithADoubleAndUnit *fGasRMaxCmd;
   G4UIcmdWithADoubleAndUnit *fGasLengthCmd;
   G4UIcmdWithADoubleAndUnit *fLYSOThicknessCmd;
   G4UIcmdWithAnInteger      *fNLYSOLayerCmd;
   G4UIcmdWithADoubleAndUnit *fLayerRadius0Cmd;
   G4UIcmdWithADoubleAndUnit *fLayerRadius1Cmd;
   G4UIcmdWithADoubleAndUnit *fLayerRadius2Cmd;
   G4UIcmdWithADoubleAndUnit *fLayerRadius3Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsInZ0Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsInZ1Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsInZ2Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsInZ3Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsPerModuleInPhi0Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsPerModuleInPhi1Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsPerModuleInPhi2Cmd;
   G4UIcmdWithAnInteger      *fNSegmentsPerModuleInPhi3Cmd;

   G4UIcmdWithADouble        *fNCellPhi0Cmd;
   G4UIcmdWithADouble        *fNCellPhi1Cmd;
   G4UIcmdWithADouble        *fNCellPhi2Cmd;
   G4UIcmdWithADouble        *fNCellPhi3Cmd;
   G4UIcmdWithAnInteger      *fNSublayersCmd;
   G4UIcmdWithADoubleAndUnit *fCellDrCmd;

   

public:
   GCSMessenger(GCSModule*);
   ~GCSMessenger();

   void SetNewValue(G4UIcommand*, G4String);

private:

};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
