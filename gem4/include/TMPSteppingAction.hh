//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef TMPSteppingAction_H
#define TMPSteppingACtion_H 1

#include "globals.hh"
#include "G4UserSteppingAction.hh"

class TMPSteppingMessenger;
class G4Material;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TMPSteppingAction : public G4UserSteppingAction
{
private:
   G4int      idx;
   G4Material *silicon;

public:
   TMPSteppingAction();
   ~TMPSteppingAction();
   virtual void UserSteppingAction(const G4Step*);
   void SetIndex(G4int i) { idx = i; }
   void SetSilicon(G4Material *mat) { silicon = mat; }
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
