#ifndef GEMMuonDecayChannelWithSpin_hh
#define GEMMuonDecayChannelWithSpin_hh 1

#include "G4PhysicalConstants.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4MuonDecayChannel.hh"

class GEMMuonDecayChannelWithSpin : public G4MuonDecayChannel
{
   // Class Decription
   // This class describes muon decay kinemtics.
   // This version assumes V-A coupling with 1st order radiative correctons,
   //              the standard model Michel parameter values, but
   //              gives incorrect energy spectrum for neutrinos

public:  // With Description

   //Constructors
   GEMMuonDecayChannelWithSpin(const G4String& theParentName,
                               G4double theBR);
   //  Destructor
   virtual ~GEMMuonDecayChannelWithSpin();

public:  // With Description

   virtual G4DecayProducts *DecayIt(G4double);

   void SetPolarization(G4ThreeVector);
   const G4ThreeVector& GetPolarization() const;
   void SetRange(G4double x0, G4double x1);

private:

   G4ThreeVector parent_polarization;
   G4double xMin;
   G4double xMax;

// Radiative Correction Factors

   G4double F_c(G4double x, G4double x0);
   G4double F_theta(G4double x, G4double x0);
   G4double R_c(G4double x);

   G4double EMMU;
   G4double EMASS;

};

inline void GEMMuonDecayChannelWithSpin::SetPolarization(G4ThreeVector polar)
{
   parent_polarization = polar;
}

inline const G4ThreeVector& GEMMuonDecayChannelWithSpin::GetPolarization() const
{
   return parent_polarization;
}

inline G4double GEMMuonDecayChannelWithSpin::F_c(G4double x, G4double x0)
{
   G4double omega = std::log(EMMU / EMASS);

   G4double f_c;

   f_c = (5. + 17.*x - 34.*x * x) * (omega + std::log(x)) - 22.*x + 34.*x * x;
   f_c = (1. - x) / (3.*x * x) * f_c;
   f_c = (6. - 4.*x) * R_c(x) + (6. - 6.*x) * std::log(x) + f_c;
   f_c = (fine_structure_const / twopi) * (x * x - x0 * x0) * f_c;

   return f_c;
}

inline G4double GEMMuonDecayChannelWithSpin::F_theta(G4double x, G4double x0)
{
   G4double omega = std::log(EMMU / EMASS);

   G4double f_theta;

   f_theta = (1. + x + 34 * x * x) * (omega + std::log(x)) + 3. - 7.*x - 32.*x * x;
   f_theta = f_theta + ((4.*(1. - x) * (1. - x)) / x) * std::log(1. - x);
   f_theta = (1. - x) / (3.*x * x) * f_theta;
   f_theta = (2. - 4.*x) * R_c(x) + (2. - 6.*x) * std::log(x) - f_theta;
   f_theta = (fine_structure_const / twopi) * (x * x - x0 * x0) * f_theta;

   return f_theta;
}

#endif
