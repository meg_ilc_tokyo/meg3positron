//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <vector>
#include <map>
#include "GEMAbsUserEventInformation.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMUserPrimaryVertexInformation.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

#ifndef GEMUserEventInformation_h
#define GEMUserEventInformation_h 1


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMUserEventInformation : public GEMAbsUserEventInformation
{
public:
   GEMUserEventInformation();
   ~GEMUserEventInformation();
   const char* GetName() const { return "GEM"; }

   inline void Print() const {};
   void AddUserEventInformation(GEMAbsUserEventInformation* ui)
   {
      modules.push_back(ui);
   }

   GEMAbsUserEventInformation *GetModule(const char* name);
   GEMAbsUserEventInformation *GetModuleAt(G4int i) { return modules[i]; }
   void AddPrimaryInfo(const G4Track* aTrack);
   void AddSecondaryInfo(const G4Track* aTrack);
   void AddTrackInfo(const GEMUserTrackInformation &info);

   G4int GetPrimaryInfoSize()   { return primaryInfos.size(); }
   G4int GetSecondaryInfoSize() { return secondaryInfos.size(); }
   G4int GetTrackInfoSize()     { return trackInfos.size(); }

   G4int GetTrackInfoIndexFromID(G4int id);
   G4int GetPrimaryInfoIndexFromID(G4int id);
   G4int GetSecondaryInfoIndexFromID(G4int id);

private:
   std::vector<GEMAbsUserEventInformation*> modules;

public:
   G4double timeOfInterest;

   // primaryInfo, elements are kept even after the vertex is deleted
   std::vector<GEMUserPrimaryVertexInformation*> primaryInfos;
   std::map<G4int /*trackID*/, G4int /*index*/>  primaryLookUp;

   // sedondaryInfo, elements are kept even after the vertex is deleted
   std::vector<GEMUserPrimaryVertexInformation*> secondaryInfos;
   std::map<G4int /*trackID*/, G4int /*index*/>  secondaryLookUp;

   // trackInfo, elements are kept even after the track is deleted
   std::vector<GEMUserTrackInformation*>        trackInfos;
   std::map<G4int /*trackID*/, G4int /*index*/> trackLookUp;

   // radiation from the primary positron
   std::vector<GEMUserPrimaryVertexInformation*> radiationInfos;
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
