//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef SVTDummySD_h
#define SVTDummySD_h 1

#include "G4VSensitiveDetector.hh"

class G4Step;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTDummySD : public G4VSensitiveDetector
{
private:

public:
   SVTDummySD();
   ~SVTDummySD() {}

   void Initialize(G4HCofThisEvent*) {}
   G4bool ProcessHits(G4Step*, G4TouchableHistory*) {return false;}
   void EndOfEvent(G4HCofThisEvent*) {}
   void clear() {}
   void DrawAll() {}
   void PrintAll() {}

protected:

};
SVTDummySD::SVTDummySD()
: G4VSensitiveDetector("/svt/dummySD")
{}
#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
