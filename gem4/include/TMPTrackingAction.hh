//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef TMPTrackingAction_h
#define TMPTrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "globals.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TMPTrackingAction : public G4UserTrackingAction
{
private:
   G4int idx;

public:
   TMPTrackingAction();
   ~TMPTrackingAction() {};

   void PreUserTrackingAction(const G4Track*);
   void PostUserTrackingAction(const G4Track*);
   void SetIndex(G4int i) { idx = i; }
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
