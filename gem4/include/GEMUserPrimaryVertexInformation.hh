//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <vector>
#include "globals.hh"
#include "G4VUserPrimaryVertexInformation.hh"

#ifndef GEMUserPrimaryVertexInformation_h
#define GEMUserPrimaryVertexInformation_h 1

class G4Track;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMUserPrimaryVertexInformation : public G4VUserPrimaryVertexInformation
{
public:
   GEMUserPrimaryVertexInformation();
   GEMUserPrimaryVertexInformation(const G4Track *aTrack);
   GEMUserPrimaryVertexInformation(const GEMUserPrimaryVertexInformation &info);
   ~GEMUserPrimaryVertexInformation();

   inline void Print() const {};

public:
   G4int    vtxid; // GEANT vertex id
   G4int    vtype; // type of the primary
   G4int    gcode; // geant particle code
   G4double xvtx;  // x of primary
   G4double yvtx;  // y of primary
   G4double zvtx;  // z of primary
   G4double tvtx;  // time of primary
   G4double xmom;  // x of momentum of particle
   G4double ymom;  // y of momentum of particle
   G4double zmom;  // z of momentum of particle
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
