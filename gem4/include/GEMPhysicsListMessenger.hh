//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMPhysicsListMessenger_h
#define GEMPhysicsListMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class GEMPhysicsList;
class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAString;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMPhysicsListMessenger: public G4UImessenger
{
public:
   GEMPhysicsListMessenger(GEMPhysicsList*);
   ~GEMPhysicsListMessenger();

   void SetNewValue(G4UIcommand*, G4String);

private:
   GEMPhysicsList*          fPhysicsList;

   G4UIdirectory*           fPhysDir;
   G4UIcmdWithAnInteger    *fVerboseCmd;
   G4UIcmdWithAString*      fAddPhysicsListCmd;
   G4UIcmdWithoutParameter *fAddPAICmd;
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
