//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include <vector>
#include "GEMAbsUserEventInformation.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

#ifndef TMPUserEventInformation_h
#define TMPUserEventInformation_h 1

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TMPUserEventInformation : public GEMAbsUserEventInformation
{
public:
   G4double energyDeposit;

public:
   TMPUserEventInformation();
   ~TMPUserEventInformation();

   const char *GetName() const { return "TMP"; }

   inline void Print() const {};
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
