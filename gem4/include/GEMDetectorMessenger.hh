//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMDetectorMessenger_h
#define GEMDetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class GEMDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAnInteger;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
class G4UIcmdWithADoubleAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMDetectorMessenger: public G4UImessenger
{
public:
   GEMDetectorMessenger(GEMDetectorConstruction*);
   ~GEMDetectorMessenger();

   void SetNewValue(G4UIcommand*, G4String);

private:
   GEMDetectorConstruction   *fDetector;

   G4UIdirectory             *fGeomDir;
   G4UIdirectory             *fVisDir;
   G4UIcmdWithoutParameter   *fUpdateCmd;
   G4UIcmdWithAString        *fWriteGDMLCmd;
   G4UIcmdWithAnInteger      *fCustomRunNumberCmd;
   G4UIcmdWithAnInteger      *fSPXConfIdCmd;
   G4UIcmdWithAString        *fReadDatabaseCmd;
   G4UIcmdWithoutParameter   *fDisconnectDatabaseCmd;
   G4UIcmdWithAnInteger      *fDetectorVerboseCmd;
   G4UIcmdWithABool          *fUseGEMTrajectoryDrawingCmd;
   G4UIcmdWithABool          *fDrawOpticalPhotonsCmd;
   G4UIcmdWithADoubleAndUnit *fDrawEnergyMinCmd;
   G4UIcmdWithABool          *fSetMagneticFieldCmd;
   G4UIcmdWithAnInteger      *fMagnetTypeCmd;
   G4UIcmdWithADoubleAndUnit *fMagneticFieldValueCmd;
   
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
