//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef MAGModule_h
#define MAGModule_h 1

#include "globals.hh"
#include "GEMAbsModule.hh"
#include "G4RotationMatrix.hh"

class G4LogicalVolume;
class G4UIdirectory;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class MAGModule : public GEMAbsModule
{
public:
   MAGModule(const char* name);
   ~MAGModule();

private:
   G4Material       *Air;
   G4Material       *Aluminium;
   G4Material       *Iron;
   G4Material       *CobraConductor;
   G4RotationMatrix *rotzr;
   static G4int      fgMagnetType;
   
private:
   MAGModule() {}

public:
   void Construct(G4LogicalVolume *parent);
   GEMAbsUserEventInformation* MakeEventInformation(const G4Event* anEvent);
   GEMAbsUserTrackInformation* MakeTrackInformation(const G4Track* anTrack);
   static void SetMagnetType(G4int type) { fgMagnetType = type; }
   
private:
   void ConstructMaterials();
   void BuildMagnet(G4LogicalVolume *parent);
   void BuildCOBRA(G4LogicalVolume *parent);   

   G4LogicalVolume *ConstructFGas();
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
