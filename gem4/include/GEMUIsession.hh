#ifndef GEMUIsession_h
#define GEMUIsession_h 1

#include <fstream>
#include "G4String.hh"
#include "G4UIsession.hh"

class GEMUIsession : public G4UIsession
{
protected:
   std::ofstream *outFile;
   std::ofstream *errFile;
   G4String       outFileName;
   G4String       errFileName;

public:
   GEMUIsession();
   virtual ~GEMUIsession();

   G4int  ReceiveG4cout(const G4String &coutString);
   G4int  ReceiveG4cerr(const G4String &cerrString);
   G4bool OpenOutFile(const char* filename);
   void   CloseOutFile();
   G4bool OpenErrFile(const char* filename);
   void   CloseErrFile();
};

#endif
