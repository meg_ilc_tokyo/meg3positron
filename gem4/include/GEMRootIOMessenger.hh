//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef GEMRootIOMessenger_h
#define GEMRootIOMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class GEMRootIO;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class GEMRootIOMessenger: public G4UImessenger
{
public:
   GEMRootIOMessenger(GEMRootIO*);
   ~GEMRootIOMessenger();

   void SetNewValue(G4UIcommand*, G4String);

private:
   GEMRootIO                 *rootIO;
   G4UIdirectory             *outDir;

   G4UIdirectory             *simDir;
   G4UIdirectory             *simBrDir;
   G4UIcmdWithABool          *overwriteCmd;
   G4UIcmdWithABool          *simFileActiveCmd;
   G4UIcmdWithAString        *simFileNameCmd;
   G4UIcmdWithAnInteger      *simCompressionLevelCmd;

   G4UIcmdWithABool          *mcmixeventBranchActiveCmd;
   G4UIcmdWithABool          *mckineBranchActiveCmd;
   G4UIcmdWithABool          *mctrackBranchActiveCmd;
   G4UIcmdWithABool          *mctargetBranchActiveCmd;

   G4UIdirectory             *sevDir;
   G4UIdirectory             *sevBrDir;
   G4UIcmdWithABool          *sevFileActiveCmd;
   G4UIcmdWithAString        *sevFileNameCmd;
   G4UIcmdWithAnInteger      *sevCompressionLevelCmd;

   G4UIcmdWithABool          *sevkineBranchActiveCmd;
   G4UIcmdWithABool          *sevtrackBranchActiveCmd;
   G4UIcmdWithABool          *sevtargetBranchActiveCmd;

   G4UIcmdWithABool          *saveTrajectoryCmd;
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
