//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef GEMPrimaryGeneratorMessenger_h
#define GEMPrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class GEMPrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMPrimaryGeneratorMessenger: public G4UImessenger
{
private:
   GEMPrimaryGeneratorAction *GEMAction;
   G4UIdirectory             *targetDir;
   G4UIdirectory             *genDir;
   G4UIdirectory             *muonDir;
   G4UIdirectory             *radiativeDir;
   G4UIdirectory             *stoppedMuonDir;
   G4UIdirectory             *muonBeamDir;
   G4UIdirectory             *muonBeamPhaseSpaceDir;
   G4UIcmdWithoutParameter   *listCmd;
   G4UIcmdWithAnInteger      *evtypeCmd;
   G4UIcmdWithAnInteger      *fTGTGeometryIdCmd;

   G4UIcmdWithADoubleAndUnit *stoppedMuonCenterXCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonSigmaXCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonCenterYCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonSigmaYCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonDepthCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonDepthSigmaCmd;

   G4UIcmdWithoutParameter   *stoppedMuonMaximizeGammaAngleCmd;
   G4UIcmdWithoutParameter   *stoppedMuonMaximizePositronAngleCmd;
   G4UIcmdWithoutParameter   *stoppedMuonInterestRangeCmd;
   G4UIcmdWithoutParameter   *stoppedMuonExtendedRangeCmd;

   G4UIcmdWithADouble        *stoppedMuonCosThetaGammaCenterCmd;
   G4UIcmdWithADouble        *stoppedMuonCosThetaGammaWidthCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonPhiGammaCenterCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonPhiGammaWidthCmd;
   G4UIcmdWithADouble        *stoppedMuonCosThetaPositronCenterCmd;
   G4UIcmdWithADouble        *stoppedMuonCosThetaPositronWidthCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonPhiPositronCenterCmd;
   G4UIcmdWithADoubleAndUnit *stoppedMuonPhiPositronWidthCmd;
   G4UIcmdWithADouble        *stoppedMuonXRangeMinCmd;
   G4UIcmdWithADouble        *stoppedMuonXRangeMaxCmd;
   G4UIcmdWithADouble        *stoppedMuonYRangeMinCmd;
   G4UIcmdWithADouble        *stoppedMuonYRangeMaxCmd;
   G4UIcmdWithADouble        *stoppedMuonZRangeMinCmd;
   G4UIcmdWithADouble        *stoppedMuonZRangeMaxCmd;

   G4UIcmdWithADouble        *muonPolarizationCmd;
   G4UIcmdWithADouble        *muonRadiativeBranchingRatioCmd;
   G4UIcmdWithADouble        *muonRadiativeMaxFuncValueCmd;
   G4UIcmdWithAnInteger      *muonRadiativeScanNBinCmd;
   G4UIcmdWithADoubleAndUnit *muonRadiativeGammaSpectrumLowerEdgeCmd;
   G4UIcmdWithADouble        *muonRadiativeSafetyFactorCmd;
   G4UIcmdWith3VectorAndUnit *targetSizeCmd;
   G4UIcmdWith3VectorAndUnit *targetAngleCmd;
   G4UIcmdWith3VectorAndUnit *targetPositionCmd;
   G4UIcmdWithAnInteger      *targetMaterialCmd;

   G4UIcmdWithAnInteger      *muonBeamMuonDecayModeCmd;
   G4UIcmdWithABool          *muonBeamPhaseSpaceReadFromFileCmd;
   G4UIcmdWithAString        *muonBeamPhaseSpaceFileNameCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMeanXCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceSigmaXCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMeanXpCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceSigmaXpCmd;
   G4UIcmdWithADouble        *muonBeamPhaseSpaceCorrXCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMeanYCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceSigmaYCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMeanYpCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceSigmaYpCmd;
   G4UIcmdWithADouble        *muonBeamPhaseSpaceCorrYCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMeanZCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceSigmaZCmd;
   G4UIcmdWithAnInteger      *muonBeamPhaseSpaceMomentumShapeCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMomentumMeanCmd;
   G4UIcmdWithADoubleAndUnit *muonBeamPhaseSpaceMomentumSigmaCmd;
   G4UIcmdWithADouble        *muonBeamPhaseSpaceMomentumWidthCmd;

public:
   GEMPrimaryGeneratorMessenger(GEMPrimaryGeneratorAction*);
   ~GEMPrimaryGeneratorMessenger();

   void SetNewValue(G4UIcommand*, G4String);

private:
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
