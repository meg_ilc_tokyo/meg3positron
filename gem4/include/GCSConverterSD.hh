#ifndef GCSConverterSD_h
#define GCSConverterSD_h 1

#include <vector>
#include "G4VSensitiveDetector.hh"

#include <TClonesArray.h>

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class GCSConverterSD : public G4VSensitiveDetector
{
public:

   GCSConverterSD(G4String name);
   ~GCSConverterSD(){};

   G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*);
   void Initialize(G4HCofThisEvent* HCE);

private:
   TClonesArray *pConverterEvent_arr;
   std::map<G4String, G4int> map_hitID;
   G4int hitID;

};

#endif

