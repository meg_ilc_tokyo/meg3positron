//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <vector>
#include "G4VUserTrackInformation.hh"
#include "G4Track.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"
#include <set>
#include <map>

#ifndef GEMUserTrackInformation_h
#define GEMUserTrackInformation_h 1

class GEMAbsUserTrackInformation;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMTrackInfo
{
public:
   GEMTrackInfo();
   GEMTrackInfo(const GEMTrackInfo &info);
   ~GEMTrackInfo() {}
   GEMTrackInfo &operator=(const GEMTrackInfo &);

public:
   G4ParticleDefinition *particledef;          // GEANT particle definition
   G4int                 trackid;              // GEANT track id
   G4int                 vertexid;             // GEANT vertex id
   G4int                 TrackIndex;           // Array index in MCTrackEvent
   G4int                 PrimaryParticleIndex; // Array index of associated vertex in MCKineEvent
   G4int                 ParentID;             // Track ID of parent MCTrackEvent
   G4int                 ParentIndex;          // Array index of parent MCTrackEvent
   G4float               tekine;               // initial kinetic energy
   G4float               tracklength;          // track length
   G4float               trktotphi;            // track phi
   G4int                 trackendmec;          // GEANT mechanism at the end of track
   G4int                 nstep;                // # of steps in track
   G4int                 nturn;                // # of turns in track
   G4int                 annihiltrackid;       // -1 or GEANT track id of the original annihilation gamma
   G4int                 bremstrackid;         // -1 or GEANT track id of the original bremsstrahlung gamma
   G4double              radx;                 // x of original annihilation or bremsstrahlung gamma
   G4double              rady;                 // y of original annihilation or bremsstrahlung gamma
   G4double              radz;                 // z of original annihilation or bremsstrahlung gamma
   G4double              momx;                 // x of initial momentum
   G4double              momy;                 // y of initial momentum
   G4double              momz;                 // z of initial momentum
   std::vector<G4float>  trackbufx;            // [nstep] x of step
   std::vector<G4float>  trackbufy;            // [nstep] y of step
   std::vector<G4float>  trackbufz;            // [nstep] z of step
   std::vector<G4float>  trackbuft;            // [nstep] time
   std::vector<G4float>  trackbufde;           // [nstep] energy deposit
   std::set<G4int> hitcyldch;                  // set of turns hiting the CYLDCH inner wall
   std::map<G4int,G4ThreeVector> poscyldch;    // [nturn] position at CYLDCH entrance
   std::map<G4int,G4ThreeVector> momcyldch;    // [nturn] momentum at CYLDCH entrance

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMUserTrackInformation : public G4VUserTrackInformation
{
public:
   GEMUserTrackInformation();
   GEMUserTrackInformation(const GEMUserTrackInformation &info);
   ~GEMUserTrackInformation();
   const char* GetName() const { return "GEM"; }

   inline void Print() const {};
   void AddUserTrackInformation(GEMAbsUserTrackInformation* ui)
   {
      modules.push_back(ui);
   }
   GEMAbsUserTrackInformation *GetModule(const char* name);
   GEMAbsUserTrackInformation *GetModuleAt(G4int i) { return modules[i]; }
   static G4int GetG3ParticleID(const G4ParticleDefinition* particle);
   GEMUserTrackInformation &operator=(const GEMUserTrackInformation &);

protected:
   std::vector<GEMAbsUserTrackInformation*> modules;

public:
   GEMTrackInfo trackInfo;
};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
