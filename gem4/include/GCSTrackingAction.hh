#ifndef GCSTrackingAction_h
#define GCSTrackingAction_h 1
#include "G4UserTrackingAction.hh"

class GCSTrackingAction : public G4UserTrackingAction
{
   friend class GCSModule;
public:
   GCSTrackingAction();
   ~GCSTrackingAction();
   void PreUserTrackingAction(const G4Track*);
   void PostUserTrackingAction(const G4Track*);

};

#endif
