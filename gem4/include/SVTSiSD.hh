//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef SVTSiSD_h
#define SVTSiSD_h 1

#include "G4VSensitiveDetector.hh"
#include "SVTSiHit.hh"
#include "SVTPixelHit.hh"
#include "SVTModule.hh"

class G4Step;
class G4HCofThisEvent;
class G4VPhysicalVolume;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class SVTSiSD : public G4VSensitiveDetector
{
private:
   G4int                   idx; // index in the globa module array
   SVTSiHitsCollection    *fSiHitsCollection;    // Si hits collection
   SVTPixelHitsCollection *fPixelHitsCollection; // Pixel hits collection
   static SVTModule       *fgModule;             // pointer to SVT detector
   G4int                   fPrevStepTrackID;
   G4VPhysicalVolume      *fPrevStepPhysicalVol;
   G4int                   fVerbose;

public:
   SVTSiSD(G4String name);
   ~SVTSiSD();

   void Initialize(G4HCofThisEvent* HCE);
   G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
   void EndOfEvent(G4HCofThisEvent* HCE);
   void clear();
   void DrawAll();
   void PrintAll();
   void SetIndex(G4int i) { idx = i; }
   void SetModule(SVTModule* module) {fgModule = module;}
   void SetVerboseLevel(G4int level) {fVerbose = level;}

protected:

};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
