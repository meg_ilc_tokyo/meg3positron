//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef TARSteppingAction_H
#define TARSteppingACtion_H 1

#include "globals.hh"
#include "G4UserSteppingAction.hh"

class TARSteppingMessenger;
class G4Material;
class G4VPhysicalVolume;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class TARSteppingAction : public G4UserSteppingAction
{
private:
   G4int              idx;
   G4VPhysicalVolume *targetVol;

public:
   TARSteppingAction();
   ~TARSteppingAction();
   virtual void UserSteppingAction(const G4Step*);
   void SetIndex(G4int i) { idx = i; }
   void SetTargetVolume(G4VPhysicalVolume *v) { targetVol = v; }
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
