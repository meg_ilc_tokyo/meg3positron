#ifndef GEMConstants_h
#define GEMConstants_h 1

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "globals.hh"

namespace
{
const G4double kInvalidTime           = 1e15 * second;
const G4double kInvalidTimeDouble     = kInvalidTime;
const G4float  kInvalidTimeFloat      = kInvalidTime;

const G4int    kDefaultCustomRunNumber       = 500011;
const G4double kDefaultAirFraction           = 0; // air in COBRA

const G4int    kDefaultEventType             = 1;

const G4bool   kDefaultMagneticFieldFlag     = true;

const G4double kDefaultStoppedMuonCenterX    = 0;
const G4double kDefaultStoppedMuonSigmaX     = 0.5 * cm;
const G4double kDefaultStoppedMuonCenterY    = 0;
const G4double kDefaultStoppedMuonSigmaY     = 0.5 * cm;
const G4double kDefaultStoppedMuonDepth      = 0.030 * cm;
const G4double kDefaultStoppedMuonDepthSigma = 0.020 * cm;
const G4int    kDefaultTargetType        = 3;
const G4double kDefaultTargetSize[]      = {14.00 * cm,
                                            4 * cm,
                                            0.0140 * cm
                                           };
const G4double kDefaultTargetAngle[]     = {15.00 * degree,
                                            0.00 * degree,
                                            0.00 * degree
                                           };
const G4double kDefaultTargetPosition[]  = { 0.00 * mm,
                                             0.00 * mm,
                                             0.00 * mm
                                           };
const G4double kDefaultDegraderThickness = 330 * micrometer;

const G4double muon_mass_c2 = 105.6583668 * MeV; // PDG 2008
const G4double kNegPiMass   = 139.57061   * MeV;
const G4double kPi0Mass     = 134.9770    * MeV;
const G4double kProtonMass  = 938.2720813 * MeV;
const G4double kNeutronMass = 939.5654133 * MeV;

const G4int       kDefaultMuonBeamMuonDecayMode           = 0;
const G4bool      kDefaultMuonBeamPhaseSpaceReadFromFile  = false;
const char *const kDefaultMuonBeamPhaseSpaceFileName      = "";
const G4double    kDefaultMuonBeamPhaseSpaceMeanX         = 0 * cm;
const G4double    kDefaultMuonBeamPhaseSpaceSigmaX        = 2.727 * cm;
const G4double    kDefaultMuonBeamPhaseSpaceMeanXp        = 0 * mrad;
const G4double    kDefaultMuonBeamPhaseSpaceSigmaXp       = 28.0 * mrad;
const G4double    kDefaultMuonBeamPhaseSpaceCorrX         = -0.80;
const G4double    kDefaultMuonBeamPhaseSpaceMeanY         = 0 * cm;
const G4double    kDefaultMuonBeamPhaseSpaceSigmaY        = 2.548 * cm;
const G4double    kDefaultMuonBeamPhaseSpaceMeanYp        = 0 * mrad;
const G4double    kDefaultMuonBeamPhaseSpaceSigmaYp       = 29.0 * mrad;
const G4double    kDefaultMuonBeamPhaseSpaceCorrY         = -0.84;
const G4double    kDefaultMuonBeamPhaseSpaceMeanZ         = -564.70 * cm;
const G4double    kDefaultMuonBeamPhaseSpaceSigmaZ        = 0 * cm;
const G4int       kDefaultMuonBeamPhaseSpaceMomentumShape = 1;
const G4double    kDefaultMuonBeamPhaseSpaceMomentumMean  = 27.9 * MeV;
const G4double    kDefaultMuonBeamPhaseSpaceMomentumSigma = 0.90 * MeV;
const G4double    kDefaultMuonBeamPhaseSpaceMomentumWidth = 3.00;

const G4double    kInterestCTHGENG   = 0;
const G4double    kInterestDCOSTHG   = 0.35;
const G4double    kInterestPHIGENG   = 1 * pi;
const G4double    kInterestDPHIG     = 1. / 3 * pi;
const G4double    kInterestCTHGENP   = 0;
const G4double    kInterestDCOSTHP   = 0.35;
const G4double    kInterestPHIGENP   = 0 * pi;
const G4double    kInterestDPHIP     = 1. / 3 * pi;

const G4double    kExtendedCTHGENG   = 0;
const G4double    kExtendedDCOSTHG   = 0.45;
const G4double    kExtendedPHIGENG   = 1 * pi;
const G4double    kExtendedDPHIG     = 7. / 18 * pi;
const G4double    kExtendedCTHGENP   = 0;
const G4double    kExtendedDCOSTHP   = 0.45;
const G4double    kExtendedPHIGENP   = 0 * pi;
const G4double    kExtendedDPHIP     = 7. / 18 * pi;

const G4double    kDefaultCTHGENG   = kExtendedCTHGENG;
const G4double    kDefaultDCOSTHG   = kExtendedDCOSTHG;
const G4double    kDefaultPHIGENG   = kExtendedPHIGENG;
const G4double    kDefaultDPHIG     = kExtendedDPHIG;
const G4double    kDefaultCTHGENP   = kExtendedCTHGENP;
const G4double    kDefaultDCOSTHP   = kExtendedDCOSTHP;
const G4double    kDefaultPHIGENP   = kExtendedPHIGENP;
const G4double    kDefaultDPHIP     = kExtendedDPHIP;

const G4double    kDefaultXRangeMin = 0.009672; // 0.01 in gem
const G4double    kDefaultXRangeMax = 1.000024;
const G4double    kDefaultYRangeMin = 0; //0.1 in gem
const G4double    kDefaultYRangeMax = 0.999977;
const G4double    kDefaultZRangeMin = -1;
const G4double    kDefaultZRangeMax = +1;

const G4double    kDefaultMuonPolarization = 0.9; // MEG TN068
const G4double    kDefaultMuonRadiativeBranchingRatio = 0.0691;
}

#endif
