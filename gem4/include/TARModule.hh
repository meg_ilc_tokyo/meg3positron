// $

#ifndef TARModule_h
#define TARModule_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4RotationMatrix.hh"
#include "G4LogicalVolume.hh"
#include "GEMAbsTargetModule.hh"
#include "GEMPhysicsList.hh"
#include <vector>

class G4Material;
class G4VSensitiveDetector;
class G4VisAttributes;
class G4Region;
class TARMessenger;

class TARModule : public GEMAbsTargetModule
{
   friend class GEMPrimaryGeneratorAction;

public:
   TARModule(const char* n, GEMPhysicsList *p, G4int i);
   virtual ~TARModule();

private:
   TARModule();

public:
   void Construct(G4LogicalVolume *parent);
   GEMAbsUserEventInformation* MakeEventInformation(const G4Event* anEvent);
   GEMAbsUserTrackInformation* MakeTrackInformation(const G4Track* anTrack);
   G4bool IsInsideFGAS() const { return true; } // this is in FGAS (MACONHE) volume
   void ReadDatabase(ROMESQLDataBase *db, G4int run);
   void SetNumberOfHoles(G4int n) { fNumberOfHoles = n; }
   void SetHoleRadius(G4double r);
   G4int GetNumberOfHoles() const { return fNumberOfHoles; }
   void SetVerboseLevel(G4int verbose) { fVerboseLevel = verbose; }   

protected:
   void SetTargetSize(G4ThreeVector &size)      { fTargetSize = size; }
   void SetTargetRotation(G4RotationMatrix *rot) { fTargetRotation = *rot; }
   void SetTargetPosition(G4ThreeVector& pos)   { fTargetPosition = pos; }
   void SetTargetEulerAngles(G4ThreeVector& eul) { fTargetEulerAngles = eul;}
   inline void SetTargetMaterial(G4int &i) {fTargetMaterialId = i; G4cout << "TargetMaterialId = " << fTargetMaterialId << G4endl;}

private:
   void ConstructMaterials();
   void CreateRegion();
   void SetRegionCuts();

private:
   G4int    fIdx;
   G4int    fVerboseLevel;
   GEMPhysicsList *fPhysics;
   TARMessenger   *fMessenger;
   G4bool   fMaterialConstructed;
   G4int    fITAR;
   G4double frame_a;
   G4double frame_b;
   G4double frame_thickness;
   G4double frame_x_width;
   G4double frame_y_width;
   G4double front_box_spacing;
   G4double back_box_spacing;
   G4double kSupport1[2];
   G4double kSupport2[2];
   G4double kSupport3[3];
   G4double kSupport4[4];
   G4double kRohacellInsertionTube[4];
   G4int    fTargetMaterialId; // 0: Polyethylene and polyester sandwitch, 1: Scintillator, 2: Polyethylene, 3: Silicon
   G4int    fNumberOfHoles;
   std::vector<G4double> fHoleRadii;
   std::vector<std::vector<G4double> > fHolePositions;


   // Materials for target
   G4Material *fRohacell51;
   G4Material *fRohacell;
   G4Material *fCarbonFiberFrame;
   G4Material *fPolyethylene;
   G4Material *fPolyethylenePolyester;
   G4Material *fMylar;
   G4Material *fSUS;
   G4Material *fScintillator;
   G4Material *fSilicon;

   G4Region   *fTargetRegion;
};

#endif

