//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#ifndef GCSModule_h
#define GCSModule_h 1

#include "globals.hh"
#include "GEMAbsModule.hh"

#include "G4Material.hh"
#include "GCSWireSD.hh"
#include "GCSConverterSD.hh"
#include "GCSEventAction.hh"
#include "GCSStackingAction.hh"
#include "GCSSteppingAction.hh"
#include "GCSTrackingAction.hh"


class GCSMessenger;
class G4UIdirectory;
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

class GCSModule : public GEMAbsModule
{
private:
   G4int       idx;
   G4bool      materialConstructed;
   
   G4UIdirectory        *fDir;
   GCSMessenger *fMessenger;

   G4Material*  fLYSO;
   G4Material*  fHe;
   GCSWireSD*   fWireSD;
   GCSConverterSD*   fConverterSD;

   // Geometry
   // Gas
   G4double                   fGasRMin;
   G4double                   fGasRMax;
   G4double                   fGasLength;
   // LYSO
   G4double                   fLYSOThickness; //s = 4*mm;
   G4int                      fNLYSOLayers;
   std::vector<G4double>      fLYSORadius; //[nLYSOLayer] = {20.0*cm, 27.5*cm, 35.0*cm, 42.5*cm};
   std::vector<G4double>      fLYSOLength; //[nLYSOLayer] = {63*cm, 78*cm, 93*cm, 108*cm};
   std::vector<G4int>         fLYSONModules;//[nLYSOLayer] = {8, 11, 14, 17};
   std::vector<G4int>         fLYSONSegmentsPerModuleInPhi;//[nLYSOLayer] = {16, 16, 16, 16};
   std::vector<G4int>         fLYSONSegmentsInZ;//[nLYSOLayer] = {21, 26, 31, 36 };
   // Gas cells/wires
   std::vector<G4int>         fNCellPhi;
   G4int                      fNSublayers;
   G4double                   fCellDr;

public:
   GCSModule(const char* n, G4int i);
   ~GCSModule();

   void Construct(G4LogicalVolume *parent);
   GEMAbsUserEventInformation* MakeEventInformation(const G4Event* anEvent);
   GEMAbsUserTrackInformation* MakeTrackInformation(const G4Track* anTrack);

   // Messenger functions
   inline void        SetGasRMin(double gasRMin) { fGasRMin = gasRMin; }
   inline G4double    GetGasRMin() const { return fGasRMin; }

   inline void        SetGasRMax(double gasRMax) { fGasRMax = gasRMax; }
   inline G4double    GetGasRMax() const { return fGasRMax; }

   inline void        SetGasLength(double gasLength) { fGasLength = gasLength; }
   inline G4double    GetGasLength() const { return fGasLength; }

   inline void        SetLYSOThickness(double LYSOThickness) { fLYSOThickness = LYSOThickness; }
   inline G4double    GetLYSOThickness() const { return fLYSOThickness; }

   inline void        SetNLYSOLayers(G4int NLYSOLayers) { fNLYSOLayers = NLYSOLayers; }
   inline G4int       GetNLYSOLayers() const { return fNLYSOLayers; }

   inline void        SetLYSORadiusAt(G4int i,G4double r)  {fLYSORadius.at(i) = r; }
   inline G4double    GetLYSORadiusAt(G4int i) const       {return fLYSORadius.at(i);}

   inline void        SetLYSOLengthAt(G4int i,G4double length)  {fLYSOLength.at(i) = length; }
   inline G4double    GetLYSOLengthAt(G4int i) const       {return fLYSOLength.at(i);}

   inline void        SetLYSONModulesPerLayerAt(G4int i,G4int nModules)  {fLYSONModules.at(i) = nModules; }
   inline G4int       GetLYSONModulesPerLayerAt(G4int i) const       {return fLYSONModules.at(i);}

   inline void        SetLYSONSegmentsPerModuleInPhiAt(G4int i,G4int nSegments)  {fLYSONSegmentsPerModuleInPhi.at(i) = nSegments; }
   inline G4int       GetLYSONSegmentsPerModuleInPhiAt(G4int i) const       {return fLYSONSegmentsPerModuleInPhi.at(i);}

   inline void        SetLYSONSegmentsInZAt(G4int i,G4int nSegments)  {fLYSONSegmentsInZ.at(i) = nSegments; }
   inline G4int       GetLYSONSegmentsInZAt(G4int i) const       {return fLYSONSegmentsInZ.at(i);}

   inline void        SetNCellPhiAt(G4int i,G4int nCellPhi)  {fNCellPhi.at(i) = nCellPhi; }
   inline G4int       GetNCellPhiAt(G4int i) const       {return fNCellPhi.at(i);}

   inline void        SetNSublayers(G4int nSublayers)  {fNSublayers = nSublayers; }
   inline G4int       GetNSublayers() const       {return fNSublayers;}

   inline void        SetCellDr(G4double cellDr)  {fCellDr = cellDr; }
   inline G4double    GetCellDr() const       {return fCellDr;}

private:
   void ConstructMaterials();
};

#endif

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
