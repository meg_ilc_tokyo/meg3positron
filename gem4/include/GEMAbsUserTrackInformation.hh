//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4VUserTrackInformation.hh"
#include "globals.hh"

#ifndef GEMAbsUserTrackInformation_h
#define GEMAbsUserTrackInformation_h 1

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class GEMAbsUserTrackInformation : public G4VUserTrackInformation
{
public:
   GEMAbsUserTrackInformation() {}
   ~GEMAbsUserTrackInformation() {}

   virtual const char *GetName() const = 0;

   virtual GEMAbsUserTrackInformation *Clone() const = 0;
   // when nonzero pointer is returned, track information is kept in
   // GEMUserEventInformation even after the track is deleted.

};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
