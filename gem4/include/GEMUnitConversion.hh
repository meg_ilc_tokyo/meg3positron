//
//            G3         G4
// Length     cm         mm
// Time       sec        nsec
// Energy     GeV        MeV
// Angle      deg        rad

#ifndef GEMUNITCONVERSION_H
#define GEMUNITCONVERSION_H 1

static constexpr double kG4ToG3Length              = 0.1;
static constexpr double kG4ToG3Time                = 1e-9;
static constexpr double kG4ToG3Energy              = 1e-3;
static constexpr double kG4ToG3Angle               = 180 / 3.14159265358979323846;
static constexpr double kG4ToG3Charge              = 1e-9;
static constexpr double kG4ToG3Temperature         = 1;
static constexpr double kG4ToG3LuminousIntensity   = 1;
static constexpr double kG4ToG3Weight              = kG4ToG3Energy * kG4ToG3Time * kG4ToG3Time /
                                                     kG4ToG3Length / kG4ToG3Length;
static constexpr double kG4ToG3Pressure            = kG4ToG3Energy / kG4ToG3Length / kG4ToG3Length /
                                                     kG4ToG3Length;
static constexpr double kG4ToG3Current             = kG4ToG3Charge / kG4ToG3Time;
static constexpr double kG4ToG3Voltage             = kG4ToG3Energy / kG4ToG3Charge;
static constexpr double kG4ToG3Resistance          = kG4ToG3Energy / kG4ToG3Charge / kG4ToG3Charge *
                                                     kG4ToG3Time;
static constexpr double kG4ToG3Capacitance         = kG4ToG3Charge * kG4ToG3Charge / kG4ToG3Energy;
static constexpr double kG4ToG3MagneticFlux        = kG4ToG3Energy / kG4ToG3Charge * kG4ToG3Time;
static constexpr double kG4ToG3MagneticFluxDensity = kG4ToG3Energy / kG4ToG3Charge * kG4ToG3Time /
                                                     kG4ToG3Length / kG4ToG3Length;
static constexpr double kG4ToG3Inductance          = kG4ToG3Energy / kG4ToG3Charge * kG4ToG3Time;
static constexpr double kG4ToG3Radioactivity       = 1 / kG4ToG3Time;
static constexpr double kG4ToG3Dose                = 1 / kG4ToG3Time / kG4ToG3Time * kG4ToG3Length *
                                                     kG4ToG3Length;
static constexpr double kG4ToG3LuminousFlux        = 1;
static constexpr double kG4ToG3LuminousEmittance   = 1 / kG4ToG3Length / kG4ToG3Length;


#ifdef UNITCONVG4TOG3
static constexpr double kUnitConvLength              = kG4ToG3Length;
static constexpr double kUnitConvTime                = kG4ToG3Time;
static constexpr double kUnitConvEnergy              = kG4ToG3Energy;
static constexpr double kUnitConvAngle               = kG4ToG3Angle;
static constexpr double kUnitConvCharge              = kG4ToG3Charge;
static constexpr double kUnitConvTemperature         = kG4ToG3Temperature;
static constexpr double kUnitConvLuminousIntensity   = kG4ToG3LuminousIntensity;
static constexpr double kUnitConvWeight              = kG4ToG3Weight;
static constexpr double kUnitConvPressure            = kG4ToG3Pressure;
static constexpr double kUnitConvCurrent             = kG4ToG3Current;
static constexpr double kUnitConvVoltage             = kG4ToG3Voltage;
static constexpr double kUnitConvResistance          = kG4ToG3Resistance;
static constexpr double kUnitConvCapacitance         = kG4ToG3Capacitance;
static constexpr double kUnitConvMagneticFlux        = kG4ToG3MagneticFlux;
static constexpr double kUnitConvMagneticFluxDensity = kG4ToG3MagneticFluxDensity;
static constexpr double kUnitConvInductance          = kG4ToG3Inductance;
static constexpr double kUnitConvRadioactivity       = kG4ToG3Radioactivity;
static constexpr double kUnitConvDose                = kG4ToG3Dose;
static constexpr double kUnitConvLuminousFlux        = kG4ToG3LuminousFlux;
static constexpr double kUnitConvLuminousEmittance   = kG4ToG3LuminousEmittance;
#else
static constexpr double kUnitConvLength              = 1;
static constexpr double kUnitConvTime                = 1;
static constexpr double kUnitConvEnergy              = 1;
static constexpr double kUnitConvAngle               = 1;
static constexpr double kUnitConvCharge              = 1;
static constexpr double kUnitConvTemperature         = 1;
static constexpr double kUnitConvLuminousIntensity   = 1;
static constexpr double kUnitConvWeight              = 1;
static constexpr double kUnitConvPressure            = 1;
static constexpr double kUnitConvCurrent             = 1;
static constexpr double kUnitConvVoltage             = 1;
static constexpr double kUnitConvResistance          = 1;
static constexpr double kUnitConvCapacitance         = 1;
static constexpr double kUnitConvMagneticFlux        = 1;
static constexpr double kUnitConvMagneticFluxDensity = 1;
static constexpr double kUnitConvInductance          = 1;
static constexpr double kUnitConvRadioactivity       = 1;
static constexpr double kUnitConvDose                = 1;
static constexpr double kUnitConvLuminousFlux        = 1;
static constexpr double kUnitConvLuminousEmittance   = 1;
#endif

#endif
