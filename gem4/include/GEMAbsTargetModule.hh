#ifndef GEMAbsTargetTARModule_h
#define GEMAbsTargetTARModule_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4RotationMatrix.hh"
#include "G4LogicalVolume.hh"

#include "GEMAbsModule.hh"

class G4VPhysicalVolume;
class G4Material;
class G4VSensitiveDetector;
class G4VisAttributes;

class GEMAbsTargetModule : public GEMAbsModule
{
   friend class GEMPrimaryGeneratorAction;

public:
   GEMAbsTargetModule(): GEMAbsModule(), fTargetVolume(nullptr), fTGTGeometryId(-1), fTargetRotation() {}
   GEMAbsTargetModule(const char* n): GEMAbsModule(n), fTargetVolume(0), fTGTGeometryId(-1), fTargetRotation() {}
   virtual ~GEMAbsTargetModule() {}

public:
   G4VPhysicalVolume *GetTargetVolume() { return fTargetVolume; }
   G4int GetTGTGeometryId() const {return fTGTGeometryId; }
   void SetTGTGeometryId(G4int id) {fTGTGeometryId = id;}

protected:
   virtual void SetTargetSize(G4ThreeVector&)     = 0;
   virtual void SetTargetRotation(G4RotationMatrix*)    = 0;
   virtual void SetTargetPosition(G4ThreeVector&) = 0;
   virtual void SetTargetMaterial(G4int&)         = 0;
   virtual void SetTargetEulerAngles(G4ThreeVector&) = 0;
protected:
   G4VPhysicalVolume *fTargetVolume; // this is used in event generation
   G4int              fTGTGeometryId;
   G4RotationMatrix   fTargetRotation;
   G4ThreeVector      fTargetSize;
   G4ThreeVector      fTargetPosition;
   G4ThreeVector      fTargetEulerAngles;
};

#endif

