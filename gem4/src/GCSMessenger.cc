//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "GCSMessenger.hh"
#include "GCSModule.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"


GCSModule* GCSMessenger::fgModule = 0;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GCSMessenger::GCSMessenger(GCSModule *mod)
{
   fgModule = mod;

   // Make directories
   fGeomDir = new G4UIdirectory("/gcs/geom/");
   fGeomDir->SetGuidance("Commands for geometry");

   fTrackingDir = new G4UIdirectory("/gcs/tracking/");
   fTrackingDir->SetGuidance("Commands for tracking");

   fMaterialDir = new G4UIdirectory("/gcs/material/");
   fMaterialDir->SetGuidance("Commands for materials");

   fVisDir = new G4UIdirectory("/gcs/vis/");
   fVisDir->SetGuidance("Commands for visualization");

   
   // Define commands
   fVerboseCmd = new G4UIcmdWithAnInteger("/gcs/verbose", this);
   fVerboseCmd->SetGuidance("Set the verbosity");
   fVerboseCmd->SetParameterName("verbose", true);
   fVerboseCmd->SetDefaultValue(1);

   fCheckOverlapCmd = new G4UIcmdWithABool("/gcs/geom/checkOverlap", this);
   fCheckOverlapCmd->SetGuidance("Check overlap of volumes");
   fCheckOverlapCmd->SetParameterName("checkOverlap", true);
   fCheckOverlapCmd->SetDefaultValue(false);

   fGasRMinCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/gasRMin", this);
   fGasRMinCmd->SetGuidance("Set minimum radius of the gas volume");
   fGasRMinCmd->SetParameterName("rmin", false);
   fGasRMinCmd->SetUnitCategory("Length");
   fGasRMinCmd->SetDefaultValue(20);
   fGasRMinCmd->SetDefaultUnit("cm");
   fGasRMinCmd->SetRange("0 < x");

   fGasRMaxCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/gasRMax", this);
   fGasRMaxCmd->SetGuidance("Set maximum radius of the gas volume");
   fGasRMaxCmd->SetParameterName("rmax", false);
   fGasRMaxCmd->SetUnitCategory("Length");
   fGasRMaxCmd->SetDefaultValue(50);
   fGasRMaxCmd->SetDefaultUnit("cm");
   fGasRMaxCmd->SetRange("0 < x");

   fGasLengthCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/gasLength", this);
   fGasLengthCmd->SetGuidance("Set length of the gas volume");
   fGasLengthCmd->SetParameterName("length", false);
   fGasLengthCmd->SetUnitCategory("Length");
   fGasLengthCmd->SetDefaultValue(108);
   fGasLengthCmd->SetDefaultUnit("cm");
   fGasLengthCmd->SetRange("0 < length");

   fLYSOThicknessCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/lysoThickness", this);
   fLYSOThicknessCmd->SetGuidance("Set effective thickness of the LYSO");
   fLYSOThicknessCmd->SetParameterName("thickness", false);
   fLYSOThicknessCmd->SetUnitCategory("Length");
   fLYSOThicknessCmd->SetDefaultValue(4);
   fLYSOThicknessCmd->SetDefaultUnit("mm");
   fLYSOThicknessCmd->SetRange("0 < thickness");

   fNLYSOLayerCmd = new G4UIcmdWithAnInteger("/gcs/geom/nlysoLayer", this);
   fNLYSOLayerCmd->SetGuidance("Set number of LYSO layers");
   fNLYSOLayerCmd->SetParameterName("nlysoLayer", false);
   fNLYSOLayerCmd->SetDefaultValue(4);
   fNLYSOLayerCmd->SetRange("0 < nlysoLayer");

   fLayerRadius0Cmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/layer0Radius", this);
   fLayerRadius0Cmd->SetGuidance("Set radius of the 1st layer");
   fLayerRadius0Cmd->SetParameterName("r0", false);
   fLayerRadius0Cmd->SetUnitCategory("Length");
   fLayerRadius0Cmd->SetDefaultValue(20);
   fLayerRadius0Cmd->SetDefaultUnit("cm");
   fLayerRadius0Cmd->SetRange("0 < r0");

   fLayerRadius1Cmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/layer1Radius", this);
   fLayerRadius1Cmd->SetGuidance("Set radius of the 2nd layer");
   fLayerRadius1Cmd->SetParameterName("r1", false);
   fLayerRadius1Cmd->SetUnitCategory("Length");
   fLayerRadius1Cmd->SetDefaultValue(27.5);
   fLayerRadius1Cmd->SetDefaultUnit("cm");
   fLayerRadius1Cmd->SetRange("0 < r1");

   fLayerRadius2Cmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/layer2Radius", this);
   fLayerRadius2Cmd->SetGuidance("Set radius of the 3rd layer");
   fLayerRadius2Cmd->SetParameterName("r2", false);
   fLayerRadius2Cmd->SetUnitCategory("Length");
   fLayerRadius2Cmd->SetDefaultValue(35);
   fLayerRadius2Cmd->SetDefaultUnit("cm");
   fLayerRadius2Cmd->SetRange("0 < r2");

   fLayerRadius3Cmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/layer3Radius", this);
   fLayerRadius3Cmd->SetGuidance("Set radius of the 4th layer");
   fLayerRadius3Cmd->SetParameterName("r3", false);
   fLayerRadius3Cmd->SetUnitCategory("Length");
   fLayerRadius3Cmd->SetDefaultValue(42.5);
   fLayerRadius3Cmd->SetDefaultUnit("cm");
   fLayerRadius3Cmd->SetRange("0 < r3");

   fNSegmentsInZ0Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsInZ0", this);
   fNSegmentsInZ0Cmd->SetGuidance("Set number of segments in z in the 1st layer");
   fNSegmentsInZ0Cmd->SetParameterName("nSegmentsInZ0", false);
   fNSegmentsInZ0Cmd->SetDefaultValue(21);
   fNSegmentsInZ0Cmd->SetRange("0 < nSegmentsInZ0");

   fNSegmentsInZ1Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsInZ1", this);
   fNSegmentsInZ1Cmd->SetGuidance("Set number of segments in z in the 2nd layer");
   fNSegmentsInZ1Cmd->SetParameterName("nSegmentsInZ1", false);
   fNSegmentsInZ1Cmd->SetDefaultValue(26);
   fNSegmentsInZ1Cmd->SetRange("0 < nSegmentsInZ1");

   fNSegmentsInZ2Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsInZ2", this);
   fNSegmentsInZ2Cmd->SetGuidance("Set number of segments in z in the 3rd layer");
   fNSegmentsInZ2Cmd->SetParameterName("nSegmentsInZ2", false);
   fNSegmentsInZ2Cmd->SetDefaultValue(31);
   fNSegmentsInZ2Cmd->SetRange("0 < nSegmentsInZ2");

   fNSegmentsInZ3Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsInZ3", this);
   fNSegmentsInZ3Cmd->SetGuidance("Set number of segments in z in the 4th layer");
   fNSegmentsInZ3Cmd->SetParameterName("nSegmentsInZ3", false);
   fNSegmentsInZ3Cmd->SetDefaultValue(36);
   fNSegmentsInZ3Cmd->SetRange("0 < nSegmentsInZ3");

   fNSegmentsPerModuleInPhi0Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsPerModuleInPhi0", this);
   fNSegmentsPerModuleInPhi0Cmd->SetGuidance("Set number of segments per module in phi in the 1st layer");
   fNSegmentsPerModuleInPhi0Cmd->SetParameterName("nSegmentsPerModuleInPhi0", false);
   fNSegmentsPerModuleInPhi0Cmd->SetDefaultValue(16);
   fNSegmentsPerModuleInPhi0Cmd->SetRange("0 < nSegmentsPerModuleInPhi0");

   fNSegmentsPerModuleInPhi1Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsPerModuleInPhi1", this);
   fNSegmentsPerModuleInPhi1Cmd->SetGuidance("Set number of segments per module in phi in the 2nd layer");
   fNSegmentsPerModuleInPhi1Cmd->SetParameterName("nSegmentsPerModuleInPhi1", false);
   fNSegmentsPerModuleInPhi1Cmd->SetDefaultValue(16);
   fNSegmentsPerModuleInPhi1Cmd->SetRange("0 < nSegmentsPerModuleInPhi1");
   
   fNSegmentsPerModuleInPhi2Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsPerModuleInPhi2", this);
   fNSegmentsPerModuleInPhi2Cmd->SetGuidance("Set number of segments per module in phi in the 3rd layer");
   fNSegmentsPerModuleInPhi2Cmd->SetParameterName("nSegmentsPerModuleInPhi2", false);
   fNSegmentsPerModuleInPhi2Cmd->SetDefaultValue(16);
   fNSegmentsPerModuleInPhi2Cmd->SetRange("0 < nSegmentsPerModuleInPhi2");

   fNSegmentsPerModuleInPhi3Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSegmentsPerModuleInPhi3", this);
   fNSegmentsPerModuleInPhi3Cmd->SetGuidance("Set number of segments per module in phi in the 4th layer");
   fNSegmentsPerModuleInPhi3Cmd->SetParameterName("nSegmentsPerModuleInPhi3", false);
   fNSegmentsPerModuleInPhi3Cmd->SetDefaultValue(16);
   fNSegmentsPerModuleInPhi3Cmd->SetRange("0 < nSegmentsPerModuleInPhi3");

   fNCellPhi0Cmd = new G4UIcmdWithADouble("/gcs/geom/nCellPhi0", this);
   fNCellPhi0Cmd->SetGuidance("Set number of gas cells in phi in the 1st layer");
   fNCellPhi0Cmd->SetParameterName("nCellPhi0", false);
   fNCellPhi0Cmd->SetDefaultValue(120);
   fNCellPhi0Cmd->SetRange("0 < nCellPhi0");

   fNCellPhi1Cmd = new G4UIcmdWithADouble("/gcs/geom/nCellPhi1", this);
   fNCellPhi1Cmd->SetGuidance("Set number of gas cells in phi in the 2nd layer");
   fNCellPhi1Cmd->SetParameterName("nCellPhi1", false);
   fNCellPhi1Cmd->SetDefaultValue(180);
   fNCellPhi1Cmd->SetRange("0 < nCellPhi1");

   fNCellPhi2Cmd = new G4UIcmdWithADouble("/gcs/geom/nCellPhi2", this);
   fNCellPhi2Cmd->SetGuidance("Set number of gas cells in phi in the 3rd layer");
   fNCellPhi2Cmd->SetParameterName("nCellPhi2", false);
   fNCellPhi2Cmd->SetDefaultValue(240);
   fNCellPhi2Cmd->SetRange("0 < nCellPhi2");

   fNCellPhi3Cmd = new G4UIcmdWithADouble("/gcs/geom/nCellPhi3", this);
   fNCellPhi3Cmd->SetGuidance("Set number of gas cells in phi in the 4th layer");
   fNCellPhi3Cmd->SetParameterName("nCellPhi3", false);
   fNCellPhi3Cmd->SetDefaultValue(300);
   fNCellPhi3Cmd->SetRange("0 < nCellPhi3");

   fNSublayersCmd = new G4UIcmdWithAnInteger("/gcs/geom/nSubLayers", this);
   fNSublayersCmd->SetGuidance("Set number of sublayers of wire cells (segmentation in r)");
   fNSublayersCmd->SetParameterName("nSubLayers", false);
   fNSublayersCmd->SetDefaultValue(6);
   fNSublayersCmd->SetRange("0 < nSubLayers");

   fCellDrCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/cellDr", this);   
   fCellDrCmd->SetGuidance("Set sublayer spacing in r");
   fCellDrCmd->SetParameterName("cellDr", false);
   fCellDrCmd->SetUnitCategory("Length");
   fCellDrCmd->SetDefaultValue(1);
   fCellDrCmd->SetDefaultUnit("cm");
   fCellDrCmd->SetRange("0 < cellDr");

   // std::vector<G4double>      fLYSO_r; //[nLYSOLayer] = {20.0*cm, 27.5*cm, 35.0*cm, 42.5*cm};
   // std::vector<G4double>      fLYSO_length; //[nLYSOLayer] = {63*cm, 78*cm, 93*cm, 108*cm};
   // std::vector<G4int>         fLYSO_n_modules;//[nLYSOLayer] = {8, 11, 14, 17};
   // std::vector<G4int>         fLYSO_n_segments_per_module_phi;//[nLYSOLayer] = {16, 16, 16, 16};
   // std::vector<G4int>         fLYSO_n_segments_z;//[nLYSOLayer] = {21, 26, 31, 36 };
   // fLayerRadius0Cmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/layerRadius0", this);
   // fLayerRadius0Cmd->SetGuidance("Set radius of the 1st layer");
   // fLayerRadius0Cmd->SetParameterName("r0", false);
   // fLayerRadius0Cmd->SetUnitCategory("Length");
   //
   // fLayerRadius1Cmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/layerRadius1", this);
   // fLayerRadius1Cmd->SetGuidance("Set radius of the 2nd layer");
   // fLayerRadius1Cmd->SetParameterName("r1", false);
   // fLayerRadius1Cmd->SetUnitCategory("Length");
   //
   // fNSensorInZ0Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSensorInZ0", this);
   // fNSensorInZ0Cmd->SetGuidance("Set number of sensor chips in Z dir in 1st layer");
   // fNSensorInZ0Cmd->SetParameterName("nz0", false);
   //
   // fNSensorInZ1Cmd = new G4UIcmdWithAnInteger("/gcs/geom/nSensorInZ1", this);
   // fNSensorInZ1Cmd->SetGuidance("Set number of sensor chips in Z dir in 2nd layer");
   // fNSensorInZ1Cmd->SetParameterName("nz1", false);
   //
   // fSensorSlantAngleCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/slant", this);
   // fSensorSlantAngleCmd->SetGuidance("Set sensor slant angle in phi");
   // fSensorSlantAngleCmd->SetParameterName("slant", false);
   // fSensorSlantAngleCmd->SetUnitCategory("Angle");
   //
   // fSensorOverlapCmd = new G4UIcmdWithADoubleAndUnit("/gcs/geom/overlap", this);
   // fSensorOverlapCmd->SetGuidance("Set sensor overlap width");
   // fSensorOverlapCmd->SetParameterName("overlap", false);
   // fSensorOverlapCmd->SetUnitCategory("Length");
   //
   // fSensorThicknessCmd = new G4UIcmdWithADoubleAndUnit("/gcs/sensor/thickness", this);
   // fSensorThicknessCmd->SetGuidance("Set silicon wafer thickness");
   // fSensorThicknessCmd->SetParameterName("thickness", false);
   // fSensorThicknessCmd->SetUnitCategory("Length");
   //
   // fDepThicknessCmd = new G4UIcmdWithADoubleAndUnit("/gcs/sensor/depThickness", this);
   // fDepThicknessCmd->SetGuidance("Set thickness of sensitive (depleted) region");
   // fDepThicknessCmd->SetParameterName("depThickness", false);
   // fDepThicknessCmd->SetUnitCategory("Length");
   //
   // fPixelSizeZCmd = new G4UIcmdWithADoubleAndUnit("/gcs/sensor/pixelSizeZ", this);
   // fPixelSizeZCmd->SetGuidance("Set pixel size in Z");
   // fPixelSizeZCmd->SetParameterName("pixelsizeZ", false);
   // fPixelSizeZCmd->SetUnitCategory("Length");
   //
   // fPixelSizeYCmd = new G4UIcmdWithADoubleAndUnit("/gcs/sensor/pixelSizeY", this);
   // fPixelSizeYCmd->SetGuidance("Set pixel size in Y");
   // fPixelSizeYCmd->SetParameterName("pixelsizeY", false);
   // fPixelSizeYCmd->SetUnitCategory("Length");
   

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GCSMessenger::~GCSMessenger()
{
   delete fVerboseCmd;
   delete fCheckOverlapCmd;
   delete fGasRMinCmd;
   delete fGasRMaxCmd;
   delete fGasLengthCmd;
   delete fLYSOThicknessCmd;
   delete fLayerRadius0Cmd;
   delete fLayerRadius1Cmd;
   delete fLayerRadius2Cmd;
   delete fLayerRadius3Cmd;
   delete fNSegmentsInZ0Cmd;
   delete fNSegmentsInZ1Cmd;
   delete fNSegmentsInZ2Cmd;
   delete fNSegmentsInZ3Cmd;
   delete fNSegmentsPerModuleInPhi0Cmd;
   delete fNSegmentsPerModuleInPhi1Cmd;
   delete fNSegmentsPerModuleInPhi2Cmd;
   delete fNSegmentsPerModuleInPhi3Cmd;
   delete fNLYSOLayerCmd;

   delete fNCellPhi0Cmd;
   delete fNCellPhi1Cmd;
   delete fNCellPhi2Cmd;
   delete fNCellPhi3Cmd;
   delete fNSublayersCmd;
   delete fCellDrCmd;

   delete fGeomDir;
   delete fTrackingDir;
   delete fMaterialDir;
   delete fSensorDir;
   delete fSupportDir;
   delete fDigiDir;
   delete fVisDir;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void GCSMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
   if (command == fVerboseCmd) {
   //    fgModule->SetVerboseLevel(fVerboseCmd->GetNewIntValue(newValue));

   } else if (command == fCheckOverlapCmd) {
   //    fgModule->SetCheckVolumeOverlap(fCheckOverlapCmd->GetNewBoolValue(newValue));

   } else if (command == fGasRMinCmd) {
      fgModule->SetGasRMin(fGasRMinCmd->GetNewDoubleValue(newValue));
   } else if (command == fGasRMaxCmd) {
      fgModule->SetGasRMax(fGasRMaxCmd->GetNewDoubleValue(newValue));
   } else if (command == fGasLengthCmd) {
      fgModule->SetGasLength(fGasLengthCmd->GetNewDoubleValue(newValue));
   } else if (command == fLYSOThicknessCmd) {
      fgModule->SetLYSOThickness(fLYSOThicknessCmd->GetNewDoubleValue(newValue));
   } else if (command == fLayerRadius0Cmd) {
      fgModule->SetLYSORadiusAt(0, fLayerRadius0Cmd->GetNewDoubleValue(newValue));
   } else if (command == fLayerRadius1Cmd) {
      fgModule->SetLYSORadiusAt(1, fLayerRadius1Cmd->GetNewDoubleValue(newValue));
   } else if (command == fLayerRadius2Cmd) {
      fgModule->SetLYSORadiusAt(2, fLayerRadius2Cmd->GetNewDoubleValue(newValue));
   } else if (command == fLayerRadius3Cmd) {
      fgModule->SetLYSORadiusAt(3, fLayerRadius3Cmd->GetNewDoubleValue(newValue));
   } else if (command == fNLYSOLayerCmd) {
      fgModule->SetNLYSOLayers(fNLYSOLayerCmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsInZ0Cmd) {
      fgModule->SetLYSONSegmentsInZAt(0, fNSegmentsInZ0Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsInZ1Cmd) {
      fgModule->SetLYSONSegmentsInZAt(1, fNSegmentsInZ1Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsInZ2Cmd) {
      fgModule->SetLYSONSegmentsInZAt(2, fNSegmentsInZ2Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsInZ3Cmd) {
      fgModule->SetLYSONSegmentsInZAt(3, fNSegmentsInZ3Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsPerModuleInPhi0Cmd) {
      fgModule->SetLYSONSegmentsPerModuleInPhiAt(0, fNSegmentsPerModuleInPhi0Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsPerModuleInPhi1Cmd) {
      fgModule->SetLYSONSegmentsPerModuleInPhiAt(1, fNSegmentsPerModuleInPhi1Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsPerModuleInPhi2Cmd) {
      fgModule->SetLYSONSegmentsPerModuleInPhiAt(2, fNSegmentsPerModuleInPhi2Cmd->GetNewIntValue(newValue));
   } else if (command == fNSegmentsPerModuleInPhi3Cmd) {
      fgModule->SetLYSONSegmentsPerModuleInPhiAt(3, fNSegmentsPerModuleInPhi3Cmd->GetNewIntValue(newValue));
   } else if (command == fNCellPhi0Cmd) {
      fgModule->SetNCellPhiAt(0, fNCellPhi0Cmd->GetNewDoubleValue(newValue));
   } else if (command == fNCellPhi1Cmd) {
      fgModule->SetNCellPhiAt(1, fNCellPhi1Cmd->GetNewDoubleValue(newValue));
   } else if (command == fNCellPhi2Cmd) {
      fgModule->SetNCellPhiAt(2, fNCellPhi2Cmd->GetNewDoubleValue(newValue));
   } else if (command == fNCellPhi3Cmd) {
      fgModule->SetNCellPhiAt(3, fNCellPhi3Cmd->GetNewDoubleValue(newValue));
   } else if (command == fNSublayersCmd) {
      fgModule->SetNSublayers(fNSublayersCmd->GetNewIntValue(newValue));
   } else if (command == fCellDrCmd) {
      fgModule->SetCellDr(fCellDrCmd->GetNewDoubleValue(newValue));
   }
   //
   // std::vector<G4double>      fLYSO_length; //[nLYSOLayer] = {63*cm, 78*cm, 93*cm, 108*cm};
   // std::vector<G4int>         fLYSO_n_modules;//[nLYSOLayer] = {8, 11, 14, 17};
   // std::vector<G4int>         fLYSO_n_segments_per_module_phi;//[nLYSOLayer] = {16, 16, 16, 16};
   // std::vector<G4int>         fLYSO_n_segments_z;//[nLYSOLayer] = {21, 26, 31, 36 };
   // } else if (command == fLayerRadius0Cmd) {
   //    fgModule->SetLayerRAt(0, fLayerRadius0Cmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fLayerRadius1Cmd) {
   //    fgModule->SetLayerRAt(1, fLayerRadius1Cmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fNSensorInZ0Cmd) {
   //    fgModule->SetNSensorInZAt(0, fNSensorInZ0Cmd->GetNewIntValue(newValue));
   //
   // } else if (command == fNSensorInZ1Cmd) {
   //    fgModule->SetNSensorInZAt(1, fNSensorInZ1Cmd->GetNewIntValue(newValue));
   //
   // } else if (command == fSensorSlantAngleCmd) {
   //    fgModule->SetSlantAngle(fSensorSlantAngleCmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fSensorOverlapCmd) {
   //    fgModule->SetOverlapWidth(fSensorOverlapCmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fSensorThicknessCmd) {
   //    fgModule->SetSensorThickness(fSensorThicknessCmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fDepThicknessCmd) {
   //    fgModule->SetDepThickness(fDepThicknessCmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fPixelSizeZCmd) {
   //    fgModule->SetPixelSizeAt(0, fPixelSizeZCmd->GetNewDoubleValue(newValue));
   //
   // } else if (command == fPixelSizeYCmd) {
   //    fgModule->SetPixelSizeAt(1, fPixelSizeYCmd->GetNewDoubleValue(newValue));
   //
   // }
}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
