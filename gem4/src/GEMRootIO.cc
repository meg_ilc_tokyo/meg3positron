//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <sstream>
#include <TSystem.h>
#include <TTree.h>
#include "ROMETreeInfo.h"
#include "generated/MEGUnits.h"
#include "generated/MEGMCSVTRunHeader.h"
#include "generated/MEGMCMixtureInfoEvent.h"
#include "generated/MEGMCKineEvent.h"
#include "generated/MEGMCTrackEvent.h"
#include "generated/MEGMCTargetEvent.h"
#include "generated/MEGMCSVTEvent.h"
#include "generated/MEGMCSVTPixelEvent.h"
#include "generated/MEGMCGCSWireEvent.h"
#include "generated/MEGMCGCSConverterEvent.h"
#include "generated/MEGMCKineSubevent.h"
#include "generated/MEGMCTrackSubevent.h"
#include "generated/MEGMCTargetSubevent.h"
#include "generated/MEGMCSVTSubevent.h"
#include "GEMRootIO.hh"
#include "GEMRootIOMessenger.hh"
#include "GEMDetectorConstruction.hh"
#include <G4SDManager.hh>
#include <G4HCofThisEvent.hh>
#include <G4RunManager.hh>
#include <G4EventManager.hh>
#include <G4Run.hh>
#include <G4Event.hh>
#include "G4SystemOfUnits.hh"

static GEMRootIO* instance = nullptr;

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMRootIO::GEMRootIO()
   : fRunManager(0)
   , fInfo(new ROMETreeInfo())
   , fUnits(new MEGUnits())
   , fMCSVTRunHeader(new MEGMCSVTRunHeader())
   , fMCMixtureInfoEvent(new MEGMCMixtureInfoEvent())
   , fMCKineEvent(new MEGMCKineEvent())
   , fMCTrackEvents(new TClonesArray("MEGMCTrackEvent"))
// ,fMCTargetEvent(new MEGMCTargetEvent())
   , fMCTargetEvents(new TClonesArray("MEGMCTargetEvent"))
   , fMCSVTEvent(new MEGMCSVTEvent())
   , fMCSVTPixelEvents(new TClonesArray("MEGMCSVTPixelEvent"))
   , fMCGCSWireEvents(new TClonesArray("MEGMCGCSWireEvent"))
   , fMCGCSConverterEvents(new TClonesArray("MEGMCGCSConverterEvent"))
   , fMCKineSubevents(new TClonesArray("MEGMCKineSubevent"))
   , fMCTrackSubevents(new TClonesArray("MEGMCTrackSubevent"))
   , fMCTargetSubevents(new TClonesArray("MEGMCTargetSubevent"))
   , fMCSVTSubevents(new TClonesArray("MEGMCSVTSubevent"))     
   , fMessenger(new GEMRootIOMessenger(this))
{
   // initialize ROOT

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMRootIO::Open()
{
   if (fSimFileActive) {
      std::string tmpName = fSimFileName;
      size_t pos;
      while ((pos = tmpName.find('#')) != std::string::npos) {
         std::ostringstream o;
         o << std::setw(5) << setfill('0') << fRunManager->GetCurrentRun()->GetRunID();
         tmpName.replace(pos, 1, o.str().c_str());
      }
      G4cout << "Output file : " << tmpName << G4endl;

      if (!overwrite && !gSystem->AccessPathName(tmpName.c_str(), kFileExists)) {
         openFileFailed = true;
         return false;
      }
      fSimFile = new TFile(tmpName.c_str(), "RECREATE");
      fSimTree = new TTree("sim", "sim");

      // Branches
      fSimTree->Branch("Info.",           "ROMETreeInfo",          &fInfo,               32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      if (mcmixeventBranchActive) {
         fSimTree->Branch("mcmixevent.",  "MEGMCMixtureInfoEvent", &fMCMixtureInfoEvent, 32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mckineBranchActive) {
         fSimTree->Branch("mckine.",      "MEGMCKineEvent",        &fMCKineEvent,        32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mctrackBranchActive) {
         fSimTree->Branch("mctrack",      "TClonesArray",          &fMCTrackEvents,      32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mctargetBranchActive && fDetector && fDetector->FindModule("TAR")) {
         fSimTree->Branch("mctar",       "TClonesArray",           &fMCTargetEvents,     32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mcsvtBranchActive && fDetector && fDetector->FindModule("SVT")) {
         fSimTree->Branch("mcsvt.",       "MEGMCSVTEvent",         &fMCSVTEvent,         32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mcsvtpixelBranchActive && fDetector && fDetector->FindModule("SVT")) {
         fSimTree->Branch("mcsvtpixel",     "TClonesArray",          &fMCSVTPixelEvents,     32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mcgcswireBranchActive && fDetector && fDetector->FindModule("GCS")) {
         fSimTree->Branch("mcgcswire",     "TClonesArray",          &fMCGCSWireEvents,     32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
      if (mcgcsconverterBranchActive && fDetector && fDetector->FindModule("GCS")) {
         fSimTree->Branch("mcgcsconverter",     "TClonesArray",          &fMCGCSConverterEvents, 32000,
                          99)->SetCompressionLevel(fSimCompressionLevel);
      }
   } else {
      fSimFile = 0;
   }

   if (fSevFileActive) {
      std::string tmpName = fSevFileName;
      size_t pos;
      while ((pos = tmpName.find('#')) != std::string::npos) {
         std::ostringstream o;
         o << std::setw(5) << setfill('0') << fRunManager->GetCurrentRun()->GetRunID();
         tmpName.replace(pos, 1, o.str().c_str());
      }
      G4cout << "Output file : " << tmpName << G4endl;
      if (!overwrite && !gSystem->AccessPathName(tmpName.c_str(), kFileExists)) {
         openFileFailed = true;
         return false;
      }
      fSevFile = new TFile(tmpName.c_str(), "RECREATE");
      fSevTree = new TTree("sev", "sev");

      // Branches
      fSevTree->Branch("Info",         "ROMETreeInfo", &fInfo,              32000,
                       99)->SetCompressionLevel(fSevCompressionLevel);
      if (sevkineBranchActive) {
         fSevTree->Branch("sevkine",   "TClonesArray", &fMCKineSubevents,   32000,
                          0)->SetCompressionLevel(fSevCompressionLevel);
      }
      if (sevtrackBranchActive) {
         fSevTree->Branch("sevtrack",  "TClonesArray", &fMCTrackSubevents,  32000,
                          0)->SetCompressionLevel(fSevCompressionLevel);
      }
      if (sevtargetBranchActive) {
         fSevTree->Branch("sevtar",    "TClonesArray", &fMCTargetSubevents, 32000,
                          0)->SetCompressionLevel(fSevCompressionLevel);
      }
      if (sevsvtBranchActive) {
         fSevTree->Branch("sevsvt",    "TClonesArray", &fMCSVTSubevents,    32000,
                          0)->SetCompressionLevel(fSevCompressionLevel);
      }
   } else {
      fSevFile = 0;
   }
   return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMRootIO::WriteRunHeaders()
{
   if (fSimFileActive && fSimFile) {
      fSimFile->cd();
      fUnits->Write("MEGUnits", TObject::kOverwrite);
      fMCSVTRunHeader->Write("MCSVTRunHeader", TObject::kOverwrite);
   }

   if (fSevFileActive && fSevFile) {
      fSevFile->cd();
      fUnits->Write("MEGUnits", TObject::kOverwrite);
      fMCSVTRunHeader->Write("MCSVTRunHeader", TObject::kOverwrite);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMRootIO::~GEMRootIO()
{
   if (fMCTrackEvents)      {
      fMCTrackEvents->Delete();
   }
   if (fMCSVTPixelEvents)     {
      fMCSVTPixelEvents->Delete();
   }
   if (fMCGCSWireEvents)     {
      fMCGCSWireEvents->Delete();
   }
   if (fMCGCSConverterEvents)     {
      fMCGCSConverterEvents->Delete();
   }

   delete fInfo;
   delete fUnits;
   delete fMCSVTRunHeader;
   delete fMCMixtureInfoEvent;
   delete fMCKineEvent;
   delete fMCTrackEvents;
   delete fMCKineSubevents;
   delete fMCTargetEvents;
   delete fMCTargetSubevents;
   delete fMCSVTEvent;
   delete fMCSVTPixelEvents;
   delete fMCGCSWireEvents;
   delete fMCGCSConverterEvents;
   delete fMCSVTSubevents;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMRootIO* GEMRootIO::GetInstance()
{
   if (instance == 0) {
      instance = new GEMRootIO();
   }
   return instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMRootIO::Fill()
{
   fInfo->SetRunNumber(fRunManager->GetCurrentRun()->GetRunID());
   fInfo->SetEventNumber(fRunManager->GetCurrentEvent()->GetEventID());

   if (!fFillThisEvent) {
      return;
   }

   if (fSimTree) {
      fSimTree->Fill();
   }
   if (fSevTree) {
      fSevTree->Fill();
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMRootIO::Close()
{
   if (fSimFile && fSimTree) {
      fSimFile->cd();
      fSimTree->Write(0, TObject::kOverwrite);
      fSimFile->Close();
      delete fSimFile;
      fSimFile = 0;
      fSimTree = 0;
   }
   if (fSevFile && fSevTree) {
      fSevFile->cd();
      fSevTree->Write(0, TObject::kOverwrite);
      fSevFile->Close();
      delete fSevFile;
      fSevFile = 0;
      fSevTree = 0;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
