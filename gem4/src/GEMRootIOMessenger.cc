//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "GEMRootIOMessenger.hh"
#include "GEMRootIO.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMRootIOMessenger::GEMRootIOMessenger(GEMRootIO* io)
   : rootIO(io)
{
   outDir = new G4UIdirectory("/gem/output/");
   outDir->SetGuidance("File output control");

   simDir = new G4UIdirectory("/gem/output/sim/");
   simDir->SetGuidance("\"sim\" ROOT file output control");

   simBrDir = new G4UIdirectory("/gem/output/sim/branches/");
   simBrDir->SetGuidance("\"sim\" branches control");

   overwriteCmd = new G4UIcmdWithABool("/gem/output/overwrite", this);
   overwriteCmd->SetGuidance("Overwrite output files when files with the same names exist");
   overwriteCmd->SetDefaultValue(1);

   simFileActiveCmd = new G4UIcmdWithABool("/gem/output/sim/active", this);
   simFileActiveCmd->SetGuidance("Flag to write \"sim\" ROOT file");
   simFileActiveCmd->SetDefaultValue(1);

   simFileNameCmd = new G4UIcmdWithAString("/gem/output/sim/filename", this);
   simFileNameCmd->SetGuidance("File name of \"sim\" ROOT file");
   simFileNameCmd->SetDefaultValue("sim.root");

   simCompressionLevelCmd = new G4UIcmdWithAnInteger("/gem/output/sim/compressionLevel", this);
   simCompressionLevelCmd->SetGuidance("Compression level of \"sim\" ROOT file (0-9)");
   simCompressionLevelCmd->SetDefaultValue(1);

   mcmixeventBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sim/branches/mcmixeventActive", this);
   mcmixeventBranchActiveCmd->SetGuidance("Flag to write \"mcmixevent\" branch");
   mcmixeventBranchActiveCmd->SetDefaultValue(1);

   mckineBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sim/branches/mckineActive", this);
   mckineBranchActiveCmd->SetGuidance("Flag to write \"mckine\" branch");
   mckineBranchActiveCmd->SetDefaultValue(1);

   mctrackBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sim/branches/mctrackActive", this);
   mctrackBranchActiveCmd->SetGuidance("Flag to write \"mctrack\" branch");
   mctrackBranchActiveCmd->SetDefaultValue(1);

   mctargetBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sim/branches/mctargetActive", this);
   mctargetBranchActiveCmd->SetGuidance("Flag to write \"mctarget\" branch");
   mctargetBranchActiveCmd->SetDefaultValue(1);


   sevDir = new G4UIdirectory("/gem/output/sev/");
   sevDir->SetGuidance("\"sev\" ROOT file output control");

   sevBrDir = new G4UIdirectory("/gem/output/sev/branches/");
   sevBrDir->SetGuidance("\"sev\" branches control");

   sevFileActiveCmd = new G4UIcmdWithABool("/gem/output/sev/active", this);
   sevFileActiveCmd->SetGuidance("Flag to write \"sev\" ROOT file");
   sevFileActiveCmd->SetDefaultValue(1);

   sevFileNameCmd = new G4UIcmdWithAString("/gem/output/sev/filename", this);
   sevFileNameCmd->SetGuidance("File name of \"sev\" ROOT file");
   sevFileNameCmd->SetDefaultValue("sev.root");

   sevCompressionLevelCmd = new G4UIcmdWithAnInteger("/gem/output/sev/compressionLevel", this);
   sevCompressionLevelCmd->SetGuidance("Compression level of \"sev\" ROOT file (0-9)");
   sevCompressionLevelCmd->SetDefaultValue(1);

   sevkineBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sev/branches/sevkineActive", this);
   sevkineBranchActiveCmd->SetGuidance("Flag to write \"sevkine\" branch");
   sevkineBranchActiveCmd->SetDefaultValue(1);

   sevtrackBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sev/branches/sevtrackActive", this);
   sevtrackBranchActiveCmd->SetGuidance("Flag to write \"sevtrack\" branch");
   sevtrackBranchActiveCmd->SetDefaultValue(1);

   sevtargetBranchActiveCmd = new G4UIcmdWithABool("/gem/output/sev/branches/sevtargetActive", this);
   sevtargetBranchActiveCmd->SetGuidance("Flag to write \"sevtar\" branch");
   sevtargetBranchActiveCmd->SetDefaultValue(1);

   saveTrajectoryCmd = new G4UIcmdWithABool("/gem/output/saveTrajectory", this);
   saveTrajectoryCmd->SetGuidance("Flag to write \"trackbuf\" in MCTrackSubevent and MCTrackEvent branch");
   saveTrajectoryCmd->SetDefaultValue(1);

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMRootIOMessenger::~GEMRootIOMessenger()
{
   delete overwriteCmd;
   delete simFileActiveCmd;
   delete simFileNameCmd;
   delete simCompressionLevelCmd;
   delete mcmixeventBranchActiveCmd;
   delete mckineBranchActiveCmd;
   delete mctrackBranchActiveCmd;
   delete mctargetBranchActiveCmd;
   delete sevFileActiveCmd;
   delete sevFileNameCmd;
   delete sevCompressionLevelCmd;
   delete sevkineBranchActiveCmd;
   delete sevtrackBranchActiveCmd;
   delete sevtargetBranchActiveCmd;
   delete saveTrajectoryCmd;

   delete outDir;
   delete simDir;
   delete simBrDir;
   delete sevDir;
   delete sevBrDir;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void GEMRootIOMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
   if (command == overwriteCmd) {
      rootIO->SetOverwrite(overwriteCmd->GetNewBoolValue(newValue));

   } else if (command == simFileActiveCmd) {
      rootIO->SetSimFileActive(simFileActiveCmd->GetNewBoolValue(newValue));

   } else if (command == simFileNameCmd) {
      rootIO->SetSimFileName(newValue);

   } else if (command == simCompressionLevelCmd) {
      rootIO->SetSimFileCompressionLevel(simCompressionLevelCmd->GetNewIntValue(newValue));

   } else if (command == mcmixeventBranchActiveCmd) {
      rootIO->SetmcmixeventBranchActive(mcmixeventBranchActiveCmd->GetNewBoolValue(newValue));

   } else if (command == mckineBranchActiveCmd) {
      rootIO->SetmckineBranchActive(mckineBranchActiveCmd->GetNewBoolValue(newValue));

   } else if (command == mctrackBranchActiveCmd) {
      rootIO->SetmctrackBranchActive(mctrackBranchActiveCmd->GetNewBoolValue(newValue));

   } else if (command == mctargetBranchActiveCmd) {
      rootIO->SetmctargetBranchActive(mctargetBranchActiveCmd->GetNewBoolValue(newValue));

   } else if (command == sevFileActiveCmd) {
      rootIO->SetSevFileActive(sevFileActiveCmd->GetNewBoolValue(newValue));

   } else if (command == sevFileNameCmd) {
      rootIO->SetSevFileName(newValue);

   } else if (command == sevCompressionLevelCmd) {
      rootIO->SetSevFileCompressionLevel(sevCompressionLevelCmd->GetNewIntValue(newValue));

   } else if (command == sevkineBranchActiveCmd) {
      rootIO->SetsevkineBranchActive(sevkineBranchActiveCmd->GetNewBoolValue(newValue));

   } else if (command == sevtrackBranchActiveCmd) {
      rootIO->SetsevtrackBranchActive(sevtrackBranchActiveCmd->GetNewBoolValue(newValue));

   } else if (command == sevtargetBranchActiveCmd) {
      rootIO->SetsevtargetBranchActive(sevtargetBranchActiveCmd->GetNewBoolValue(newValue));

   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
