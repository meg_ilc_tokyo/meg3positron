//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "generated/MEGUnits.h"
#include "G4Timer.hh"
#include "G4RunManager.hh"
#include "GEMRunAction.hh"
#include "G4Run.hh"
#include "GEMRootIO.hh"
#include "GEMUnitConversion.hh"
#include "G4SystemOfUnits.hh"
//
using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMRunAction::GEMRunAction()
   : modules()
   , timer(new G4Timer)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMRunAction::~GEMRunAction()
{
   delete timer;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMRunAction::BeginOfRunAction(const G4Run* aRun)
{
   G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
   timer->Start();

   FillUnits();

   // Open output file
   if (!GEMRootIO::GetInstance()->Open()) {
      // how to stop a run from here ?
      // G4RunManager::GetRunManager()->AbortRun();
   }

   G4UserRunAction *act;
   std::vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetRunAction())) {
         act->BeginOfRunAction(aRun);
      }
   }

   GEMRootIO::GetInstance()->WriteRunHeaders();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMRunAction::EndOfRunAction(const G4Run* aRun)
{
   G4UserRunAction *act;
   vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetRunAction())) {
         act->EndOfRunAction(aRun);
      }
   }

   // Close output file
   GEMRootIO::GetInstance()->Close();

   timer->Stop();
   G4cout << "number of event = " << aRun->GetNumberOfEvent()
          << " " << *timer << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMRunAction::FillUnits()
{
   // Fill units used for variables in ROOT files

   MEGUnits *units = GEMRootIO::GetInstance()->GetUnits();

   units->SetMeter(meter         * kUnitConvLength);
   units->SetRadian(radian       * kUnitConvAngle);
   units->SetSecond(second       * kUnitConvTime);
   units->SetCoulomb(coulomb     * kUnitConvCharge);
   units->SetJoule(joule         * kUnitConvEnergy);
   units->SetKelvin(kelvin       * kUnitConvTemperature);
   units->SetMole(mole           * 1);
   units->SetCandela(candela     * kUnitConvLuminousIntensity);
   units->SetKiloGram(kilogram   * kUnitConvWeight);
   units->SetPascal(hep_pascal   * kUnitConvPressure);
   units->SetAmpere(ampere       * kUnitConvCurrent);
   units->SetVolt(volt           * kUnitConvVoltage);
   units->SetOhm(ohm             * kUnitConvResistance);
   units->SetFarad(farad         * kUnitConvCapacitance);
   units->SetWeber(weber         * kUnitConvMagneticFlux);
   units->SetTesla(tesla         * kUnitConvMagneticFluxDensity);
   units->SetHenry(henry         * kUnitConvInductance);
   units->SetBecquerel(becquerel * kUnitConvRadioactivity);
   units->SetGray(gray           * kUnitConvDose);
   units->SetLumen(lumen         * kUnitConvLuminousFlux);
   units->SetLux(lux             * kUnitConvLuminousEmittance);

   return;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
