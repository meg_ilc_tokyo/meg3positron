//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "GEMSteppingAction.hh"

#include "G4SteppingManager.hh"
#include "G4SDManager.hh"
#include "G4EventManager.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessType.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4VProcess.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4EmProcessSubType.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMUserPrimaryVertexInformation.hh"
#include "G4SystemOfUnits.hh"

#include "TMath.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMSteppingAction::GEMSteppingAction()
   : modules()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMSteppingAction::~GEMSteppingAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMSteppingAction::UserSteppingAction(const G4Step *theStep)
{
   G4Track     *theTrack     = theStep->GetTrack();
   G4StepPoint *thePostPoint = theStep->GetPostStepPoint();
   GEMUserTrackInformation* ui =
      dynamic_cast<GEMUserTrackInformation*>(theTrack->GetUserInformation());

   const G4VProcess* process = theStep->GetPostStepPoint()->GetProcessDefinedStep();
   G4int type = process->GetProcessType();
   G4int subType = process->GetProcessSubType();


   if (!(type == fElectromagnetic && subType == fCoulombScattering &&
         theStep->GetTrack()->GetDefinition() == G4MuonPlus::MuonPlusDefinition())) {
      // decrease step points for muon beam because the output files become too large.

      // save only finite length step
      if (thePostPoint->GetGlobalTime() - theStep->GetPreStepPoint()->GetGlobalTime() != 0) {
         //if (static_cast<Float_t>(thePostPoint->GetGlobalTime()) - static_cast<Float_t>(theStep->GetPreStepPoint()->GetGlobalTime()) != 0) {

         if (!ui->trackInfo.nstep) {
            // put the creation point of the track
            G4StepPoint *thePrePoint = theStep->GetPreStepPoint();
            ui->trackInfo.nstep++;
            ui->trackInfo.trackbufx.push_back(thePrePoint->GetPosition().x());
            ui->trackInfo.trackbufy.push_back(thePrePoint->GetPosition().y());
            ui->trackInfo.trackbufz.push_back(thePrePoint->GetPosition().z());
            ui->trackInfo.trackbuft.push_back(thePrePoint->GetGlobalTime());
            ui->trackInfo.trackbufde.push_back(0);
         }

         ui->trackInfo.nstep++;
         ui->trackInfo.trackbufx.push_back(thePostPoint->GetPosition().x());
         ui->trackInfo.trackbufy.push_back(thePostPoint->GetPosition().y());
         ui->trackInfo.trackbufz.push_back(thePostPoint->GetPosition().z());
         ui->trackInfo.trackbuft.push_back(thePostPoint->GetGlobalTime());
         ui->trackInfo.trackbufde.push_back(theStep->GetTotalEnergyDeposit());

         // counting #of turns only for e- or e+
         if (theStep->GetTrack()->GetDefinition() == G4Electron::ElectronDefinition() ||
             theStep->GetTrack()->GetDefinition() == G4Positron::PositronDefinition()) {
            G4float pabs, pold, cosphi, sinphi, phi;
            G4int istep = ui->trackInfo.nstep;
            if (istep > 2) {
               G4double vold[2] = {ui->trackInfo.trackbufx[istep - 2] - ui->trackInfo.trackbufx[istep - 3],
                                   ui->trackInfo.trackbufy[istep - 2] - ui->trackInfo.trackbufy[istep - 3]
                                  };
               G4double vect[2] = {ui->trackInfo.trackbufx[istep - 1] - ui->trackInfo.trackbufx[istep - 2],
                                   ui->trackInfo.trackbufy[istep - 1] - ui->trackInfo.trackbufy[istep - 2]
                                  };
               double pdir = vect[0] * (ui->trackInfo.trackbufx[istep - 1] + ui->trackInfo.trackbufx[istep - 2])
                             + vect[1] * (ui->trackInfo.trackbufy[istep - 1] + ui->trackInfo.trackbufy[istep - 2]);
               double pdirold = vold[0] * (ui->trackInfo.trackbufx[istep - 2] + ui->trackInfo.trackbufx[istep - 3])
                                + vold[1] * (ui->trackInfo.trackbufy[istep - 2] + ui->trackInfo.trackbufy[istep - 3]);
               pold = TMath::Hypot(vold[0], vold[1]);
               pabs = TMath::Hypot(vect[0], vect[1]);
               if (pabs > 1.*um) {
                  //reverse to first not small step to avoid floating point precision
                  if (!(pold > 1.*um)) {
                     int istepold = istep - 1;
                     while ((!(pold > 1.*um)) && istepold > 2) {
                        vold[0] = ui->trackInfo.trackbufx[istepold - 2] - ui->trackInfo.trackbufx[istepold - 3];
                        vold[1] = ui->trackInfo.trackbufy[istepold - 2] - ui->trackInfo.trackbufy[istepold - 3];
                        pold = TMath::Hypot(vold[0], vold[1]);
                        pdirold = vold[0] * (ui->trackInfo.trackbufx[istepold - 2] + ui->trackInfo.trackbufx[istepold - 3])
                                  + vold[1] * (ui->trackInfo.trackbufy[istepold - 2] + ui->trackInfo.trackbufy[istepold - 3]);
                        istepold--;
                     }
                  }
                  if (pold > 1.*um) {
                     cosphi = (vect[0] * vold[0] + vect[1] * vold[1]) / (pabs * pold);
                     sinphi = (vold[0] * vect[1] - vold[1] * vect[0]) / (pabs * pold);

                     phi = -TMath::ATan2(sinphi, cosphi);
                     if (phi < -TMath::Pi() / 2) {
                        phi += 2 * TMath::Pi();
                     }

                     ui->trackInfo.trktotphi += phi;
                     /*int nturn=(G4int)(fabs(ui->trackInfo.trktotphi)/TMath::TwoPi()) + 1;
                       if(nturn>ui->trackInfo.nturn) ui->trackInfo.nturn=nturn;
                     */
                     /*std::cout<<istep<<" "<<pdirold<<" "<<pdir<<" "<<ui->trackInfo.nturn<<" "<<ui->trackInfo.trktotphi/TMath::TwoPi()
                       <<" "<<ui->trackInfo.trackbufx[istep-1]<<" "<<ui->trackInfo.trackbufy[istep-1]<<" "<<ui->trackInfo.trackbufz[istep-1]<<" "<<ui->trackInfo.trackbufde[istep-1]
                       <<" "<<ui->trackInfo.trackbufx[istep-2]<<" "<<ui->trackInfo.trackbufy[istep-2]<<" "<<ui->trackInfo.trackbufz[istep-2]<<" "<<ui->trackInfo.trackbufde[istep-2]
                       <<" "<<ui->trackInfo.trackbufx[istep-3]<<" "<<ui->trackInfo.trackbufy[istep-3]<<" "<<ui->trackInfo.trackbufz[istep-3]<<" "<<ui->trackInfo.trackbufde[istep-3]
                       <<std::endl;*/
                     // define turn change as crossing closest point to Z-axis
                     if (fabs(ui->trackInfo.trktotphi) - 2 * TMath::Pi() * (ui->trackInfo.nturn - 1. / 4) > 0 &&
                         pdirold <= 0 && pdir > 0) {
                        ui->trackInfo.nturn++;
                     }
                  }
               }
            }
         }
      }
   }


   G4UserSteppingAction *act;
   std::vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetSteppingAction())) {
         act->UserSteppingAction(theStep);
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
