//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include <vector>
#include "G4VisAttributes.hh"
#include "BMUModule.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4UnionSolid.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "ROMESQLDataBase.h"
#include "ROMEString.h"
#include "ROMEStr2DArray.h"
#include "GEMConstants.hh"
#include "MAGConstants.hh"

namespace
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void BMUModule::ConstructMaterials()
{
   Vacuum  = G4NistManager::Instance()->FindOrBuildMaterial("G4_Galactic");
   Air     = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
   Lead    = G4NistManager::Instance()->FindOrBuildMaterial("G4_Pb");

   G4Element *elH  = G4NistManager::Instance()->FindOrBuildElement("H");
   G4Element *elC  = G4NistManager::Instance()->FindOrBuildElement("C");
   G4Element *elO  = G4NistManager::Instance()->FindOrBuildElement("O");
#if 0
   G4Element *elAl = G4NistManager::Instance()->FindOrBuildElement("Al");
   G4Element* elFe = G4NistManager::Instance()->FindOrBuildElement("Fe");
#endif

   G4double density = 0.;

   density = 1.395 * g / cm3;
   mabmumy1 = new G4Material("Mylar for degrader", density, 3);
   mabmumy1->AddElement(elH, 4);
   mabmumy1->AddElement(elC, 5);
   mabmumy1->AddElement(elO, 2);

   density = 1.377 * g / cm3;
   mabmumy2 = new G4Material("Mylar for vac. window", density, 3);
   mabmumy2->AddElement(elH, 4);
   mabmumy2->AddElement(elC, 5);
   mabmumy2->AddElement(elO, 2);

#if 1
   density = 0.922 * g / cm3;
   mabmupe = new G4Material("PolyethyleneBMU", density, 2);
   mabmupe->AddElement(elH, 2);
   mabmupe->AddElement(elC, 1);
#else
   mabmupe = G4NistManager::Instance()->FindOrBuildMaterial("G4_POLYETHYLENE");
#endif

   //
   // Alminium for cooling beam pipe
#if 0
   density = 2.8 * g / cm3;
   MAAL = new G4Material(name = "Aluminium", density, 1);
   MAAL->AddElement(elAl, 1.);
#else
   MAAL = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");
#endif

   //
   // Currently Fe is used instead of real SUS
#if 0
   density = 7.874 * g / cm3;
   MASUS = new G4Material(name = "SUS", density, 1);
   MASUS->AddElement(elFe, 1.);
#else
   MASUS = G4NistManager::Instance()->FindOrBuildMaterial("G4_Fe");
#endif
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void BMUModule::Construct(G4LogicalVolume *parent)
{
   if (!materialConstructed) {
      ConstructMaterials();
      materialConstructed = true;
   }

   G4ThreeVector trans;
   G4VisAttributes *bmuVis = new G4VisAttributes(G4VisAttributes::GetInvisible());

   //-- Volume parameters for the upstream beam elements
   // const G4double zbmu  = -142. * cm;       // inner z edge of the upstream beam elements mother
   const G4double zbmu = -(ALMAG1 + ALMAG2 + ALMAG3
                           + ALMAG4 / 2);   // inner z edge of the upstream beam element mother
   // = the edge of COBRA = the edge of FGAS volume
   const G4double lbmu  = 565. * cm + zbmu; // length of the upstream beam elements mother
   const G4double zcbmu = zbmu - lbmu / 2.; // center of the upstream beam elements mother
   const G4double rbmu  = 60. * cm;         // radius of the upstream beam elements mother

   //-- Volume parameters for BTS
   const G4double lbts       = 281. * cm;
   const G4double zdbts      = zbmu - 40.2 * cm;
   const G4double zubts      = zdbts - lbts;
   const G4double r1btscryo  = 19.0 * cm;
   const G4double r2btscryo  = 32.5 * cm;
   const G4double rt1btscryo =  0.4 * cm;
   const G4double rt2btscryo =  0.4 * cm;
   const G4double tbtsuep    =   3. * cm;
   const G4double tbtsdep    =   3. * cm;
   const G4double rbtsvac    = r1btscryo;
   const G4double lbtsvac    = lbts;

   //-- Volume parameters for beam pipe in front of the BTS
   const G4double zubmpu   = zbmu - lbmu;
   const G4double zubmpd   = zubts;
   const G4double lubmp    = zubmpd - zubmpu;
   const G4double tubmpfl  = 3. * cm;
   const G4double r1ubmp   = 31.8 / 2. * cm;
   const G4double rtubmp   = 0.3 * cm;
   const G4double r2ubmp   = r1ubmp + rtubmp;
   const G4double r1ubmpfl = r1ubmp;
   const G4double r2ubmpfl = 42.5 / 2. * cm;
   const G4double rubmpvac = r1ubmp;
//   const G4double lubmpvac = lubmp;

   //-- Volume parameters for degrader in the BTS
   const G4double rdgr1    = rubmpvac;
   const G4double zdgr1    = -322.5 * cm; // Z of DGR1 in MEG mother CS (-322.5cm for Bbts=-3.55kG)

   //-- Volume parameters for collimator
   const G4double tbcol  =  5.0 * cm;            // Collimator thickness (cm)
   const G4double ribcol =  6.0 * cm;            // Collimator inner radius (cm)
   const G4double robcol = 15.0 * cm;            // Collimator outer radius (cm)
   const G4double zbcol  = (-564.7 + 76.0) * cm; // Z of collimator center

//   G4int MDGR1 = 2;   // degrader material 1/Polyethylene, 2/Mylar

   //-- Volume parameters for upstream end-cap mother
   const G4double luendcap = 40.2 * cm; // length in z
   const G4double tuendcap = 30.2 * cm; // thickness
   const G4double ruendcap = 60. * cm;  // radius
   const G4double zuendcap = lbmu / 2. - luendcap / 2.; // z position of the end-cap center

   //-- Volume parameters for inner flange
   const G4double liecf    = 2.2 * cm;  // length in z
   const G4double tiecf    = 2.3 * cm;  // thickness in r
   const G4double r1iecf1  = 23.7 * cm; // rmin
   const G4double r2iecf1  = r1iecf1 + tiecf; // rmax
   const G4double p1iecf1  = 125. * deg;
   const G4double p2iecf1  =  55. * deg;
   const G4double r1iecf2  = 36.2 * cm; // rmin
   const G4double r2iecf2  = r1iecf2 + tiecf; // rmax
   const G4double p1iecf2  =  30. * deg;
   const G4double p2iecf2  = 150. * deg;
   const G4double ziecf    = luendcap / 2. - liecf / 2.; // z position

   //-- Volume parameters for inner wall
   const G4double liecw    = 29. * cm; // length in z
   const G4double tiecw    = 0.4 * cm; // thickness in r
   const G4double r1iecw1  = 26. * cm; // rmin
   const G4double r2iecw1  = r1iecw1 + tiecw; // rmin
   const G4double p1iecw1  = 135. * deg;
   const G4double p2iecw1  = 45. * deg;
   const G4double r1iecw2  = 38.5 * cm; // rmin
   const G4double r2iecw2  = r1iecw2 + tiecw; // rmin
   const G4double p1iecw2  = 30. * deg;
   const G4double p2iecw2  = 150. * deg;
   const G4double ziecw    = luendcap / 2. - liecf - liecw / 2.; // z position

   //-- Volume parameters for outer flange
   const G4double loecf    = 3. * cm; // length in z
   const G4double r1oecf   = 53.5 * cm; // rmin
   const G4double r2oecf   = r1oecf + 5. * cm; // rmax
   const G4double zoecf    = luendcap / 2. - loecf / 2.; // z position

   //-- Volume parameters for outer wall
   const G4double loecw    = tuendcap - loecf; // length in z
   const G4double r1oecw   = r1oecf; // rmin
   const G4double r2oecw   = r1oecf + 0.5 * cm; // rmax
   const G4double zoecw    = luendcap / 2. - loecf - loecw / 2.; // z position

   //-- Volume parameters for beam pipe flange
   const G4double lbpf1    = 6.5 * cm; // length in z
   const G4double r1bpf1   = 31.8 / 2. * cm; // rmin
   const G4double r2bpf1   = 42.5 / 2. * cm; // rmax
   const G4double zbpf1    = luendcap / 2. - lbpf1 / 2.; // z position in 'ECAP'

   //-- Volume parameters for beam pipe wall
   const G4double lbpw1    = 40.2 * cm - lbpf1; // length in z
   const G4double r1bpw1   = r1bpf1; // rmin
   const G4double r2bpw1   = r1bpw1 + 0.3 * cm; // rmax
   const G4double zbpw1    = luendcap / 2. - lbpf1 - lbpw1 / 2.; // z position in 'ECAP'

   //-- Volume parameters for vacuum window
   const G4double lecvw    = 0.019 * cm; // length in z
   const G4double zecvw    = luendcap / 2. - lecvw / 2.; // z position in 'ECAP'

   //-- Volume parameters for vacuum pipe
   const G4double zecvc    = -lecvw / 2.; // z position in 'ECAP'

   //--   upstream end-cap
   G4double vpuendcap[3] = {0., ruendcap, luendcap / 2.};
   //--   Al outer flange in upstream end-cap
   G4double vpoecf[3]    = {r1oecf, r2oecf, loecf / 2.};
   //--   Al outer wall in upstream end-cap
   G4double vpoecw[3]    = {r1oecw, r2oecw, loecw / 2.};
   //--   SUS beam pipe flange in upstream end-cap
   G4double vpbpf1[3]    = {r1bpf1, r2bpf1, lbpf1 / 2.};
   //--   SUS beam pipe wall in upstream end-cap
   G4double vpbpw1[3]    = {r1bpw1, r2bpw1, lbpw1 / 2.};
   //--   Vacuum window in beam pipe
   G4double vpecvw[3]    = {0., r1bpf1, lecvw / 2.};
   //--   Vacuum tube in beam pipe
   G4double vpecvc[3]    = {0., r1bpf1, luendcap / 2. - lecvw / 2.};


   //
   //-- Define beamline mother
   //
   G4double vpbmu[3]   = {0., rbmu, lbmu / 2.};
   G4Tubs *BMUSol = new G4Tubs("BMUSol", vpbmu[0], vpbmu[1], vpbmu[2], 0., 360.*deg);
   G4LogicalVolume *BMULog = new G4LogicalVolume(BMUSol, Air, "BMULog", 0, 0, 0);
   BMULog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zcbmu), BMULog, "BMUPhys", parent, false, 0);

   //
   //-- Define upstream end-cap mother
   //
   G4Tubs *UECPSol = new G4Tubs("UECPSol", vpuendcap[0], vpuendcap[1], vpuendcap[2], 0., 360.*deg);
   G4LogicalVolume *UECPLog = new G4LogicalVolume(UECPSol, fMACONHE, "UECPLog", 0, 0, 0);
   UECPLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zuendcap), UECPLog, "UECPPhys", BMULog, false, 0);

   //
   //-- Define realistic end-cap
   //
   //-- inner flange(1)
   G4double vpiecf1[5] = {r1iecf1, r2iecf1, liecf / 2., p1iecf1, p2iecf1};
   G4Tubs *ECI1Sol = new G4Tubs("ECI1Sol", vpiecf1[0], vpiecf1[1], vpiecf1[2],
                                vpiecf1[3],
                                vpiecf1[4] + 360.*deg - vpiecf1[3]);

   //-- inner flange(2)
   G4double vpiecf2[5] = {r1iecf2, r2iecf2, liecf / 2., p1iecf2, p2iecf2};
   vpiecf2[4] -= vpiecf2[3];
   G4Tubs *ECI2Sol = new G4Tubs("ECI2Sol", vpiecf2[0], vpiecf2[1], vpiecf2[2], vpiecf2[3], vpiecf2[4]);

   //-- inner flange(3)
   G4double xecf1, xecf2, dxc, cxc, xiecf3, yiecf3;
   xecf1 = r2iecf2 * cos(p1iecf2) - tiecf / cos(p1iecf2);
   xecf2 = r2iecf1 * cos(p2iecf1);
   dxc   = (xecf1 - xecf2);
   cxc   = (xecf1 + xecf2) / 2.;
   xiecf3     = cxc;
   yiecf3     = r2iecf1 * sin(p2iecf1) - tiecf / 2.;
   G4double vpiecf3[3] = {
      dxc / 2., tiecf / 2.,
      liecf / 2. - 1 * micrometer /*not to share surface*/
   };
   G4Box *ECI3Sol = new G4Box("ECI3Sol", vpiecf3[0], vpiecf3[1], vpiecf3[2]);

   trans.setX(xiecf3);
   trans.setY(yiecf3);
   trans.setZ(0);
   G4UnionSolid *ECI13   = new G4UnionSolid("ECI13",   ECI1Sol,   ECI3Sol, 0, trans);
   trans.setX(0);
   trans.setY(0);
   trans.setZ(0);
   G4UnionSolid *ECI123  = new G4UnionSolid("ECI123",  ECI13,  ECI2Sol, 0, trans);
   trans.setX(-xiecf3);
   trans.setY(yiecf3);
   trans.setZ(0);
   G4UnionSolid *ECI1234 = new G4UnionSolid("ECI1234", ECI123, ECI3Sol, 0, trans);
   G4LogicalVolume *ECI1234Log = new G4LogicalVolume(ECI1234, MAAL, "ECI1234Log", 0, 0, 0);
   ECI1234Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0, 0, ziecf), ECI1234Log, "ECI1234Phys", UECPLog, true, 0);

   //-- inner wall(1)
   G4double vpiecw1[5] = {r1iecw1, r2iecw1, liecw / 2., p1iecw1, p2iecw1};
   G4Tubs *ECI5Sol = new G4Tubs("ECI5Sol", vpiecw1[0], vpiecw1[1], vpiecw1[2],
                                vpiecw1[3],
                                vpiecw1[4] + 360.*deg - vpiecw1[3]);

   //-- inner wall(2)
   G4double vpiecw2[5] = {r1iecw2, r2iecw2, liecw / 2., p1iecw2, p2iecw2};
   vpiecw2[4] -= vpiecw2[3];
   G4Tubs *ECI6Sol = new G4Tubs("ECI6Sol", vpiecw2[0], vpiecw2[1], vpiecw2[2], vpiecw2[3], vpiecw2[4]);
   G4LogicalVolume *ECI6Log = new G4LogicalVolume(ECI6Sol, MAAL, "ECI6Log", 0, 0, 0);
   ECI6Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., ziecw), ECI6Log, "ECI6Phys", UECPLog, true, 0);

   //-- inner wall(3)
   G4double xec1, xec2, dx, cx, xiecw3, yiecw3;
   xec1 = vpiecw2[1] * cos(vpiecw2[3]) - tiecw / cos(vpiecw2[3]);
   xec2 = vpiecw1[1] * cos(vpiecw1[4]);
   dx = (xec1 - xec2);
   cx = (xec1 + xec2) / 2.;
   xiecw3 = cx;
   yiecw3 = vpiecw1[1] * sin(vpiecw1[4]) - tiecw / 2.;
   G4double vpiecw3[3] = {
      dx / 2., tiecw / 2.,
      liecw / 2. - 1 * micrometer /*not to share surface*/
   };
   G4Box *ECI7Sol = new G4Box("ECI7Sol", vpiecw3[0], vpiecw3[1], vpiecw3[2]);

   trans.setX(xiecw3);
   trans.setY(yiecw3);
   trans.setZ(0);
   G4UnionSolid *ECI57 = new G4UnionSolid("ECI57", ECI5Sol, ECI7Sol, 0, trans);
   trans.setX(-xiecw3);
   trans.setY(yiecw3);
   trans.setZ(0);
   G4UnionSolid *ECI578 = new G4UnionSolid("ECI578", ECI57, ECI7Sol, 0, trans);
   G4LogicalVolume *ECI578Log = new G4LogicalVolume(ECI578, MAAL, "ECI578Log", 0, 0, 0);
   ECI578Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., ziecw), ECI578Log, "ECI578Phys", UECPLog, true, 0);

   //-- outer flange
   G4Tubs *ECO1Sol = new G4Tubs("ECO1Sol", vpoecf[0], vpoecf[1], vpoecf[2], 0., 360.*deg);
   G4LogicalVolume *ECO1Log = new G4LogicalVolume(ECO1Sol, MAAL, "ECO1Log", 0, 0, 0);
   ECO1Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zoecf), ECO1Log, "ECO1Phys", UECPLog, true, 1);

   //-- outer wall
   G4Tubs *ECO2Sol = new G4Tubs("ECO2Sol", vpoecw[0], vpoecw[1], vpoecw[2], 0., 360.*deg);
   G4LogicalVolume *ECO2Log = new G4LogicalVolume(ECO2Sol, MAAL, "ECO2Log", 0, 0, 0);
   ECO2Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zoecw), ECO2Log, "ECO2Phys", UECPLog, true, 1);

   //-- beam pipe flange
   G4Tubs *BPF1Sol = new G4Tubs("BPF1Sol", vpbpf1[0], vpbpf1[1], vpbpf1[2], 0., 360.*deg);
   G4LogicalVolume *BPF1Log = new G4LogicalVolume(BPF1Sol, MASUS, "BPF1Log", 0, 0, 0);
   BPF1Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zbpf1), BPF1Log, "BPF1Phys", UECPLog, true, 1);

   //-- beam pipe wall
   G4Tubs *BPW1Sol = new G4Tubs("BPW1Sol", vpbpw1[0], vpbpw1[1], vpbpw1[2], 0., 360.*deg);
   G4LogicalVolume *BPW1Log = new G4LogicalVolume(BPW1Sol, MASUS, "BPW1Log", 0, 0, 0);
   BPW1Log->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zbpw1), BPW1Log, "BPW1Phys", UECPLog, true, 1);

   //
   //-- Vacuum window + vacuum tube
   //

   //-- vacuum window
   G4Tubs *ECVWSol = new G4Tubs("ECVWSol", vpecvw[0], vpecvw[1], vpecvw[2], 0., 360.*deg);
   G4LogicalVolume *ECVWLog = new G4LogicalVolume(ECVWSol, mabmumy2, "ECVWLog", 0, 0, 0);
   ECVWLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zecvw), ECVWLog, "ECVWPhys", UECPLog, true, 1);

   //-- vacuum tube
   G4Tubs *ECVCSol = new G4Tubs("ECVCSol", vpecvc[0], vpecvc[1], vpecvc[2], 0., 360.*deg);
   G4LogicalVolume *ECVCLog = new G4LogicalVolume(ECVCSol, Vacuum, "ECVCLog", 0, 0, 0);
   ECVCLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zecvc), ECVCLog, "ECVCPhys", UECPLog, true, 1);

   G4double vpbcri[3]  = {r1btscryo, r1btscryo + rt1btscryo, (lbts - tbtsuep - tbtsdep) / 2.};
   G4double vpbcro[3]  = {r2btscryo, r2btscryo + rt2btscryo, (lbts - tbtsuep - tbtsdep) / 2.};
   G4double vpbuep[3]  = {r1btscryo, r2btscryo + rt2btscryo, tbtsuep / 2.};
   G4double vpbdep[3]  = {r1btscryo, r2btscryo + rt2btscryo, tbtsdep / 2.};
   G4double vpbtsv[3]  = {0., rbtsvac, lbtsvac / 2.};
   G4double phivpubmp[2] = {0., 360.*deg};
   const G4int nvpubmp = 4;
   std::vector<G4double> zvpubmp(nvpubmp);
   std::vector<G4double> rivpubmp(nvpubmp);
   std::vector<G4double> rovpubmp(nvpubmp);
   zvpubmp[0]   = zubmpd - zcbmu;
   rivpubmp[0]  = r1ubmpfl;
   rovpubmp[0]  = r2ubmpfl;
   zvpubmp[1]   = zubmpd - tubmpfl - zcbmu;
   rivpubmp[1]  = r1ubmpfl;
   rovpubmp[1]  = r2ubmpfl;
   zvpubmp[2]   = zubmpd - tubmpfl - zcbmu;
   rivpubmp[2]  = r1ubmp;
   rovpubmp[2]  = r2ubmp;
   zvpubmp[3]   = zubmpu - zcbmu;
   rivpubmp[3]  = r1ubmp;
   rovpubmp[3]  = r2ubmp;
   G4double vpubpv[3] = {0., rubmpvac, lubmp / 2.};
   G4double vpdgr1[3] = {0., rdgr1, TDGR1 / 2.};
   G4double vpbcol[3] = {ribcol, robcol, tbcol / 2.};

   //
   //-- BTS geometry
   //

   //
   //-- cryostat inner cylinder
   //
   G4Tubs *BCRISol = new G4Tubs("BCRISol", vpbcri[0], vpbcri[1], vpbcri[2], 0., 360.*deg);
   G4LogicalVolume *BCRILog = new G4LogicalVolume(BCRISol, MASUS, "BCRILog", 0, 0, 0);
   BCRILog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zdbts - tbtsdep - vpbcri[2] - zcbmu), BCRILog, "BCRIPhys", BMULog,
                     false, 0);

   //
   //-- cryostat outer cylinder
   //
   G4Tubs *BCROSol = new G4Tubs("BCROSol", vpbcro[0], vpbcro[1], vpbcro[2], 0., 360.*deg);
   G4LogicalVolume *BCROLog = new G4LogicalVolume(BCROSol, MASUS, "BCROLog", 0, 0, 0);
   BCROLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zdbts - tbtsdep - vpbcro[2] - zcbmu), BCROLog, "BCROPhys", BMULog,
                     false, 0);

   //
   //-- cryostat upstream endplate
   //
   G4Tubs *BUEPSol = new G4Tubs("BUEPSol", vpbuep[0], vpbuep[1], vpbuep[2], 0., 360.*deg);
   G4LogicalVolume *BUEPLog = new G4LogicalVolume(BUEPSol, MASUS, "BUEPLog", 0, 0, 0);
   BUEPLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zubts + vpbuep[2] - zcbmu), BUEPLog, "BUEPPhys", BMULog, false, 0);

   //
   //-- cryostat downstream endplate
   //
   G4Tubs *BDEPSol = new G4Tubs("BDEPSol", vpbdep[0], vpbdep[1], vpbdep[2], 0., 360.*deg);
   G4LogicalVolume *BDEPLog = new G4LogicalVolume(BDEPSol, MASUS, "BDEPLog", 0, 0, 0);
   BDEPLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zdbts - vpbuep[2] - zcbmu), BDEPLog, "BDEPPhys", BMULog, false, 0);

   //
   //-- vacuum tube in the BTS
   //
   G4Tubs *BTSVSol = new G4Tubs("BTSVSol", vpbtsv[0], vpbtsv[1], vpbtsv[2], 0., 360.*deg);
   G4LogicalVolume *BTSVLog = new G4LogicalVolume(BTSVSol, Vacuum, "BTSVLog", 0, 0, 0);
   BTSVLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zdbts - vpbtsv[2] - zcbmu), BTSVLog, "BTSVPhys", BMULog, false, 0);

   //
   //-- Beam pipe in front of the BTS
   //
   G4Polycone *UBMPSol = new G4Polycone("UBMPSol", phivpubmp[0], phivpubmp[1], nvpubmp, &zvpubmp[0],
                                        &rivpubmp[0], &rovpubmp[0]);
   G4LogicalVolume *UBMPLog = new G4LogicalVolume(UBMPSol, MASUS, "UBMPLog", 0, 0, 0);
   UBMPLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), UBMPLog, "UBMPPhys", BMULog, false, 0);

   //
   //-- Vacuum tube in the beam pipe in front of the BTS
   //
   G4Tubs *UBPVSol = new G4Tubs("UBPVSol", vpubpv[0], vpubpv[1], vpubpv[2], 0., 360.*deg);
   G4LogicalVolume *UBPVLog = new G4LogicalVolume(UBPVSol, Vacuum, "UBPVLog", 0, 0, 0);
   UBPVLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zubts - vpubpv[2] - zcbmu), UBPVLog, "UBPVPhys", BMULog, true, 0);

   //
   //-- Degrader in the BTS
   //
   if (vpdgr1[2] > 0) {
      G4Tubs *DGR1Sol = new G4Tubs("DGR1Sol", vpdgr1[0], vpdgr1[1], vpdgr1[2], 0., 360.*deg);
      G4LogicalVolume *DGR1Log = new G4LogicalVolume(DGR1Sol, mabmumy1, "DGR1Log", 0, 0, 0);
      DGR1Log->SetVisAttributes(bmuVis);
      new G4PVPlacement(0, G4ThreeVector(0., 0., zdgr1 - zdbts + vpbtsv[2]), DGR1Log, "DGR1Phys", BTSVLog, false,
                        0);
   }

   //
   //-- Beam collimator
   //
   G4Tubs *BCOLSol = new G4Tubs("BCOLSol", vpbcol[0], vpbcol[1], vpbcol[2], 0., 360.*deg);
   G4LogicalVolume *BCOLLog = new G4LogicalVolume(BCOLSol, Lead, "BCOLLog", 0, 0, 0);
   BCOLLog->SetVisAttributes(bmuVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zbcol - zubts + vpubpv[2]), BCOLLog, "BCOLPhys", UBPVLog, false,
                     0);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

BMUModule::BMUModule(const char* n, G4int i)
   : GEMAbsModule(n)
   , idx(i)
   , materialConstructed(false)
   , messenger(0)
   , Air(0)
   , Vacuum(0)
   , Lead(0)
   , mabmumy1(0)
   , mabmumy2(0)
   , mabmupe(0)
   , MAAL(0)
   , MASUS(0)
   , TDGR1(kDefaultDegraderThickness)
{
   fRunAction      = 0;
   fEventAction    = 0;
   fStackingAction = 0;
   fSteppingAction = 0;
   fTrackingAction = 0;

   messenger = new BMUMessenger(this);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

BMUModule::~BMUModule()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserEventInformation* BMUModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserTrackInformation* BMUModule::MakeTrackInformation(const G4Track* /*aTrack*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void BMUModule::ReadDatabase(ROMESQLDataBase *db, G4int run)
{
   ROMEStr2DArray *values = new ROMEStr2DArray(1, 1);
   ROMEString path;

   G4cout << "___ BMU reading from DB ___" << G4endl;
   path.SetFormatted("/RunCatalog[id=%d]/MEGConf_id", run);
   db->Read(values, path, run, 0);
   G4int megid = strtol(values->At(0, 0).Data(), 0, 10);

   path.SetFormatted("/MEGConf[id=%d]/BMUGeometry_id", megid);
   db->Read(values, path, run, 0);
   G4int bmuid = strtol(values->At(0, 0).Data(), 0, 10);
   G4cout << "BeamU_id = " << bmuid << G4endl;


   path.SetFormatted("/BMUGeometry[id=%d]/DGRThickness", bmuid);
   db->Read(values, path, run, 0);
   TDGR1 = strtod(values->At(0, 0).Data(), 0) * cm;
   G4cout << "Degrader = " << TDGR1 / um << " um" << G4endl;

   G4cout << "___________________________" << G4endl;

   delete values;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
