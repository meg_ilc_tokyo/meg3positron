//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "BMUMessenger.hh"
#include "BMUModule.hh"
//#include "BMURunAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

BMUMessenger::BMUMessenger(BMUModule *mod)
   : module(mod)
{
   bmuDir = new G4UIdirectory("/bmu/");
   bmuDir->SetGuidance("Commands for US beam elements");

   degraderThicknessCmd = new G4UIcmdWithADoubleAndUnit("/bmu/degraderThickness", this);
   degraderThicknessCmd->SetGuidance("Set the thickness of the degrader");
   degraderThicknessCmd->SetParameterName("thickness", false);
   degraderThicknessCmd->SetUnitCategory("Length");
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

BMUMessenger::~BMUMessenger()
{
   delete degraderThicknessCmd;
   delete bmuDir;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void BMUMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
   if (command == degraderThicknessCmd) {
      module->SetDegraderThickness(degraderThicknessCmd->GetNewDoubleValue(newValue));

   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
