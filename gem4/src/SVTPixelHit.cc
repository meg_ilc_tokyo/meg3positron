//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "SVTPixelHit.hh"
#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4Allocator<SVTPixelHit> SVTPixelHitAllocator;
G4LogicalVolume* SVTPixelHit::fgLogVol = 0;
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTPixelHit::SVTPixelHit()
:G4VHit()
, fTrackID(-1)
, fLayerID(-1)
, fTime(0)
, fXYZMean(G4ThreeVector())
, fXYZMeanWeight(0)
, fEloss(0)
, fEdepTot(0)
, fIncAngle(0)
, fPathLength(0)
, fRot(G4RotationMatrix())
{
}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTPixelHit::~SVTPixelHit()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTPixelHit::SVTPixelHit(const SVTPixelHit &right)
:G4VHit()
, fTrackIDSet(right.fTrackIDSet)
, fTrackID(right.fTrackID)
, fLayerID(right.fLayerID)
, fTime(right.fTime)
, fXYZMean(right.fXYZMean)
, fXYZMeanWeight(right.fXYZMeanWeight)
, fEloss(right.fEloss)
, fEdepTot(right.fEdepTot)
, fIncAngle(right.fIncAngle)
, fPathLength(right.fPathLength)
, fChipIDVec(right.fChipIDVec)
, fPixelIndexVec(right.fPixelIndexVec)
, fXYZPixelVec(right.fXYZPixelVec)
, fEdepVec(right.fEdepVec)
, fRot(right.fRot)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

const SVTPixelHit& SVTPixelHit::operator=(const SVTPixelHit &right)
{
   fTrackIDSet    = right.fTrackIDSet;
   fTrackID       = right.fTrackID;
   fLayerID       = right.fLayerID;
   fTime          = right.fTime;
   fXYZMean       = right.fXYZMean;
   fXYZMeanWeight = right.fXYZMeanWeight;
   fEloss         = right.fEloss;
   fEdepTot       = right.fEdepTot;
   fIncAngle      = right.fIncAngle;
   fPathLength    = right.fPathLength;
   fChipIDVec     = right.fChipIDVec;
   fPixelIndexVec = right.fPixelIndexVec;
   fXYZPixelVec   = right.fXYZPixelVec;
   fEdepVec       = right.fEdepVec;
   fRot           = right.fRot;

   return *this;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4int SVTPixelHit::operator==(const SVTPixelHit&) const
{
   return false;
   //returns false because there currently isnt need to check for equality yet
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4bool SVTPixelHit::FindTrack(G4int trackID) const
{
   if (fTrackIDSet.find(trackID) == fTrackIDSet.end())
      return false;
   else
      return true;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTPixelHit::Draw()
{
   // Draw each hit as a circle
   if (!fgLogVol) return;

   G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
   if (pVVisManager) {
      const G4VisAttributes *pVA = fgLogVol->GetVisAttributes();
      G4VisAttributes va;
      if (pVA)
         va = *pVA;
      G4Colour colour(1.0, 0.0, 0.0); //red
      va.SetColour(colour);
      va.SetForceWireframe(false);
      va.SetForceSolid(true);
      for (size_t iPixel = 0; iPixel < fPixelIndexVec.size(); iPixel++) {
         G4Transform3D trans(fRot, fXYZPixelVec[iPixel]);
         pVVisManager->Draw(*fgLogVol, va, trans);
      }
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTPixelHit::Print()
{

//    G4cout<<"Pixel hit in SVT "<<fChipID<<"-"<<fPixelIndex<<" "<<fEloss/keV<<" keV, "<<fTime/ns<<" ns"
//          <<G4endl;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
