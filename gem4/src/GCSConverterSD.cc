#include "GCSConverterSD.hh"
#include "generated/MEGMCGCSConverterEvent.h"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4VProcess.hh"
#include "G4SDManager.hh"
#include "G4VTouchable.hh"
#include "G4Tubs.hh"
#include <TString.h>

#include "GEMUnitConversion.hh"
#include "GEMRootIO.hh"

using namespace std;
using namespace CLHEP;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GCSConverterSD::GCSConverterSD(G4String name)
   :G4VSensitiveDetector(name)
{
   pConverterEvent_arr = GEMRootIO::GetInstance()->GetMCGCSConverterEvents();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void GCSConverterSD::Initialize(G4HCofThisEvent*)
{
   // Initializations at beginning of each event
   hitID = 0;
   map_hitID.clear();
   pConverterEvent_arr->Clear();
}
   
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4bool GCSConverterSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{

   G4Track* theTrack = aStep->GetTrack();

   // Only for a charged particle
   if (theTrack->GetParticleDefinition()->GetPDGCharge() == 0.)
      return false;

   // Skip if Edep is 0
   G4double edep = aStep->GetTotalEnergyDeposit();
   if (edep <= 0.)
      return false;

   // Get step points
   auto preStepPoint  = aStep->GetPreStepPoint();
   auto postStepPoint = aStep->GetPostStepPoint();

   // Check volume name
   auto theTouchable = preStepPoint->GetTouchable();
   auto physVol = theTouchable->GetVolume();
   auto volname = physVol->GetName();
   if (!G4StrUtil::contains(volname, "ConverterSegment")) {
      G4cout << "Unexpected volume name: " << volname << G4endl;
      return false;
   }
   volname = Form("%s_%d", volname.data(), theTouchable->GetReplicaNumber(0));

   // Get parameters of this step
   G4double timePre = preStepPoint->GetGlobalTime();
   auto vPre  = preStepPoint->GetPosition();
   auto vPost = postStepPoint->GetPosition();

   // Get hit event of this wire
   MEGMCGCSConverterEvent *pConverterEvent(0);
   if (map_hitID.count(volname) == 0) {

      // Make a new hit
      TClonesArray &arr = *pConverterEvent_arr;
      pConverterEvent = new(arr[hitID]) MEGMCGCSConverterEvent();

      // Add hit to the map of hits
      map_hitID[volname] = hitID;
      hitID++;

      // Initialize hit
      pConverterEvent->Setedeptot(0);
      //pConverterEvent->SetIP(1e5);

      // Calculate wire position
      auto tubs = (G4Tubs*)physVol->GetLogicalVolume()->GetSolid();
      G4double rConverter = (tubs->GetInnerRadius() + tubs->GetOuterRadius()) / 2.;
   } else {
      // Hit already exists for this wire
      G4int index = map_hitID[volname];
      pConverterEvent = static_cast<MEGMCGCSConverterEvent*>(pConverterEvent_arr->At(index));
   }

   // Add Edep
   pConverterEvent->Addedeptot(edep);

   return true;

}


