//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "TARSteppingAction.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMUserEventInformation.hh"
#include "TARUserEventInformation.hh"

#include "G4SteppingManager.hh"
#include "G4SDManager.hh"
#include "G4EventManager.hh"
#include "G4ProcessManager.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TARSteppingAction::TARSteppingAction()
   : idx(-1)
   , targetVol(0)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TARSteppingAction::~TARSteppingAction()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TARSteppingAction::UserSteppingAction(const G4Step *theStep)
{
   TARUserEventInformation* eveInfo =
      dynamic_cast<TARUserEventInformation*>(
         dynamic_cast<GEMUserEventInformation*>(
            G4EventManager::GetEventManager()->GetConstCurrentEvent()->
            GetUserInformation())->GetModuleAt(idx));

   G4Track     *theTrack    = theStep->GetTrack();
   G4StepPoint *thePrePoint = theStep->GetPreStepPoint();

   if (targetVol && theTrack->GetParentID() == 0 &&
       theTrack->GetDefinition() == G4Positron::PositronDefinition()) {
      if (!eveInfo->vecIn.mag2()) {
         if (thePrePoint->GetPhysicalVolume() == targetVol) {
            eveInfo->vecIn = thePrePoint->GetMomentumDirection();
         }
      } else if (!eveInfo->vecOut.mag2()) {
         if (thePrePoint->GetPhysicalVolume() != targetVol) {
            eveInfo->vecOut = thePrePoint->GetMomentumDirection();
         }
      }
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
