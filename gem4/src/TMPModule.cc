//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_


#include "TMPModule.hh"

#include "TMPEventAction.hh"
#include "TMPStackingAction.hh"
#include "TMPSteppingAction.hh"
#include "TMPTrackingAction.hh"

#include "TMPUserEventInformation.hh"
#include "TMPUserTrackInformation.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

namespace
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TMPModule::ConstructMaterials()
{
   G4Element *Si = G4NistManager::Instance()->FindOrBuildElement("Si");

   Silicon = new G4Material("TMPSilicon", 2.329 * g / cm3, 1);
   Silicon->AddElement(Si, 1.);

   if (fSteppingAction) {
      static_cast<TMPSteppingAction*>(fSteppingAction)->SetSilicon(Silicon);
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TMPModule::Construct(G4LogicalVolume *parent)
{
   if (!materialConstructed) {
      ConstructMaterials();
      materialConstructed = true;
   }

   G4Box *tmp_box = new G4Box("TemplateBox", 10 * cm, 5 * cm, 1 * mm);
   G4LogicalVolume *tmpbox_log = new G4LogicalVolume(tmp_box, Silicon, "TemplateBox", 0, 0, 0);
   new G4PVPlacement(0, G4ThreeVector(), tmpbox_log, "TemplateBox", parent, false, 0);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TMPModule::TMPModule(const char* n, G4int i)
   : GEMAbsModule(n)
   , idx(i)
   , materialConstructed(false)
   , Silicon(0)
{
   fRunAction      = 0;
   fEventAction    = new TMPEventAction();
   fStackingAction = new TMPStackingAction();
   fSteppingAction = new TMPSteppingAction();
   fTrackingAction = new TMPTrackingAction();

   //static_cast<TMPRunAction*>     (fRunAction     )->SetIndex(idx);
   static_cast<TMPEventAction*>(fEventAction)->SetIndex(idx);
   static_cast<TMPStackingAction*>(fStackingAction)->SetIndex(idx);
   static_cast<TMPSteppingAction*>(fSteppingAction)->SetIndex(idx);
   static_cast<TMPTrackingAction*>(fTrackingAction)->SetIndex(idx);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TMPModule::~TMPModule()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserEventInformation* TMPModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return new TMPUserEventInformation();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserTrackInformation* TMPModule::MakeTrackInformation(const G4Track* /*aTrack*/)
{
   return new TMPUserTrackInformation();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
