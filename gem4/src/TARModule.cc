#include <map>
#include <iostream>
#include "TARModule.hh"
#include "ROMESQLDataBase.h"
#include "ROMEString.h"
#include "ROMEStr2DArray.h"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4MaterialTable.hh"
#include "G4NistManager.hh"
#include "G4VSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4Trap.hh"
#include "G4EllipticalTube.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4UserLimits.hh"
#include "G4AssemblyVolume.hh"
#include "G4Transform3D.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4VSensitiveDetector.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4GDMLParser.hh"
#include "G4ios.hh"
#include "G4PVReplica.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "GEMConstants.hh"
#include "TAREventAction.hh"
#include "TARUserEventInformation.hh"
#include "TARSteppingAction.hh"
#include "TARMessenger.hh"
#include "ROMEPrint.h"

using namespace::std;
namespace
{
map<G4String, G4int> gTARMatMap
= { {"polyethylene-polyester", 0},
   {"scintillator", 1},
   {"polyethylene", 2},
   {"silicon", 3},
};
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TARModule::TARModule(const char* n, GEMPhysicsList *p, G4int i)
   : GEMAbsTargetModule(n)
   , fIdx(i)
   , fVerboseLevel(ROMEPrint::kNormal)
   , fPhysics(p)
   , fMessenger(new TARMessenger(this))
   , fMaterialConstructed(false)
   , fITAR(kDefaultTargetType)
   , fTargetMaterialId(2)
   , fRohacell51(nullptr)
   , fRohacell(nullptr)
   , fCarbonFiberFrame(nullptr)
   , fPolyethylene(nullptr)
   , fPolyethylenePolyester(nullptr)
   , fMylar(nullptr)
   , fSUS(nullptr)
   , fScintillator(nullptr)
   , fSilicon(nullptr)
   , fTargetRegion(nullptr)
{
   fTargetSize.set(kDefaultTargetSize[0], kDefaultTargetSize[1], kDefaultTargetSize[2]);
   fTargetPosition.set(kDefaultTargetPosition[0], kDefaultTargetPosition[1], kDefaultTargetPosition[2]);
   fEventAction    = new TAREventAction();
   fSteppingAction = new TARSteppingAction();
   static_cast<TAREventAction*>(fEventAction)->SetIndex(fIdx);
   static_cast<TARSteppingAction*>(fSteppingAction)->SetIndex(fIdx);
   fNumberOfHoles = 0;
   fHoleRadii.assign(fNumberOfHoles, 5 * mm);
   std::vector<std::vector<G4double> > defaultHolePosition = { // target position of x,y in local coordinate
      { 6.65 * cm,  0.000 * cm},
      { 3.35 * cm,  0.000 * cm},
      { 0.00 * cm,  1.475 * cm},
      { 0.00 * cm, -1.475 * cm},
      {-3.35 * cm,  0.000 * cm},
      {-6.65 * cm,  0.000 * cm}
   } ;
   fHolePositions = defaultHolePosition;

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TARModule::~TARModule()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TARModule::Construct(G4LogicalVolume *parent)
{
   // All managed (deleted) by SDManager
   if (!fMaterialConstructed) {
      ConstructMaterials();
      fMaterialConstructed = true;
   }
   G4String name;
   G4ThreeVector trans;
   fTargetVolume = nullptr; // Reset
   G4RotationMatrix *norot = new G4RotationMatrix();    // rotation matrix for no rotation
   G4double targetHalfSize[3];
   for (G4int i = 0; i < 3; i++) {
      targetHalfSize[i] = fTargetSize[i] / 2.;
   }
   G4VSolid *targetBaseSolid = new G4EllipticalTube("TARBaseSolid", targetHalfSize[0], targetHalfSize[1],
                                                    targetHalfSize[2]);//original- following lines added for frame
   G4LogicalVolume *targetLog = nullptr;
   vector<G4LogicalVolume*> frameLogs;
   vector<G4LogicalVolume*> supportLogs;

   if (fITAR < 5) {
      // assembly volume
      G4AssemblyVolume *targetAssembly = new G4AssemblyVolume();
      G4VSolid *targetSolid = targetBaseSolid;

      //targetSolid
      // target itself fTargetMaterialId determines material
      if (fTargetMaterialId == 0) {
         targetLog = new G4LogicalVolume(targetSolid, fPolyethylenePolyester, "TRGTLogical", 0, 0, 0);
      } else if (fTargetMaterialId == 1) {
         targetLog = new G4LogicalVolume(targetSolid, fScintillator, "TRGTLogical", 0, 0, 0);
      } else if (fTargetMaterialId == 2) {
         targetLog = new G4LogicalVolume(targetSolid, fPolyethylene, "TRGTLogical", 0, 0, 0);
      } else if (fTargetMaterialId == 3) {
         targetLog = new G4LogicalVolume(targetSolid, fSilicon, "TRGTLogical", 0, 0, 0);
      }
      trans.setX(0);
      trans.setY(0);
      trans.setZ(0);
      targetAssembly->AddPlacedVolume(targetLog, trans, norot);

      if (fITAR > 3) {
         kSupport1[0] = 0.20 * cm; // rohacell frame thickness for {frame support, frame}
         kSupport1[1] = 0.2 * cm; // rohacell frame thickness for {frame support, frame}      

         //targetAssembly->AddPlacedVolume(FrontFrame, kDefaultFrontOuterFramePosition, norot);
         G4double solidPar[6];
         
         // target elliptical rohacell frame
         solidPar[0] = targetHalfSize[0];
         solidPar[1] = targetHalfSize[1];
         solidPar[2] = (kSupport1[1] - fTargetSize[2] / 2.) / 2.;
         G4VSolid *frame1Solid = new G4EllipticalTube("TARFrame1Solid", solidPar[0], solidPar[1], solidPar[2]);
         solidPar[0] = targetHalfSize[0] - kSupport1[0];
         solidPar[1] = targetHalfSize[1] - kSupport1[0];
         solidPar[2] = (kSupport1[1] - fTargetSize[2] / 2.) / 2. + 0.001 * cm;
         G4VSolid *frame2Solid = new G4EllipticalTube("TARFrame2Solid", solidPar[0], solidPar[1], solidPar[2]);
         
         // frame1 & frame2 boolean Solid
         G4VSolid *frameSolid = new G4SubtractionSolid("TARFrameSolid", frame1Solid, frame2Solid);
         G4LogicalVolume *frameLog = new G4LogicalVolume(frameSolid, fRohacell, "TARFrameLog", 0, 0, 0);
         trans.setX(0);
         trans.setY(0);
         trans.setZ(+(solidPar[2] + targetHalfSize[2]));
         targetAssembly->AddPlacedVolume(frameLog, trans, norot);
         trans.setX(0);
         trans.setY(0);
         trans.setZ(-(solidPar[2] + targetHalfSize[2]));
         targetAssembly->AddPlacedVolume(frameLog, trans, norot);
         frameLogs.push_back(frameLog);
      }
      
      //////////////////////////
      // target mother volume //
      //////////////////////////
      trans = fTargetPosition;
      targetAssembly->MakeImprint(parent, trans, &fTargetRotation, 0);

      std::vector<G4VPhysicalVolume*>::iterator physVol = targetAssembly->GetVolumesIterator();
      unsigned int nPhysVol = targetAssembly->TotalImprintedVolumes();
      unsigned int iPhysVol;
      for (iPhysVol = 0; iPhysVol < nPhysVol; iPhysVol++) {
         if ((*physVol) && G4StrUtil::contains((*physVol)->GetName(), "TRGTLogical")) {
            fTargetVolume = (*physVol);
            break;
         }
         ++physVol;
      }
      if (!fTargetVolume) {
         std::ostringstream o;
         o << "Target volume not found";
         G4Exception(__func__, "", FatalException, o.str().c_str());
      }
   }
   static_cast<TARSteppingAction*>(fSteppingAction)->SetTargetVolume(fTargetVolume);

   CreateRegion();
   targetLog->SetRegion(fTargetRegion);
   fTargetRegion->AddRootLogicalVolume(targetLog);

   // visualization attributes ------------------------------------------------
   G4VisAttributes *targetVisAtt = new G4VisAttributes(G4Colour(1., 0., 0.));  // Red
   targetVisAtt->SetForceSolid(true);
   targetLog->SetVisAttributes(targetVisAtt);

   G4VisAttributes *supportVisAtt = new G4VisAttributes(G4Colour(1., 1., 1.));
   if (supportVisAtt) {
      supportVisAtt->SetForceSolid(true);
   }
   for (auto&& log : frameLogs) {
      if (log) {
         log->SetVisAttributes(supportVisAtt);
      }
   }
   for (auto&& log : supportLogs) {
      if (log) {
         log->SetVisAttributes(supportVisAtt);
      }
   }

   SetRegionCuts();

   // return the world physical volume ----------------------------------------

   //  G4cout << G4endl << "The geometrical tree defined are : " << G4endl << G4endl;
   //  DumpGeometricalTree(worldPhysical);
}

void TARModule::ConstructMaterials()
{
   G4double density;
   G4String name;
   G4String symbol;
   G4int nElem;

   // elements for mixtures and compounds
   G4Element *elH  = G4NistManager::Instance()->FindOrBuildElement("H");
   G4Element *elC  = G4NistManager::Instance()->FindOrBuildElement("C");
   G4Element *elN  = G4NistManager::Instance()->FindOrBuildElement("N");
   G4Element *elO  = G4NistManager::Instance()->FindOrBuildElement("O");
   G4Element *elCr = G4NistManager::Instance()->FindOrBuildElement("Cr");
   G4Element *elFe = G4NistManager::Instance()->FindOrBuildElement("Fe");
   G4Element *elNi = G4NistManager::Instance()->FindOrBuildElement("Ni");

   // Rohacell51 (polymethacrylimide hard foam, H13-C9-N1-O2)
   density = 0.05 * g / cm3;
   fRohacell51 = new G4Material(name = "Rohacell51", density, nElem = 4);
   fRohacell51->AddElement(elH, 13);
   fRohacell51->AddElement(elC, 9);
   fRohacell51->AddElement(elN, 1);
   fRohacell51->AddElement(elO, 2);

   /////////////////////////////
   // Target Materials        //
   /////////////////////////////

   // Polyethylene n-(CH2=CH2)
#if 1
   density = 0.922 * g / cm3;
   fPolyethylene = new G4Material("PolyethyleneTAR", density, nElem = 2);
   fPolyethylene->AddElement(elH, 2);
   fPolyethylene->AddElement(elC, 1);
#else
   fPolyethylene = G4NistManager::Instance()->FindOrBuildMaterial("G4_POLYETHYLENE");
#endif


   // carbon fiber we say is half H and half C, the density is calculated using the known weight and the known volume
   density = 1.1418 * g / cm3; //mixture of C and CH2
   fCarbonFiberFrame = new G4Material(name = "CarbonFiberFrame", density, nElem = 2);
   fCarbonFiberFrame->AddElement(elH, 1);
   fCarbonFiberFrame->AddElement(elC, 1);




   // Rohacell (polymethacrylimide- (PMI-) hard foam, H11-C8-N1-O2)
   density = 0.052 * g / cm3;
   fRohacell = new G4Material(name = "Rohacell", density, nElem = 4);
   fRohacell->AddElement(elH, 11);
   fRohacell->AddElement(elC, 8);
   fRohacell->AddElement(elN, 1);
   fRohacell->AddElement(elO, 2);

   // Polyethylene/polyester sandwitch n-(CH2=CH2)
   density = 0.895 * g / cm3;
   fPolyethylenePolyester = new G4Material("PolySandwitch", density, nElem = 2);
   fPolyethylenePolyester->AddElement(elH, 2);
   fPolyethylenePolyester->AddElement(elC, 1);

   // Scintillator PVT(POLYVINYLTOLUENE) ( C27H30 )
   density = 1.032 * g / cm3;

   fScintillator = new G4Material("Scintillator", density, nElem = 2);
   fScintillator->AddElement(elC, 9);
   fScintillator->AddElement(elH, 10);

   // Mylar (polyethylene terephthlate, C5H4O2)
   density = 1.395 * g / cm3;
   fMylar = new G4Material("Maylar", density, nElem = 3);
   fMylar->AddElement(elH, 4);
   fMylar->AddElement(elC, 5);
   fMylar->AddElement(elO, 2);

   // Silicon
   fSilicon = G4NistManager::Instance()->FindOrBuildMaterial("G4_Si");

   // Composition of SUS-310 (Fe and Cr and Ni)
#if 1
   density = 7.98 * g / cm3;
   fSUS = new G4Material("SUS-301", density, nElem = 3);
   fSUS->AddElement(elCr, 0.25);
   fSUS->AddElement(elFe, 0.55);
   fSUS->AddElement(elNi, 0.20);
#else
   fSUS = G4NistManager::Instance()->FindOrBuildMaterial("G4_STAINLESS-STEEL");
#endif
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserEventInformation* TARModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return new TARUserEventInformation();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserTrackInformation* TARModule::MakeTrackInformation(const G4Track* /*aTrack*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
void TARModule::SetHoleRadius(G4double r)
{
   // Set size of all the holes
   for (auto&& radius : fHoleRadii) {
      radius = r;
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
void TARModule::ReadDatabase([[maybe_unused]] ROMESQLDataBase *db, [[maybe_unused]] G4int run)
{
   // Read database for target parameters
   // Target geometrical parameters are read in GEMDetectorConstruction
   // because it is necessary for other type of target modules and event generators
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TARModule::CreateRegion()
{
   if (fTargetRegion) {
      delete fTargetRegion;
   }
   fTargetRegion = new G4Region("TARRegion");
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TARModule::SetRegionCuts()
{
   G4ProductionCuts *cuts = new G4ProductionCuts;
   cuts->SetProductionCut(fPhysics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("gamma"));
   cuts->SetProductionCut(fPhysics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("e-"));
#if 1
   cuts->SetProductionCut(fPhysics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("e+"));
#else
   cuts->SetProductionCut(1 * um, G4ProductionCuts::GetIndex("e+"));
#endif
   cuts->SetProductionCut(fPhysics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("proton"));
   fTargetRegion->SetProductionCuts(cuts);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
