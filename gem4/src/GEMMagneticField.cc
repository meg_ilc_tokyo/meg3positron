// $

#include "GEMMagneticField.hh"
#include "GEMMagneticFieldMessenger.hh"
#include "globals.hh"
//extern "C" {
#include "bfield/megbfield_c.h"
//}
#include "G4SystemOfUnits.hh"


GEMMagneticField::GEMMagneticField()
   : magnetType(kAll)
   , fieldType(kReconstructed1)
   , cobraScale(1.)
   , btsScale(1.)
   , bypassScale(0.)
{
   messenger = new GEMMagneticFieldMessenger(this);
}

GEMMagneticField::~GEMMagneticField()
{
   freebfield_();
   delete messenger;
}

void GEMMagneticField::InitMEGBField()
{
   G4cout << G4endl << "___ B field setting ___" << G4endl;
   G4cout << "BField Type = " << fieldType << " cobraScale = " << cobraScale << " btsScale = " << btsScale <<
          " bypassScale = " << bypassScale << G4endl;
   initbfield_(&magnetType, &fieldType, &cobraScale, &btsScale, &bypassScale);
   G4cout << "___________________________" << G4endl;
}

void GEMMagneticField::GetFieldValue(const G4double Point[3], double *Bfield) const
{
   //
   G4double phi = atan2(Point[1], Point[0]);
   G4double zz  = Point[2] / cm;
   G4double rr  = sqrt(Point[0] * Point[0] + Point[1] * Point[1]) / cm;
   G4double pp  = phi / degree;

   G4double Bz   = getbz_(&zz, &rr, &pp) * gauss;
   G4double Br   = getbr_(&zz, &rr, &pp) * gauss;
   G4double Bphi = getbphi_(&zz, &rr, &pp) * gauss;

   Bfield[0] = Br * cos(phi) - Bphi * sin(phi);
   Bfield[1] = Br * sin(phi) + Bphi * cos(phi);
   Bfield[2] = Bz;
}

G4double GEMMagneticField::GetBz(G4double Point[3])
{
   G4double phi = atan2(Point[1], Point[0]);
   G4double zz  = Point[2] / cm;
   G4double rr  = sqrt(Point[0] * Point[0] + Point[1] * Point[1]) / cm;
   G4double pp  = phi / degree;

   G4double Bz  = getbz_(&zz, &rr, &pp);
   return Bz;
}

