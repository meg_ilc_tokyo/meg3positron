//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "GEMTrajectory.hh"
#include "G4TrajectoryPoint.hh"
#include "G4Trajectory.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "G4ThreeVector.hh"
#include "G4Polyline.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"
#include "G4Polymarker.hh"

G4Allocator<GEMTrajectory> GEMTrajectoryAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrajectory::GEMTrajectory()
   : G4Trajectory()
   , drawit(false)
   , particleDefinition(0)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrajectory::GEMTrajectory(const G4Track* aTrack)
   : G4Trajectory(aTrack)
   , drawit(false)
   , particleDefinition(aTrack->GetDefinition())
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrajectory::GEMTrajectory(GEMTrajectory &right)
   : G4Trajectory(right)
   , drawit(right.drawit)
   , particleDefinition(right.particleDefinition)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrajectory::~GEMTrajectory()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMTrajectory::DrawTrajectory() const
{
   G4VTrajectory::DrawTrajectory();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMTrajectory::DrawTrajectory(G4int i_mode) const
{
   if (!drawit) {
      return;
   }

   // If i_mode>=0, draws a trajectory as a polyline and, if i_mode!=0,
   // adds markers - yellow circles for step points and magenta squares
   // for auxiliary points, if any - whose screen size in pixels is
   // given by std::abs(i_mode)/1000.  E.g: i_mode = 5000 gives easily
   // visible markers.
   G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
   if (!pVVisManager) {
      return;
   }

   const G4double markerSize = std::abs(i_mode) / 1000;
   G4bool lineRequired(i_mode >= 0);
   G4bool markersRequired(markerSize > 0.);

   G4Polyline trajectoryLine;
   G4Polymarker stepPoints;
   G4Polymarker auxiliaryPoints;

   for (G4int i = 0; i < GetPointEntries() ; i++) {
      G4VTrajectoryPoint* aTrajectoryPoint = GetPoint(i);
      const std::vector<G4ThreeVector>* auxiliaries =
         aTrajectoryPoint->GetAuxiliaryPoints();
      if (auxiliaries) {
         for (size_t iAux = 0; iAux < auxiliaries->size(); ++iAux) {
            const G4ThreeVector pos((*auxiliaries)[iAux]);
            if (lineRequired) {
               trajectoryLine.push_back(pos);
            }
            if (markersRequired) {
               auxiliaryPoints.push_back(pos);
            }
         }
      }
      const G4ThreeVector pos(aTrajectoryPoint->GetPosition());
      if (lineRequired) {
         trajectoryLine.push_back(pos);
      }
      if (markersRequired) {
         stepPoints.push_back(pos);
      }
   }

   if (lineRequired) {
      G4Colour colour;

      if (particleDefinition == G4Gamma::GammaDefinition()) {
         colour = G4Colour(1, 1, 0);
      } else if (particleDefinition == G4Electron::ElectronDefinition()) {
         colour = G4Colour(1, 0, 1);
      } else if (particleDefinition == G4Positron::PositronDefinition()) {
         colour = G4Colour(0, 1, 1);
      } else if (particleDefinition == G4OpticalPhoton::OpticalPhotonDefinition()) {
         colour = G4Colour(0, 1, 0);
      } else {
         colour = G4Colour(0.5, 0.5, 0.5);
      }

      G4VisAttributes trajectoryLineAttribs(colour);
      trajectoryLine.SetVisAttributes(&trajectoryLineAttribs);
      if (particleDefinition != G4NeutrinoE::NeutrinoEDefinition() &&
          particleDefinition != G4AntiNeutrinoE::AntiNeutrinoEDefinition() &&
          particleDefinition != G4NeutrinoMu::NeutrinoMuDefinition() &&
          particleDefinition != G4AntiNeutrinoMu::AntiNeutrinoMuDefinition()) {
         pVVisManager->Draw(trajectoryLine);
      }
   }
   if (markersRequired) {
      auxiliaryPoints.SetMarkerType(G4Polymarker::squares);
      auxiliaryPoints.SetScreenSize(markerSize);
      auxiliaryPoints.SetFillStyle(G4VMarker::filled);
      G4VisAttributes auxiliaryPointsAttribs(G4Colour(0., 1., 1.)); // Magenta
      auxiliaryPoints.SetVisAttributes(&auxiliaryPointsAttribs);
      pVVisManager->Draw(auxiliaryPoints);

      stepPoints.SetMarkerType(G4Polymarker::circles);
      stepPoints.SetScreenSize(markerSize);
      stepPoints.SetFillStyle(G4VMarker::filled);
      G4VisAttributes stepPointsAttribs(G4Colour(1., 1., 0.)); // Yellow.
      stepPoints.SetVisAttributes(&stepPointsAttribs);
      pVVisManager->Draw(stepPoints);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
