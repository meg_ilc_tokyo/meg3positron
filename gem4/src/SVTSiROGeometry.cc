//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "SVTSiROGeometry.hh"
#include "SVTModule.hh"
#include "SVTDummySD.hh"
#include "SVTSiSD.hh"

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4SDManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"
#include "G4SystemOfUnits.hh"
#include <vector>
using namespace std;


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiROGeometry::SVTSiROGeometry()
:G4VReadOutGeometry()
{
}



//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiROGeometry::SVTSiROGeometry(G4String string, SVTModule *module)
:G4VReadOutGeometry(string)
, fModule(module)
, fDummyMat(0)
{
}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiROGeometry::~SVTSiROGeometry()
{
   delete fDummyMat;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4VPhysicalVolume* SVTSiROGeometry::Build()
{

   // A dummy material is used to fill the volumes of the readout geometry.
   // (It will be allowed to set a NULL pointer in volumes of such virtual
   // division in future, since this material is irrelevant for tracking.)
   fDummyMat  = new G4Material(name="dummyMat", 1., 1.*g/mole, 1.*g/cm3);

   // Build the ReadOut world
   const G4double expHallLen = 10 * meter;
   G4Box *roWorldSol = new G4Box("SVTWorldSolRO", expHallLen, expHallLen, expHallLen);
   G4LogicalVolume *roWorldLog = new G4LogicalVolume(roWorldSol, fDummyMat,
                                                     "SVTWorldLogRO", 0, 0, 0);
   roWorldLog->SetVisAttributes(G4VisAttributes::GetInvisible());
   G4PVPlacement *roWorldPhys = new G4PVPlacement(0,                   // no rotation
                                                  G4ThreeVector(),     // its position
                                                  roWorldLog,          // its logical volume
                                                  "SVTWorldRO",        // its name
                                                  0,                   // its mother
                                                  false,               // no boolean operation
                                                  0);                  // copy number
  
   // SVT volume 
   vector<G4LogicalVolume*> logVec;
   fModule->BuildAndAssemble(roWorldLog, logVec, true);
  
   //Flags the cells as sensitive .The pointer here serves
   // as a flag only to check for sensitivity.
   // (Could we make it by a simple cast of a non-NULL value ?)
   static SVTDummySD * dummySD = new SVTDummySD;
   for (G4int iVol = 0; iVol < (G4int)logVec.size(); iVol++) {
      logVec[iVol]->SetSensitiveDetector(dummySD);
   }

   return roWorldPhys;
}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
