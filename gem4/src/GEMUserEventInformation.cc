//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "GEMUserEventInformation.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserEventInformation::GEMUserEventInformation()
   : modules(0)
   , timeOfInterest(0)
   , primaryInfos(0)
   , primaryLookUp()
   , secondaryInfos(0)
   , secondaryLookUp()
   , trackInfos(0)
   , trackLookUp()
   , radiationInfos()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserEventInformation::~GEMUserEventInformation()
{
   std::vector<GEMAbsUserEventInformation*>::iterator it1;
   for (it1 = modules.begin(); it1 != modules.end(); ++it1) {
      delete *it1;
   }

   std::vector<GEMUserPrimaryVertexInformation*>::iterator it2;
   for (it2 = primaryInfos.begin(); it2 != primaryInfos.end(); ++it2) {
      delete *it2;
   }

   std::vector<GEMUserTrackInformation*>::iterator it3;
   for (it3 = trackInfos.begin(); it3 != trackInfos.end(); ++it3) {
      delete *it3;
   }

   for (it2 = radiationInfos.begin(); it2 != radiationInfos.end(); ++it2) {
      delete *it2;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMAbsUserEventInformation *GEMUserEventInformation::GetModule(const char* name)
{
   std::vector<GEMAbsUserEventInformation*>::iterator iter;
   G4String str = name;
   for (iter = modules.begin(); iter != modules.end(); ++iter) {
      if ((*iter) && name == (*iter)->GetName()) {
         return (*iter);
      }
   }
   return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMUserEventInformation::AddPrimaryInfo(const G4Track* aTrack)
{
   primaryInfos.push_back(new GEMUserPrimaryVertexInformation(aTrack));
   primaryLookUp.insert(std::pair<G4int, G4int>(aTrack->GetTrackID(), primaryInfos.size() - 1));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMUserEventInformation::AddSecondaryInfo(const G4Track* aTrack)
{
   secondaryInfos.push_back(new GEMUserPrimaryVertexInformation(aTrack));
   secondaryLookUp.insert(std::pair<G4int, G4int>(aTrack->GetTrackID(), secondaryInfos.size() - 1));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#if 0
void GEMUserEventInformation::AddTrackInfo(const G4Track* aTrack)
{
   trackInfos.insert(new GEMUserTrackInformation(aTrack));
   trackLookUp.insert(std::pair<G4int, G4int>(aTrack->GetTrackID(), trackInfos.size() - 1));
}
#endif
void GEMUserEventInformation::AddTrackInfo(const GEMUserTrackInformation &info)
{
   if (!info.trackInfo.particledef) {
      return;
   }
   if (trackLookUp.find(info.trackInfo.trackid) == trackLookUp.end()) {
      // add
      trackInfos.push_back(new GEMUserTrackInformation(info));
      trackLookUp.insert(std::pair<G4int, G4int>(info.trackInfo.trackid, trackInfos.size() - 1));
   } else {
      // replace
      *trackInfos[trackLookUp[info.trackInfo.trackid]] = info;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int GEMUserEventInformation::GetTrackInfoIndexFromID(G4int id)
{
   std::map<G4int, G4int>::iterator it;
   if ((it = trackLookUp.find(id)) != trackLookUp.end()) {
      return it->second;
   }
   return -1; // primary
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int GEMUserEventInformation::GetPrimaryInfoIndexFromID(G4int id)
{
   G4int currIdx = GetTrackInfoIndexFromID(id);
   if (currIdx < 0) {
      return -1;
   }
   G4int nextIdx = trackInfos[currIdx]->trackInfo.ParentIndex;
   while (nextIdx != -1) {
      currIdx = nextIdx;
      nextIdx = trackInfos[nextIdx]->trackInfo.ParentIndex;
   }
   G4int primaryId = trackInfos[currIdx]->trackInfo.trackid;
   std::map<G4int, G4int>::iterator it;
   if ((it = primaryLookUp.find(primaryId)) != primaryLookUp.end()) {
      return it->second;
   }
   return -1; // not found
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
