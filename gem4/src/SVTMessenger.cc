//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "SVTMessenger.hh"
#include "SVTModule.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"


SVTModule* SVTMessenger::fgModule = 0;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTMessenger::SVTMessenger(SVTModule *mod)
{
   fgModule = mod;

   // Make directories
   fGeomDir = new G4UIdirectory("/svt/geom/");
   fGeomDir->SetGuidance("Commands for geometry");

   fTrackingDir = new G4UIdirectory("/svt/tracking/");
   fTrackingDir->SetGuidance("Commands for tracking");

   fMaterialDir = new G4UIdirectory("/svt/material/");
   fMaterialDir->SetGuidance("Commands for materials");

   fSensorDir = new G4UIdirectory("/svt/sensor/");
   fSensorDir->SetGuidance("Commands for sensor");

   fSupportDir = new G4UIdirectory("/svt/support/");
   fSupportDir->SetGuidance("Commands for support");

   fDigiDir = new G4UIdirectory("/svt/digi/");
   fDigiDir->SetGuidance("Commands for digitization");

   fVisDir = new G4UIdirectory("/svt/vis/");
   fVisDir->SetGuidance("Commands for visualization");

   
   // Define commands
   fVerboseCmd = new G4UIcmdWithAnInteger("/svt/verbose", this);
   fVerboseCmd->SetGuidance("Set the verbosity");
   fVerboseCmd->SetParameterName("verbose", true);
   fVerboseCmd->SetDefaultValue(1);

   fCheckOverlapCmd = new G4UIcmdWithABool("/svt/geom/checkOverlap", this);
   fCheckOverlapCmd->SetGuidance("Check overlap of volumes");
   fCheckOverlapCmd->SetParameterName("checkOverlap", true);
   fCheckOverlapCmd->SetDefaultValue(false);

   fLayerRadius0Cmd = new G4UIcmdWithADoubleAndUnit("/svt/geom/layerRadius0", this);
   fLayerRadius0Cmd->SetGuidance("Set radius of the 1st layer");
   fLayerRadius0Cmd->SetParameterName("r0", false);
   fLayerRadius0Cmd->SetUnitCategory("Length");

   fLayerRadius1Cmd = new G4UIcmdWithADoubleAndUnit("/svt/geom/layerRadius1", this);
   fLayerRadius1Cmd->SetGuidance("Set radius of the 2nd layer");
   fLayerRadius1Cmd->SetParameterName("r1", false);
   fLayerRadius1Cmd->SetUnitCategory("Length");

   fNSensorInZ0Cmd = new G4UIcmdWithAnInteger("/svt/geom/nSensorInZ0", this);
   fNSensorInZ0Cmd->SetGuidance("Set number of sensor chips in Z dir in 1st layer");
   fNSensorInZ0Cmd->SetParameterName("nz0", false);

   fNSensorInZ1Cmd = new G4UIcmdWithAnInteger("/svt/geom/nSensorInZ1", this);
   fNSensorInZ1Cmd->SetGuidance("Set number of sensor chips in Z dir in 2nd layer");
   fNSensorInZ1Cmd->SetParameterName("nz1", false);

   fSensorSlantAngleCmd = new G4UIcmdWithADoubleAndUnit("/svt/geom/slant", this);
   fSensorSlantAngleCmd->SetGuidance("Set sensor slant angle in phi");
   fSensorSlantAngleCmd->SetParameterName("slant", false);
   fSensorSlantAngleCmd->SetUnitCategory("Angle");

   fSensorOverlapCmd = new G4UIcmdWithADoubleAndUnit("/svt/geom/overlap", this);
   fSensorOverlapCmd->SetGuidance("Set sensor overlap width");
   fSensorOverlapCmd->SetParameterName("overlap", false);
   fSensorOverlapCmd->SetUnitCategory("Length");
   
   fSensorThicknessCmd = new G4UIcmdWithADoubleAndUnit("/svt/sensor/thickness", this);
   fSensorThicknessCmd->SetGuidance("Set silicon wafer thickness");
   fSensorThicknessCmd->SetParameterName("thickness", false);
   fSensorThicknessCmd->SetUnitCategory("Length");

   fDepThicknessCmd = new G4UIcmdWithADoubleAndUnit("/svt/sensor/depThickness", this);
   fDepThicknessCmd->SetGuidance("Set thickness of sensitive (depleted) region");
   fDepThicknessCmd->SetParameterName("depThickness", false);
   fDepThicknessCmd->SetUnitCategory("Length");
   
   fPixelSizeZCmd = new G4UIcmdWithADoubleAndUnit("/svt/sensor/pixelSizeZ", this);
   fPixelSizeZCmd->SetGuidance("Set pixel size in Z");
   fPixelSizeZCmd->SetParameterName("pixelsizeZ", false);
   fPixelSizeZCmd->SetUnitCategory("Length");
   
   fPixelSizeYCmd = new G4UIcmdWithADoubleAndUnit("/svt/sensor/pixelSizeY", this);
   fPixelSizeYCmd->SetGuidance("Set pixel size in Y");
   fPixelSizeYCmd->SetParameterName("pixelsizeY", false);
   fPixelSizeYCmd->SetUnitCategory("Length");
   

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTMessenger::~SVTMessenger()
{
   delete fVerboseCmd;
   delete fCheckOverlapCmd;
   delete fLayerRadius0Cmd;
   delete fLayerRadius1Cmd;
   delete fNSensorInZ0Cmd;
   delete fNSensorInZ1Cmd;
   delete fSensorSlantAngleCmd;
   delete fSensorOverlapCmd;
   delete fSensorThicknessCmd;
   delete fDepThicknessCmd;
   delete fPixelSizeZCmd;
   delete fPixelSizeYCmd;

   delete fGeomDir;
   delete fTrackingDir;
   delete fMaterialDir;
   delete fSensorDir;
   delete fSupportDir;
   delete fDigiDir;
   delete fVisDir;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
   if (command == fVerboseCmd) {
      fgModule->SetVerboseLevel(fVerboseCmd->GetNewIntValue(newValue));

   } else if (command == fCheckOverlapCmd) {
      fgModule->SetCheckVolumeOverlap(fCheckOverlapCmd->GetNewBoolValue(newValue));

   } else if (command == fLayerRadius0Cmd) {
      fgModule->SetLayerRAt(0, fLayerRadius0Cmd->GetNewDoubleValue(newValue));

   } else if (command == fLayerRadius1Cmd) {
      fgModule->SetLayerRAt(1, fLayerRadius1Cmd->GetNewDoubleValue(newValue));

   } else if (command == fNSensorInZ0Cmd) {
      fgModule->SetNSensorInZAt(0, fNSensorInZ0Cmd->GetNewIntValue(newValue));

   } else if (command == fNSensorInZ1Cmd) {
      fgModule->SetNSensorInZAt(1, fNSensorInZ1Cmd->GetNewIntValue(newValue));

   } else if (command == fSensorSlantAngleCmd) {
      fgModule->SetSlantAngle(fSensorSlantAngleCmd->GetNewDoubleValue(newValue));

   } else if (command == fSensorOverlapCmd) {
      fgModule->SetOverlapWidth(fSensorOverlapCmd->GetNewDoubleValue(newValue));

   } else if (command == fSensorThicknessCmd) {
      fgModule->SetSensorThickness(fSensorThicknessCmd->GetNewDoubleValue(newValue));

   } else if (command == fDepThicknessCmd) {
      fgModule->SetDepThickness(fDepThicknessCmd->GetNewDoubleValue(newValue));

   } else if (command == fPixelSizeZCmd) {
      fgModule->SetPixelSizeAt(0, fPixelSizeZCmd->GetNewDoubleValue(newValue));

   } else if (command == fPixelSizeYCmd) {
      fgModule->SetPixelSizeAt(1, fPixelSizeYCmd->GetNewDoubleValue(newValue));

   }
}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
