//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <algorithm>
#include "MAGModule.hh"
#include "G4RotationMatrix.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4Trap.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4Color.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"
#include "G4NistManager.hh"
#include "G4UnionSolid.hh"
#include "G4Region.hh"
#include "MAGConstants.hh"

namespace
{
//
//-- Set up default magnet geometry parameters
//
const G4double almag1 = ALMAG1;
const G4double almag2 = ALMAG2;
const G4double almag3 = ALMAG3;
const G4double almag4 = ALMAG4;
const G4double r1mag1 = R1MAG1;
const G4double r2mag1 = R2MAG1;
const G4double r1mag3 = R1MAG3;
const G4double r1mag4 = R1MAG4;

// Paramaters for inner cryostat wall
const G4double lecri1 = 3.1   * centimeter;
const G4double lecri2 = 104.9 * centimeter;
const G4double lecri3 = 1.5   * centimeter;
const G4double lecri4 = 15.5  * centimeter;
const G4double lecri5 = 1.8   * centimeter;
const G4double lecri6 = 1.9   * centimeter;
const G4double lecri7 = 26.6  * centimeter;
const G4double zcri7  = 0    * centimeter;
const G4double t1cry2 = 0.3  * centimeter;
const G4double t1cry3 = 5.8  * centimeter;
const G4double t1cry4 = 0.3  * centimeter;
const G4double t1cry6 = 2.0  * centimeter;
const G4double t1cry7 = 0.15 * centimeter;

// Paramaters for outer cryostat wall
const G4double lecro1 = 33.4 * centimeter;
const G4double lecro2 = 28.0 * centimeter;
const G4double lecro3 = 56.5 * centimeter;
const G4double lecro4 = 42.0 * centimeter;
const G4double zcro4  = 0    * centimeter;
const G4double t2cry1 = 1.2  * centimeter;
const G4double t2cry2 = 5.5  * centimeter;
const G4double t2cry3 = 1.5  * centimeter;
const G4double t2cry4 = 0.15 * centimeter;

// Paramaters for inner radiation shield
const G4double lerdi1 = 0.5    * centimeter;
const G4double lerdi2 = 101.05 * centimeter;
const G4double lerdi3 = 0.5    * centimeter;
const G4double lerdi4 = 14.6   * centimeter;
const G4double lerdi5 = 1.9    * centimeter;
const G4double lerdi6 = 29.3   * centimeter;
const G4double zrdi6  = 0      * centimeter;
const G4double r1rdi1 = 43.2   * centimeter;
const G4double r2rdi1 = 50.8   * centimeter;
const G4double t1rad2 = 0.3    * centimeter;
const G4double r1rdi3 = 38.5   * centimeter;
const G4double t1rad3 = 5.0    * centimeter;
const G4double t1rad4 = 0.3    * centimeter;
const G4double r1rdi6 = 33.4   * centimeter;
const G4double t1rad6 = 0.03   * centimeter;

// Paramaters for outer radiation shield
const G4double lerdo1 = 110.1 * centimeter;
const G4double lerdo2 = 1.5   * centimeter;
const G4double lerdo3 = 42.0  * centimeter;
const G4double zrdo3  = 0     * centimeter;
const G4double t2rad1 = 0.3   * centimeter;
const G4double t2rad2 = 2.33  * centimeter;
const G4double t2rad3 = 0.03  * centimeter;

// Paramaters for the support structure of the coil
const G4double lespt1 = 98.3      * centimeter;
const G4double lespt2 = 13.67     * centimeter;
const G4double lespt3 = 1.93      * centimeter;
const G4double lespt4 = 14.6  * 2 * centimeter;
const G4double lespt5 = 16.53 * 2 * centimeter;
const G4double zspt4  = 0         * centimeter;
const G4double zspt5  = 0         * centimeter;
const G4double r2spt1 = 47.0      * centimeter;
const G4double tspt1  = 0.61      * centimeter;
const G4double tspt2  = 5.585     * centimeter;
const G4double tspt3  = 1.47      * centimeter;
const G4double r1spt4 = 35.62     * centimeter;
const G4double tspt4  = 0.18      * centimeter;
const G4double r1spt5 = 42.5      * centimeter;
const G4double tspt5  = 0.18      * centimeter;

// Parameters for the coil
const G4double leco1 = 94.6  * centimeter;
const G4double leco2 = 11.74 * centimeter;
const G4double leco3 = 24.0  * centimeter;
const G4double zco1  = -78.0 * centimeter;
const G4double zco2  = -23.5 * centimeter;
const G4double zco3  = 0     * centimeter;
const G4double r1co1 = 46.0  * centimeter;
const G4double tco1  = 0.071 * centimeter;
const G4double r1co2 = 40.5  * centimeter;
const G4double tco2  = 0.135 * centimeter;
const G4double r1co3 = 35.0  * centimeter;
const G4double tco3  = 0.166 * centimeter;

const G4double almag5 = almag3;
const G4double almag6 = almag2;
const G4double almag7 = almag1;

// Total length of the magnet:
const G4double almag = almag1 + almag2 + almag3 + almag4 + almag5 + almag6 + almag7;

//
const G4double r1mag2 = r1mag1;
const G4double r2mag2 = r2mag1 - t2cry2 + t2cry3;
const G4double r2mag3 = r2mag2;
const G4double r2mag4 = r2mag2;
const G4double r1mag5 = r1mag3;
const G4double r2mag5 = r2mag3;
const G4double r1mag6 = r1mag2;
const G4double r2mag6 = r2mag2;
const G4double r1mag7 = r1mag1;
const G4double r2mag7 = r2mag1;

// Paramaters for inner cryostat wall
const G4double lecri8  = lecri6;
const G4double lecri9  = lecri5;
const G4double lecri10 = lecri4;
const G4double lecri11 = lecri3;
const G4double lecri12 = lecri2;
const G4double lecri13 = lecri1;

const G4double zcri1   = -(lecri1 / 2 + lecri2 + lecri3 + lecri4 + lecri5 + lecri6 + lecri7 / 2);
const G4double zcri2   = -(lecri2 / 2 + lecri3 + lecri4 + lecri5 + lecri6 + lecri7 / 2);
const G4double zcri3   = -(lecri3 / 2 + lecri4 + lecri5 + lecri6 + lecri7 / 2);
const G4double zcri4   = -(lecri4 / 2 + lecri5 + lecri6 + lecri7 / 2);
const G4double zcri5   = -(lecri5 / 2 + lecri6 + lecri7 / 2);
const G4double zcri6   = -(lecri6 / 2 + lecri7 / 2);
const G4double zcri8   = -zcri6;
const G4double zcri9   = -zcri5;
const G4double zcri10  = -zcri4;
const G4double zcri11  = -zcri3;
const G4double zcri12  = -zcri2;
const G4double zcri13  = -zcri1;

const G4double r1cri1  = r1mag1;
const G4double r2cri1  = r2mag1;

const G4double r1cri2  = r1mag1;
const G4double r2cri2  = r1cri2 + t1cry2;
const G4double r1cri3  = r1mag3;
const G4double r2cri3  = r1cri3 + t1cry3;

const G4double r1cri4  = r1mag3;
const G4double r2cri4  = r1cri4 + t1cry4;

const G4double r1cri5  = r1mag4;
const G4double r2cri5  = r2cri4;

const G4double r1cri6  = r1mag4;
const G4double r2cri6  = r1cri6 + t1cry6;

const G4double r1cri7  = r1mag4;
const G4double r2cri7  = r1cri7 + t1cry7;
const G4double r1cri8  = r1cri6;
const G4double r2cri8  = r2cri6;

const G4double r1cri9  = r1cri5;
const G4double r2cri9  = r2cri5;

const G4double r1cri10 = r1cri4;
const G4double r2cri10 = r2cri4;

const G4double r1cri11 = r1cri3;
const G4double r2cri11 = r2cri3;

const G4double r1cri12 = r1cri2;
const G4double r2cri12 = r2cri2;

const G4double r1cri13 = r1cri1;
const G4double r2cri13 = r2cri1;

// Paramaters for outer cryostat wall
const G4double lecro5 = lecro3;
const G4double lecro6 = lecro2;
const G4double lecro7 = lecro1;

const G4double zcro1  = -(lecro1 / 2 + lecro2 + lecro3 + lecro4 / 2);
const G4double zcro2  = -(lecro2 / 2 + lecro3 + lecro4 / 2);
const G4double zcro3  = -(lecro3 / 2 + lecro4 / 2);
const G4double zcro5  = -zcro3;
const G4double zcro6  = -zcro2;
const G4double zcro7  = -zcro1;

const G4double r1cro1 = r2mag1 - t2cry1;
const G4double r2cro1 = r2mag1;

const G4double r1cro2 = r2mag1 - t2cry2;
const G4double r2cro2 = r2mag1;

const G4double r1cro3 = r1cro2;
const G4double r2cro3 = r1cro3 + t2cry3;

const G4double r2cro4 = r2cro3;
const G4double r1cro4 = r2cro4 - t2cry4;

const G4double r1cro5 = r1cro3;
const G4double r2cro5 = r2cro3;

const G4double r1cro6 = r1cro2;
const G4double r2cro6 = r2cro2;

const G4double r1cro7 = r1cro1;
const G4double r2cro7 = r2cro1;

// Paramaters for inner radiation shield
const G4double lerdi7  = lerdi5;
const G4double lerdi8  = lerdi4;
const G4double lerdi9  = lerdi3;
const G4double lerdi10 = lerdi2;
const G4double lerdi11 = lerdi1;

const G4double zrdi1   = -(lerdi1 / 2 + lerdi2 + lerdi3 + lerdi4 + lerdi5 + lerdi6 / 2);
const G4double zrdi2   = -(lerdi2 / 2 + lerdi3 + lerdi4 + lerdi5 + lerdi6 / 2);
const G4double zrdi3   = -(lerdi3 / 2 + lerdi4 + lerdi5 + lerdi6 / 2);
const G4double zrdi4   = -(lerdi4 / 2 + lerdi5 + lerdi6 / 2);
const G4double zrdi5   = -(lerdi5 / 2 + lerdi6 / 2);
const G4double zrdi7   = -zrdi5;
const G4double zrdi8   = -zrdi4;
const G4double zrdi9   = -zrdi3;
const G4double zrdi10  = -zrdi2;
const G4double zrdi11  = -zrdi1;

const G4double r1rdi2  = r1rdi1;
const G4double r2rdi2  = r1rdi2 + t1rad2;

const G4double r2rdi3  = r1rdi3 + t1rad3;

const G4double r1rdi4  = r1rdi3;
const G4double r2rdi4  = r1rdi4 + t1rad4;

const G4double r1rdi5  = r1rdi4;
const G4double r2rdi5  = r2rdi4;

const G4double r2rdi6  = r1rdi6 + t1rad6;

const G4double r1rdi7  = r1rdi5;
const G4double r2rdi7  = r2rdi5;

const G4double r1rdi8  = r1rdi4;
const G4double r2rdi8  = r2rdi4;

const G4double r1rdi9  = r1rdi3;
const G4double r2rdi9  = r2rdi3;

const G4double r1rdi10 = r1rdi2;
const G4double r2rdi10 = r2rdi2;

const G4double r1rdi11 = r1rdi1;
const G4double r2rdi11 = r2rdi1;

// Paramaters for outer radiation shield
const G4double lerdo4 = lerdo2;
const G4double lerdo5 = lerdo1;

const G4double zrdo1  = -(lerdo1 / 2 + lerdo2 + lerdo3 / 2);
const G4double zrdo2  = -(lerdo2 / 2 + lerdo3 / 2);
const G4double zrdo4  = -zrdo2;
const G4double zrdo5  = -zrdo1;

const G4double r2rdo1 = r2rdi1;
const G4double r1rdo1 = r2rdo1 - t2rad1;

const G4double r2rdo2 = r2rdo1;
const G4double r1rdo2 = r2rdo2 - t2rad2;

const G4double r1rdo3 = r1rdo2;
const G4double r2rdo3 = r1rdo3 + t2rad3;

const G4double r1rdo4 = r1rdo2;
const G4double r2rdo4 = r2rdo2;

const G4double r1rdo5 = r1rdo1;
const G4double r2rdo5 = r2rdo1;

// Paramaters for the support structure of the coil
const G4double lespt6 = lespt3;
const G4double lespt7 = lespt2;
const G4double lespt8 = lespt1;
const G4double zspt1  = -(lespt1 / 2 + lespt2 + lespt3 + lespt4 / 2);
const G4double zspt2  = -(lespt2 / 2 + lespt3 + lespt4 / 2);
const G4double zspt3  = -(lespt3 / 2 + lespt4 / 2);
const G4double zspt6  = -zspt3;
const G4double zspt7  = -zspt2;
const G4double zspt8  = -zspt1;

const G4double r1spt1 = r2spt1 - tspt1;

const G4double r2spt2 = r2spt1;
const G4double r1spt2 = r2spt2 - tspt2;

const G4double r1spt3 = r1spt2;
const G4double r2spt3 = r1spt3 + tspt3;

const G4double r2spt4 = r1spt4 + tspt4;

const G4double r2spt5 = r1spt5 + tspt5;
const G4double r1spt6 = r1spt3;
const G4double r2spt6 = r2spt3;

const G4double r1spt7 = r1spt2;
const G4double r2spt7 = r2spt2;

const G4double r1spt8 = r1spt1;
const G4double r2spt8 = r2spt1;

// Parameters for the coil
const G4double leco4 = leco2;
const G4double leco5 = leco1;

const G4double zco4  = -zco2;
const G4double zco5  = -zco1;

const G4double r2co1 = r1co1 + tco1;

const G4double r2co2 = r1co2 + tco2;
const G4double r2co3 = r1co3 + tco3;

const G4double r1co4 = r1co2;
const G4double r2co4 = r2co2;

const G4double r1co5 = r1co1;
const G4double r2co5 = r2co1;

}

G4int MAGModule::fgMagnetType = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MAGModule::ConstructMaterials()
{
   G4double density;
   G4int nelements;

   // Elements
   G4Element *Nb = G4NistManager::Instance()->FindOrBuildElement("Nb");
   G4Element *Ti = G4NistManager::Instance()->FindOrBuildElement("Ti");
   G4Element *Cu = G4NistManager::Instance()->FindOrBuildElement("Cu");

   // Materials
   Air       = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
   Aluminium = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");
   Iron      = G4NistManager::Instance()->FindOrBuildMaterial("G4_Fe");

   // Cobra conductor
   //
   if (!CobraConductor) {
      density = 7.03 * g / cm3;
      CobraConductor = new G4Material("CobraConductor", density, nelements = 3);
      CobraConductor->AddElement(Nb, 0.222);
      CobraConductor->AddElement(Ti, 0.192);
      CobraConductor->AddElement(Cu, 0.586);
   }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4LogicalVolume *MAGModule::ConstructFGas()
{
   G4Tubs *fgas_tub = new G4Tubs("FGAS", 0, 20 * cm, 142 * cm, 0 * degree, 360 * degree);
   G4LogicalVolume *fgas_log = new G4LogicalVolume(fgas_tub, fMACONHE, "FGAS", 0, 0, 0);

   return fgas_log;
}
   
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MAGModule::BuildMagnet(G4LogicalVolume *parent)
{
   if (fgMagnetType == 1) {
      // Build COBRA magnet
      BuildCOBRA(parent);

   } else {
      // Build a simple solenoid
      
      G4VisAttributes *vis_attr = new G4VisAttributes(true, G4Colour(0.5, 0.5, 0.5));
      //vis_attr->SetForceWireframe(true);
      vis_attr->SetForceSolid(true);
      // G4VisAttributes *vis_attr = new G4VisAttributes(G4VisAttributes::GetInvisible());

      G4Tubs *coil_tubs         = new G4Tubs("CoilTube", 50 * cm, 70 * cm, 142 * cm, 0 * degree, 360 * degree);
      G4LogicalVolume *coilLog = new G4LogicalVolume(coil_tubs, CobraConductor, "CoilLog", 0, 0, 0);
      coilLog->SetVisAttributes(vis_attr);
      new G4PVPlacement(rotzr, G4ThreeVector(0, 0, 0), coilLog, "CoilVol", parent, false, 0);

      // put beam dump
      G4Tubs *dump_tubs  = new G4Tubs("DumpTub", 5 * cm, 20 * cm, 2 * cm, 0 * degree, 360 * degree);
      G4LogicalVolume *dumpLog = new G4LogicalVolume(dump_tubs, Iron, "DumpLog", 0, 0, 0);
      dumpLog->SetVisAttributes(vis_attr);
      // new G4PVPlacement(rotzr, G4ThreeVector(0, 0, -150*cm), dumpLog, "DumpUS", parent, false, 0);
      new G4PVPlacement(rotzr, G4ThreeVector(0, 0,  150*cm), dumpLog, "DumpDS", parent, false, 1);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MAGModule::BuildCOBRA(G4LogicalVolume *parent)
{
   const G4int vpmagnz = 14;
   const G4double vpmag[] = {
      0 * degree, // 0
      360 * degree,

      -almag / 2, //2
      -(almag / 2 - almag1),
      -(almag / 2 - almag1),
      -(almag / 2 - almag1 - almag2),
      -(almag / 2 - almag1 - almag2),
      -(almag / 2 - almag1 - almag2 - almag3),
      -almag4 / 2,
      +almag4 / 2,
      +(almag / 2 - almag1 - almag2 - almag3),
      +(almag / 2 - almag1 - almag2),
      +(almag / 2 - almag1 - almag2),
      +(almag / 2 - almag1),
      +(almag / 2 - almag1),
      +almag / 2,

      r1mag1, // 16
      r1mag1,
      r1mag2,
      r1mag2,
      r1mag3,
      r1mag3,
      r1mag4,
      r1mag4,
      r1mag5,
      r1mag5,
      r1mag6,
      r1mag6,
      r1mag7,
      r1mag7,

      r2mag1, // 30
      r2mag1,
      r2mag2,
      r2mag2,
      r2mag3,
      r2mag3,
      r2mag4,
      r2mag4,
      r2mag5,
      r2mag5,
      r2mag6,
      r2mag6,
      r2mag7,
      r2mag7
   };

#if 0
   G4VisAttributes *vis_attr = new G4VisAttributes();
   vis_attr->SetForceWireframe(true);
#else
   G4VisAttributes *vis_attr = new G4VisAttributes(G4VisAttributes::GetInvisible());
#endif

   G4Polycone *mag_pcon     = new G4Polycone("MAG", vpmag[0], vpmag[1], vpmagnz,
                                             vpmag + 2,
                                             vpmag + 2 + vpmagnz, vpmag + 2 + 2 * vpmagnz);
   G4LogicalVolume *mag_log = new G4LogicalVolume(mag_pcon, Air, "MAG", 0, 0, 0);
   mag_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, 0),
                     mag_log, "MAG", parent, false, 0);

   //
   //-- Define volumes for the inner cryostat
   //
   const G4double vpcri1[] = {
      r1cri1,
      r2cri1,
      lecri1 / 2
   };

   const G4double vpcri2[] = {
      r1cri2,
      r2cri2,
      lecri2 / 2
   };

   const G4double vpcri3[] = {
      r1cri3,
      r2cri3,
      lecri3 / 2
   };

   const G4double vpcri4[] = {
      r1cri4,
      r2cri4,
      lecri4 / 2
   };

   const G4double vpcri5[] = {
      lecri5 / 2,
      r1cri5,
      r2cri5,
      r1cri5,
      r2cri6
   };

   const G4double vpcri6[] = {
      r1cri6,
      r2cri6,
      lecri6 / 2
   };

   const G4double vpcri7[] = {
      r1cri7,
      r2cri7,
      lecri7 / 2
   };

   const G4double vpcri8[] = {
      r1cri8,
      r2cri8,
      lecri8 / 2
   };

   const G4double vpcri9[] = {
      lecri9 / 2,
      r1cri9,
      r2cri8,
      r1cri9,
      r2cri9
   };

   const G4double vpcri10[] = {
      r1cri10,
      r2cri10,
      lecri10 / 2
   };

   const G4double vpcri11[] = {
      r1cri11,
      r2cri11,
      lecri11 / 2
   };

   const G4double vpcri12[] = {
      r1cri12,
      r2cri12,
      lecri12 / 2
   };

   const G4double vpcri13[] = {
      r1cri13,
      r2cri13,
      lecri13 / 2
   };

   G4Tubs *cri1_tubs         = new G4Tubs("CRI1", vpcri1[0], vpcri1[1], vpcri1[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri1_log = new G4LogicalVolume(cri1_tubs, Iron, "CRI1", 0, 0, 0);
   cri1_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri1), cri1_log, "CRI1", mag_log, false, 0);

   G4Tubs *cri2_tubs         = new G4Tubs("CRI2", vpcri2[0], vpcri2[1], vpcri2[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri2_log = new G4LogicalVolume(cri2_tubs, Iron, "CRI2", 0, 0, 0);
   cri2_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri2), cri2_log, "CRI2", mag_log, false, 0);

   G4Tubs *cri3_tubs         = new G4Tubs("CRI3", vpcri3[0], vpcri3[1], vpcri3[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri3_log = new G4LogicalVolume(cri3_tubs, Iron, "CRI3", 0, 0, 0);
   cri3_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri3), cri3_log, "CRI3", mag_log, false, 0);

   G4Tubs *cri4_tubs         = new G4Tubs("CRI4", vpcri4[0], vpcri4[1], vpcri4[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri4_log = new G4LogicalVolume(cri4_tubs, Iron, "CRI4", 0, 0, 0);
   cri4_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri4), cri4_log, "CRI4", mag_log, false, 0);

   G4Cons *cri5_cons         = new G4Cons("CRI5", vpcri5[1], vpcri5[2], vpcri5[3], vpcri5[4], vpcri5[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *cri5_log = new G4LogicalVolume(cri5_cons, Iron, "CRI5", 0, 0, 0);
   cri5_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri5), cri5_log, "CRI5", mag_log, false, 0);

   G4Tubs *cri6_tubs         = new G4Tubs("CRI6", vpcri6[0], vpcri6[1], vpcri6[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri6_log = new G4LogicalVolume(cri6_tubs, Iron, "CRI6", 0, 0, 0);
   cri6_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri6), cri6_log, "CRI6", mag_log, false, 0);

   G4Tubs *cri7_tubs         = new G4Tubs("CRI7", vpcri7[0], vpcri7[1], vpcri7[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri7_log = new G4LogicalVolume(cri7_tubs, Aluminium, "CRI7", 0, 0, 0);
   cri7_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri7), cri7_log, "CRI7", mag_log, false, 0);

   G4Tubs *cri8_tubs         = new G4Tubs("CRI8", vpcri8[0], vpcri8[1], vpcri8[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cri8_log = new G4LogicalVolume(cri8_tubs, Iron, "CRI8", 0, 0, 0);
   cri8_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri8), cri8_log, "CRI8", mag_log, false, 0);

   G4Cons *cri9_cons         = new G4Cons("CRI9", vpcri9[1], vpcri9[2], vpcri9[3], vpcri9[4], vpcri9[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *cri9_log = new G4LogicalVolume(cri9_cons, Iron, "CRI9", 0, 0, 0);
   cri9_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri9), cri9_log, "CRI9", mag_log, false, 0);

   G4Tubs *cri10_tubs         = new G4Tubs("CRI10", vpcri10[0], vpcri10[1], vpcri10[2], 0 * degree,
                                           360 * degree);
   G4LogicalVolume *cri10_log = new G4LogicalVolume(cri10_tubs, Iron, "CRI10", 0, 0, 0);
   cri10_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri10), cri10_log, "CRI10", mag_log, false, 0);

   G4Tubs *cri11_tubs         = new G4Tubs("CRI11", vpcri11[0], vpcri11[1], vpcri11[2], 0 * degree,
                                           360 * degree);
   G4LogicalVolume *cri11_log = new G4LogicalVolume(cri11_tubs, Iron, "CRI11", 0, 0, 0);
   cri11_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri11), cri11_log, "CRI11", mag_log, false, 0);

   G4Tubs *cri12_tubs         = new G4Tubs("CRI12", vpcri12[0], vpcri12[1], vpcri12[2], 0 * degree,
                                           360 * degree);
   G4LogicalVolume *cri12_log = new G4LogicalVolume(cri12_tubs, Iron, "CRI12", 0, 0, 0);
   cri12_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri12), cri12_log, "CRI12", mag_log, false, 0);

   G4Tubs *cri13_tubs         = new G4Tubs("CRI13", vpcri13[0], vpcri13[1], vpcri13[2], 0 * degree,
                                           360 * degree);
   G4LogicalVolume *cri13_log = new G4LogicalVolume(cri13_tubs, Iron, "CRI13", 0, 0, 0);
   cri13_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcri13), cri13_log, "CRI13", mag_log, false, 0);

   //
   //-- Define volumes for the outer cryostat
   //
   const G4double vpcro1[] = {
      r1cro1,
      r2cro1,
      lecro1 / 2
   };

   const G4double vpcro2[] = {
      r1cro2,
      r2cro2,
      lecro2 / 2
   };

   const G4double vpcro3[] = {
      r1cro3,
      r2cro3,
      lecro3 / 2
   };

   const G4double vpcro4[] = {
      r1cro4,
      r2cro4,
      lecro4 / 2
   };

   const G4double vpcro5[] = {
      r1cro5,
      r2cro5,
      lecro5 / 2
   };

   const G4double vpcro6[] = {
      r1cro6,
      r2cro6,
      lecro6 / 2
   };

   const G4double vpcro7[] = {
      r1cro7,
      r2cro7,
      lecro7 / 2
   };

   //
   // Define the volume for the outer cryostat wall
   //
   G4Tubs *cro1_tubs         = new G4Tubs("CRO1", vpcro1[0], vpcro1[1], vpcro1[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro1_log = new G4LogicalVolume(cro1_tubs, Iron, "CRO1", 0, 0, 0);
   cro1_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro1), cro1_log, "CRO1", mag_log, false, 0);

   G4Tubs *cro2_tubs         = new G4Tubs("CRO2", vpcro2[0], vpcro2[1], vpcro2[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro2_log = new G4LogicalVolume(cro2_tubs, Iron, "CRO2", 0, 0, 0);
   cro2_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro2), cro2_log, "CRO2", mag_log, false, 0);

   G4Tubs *cro3_tubs         = new G4Tubs("CRO3", vpcro3[0], vpcro3[1], vpcro3[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro3_log = new G4LogicalVolume(cro3_tubs, Iron, "CRO3", 0, 0, 0);
   cro3_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro3), cro3_log, "CRO3", mag_log, false, 0);

   G4Tubs *cro4_tubs         = new G4Tubs("CRO4", vpcro4[0], vpcro4[1], vpcro4[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro4_log = new G4LogicalVolume(cro4_tubs, Aluminium, "CRO4", 0, 0, 0);
   cro4_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro4), cro4_log, "CRO4", mag_log, false, 0);

   G4Tubs *cro5_tubs         = new G4Tubs("CRO5", vpcro5[0], vpcro5[1], vpcro5[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro5_log = new G4LogicalVolume(cro5_tubs, Iron, "CRO5", 0, 0, 0);
   cro5_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro5), cro5_log, "CRO5", mag_log, false, 0);

   G4Tubs *cro6_tubs         = new G4Tubs("CRO6", vpcro6[0], vpcro6[1], vpcro6[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro6_log = new G4LogicalVolume(cro6_tubs, Iron, "CRO6", 0, 0, 0);
   cro6_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro6), cro6_log, "CRO6", mag_log, false, 0);

   G4Tubs *cro7_tubs         = new G4Tubs("CRO7", vpcro7[0], vpcro7[1], vpcro7[2], 0 * degree, 360 * degree);
   G4LogicalVolume *cro7_log = new G4LogicalVolume(cro7_tubs, Iron, "CRO7", 0, 0, 0);
   cro7_log->SetVisAttributes(vis_attr);
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zcro7), cro7_log, "CRO7", mag_log, false, 0);

   //
   //-- Define volumes for the inner radiation shield
   //
   const G4double vprdi1[] = {
      r1rdi1,
      r2rdi1,
      lerdi1 / 2
   };

   const G4double vprdi2[] = {
      r1rdi2,
      r2rdi2,
      lerdi2 / 2
   };

   const G4double vprdi3[] = {
      r1rdi3,
      r2rdi3,
      lerdi3 / 2
   };

   const G4double vprdi4[] = {
      r1rdi4,
      r2rdi4,
      lerdi4 / 2
   };

   const G4double vprdi5[] = {
      lerdi5 / 2,
      r1rdi5,
      r2rdi5,
      r1rdi6,
      r2rdi6
   };

   const G4double vprdi6[] = {
      r1rdi6,
      r2rdi6,
      lerdi6 / 2
   };

   const G4double vprdi7[] = {
      lerdi7 / 2,
      r1rdi6,
      r2rdi6,
      r1rdi7,
      r2rdi7
   };

   const G4double vprdi8[] = {
      r1rdi8,
      r2rdi8,
      lerdi8 / 2
   };

   const G4double vprdi9[] = {
      r1rdi9,
      r2rdi9,
      lerdi9 / 2
   };

   const G4double vprdi10[] = {
      r1rdi10,
      r2rdi10,
      lerdi10 / 2
   };

   const G4double vprdi11[] = {
      r1rdi11,
      r2rdi11,
      lerdi11 / 2
   };

   G4Tubs *rdi1_tubs         = new G4Tubs("RDI1", vprdi1[0], vprdi1[1], vprdi1[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi1_log = new G4LogicalVolume(rdi1_tubs, Iron, "RDI1", 0, 0, 0);
   rdi1_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi1), rdi1_log, "RDI1", mag_log, false, 0);

   G4Tubs *rdi2_tubs         = new G4Tubs("RDI2", vprdi2[0], vprdi2[1], vprdi2[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi2_log = new G4LogicalVolume(rdi2_tubs, Iron, "RDI2", 0, 0, 0);
   rdi2_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi2), rdi2_log, "RDI2", mag_log, false, 0);

   G4Tubs *rdi3_tubs         = new G4Tubs("RDI3", vprdi3[0], vprdi3[1], vprdi3[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi3_log = new G4LogicalVolume(rdi3_tubs, Iron, "RDI3", 0, 0, 0);
   rdi3_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi3), rdi3_log, "RDI3", mag_log, false, 0);

   G4Tubs *rdi4_tubs         = new G4Tubs("RDI4", vprdi4[0], vprdi4[1], vprdi4[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi4_log = new G4LogicalVolume(rdi4_tubs, Iron, "RDI4", 0, 0, 0);
   rdi4_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi4), rdi4_log, "RDI4", mag_log, false, 0);

   G4Cons *rdi5_cons         = new G4Cons("RDI5", vprdi5[1], vprdi5[2], vprdi5[3], vprdi5[4], vprdi5[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *rdi5_log = new G4LogicalVolume(rdi5_cons, Iron, "RDI5", 0, 0, 0);
   rdi5_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi5), rdi5_log, "RDI5", mag_log, false, 0);

   G4Tubs *rdi6_tubs         = new G4Tubs("RDI6", vprdi6[0], vprdi6[1], vprdi6[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi6_log = new G4LogicalVolume(rdi6_tubs, Aluminium, "RDI6", 0, 0, 0);
   rdi6_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi6), rdi6_log, "RDI6", mag_log, false, 0);

   G4Cons *rdi7_cons         = new G4Cons("RDI7", vprdi7[1], vprdi7[2], vprdi7[3], vprdi7[4], vprdi7[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *rdi7_log = new G4LogicalVolume(rdi7_cons, Iron, "RDI7", 0, 0, 0);
   rdi7_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi7), rdi7_log, "RDI7", mag_log, false, 0);

   G4Tubs *rdi8_tubs         = new G4Tubs("RDI8", vprdi8[0], vprdi8[1], vprdi8[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi8_log = new G4LogicalVolume(rdi8_tubs, Iron, "RDI8", 0, 0, 0);
   rdi8_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi8), rdi8_log, "RDI8", mag_log, false, 0);

   G4Tubs *rdi9_tubs         = new G4Tubs("RDI9", vprdi9[0], vprdi9[1], vprdi9[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdi9_log = new G4LogicalVolume(rdi9_tubs, Iron, "RDI9", 0, 0, 0);
   rdi9_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi9), rdi9_log, "RDI9", mag_log, false, 0);

   G4Tubs *rdi10_tubs         = new G4Tubs("RDI10", vprdi10[0], vprdi10[1], vprdi10[2], 0 * degree,
                                           360 * degree);
   G4LogicalVolume *rdi10_log = new G4LogicalVolume(rdi10_tubs, Iron, "RDI10", 0, 0, 0);
   rdi10_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi10), rdi10_log, "RDI10", mag_log, false, 0);

   G4Tubs *rdi11_tubs         = new G4Tubs("RDI11", vprdi11[0], vprdi11[1], vprdi11[2], 0 * degree,
                                           360 * degree);
   G4LogicalVolume *rdi11_log = new G4LogicalVolume(rdi11_tubs, Iron, "RDI11", 0, 0, 0);
   rdi11_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdi11), rdi11_log, "RDI11", mag_log, false, 0);

   //
   //-- Define volumes for the outer radiation shield
   //
   const G4double vprdo1[] = {
      r1rdo1,
      r2rdo1,
      lerdo1 / 2
   };

   const G4double vprdo2[] = {
      r1rdo2,
      r2rdo2,
      lerdo2 / 2
   };

   const G4double vprdo3[] = {
      r1rdo3,
      r2rdo3,
      lerdo3 / 2
   };

   const G4double vprdo4[] = {
      r1rdo4,
      r2rdo4,
      lerdo4 / 2
   };

   const G4double vprdo5[] = {
      r1rdo5,
      r2rdo5,
      lerdo5 / 2
   };

   G4Tubs *rdo1_tubs         = new G4Tubs("RDO1", vprdo1[0], vprdo1[1], vprdo1[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdo1_log = new G4LogicalVolume(rdo1_tubs, Iron, "RDO1", 0, 0, 0);
   rdo1_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdo1), rdo1_log, "RDO1", mag_log, false, 0);

   G4Tubs *rdo2_tubs         = new G4Tubs("RDO2", vprdo2[0], vprdo2[1], vprdo2[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdo2_log = new G4LogicalVolume(rdo2_tubs, Iron, "RDO2", 0, 0, 0);
   rdo2_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdo2), rdo2_log, "RDO2", mag_log, false, 0);

   G4Tubs *rdo3_tubs         = new G4Tubs("RDO3", vprdo3[0], vprdo3[1], vprdo3[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdo3_log = new G4LogicalVolume(rdo3_tubs, Aluminium, "RDO3", 0, 0, 0);
   rdo3_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdo3), rdo3_log, "RDO3", mag_log, false, 0);

   G4Tubs *rdo4_tubs         = new G4Tubs("RDO4", vprdo4[0], vprdo4[1], vprdo4[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdo4_log = new G4LogicalVolume(rdo4_tubs, Iron, "RDO4", 0, 0, 0);
   rdo4_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdo4), rdo4_log, "RDO4", mag_log, false, 0);

   G4Tubs *rdo5_tubs         = new G4Tubs("RDO5", vprdo5[0], vprdo5[1], vprdo5[2], 0 * degree, 360 * degree);
   G4LogicalVolume *rdo5_log = new G4LogicalVolume(rdo5_tubs, Iron, "RDO5", 0, 0, 0);
   rdo5_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zrdo5), rdo5_log, "RDO5", mag_log, false, 0);

   //
   //-- Define volumes for the support structure of the coil
   //
   const G4double vpspt1[] = {
      r1spt1,
      r2spt1,
      lespt1 / 2
   };

   const G4double vpspt2[] = {
      lespt2 / 2,
      r1spt2,
      r2spt2,
      r1spt3,
      r2spt5
   };

   const G4double vpspt3[] = {
      lespt3 / 2,
      r1spt3,
      r2spt3,
      r1spt4,
      r2spt4
   };

   const G4double vpspt4[] = {
      r1spt4,
      r2spt4,
      lespt4 / 2
   };

#if 0
   const G4double vpspt5[] = {
      r1spt5,
      r2spt5,
      lespt5 / 2
   };
#else
   // avoid overlap
   const G4int vpspt5nz = 4;
   const G4double vpspt5[] = {
      0 * degree,
      360 * degree,

      // z
      std::max(-lespt5 / 2, -lespt3 / (r2spt3 - r1spt3) * (r2spt5 - r1spt3) + zspt3 + lespt3 / 2),
      std::max(-lespt5 / 2, -lespt3 / (r2spt3 - r1spt3) * (r1spt5 - r1spt3) + zspt3 + lespt3 / 2),
      std::min(lespt5 / 2,  lespt6 / (r2spt6 - r1spt6) * (r1spt5 - r1spt6) + zspt6 - lespt6 / 2),
      std::min(lespt5 / 2,  lespt6 / (r2spt6 - r1spt6) * (r2spt5 - r1spt6) + zspt6 - lespt6 / 2),

      // rmin
      r2spt5,
      r1spt5,
      r1spt5,
      r2spt5,

      // rmax
      r2spt5,
      r2spt5,
      r2spt5,
      r2spt5
   };
#endif

   const G4double vpspt6[] = {
      lespt6 / 2,
      r1spt4,
      r2spt4,
      r1spt6,
      r2spt6
   };

   const G4double vpspt7[] = {
      lespt7 / 2,
      r1spt6,
      r2spt5,
      r1spt7,
      r2spt7
   };

   const G4double vpspt8[] = {
      r1spt8,
      r2spt8,
      lespt8 / 2
   };

   G4Tubs *spt1_tubs         = new G4Tubs("SPT1", vpspt1[0], vpspt1[1], vpspt1[2], 0 * degree, 360 * degree);
   G4LogicalVolume *spt1_log = new G4LogicalVolume(spt1_tubs, Iron, "SPT1", 0, 0, 0);
   spt1_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt1), spt1_log, "SPT1", mag_log, false, 0);

   G4Cons *spt2_cons         = new G4Cons("SPT2", vpspt2[1], vpspt2[2], vpspt2[3], vpspt2[4], vpspt2[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *spt2_log = new G4LogicalVolume(spt2_cons, Iron, "SPT2", 0, 0, 0);
   spt2_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt2), spt2_log, "SPT2", mag_log, false, 0);

   G4Cons *spt3_cons         = new G4Cons("SPT3", vpspt3[1], vpspt3[2], vpspt3[3], vpspt3[4], vpspt3[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *spt3_log = new G4LogicalVolume(spt3_cons, Iron, "SPT3", 0, 0, 0);
   spt3_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt3), spt3_log, "SPT3", mag_log, false, 0);

   G4Tubs *spt4_tubs         = new G4Tubs("SPT4", vpspt4[0], vpspt4[1], vpspt4[2], 0 * degree, 360 * degree);
   G4LogicalVolume *spt4_log = new G4LogicalVolume(spt4_tubs, Iron, "SPT4", 0, 0, 0);
   spt4_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt4), spt4_log, "SPT4", mag_log, false, 0);

#if 0
   G4Tubs *spt5_tubs         = new G4Tubs("SPT5", vpspt5[0], vpspt5[1], vpspt5[2], 0 * degree, 360 * degree);
   G4LogicalVolume *spt5_log = new G4LogicalVolume(spt5_tubs, Iron, "SPT5", 0, 0, 0);
   spt5_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt5), spt5_log, "SPT5", mag_log, false, 0);
#else
   // avoid overlap
   G4Polycone *spt5_pcon     = new G4Polycone("SPT5", vpspt5[0], vpspt5[1], vpspt5nz,
                                              vpspt5 + 2, vpspt5 + 6, vpspt5 + 10);
   G4LogicalVolume *spt5_log = new G4LogicalVolume(spt5_pcon, Iron, "SPT5", 0, 0, 0);
   spt5_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt5), spt5_log, "SPT5", mag_log, false, 0);
#endif

   G4Cons *spt6_cons         = new G4Cons("SPT6", vpspt6[1], vpspt6[2], vpspt6[3], vpspt6[4], vpspt6[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *spt6_log = new G4LogicalVolume(spt6_cons, Iron, "SPT6", 0, 0, 0);
   spt6_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt6), spt6_log, "SPT6", mag_log, false, 0);

   G4Cons *spt7_cons         = new G4Cons("SPT7", vpspt7[1], vpspt7[2], vpspt7[3], vpspt7[4], vpspt7[0],
                                          0 * degree, 360 * degree);
   G4LogicalVolume *spt7_log = new G4LogicalVolume(spt7_cons, Iron, "SPT7", 0, 0, 0);
   spt7_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt7), spt7_log, "SPT7", mag_log, false, 0);

   G4Tubs *spt8_tubs         = new G4Tubs("SPT8", vpspt8[0], vpspt8[1], vpspt8[2], 0 * degree, 360 * degree);
   G4LogicalVolume *spt8_log = new G4LogicalVolume(spt8_tubs, Iron, "SPT8", 0, 0, 0);
   spt8_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zspt8), spt8_log, "SPT8", mag_log, false, 0);

   //
   //-- Define volumes for coils
   //
   const G4double vpco1[] = {
      r1co1,
      r2co1,
      leco1 / 2
   };

   const G4double vpco2[] = {
      r1co2,
      r2co2,
      leco2 / 2
   };

   const G4double vpco3[] = {
      r1co3,
      r2co3,
      leco3 / 2
   };

   const G4double vpco4[] = {
      r1co4,
      r2co4,
      leco4 / 2
   };

   const G4double vpco5[] = {
      r1co5,
      r2co5,
      leco5 / 2
   };

   G4Tubs *sco1_tubs         = new G4Tubs("SCO1", vpco1[0], vpco1[1], vpco1[2], 0 * degree, 360 * degree);
   G4LogicalVolume *sco1_log = new G4LogicalVolume(sco1_tubs, CobraConductor, "SCO1", 0, 0, 0);
   sco1_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zco1), sco1_log, "SCO1", mag_log, false, 0);

   G4Tubs *sco2_tubs         = new G4Tubs("SCO2", vpco2[0], vpco2[1], vpco2[2], 0 * degree, 360 * degree);
   G4LogicalVolume *sco2_log = new G4LogicalVolume(sco2_tubs, CobraConductor, "SCO2", 0, 0, 0);
   sco2_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zco2), sco2_log, "SCO2", mag_log, false, 0);

   G4Tubs *sco3_tubs         = new G4Tubs("SCO3", vpco3[0], vpco3[1], vpco3[2], 0 * degree, 360 * degree);
   G4LogicalVolume *sco3_log = new G4LogicalVolume(sco3_tubs, CobraConductor, "SCO3", 0, 0, 0);
   sco3_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zco3), sco3_log, "SCO3", mag_log, false, 0);

   G4Tubs *sco4_tubs         = new G4Tubs("SCO4", vpco4[0], vpco4[1], vpco4[2], 0 * degree, 360 * degree);
   G4LogicalVolume *sco4_log = new G4LogicalVolume(sco4_tubs, CobraConductor, "SCO4", 0, 0, 0);
   sco4_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zco4), sco4_log, "SCO4", mag_log, false, 0);

   G4Tubs *sco5_tubs         = new G4Tubs("SCO5", vpco5[0], vpco5[1], vpco5[2], 0 * degree, 360 * degree);
   G4LogicalVolume *sco5_log = new G4LogicalVolume(sco5_tubs, CobraConductor, "SCO5", 0, 0, 0);
   sco5_log->SetVisAttributes(G4VisAttributes::GetInvisible());
   new G4PVPlacement(rotzr, G4ThreeVector(0, 0, zco5), sco5_log, "SCO5", mag_log, false, 0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MAGModule::Construct(G4LogicalVolume *parent)
{
   ConstructMaterials();
   BuildMagnet(parent);
//   SetRegionCuts();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MAGModule::MAGModule(const char* name/*, GEMPhysicsList *p*/)
   : GEMAbsModule(name)
//,fgasRegion(0)
//,physics(p)
   , Air(0)
   , Aluminium(0)
   , Iron(0)
   , CobraConductor(0)
   , rotzr(0)
{
   fRunAction      = 0;
   fEventAction    = 0;
   fStackingAction = 0;
   fSteppingAction = 0;
   fTrackingAction = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MAGModule::~MAGModule()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GEMAbsUserEventInformation* MAGModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GEMAbsUserTrackInformation* MAGModule::MakeTrackInformation(const G4Track* /*anTrack*/)
{
   return 0;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
