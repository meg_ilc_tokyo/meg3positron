//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4Track.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMUserPrimaryVertexInformation.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserPrimaryVertexInformation::GEMUserPrimaryVertexInformation()
   : vtxid(0)
   , vtype(0)
   , gcode(-1)
   , xvtx(0)
   , yvtx(0)
   , zvtx(0)
   , tvtx(0)
   , xmom(0)
   , ymom(0)
   , zmom(0)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GEMUserPrimaryVertexInformation::GEMUserPrimaryVertexInformation(const G4Track *aTrack)
   : vtxid(0)
   , vtype(0)
   , gcode(-1)
   , xvtx(0)
   , yvtx(0)
   , zvtx(0)
   , tvtx(0)
   , xmom(0)
   , ymom(0)
   , zmom(0)
{
   if (aTrack) {
#if 0
      vtxid; // not implemented
      vtype; // not implemented
#endif
      gcode = GEMUserTrackInformation::GetG3ParticleID(aTrack->GetDefinition());
      xvtx  = aTrack->GetPosition().x();
      yvtx  = aTrack->GetPosition().y();
      zvtx  = aTrack->GetPosition().z();
      tvtx  = aTrack->GetGlobalTime();
      xmom  = aTrack->GetMomentum().x();
      ymom  = aTrack->GetMomentum().y();
      zmom  = aTrack->GetMomentum().z();
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserPrimaryVertexInformation::~GEMUserPrimaryVertexInformation()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
