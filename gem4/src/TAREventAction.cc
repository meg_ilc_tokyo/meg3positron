//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "generated/MEGMCTargetEvent.h"
#include "generated/MEGMCTargetSubevent.h"
#include "TAREventAction.hh"
#include "GEMUserEventInformation.hh"
#include "TARUserEventInformation.hh"

#include "GEMRootIO.hh"

#include "G4OpticalPhoton.hh"
#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include "G4UImanager.hh"

#include "GEMUnitConversion.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TAREventAction::TAREventAction()
   : idx(-1)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TAREventAction::~TAREventAction()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TAREventAction::BeginOfEventAction(const G4Event* /*anEvent*/)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TAREventAction::EndOfEventAction(const G4Event* /* anEvent */)
{
   TARUserEventInformation* eveInfo =
      dynamic_cast<TARUserEventInformation*>(
         dynamic_cast<GEMUserEventInformation*>(
            G4EventManager::GetEventManager()->GetConstCurrentEvent()->
            GetUserInformation())->GetModuleAt(idx));

   // Copy event information to ROME folders
   // MEGMCTargetEvent *romeEvent     = GEMRootIO::GetInstance()->GetMCTargetEvent();
   TClonesArray     *romeEvents    = GEMRootIO::GetInstance()->GetMCTargetEvents();
   TClonesArray     *romeSubevents = GEMRootIO::GetInstance()->GetMCTargetSubevents();

   romeSubevents->Delete();
   romeSubevents->ExpandCreate(0); // clear
   romeSubevents->ExpandCreate(1);
   MEGMCTargetSubevent *romeSubevent = static_cast<MEGMCTargetSubevent*>(romeSubevents->At(0));
   romeEvents->Delete();
   romeEvents->ExpandCreate(0); // clear
   romeEvents->ExpandCreate(1);
   MEGMCTargetEvent *romeEvent = static_cast<MEGMCTargetEvent*>(romeEvents->At(0));

   romeEvent->Setsevid(0);
   romeEvent->SetvecInAt(0, eveInfo->vecIn.x());
   romeEvent->SetvecInAt(1, eveInfo->vecIn.y());
   romeEvent->SetvecInAt(2, eveInfo->vecIn.z());
   romeEvent->SetvecOutAt(0, eveInfo->vecOut.x());
   romeEvent->SetvecOutAt(1, eveInfo->vecOut.y());
   romeEvent->SetvecOutAt(2, eveInfo->vecOut.z());
   romeSubevent->SetvecInAt(0, eveInfo->vecIn.x());
   romeSubevent->SetvecInAt(1, eveInfo->vecIn.y());
   romeSubevent->SetvecInAt(2, eveInfo->vecIn.z());
   romeSubevent->SetvecOutAt(0, eveInfo->vecOut.x());
   romeSubevent->SetvecOutAt(1, eveInfo->vecOut.y());
   romeSubevent->SetvecOutAt(2, eveInfo->vecOut.z());
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
