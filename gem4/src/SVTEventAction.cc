//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "generated/MEGMCSVTEvent.h"
#include "generated/MEGMCSVTPixelEvent.h"
#include "generated/MEGMCSVTSubevent.h"

#include "SVTEventAction.hh"
#include "SVTSiHit.hh"
#include "SVTPixelHit.hh"

#include "GEMUserEventInformation.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMRootIO.hh"
#include "GEMUnitConversion.hh"

#include "G4EventManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4SystemOfUnits.hh"
#include <set>
#include <map>

using namespace std;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTEventAction::SVTEventAction()
:idx(-1)
, fSiHitsCollectionID(-1)
, fPixelHitsCollectionID(-1)
, fVerbose(0)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTEventAction::~SVTEventAction()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTEventAction::BeginOfEventAction(const G4Event* /*anEvent*/)
{
   if (fSiHitsCollectionID < 0) {
      G4SDManager * sdMan = G4SDManager::GetSDMpointer();
      fSiHitsCollectionID    = sdMan->GetCollectionID("SVTSiHitsCollection");
      fPixelHitsCollectionID = sdMan->GetCollectionID("SVTPixelHitsCollection");
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTEventAction::EndOfEventAction(const G4Event* anEvent)
{
   
   // Pointer to hits collection
   G4HCofThisEvent *hitsColEvent = anEvent->GetHCofThisEvent();
   SVTSiHitsCollection    *pSiHitsCol(0);
   SVTPixelHitsCollection *pPixelHitsCol(0);

   // Get hits collections
   if (hitsColEvent) {
      if (fSiHitsCollectionID >= 0)
         pSiHitsCol = (SVTSiHitsCollection*)(hitsColEvent->GetHC(fSiHitsCollectionID));
      if (fPixelHitsCollectionID >= 0)
         pPixelHitsCol = (SVTPixelHitsCollection*)(hitsColEvent->GetHC(fPixelHitsCollectionID));
   }

   // Pointer to hit object in gem4
   // SVTSiHit    *pSiHit(0);
   SVTPixelHit *pPixelHit(0);

   GEMUserEventInformation* gemEveInfo =
      dynamic_cast<GEMUserEventInformation*>(
         G4EventManager::GetEventManager()->GetConstCurrentEvent()->
         GetUserInformation());
   
   // time reference for output
   G4double timeOI = gemEveInfo->timeOfInterest;
   

   // Get pointers to ROME folders
   TClonesArray       *pSubevents   = GEMRootIO::GetInstance()->GetMCSVTSubevents();
   MEGMCSVTEvent      *pEvent       = GEMRootIO::GetInstance()->GetMCSVTEvent();
   TClonesArray       *pPixelEvents = GEMRootIO::GetInstance()->GetMCSVTPixelEvents();
//   TClonesArray       *pGEMHits(0);
   TClonesArray       *pSubMCHits(0);
   TClonesArray       *pEveMCHits(0);
//   MEGSVTGEMHit       *pGEMHit(0);
   MEGSVTMCHit        *pSubMCHit(0);
   MEGSVTMCHit        *pEveMCHit(0);
   MEGMCSVTSubevent   *pSubevent(0);
   MEGMCSVTPixelEvent *pPixelEvent(0);

   //
   // Fill sub-event output
   //
   if (pSubevents->GetEntriesFast() != 1) { // Number of subevents is always 1 in gem
      pSubevents->Delete();
      //pSubevents->ExpandCreate(0); // clear
      pSubevents->ExpandCreate(1);
   }
   pSubevent = static_cast<MEGMCSVTSubevent*>(pSubevents->At(0));

//   pGEMHits = pSubevent->GetSVTGEMHit();
   pSubMCHits  = pSubevent->GetSVTMCHit();

//    // Copy SVTSiHit to SVTGEMHit
//    pGEMHits->Clear(); // SVTGEMHit doesn't have member allocating memory
//    if (pSiHitsCol) {
//       TClonesArray &hits = *pGEMHits;
//       G4int nhit(0);
//       G4int nHit = pSiHitsCol->entries();
//       for (G4int iHit = 0; iHit < nHit; iHit++) {
//          pSiHit  = (*pSiHitsCol)[iHit];
//          if (pSiHit) {
//             pGEMHit = new(hits[nhit++]) MEGSVTGEMHit(); // create a new hit in array
// //             pGEMHit->Settrackid(pSiHit->GetTrackID());
//             pGEMHit->Settrackid(gemEveInfo->GetTrackInfoIndexFromID(pSiHit->GetTrackID()));
//             pGEMHit->Setparticleid(pSiHit->GetParticleID());
//             pGEMHit->Seticode(pSiHit->GetChipID());
//             pGEMHit->Settime((pSiHit->GetTime() - timeOI) * kUnitConvTime);
//             pGEMHit->SetxyzpreAt(0, pSiHit->GetXPre() * kUnitConvLength);
//             pGEMHit->SetxyzpreAt(1, pSiHit->GetYPre() * kUnitConvLength);
//             pGEMHit->SetxyzpreAt(2, pSiHit->GetZPre() * kUnitConvLength);
//             pGEMHit->SetxyzpostAt(0, pSiHit->GetXPost() * kUnitConvLength);
//             pGEMHit->SetxyzpostAt(1, pSiHit->GetYPost() * kUnitConvLength);
//             pGEMHit->SetxyzpostAt(2, pSiHit->GetZPost() * kUnitConvLength);
//             pGEMHit->SetmomAt(0, pSiHit->GetMomentumX() * kUnitConvEnergy);
//             pGEMHit->SetmomAt(1, pSiHit->GetMomentumY() * kUnitConvEnergy);
//             pGEMHit->SetmomAt(2, pSiHit->GetMomentumZ() * kUnitConvEnergy);
//             pGEMHit->Seteloss(pSiHit->GetEloss() * kUnitConvEnergy);
//             pGEMHit->Setstepl(pSiHit->GetStepLength() * kUnitConvLength);
//          }
//       }
//    }

   // Copy SVTPixelHit to SVTMCHit
   const G4double kMinEnergyDeposit = 300 * eV; // low enough compared to digitization threshold
   set<pair<G4int,G4int> > pixelSet;
   pSubMCHits->Delete(); // SVTMCHit has member allocating memory
   if (pPixelHitsCol) {
      TClonesArray &hits = *pSubMCHits;
      G4int nhit(0);
      G4int nHit = pPixelHitsCol->entries();
      for (G4int iHit = 0; iHit < nHit; iHit++) {
         pPixelHit = (*pPixelHitsCol)[iHit];
         if (pPixelHit) {
            G4double totalEdep = pPixelHit->GetEdepTot(); 
            // simple hit selections
            if (totalEdep < kMinEnergyDeposit * 0.1) continue;

            pSubMCHit = new(hits[nhit++]) MEGSVTMCHit(); // create a new hit in array
//             pSubMCHit->Settrackid(pPixelHit->GetTrackID()); // trackID in gem
            pSubMCHit->Settrackid(gemEveInfo->GetTrackInfoIndexFromID(pPixelHit->GetTrackID()));
            pSubMCHit->SettrackIndex(gemEveInfo->GetTrackInfoIndexFromID(pPixelHit->GetTrackID()));
            pSubMCHit->Setlayerid(pPixelHit->GetLayerID());
            pSubMCHit->Settime((pPixelHit->GetTime() - timeOI) * kUnitConvTime);
            pSubMCHit->SetxyzAt(0, pPixelHit->GetXMean() * kUnitConvLength);
            pSubMCHit->SetxyzAt(1, pPixelHit->GetYMean() * kUnitConvLength);
            pSubMCHit->SetxyzAt(2, pPixelHit->GetZMean() * kUnitConvLength);
            pSubMCHit->Seteloss(pPixelHit->GetEloss() * kUnitConvEnergy);
            pSubMCHit->Setedeptot(totalEdep * kUnitConvEnergy);
            pSubMCHit->Setpathlength(pPixelHit->GetPathLength() * kUnitConvLength);
            pSubMCHit->Setincangle(pPixelHit->GetIncAngle() * kUnitConvAngle);
            size_t npixel = pPixelHit->GetPixelIndexVec().size();
            pSubMCHit->Setnpixel(npixel);
            pSubMCHit->SetpixelIndexSize(npixel);
            pSubMCHit->SetchipidSize(npixel);
            pSubMCHit->SetedepSize(npixel);
            pSubMCHit->SetxpixelSize(npixel);
            pSubMCHit->SetypixelSize(npixel);
            pSubMCHit->SetzpixelSize(npixel);
            for (size_t iPixel = 0; iPixel < npixel; iPixel++) {
               pixelSet.insert(pair<G4int,G4int>(pPixelHit->GetChipIDVec()[iPixel],pPixelHit->GetPixelIndexVec()[iPixel]));
               pSubMCHit->SetpixelIndexAt(iPixel, pPixelHit->GetPixelIndexVec()[iPixel]);
               pSubMCHit->SetchipidAt(iPixel, pPixelHit->GetChipIDVec()[iPixel]);
               pSubMCHit->SetedepAt(iPixel, pPixelHit->GetEdepVec()[iPixel] * kUnitConvEnergy);
               pSubMCHit->SetxpixelAt(iPixel, pPixelHit->GetXYZPixelVec()[iPixel].x() * kUnitConvLength);
               pSubMCHit->SetypixelAt(iPixel, pPixelHit->GetXYZPixelVec()[iPixel].y() * kUnitConvLength);
               pSubMCHit->SetzpixelAt(iPixel, pPixelHit->GetXYZPixelVec()[iPixel].z() * kUnitConvLength);
            }
         }
      }
   }


   //
   // Fill pixel event output
   //
   // in gem pixel event is simulated in simple way
   // no device nor electronics simulation are performed. These will be done in bartender
   map<pair<G4int,G4int>, G4int> pixelToIndex; // 1st: chip-pixel index, 2nd: pixel event index
   map<pair<G4int,G4int>, vector<Int_t> > pixelToTrack;
   map<pair<G4int,G4int>, vector<Float_t> > pixelToE;
   map<pair<G4int,G4int>, vector<Double_t> > pixelToT;
   map<pair<G4int,G4int>, G4int>::iterator pixelToIndexIter;
   pPixelEvents->Delete();// MCSVTPixelEvent has members allocating memory
   if (pPixelHitsCol) {
      TClonesArray &pixels = *pPixelEvents;
      G4int nTotalPixel(0);
      G4int nHit = pPixelHitsCol->entries();
      for (G4int iHit = 0; iHit < nHit; iHit++) {
         pPixelHit = (*pPixelHitsCol)[iHit];
         G4int trackIndex = gemEveInfo->GetTrackInfoIndexFromID(pPixelHit->GetTrackID());
         if (pPixelHit) {            
            size_t npixel = pPixelHit->GetPixelIndexVec().size();
            for (size_t iPixel = 0; iPixel < npixel; iPixel++) {
               G4int chipID = pPixelHit->GetChipIDVec()[iPixel];
               G4int pixelIndex = pPixelHit->GetPixelIndexVec()[iPixel];
               pixelToIndexIter = pixelToIndex.find(pair<G4int,G4int>(chipID, pixelIndex));
               if (pixelToIndexIter == pixelToIndex.end()) { // new pixel
                  pixelToIndex.insert(pair<pair<G4int,G4int>, G4int>
                                      (pair<G4int,G4int>(chipID, pixelIndex), nTotalPixel));
                  pPixelEvent = new(pixels[nTotalPixel++]) MEGMCSVTPixelEvent(); // create a new pixel in array
                  pPixelEvent->SetpixelIndex(pixelIndex);
                  pPixelEvent->Setlayerid(pPixelHit->GetLayerID());
                  pPixelEvent->Setchipid(chipID);
                  pPixelEvent->SetxyzpixelAt(0, pPixelHit->GetXYZPixelVec()[iPixel].x() * kUnitConvLength);
                  pPixelEvent->SetxyzpixelAt(1, pPixelHit->GetXYZPixelVec()[iPixel].y() * kUnitConvLength);
                  pPixelEvent->SetxyzpixelAt(2, pPixelHit->GetXYZPixelVec()[iPixel].z() * kUnitConvLength);
                  pPixelEvent->Setedeptot(pPixelHit->GetEdepVec()[iPixel] * kUnitConvEnergy);
                  pPixelEvent->Setnhit(1);
                  pixelToTrack.insert(pair<pair<G4int,G4int>, vector<Int_t> >
                                      (pair<G4int,G4int>(chipID, pixelIndex), vector<Int_t>(1, trackIndex)));
                  pixelToE.insert(pair<pair<G4int,G4int>, vector<Float_t> >
                                  (pair<G4int,G4int>(chipID, pixelIndex), 
                                   vector<Float_t>(1, pPixelHit->GetEdepVec()[iPixel] * kUnitConvEnergy)));
                  pixelToT.insert(pair<pair<G4int,G4int>, vector<Double_t> >
                                  (pair<G4int,G4int>(chipID, pixelIndex), 
                                   vector<Double_t>(1, (pPixelHit->GetTime() - timeOI) * kUnitConvTime)));

               } else { // hit on same pixel
                  G4int index = (*pixelToIndexIter).second;
                  pPixelEvent = static_cast<MEGMCSVTPixelEvent*>(pPixelEvents->At(index));
                  pPixelEvent->Addnhit(1);
                  pPixelEvent->Addedeptot(pPixelHit->GetEdepVec()[iPixel] * kUnitConvEnergy);
                  pixelToTrack[(*pixelToIndexIter).first].push_back(trackIndex);
                  pixelToE[(*pixelToIndexIter).first].push_back(pPixelHit->GetEdepVec()[iPixel] * kUnitConvEnergy);
                  pixelToT[(*pixelToIndexIter).first].push_back((pPixelHit->GetTime() - timeOI) * kUnitConvTime);
               }
            }
         }
      }
      for (pixelToIndexIter = pixelToIndex.begin(); pixelToIndexIter != pixelToIndex.end(); ++pixelToIndexIter) {
         G4int index = (*pixelToIndexIter).second;
         pPixelEvent = static_cast<MEGMCSVTPixelEvent*>(pPixelEvents->At(index));
         G4double totalEdep = pPixelEvent->Getedeptot(); 
         // simple hit selections
         if (totalEdep > kMinEnergyDeposit)
            pPixelEvent->Setfired(kTRUE);
         else
            pPixelEvent->Setfired(kFALSE);
         Int_t nhit =  pPixelEvent->Getnhit();
         pPixelEvent->SetsevidSize(nhit, kFALSE, kTRUE);
         pPixelEvent->SettrackIndex(pixelToTrack[(*pixelToIndexIter).first]);
         pPixelEvent->Sethitedep(pixelToE[(*pixelToIndexIter).first]);
         pPixelEvent->Sethittime(pixelToT[(*pixelToIndexIter).first]);
      }
   }


   //
   // Fill event output
   //
   pEveMCHits  = pEvent->GetSVTMCHit();
   pEvent->Setsvtnsev(1); // in gem make only one sub-event
   pEvent->SetsvtSevIDSize(1);
   pEvent->SetsvtSevIDAt(0, 0);
   pEvent->Setnpixel(static_cast<G4int>(pixelSet.size()));
   pEveMCHits->Delete(); // SVTMCHit has members allocating memory
   if (pPixelHitsCol) {
      TClonesArray &hits = *pEveMCHits;
      G4int nhit(0);
      G4int nHit = pSubMCHits->GetEntriesFast();
      for (G4int iHit = 0; iHit < nHit; iHit++) {
         pEveMCHit = new(hits[nhit++]) MEGSVTMCHit(); // create a new hit in array
         *pEveMCHit = *(MEGSVTMCHit*)(pSubMCHits->At(iHit));// Copy MCHit in subevent
//          pEveMCHit->SettrackIndex(gemEveInfo->GetTrackInfoIndexFromID(pEveMCHit->Gettrackid()));
      }
   }



   // Draw all hits
   if (pSiHitsCol) {
      G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
      if(pVVisManager) {
         pSiHitsCol->DrawAllHits();
      }
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
