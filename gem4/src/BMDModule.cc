//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "G4VisAttributes.hh"
#include "BMDModule.hh"
#include "MAGConstants.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4UnionSolid.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

namespace
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void BMDModule::ConstructMaterials()
{
   G4Element *elH  = G4NistManager::Instance()->FindOrBuildElement("H");
   G4Element *elC  = G4NistManager::Instance()->FindOrBuildElement("C");
   G4Element *elO  = G4NistManager::Instance()->FindOrBuildElement("O");

   G4double density;
   density = 1.377 * g / cm3;
   mabmumy2 = new G4Material("Mylar for vac. window", density, 3);
   mabmumy2->AddElement(elH, 4);
   mabmumy2->AddElement(elC, 5);
   mabmumy2->AddElement(elO, 2);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void BMDModule::Construct(G4LogicalVolume *parent)
{
   if (!materialConstructed) {
      ConstructMaterials();
      materialConstructed = true;
   }

   G4VisAttributes *bmdVis = new G4VisAttributes(G4VisAttributes::GetInvisible());

   // realistic geometry is not implemented yet
   const G4double zbmd  = ALMAG1 + ALMAG2 + ALMAG3 + ALMAG4 / 2 + kCOBRAVolumeRDCExtension;
   const G4double lbmd  = 190  * micrometer;
   const G4double zcbmd = zbmd + lbmd / 2;
   const G4double rbmd  = 60 * cm;

   G4Tubs *BMDSol = new G4Tubs("BMDSol", 0 * cm, rbmd, lbmd / 2, 0., 360.*deg);
   G4LogicalVolume *BMDLog = new G4LogicalVolume(BMDSol, mabmumy2, "BMDLog", 0, 0, 0);
   BMDLog->SetVisAttributes(bmdVis);
   new G4PVPlacement(0, G4ThreeVector(0., 0., zcbmd), BMDLog, "BMDPhys", parent, false, 0);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

BMDModule::BMDModule(const char* n, G4int i)
   : GEMAbsModule(n)
   , idx(i)
   , materialConstructed(false)
   , mabmumy2(0)
{
   fRunAction      = 0;
   fEventAction    = 0;
   fStackingAction = 0;
   fSteppingAction = 0;
   fTrackingAction = 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

BMDModule::~BMDModule()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserEventInformation* BMDModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserTrackInformation* BMDModule::MakeTrackInformation(const G4Track* /*aTrack*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
