//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4Version.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMAbsUserTrackInformation.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserTrackInformation::GEMUserTrackInformation()
   : modules(0)
   , trackInfo()
{
#if G4VERSION_NUMBER == 951
   // there is a bug in Geant4.9.5.p01 in the copy constructor
   if (!pType) {
      pType = new G4String("NONE");
   }
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserTrackInformation::GEMUserTrackInformation(const GEMUserTrackInformation &info)
   : G4VUserTrackInformation(info)
   , modules(0)
   , trackInfo(info.trackInfo)
{
   std::vector<GEMAbsUserTrackInformation*>::iterator iter;
   std::vector<GEMAbsUserTrackInformation*> tmp = info.modules;
   for (iter = tmp.begin(); iter != tmp.end(); ++iter) {
      if ((*iter)) {
         modules.push_back((*iter)->Clone());
      } else {
         modules.push_back(0);
      }
   }

#if G4VERSION_NUMBER == 951
   // there is a bug in Geant4.9.5.p01 in the copy constructor
   if (!pType) {
      pType = new G4String("NONE");
   }
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserTrackInformation& GEMUserTrackInformation::operator=(const GEMUserTrackInformation &info)
{
   if (this != &info) {
      G4VUserTrackInformation::operator=(info);
      trackInfo = info.trackInfo;
      std::vector<GEMAbsUserTrackInformation*>::iterator iter;
      GEMAbsUserTrackInformation *el;
      std::vector<GEMAbsUserTrackInformation*> tmp = info.modules;
      for (iter = tmp.begin(); iter != tmp.end(); ++iter) {
         if ((*iter) && (el = (*iter)->Clone())) {
            modules.push_back(el);
         } else {
            modules.push_back(0);
         }
      }
   }
   return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUserTrackInformation::~GEMUserTrackInformation()
{
   std::vector<GEMAbsUserTrackInformation*>::iterator iter;
   for (iter = modules.begin(); iter != modules.end(); ++iter) {
      delete *iter;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMAbsUserTrackInformation *GEMUserTrackInformation::GetModule(const char* name)
{
   std::vector<GEMAbsUserTrackInformation*>::iterator iter;
   G4String str = name;
   for (iter = modules.begin(); iter != modules.end(); ++iter) {
      if ((*iter) && name == (*iter)->GetName()) {
         return (*iter);
      }
   }
   return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int GEMUserTrackInformation::GetG3ParticleID(const G4ParticleDefinition* particle)
{
   if (particle == G4OpticalPhoton::OpticalPhotonDefinition()) {
      return 50;
   }
   if (particle == G4Gamma::GammaDefinition()) {
      return 1;
   }
   if (particle == G4Positron::PositronDefinition()) {
      return 2;
   }
   if (particle == G4Electron::ElectronDefinition()) {
      return 3;
   }
   if (particle == G4Alpha::AlphaDefinition()) {
      return 47;
   }
   if (particle == G4MuonPlus::MuonPlusDefinition()) {
      return 5;
   }
   if (particle == G4MuonMinus::MuonMinusDefinition()) {
      return 6;
   }
   if (particle == G4Proton::ProtonDefinition()) {
      return 14;
   }
   if (particle == G4AntiProton::AntiProtonDefinition()) {
      return 15;
   }
   if (particle == G4Neutron::NeutronDefinition()) {
      return 13;
   }
   if (particle == G4Geantino::GeantinoDefinition()) {
      return 48;
   }
   if (particle == G4PionZero::PionZeroDefinition()) {
      return 7;
   }
   if (particle == G4PionMinus::PionMinusDefinition()) {
      return 9;
   }
   if (particle == G4NeutrinoE::NeutrinoEDefinition()) {
      return 4;
   }
   if (particle == G4NeutrinoMu::NeutrinoMuDefinition()) {
      return 4;
   }
   if (particle == G4NeutrinoTau::NeutrinoTauDefinition()) {
      return 4;
   }
   if (particle == G4AntiNeutrinoE::AntiNeutrinoEDefinition()) {
      return 4;
   }
   if (particle == G4AntiNeutrinoMu::AntiNeutrinoMuDefinition()) {
      return 4;
   }
   if (particle == G4AntiNeutrinoTau::AntiNeutrinoTauDefinition()) {
      return 4;
   }


   // Neutrino     4
   // Pion0        7
   // Pion+        8
   // Pion-        9
   // Kaon0long   10
   // Kaon+       11
   // Kaon-       12
   // Kaon0short  16
   // Eta         17
   // Lambda      18
   // Sigma+      19
   // Sigma0      20
   // Sigma-      21
   // Xi0         22
   // Xi-         23
   // Omega       24
   // Antineutron 25
   // Antilambda  26
   // Antisigma-  27
   // Antisigma0  28
   // Antisigma+  29
   // Antixi0     30
   // Antixi+     31
   // Antiomega+  32
   // Deuteron    45
   // Tritium     46
   // He3         49

   return -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrackInfo::GEMTrackInfo()
   : particledef(0)
   , trackid(-1)
   , vertexid(-1)
   , TrackIndex(-1)
   , PrimaryParticleIndex(-1)
   , ParentID(-1)
   , ParentIndex(-1)
   , tekine(0)
   , tracklength(0)
   , trktotphi(0)
   , trackendmec(-1)
   , nstep(0)
   , nturn(1)
   , annihiltrackid(-1)
   , bremstrackid(-1)
   , radx(0)
   , rady(0)
   , radz(0)
   , momx(0)
   , momy(0)
   , momz(0)
   , trackbufx(0)
   , trackbufy(0)
   , trackbufz(0)
   , trackbuft(0)
   , trackbufde(0)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrackInfo::GEMTrackInfo(const GEMTrackInfo &info)
   : particledef(info.particledef)
   , trackid(info.trackid)
   , vertexid(info.vertexid)
   , TrackIndex(info.TrackIndex)
   , PrimaryParticleIndex(info.PrimaryParticleIndex)
   , ParentID(info.ParentID)
   , ParentIndex(info.ParentIndex)
   , tekine(info.tekine)
   , tracklength(info.tracklength)
   , trktotphi(info.trktotphi)
   , trackendmec(info.trackendmec)
   , nstep(info.nstep)
   , nturn(info.nturn)
   , annihiltrackid(info.annihiltrackid)
   , bremstrackid(info.bremstrackid)
   , radx(info.radx)
   , rady(info.rady)
   , radz(info.radz)
   , momx(info.momx)
   , momy(info.momy)
   , momz(info.momz)
   , trackbufx(info.trackbufx)
   , trackbufy(info.trackbufy)
   , trackbufz(info.trackbufz)
   , trackbuft(info.trackbuft)
   , trackbufde(info.trackbufde)
   , hitcyldch(info.hitcyldch)
   , poscyldch(info.poscyldch)
   , momcyldch(info.momcyldch)
{
}

GEMTrackInfo &GEMTrackInfo::operator=(const GEMTrackInfo &info)
{
   particledef          = info.particledef;
   trackid              = info.trackid;
   vertexid             = info.vertexid;
   TrackIndex           = info.TrackIndex;
   PrimaryParticleIndex = info.PrimaryParticleIndex;
   ParentID             = info.ParentID;
   ParentIndex          = info.ParentIndex;
   tekine               = info.tekine;
   tracklength          = info.tracklength;
   trktotphi            = info.trktotphi;
   trackendmec          = info.trackendmec;
   nstep                = info.nstep;
   nturn                = info.nturn;
   annihiltrackid       = info.annihiltrackid;
   bremstrackid         = info.bremstrackid;
   radx                 = info.radx;
   rady                 = info.rady;
   radz                 = info.radz;
   momx                 = info.momx;
   momy                 = info.momy;
   momz                 = info.momz;
   trackbufx            = info.trackbufx;
   trackbufy            = info.trackbufy;
   trackbufz            = info.trackbufz;
   trackbuft            = info.trackbuft;
   trackbufde           = info.trackbufde;
   hitcyldch            = info.hitcyldch;
   poscyldch            = info.poscyldch;
   momcyldch            = info.momcyldch;

   return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
