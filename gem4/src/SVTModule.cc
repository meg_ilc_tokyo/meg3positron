//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "SVTModule.hh"

#include "SVTMessenger.hh"
#include "SVTEventAction.hh"
#include "SVTSiSD.hh"
#include "SVTSiROGeometry.hh"
// #include "SVTUserEventInformation.hh"
// #include "SVTUserTrackInformation.hh"
#include "generated/MEGMCSVTRunHeader.h"

#include "GEMRootIO.hh"
#include "GEMUnitConversion.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4UserLimits.hh"
#include "G4VSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4Para.hh"
#include "G4Trap.hh"
#include "G4AssemblyVolume.hh"
#include "G4Region.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"
#include "G4ProductionCuts.hh"
#include "G4UIdirectory.hh"
#include "GEMConstants.hh"

#include "TMath.h"
#include "TString.h"
#include <vector>

using namespace std;

namespace {
   //
   // Definition of constants to describe the detector
   //

   // Sensor parameters
   constexpr G4double kSensorSize[2]        = {2.0 * cm,   // z dir
                                           2.0 * cm};  // phi dir  (full width)
   constexpr G4double kSensorDeadWidth[4]   = {0.0 * mm,   //  --------- [2]
                                           0.0 * mm,   //  |       |
                                           0.0 * mm,   //  |       |
                                           0.48 * mm}; //  --------- [3]
                                                       //  [0]    [1] 
   constexpr G4double kDefaultSensorThickness = 50. * um;  // thickness of silicon wafer
   constexpr G4double kDefaultDepThickness   = 14. * um;   // thickness of sensitive (depleted) region
   constexpr G4double kSensorSensitiveDepth  = 10. * um;   // depth of the surface of sensitive region
   constexpr G4double kDefaultPixelSize[2]   = {80. * um,  // z dir
                                            80. * um}; // phi dir   (full width)
                                                       // precise pixel size is adjusted later in code
                                                       // to match the sensor sensitive size
   // Support frame sheet
   constexpr G4double kFrameThickness       = 25. * um;
   constexpr G4double kFrameZHalfLength     = 11 * cm;     // half length in z. This also defines the position of end rings

   // Flex print
   constexpr G4double kFlexPrintThickness   = 25. * um;
   constexpr G4double kAlTraceThickness     = 15. * um;
   constexpr G4double kAlTraceCoverage      = 0.5;
   constexpr G4int    kNAlTrace             = 20;         // number of traces in a segment of print

   // End ring
   constexpr G4double kDefaultEndRingThickness = 5. * mm; // thickness in z
   constexpr G4double kDefaultEndRingWidth     = 3. * mm; // width in r

   // Geometrical design
   constexpr G4int    kNLayers                     = 4;          // Number of layers (fixed)
   constexpr G4double kDefaultLayerR[kNLayers]     = {3.194 * cm,
                                                  4.105 * cm,
                                                  6.511 * cm, 
                                                  7.409 * cm};
   constexpr G4int    kDefaultNSensorInZ[kNLayers] = {9,
                                                  10,
                                                  33,
                                                  35
                                                   };
   constexpr G4double kZGap                        = 0.0 * cm;   // gap in z b/w sensors 
   constexpr G4double kDefaultSlantAngle           = -10. * deg; // sensor slant angle in phi from the tangent
   constexpr G4double kDefaultOverlapWidth         = 1.0 * mm;


   // Parameter to calculate necessary phi coverage
   constexpr G4double kMinPhiAcceptance     = 300. * deg;
   constexpr G4double kMaxBeamSpread        = 2. * cm;   // to be covered
//   constexpr G4double   KTargetSlantAngle     = kDefaultTargetAngle[0];

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::Construct(G4LogicalVolume *parent)
{
   // Construct detector

   //
   // Construct volumes and assemble detector
   //
   vector<G4LogicalVolume*> logVec;
   BuildAndAssemble(parent, logVec, false);

   //
   // Make a region and root logical volume of SVT silicon
   //
   if (fSVTRegion)
      delete fSVTRegion;
   fSVTRegion = new G4Region("SVTRegion");
   fWaferLog->SetRegion(fSVTRegion);
   fSVTRegion->AddRootLogicalVolume(fWaferLog);

   // set dedicated production cut
   G4double cutrange = 10 * um;
   G4ProductionCuts * cuts = new G4ProductionCuts;
   cuts->SetProductionCut(cutrange);
//    cuts->SetProductionCut(physics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("gamma"));
//    cuts->SetProductionCut(physics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("e-"));
//    cuts->SetProductionCut(physics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("e+"));
//    cuts->SetProductionCut(physics->GetDefaultCutValue(), G4ProductionCuts::GetIndex("proton"));
   fSVTRegion->SetProductionCuts(cuts);
   // set user limits
   G4double maxStep = 0.5 * fDepThickness;
   if (fStepLimit)
      delete fStepLimit;
   fStepLimit = new G4UserLimits(maxStep);
   fSVTRegion->SetUserLimits(fStepLimit);

   //
   // Sensitive detectors
   //
   G4SDManager *sdMan = G4SDManager::GetSDMpointer();
   static G4VReadOutGeometry *pSVTSiRO(0);
   if (!fSVTSiSD) { // determine if it has already been created
      fSVTSiSD = new SVTSiSD("/svt/SiSD");
      fSVTSiSD->SetModule(this);      
      sdMan->AddNewDetector(fSVTSiSD);
   }
   pSVTSiRO = new SVTSiROGeometry("SVTSiROGeom", this);
   pSVTSiRO->BuildROGeometry();
   fSVTSiSD->SetROgeometry(pSVTSiRO);
   fSensorLog->SetSensitiveDetector(fSVTSiSD);
   fWaferLog->SetSensitiveDetector(fSVTSiSD);
   
   //
   // Set visualization attributes
   //
   SetVisAttributes();

   //
   // Fill run header to store module information
   //
   FillRunHeader();

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::BuildAndAssemble(G4LogicalVolume *parent, vector<G4LogicalVolume*> &logVec,
                                 G4bool readout)
{
   // Construct detector

   if (!fMaterialConstructed) {
      ConstructMaterials();
   }

   CalculatePhiCoverage();

   G4String name, option = "";
   if (readout)
      option = "RO";

   // Logical volumes
   vector<G4LogicalVolume*> sensorArrayLog;
   G4LogicalVolume      *sensorLog(0); 
   G4LogicalVolume      *waferLog(0);
   G4LogicalVolume      *pixelLog(0);
   sensorArrayLog.resize(fNLayers, 0);

   
   //
   // Constract sensor
   //

   // Si wafer
   G4Box *waferSol = new G4Box("SVTWaferSol"+option,
                               fSensorThickness/2., kSensorSize[1]/2., kSensorSize[0]/2.);
   waferLog = new G4LogicalVolume(waferSol, fSilicon, "SVTWaferLog"+option, 0, 0, 0);



   // A pixel (currently used for readout geometry or drawing)
   // Si readout division
   fNPixelInZ = static_cast<G4int>((kSensorSize[0] - kSensorDeadWidth[0]
                                    - kSensorDeadWidth[1]) / fPixelSize[0]);
   fNPixelInY = static_cast<G4int>((kSensorSize[1] - kSensorDeadWidth[2]
                                    - kSensorDeadWidth[3]) / fPixelSize[1]);
   // update pixel size to be adjusted in fixed sensor size
   fPixelSize[0] = (kSensorSize[0] - kSensorDeadWidth[0] - kSensorDeadWidth[1]) / fNPixelInZ;
   fPixelSize[1] = (kSensorSize[1] - kSensorDeadWidth[2] - kSensorDeadWidth[3]) / fNPixelInY;
   
   G4Box *pixelSol = new G4Box("SVTPixelSol"+option, fSensorThickness/2.,
                               fPixelSize[1]/2., fPixelSize[0]/2.);
   pixelLog = new G4LogicalVolume(pixelSol, fSilicon,
                                  "SVTPixelLog"+option, 0, 0, 0);
   if (!readout) {
      // Sensitive region
      G4Box *sensorSol = new G4Box("SVTSensorSol"+option, fDepThickness/2.,
                                   (kSensorSize[1] - kSensorDeadWidth[2] - kSensorDeadWidth[3])/2.,
                                   (kSensorSize[0] - kSensorDeadWidth[0] - kSensorDeadWidth[1])/2.);
      sensorLog = new G4LogicalVolume(sensorSol, fSilicon, "SVTSensorLog"+option, 0, 0, 0);
      G4ThreeVector sensorCenter(-fSensorThickness/2. 
                                 + kSensorSensitiveDepth + fDepThickness/2.,
                                 (kSensorDeadWidth[3] - kSensorDeadWidth[2])/2.,
                                 (kSensorDeadWidth[0] - kSensorDeadWidth[1])/2.);
      new G4PVPlacement(0,                               // no rotation
                        sensorCenter,                    // its position
                        sensorLog,                       // its logical volume
                        "SVTSensor"+option,              // its name
                        waferLog,                        // its mother
                        false,                           // no boolean operation
                        0,                               // copy number
                        fCheckVolumeOverlap);            // check overlap
   } else {
      // Sensitive region (thickness is different for readout geometry)
      G4Box *sensorSol = new G4Box("SVTSensorSol"+option, fSensorThickness/2.,
                                   (kSensorSize[1] - kSensorDeadWidth[2] - kSensorDeadWidth[3])/2.,
                                   (kSensorSize[0] - kSensorDeadWidth[0] - kSensorDeadWidth[1])/2.);
      sensorLog = new G4LogicalVolume(sensorSol, fSilicon, "SVTSensorLog"+option, 0, 0, 0);
      G4ThreeVector sensorCenter(0,
                                 (kSensorDeadWidth[3] - kSensorDeadWidth[2])/2.,
                                 (kSensorDeadWidth[0] - kSensorDeadWidth[1])/2.);
      new G4PVPlacement(0,                               // no rotation
                        sensorCenter,                    // its position
                        sensorLog,                       // its logical volume
                        "SVTSensor"+option,              // its name
                        waferLog,                        // its mother
                        false,                           // no boolean operation
                        0,                               // copy number
                        fCheckVolumeOverlap);            // check overlap
      
      G4Box *pixelArraySol = new G4Box("SVTPixelArraySol"+option, fSensorThickness/2.,
                                       fPixelSize[1]/2., (fPixelSize[0] * fNPixelInZ)/2.);
      G4LogicalVolume *pixelArrayLog = new G4LogicalVolume(pixelArraySol, fSilicon,
                                                           "SVTPixelArrayLog"+option, 0, 0, 0);
      new G4PVReplica("SVTPixelArray"+option,
                      pixelArrayLog,              // its logical volume
                      sensorLog,                  // its mother
                      kYAxis,                     // axis of replication
                      fNPixelInY,                 // number of replica
                      fPixelSize[1]);             // width of replica

      new G4PVReplica("SVTPixel"+option,
                      pixelLog,                   // its logical volume
                      pixelArrayLog,              // its mother
                      kZAxis,                     // axis of replication
                      fNPixelInZ,                 // number of replica
                      fPixelSize[0]);             // width of replica

      logVec.push_back(pixelLog);

      // // insensitive region for readout space
      // for (G4int iSide = 0; iSide < 4; iSide++) {
      //    if (kSensorDeadWidth[iSide] > 0.) {
      //       G4double length[2], center[2];
      //       if (iSide <= 1) {
      //          length[1] = kSensorSize[1] - kSensorDeadWidth[2] - kSensorDeadWidth[3];// y
      //          length[0] = kSensorDeadWidth[iSide];// z
      //          center[1] = (kSensorDeadWidth[3] - kSensorDeadWidth[2])/2.;// y
      //          center[0] = -kSensorSize[0]/2. + kSensorDeadWidth[iSide]/2.;//z
      //          if (iSide == 1) center[0] *= -1;
      //       } else {
      //          length[1] = kSensorDeadWidth[iSide];// y
      //          length[0] = kSensorSize[0];// z
      //          center[1] = kSensorSize[1]/2. - kSensorDeadWidth[iSide]/2.;//y
      //          if (iSide == 3) center[1] *= -1;
      //          center[0] = 0;
      //       }
      //       G4Box *sideSol = new G4Box(Form("SVTSideSol%s%d", option.c_str(), iSide),
      //                                  fSensorThickness/2., length[1]/2., length[0]/2.);
      //       G4LogicalVolume *sideLog = new G4LogicalVolume(sideSol, fSilicon,
      //                                                      Form("SVTSideLog%s%d", option.c_str(), iSide),
      //                                                      0, 0, 0);

      //       new G4PVPlacement(0,                               // no rotation
      //                         G4ThreeVector(0, center[1], center[0]),// its position
      //                         sideLog,                         // its logical volume
      //                         Form("SVTSideLog%s%d", option.c_str(), iSide),// its name
      //                         waferLog,                        // its mother
      //                         false,                           // no boolean operation
      //                         0,                               // copy number
      //                         fCheckVolumeOverlap);            // check overlap
      //       logVec.push_back(sideLog);
      //    }
      // }
   }

   
   vector<G4AssemblyVolume*> endRingAssembly;
   vector<G4double> frameZHalfLengths(fNLayers);  // half length in z. This also defines the position of end rings
   if (!readout) {
      for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
         frameZHalfLengths[iLayer] =
               (kSensorSize[0] * fNSensorInZ[iLayer]  // sensor array length
                + 4 * centimeter) // margin
               / 2.;
      }

      //
      // Construct endrings
      //
      fEndRingLogs.resize(fNLayers);
      // fEndRingLogs.resize(fNLayers * 2);
      endRingAssembly.resize(fNLayers);
      for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
         G4Tubs *ringSegSol = new G4Tubs(Form("SVTRingSegTub%d", iLayer), 
                                         fEndRingRMin[iLayer], // r_min
                                         fEndRingRMax[iLayer], // r_max
                                         fEndRingThickness / 2., // thickness
                                         0., // start phi
                                         fPhiPerSensor[iLayer]); // dPhi
         G4Para *grooveSol = new G4Para(Form("SVTRingGrooveSol%d", iLayer),
                                        (fEndRingGrooveDepth[iLayer]+50*um) / 2., // half dx
                                        (fEndRingGrooveHeight[iLayer]+1*mm) / 2., // half dy
                                        fEndRingThickness / 2. * 1.1, // half dz
                                        (halfpi - fEndRingGrooveAngle[iLayer]),
                                        0, 0);
         G4double xpos = fEndRingRMax[iLayer] - fEndRingGrooveDepth[iLayer] / 2.
               + fEndRingGrooveHeight[iLayer] / 2. / tan(fEndRingGrooveAngle[iLayer]);
         G4double ypos = fEndRingGrooveHeight[iLayer] / 2.;
         G4VSolid *subRingSeg = new G4SubtractionSolid(Form("SVTRingSegSub%d", iLayer),
                                                       ringSegSol, grooveSol,
                                                       0, // rotation of B
                                                       G4ThreeVector(xpos, ypos, 0.)); // trans of B
         fEndRingLogs[iLayer] = new G4LogicalVolume(subRingSeg, fEndRingMat, "SVTRingSeg", 0, 0, 0);

         endRingAssembly[iLayer] = new G4AssemblyVolume();
            
         G4double firstPhi = (fNSensorInPhi[iLayer] / 2. - 0.5) * fPhiPerSensor[iLayer];
         firstPhi = firstPhi - 0.5 * fPhiPerSensor[iLayer] + fPhiShift[iLayer];
         firstPhi += fPhiCenter[iLayer];
         for (G4int iSeg = 0; iSeg < fNSensorInPhi[iLayer]; iSeg++) {

            G4ThreeVector pos(0, 0., 0.); // position of this segment
            G4double phi = firstPhi - iSeg * fPhiPerSensor[iLayer]; // in negative phi direction
            G4RotationMatrix rot;
            rot.rotateZ(phi);
            G4Transform3D trans(rot, pos);
            endRingAssembly[iLayer]->AddPlacedVolume(fEndRingLogs[iLayer],
                                                      trans);
         }

//          // no sensor part
//          G4VSolid *ringArchSol = new G4Tubs(Form("SVTRingArch%d", iLayer), 
//                                             fEndRingRMin[iLayer], // r_min
// //                                             fEndRingRMax[iLayer] - fEndRingGrooveDepth[iLayer], // r_max
//                                             fEndRingRMax[iLayer], // r_max
//                                             fEndRingThickness / 2., // thickness
//                                             firstPhi + fPhiPerSensor[iLayer], // start phi
//                                             twopi - fPhiPerSensor[iLayer] * fNSensorInPhi[iLayer]); // dPhi
//          fEndRingLogs[fNLayers + iLayer] = new G4LogicalVolume(ringArchSol, fEndRingMat, Form("SVTRingArch%d", iLayer), 0, 0, 0);
//          G4ThreeVector pos(0, 0., 0.);   
//          endRingAssembly[iLayer]->AddPlacedVolume(fEndRingLogs[fNLayers + iLayer], pos, 0);
      }
   }

   // 
   // Assemble
   //
   fSensorLayer.clear();
   fSensorPos.clear();
   fSensorDir.clear();
   G4ThreeVector globalOffset;
   G4RotationMatrix globalRot;
   G4int segmentOffset(0);
   vector<G4AssemblyVolume*> ladderAssembly(fNLayers);
   G4int sensorCopyNumOffset(0);
   for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
      G4Box *sensorArraySol = new G4Box(Form("SVTSensorArraySol%d%s", iLayer, option.c_str()),
                                        fSensorThickness/2., kSensorSize[1]/2.,
                                        kSensorSize[0] * fNSensorInZ[iLayer]/2.);
      sensorArrayLog[iLayer] = new G4LogicalVolume(sensorArraySol, fSilicon,
                                                   Form("SVTSensorArrayLog%d%s", iLayer, option.c_str()),
                                                   0, 0, 0);
      // Place sensor chips (wafer) in a segment into sensorArray volume
      new G4PVReplica("SVTWafer"+option,
                      waferLog,                          // its logical volume
                      sensorArrayLog[iLayer],            // its mother
                      kZAxis,                            // axis of replication
                      fNSensorInZ[iLayer],               // number of replica
                      kSensorSize[0]);                   // witdh of replica
      vector<G4ThreeVector> posInArray(fNSensorInZ[iLayer], G4ThreeVector());
      posInArray[0].setZ(-sensorArraySol->GetZHalfLength() + kSensorSize[0]/2.);
      for (G4int iChip = 1; iChip < fNSensorInZ[iLayer]; iChip++) {
         posInArray[iChip].setZ(posInArray[iChip-1].z() + kSensorSize[0]);
      }

      G4LogicalVolume      *frameLog(nullptr);    
      G4LogicalVolume      *flexMotherLog(nullptr);
      G4LogicalVolume      *flexLog(nullptr);
      G4LogicalVolume      *traceLog(nullptr);
      if (!readout) {
    
         //
         // Constract frame
         //

         // Kapton frame
         if (kFrameThickness > 0.) {
            G4double frameLengthY = kSensorSize[1];
            G4Box *frameSol = new G4Box("SVTFrameSol", kFrameThickness/2.,
                                        frameLengthY/2., frameZHalfLengths[iLayer]);
            frameLog = new G4LogicalVolume(frameSol, fKapton, "SVTFrameLog", 0, 0, 0);
            fFrameLogs.push_back(frameLog);
         }
      
         //
         // Constract flex print
         //
      
         if (kFlexPrintThickness > 0.) {
            // Flex print dummy (mother) volume
            G4double flexMotherLengthY = kSensorSize[1];
            G4Box *flexMotherSol = new G4Box("SVTFlexMotherSol", kFlexPrintThickness/2., 
                                             flexMotherLengthY/2., frameZHalfLengths[iLayer]);
            flexMotherLog = new G4LogicalVolume(flexMotherSol, fKapton, "SVTFlexMotherLog", 0, 0, 0);
            fFlexMotherLogs.push_back(flexMotherLog);
         
            // Main volume of flex print of Kapton and Al trace will be inserted
            G4double flexLengthY = kSensorSize[1] / kNAlTrace;
            G4Box *flexSol = new G4Box("SVTFlexSol", kFlexPrintThickness/2.,
                                       flexLengthY/2., frameZHalfLengths[iLayer]);
            flexLog = new G4LogicalVolume(flexSol, fKapton, "SVTFlexLog", 0, 0, 0);
            fFlexLogs.push_back(flexLog);
            
            // Aluminum trace in flex print
            if (kAlTraceThickness > 0.) {
               G4double traceWidth = kSensorSize[1] * kAlTraceCoverage / kNAlTrace;
               G4Box *traceSol = new G4Box("SVTTraceSol", kAlTraceThickness/2., 
                                           traceWidth/2., frameZHalfLengths[iLayer]);
               traceLog = new G4LogicalVolume(traceSol, fAluminum, "SVTTraceLog", 0, 0, 0);
               fTraceLogs.push_back(traceLog);
               
               new G4PVPlacement(0,                           // no rotation
                                 G4ThreeVector(0,0,0),        // its position
                                 traceLog,                    // its logical volume
                                 "SVTTrace",                  // its name
                                 flexLog,                     // its mother
                                 false,                       // no boolean operation
                                 0,                           // copy number
                                 fCheckVolumeOverlap);        // check overlap
            }
            new G4PVReplica("SVTFlex",
                            flexLog,                          // its logical volume
                            flexMotherLog,                    // its mother
                            kYAxis,                           // axis of replication
                            kNAlTrace,                        // number of replica
                            flexLengthY);                     // width of replica
         
         }
      }

      
      //
      // ladder assemble
      //
      ladderAssembly[iLayer] = new G4AssemblyVolume();
      G4ThreeVector pos(0, 0., 0.); // position of this segment
      G4RotationMatrix rot;

      pos.set(kFrameThickness + kFlexPrintThickness + fSensorThickness/2., 0., 0.);
      G4Transform3D transSensor(rot, pos);
      ladderAssembly[iLayer]->AddPlacedVolume(sensorArrayLog[iLayer], transSensor); // add sensor first to get  correct copy number
      sensorCopyNumOffset = 1;
      if (frameLog) {
         pos.set(kFrameThickness/2., 0., 0.);
         G4Transform3D transFrame(rot, pos);
         ladderAssembly[iLayer]->AddPlacedVolume(frameLog, transFrame);
      }
      if (flexMotherLog) {
         pos.set(kFrameThickness + kFlexPrintThickness/2., 0., 0.);
         G4Transform3D transFlex(rot, pos);
         ladderAssembly[iLayer]->AddPlacedVolume(flexMotherLog, transFlex);
      }


      G4double firstPhi = (fNSensorInPhi[iLayer] / 2. - 0.5) * fPhiPerSensor[iLayer];
      firstPhi += fPhiCenter[iLayer];

      if (iLayer > 0) {
         segmentOffset += fNSensorInPhi[iLayer - 1];
      }
      for (G4int iSeg = 0; iSeg < fNSensorInPhi[iLayer]; iSeg++) {
         // Ordering of segment is from positive phi to negative phi segments
         
         // Place a ladder
         G4ThreeVector posSeg(fLayerR[iLayer], 0., 0.);// position of this segment, start at r
         G4double phi = firstPhi - iSeg * fPhiPerSensor[iLayer]; // in negative phi direction
         posSeg.rotateZ(phi);

         G4RotationMatrix rotSeg;
         rotSeg.rotateZ(phi + fSlantAngle);             // rotate angle of sensor

         G4ThreeVector dir(0., 1., 0.);              // direction parallel to sensor plane
         dir.rotateZ(phi + fSlantAngle);

         posSeg += (fOverlapWidth * 0.5 + 250*um) * dir; // slide
         
         G4Transform3D transform(rotSeg, posSeg);
         ladderAssembly[iLayer]->MakeImprint(parent,      // mother
                                             transform,   // rotation and translation
                                             iSeg + segmentOffset - sensorCopyNumOffset, // copy number base
                                             fCheckVolumeOverlap); // check overlap


         // Place physical volume of frame
         posSeg.set(fLayerR[iLayer] + kFrameThickness/2., 0., 0.); // position of this segment, start at r
         posSeg.rotateZ(phi);                           // rotate center position to phi

         posSeg += (fOverlapWidth * 0.5 + 500*um) * dir;         // translation (slide)
         if (frameLog) {
            new G4PVPlacement(G4Transform3D(rotSeg, posSeg),      // rotation and translation
                              frameLog,                           // its logical volume
                              "SVTFrame",                         // its name
                              parent,                             // its mother
                              false,                              // no boolean operation
                              iSeg + segmentOffset,               // copy number
                              fCheckVolumeOverlap);               // check overlap
         }

         // Place physical volume of flex print
         if (flexMotherLog) {
            posSeg.set(fLayerR[iLayer] + kFrameThickness + kFlexPrintThickness/2., 0., 0.);
            posSeg.rotateZ(phi);
            posSeg += (fOverlapWidth * 0.5 + 500*um) * dir;         // translation (slide)
            new G4PVPlacement(G4Transform3D(rotSeg, posSeg),      // rotation and translation
                              flexMotherLog,                      // its logical volume
                              "SVTFlex",                          // its name
                              parent,                             // its mother
                              false,                              // no boolean operation
                              iSeg + segmentOffset,               // copy number
                              fCheckVolumeOverlap);               // check overlap
         }

         // Place physical volume of sensors
         posSeg.set(fLayerR[iLayer] + kFrameThickness + kFlexPrintThickness + fSensorThickness/2., 0., 0.);
         posSeg.rotateZ(phi);
         posSeg += (fOverlapWidth * 0.5 + 50*um) * dir;         // translation (slide)
         new G4PVPlacement(G4Transform3D(rotSeg, posSeg),         // rotation and translation
                           sensorArrayLog[iLayer],                // its logical volume
                           "SVTSensorArray",                      // its name
                           parent,                                // its mother
                           false,                                 // no boolean operation
                           iSeg + segmentOffset,                  // copy number
                           fCheckVolumeOverlap);                  // check overlap



         // Global position of sensors
         for (G4int iChip = 0; iChip < fNSensorInZ[iLayer]; iChip++) {
            fSensorLayer.push_back(iLayer);
            fSensorPos.push_back(posInArray[iChip] + posSeg + globalOffset);

            G4ThreeVector norm(1.0, 0.0, 0.0);
            norm.transform(rotSeg);
            norm.transform(globalRot);
            fSensorDir.push_back(norm);
            //G4cout<<iChip<<" "<<fSensorPos.back()<<" "<<fSensorDir.back()<<" "<<norm.phi()/deg<<G4endl;
         }
         

         //G4cout<<"layer "<<iLayer<<" row"<<iSeg<<" phi="<<phi/deg<<"deg "<<" rotphi="<<rot.phiX()/deg<<" "<<pos<<endl;
      }

      // Place end rings at both ends
      if (!endRingAssembly.empty()) {
         G4ThreeVector trans;
         // US side
         trans.set(0, 0, -(frameZHalfLengths[iLayer] - fEndRingThickness / 2.));
         endRingAssembly[iLayer]->MakeImprint(parent,  // mother
                                              trans,   // position
                                              0,       // no rotation
                                              0);      // copy number
         // DS side
         trans.set(0, 0, (frameZHalfLengths[iLayer] - fEndRingThickness / 2.));
         endRingAssembly[iLayer]->MakeImprint(parent,  // mother
                                              trans,   // position
                                              0,       // no rotation
                                              1);      // copy number
      }
   } 
   if (!readout) {
      fSensorLog = sensorLog;
      fWaferLog = waferLog;
      fPixelLog = pixelLog;
      fSensorArrayLog.resize(fNLayers, 0);
      for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
         fSensorArrayLog[iLayer] = sensorArrayLog[iLayer];
      }
   }
}



//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::CalculatePhiCoverage()
{
   // Calculate necessary phi range to cover phi acceptance for signal e+

   if (fGeometryCalculated) return;
   fGeometryCalculated = true;

   const G4double kSignalPositronRadius = 13 * cm; //roughly

   G4cout<<"___ Calculating SVT geometry ___"<<G4endl;

   fNSensors = 0;
   fPhiCoverage.resize(fNLayers);
   fPhiCenter.resize(fNLayers);
   fPhiShift.resize(fNLayers);
   fNSensorInPhi.resize(fNLayers);
   fPhiPerSensor.resize(fNLayers);
   fEndRingRMin.resize(fNLayers);
   fEndRingRMax.resize(fNLayers);
   fEndRingGrooveAngle.resize(fNLayers);
   fEndRingGrooveDepth.resize(fNLayers);
   fEndRingGrooveHeight.resize(fNLayers);
   for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
      // phi coverage
      // G4double dPhi = TMath::ASin(kMaxBeamSpread / fLayerR[iLayer]
      //                             * TMath::Sin(kMinPhiAcceptance)) * radian;
      // fPhiCoverage[iLayer] = kMinPhiAcceptance + dPhi;
      fPhiCoverage[iLayer] = 180 * deg;
      G4cout<<"Layer"<<iLayer<<": necessary phi coverage is "<<fPhiCoverage[iLayer]/deg<<"deg"<<G4endl;

      G4double prevDR = 1 * cm;
      while (1) {
      
         // phi angle per sensor
         G4double d = (kSensorSize[1] - fOverlapWidth) * 0.5;
         G4double a = fLayerR[iLayer] * fLayerR[iLayer] + d * d
               - 2 * fLayerR[iLayer] * d * TMath::Cos(90*deg - fSlantAngle); // low of cosines
         a = TMath::Sqrt(a);
         G4double b = fLayerR[iLayer] * fLayerR[iLayer] + d * d
               - 2 * fLayerR[iLayer] * d * TMath::Cos(90*deg + fSlantAngle); 
         b = TMath::Sqrt(b);
         G4double alpha = TMath::ASin(d * TMath::Sin(90*deg - fSlantAngle) / a) * radian; // low of sines
         G4double beta  = TMath::ASin(d * TMath::Sin(90*deg + fSlantAngle) / b) * radian;
         
//       if (beta < -fSlantAngle) {
//          // take the thickness into account
//          G4double t = fSensorThickness + kFrameThickness + kFlexPrintThickness;
//          G4double c = b * b + t * t - 2 * b * t * TMath::Cos(180*deg + fSlantAngle + beta);
//          c = TMath::Sqrt(c);
//          G4double delta = TMath::ASin(t / c * TMath::Sin(180*deg + fSlantAngle + beta));
//          beta += delta;
//       }
         fPhiPerSensor[iLayer] = alpha + beta;
         fPhiShift[iLayer] = (alpha - beta) / 2.;
         if (prevDR == 1 * cm) {
            // Fix the number of ladders with the initial calculation
            fNSensorInPhi[iLayer] = static_cast<G4int>(fPhiCoverage[iLayer]*2 / fPhiPerSensor[iLayer] + 0.5);
         }
         fPhiCoverage[iLayer] = fPhiPerSensor[iLayer] * fNSensorInPhi[iLayer] / 2;

         // phi shift (rotation)
         G4double deltaPhi = TMath::ASin(fLayerR[iLayer]
                                         / (2 * kSignalPositronRadius)) * radian;
         fPhiCenter[iLayer] = -deltaPhi;


         // Calculate parameters of end ring
         fEndRingRMin[iLayer] = fLayerR[iLayer] - fEndRingWidth;
         fEndRingRMax[iLayer] = a;
         fEndRingGrooveDepth[iLayer] = (a - b);
         fEndRingGrooveAngle[iLayer] = beta + (90*deg + fSlantAngle);
         fEndRingGrooveHeight[iLayer] = 2*d * sin(fEndRingGrooveAngle[iLayer]);

         if (fPhiCoverage[iLayer] < 180 * deg) {
            if (prevDR == 0.1 * mm || prevDR == 0.01 * mm || prevDR == -0.01 * mm) {               
               fLayerR[iLayer] -= 0.01 * mm;
               prevDR = -0.01*mm;
            } else {
               fLayerR[iLayer] -= 0.1 * mm;
               prevDR = -0.1*mm;
            }
         } else {
            if (prevDR == -0.01 * mm) {
               break;
            } else if (prevDR == -0.1 * mm || prevDR == 0.01 * mm) {               
               fLayerR[iLayer] += 0.01 * mm;
               prevDR = 0.01*mm;
            } else {
               fLayerR[iLayer] += 0.1 * mm;
               prevDR = 0.1*mm;
            }
         }
      }
      G4cout<<"Layer"<<iLayer<<": R="<<fLayerR[iLayer]/cm<<" cm,  +-"<<fPhiCoverage[iLayer]/deg
            <<"deg is covered by "
            << fNSensorInPhi[iLayer]<<" sensers"<<G4endl;
      G4cout<<"Layer"<<iLayer<<": phi center is "<<fPhiCenter[iLayer]/deg<<"deg"<<G4endl;
      
      fNSensors += fNSensorInPhi[iLayer] * fNSensorInZ[iLayer];
   }
   
   G4cout<<"Total number of sensors is "<<fNSensors<<" (";
   for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
      G4cout<<fNSensorInPhi[iLayer] * fNSensorInZ[iLayer]<<", ";
   }
   G4cout<<")"<<G4endl;
   G4cout<<"_________________________"<<G4endl<<G4endl;

}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::ConstructMaterials()
{
   // Prepare materials used in this detector

   if (fMaterialConstructed)
      return;

//    G4double z;        // atomic number
//    G4double a;        // atomic mass
   G4double density;
   G4int    nel;      // number of elements
   G4int    natoms;   // number of atoms in molcule
   G4double fracmass; // fraction of mass

   // Elements 
   G4Element *elH  = G4NistManager::Instance()->FindOrBuildElement("H");
   G4Element *elC  = G4NistManager::Instance()->FindOrBuildElement("C");
   G4Element *elN  = G4NistManager::Instance()->FindOrBuildElement("N");
   G4Element *elO  = G4NistManager::Instance()->FindOrBuildElement("O");

   // Silicon for sensors
   fSilicon = G4NistManager::Instance()->FindOrBuildMaterial("G4_Si");
//    fSilicon = new G4Material("Silicon", z=14., a=28.0855*g/mole, density=2.329*g/cm3);

   // Aluminum for trace in flex print
   fAluminum = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");
//    fAluminum = new G4Material("Aluminum", z=13., a=26.98*g/mole, density=2.7*g/cm3);

   // Kapton (C22H10N2O5)_n for frame and flex print
   fKapton = new G4Material("Kapton", density=1.42*g/cm3, nel=4);
   fKapton->AddElement(elH, natoms=10);
   fKapton->AddElement(elC, natoms=22);
   fKapton->AddElement(elN, natoms=2);
   fKapton->AddElement(elO, natoms=5);


   // Material for end ring
   // temporary option carbon fiber
   // copied from DCH frame
   // Composition of Carbon fibre for cathode hood
   // --- see description (note-3) below   
   G4Material *carbonFibre = new G4Material("CarbonFibre", density=1.56*g/cm3, nel=3);
   carbonFibre->AddElement(elH, fracmass=.0315);
   carbonFibre->AddElement(elC, fracmass=.8965);
   carbonFibre->AddElement(elO, fracmass=.072);
   fEndRingMat = carbonFibre;


//    G4cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<G4endl;
//    G4cout << *(G4Material::GetMaterialTable()) << G4endl; 

   fMaterialConstructed = true;
   return;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::SetVisAttributes()
{
   // Visualization attributes 
   // RGB setting examples
   // black      (0, 0, 0)
   // gray       (0.x, 0.x, 0.x)
   // white      (1.0, 1.0, 1.0)
   // magenta    (1.0, 0.0, 1.0)
   // cyan       (0.0, 1.0, 1.0)
   // yellow     (1.0, 1.0, 0.0)
   // light blue (0.0, 0.5, 1.0)
   // orange     (1.0, 0.5, 0.0)
   // brown      (0.5, 0.25,0.0)
   // purple     (0.5, 0.0, 1.0)
   // pink       (1.0, 0.5, 0.5)


   if (!fFrameLogs.empty()) {
      G4VisAttributes *frameVis = new G4VisAttributes(false, G4Colour(0.5, 0.25, 0.0));
      frameVis->SetForceWireframe(true);
      for (auto &&log : fFrameLogs) {
         log->SetVisAttributes(frameVis);
      }
   }
   if (!fFlexLogs.empty()) {
      G4VisAttributes *flexVis = new G4VisAttributes(true, G4Colour(0.0, 0.3, 0.0));
      flexVis->SetForceWireframe(true);
      for (auto &&log : fFlexLogs) {      
         log->SetVisAttributes(flexVis);
      }
   }
   if (!fTraceLogs.empty()) {
      G4VisAttributes *traceVis = new G4VisAttributes(false, G4Colour(0.4, 0.4, 0.4));
      traceVis->SetForceWireframe(true);
      for (auto &&log : fTraceLogs) {      
         log->SetVisAttributes(traceVis);
      }
   }
   if (!fFlexMotherLogs.empty()) {
      G4VisAttributes *flexMotherVis = new G4VisAttributes(false, G4Colour(0.5, 0.25, 0.0));
      flexMotherVis->SetForceWireframe(true);
      for (auto &&log : fFlexMotherLogs) {      
         log->SetVisAttributes(flexMotherVis);
      }
   }
   if (!fEndRingLogs.empty()) {
//       G4VisAttributes *endRingVis = new G4VisAttributes(true, G4Color(0.5, 0.3, 0.2));
      G4VisAttributes *endRingVis = new G4VisAttributes(true, G4Color(0.5, 0.5, 0.5));
      for (auto &&log : fEndRingLogs) {
         log->SetVisAttributes(endRingVis);
      }
   }

   if (fWaferLog) {
      G4VisAttributes *waferVis = new G4VisAttributes(true, G4Colour(0.5, 0.5, 0.5));
      waferVis->SetForceWireframe(true);
      fWaferLog->SetVisAttributes(waferVis);
   }
   if (fSensorLog) {
      G4VisAttributes *sensorVis = new G4VisAttributes(true, G4Colour(0.0, 0.0, 1.0));
      //sensorVis->SetForceWireframe(true);
      fSensorLog->SetVisAttributes(sensorVis);
   }
   G4VisAttributes *sensorArrayVis = new G4VisAttributes(false, G4Colour(0.5, 0.5, 0.5));
   sensorArrayVis->SetForceWireframe(true);
   for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
      fSensorArrayLog[iLayer]->SetVisAttributes(sensorArrayVis);
   }
   if (fPixelLog) {
      G4VisAttributes *pixelVis = new G4VisAttributes(false, G4Colour(1.0, 0.0, 0.0));
      pixelVis->SetForceWireframe(true);
      fPixelLog->SetVisAttributes(pixelVis);
   }      

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::SetVerboseLevel(G4int level)
{
   fVerbose = level;
   if (fEventAction) {
      static_cast<SVTEventAction*>(fEventAction)->SetEventVerbose(level);
   }
   if (fSVTSiSD)
      fSVTSiSD->SetVerboseLevel(level);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTModule::FillRunHeader()
{
   // convert units to those in MEG system

   MEGMCSVTRunHeader *pRunHeader = GEMRootIO::GetInstance()->GetMCSVTRunHeader();
   pRunHeader->SetNLayers(fNLayers);
   pRunHeader->SetNSensors(fNSensors);
   pRunHeader->SetNSensorsInZSize(fNLayers);
   pRunHeader->SetNSensorsInPhiSize(fNLayers);
   pRunHeader->SetLayerRSize(fNLayers);
   for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
      pRunHeader->SetNSensorsInZAt(iLayer, fNSensorInZ[iLayer]);
      pRunHeader->SetNSensorsInPhiAt(iLayer, fNSensorInPhi[iLayer]);
      pRunHeader->SetLayerRAt(iLayer, fLayerR[iLayer] * kUnitConvLength);
   }
   pRunHeader->SetSlantAngle(fSlantAngle * kUnitConvAngle);
   pRunHeader->SetOverlapWidth(fOverlapWidth * kUnitConvLength);
   pRunHeader->SetSensorSizeAt(0, fSensorThickness * kUnitConvLength);
   pRunHeader->SetSensorSizeAt(1, kSensorSize[1] * kUnitConvLength);
   pRunHeader->SetSensorSizeAt(2, kSensorSize[0] * kUnitConvLength);
   pRunHeader->SetNPixelsAt(0, fNPixelInZ);
   pRunHeader->SetNPixelsAt(1, fNPixelInY);
   pRunHeader->SetPixelSizeAt(0, fPixelSize[0] * kUnitConvLength);
   pRunHeader->SetPixelSizeAt(1, fPixelSize[1] * kUnitConvLength);
   pRunHeader->SetSensorLayerSize(fNSensors);
   pRunHeader->SetSensorXSize(fNSensors);
   pRunHeader->SetSensorYSize(fNSensors);
   pRunHeader->SetSensorZSize(fNSensors);
   pRunHeader->SetSensorDirXSize(fNSensors);
   pRunHeader->SetSensorDirYSize(fNSensors);
   pRunHeader->SetSensorDirZSize(fNSensors);
   for (G4int iChip = 0; iChip < fNSensors; iChip++) {
      pRunHeader->SetSensorLayerAt(iChip, fSensorLayer[iChip]);
      pRunHeader->SetSensorXAt(iChip, fSensorPos[iChip].x() * kUnitConvLength);
      pRunHeader->SetSensorYAt(iChip, fSensorPos[iChip].y() * kUnitConvLength);
      pRunHeader->SetSensorZAt(iChip, fSensorPos[iChip].z() * kUnitConvLength);
      pRunHeader->SetSensorDirXAt(iChip, fSensorDir[iChip].x());
      pRunHeader->SetSensorDirYAt(iChip, fSensorDir[iChip].y());
      pRunHeader->SetSensorDirZAt(iChip, fSensorDir[iChip].z());
      //G4cout<<iChip<<" "<<fSensorPos[iChip]<<", "<<fSensorDir[iChip]<<""<<G4endl;
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTModule::SVTModule(const char* n, GEMPhysicsList *p, G4int i)
:GEMAbsModule(n)
,idx(i)
,fPhysics(p)
,fDir(0)
,fMaterialConstructed(false)
,fGeometryCalculated(false)
,fCheckVolumeOverlap(false)
,fSilicon(0)
,fKapton(0)
,fAluminum(0)
,fSensorLog(0)
,fWaferLog(0)
,fPixelLog(0)
,fSVTSiSD(0)
,fSVTRegion(0)
,fStepLimit(0)
,fNLayers(kNLayers)
,fNSensors(0)
,fSlantAngle(kDefaultSlantAngle)
,fOverlapWidth(kDefaultOverlapWidth)
,fSensorThickness(kDefaultSensorThickness)
,fDepThickness(kDefaultDepThickness)
,fEndRingThickness(kDefaultEndRingThickness)
,fEndRingWidth(kDefaultEndRingWidth)
,fVerbose(0)
{
   fRunAction      = 0;
   fEventAction    = new SVTEventAction();
   // stackingAction, steppingAction, trackingAction are
   // not defined for SVT

   fDir = new G4UIdirectory("/svt/");
   fDir->SetGuidance("SVT system control");

   fMessenger = new SVTMessenger(this);

   //static_cast<SVTRunAction*>     (fRunAction     )->SetIndex(idx);
   static_cast<SVTEventAction*>   (fEventAction   )->SetIndex(idx);

   fLayerR.resize(fNLayers);
   fNSensorInZ.resize(fNLayers);
   for (G4int iLayer = 0; iLayer < fNLayers; iLayer++) {
      fLayerR[iLayer] = kDefaultLayerR[iLayer];
      fNSensorInZ[iLayer] = kDefaultNSensorInZ[iLayer];
   }
   fPixelSize[0] = kDefaultPixelSize[0];
   fPixelSize[1] = kDefaultPixelSize[1];
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTModule::~SVTModule()
{
   delete fDir;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserEventInformation* SVTModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return 0;
//    return new SVTUserEventInformation();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserTrackInformation* SVTModule::MakeTrackInformation(const G4Track* /*aTrack*/)
{
   return 0;
//    return new SVTUserTrackInformation();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
