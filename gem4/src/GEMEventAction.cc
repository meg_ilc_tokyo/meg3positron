//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <algorithm>
#include "generated/MEGMCKineEvent.h"
#include "generated/MEGMCTrackEvent.h"
#include "generated/MEGMCKineSubevent.h"
#include "generated/MEGMCTrackSubevent.h"
#include "GEMRootIO.hh"
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4Event.hh"
#include "G4VVisManager.hh"
#include "G4OpticalPhoton.hh"
#include "G4EmProcessSubType.hh"
#include "GEMEventAction.hh"
#include "GEMUserEventInformation.hh"
#include "GEMUnitConversion.hh"
#include "GEMTrajectory.hh"
#include "G4SystemOfUnits.hh"

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMEventAction::GEMEventAction()
   : modules()
   , useGEMTrajectoryDrawing(true)
   , drawOpticalPhotons(true)
   , drawEnergyMin(100 * keV)
{
   // eventMessenger=new GEMEventMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMEventAction::~GEMEventAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMEventAction::BeginOfEventAction(const G4Event* anEvent)
{
   if (GEMRootIO::GetInstance()->GetOpenFileFailed()) {
      std::ostringstream o;
      o << "Output root files already exist.";
      G4Exception(__func__, "", RunMustBeAborted, o.str().c_str());
      G4RunManager::GetRunManager()->AbortRun();
   }
   GEMRootIO::GetInstance()->SetFillThisEvent(true);

   //New event, add the user information object
   std::vector<GEMAbsModule*>::iterator mod;

   GEMUserEventInformation *ueInfo = new GEMUserEventInformation();
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod)) {
         ueInfo->AddUserEventInformation((*mod)->MakeEventInformation(anEvent));
      }
   }
   G4EventManager::GetEventManager()->SetUserInformation(ueInfo);

   G4UserEventAction *act;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetEventAction())) {
         act->BeginOfEventAction(anEvent);
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMEventAction::EndOfEventAction(const G4Event* anEvent)
{
   GEMUserEventInformation* eveInfo =
      dynamic_cast<GEMUserEventInformation*>(
         G4EventManager::GetEventManager()->GetConstCurrentEvent()->
         GetUserInformation());

   G4double timeOI = eveInfo->timeOfInterest;

   TClonesArray *romeKineSubevents = GEMRootIO::GetInstance()->GetMCKineSubevents();
   romeKineSubevents->Delete();
   romeKineSubevents->ExpandCreate(0); // clear
   romeKineSubevents->ExpandCreate(1);

   TClonesArray *romeTrackSubevents = GEMRootIO::GetInstance()->GetMCTrackSubevents();
   romeTrackSubevents->Delete();
   romeTrackSubevents->ExpandCreate(0); // clear
   romeTrackSubevents->ExpandCreate(1);

   MEGMCKineEvent     *romePrimaries    = GEMRootIO::GetInstance()->GetMCKineEvent();
   MEGMCKineSubevent  *romeSubPrimaries = static_cast<MEGMCKineSubevent*>(romeKineSubevents->At(0));
   TClonesArray       *romeTracks       = GEMRootIO::GetInstance()->GetMCTrackEvents();
   MEGMCTrackSubevent *romeSubTrack     = static_cast<MEGMCTrackSubevent*>(romeTrackSubevents->At(0));

   G4int nPrimary = eveInfo->primaryInfos.size();
   romePrimaries->Setnvtx(nPrimary);
   romePrimaries->Setnprimary(nPrimary);
   romePrimaries->SetvtxidSize(nPrimary);
   romePrimaries->SetvtypeSize(nPrimary);
   romePrimaries->SetgcodeSize(nPrimary);
   romePrimaries->SetxvtxSize(nPrimary);
   romePrimaries->SetyvtxSize(nPrimary);
   romePrimaries->SetzvtxSize(nPrimary);
   romePrimaries->SettvtxSize(nPrimary);
   romePrimaries->SetxmomSize(nPrimary);
   romePrimaries->SetymomSize(nPrimary);
   romePrimaries->SetzmomSize(nPrimary);

   romeSubPrimaries->Setnvtx(nPrimary);
   romeSubPrimaries->Setnprimary(nPrimary);
   romeSubPrimaries->SetvtxidSize(nPrimary);
   romeSubPrimaries->SetvtypeSize(nPrimary);
   romeSubPrimaries->SetgcodeSize(nPrimary);
   romeSubPrimaries->SetxvtxSize(nPrimary);
   romeSubPrimaries->SetyvtxSize(nPrimary);
   romeSubPrimaries->SetzvtxSize(nPrimary);
   romeSubPrimaries->SettvtxSize(nPrimary);
   romeSubPrimaries->SetxmomSize(nPrimary);
   romeSubPrimaries->SetymomSize(nPrimary);
   romeSubPrimaries->SetzmomSize(nPrimary);

   romeSubTrack->Setnmcvertex(nPrimary);

   std::vector<GEMUserPrimaryVertexInformation*>::iterator primary;
   G4int iPrimary;
   for (primary = eveInfo->primaryInfos.begin(), iPrimary = 0;
        primary != eveInfo->primaryInfos.end(); ++primary, iPrimary++) {
      romePrimaries->SetvtxidAt(iPrimary, (*primary)->vtxid);
      romePrimaries->SetvtypeAt(iPrimary, (*primary)->vtype);
      romePrimaries->SetgcodeAt(iPrimary, (*primary)->gcode);
      romePrimaries->SetxvtxAt(iPrimary, (*primary)->xvtx * kUnitConvLength);
      romePrimaries->SetyvtxAt(iPrimary, (*primary)->yvtx * kUnitConvLength);
      romePrimaries->SetzvtxAt(iPrimary, (*primary)->zvtx * kUnitConvLength);
      romePrimaries->SettvtxAt(iPrimary, ((*primary)->tvtx - timeOI) * kUnitConvTime);
      romePrimaries->SetxmomAt(iPrimary, (*primary)->xmom * kUnitConvEnergy);
      romePrimaries->SetymomAt(iPrimary, (*primary)->ymom * kUnitConvEnergy);
      romePrimaries->SetzmomAt(iPrimary, (*primary)->zmom * kUnitConvEnergy);

      romeSubPrimaries->SetvtxidAt(iPrimary, (*primary)->vtxid);
      romeSubPrimaries->SetvtypeAt(iPrimary, (*primary)->vtype);
      romeSubPrimaries->SetgcodeAt(iPrimary, (*primary)->gcode);
      romeSubPrimaries->SetxvtxAt(iPrimary, (*primary)->xvtx * kUnitConvLength);
      romeSubPrimaries->SetyvtxAt(iPrimary, (*primary)->yvtx * kUnitConvLength);
      romeSubPrimaries->SetzvtxAt(iPrimary, (*primary)->zvtx * kUnitConvLength);
      romeSubPrimaries->SettvtxAt(iPrimary, ((*primary)->tvtx - timeOI) * kUnitConvTime);
      romeSubPrimaries->SetxmomAt(iPrimary, (*primary)->xmom * kUnitConvEnergy);
      romeSubPrimaries->SetymomAt(iPrimary, (*primary)->ymom * kUnitConvEnergy);
      romeSubPrimaries->SetzmomAt(iPrimary, (*primary)->zmom * kUnitConvEnergy);
   }

   G4int nSecondary = eveInfo->secondaryInfos.size();
   romePrimaries->Setnsecondary(nSecondary);
   romePrimaries->Setsevid2Size(nSecondary);
   romePrimaries->Setvtxid2Size(nSecondary);
   romePrimaries->Setvtype2Size(nSecondary);
   romePrimaries->Setgcode2Size(nSecondary);
   romePrimaries->Setxvtx2Size(nSecondary);
   romePrimaries->Setyvtx2Size(nSecondary);
   romePrimaries->Setzvtx2Size(nSecondary);
   romePrimaries->Settvtx2Size(nSecondary);
   romePrimaries->Setxmom2Size(nSecondary);
   romePrimaries->Setymom2Size(nSecondary);
   romePrimaries->Setzmom2Size(nSecondary);

   romeSubPrimaries->Setnsecondary(nSecondary);
   romeSubPrimaries->Setvtxid2Size(nSecondary);
   romeSubPrimaries->Setvtype2Size(nSecondary);
   romeSubPrimaries->Setgcode2Size(nSecondary);
   romeSubPrimaries->Setxvtx2Size(nSecondary);
   romeSubPrimaries->Setyvtx2Size(nSecondary);
   romeSubPrimaries->Setzvtx2Size(nSecondary);
   romeSubPrimaries->Settvtx2Size(nSecondary);
   romeSubPrimaries->Setxmom2Size(nSecondary);
   romeSubPrimaries->Setymom2Size(nSecondary);
   romeSubPrimaries->Setzmom2Size(nSecondary);

   std::vector<GEMUserPrimaryVertexInformation*>::iterator secondary;
   G4int iSecondary;
   for (secondary = eveInfo->secondaryInfos.begin(), iSecondary = 0;
        secondary != eveInfo->secondaryInfos.end(); ++secondary, iSecondary++) {
      romePrimaries->Setsevid2At(iSecondary, 0);
      romePrimaries->Setvtxid2At(iSecondary, (*secondary)->vtxid);
      romePrimaries->Setvtype2At(iSecondary, (*secondary)->vtype);
      romePrimaries->Setgcode2At(iSecondary, (*secondary)->gcode);
      romePrimaries->Setxvtx2At(iSecondary, (*secondary)->xvtx * kUnitConvLength);
      romePrimaries->Setyvtx2At(iSecondary, (*secondary)->yvtx * kUnitConvLength);
      romePrimaries->Setzvtx2At(iSecondary, (*secondary)->zvtx * kUnitConvLength);
      romePrimaries->Settvtx2At(iSecondary, ((*secondary)->tvtx - timeOI) * kUnitConvTime);
      romePrimaries->Setxmom2At(iSecondary, (*secondary)->xmom * kUnitConvEnergy);
      romePrimaries->Setymom2At(iSecondary, (*secondary)->ymom * kUnitConvEnergy);
      romePrimaries->Setzmom2At(iSecondary, (*secondary)->zmom * kUnitConvEnergy);

      romeSubPrimaries->Setvtxid2At(iSecondary, (*secondary)->vtxid);
      romeSubPrimaries->Setvtype2At(iSecondary, (*secondary)->vtype);
      romeSubPrimaries->Setgcode2At(iSecondary, (*secondary)->gcode);
      romeSubPrimaries->Setxvtx2At(iSecondary, (*secondary)->xvtx * kUnitConvLength);
      romeSubPrimaries->Setyvtx2At(iSecondary, (*secondary)->yvtx * kUnitConvLength);
      romeSubPrimaries->Setzvtx2At(iSecondary, (*secondary)->zvtx * kUnitConvLength);
      romeSubPrimaries->Settvtx2At(iSecondary, ((*secondary)->tvtx - timeOI) * kUnitConvTime);
      romeSubPrimaries->Setxmom2At(iSecondary, (*secondary)->xmom * kUnitConvEnergy);
      romeSubPrimaries->Setymom2At(iSecondary, (*secondary)->ymom * kUnitConvEnergy);
      romeSubPrimaries->Setzmom2At(iSecondary, (*secondary)->zmom * kUnitConvEnergy);
   }

   G4int nrad = eveInfo->radiationInfos.size();
   romePrimaries->Setnrad(nrad);
   romePrimaries->SetmecradSize(nrad);
   romePrimaries->SetxradSize(nrad);
   romePrimaries->SetyradSize(nrad);
   romePrimaries->SetzradSize(nrad);
   romePrimaries->SettradSize(nrad);
   romePrimaries->SetxradmomSize(nrad);
   romePrimaries->SetyradmomSize(nrad);
   romePrimaries->SetzradmomSize(nrad);

   romeSubPrimaries->Setnrad(nrad);
   romeSubPrimaries->SetmecradSize(nrad);
   romeSubPrimaries->SetxradSize(nrad);
   romeSubPrimaries->SetyradSize(nrad);
   romeSubPrimaries->SetzradSize(nrad);
   romeSubPrimaries->SettradSize(nrad);
   romeSubPrimaries->SetxradmomSize(nrad);
   romeSubPrimaries->SetyradmomSize(nrad);
   romeSubPrimaries->SetzradmomSize(nrad);

   std::vector<GEMUserPrimaryVertexInformation*>::iterator radIt;
   G4int irad;
   for (radIt = eveInfo->radiationInfos.begin(), irad = 0;
        radIt != eveInfo->radiationInfos.end(); ++radIt, irad++) {
      romePrimaries->SetmecradAt(irad, ((*radIt)->vtype == fAnnihilation) ? 0 : 1);
      romePrimaries->SetxradAt(irad, (*radIt)->xvtx * kUnitConvLength);
      romePrimaries->SetyradAt(irad, (*radIt)->yvtx * kUnitConvLength);
      romePrimaries->SetzradAt(irad, (*radIt)->zvtx * kUnitConvLength);
      romePrimaries->SettradAt(irad, ((*radIt)->tvtx - timeOI) * kUnitConvTime);
      romePrimaries->SetxradmomAt(irad, (*radIt)->xmom * kUnitConvEnergy);
      romePrimaries->SetyradmomAt(irad, (*radIt)->ymom * kUnitConvEnergy);
      romePrimaries->SetzradmomAt(irad, (*radIt)->zmom * kUnitConvEnergy);

      romeSubPrimaries->SetmecradAt(irad, ((*radIt)->vtype == fAnnihilation) ? 0 : 1);
      romeSubPrimaries->SetxradAt(irad, (*radIt)->xvtx * kUnitConvLength);
      romeSubPrimaries->SetyradAt(irad, (*radIt)->yvtx * kUnitConvLength);
      romeSubPrimaries->SetzradAt(irad, (*radIt)->zvtx * kUnitConvLength);
      romeSubPrimaries->SettradAt(irad, ((*radIt)->tvtx - timeOI) * kUnitConvTime);
      romeSubPrimaries->SetxradmomAt(irad, (*radIt)->xmom * kUnitConvEnergy);
      romeSubPrimaries->SetyradmomAt(irad, (*radIt)->ymom * kUnitConvEnergy);
      romeSubPrimaries->SetzradmomAt(irad, (*radIt)->zmom * kUnitConvEnergy);
   }

   size_t iTrack, nTrack;
#if 0 /* clear all once */
   romeTracks->Delete();
   romeTracks->ExpandCreate(0); // clear
   romeTracks->ExpandCreate(eveInfo->trackInfos.size());
#else
   if ((nTrack = romeTracks->GetEntriesFast()) != eveInfo->trackInfos.size()) {
      for (iTrack = eveInfo->trackInfos.size(); iTrack < nTrack; iTrack++) {
         romeTracks->RemoveAt(iTrack);
      }
      romeTracks->ExpandCreate(eveInfo->trackInfos.size());
   }
#endif
   romeSubTrack->Setnmctrack(eveInfo->trackInfos.size());
   romeSubTrack->SetparticleidSize(eveInfo->trackInfos.size());
   romeSubTrack->SettrackidSize(eveInfo->trackInfos.size());
   romeSubTrack->SetvertexidSize(eveInfo->trackInfos.size());
   romeSubTrack->SetparentidSize(eveInfo->trackInfos.size());
   romeSubTrack->SettekineSize(eveInfo->trackInfos.size());
   romeSubTrack->SettracklengthSize(eveInfo->trackInfos.size());
   romeSubTrack->SettrackendmecSize(eveInfo->trackInfos.size());
   romeSubTrack->SetnpointSize(eveInfo->trackInfos.size());
   romeSubTrack->SetnturnSize(eveInfo->trackInfos.size());

   romeSubTrack->Settrack1stxSize(eveInfo->trackInfos.size());
   romeSubTrack->Settrack1stySize(eveInfo->trackInfos.size());
   romeSubTrack->Settrack1stzSize(eveInfo->trackInfos.size());
   romeSubTrack->Settrack1sttSize(eveInfo->trackInfos.size());

   std::vector<GEMUserTrackInformation*>::iterator track;

   // count total number of points and turns
   G4int totalPoints(0), totalPointsBuf(0), totalTurns(0);
   if (GEMRootIO::GetInstance()->GetSaveTrajectory()) {
      for (track = eveInfo->trackInfos.begin();
           track != eveInfo->trackInfos.end(); ++track) {
         totalPoints += (*track)->trackInfo.nstep;
         totalTurns += (*track)->trackInfo.nturn;
      }
   }
   G4int trajectorySaveMethod = 1; // 0: save each point coordinate, 1: save difference, 2: save both
   switch (trajectorySaveMethod) {
   case 0:
   case 2:
   default:
      totalPointsBuf = totalPoints;
      break;
   case 1:
      totalPointsBuf = 0;
      break;
   }

   romeSubTrack->Setntrackbuf(totalPoints);
   romeSubTrack->SettrackbufxSize(totalPointsBuf);
   romeSubTrack->SettrackbufySize(totalPointsBuf);
   romeSubTrack->SettrackbufzSize(totalPointsBuf);
   romeSubTrack->SettrackbuftSize(totalPointsBuf);
   romeSubTrack->SettrackbufdeSize(totalPointsBuf);

   romeSubTrack->SettrackbufdxSize(totalPoints);
   romeSubTrack->SettrackbufdySize(totalPoints);
   romeSubTrack->SettrackbufdzSize(totalPoints);
   romeSubTrack->SettrackbufdtSize(totalPoints);
   romeSubTrack->SettrackbufedepSize(totalPoints);

   romeSubTrack->Setntotturn(totalTurns);
   romeSubTrack->SetxcyldchSize(totalTurns);
   romeSubTrack->SetycyldchSize(totalTurns);
   romeSubTrack->SetzcyldchSize(totalTurns);
   romeSubTrack->SetxmomcyldchSize(totalTurns);
   romeSubTrack->SetymomcyldchSize(totalTurns);
   romeSubTrack->SetzmomcyldchSize(totalTurns);

   MEGMCTrackEvent *romeTrack;
   G4int iBuf, jBuf = 0;
   G4int iturn, jturn = 0;
   for (track = eveInfo->trackInfos.begin(), iTrack = 0;
        track != eveInfo->trackInfos.end(); ++track, iTrack++) {

      romeTrack = static_cast<MEGMCTrackEvent*>(romeTracks->At(iTrack));
      romeTrack->Setparticleid(GEMUserTrackInformation::GetG3ParticleID((*track)->trackInfo.particledef));
      // romeTrack->Settrackid((*track)->trackInfo.trackid);
      romeTrack->Settrackid(eveInfo->GetTrackInfoIndexFromID((*track)->trackInfo.trackid));// give new id
      romeTrack->Setvertexid((*track)->trackInfo.vertexid);
      romeTrack->SetPrimaryParticleIndex((*track)->trackInfo.PrimaryParticleIndex);
      romeTrack->SetParentIndex(eveInfo->GetTrackInfoIndexFromID((*track)->trackInfo.ParentIndex));
      romeTrack->Settekine((*track)->trackInfo.tekine * kUnitConvEnergy);
      romeTrack->Settracklength((*track)->trackInfo.tracklength * kUnitConvLength);
      romeTrack->Setnturn((*track)->trackInfo.nturn);
      romeTrack->Settrackendmec((*track)->trackInfo.trackendmec);
      romeTrack->SetmomAt(0, (*track)->trackInfo.momx * kUnitConvEnergy);
      romeTrack->SetmomAt(1, (*track)->trackInfo.momy * kUnitConvEnergy);
      romeTrack->SetmomAt(2, (*track)->trackInfo.momz * kUnitConvEnergy);

      romeSubTrack->SetparticleidAt(iTrack,
                                    GEMUserTrackInformation::GetG3ParticleID((*track)->trackInfo.particledef));
      romeSubTrack->SettrackidAt(iTrack,
                                 eveInfo->GetTrackInfoIndexFromID((*track)->trackInfo.trackid));// give new id
      romeSubTrack->SetvertexidAt(iTrack, (*track)->trackInfo.vertexid);
      romeSubTrack->SetparentidAt(iTrack,
                                  eveInfo->GetTrackInfoIndexFromID((*track)->trackInfo.ParentID));// give new id
      romeSubTrack->SettekineAt(iTrack, (*track)->trackInfo.tekine * kUnitConvEnergy);
      romeSubTrack->SettracklengthAt(iTrack, (*track)->trackInfo.tracklength * kUnitConvLength);
      romeSubTrack->SetnturnAt(iTrack, (*track)->trackInfo.nturn);
      romeSubTrack->SettrackendmecAt(iTrack, (*track)->trackInfo.trackendmec);

      romeTrack->SetxcyldchSize((*track)->trackInfo.nturn);
      romeTrack->SetycyldchSize((*track)->trackInfo.nturn);
      romeTrack->SetzcyldchSize((*track)->trackInfo.nturn);
      romeTrack->SetxmomcyldchSize((*track)->trackInfo.nturn);
      romeTrack->SetymomcyldchSize((*track)->trackInfo.nturn);
      romeTrack->SetzmomcyldchSize((*track)->trackInfo.nturn);

      for (iturn = 0; iturn < (*track)->trackInfo.nturn; iturn++, jturn++) {

         if ((*track)->trackInfo.hitcyldch.count(iturn)) {

            romeTrack->SetxcyldchAt(iturn,(*track)->trackInfo.poscyldch[iturn].x());
            romeTrack->SetycyldchAt(iturn,(*track)->trackInfo.poscyldch[iturn].y());
            romeTrack->SetzcyldchAt(iturn,(*track)->trackInfo.poscyldch[iturn].z());
            romeTrack->SetxmomcyldchAt(iturn,(*track)->trackInfo.momcyldch[iturn].x());
            romeTrack->SetymomcyldchAt(iturn,(*track)->trackInfo.momcyldch[iturn].y());
            romeTrack->SetzmomcyldchAt(iturn,(*track)->trackInfo.momcyldch[iturn].z());
            
            romeSubTrack->SetxcyldchAt(jturn,(*track)->trackInfo.poscyldch[iturn].x());
            romeSubTrack->SetycyldchAt(jturn,(*track)->trackInfo.poscyldch[iturn].y());
            romeSubTrack->SetzcyldchAt(jturn,(*track)->trackInfo.poscyldch[iturn].z());
            romeSubTrack->SetxmomcyldchAt(jturn,(*track)->trackInfo.momcyldch[iturn].x());
            romeSubTrack->SetymomcyldchAt(jturn,(*track)->trackInfo.momcyldch[iturn].y());
            romeSubTrack->SetzmomcyldchAt(jturn,(*track)->trackInfo.momcyldch[iturn].z());
            
         } else {

            romeTrack->SetxcyldchAt(iturn,0.);
            romeTrack->SetycyldchAt(iturn,0.);
            romeTrack->SetzcyldchAt(iturn,0.);
            romeTrack->SetxmomcyldchAt(iturn,0.);
            romeTrack->SetymomcyldchAt(iturn,0.);
            romeTrack->SetzmomcyldchAt(iturn,0.);
            
            romeSubTrack->SetxcyldchAt(jturn,0.);
            romeSubTrack->SetycyldchAt(jturn,0.);
            romeSubTrack->SetzcyldchAt(jturn,0.);
            romeSubTrack->SetxmomcyldchAt(jturn,0.);
            romeSubTrack->SetymomcyldchAt(jturn,0.);
            romeSubTrack->SetzmomcyldchAt(jturn,0.);

         }

      }

      G4int nstep(0);
      if (GEMRootIO::GetInstance()->GetSaveTrajectory()) {
         nstep = (*track)->trackInfo.nstep;
      }


      // Set the 1st point of the trajectory
      Double_t prevX(0), prevY(0), prevZ(0), prevT(0);
      if ((*track)->trackInfo.nstep) {
         prevX = (*track)->trackInfo.trackbufx[0];
         prevY = (*track)->trackInfo.trackbufy[0];
         prevZ = (*track)->trackInfo.trackbufz[0];
         prevT = ((*track)->trackInfo.trackbuft[0] - timeOI);
      }
      romeSubTrack->Settrack1stxAt(iTrack, prevX  * kUnitConvLength);
      romeSubTrack->Settrack1styAt(iTrack, prevY  * kUnitConvLength);
      romeSubTrack->Settrack1stzAt(iTrack, prevZ  * kUnitConvLength);
      romeSubTrack->Settrack1sttAt(iTrack, prevT  * kUnitConvTime);
      romeTrack->Settrack1stx(prevX  * kUnitConvLength);
      romeTrack->Settrack1sty(prevY  * kUnitConvLength);
      romeTrack->Settrack1stz(prevZ  * kUnitConvLength);
      romeTrack->Settrack1stt(prevT  * kUnitConvTime);


      switch (trajectorySaveMethod) {
      case 0:
      default:
         romeSubTrack->SetnpointAt(iTrack, nstep);
         romeTrack->Setnstep(nstep);
         romeTrack->SettrackbufxSize(nstep);
         romeTrack->SettrackbufySize(nstep);
         romeTrack->SettrackbufzSize(nstep);
         romeTrack->SettrackbuftSize(nstep);
         romeTrack->SettrackbufdeSize(nstep);

         romeTrack->SettrackbufdxSize(nstep);
         romeTrack->SettrackbufdySize(nstep);
         romeTrack->SettrackbufdzSize(nstep);
         romeTrack->SettrackbufdtSize(nstep);
         romeTrack->SettrackbufedepSize(nstep);

         for (iBuf = 0; iBuf < nstep; iBuf++, jBuf++) {
            romeTrack->SettrackbufxAt(iBuf, (*track)->trackInfo.trackbufx[iBuf]  * kUnitConvLength);
            romeTrack->SettrackbufyAt(iBuf, (*track)->trackInfo.trackbufy[iBuf]  * kUnitConvLength);
            romeTrack->SettrackbufzAt(iBuf, (*track)->trackInfo.trackbufz[iBuf]  * kUnitConvLength);
            romeTrack->SettrackbuftAt(iBuf, ((*track)->trackInfo.trackbuft[iBuf] - timeOI) * kUnitConvTime);
            romeTrack->SettrackbufdeAt(iBuf, (*track)->trackInfo.trackbufde[iBuf] * kUnitConvEnergy);

            romeSubTrack->SettrackbufxAt(jBuf, (*track)->trackInfo.trackbufx[iBuf]  * kUnitConvLength);
            romeSubTrack->SettrackbufyAt(jBuf, (*track)->trackInfo.trackbufy[iBuf]  * kUnitConvLength);
            romeSubTrack->SettrackbufzAt(jBuf, (*track)->trackInfo.trackbufz[iBuf]  * kUnitConvLength);
            romeSubTrack->SettrackbuftAt(jBuf, ((*track)->trackInfo.trackbuft[iBuf] - timeOI) * kUnitConvTime);
            romeSubTrack->SettrackbufdeAt(jBuf, (*track)->trackInfo.trackbufde[iBuf] * kUnitConvEnergy);

            // delta
            romeTrack->SettrackbufdxAt(iBuf, 0);
            romeTrack->SettrackbufdyAt(iBuf, 0);
            romeTrack->SettrackbufdzAt(iBuf, 0);
            romeTrack->SettrackbufdtAt(iBuf, 0);
            romeTrack->SettrackbufedepAt(iBuf, 0);

            romeSubTrack->SettrackbufdxAt(jBuf, 0);
            romeSubTrack->SettrackbufdyAt(jBuf, 0);
            romeSubTrack->SettrackbufdzAt(jBuf, 0);
            romeSubTrack->SettrackbufdtAt(jBuf, 0);
            romeSubTrack->SettrackbufedepAt(jBuf, 0);
         }
         break;

      case 1:
         romeSubTrack->SetnpointAt(iTrack, nstep);
         romeTrack->Setnstep(nstep);
         romeTrack->SettrackbufxSize(0);
         romeTrack->SettrackbufySize(0);
         romeTrack->SettrackbufzSize(0);
         romeTrack->SettrackbuftSize(0);
         romeTrack->SettrackbufdeSize(0);

         romeTrack->SettrackbufdxSize(nstep);
         romeTrack->SettrackbufdySize(nstep);
         romeTrack->SettrackbufdzSize(nstep);
         romeTrack->SettrackbufdtSize(nstep);
         romeTrack->SettrackbufedepSize(nstep);

         for (iBuf = 0; iBuf < nstep; iBuf++, jBuf++) {
            // delta
            romeTrack->SettrackbufdxAt(iBuf, ((*track)->trackInfo.trackbufx[iBuf] - prevX)  * kUnitConvLength);
            romeTrack->SettrackbufdyAt(iBuf, ((*track)->trackInfo.trackbufy[iBuf] - prevY)  * kUnitConvLength);
            romeTrack->SettrackbufdzAt(iBuf, ((*track)->trackInfo.trackbufz[iBuf] - prevZ)  * kUnitConvLength);
            romeTrack->SettrackbufdtAt(iBuf, (((*track)->trackInfo.trackbuft[iBuf] - timeOI) - prevT) * kUnitConvTime);
            romeTrack->SettrackbufedepAt(iBuf, ((*track)->trackInfo.trackbufde[iBuf]) * kUnitConvEnergy);

            romeSubTrack->SettrackbufdxAt(jBuf, ((*track)->trackInfo.trackbufx[iBuf] - prevX)  * kUnitConvLength);
            romeSubTrack->SettrackbufdyAt(jBuf, ((*track)->trackInfo.trackbufy[iBuf] - prevY)  * kUnitConvLength);
            romeSubTrack->SettrackbufdzAt(jBuf, ((*track)->trackInfo.trackbufz[iBuf] - prevZ)  * kUnitConvLength);
            romeSubTrack->SettrackbufdtAt(jBuf, (((*track)->trackInfo.trackbuft[iBuf] - timeOI) - prevT) * kUnitConvTime);
            romeSubTrack->SettrackbufedepAt(jBuf, ((*track)->trackInfo.trackbufde[iBuf]) * kUnitConvEnergy);

            prevX = (*track)->trackInfo.trackbufx[iBuf];
            prevY = (*track)->trackInfo.trackbufy[iBuf];
            prevZ = (*track)->trackInfo.trackbufz[iBuf];
            prevT = ((*track)->trackInfo.trackbuft[iBuf] - timeOI);
         }
         break;

      case 2:
         romeSubTrack->SetnpointAt(iTrack, nstep);
         romeTrack->Setnstep(nstep);
         romeTrack->SettrackbufxSize(nstep);
         romeTrack->SettrackbufySize(nstep);
         romeTrack->SettrackbufzSize(nstep);
         romeTrack->SettrackbuftSize(nstep);
         romeTrack->SettrackbufdeSize(nstep);

         romeTrack->SettrackbufdxSize(nstep);
         romeTrack->SettrackbufdySize(nstep);
         romeTrack->SettrackbufdzSize(nstep);
         romeTrack->SettrackbufdtSize(nstep);
         romeTrack->SettrackbufedepSize(nstep);

         for (iBuf = 0; iBuf < nstep; iBuf++, jBuf++) {
            romeTrack->SettrackbufxAt(iBuf, (*track)->trackInfo.trackbufx[iBuf]  * kUnitConvLength);
            romeTrack->SettrackbufyAt(iBuf, (*track)->trackInfo.trackbufy[iBuf]  * kUnitConvLength);
            romeTrack->SettrackbufzAt(iBuf, (*track)->trackInfo.trackbufz[iBuf]  * kUnitConvLength);
            romeTrack->SettrackbuftAt(iBuf, ((*track)->trackInfo.trackbuft[iBuf] - timeOI) * kUnitConvTime);
            romeTrack->SettrackbufdeAt(iBuf, (*track)->trackInfo.trackbufde[iBuf] * kUnitConvEnergy);

            romeSubTrack->SettrackbufxAt(jBuf, (*track)->trackInfo.trackbufx[iBuf]  * kUnitConvLength);
            romeSubTrack->SettrackbufyAt(jBuf, (*track)->trackInfo.trackbufy[iBuf]  * kUnitConvLength);
            romeSubTrack->SettrackbufzAt(jBuf, (*track)->trackInfo.trackbufz[iBuf]  * kUnitConvLength);
            romeSubTrack->SettrackbuftAt(jBuf, ((*track)->trackInfo.trackbuft[iBuf] - timeOI) * kUnitConvTime);
            romeSubTrack->SettrackbufdeAt(jBuf, (*track)->trackInfo.trackbufde[iBuf] * kUnitConvEnergy);

            // delta
            romeTrack->SettrackbufdxAt(iBuf, ((*track)->trackInfo.trackbufx[iBuf] - prevX)  * kUnitConvLength);
            romeTrack->SettrackbufdyAt(iBuf, ((*track)->trackInfo.trackbufy[iBuf] - prevY)  * kUnitConvLength);
            romeTrack->SettrackbufdzAt(iBuf, ((*track)->trackInfo.trackbufz[iBuf] - prevZ)  * kUnitConvLength);
            romeTrack->SettrackbufdtAt(iBuf, (((*track)->trackInfo.trackbuft[iBuf] - timeOI) - prevT) * kUnitConvTime);
            romeTrack->SettrackbufedepAt(iBuf, ((*track)->trackInfo.trackbufde[iBuf]) * kUnitConvEnergy);

            romeSubTrack->SettrackbufdxAt(jBuf, ((*track)->trackInfo.trackbufx[iBuf] - prevX)  * kUnitConvLength);
            romeSubTrack->SettrackbufdyAt(jBuf, ((*track)->trackInfo.trackbufy[iBuf] - prevY)  * kUnitConvLength);
            romeSubTrack->SettrackbufdzAt(jBuf, ((*track)->trackInfo.trackbufz[iBuf] - prevZ)  * kUnitConvLength);
            romeSubTrack->SettrackbufdtAt(jBuf, (((*track)->trackInfo.trackbuft[iBuf] - timeOI) - prevT) * kUnitConvTime);
            romeSubTrack->SettrackbufedepAt(jBuf, ((*track)->trackInfo.trackbufde[iBuf]) * kUnitConvEnergy);

            prevX = (*track)->trackInfo.trackbufx[iBuf];
            prevY = (*track)->trackInfo.trackbufy[iBuf];
            prevZ = (*track)->trackInfo.trackbufz[iBuf];
            prevT = ((*track)->trackInfo.trackbuft[iBuf] - timeOI);
         }
         break;
      }
   }

   G4UserEventAction *act;
   vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetEventAction())) {
         act->EndOfEventAction(anEvent);
      }
   }

   GEMRootIO::GetInstance()->Fill();

   G4int eveID = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
   eveID++;
   if (((eveID >    0 && eveID <    10) && (eveID %     1 == 0)) ||
       ((eveID >    9 && eveID <   100) && (eveID %    10 == 0)) ||
       ((eveID >   99 && eveID <  1000) && (eveID %   100 == 0)) ||
       ((eveID >  999 && eveID < 10000) && (eveID %  1000 == 0)) ||
       ((eveID > 9999) && (eveID % 10000 == 0))) {
      if (eveID == 1) {
         G4cerr << eveID << " event done." << G4endl;
      } else {
         G4cerr << eveID << " events done." << G4endl;
      }
   }

   // Visualization
   if (useGEMTrajectoryDrawing) {
      G4TrajectoryContainer* trajectoryContainer = anEvent->GetTrajectoryContainer();

      G4int n_trajectories = 0;
      if (trajectoryContainer) {
         n_trajectories = trajectoryContainer->entries();
      }

      // extract the trajectories and draw them
      if (G4VVisManager::GetConcreteInstance()) {
         for (G4int i = 0; i < n_trajectories; i++) {
            GEMTrajectory* trj = dynamic_cast<GEMTrajectory*>((*(anEvent->GetTrajectoryContainer()))[i]);
            if ((trj->GetParticleDefinition() == G4OpticalPhoton::OpticalPhotonDefinition() &&
                 !drawOpticalPhotons) ||
                (trj->GetInitialKineticEnergy() < drawEnergyMin)
                || 0) {
               trj->SetDrawTrajectory(false);
            } else {
               trj->SetDrawTrajectory(true);
            }
            trj->DrawTrajectory(50);
         }
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
