//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <algorithm>
#include "GEMStackingAction.hh"
#include "GEMRootIO.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4Track.hh"
#include "G4ios.hh"
#include "G4StackManager.hh"

G4bool GEMStackingAction::fgReClassificationRequested = false;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMStackingAction::GEMStackingAction()
   : modules()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMStackingAction::~GEMStackingAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ClassificationOfNewTrack
GEMStackingAction::ClassifyNewTrack(const G4Track *aTrack)
{
   std::vector<G4ClassificationOfNewTrack> classification;
   G4ClassificationOfNewTrack c;

   G4UserStackingAction *act;
   std::vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetStackingAction())) {
         if ((c = act->ClassifyNewTrack(aTrack)) != fUrgent) {
            classification.push_back(c);
         }
      }
   }

   if (!classification.empty()) {
      c = *std::min_element(classification.begin(), classification.end());
   } else {
      c = fUrgent;
   }

   // Kill all particles of non-filled event (to save cpu time).
   if (!GEMRootIO::GetInstance()->GetFillThisEvent()) {
      c = fKill;
   }

   return c;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMStackingAction::NewStage()
{
   G4UserStackingAction *act;
   std::vector<GEMAbsModule*>::iterator mod;
   G4bool reClassify = false;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetStackingAction())) {
         act->NewStage();
         if (fgReClassificationRequested) {
            reClassify = true;
            fgReClassificationRequested = false;
         }
      }
   }
   if (!GEMRootIO::GetInstance()->GetFillThisEvent()) {
      stackManager->clear(); // Event abortion
      return;
   }
   if (reClassify) {
      stackManager->ReClassify();
   }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMStackingAction::PrepareNewEvent()
{
   G4UserStackingAction *act;
   std::vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetStackingAction())) {
         act->PrepareNewEvent();
      }
   }
   fgReClassificationRequested = false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
