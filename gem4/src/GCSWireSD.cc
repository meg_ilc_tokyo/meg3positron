// $Id: GCSWireSD.cc 244 2013-06-05 07:42:19Z renga $
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
#include "GCSWireSD.hh"
#include "generated/MEGMCGCSWireEvent.h"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4VProcess.hh"
#include "G4SDManager.hh"
#include "G4VTouchable.hh"
#include "G4Tubs.hh"
#include <TString.h>

#include "GEMUnitConversion.hh"
#include "GEMRootIO.hh"

using namespace std;
using namespace CLHEP;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GCSWireSD::GCSWireSD(G4String name)
   :G4VSensitiveDetector(name)
{
   pWireEvent_arr = GEMRootIO::GetInstance()->GetMCGCSWireEvents();
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void GCSWireSD::Initialize(G4HCofThisEvent*)
{
   // Initializations at beginning of each event
   hitID = 0;
   map_hitID.clear();
   pWireEvent_arr->Clear();
}
   
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4bool GCSWireSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{

   G4Track* theTrack = aStep->GetTrack();

   // Only for a charged particle
   if (theTrack->GetParticleDefinition()->GetPDGCharge() == 0.)
      return false;

   // Skip if Edep is 0
   G4double edep = aStep->GetTotalEnergyDeposit();
   if (edep <= 0.)
      return false;

   // Get step points
   auto preStepPoint  = aStep->GetPreStepPoint();
   auto postStepPoint = aStep->GetPostStepPoint();

   // Check volume name
   auto theTouchable = preStepPoint->GetTouchable();
   auto physVol = theTouchable->GetVolume();
   auto volname = physVol->GetName();
   if (!G4StrUtil::contains(volname, "CellPhys")) {
      G4cout << "Unexpected volume name: " << volname << G4endl;
      return false;
   }
   volname = Form("%s_%d", volname.data(), theTouchable->GetReplicaNumber(0));

   // Get parameters of this step
   G4double timePre = preStepPoint->GetGlobalTime();
   auto vPre  = preStepPoint->GetPosition();
   auto vPost = postStepPoint->GetPosition();

   // Get hit event of this wire
   MEGMCGCSWireEvent *pWireEvent(0);
   if (map_hitID.count(volname) == 0) {

      // Make a new hit
      TClonesArray &arr = *pWireEvent_arr;
      pWireEvent = new(arr[hitID]) MEGMCGCSWireEvent();

      // Add hit to the map of hits
      map_hitID[volname] = hitID;
      hitID++;

      // Initialize hit
      pWireEvent->Setedep(0);
      pWireEvent->SetIP(1e5);

      // Calculate wire position
      auto tubs = (G4Tubs*)physVol->GetLogicalVolume()->GetSolid();
      G4double rWire = (tubs->GetInnerRadius() + tubs->GetOuterRadius()) / 2.;
      G4double dPhi  = 360*degree / physVol->GetMultiplicity();
      G4double theta = dPhi * (physVol->GetCopyNo() + 0.5);
      pWireEvent->SetxyzWireAt(0, rWire*cos(theta));
      pWireEvent->SetxyzWireAt(1, rWire*sin(theta));
      pWireEvent->SetxyzWireAt(2, 0);

   } else {
      // Hit already exists for this wire
      G4int index = map_hitID[volname];
      pWireEvent = static_cast<MEGMCGCSWireEvent*>(pWireEvent_arr->At(index));
   }

   // Add Edep
   pWireEvent->Addedep(edep);

   // Make vectors
   G4ThreeVector vWireXY(pWireEvent->GetxyzWireAt(0), pWireEvent->GetxyzWireAt(1), 0);
   G4ThreeVector vPreXY(vPre.x(), vPre.y(), 0);
   G4ThreeVector vPostXY(vPost.x(), vPost.y(), 0);
   G4ThreeVector vStepXY = vPostXY - vPreXY;
   G4ThreeVector vProjXY = (vWireXY-vPreXY).project(vStepXY);

   // Find fractional distance to the minimum distance point
   G4double frac = 0;
   if (vStepXY.mag() > 0) {
      frac = vProjXY.mag() / vStepXY.mag();
      if (frac > 1) {
         frac = 1;
      } else if (frac < 0) {
         frac = 0;
      }
   }

   // Calculate vector of the minimum distance point
   G4ThreeVector vStep(vPost.x()-vPre.x(), vPost.y()-vPre.y(), vPost.z()-vPre.z());
   G4ThreeVector vIP = vPre + frac*vStep;

   // Calculate minimum distance
   G4ThreeVector vIPXY(vIP.x(), vIP.y(), 0.);
   G4double dist = (vIPXY - vWireXY).mag();

   // Update impact parameter
   if (dist < pWireEvent->GetIP()) {
      pWireEvent->SetxyzAt(0, vIP.x());
      pWireEvent->SetxyzAt(1, vIP.y());
      pWireEvent->SetxyzAt(2, vIP.z());
      pWireEvent->SetIP(dist);
      G4double dt = (postStepPoint->GetGlobalTime()-timePre);
      G4double timeIP = timePre + frac * dt;
      pWireEvent->Settime(timeIP);
   }

   // // Debugging
   // printf("aho %s %s pre:(%.1f, %.1f) wire:(%.1f, %.1f) IP:(%.1f, %.1f) IP:%.3f time:%.2f %.2f\n",
   //        theTrack->GetParticleDefinition()->GetParticleName().data(), volname.data(),
   //        vPre.x()/mm, vPre.y()/mm, vWireXY.x()/mm, vWireXY.y()/mm, vIP.x()/mm, vIP.y()/mm,
   //        pWireEvent->GetIP()/mm, pWireEvent->Gettime()/nanosecond, frac);

   return true;

}


