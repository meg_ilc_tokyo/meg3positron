//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "TARMessenger.hh"
#include "TARModule.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TARMessenger::TARMessenger(TARModule *mod)
   : fModule(mod)
{
   // See GEMPrimaryGeneratorMessenger, too

   // Make directories
   fDir = new G4UIdirectory("/tar/");
   fDir->SetGuidance("TAR (normal target) system control");
   fGeomDir = new G4UIdirectory("/tar/geom/");
   fGeomDir->SetGuidance("Commands for geometry");

   // Define commands
   fNumberOfHolesCmd = new G4UIcmdWithAnInteger("/tar/geom/numberOfHoles", this);
   fNumberOfHolesCmd->SetGuidance("Set number of holes");
   fNumberOfHolesCmd->SetParameterName("number", false);

   fHoleRadiusCmd = new G4UIcmdWithADoubleAndUnit("/tar/geom/holeRadius", this);
   fHoleRadiusCmd->SetGuidance("Set radius of holes");
   fHoleRadiusCmd->SetParameterName("radius", false);
   fHoleRadiusCmd->SetUnitCategory("Length");

   fTGTGeometryIdCmd = new G4UIcmdWithAnInteger("/tar/geom/TGTGeometryId", this);
   fTGTGeometryIdCmd->SetGuidance("Set geometry id by hand. If -1, DB value will be used.");
   fTGTGeometryIdCmd->SetParameterName("id", -1);

   fVerboseCmd = new G4UIcmdWithAnInteger("/tar/verbose", this);
   fVerboseCmd->SetGuidance("Set verbose level. 0: mute, 1: error, 2: warning, 3: info, 4: verbose, 5: debug");
   fVerboseCmd->SetParameterName("verbose", false);
   fVerboseCmd->SetDefaultValue(0);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TARMessenger::~TARMessenger()
{
   delete fNumberOfHolesCmd;
   delete fHoleRadiusCmd;
   delete fTGTGeometryIdCmd;
   delete fGeomDir;
   delete fDir;
   delete fVerboseCmd;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TARMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
   if (command == fNumberOfHolesCmd) {
      fModule->SetNumberOfHoles(fNumberOfHolesCmd->GetNewIntValue(newValue));
   } else if (command == fHoleRadiusCmd) {
      fModule->SetHoleRadius(fHoleRadiusCmd->GetNewDoubleValue(newValue));
   } else if (command == fTGTGeometryIdCmd) {
      fModule->SetTGTGeometryId(fTGTGeometryIdCmd->GetNewIntValue(newValue));
   } else if (command== fVerboseCmd) {
      fModule->SetVerboseLevel(fVerboseCmd->GetNewIntValue(newValue));
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4String TARMessenger::GetCurrentValue(G4UIcommand* command)
{
   G4String cv;
   if (command == fNumberOfHolesCmd) {
      cv = fNumberOfHolesCmd->ConvertToString(fModule->GetNumberOfHoles());
      // } else if (command == fHoleRadiusCmd) {
      //    cv = fHoleRadiusCmd->ConvertToString(fModule->GetHoleRadus());
   } else if (command == fTGTGeometryIdCmd) {
      cv = fTGTGeometryIdCmd->ConvertToString(fModule->GetTGTGeometryId());
   }
   // if (command == fVerboseCmd) {
   //    cv = fVerboseCmd->ConvertToString(module->GetVerboseLevel());
   // }
   return cv;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
