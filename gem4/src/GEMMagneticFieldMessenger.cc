// $
//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
//
#include "GEMMagneticFieldMessenger.hh"
#include "GEMMagneticField.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4ios.hh"

GEMMagneticFieldMessenger::GEMMagneticFieldMessenger(GEMMagneticField * mpga)
   : target(mpga)
{
   // fieldCmd = new G4UIcmdWithADoubleAndUnit("/mag/fieldValue", this);
   // fieldCmd->SetGuidance("Field strength");
   // fieldCmd->SetParameterName("field", true);
   // fieldCmd->SetDefaultValue(1.);
   // fieldCmd->SetDefaultUnit("tesla");

   // magnetTypeCmd = new G4UIcmdWithAnInteger("/mag/magnetType", this);
   // magnetTypeCmd->SetGuidance("Magnet Type");
   // magnetTypeCmd->SetParameterName("magnetType", true);
   // magnetTypeCmd->SetDefaultValue(0);

   fieldTypeCmd = new G4UIcmdWithAnInteger("/mag/fieldType", this);
   fieldTypeCmd->SetGuidance("Field Type of COBRA field");
   fieldTypeCmd->SetParameterName("fieldType", true);
   fieldTypeCmd->SetDefaultValue(2);

   cobraScaleCmd = new G4UIcmdWithADouble("/mag/cobraScale", this);
   cobraScaleCmd->SetGuidance("Scale Factor for COBRA magnet");
   cobraScaleCmd->SetParameterName("cobraScale", true);
   cobraScaleCmd->SetDefaultValue(1.);

   btsScaleCmd = new G4UIcmdWithADouble("/mag/btsScale", this);
   btsScaleCmd->SetGuidance("Scale Factor for BTS magnet");
   btsScaleCmd->SetParameterName("btsScale", true);
   btsScaleCmd->SetDefaultValue(1.);

   bypassScaleCmd = new G4UIcmdWithADouble("/mag/bypassScale", this);
   bypassScaleCmd->SetGuidance("Scale Factor for Bypass magnet");
   bypassScaleCmd->SetParameterName("bypassScale", true);
   bypassScaleCmd->SetDefaultValue(0.);
}

GEMMagneticFieldMessenger::~GEMMagneticFieldMessenger()
{
   // delete fieldCmd;
   // delete magnetTypeCmd;
   delete fieldTypeCmd;
   delete cobraScaleCmd;
   delete btsScaleCmd;
   delete bypassScaleCmd;
}

void GEMMagneticFieldMessenger::SetNewValue(G4UIcommand * command, G4String newValue)
{
   // if (command == fieldCmd) {
   //    target->SetField(fieldCmd->GetNewDoubleValue(newValue));
   // } else if (command == magnetTypeCmd) {
   //    target->SetMagnetType(magnetTypeCmd->GetNewIntValue(newValue));
   // } else if (command == fieldTypeCmd) {
   if (command == fieldTypeCmd) {
      target->SetFieldType(fieldTypeCmd->GetNewIntValue(newValue));
   } else if (command == cobraScaleCmd) {
      target->SetCobraScale(cobraScaleCmd->GetNewDoubleValue(newValue));
   } else if (command == btsScaleCmd) {
      target->SetBTSScale(btsScaleCmd->GetNewDoubleValue(newValue));
   } else if (command == bypassScaleCmd) {
      target->SetBypassScale(bypassScaleCmd->GetNewDoubleValue(newValue));
   }
}

G4String GEMMagneticFieldMessenger::GetCurrentValue(G4UIcommand * command)
{
   G4String cv;
   // if (command == fieldCmd) {
   //    cv = fieldCmd->ConvertToString(target->GetField(), "tesla");
   // } else if (command == magnetTypeCmd) {
   //    cv = target->GetMagnetType();
   // } else if (command == fieldTypeCmd) {
   if (command == fieldTypeCmd) {
      cv = target->GetFieldType();
   } else if (command == cobraScaleCmd) {
      cv = target->GetCobraScale();
   } else if (command == btsScaleCmd) {
      cv = target->GetBTSScale();
   } else if (command == bypassScaleCmd) {
      cv = target->GetBypassScale();
   }

   return cv;
}

