//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "TSystem.h"
#include "Randomize.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "G4TransportationManager.hh"
#include "G4Navigator.hh"

#include "G4DecayTable.hh"
#include "G4PhaseSpaceDecayChannel.hh"
#include "GEMMuonBeamGenerator.hh"
#include "GEMPrimaryGeneratorAction.hh"
#include "GEMPrimaryGeneratorMessenger.hh"
#include "GEMMuonDecayChannelWithSpin.hh"
#include "GEMMuonRadiativeDecayChannelWithSpin.hh"

#include "GEMPhysicsList.hh"
#include "GEMConstants.hh"

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMMuonBeamGenerator::GEMMuonBeamGenerator()
   : generatorAction(0)
   , phaseSpacePool(0)
   , muonDecayMode(kDefaultMuonBeamMuonDecayMode)
   , muonPolarization(kDefaultMuonPolarization)
   , radiativeBR(kDefaultMuonRadiativeBranchingRatio)
   , xRangeMin(kDefaultXRangeMin)
   , xRangeMax(kDefaultXRangeMax)
   , yRangeMin(kDefaultYRangeMin)
   , yRangeMax(kDefaultYRangeMax)
   , zRangeMin(kDefaultZRangeMin)
   , zRangeMax(kDefaultZRangeMax)
   , phaseSpaceReadFromFile(kDefaultMuonBeamPhaseSpaceReadFromFile)
   , phaseSpaceFileName(kDefaultMuonBeamPhaseSpaceFileName)
   , phaseSpaceMeanX(kDefaultMuonBeamPhaseSpaceMeanX)
   , phaseSpaceSigmaX(kDefaultMuonBeamPhaseSpaceSigmaX)
   , phaseSpaceMeanXp(kDefaultMuonBeamPhaseSpaceMeanXp)
   , phaseSpaceSigmaXp(kDefaultMuonBeamPhaseSpaceSigmaXp)
   , phaseSpaceCorrX(kDefaultMuonBeamPhaseSpaceCorrX)
   , phaseSpaceMeanY(kDefaultMuonBeamPhaseSpaceMeanY)
   , phaseSpaceSigmaY(kDefaultMuonBeamPhaseSpaceSigmaY)
   , phaseSpaceMeanYp(kDefaultMuonBeamPhaseSpaceMeanYp)
   , phaseSpaceSigmaYp(kDefaultMuonBeamPhaseSpaceSigmaYp)
   , phaseSpaceCorrY(kDefaultMuonBeamPhaseSpaceCorrY)
   , phaseSpaceMeanZ(kDefaultMuonBeamPhaseSpaceMeanZ)
   , phaseSpaceSigmaZ(kDefaultMuonBeamPhaseSpaceSigmaZ)
   , phaseSpaceMomentumShape(kDefaultMuonBeamPhaseSpaceMomentumShape)
   , phaseSpaceMomentumMean(kDefaultMuonBeamPhaseSpaceMomentumMean)
   , phaseSpaceMomentumSigma(kDefaultMuonBeamPhaseSpaceMomentumSigma)
   , phaseSpaceMomentumWidth(kDefaultMuonBeamPhaseSpaceMomentumWidth)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMMuonBeamGenerator::GEMMuonBeamGenerator(GEMPrimaryGeneratorAction *act)
   : generatorAction(act)
   , phaseSpacePool(0)
   , muonDecayMode(kDefaultMuonBeamMuonDecayMode)
   , muonPolarization(kDefaultMuonPolarization)
   , radiativeBR(kDefaultMuonRadiativeBranchingRatio)
   , xRangeMin(kDefaultXRangeMin)
   , xRangeMax(kDefaultXRangeMax)
   , yRangeMin(kDefaultYRangeMin)
   , yRangeMax(kDefaultYRangeMax)
   , zRangeMin(kDefaultZRangeMin)
   , zRangeMax(kDefaultZRangeMax)
   , phaseSpaceReadFromFile(kDefaultMuonBeamPhaseSpaceReadFromFile)
   , phaseSpaceFileName(kDefaultMuonBeamPhaseSpaceFileName)
   , phaseSpaceMeanX(kDefaultMuonBeamPhaseSpaceMeanX)
   , phaseSpaceSigmaX(kDefaultMuonBeamPhaseSpaceSigmaX)
   , phaseSpaceMeanXp(kDefaultMuonBeamPhaseSpaceMeanXp)
   , phaseSpaceSigmaXp(kDefaultMuonBeamPhaseSpaceSigmaXp)
   , phaseSpaceCorrX(kDefaultMuonBeamPhaseSpaceCorrX)
   , phaseSpaceMeanY(kDefaultMuonBeamPhaseSpaceMeanY)
   , phaseSpaceSigmaY(kDefaultMuonBeamPhaseSpaceSigmaY)
   , phaseSpaceMeanYp(kDefaultMuonBeamPhaseSpaceMeanYp)
   , phaseSpaceSigmaYp(kDefaultMuonBeamPhaseSpaceSigmaYp)
   , phaseSpaceCorrY(kDefaultMuonBeamPhaseSpaceCorrY)
   , phaseSpaceMeanZ(kDefaultMuonBeamPhaseSpaceMeanZ)
   , phaseSpaceSigmaZ(kDefaultMuonBeamPhaseSpaceSigmaZ)
   , phaseSpaceMomentumShape(kDefaultMuonBeamPhaseSpaceMomentumShape)
   , phaseSpaceMomentumMean(kDefaultMuonBeamPhaseSpaceMomentumMean)
   , phaseSpaceMomentumSigma(kDefaultMuonBeamPhaseSpaceMomentumSigma)
   , phaseSpaceMomentumWidth(kDefaultMuonBeamPhaseSpaceMomentumWidth)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMMuonBeamGenerator::~GEMMuonBeamGenerator()
{
   generatorAction = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMMuonBeamGenerator::ReadPhaseSpaceFile(const char* filename)
{
   vector<G4double> phspinput(6);
   G4int i;
   if (!phaseSpacePool) {
      phaseSpacePool = new list<vector<G4double> > ;
   } else {
      phaseSpacePool->clear();
   }
   ifstream ifile(filename);
   if (!ifile.good()) {
      std::ostringstream o;
      o << "File not found : " << filename;
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }
   while (!ifile.eof()) {
      for (i = 0; i < 6; i++) {
         ifile >> phspinput[i];
      }
      if (!ifile.fail()) {
         phspinput[0] *= cm;
         phspinput[1] *= mrad;
         phspinput[2] *= cm;
         phspinput[3] *= mrad;
         phspinput[4] *= cm;
         phspinput[5] *= MeV;
         phaseSpacePool->push_back(phspinput);
      }
   }
   ifile.close();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMMuonBeamGenerator::GeneratePrimaries(G4Event *anEvent)
{
   vector<G4double> phspinput(6);
   G4double rn[6];
   G4double gausrn[6];

   G4int i;

   if (!phaseSpaceReadFromFile) { // parameterized phase space data
      for (i = 0; i < 6; i++) {
         rn[i] = G4UniformRand();
      }
      gausrn[0] = sqrt(-2 * log(rn[0])) * cos(twopi * rn[1]);
      gausrn[1] = sqrt(-2 * log(rn[0])) * sin(twopi * rn[1]);
      gausrn[2] = sqrt(-2 * log(rn[2])) * cos(twopi * rn[3]);
      gausrn[3] = sqrt(-2 * log(rn[2])) * sin(twopi * rn[3]);
      gausrn[4] = sqrt(-2 * log(rn[4])) * cos(twopi * rn[5]);
      gausrn[5] = sqrt(-2 * log(rn[4])) * sin(twopi * rn[5]);

      phspinput[0] = phaseSpaceMeanX  + phaseSpaceSigmaX * gausrn[0];
      phspinput[1] = phaseSpaceMeanXp + phaseSpaceCorrX * phaseSpaceSigmaXp * gausrn[0] +
                     sqrt((1 - phaseSpaceCorrX) * (1 + phaseSpaceCorrX)) * phaseSpaceSigmaXp * gausrn[1];

      phspinput[2] = phaseSpaceMeanY + phaseSpaceSigmaY * gausrn[2];
      phspinput[3] = phaseSpaceMeanYp + phaseSpaceCorrY * phaseSpaceSigmaYp * gausrn[2] +
                     sqrt((1 - phaseSpaceCorrY) * (1 + phaseSpaceCorrY)) * phaseSpaceSigmaYp * gausrn[3];

      phspinput[4] = phaseSpaceMeanZ + phaseSpaceSigmaZ * gausrn[4];

      if (phaseSpaceMomentumShape == MOMSHAPE_GAUSS) { // gaussian distribution
         phspinput[5] = phaseSpaceMomentumMean + phaseSpaceMomentumSigma * gausrn[5];
      } else { // truncated gaussian distribution
         while (1) {
            rn[0] = G4UniformRand();
            rn[1] = G4UniformRand();
            gausrn[0] = sqrt(-2 * log(rn[0])) * cos(twopi * rn[1]);
            gausrn[1] = sqrt(-2 * log(rn[0])) * sin(twopi * rn[1]);
            if (fabs(gausrn[0]) < phaseSpaceMomentumWidth) {
               phspinput[5] = phaseSpaceMomentumMean + phaseSpaceMomentumSigma * gausrn[0];
               break;
            } else if (fabs(gausrn[1]) < phaseSpaceMomentumWidth) {
               phspinput[5] = phaseSpaceMomentumMean + phaseSpaceMomentumSigma * gausrn[1];
               break;
            }
         }
      }
   } else { // read phase space data from file
      G4String CPHSPFNAM = phaseSpaceFileName;
      if (CPHSPFNAM == "") {
         CPHSPFNAM = gSystem->ExpandPathName("$(MEG2SYS)/meg2lib/common/beam/phasespace1full.dat");
      }
      if (!phaseSpacePool) {
         // this is the first time
         ReadPhaseSpaceFile(CPHSPFNAM);
      }
      if (phaseSpacePool->empty()) {
         G4cerr << __FILE__ << " " << __func__ << " " << __LINE__ << " " <<
                "Phase space pool exhausted. The same file is reused." << G4endl;
         ReadPhaseSpaceFile(CPHSPFNAM);
      }
      list<vector<G4double> >::iterator iter = phaseSpacePool->begin();
#if 0 /* choose a random entry */
      G4int idx = CLHEP::RandFlat::shootInt(phaseSpacePool->size());
      for (i = 0; i < idx; i++) {
         ++iter;
      }
#endif
      phspinput = (*iter);
      phaseSpacePool->erase(iter);
   }

   switch (muonDecayMode) {
   case MUDECAY_SM:
      if (GammaRequired()) {
         generatorAction->GetPhysicsList()->SetMuonDecayBR(0, 0, 1);
      } else {
         generatorAction->GetPhysicsList()->SetMuonDecayBR(0, (1 - radiativeBR), radiativeBR);
      }
      break;
   case MUDECAY_SIGNAL_EGAMMA:
      generatorAction->GetPhysicsList()->SetMuonDecayBR(1, 0, 0);
      break;
   default:
      std::ostringstream o;
      o << "Unknown muon decay mode : " << muonDecayMode;
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }

   G4ThreeVector pos(phspinput[0],
                     phspinput[2],
                     phspinput[4]);

   G4ParticleMomentum mom(phspinput[5] * tan(phspinput[1]) /
                          sqrt(1 +
                               tan(phspinput[1]) * tan(phspinput[1]) +
                               tan(phspinput[3]) * tan(phspinput[3])),

                          phspinput[5] * tan(phspinput[3]) /
                          sqrt(1 +
                               tan(phspinput[1]) * tan(phspinput[1]) +
                               tan(phspinput[3]) * tan(phspinput[3])),

                          phspinput[5] /
                          sqrt(1 +
                               tan(phspinput[1]) * tan(phspinput[1]) +
                               tan(phspinput[3]) * tan(phspinput[3])));

   G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
   G4ParticleGun *gun = generatorAction->GetParticleGun();
   gun->SetParticleDefinition(particleTable->FindParticle("mu+"));

   G4double mommag = mom.mag();
   G4double mass   = muon_mass_c2;
   G4double energy = std::sqrt(mommag * mommag + mass * mass) - mass;
   gun->SetParticleEnergy(energy);
   gun->SetParticleMomentumDirection(mom.unit());
   gun->SetParticlePosition(pos);

   // polarization
   // I don't know which actually works...,  should be investigated.
#if 1
   // set polarization of decay channels
   G4ThreeVector polar(0, 0, -1);
   if (G4UniformRand() > muonPolarization) {
      // rotate 4pi
      G4double rphi   = twopi * G4UniformRand() * rad;
      G4double rtheta = std::acos(2  * G4UniformRand() - 1);
      G4double rpsi   = twopi * G4UniformRand() * rad;
      G4RotationMatrix rot;
      rot.set(rphi, rtheta, rpsi);
      polar *= rot;
   }
   generatorAction->GetPhysicsList()->GetMuonChannelWithSpin()->SetPolarization(polar);
   generatorAction->GetPhysicsList()->GetMuonRadiativeChannelWithSpin()->SetPolarization(polar);
#else
   // set polarization of a generated muon
   G4ThreeVector polar = -(gun->GetParticleMomentumDirection());
   if (G4UniformRand() > muonPolarization) {
      // rotate 4pi
      G4double rphi   = twopi * G4UniformRand() * rad;
      G4double rtheta = std::acos(2  * G4UniformRand() - 1);
      G4double rpsi   = twopi * G4UniformRand() * rad;
      G4RotationMatrix rot;
      rot.set(rphi, rtheta, rpsi);
      polar *= rot;
   }
   gun->SetParticlePolarization(polar);
#endif

   if (muonDecayMode == MUDECAY_SM) {
      generatorAction->GetPhysicsList()->GetMuonChannelWithSpin()->
      SetRange(xRangeMin, xRangeMax);
      generatorAction->GetPhysicsList()->GetMuonRadiativeChannelWithSpin()->
      SetRange(xRangeMin, xRangeMax,
               yRangeMin, yRangeMax,
               zRangeMin, zRangeMax);
      static G4bool checked = false;
      if (!checked) { // assuming ranges are not changed in a process
         checked = true;
         G4bool rangeLimited =
            !GammaRequired() &&
            (
               xRangeMin > kDefaultXRangeMin + 1e-10 ||
               xRangeMax < kDefaultXRangeMax - 1e-10 ||
               // yRangeMin > kDefaultYRangeMin + 1e-10 ||
               // yRangeMax < kDefaultYRangeMax - 1e-10 ||
               // zRangeMin > kDefaultZRangeMin + 1e-10 ||
               // zRangeMax < kDefaultZRangeMax - 1e-10 ||
               0
            );

         if (rangeLimited) {
            // not suppored
            std::ostringstream o;
            o << "It seems the default radiative BR is used while positron energy is limited." << G4endl;
            o << "RMD BR may not be correct." << G4endl;
            o << "Solutions are one of followings," << G4endl;
            o << "  - adjust radiative BR for your range cuts" << G4endl;
            o << "  - use the full range of positron energy." << G4endl;
            o << "  - set the radiative BR to be 0 or 1." << G4endl;
            G4Exception(__func__, "", JustWarning, o.str().c_str());
         }
      }
   }

   gun->GeneratePrimaryVertex(anEvent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool GEMMuonBeamGenerator::GammaRequired()
{
   return
      yRangeMax < kDefaultYRangeMax - 100 * keV / (muon_mass_c2 / 2) ||
      yRangeMin > kDefaultYRangeMin + 100 * keV / (muon_mass_c2 / 2) ||
      zRangeMax < kDefaultZRangeMax - 1e-10 ||
      zRangeMin > kDefaultZRangeMin + 1e-10;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
