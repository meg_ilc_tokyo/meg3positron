#include "GEMUIsession.hh"

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUIsession::GEMUIsession()
   : G4UIsession()
   , outFile(0)
   , errFile(0)
   , outFileName("gem4.out")
   , errFileName("STDERR")
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMUIsession::~GEMUIsession()
{
   CloseOutFile();
   CloseErrFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4int GEMUIsession::ReceiveG4cout(const G4String &coutString)
{
   if (outFile) {
      *outFile << coutString << std::flush;
      return 0;
   } else {
      return G4UIsession::ReceiveG4cout(coutString);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int GEMUIsession::ReceiveG4cerr(const G4String &cerrString)
{
   if (errFile) {
      *errFile << cerrString << std::flush;
      return 0;
   } else {
      return G4UIsession::ReceiveG4cerr(cerrString);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMUIsession::OpenOutFile(const char* filename)
{
   G4bool append = false;
   if (filename && filename[0] != '\0') {
      outFileName = filename;
      if (outFileName.length() && outFileName[outFileName.length() - 1] == '+') {
         append = true;
         outFileName.erase(outFileName.length() - 1);
      }
   }
   if (outFileName == "STDOUT") {
      if (outFile) {
         delete outFile;
         outFile = 0;
      }
      return true;
   }

   G4bool outAndErrSameFile = false;
   if (outFileName == errFileName) {
      outAndErrSameFile = true;
   }
   if (outFile) {
      delete outFile;
      outFile = 0;
   }
   if (outAndErrSameFile && errFile) {
      G4cout << "stdout redirect to file " << errFileName << G4endl;
      outFile = errFile;
   } else if (outFileName.length()) {
      G4cout << "stdout redirect to file " << outFileName << G4endl;
      if (append) {
         outFile = new ofstream(outFileName, fstream::out | fstream::app);
      } else {
         outFile = new ofstream(outFileName);
      }
   }
   return (outFile && outFile->good());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMUIsession::CloseOutFile()
{
   G4bool outAndErrSameFile = (outFile == errFile);
   if (outAndErrSameFile) {
      if (outFile) {
         outFile->close();
         delete outFile;
         outFile = 0;
         errFile->close();
         delete errFile;
         errFile = 0;
      }
   } else if (outFile) {
      outFile->close();
      delete outFile;
      outFile = 0;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMUIsession::OpenErrFile(const char* filename)
{
   G4bool append = false;
   if (filename && filename[0] != '\0') {
      errFileName = filename;
      if (errFileName.length() && errFileName[errFileName.length() - 1] == '+') {
         append = true;
         errFileName.erase(errFileName.length() - 1);
      }
   }
   if (errFileName == "STDERR") {
      if (errFile) {
         delete errFile;
         errFile = 0;
      }
      return true;
   }

   G4bool outAndErrSameFile = false;
   if (errFileName == outFileName) {
      outAndErrSameFile = true;
   }
   if (errFile) {
      delete errFile;
      errFile = 0;
   }
   if (outAndErrSameFile && outFile) {
      G4cerr << "stderr redirect to file " << outFileName << G4endl;
      errFile = outFile;
   } else if (errFileName.length()) {
      G4cerr << "stderr redirect to file " << errFileName << G4endl;
      if (append) {
         errFile = new ofstream(errFileName, fstream::out | fstream::app);
      } else {
         errFile = new ofstream(errFileName);
      }
   }
   return (errFile && errFile->good());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMUIsession::CloseErrFile()
{
   G4bool outAndErrSameFile = (errFile == outFile);
   if (outAndErrSameFile) {
      if (errFile) {
         outFile->close();
         delete outFile;
         outFile = 0;
         errFile->close();
         delete errFile;
         errFile = 0;
      }
   } else {
      if (errFile) {
         errFile->close();
         delete errFile;
         errFile = 0;
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
