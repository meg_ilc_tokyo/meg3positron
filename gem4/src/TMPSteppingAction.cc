//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "TMPSteppingAction.hh"
#include "TMPEventAction.hh"
#include "TMPTrackingAction.hh"
#include "GEMUserTrackInformation.hh"
#include "GEMUserEventInformation.hh"
#include "TMPUserTrackInformation.hh"
#include "TMPUserEventInformation.hh"

#include "G4SteppingManager.hh"
#include "G4SDManager.hh"
#include "G4EventManager.hh"
#include "G4ProcessManager.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TMPSteppingAction::TMPSteppingAction()
   : idx(-1)
   , silicon(0)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

TMPSteppingAction::~TMPSteppingAction()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void TMPSteppingAction::UserSteppingAction(const G4Step *theStep)
{
   TMPUserEventInformation* eveInfo =
      dynamic_cast<TMPUserEventInformation*>(
         dynamic_cast<GEMUserEventInformation*>(
            G4EventManager::GetEventManager()->GetConstCurrentEvent()->
            GetUserInformation())->GetModuleAt(idx));

   TMPUserTrackInformation* trackInfo =
      dynamic_cast<TMPUserTrackInformation*>(
         dynamic_cast<GEMUserTrackInformation*>(
            theStep->GetTrack()->GetUserInformation())->GetModuleAt(idx));

   if (theStep->GetTrack()->GetMaterial() == silicon) {
      eveInfo->energyDeposit   += theStep->GetTotalEnergyDeposit();
      trackInfo->energyDeposit += theStep->GetTotalEnergyDeposit();
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
