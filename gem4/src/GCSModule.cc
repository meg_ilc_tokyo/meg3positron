//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_


#include "GCSModule.hh"
#include "GCSMessenger.hh"
#include "TString.h"

#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4Color.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4Element.hh"
#include "G4UserLimits.hh"
#include "G4SDManager.hh"

namespace
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void GCSModule::ConstructMaterials()
{

   // LYSO
   G4Element *elLu = G4NistManager::Instance()->FindOrBuildElement("Lu");
   G4Element *elY  = G4NistManager::Instance()->FindOrBuildElement("Y");
   G4Element *elSi = G4NistManager::Instance()->FindOrBuildElement("Si");
   G4Element *elO  = G4NistManager::Instance()->FindOrBuildElement("O");
   fLYSO = new G4Material("LYSO", 7.1*g/cm3, 4);
   fLYSO->AddElement(elLu, 9);
   fLYSO->AddElement(elY,  1);
   fLYSO->AddElement(elSi, 5);
   fLYSO->AddElement(elO,  25);

   // He gas
   G4Element *elHe = G4NistManager::Instance()->FindOrBuildElement("He");
   fHe = new G4Material("Helium", 1.66e-04*g/cm3, 1);
   fHe->AddElement(elHe, 1);

}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void GCSModule::Construct(G4LogicalVolume *parent)
{
   if (!materialConstructed) {
      ConstructMaterials();
      materialConstructed = true;
   }

   //////////////////////
   //    Gas volume    //
   //////////////////////

   // Construct Gas volume
   G4Tubs *GasSol = new G4Tubs("GasSol", GetGasRMin(), GetGasRMax(), GetGasLength(), 0.*deg, 360*deg);
   G4LogicalVolume *GasLog = new G4LogicalVolume(GasSol, fHe, "GasLog");
   GasLog->SetVisAttributes(G4Color(0, 1, 0, 0.3));
   new G4PVPlacement(0, G4ThreeVector(0,0,0), GasLog, "GasPhys", parent, false, 0);
   GasLog->SetUserLimits(new G4UserLimits(5.*mm));
   parent->SetUserLimits(new G4UserLimits(5.*mm));

   /////////////////////
   //    LYSO layer   //
   /////////////////////
   
   // Sensitive Detector
   G4SDManager* SDman = G4SDManager::GetSDMpointer();
   if (!fConverterSD) {
      fConverterSD = new GCSConverterSD("/gcs/converterSD");
      SDman->AddNewDetector(fConverterSD);
   }

   // Construct LYSO layers
   for (G4int iLayer=0; iLayer<GetNLYSOLayers(); iLayer++) {
      G4double r_min = GetLYSORadiusAt(iLayer);// LYSO_r[iLayer];
      G4double r_max = r_min + GetLYSOThickness();
      //G4double r_max = LYSO_r[iLayer]+LYSO_thickness;
      G4double phi_step = 360. * deg / GetLYSONModulesPerLayerAt(iLayer);//LYSO_n_modules[iLayer];
      G4double z_step = GetLYSOLengthAt(iLayer) / GetLYSONSegmentsInZAt(iLayer);//LYSO_length[iLayer] / LYSO_n_segments_z[iLayer];
      G4double phi_segment_step = phi_step / GetLYSONSegmentsPerModuleInPhiAt(iLayer);//LYSO_n_segments_per_module_phi[iLayer];
      G4double length = GetLYSOLengthAt(iLayer);


      // Module volume creation
      G4Tubs* moduleTub = new G4Tubs(Form("ModuleTub%d", iLayer), r_min, r_max, length/2., 0, phi_step);
      G4LogicalVolume* moduleLog = new G4LogicalVolume(moduleTub, fHe, Form("ModuleLog%d", iLayer));
      moduleLog->SetVisAttributes(false);

      // Place the segments in the module volume
      G4int segmentCopyNo = 0;
      for(G4int jSegment=0; jSegment<GetLYSONSegmentsInZAt(iLayer); jSegment++) {
         G4double z_segment = jSegment * z_step - length / 2. + z_step/2.;
         // Segment volume creation
         // Scale thickness by sin(theta)
         G4Tubs* segmentTub = new G4Tubs(Form("SegmentTub%d", iLayer), r_min, r_min + GetLYSOThickness() * sin(atan2(r_min, z_segment)), z_step/2, 0, phi_segment_step);
         G4LogicalVolume* segmentLog = new G4LogicalVolume(segmentTub, fLYSO, Form("SegmentLog%d", iLayer));
         segmentLog->SetVisAttributes(G4Color(1, 1, 0, 0.5));
         segmentLog->SetSensitiveDetector(fConverterSD);
         for(G4int iSegment=0; iSegment<GetLYSONSegmentsPerModuleInPhiAt(iLayer); iSegment++) {
            G4double phi_segment = iSegment * phi_segment_step;
            new G4PVPlacement(G4Transform3D(G4RotationMatrix().rotateZ(phi_segment), G4ThreeVector(0, 0, z_segment)),
                              segmentLog, Form("ConverterSegment%d_%d_%d", iLayer, iSegment, jSegment), moduleLog, false, segmentCopyNo++);
         }
      }

      // Layer volume creation
      G4Tubs* layerTub = new G4Tubs(Form("LayerTub%d", iLayer), r_min, r_max, length/2., 0, 360.*deg);
      G4LogicalVolume* layerLog = new G4LogicalVolume(layerTub, fLYSO, Form("LayerLog%d", iLayer));
      layerLog->SetVisAttributes(false);

      // Place the modules in the detector volume
      for(G4int iModule=0; iModule<GetLYSONModulesPerLayerAt(iLayer); iModule++) {
         G4double phi_module = iModule * phi_step;
         new G4PVPlacement(G4Transform3D(G4RotationMatrix().rotateZ(phi_module), G4ThreeVector(0, 0, 0)),
                           moduleLog, Form("ConverterModule%d_%d", iLayer, iModule), layerLog, false, iModule);
      }

      // Place the layer in the gas volume
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), layerLog, Form("ConverterLayer%d", iLayer), GasLog, false, iLayer);  
   }

   //////////////////////////////////////////////////
   //    Wire cells (just a gas region for now)    //
   //////////////////////////////////////////////////

   // Sensitive detector
   SDman = G4SDManager::GetSDMpointer();
   if (!fWireSD) {
      fWireSD = new GCSWireSD("/gcs/wireSD");
      SDman->AddNewDetector(fWireSD);
   }

   // Parameters for Wire cells
   G4VisAttributes *vis_attr = new G4VisAttributes();
   vis_attr->SetColor(G4Color(0, 1, 0));
   // vis_attr->SetColor(G4Color(0, 1, 0, 0.2));
   vis_attr->SetForceWireframe(true);

   // Step size limit
   G4double maxStep = 0.1 * mm;
   G4UserLimits* stepLimit = new G4UserLimits(maxStep);

   // Loop for LYSO layers
   for (G4int iLayer=0; iLayer<GetNLYSOLayers(); iLayer++) {

      // Loop for sub-layers of Wire cells
      for (G4int jLayer=0; jLayer<GetNSublayers(); jLayer++) {

         // Mother volume of this sub-layer
         G4double r_min = GetLYSORadiusAt(iLayer) + GetLYSOThickness() +  jLayer   *fCellDr;
         G4double r_max = GetLYSORadiusAt(iLayer) + GetLYSOThickness() + (jLayer+1)*fCellDr;
         // Assert that the sublayers do not grow into the the next LYSO layer
         if(iLayer < GetNLYSOLayers() -1) {
            if(r_max > GetLYSORadiusAt(iLayer+1)) {
               G4ExceptionDescription errmsg;
               errmsg << "Wire layer " << iLayer << ", sublayer" << jLayer  <<
                  " is larger than the next LYSO layer " << G4endl;
               G4Exception("GCSModule::Construct", "WireCells",
                           FatalException, errmsg);
            }
         }


         G4Tubs *CellSol_mother = new G4Tubs(Form("CellSol_mother%d_%d", iLayer, jLayer),
                                             r_min, r_max, GetLYSOLengthAt(iLayer), 0.*deg, 360*deg);
         G4LogicalVolume *CellLog_mother = new G4LogicalVolume(CellSol_mother, fHe,
                                                               Form("CellLog_mother%d_%d", iLayer, jLayer));
         new G4PVPlacement(0, G4ThreeVector(0,0,0), CellLog_mother, Form("PhysLog_mother%d_%d", iLayer, jLayer),
                           GasLog, false, 0);
         CellLog_mother->SetVisAttributes(G4VisAttributes::GetInvisible());

         // Single cell volume
         G4double Cell_dphi = 360 * deg / GetNCellPhiAt(iLayer);
         G4Tubs *CellSol_single = new G4Tubs(Form("CellSol%d_%d", iLayer, jLayer),
                                             r_min, r_max, GetLYSOLengthAt(iLayer), -Cell_dphi/2., Cell_dphi);
         G4LogicalVolume *CellLog_single = new G4LogicalVolume(CellSol_single, fHe,
                                                               Form("CellLog_single%d_%d", iLayer, jLayer));
         // CellLog_single->SetVisAttributes(G4Color(0, 1, 0, 0.2));
         CellLog_single->SetVisAttributes(vis_attr);

         // Place all cells in this sub-layer
         new G4PVReplica(Form("CellPhys%d_%d", iLayer, jLayer), CellLog_single, CellLog_mother, kPhi, 
                         GetNCellPhiAt(iLayer), Cell_dphi);

         // Set sensitive detector
         CellLog_single->SetSensitiveDetector(fWireSD);

         // Set step size limit
         CellLog_single->SetUserLimits(stepLimit);

      }
   }
   
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GCSModule::GCSModule(const char* n, G4int i)
   : GEMAbsModule(n)
   , idx(i)
   , materialConstructed(false)
   , fGasRMin(20*cm)
   , fGasRMax(50*cm)
   , fGasLength(108*cm)
   , fLYSOThickness(4*mm)
   , fNLYSOLayers(4)
   , fLYSORadius({20*cm, 27.5*cm, 35.0*cm, 42.5*cm})
   , fLYSOLength({63*cm, 78*cm, 93*cm, 108*cm})
   , fLYSONModules({8, 11, 14, 17})
   , fLYSONSegmentsPerModuleInPhi({16, 16, 16, 16})
   , fLYSONSegmentsInZ({21, 26, 31, 36})
   , fNCellPhi({120, 180, 240, 300})
   , fNSublayers(6)
   , fCellDr(1*cm)
{
   fRunAction      = 0;
   fEventAction    = 0;
   fStackingAction = 0;
   fSteppingAction = 0;
   fTrackingAction = 0;
  
   fDir = new G4UIdirectory("/gcs/");
   fDir->SetGuidance("Gamma Conversion Spectrometer system control");
   fMessenger = new GCSMessenger(this);

   fLYSORadius.resize(fNLYSOLayers);
   fLYSOLength.resize(fNLYSOLayers);
   fLYSONModules.resize(fNLYSOLayers);
   fLYSONSegmentsPerModuleInPhi.resize(fNLYSOLayers);
   fLYSONSegmentsInZ.resize(fNLYSOLayers);

   fNCellPhi.resize(fNLYSOLayers);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GCSModule::~GCSModule()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserEventInformation* GCSModule::MakeEventInformation(const G4Event* /*anEvent*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

GEMAbsUserTrackInformation* GCSModule::MakeTrackInformation(const G4Track* /*aTrack*/)
{
   return 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_


