//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"


#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"
#include "G4UserLimits.hh"
#include "G4SolidStore.hh"
#include "G4RunManager.hh"
#include "GEMDetectorConstruction.hh"
#include "GEMDetectorMessenger.hh"
#include "GEMPrimaryGeneratorAction.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "ROMESQLDataBase.h"
#include "ROMEString.h"
#include "ROMEStr2DArray.h"

#include "G4LogicalSkinSurface.hh"
#include "G4LogicalBorderSurface.hh"

#include "G4FieldManager.hh"
#include "G4RunManager.hh"
#include "G4TransportationManager.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4EqMagElectricField.hh"
#include "G4ClassicalRK4.hh"
#include "G4ChordFinder.hh"
#include "G4UniformMagField.hh"
#include "GEMMagneticField.hh"
#include "GEMConstants.hh"
#include "GEMPhysicsList.hh"
#include "MAGModule.hh"

namespace
{
//   G4Material *gMACONHE = 0;
G4double    gAirFrac = kDefaultAirFraction;
}

using namespace std;

G4String GEMDetectorConstruction::fgInputFileName = "";

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMDetectorConstruction::GEMDetectorConstruction(G4String inputFileName)
   : fModules(0)
   , fRunAct(0)
   , fEveAct(0)
   , fStaAct(0)
   , fTraAct(0)
   , fSteAct(0)
   , fMessenger(new GEMDetectorMessenger(this))
   , fMagneticField(nullptr)
   , fFieldMgr(0)
   , fExpHall_phys(0)
#ifdef GEM4_USE_GDML
   , fParser(new G4GDMLParser())
#endif
   , fCustomRunNumber(kDefaultCustomRunNumber)
   , fGeneratorAction(0)
   , fVerbose(0)
   , fSetField(0)   // No field
   , fMagnetType(0) // Uniform field
   , fMagneticFieldValue(2 * tesla)
   , fDB(nullptr)
   , fFGASRegion(nullptr)
{
   fgInputFileName = inputFileName;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMDetectorConstruction::~GEMDetectorConstruction()
{
   delete fMessenger;
   delete fMagneticField;
#ifdef GEM4_USE_GDML
   delete fParser;
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *GEMDetectorConstruction::Construct()
{

   G4Material *air    = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
   G4Material *helium = G4NistManager::Instance()->FindOrBuildMaterial("G4_He");
   // The experimental Hall
   const G4double expHallLen = 10 * meter;

   G4Box *expHall_box           = new G4Box("World", expHallLen, expHallLen, expHallLen);
   G4LogicalVolume *expHall_log = new G4LogicalVolume(expHall_box, air, "World", 0, 0, 0);
   fExpHall_phys                = new G4PVPlacement(0, G4ThreeVector(), expHall_log, "World", 0, false, 0);
   expHall_log->SetVisAttributes(G4VisAttributes::GetInvisible());

   // Composition of helium contaminated by air and ethane
   G4Material *maconhe = nullptr;
   static G4int conheCount = 1;
   if (gAirFrac <= 0) {
      //pure helium
      maconhe = helium;
   } else {
      //contamined helium
      string conheName = "HeliumAir";
      if (conheCount > 0) {
         conheName = TString::Format("%s%d", conheName.c_str(), conheCount++);
      }

      const G4double heliumDensity = 1.66322e-4 * g / cm3; // copy G4_He
      const G4double airDensity    = 1.20479e-3 * g / cm3; // copy G4_Air
      G4double wconhe[2] = {heliumDensity, airDensity};
      G4double mixweight = 0;
      wconhe[0] = (1 - gAirFrac) * wconhe[0];
      wconhe[1] = gAirFrac       * wconhe[1];
      for (G4int iel = 0; iel < 2; iel++) {
         mixweight += wconhe[iel];
      }
      G4double density = (1 - gAirFrac) * heliumDensity + gAirFrac * airDensity;
      maconhe = new G4Material(conheName.c_str(), density, 2);
      maconhe->AddMaterial(helium, wconhe[0] / mixweight);
      maconhe->AddMaterial(air,    wconhe[1] / mixweight);
   }

   for (auto &&module : fModules) {
      if (module) {
         module->SetMACONHE(maconhe);
      }
   }

   //
   //-- FGAS
   //
   G4LogicalVolume *fgas_log = nullptr;
   for (auto &&module : fModules) {
      if (module) {
         fgas_log = module->ConstructFGas();
         if (fgas_log) {
            break;
         }
      }
   }
   if (fgas_log) {
      CreateFGASRegion();
      fgas_log->SetRegion(fFGASRegion);
      fFGASRegion->AddRootLogicalVolume(fgas_log);
      SetFGASRegionCuts();
      fgas_log->SetVisAttributes(G4VisAttributes::GetInvisible());
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0),
                        fgas_log, "FGAS", expHall_log, false, 0);
   }

   // Target (base)

   G4ThreeVector v;
   // v.setX(kDefaultTargetSize[0]);
   // v.setY(kDefaultTargetSize[1]);
   // v.setZ(kDefaultTargetSize[2]);      
   // fGeneratorAction->SetTargetSize(v);
   v.setX(kDefaultTargetAngle[0]);
   v.setY(kDefaultTargetAngle[1]);
   v.setZ(kDefaultTargetAngle[2]);
   fGeneratorAction->SetTargetEulerAngles(v);
   fGeneratorAction->SetTargetAngle(v);
   
   // v.setX(kDefaultTargetPosition[0]);
   // v.setX(kDefaultTargetPosition[1]);
   // v.setX(kDefaultTargetPosition[2]);      
   // fGeneratorAction->SetTargetPosition(v);


   // Modules
   for (auto &&module : fModules) {
      if (module) {
         G4cout << module->GetName() << G4endl;
         if (module->IsInsideFGAS()) {
            module->Construct(fgas_log ? fgas_log : expHall_log);
         } else {
            module->Construct(expHall_log);
         }
      }
   }

   // Global Magnetic Field
   if (fSetField) {

      static G4bool fieldIsInitialized = false;

      G4TransportationManager *transportMgr
         = G4TransportationManager::GetTransportationManager();

      if (!fieldIsInitialized) {
         // call Magnetic field
         if (fMagnetType == 1) { // COBRA
            fMagneticField = new GEMMagneticField();
            static_cast<GEMMagneticField*>(fMagneticField)->InitMEGBField();
         } else { // Uniform
            fMagneticField = new G4UniformMagField(G4ThreeVector(0.0, 0.0, fMagneticFieldValue));
         }
         MAGModule::SetMagnetType(fMagnetType);
         
         // Set this field to the global field manager
         fFieldMgr = transportMgr->GetFieldManager();
         fFieldMgr->SetDetectorField(fMagneticField);
         fFieldMgr->CreateChordFinder(fMagneticField);
         G4double deltaValue        = 0.01 * mm;
         fFieldMgr->GetChordFinder()->SetDeltaChord(deltaValue);
         G4double deltaOneStep      = 0.001 * mm;
         fFieldMgr->SetAccuraciesWithDeltaOneStep(deltaOneStep);
         G4double deltaIntersection = 0.01 * mm;
         fFieldMgr->SetDeltaIntersection(deltaIntersection);
         fieldIsInitialized = true;
      }
   }

   if (fVerbose > 0) {
      G4cout << *(G4Material::GetMaterialTable()) << G4endl;
   }

   return fExpHall_phys;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMDetectorConstruction::WriteGDML(const char* filename)
{
   // WRITING GDML FILES
#ifdef GEM4_USE_GDML
   if (!filename || !fExpHall_phys) {
      return false;
   }
   // G4int depth=1;
   // fParser.AddModule(depth);
   fParser->Write(filename, fExpHall_phys /*,false*/);
   return true;
#else
   std::ostringstream o;
   o << "Your Geant4 is not build with the GDML option.";
   G4Exception(__func__, "", JustWarning, o.str().c_str());
   if (filename) {
      return true;
   }
   return true;
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMDetectorConstruction::ReadDatabase(const char* connection)
{
   fDB = new ROMESQLDataBase();
   G4int run = fCustomRunNumber;
   if (fDB->Init("db", "db", connection)) {

      for (auto &&module : fModules) {
         if (module) {
            module->ReadDatabase(fDB, run);
         }
      }
   } else {
      std::ostringstream o;
      o << "Failed to connect " << connection;
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMDetectorConstruction::DisconnectDatabase()
{
   if (fDB) {
      fDB->DisConnect();
      delete fDB;
      fDB = nullptr;
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMDetectorConstruction::UpdateGeometry()
{
   // Clean old geometry, if any
   G4GeometryManager::GetInstance()->OpenGeometry();
   G4PhysicalVolumeStore::GetInstance()->Clean();
   G4LogicalVolumeStore::GetInstance()->Clean();
   G4SolidStore::GetInstance()->Clean();
   G4LogicalSkinSurface::CleanSurfaceTable();
   G4LogicalBorderSurface::CleanSurfaceTable();

   G4VPhysicalVolume *world = Construct();
   G4RunManager::GetRunManager()->DefineWorldVolume(world);
   G4RunManager::GetRunManager()->GeometryHasBeenModified();
   //G4RunManager::GetRunManager()->SetGeometryToBeOptimized(true);
   //G4RunManager::GetRunManager()->PhysicsHasBeenModified();
   //G4RegionStore::GetInstance()->UpdateMaterialList(world);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMDetectorConstruction::FindModule(G4String name)
{
   G4bool found = false;
   for (auto &&module : fModules) {
      if (module) {
         found = (module->GetName() == name);
      }
      if (found) {
         break;
      }
   }
   return found;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void GEMDetectorConstruction::CreateFGASRegion()
{
   if (fFGASRegion) {
      delete fFGASRegion;
   }
   fFGASRegion = new G4Region("FGASRegion");
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMDetectorConstruction::SetFGASRegionCuts()
{
   // Set production range cut values for FGAS region
   G4ProductionCuts * cuts = new G4ProductionCuts;
   G4double precision = 2.;
   G4double cut = G4RunManager::GetRunManager()->GetUserPhysicsList()->GetDefaultCutValue();
   cuts->SetProductionCut(cut / precision, G4ProductionCuts::GetIndex("gamma"));
   cuts->SetProductionCut(cut / precision, G4ProductionCuts::GetIndex("e-"));
   cuts->SetProductionCut(cut / precision, G4ProductionCuts::GetIndex("e+"));
   cuts->SetProductionCut(cut / precision, G4ProductionCuts::GetIndex("proton"));
   fFGASRegion->SetProductionCuts(cuts);
   fFGASRegion->SetUserLimits(new G4UserLimits(0.2*cm)); // temporary hard coded
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
