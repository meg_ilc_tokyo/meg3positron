//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "GEMPhysicsListMessenger.hh"

#include "GEMPhysicsList.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPhysicsListMessenger::GEMPhysicsListMessenger(GEMPhysicsList* pPhys)
   : fPhysicsList(pPhys)
{
   fPhysDir = new G4UIdirectory("/gem/phys/");
   fPhysDir->SetGuidance("PhysicsList control");

   fVerboseCmd = new G4UIcmdWithAnInteger("/gem/phys/verbose", this);
   fVerboseCmd->SetGuidance("Set verbose for physics processes");
   fVerboseCmd->SetParameterName("verbose", true);
   fVerboseCmd->SetDefaultValue(1);
   fVerboseCmd->SetRange("verbose>=0");
   fVerboseCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

   fAddPhysicsListCmd = new G4UIcmdWithAString("/gem/phys/addPhysics", this);
   fAddPhysicsListCmd->SetGuidance("Add modula physics list.");
   fAddPhysicsListCmd->SetParameterName("PList", false);
   fAddPhysicsListCmd->AvailableForStates(G4State_PreInit);

   fAddPAICmd = new G4UIcmdWithoutParameter("/gem/phys/addPAI", this);
   fAddPAICmd->SetGuidance("Add PAI model");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPhysicsListMessenger::~GEMPhysicsListMessenger()
{
   delete fVerboseCmd;
   delete fAddPhysicsListCmd;
   delete fAddPAICmd;
   delete fPhysDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsListMessenger::SetNewValue(G4UIcommand* command,
                                          G4String newValue)
{
   if (command == fVerboseCmd) {
      fPhysicsList->SetVerbose(fVerboseCmd->GetNewIntValue(newValue));
   } else if (command == fAddPhysicsListCmd) {
      fPhysicsList->AddPhysicsList(newValue);
   } else if (command == fAddPAICmd) {
      fPhysicsList->AddPAIModel("pai");
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
