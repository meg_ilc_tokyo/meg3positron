//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

#include "GEMDetectorMessenger.hh"
#include "GEMDetectorConstruction.hh"
#include "GEMRunAction.hh"
#include "GEMEventAction.hh"
#include "GEMStackingAction.hh"
#include "GEMTrackingAction.hh"
#include "GEMSteppingAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMDetectorMessenger::GEMDetectorMessenger(GEMDetectorConstruction* det)
   : fDetector(det)
{
   fGeomDir = new G4UIdirectory("/gem/geom/");
   fGeomDir->SetGuidance("Commands for geometry");

   fVisDir = new G4UIdirectory("/gem/vis/");
   fVisDir->SetGuidance("Commands for visualization");

   fUpdateCmd = new G4UIcmdWithoutParameter("/gem/geom/update", this);
   fUpdateCmd->SetGuidance("Update geometry.");
   fUpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
   fUpdateCmd->SetGuidance("if you changed geometrical value(s).");
   fUpdateCmd->AvailableForStates(G4State_Idle);

   fWriteGDMLCmd = new G4UIcmdWithAString("/gem/geom/writeGDML", this);
   fWriteGDMLCmd->SetGuidance("Export the geometry to a GDML file");
   fWriteGDMLCmd->SetParameterName("name", false);

   fCustomRunNumberCmd = new G4UIcmdWithAnInteger("/gem/customRunNumber", this);
   fCustomRunNumberCmd->SetGuidance("Set the custom run number used to read the database");
   fCustomRunNumberCmd->SetParameterName("run", false);
   fCustomRunNumberCmd->AvailableForStates(G4State_PreInit);

   fReadDatabaseCmd = new G4UIcmdWithAString("/gem/readDatabase", this);
   fReadDatabaseCmd->SetGuidance("Read parameters from a database");
   fReadDatabaseCmd->SetParameterName("name", false);
   fReadDatabaseCmd->AvailableForStates(G4State_PreInit);

   fDisconnectDatabaseCmd = new G4UIcmdWithoutParameter("/gem/disconnectDatabase", this);
   fDisconnectDatabaseCmd->SetGuidance("Disconnect database.");
   fDisconnectDatabaseCmd->AvailableForStates(G4State_Idle);

   fDetectorVerboseCmd = new G4UIcmdWithAnInteger("/gem/geom/verbose", this);
   fDetectorVerboseCmd->SetGuidance("Set the verbose level of the detector construction");
   fDetectorVerboseCmd->SetParameterName("verbose", false);

   fUseGEMTrajectoryDrawingCmd = new G4UIcmdWithABool("/gem/vis/UseGEMTrajectoryDrawing", this);
   fUseGEMTrajectoryDrawingCmd->SetGuidance("A flag to use GEM4 dedicated trajectory drawing.");
   fUseGEMTrajectoryDrawingCmd->SetParameterName("use", false);

   fDrawOpticalPhotonsCmd = new G4UIcmdWithABool("/gem/vis/DrawOpticalPhotons", this);
   fDrawOpticalPhotonsCmd->SetGuidance("A flag to draw of optical photons.");
   fDrawOpticalPhotonsCmd->SetParameterName("draw", false);

   fDrawEnergyMinCmd = new G4UIcmdWithADoubleAndUnit("/gem/vis/DrawEnergyMin", this);
   fDrawEnergyMinCmd->SetGuidance("Set minimum kinetic energy of a trajectory to draw");
   fDrawEnergyMinCmd->SetParameterName("min", false);
   fDrawEnergyMinCmd->SetUnitCategory("Energy");

   fSetMagneticFieldCmd = new G4UIcmdWithABool("/gem/SetMagneticField", this);
   fSetMagneticFieldCmd->SetGuidance("A flag to set magnetic field");
   fSetMagneticFieldCmd->SetParameterName("setfield", true);

   fMagnetTypeCmd = new G4UIcmdWithAnInteger("/mag/magnetType", this);
   fMagnetTypeCmd->SetGuidance("Magnet Type: 0 Unifrom solenoid, 1: COBRA");
   fMagnetTypeCmd->SetParameterName("magnetType", true);
   fMagnetTypeCmd->SetDefaultValue(0);

   fMagneticFieldValueCmd = new G4UIcmdWithADoubleAndUnit("/mag/fieldValue", this);
   fMagneticFieldValueCmd->SetGuidance("Field strength for the uniform field");
   fMagneticFieldValueCmd->SetParameterName("field", true);
   fMagneticFieldValueCmd->SetDefaultValue(2.);
   fMagneticFieldValueCmd->SetDefaultUnit("tesla");

   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMDetectorMessenger::~GEMDetectorMessenger()
{
   delete fUpdateCmd;
   delete fWriteGDMLCmd;
   delete fCustomRunNumberCmd;
   delete fReadDatabaseCmd;
   delete fDisconnectDatabaseCmd;
   delete fDetectorVerboseCmd;
   delete fUseGEMTrajectoryDrawingCmd;
   delete fDrawOpticalPhotonsCmd;
   delete fDrawEnergyMinCmd;
   delete fSetMagneticFieldCmd;
   delete fGeomDir;
   delete fVisDir;
   delete fMagnetTypeCmd;
   delete fMagneticFieldValueCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMDetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
   GEMEventAction *eveAct = nullptr;

   if (command == fUpdateCmd) {
      fDetector->UpdateGeometry();

   } else if (command == fWriteGDMLCmd) {
      fDetector->WriteGDML(newValue);

   } else if (command == fCustomRunNumberCmd) {
      fDetector->SetCustomRunNumber(fCustomRunNumberCmd->GetNewIntValue(newValue));

   } else if (command == fReadDatabaseCmd) {
      fDetector->ReadDatabase(newValue);

   } else if (command == fDisconnectDatabaseCmd) {
      fDetector->DisconnectDatabase();

   } else if (command == fDetectorVerboseCmd) {
      fDetector->SetVerbose(fDetectorVerboseCmd->GetNewIntValue(newValue));

   } else if (command == fUseGEMTrajectoryDrawingCmd &&
              (eveAct = dynamic_cast<GEMEventAction*>(fDetector->GetEventAction()))) {
      eveAct->SetUseGEMTrajectoryDrawing(fUseGEMTrajectoryDrawingCmd->GetNewBoolValue(newValue));

   } else if (command == fDrawOpticalPhotonsCmd &&
              (eveAct = dynamic_cast<GEMEventAction*>(fDetector->GetEventAction()))) {
      eveAct->SetDrawOpticalPhotons(fDrawOpticalPhotonsCmd->GetNewBoolValue(newValue));

   } else if (command == fDrawEnergyMinCmd &&
              (eveAct = dynamic_cast<GEMEventAction*>(fDetector->GetEventAction()))) {
      eveAct->SetDrawEnergyMin(fDrawEnergyMinCmd->GetNewDoubleValue(newValue));

   } else if (command == fSetMagneticFieldCmd) {
      fDetector->SetBField(fSetMagneticFieldCmd->GetNewBoolValue(newValue));

   } else if (command == fMagneticFieldValueCmd) {
      fDetector->SetMagneticFieldValue(fMagneticFieldValueCmd->GetNewDoubleValue(newValue));
      
   } else if (command == fMagnetTypeCmd) {
      fDetector->SetMagnetType(fMagnetTypeCmd->GetNewIntValue(newValue));
      
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
