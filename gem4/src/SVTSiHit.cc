//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "G4SystemOfUnits.hh"

#include "SVTSiHit.hh"
#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4Allocator<SVTSiHit> SVTSiHitAllocator;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiHit::SVTSiHit()
:G4VHit()
, fIndex(-1)
, fTrackID(-1)
, fParticleID(-1)
, fChipID(-1)
, fTime(0)
, fXYZPre(G4ThreeVector())
, fXYZPost(G4ThreeVector())
, fMomentum(G4ThreeVector())
, fEloss(0)
, fStepLength(0)
{
}


//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiHit::~SVTSiHit()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiHit::SVTSiHit(const SVTSiHit &right)
:G4VHit()
, fIndex(right.fIndex)
, fTrackID(right.fTrackID)
, fParticleID(right.fParticleID)
, fChipID(right.fChipID)
, fTime(right.fTime)
, fXYZPre(right.fXYZPre)
, fXYZPost(right.fXYZPost)
, fMomentum(right.fMomentum)
, fEloss(right.fEloss)
, fStepLength(right.fStepLength)
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

const SVTSiHit& SVTSiHit::operator=(const SVTSiHit &right)
{
   fIndex = right.fIndex;
   fTrackID = right.fTrackID;
   fParticleID = right.fParticleID;
   fChipID = right.fChipID;
   fTime = right.fTime;
   fXYZPre = right.fXYZPre;
   fXYZPost = right.fXYZPost;
   fMomentum = right.fMomentum;
   fEloss = right.fEloss;
   fStepLength = right.fStepLength;
   return *this;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4int SVTSiHit::operator==(const SVTSiHit&) const
{
   return false;
   //returns false because there currently isnt need to check for equality yet
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiHit::Draw()
{
   // Draw each hit as a circle
   G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
   if (pVVisManager) {
      G4Circle circle(fXYZPre);
      circle.SetScreenSize(4);
      circle.SetFillStyle(G4Circle::filled);
      G4Colour colour(1.0, 0.0, 0.0); //red
      circle.SetVisAttributes(G4VisAttributes(colour));
      pVVisManager->Draw(circle);
   }
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiHit::Print()
{

   G4cout<<"Hit in SVT "<<fChipID<<" "<<fEloss/keV<<" keV, "<<fTime/ns<<" ns"
         <<G4endl;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
