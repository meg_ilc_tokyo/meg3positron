//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "GEMPrimaryGeneratorMessenger.hh"

#include "GEMPrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"

#include "GEMConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPrimaryGeneratorMessenger::GEMPrimaryGeneratorMessenger(GEMPrimaryGeneratorAction* GEMGun)
   : GEMAction(GEMGun)
   , targetSizeCmd(nullptr)
   , targetAngleCmd(nullptr)
   , targetPositionCmd(nullptr)
   , targetMaterialCmd(nullptr)
{
   targetDir = new G4UIdirectory("/target/");
   targetDir->SetGuidance("Target control");

   genDir = new G4UIdirectory("/gem/generation/");
   genDir->SetGuidance("Event generator control");

   muonDir = new G4UIdirectory("/gem/generation/muon/");
   muonDir->SetGuidance("Muon decay control");

   radiativeDir = new G4UIdirectory("/gem/generation/muon/radiative/");
   radiativeDir->SetGuidance("Muon radiative decay control");

   stoppedMuonDir = new G4UIdirectory("/gem/generation/stoppedMuon/");
   stoppedMuonDir->SetGuidance("Stopped muon control");

   muonBeamDir = new G4UIdirectory("/gem/generation/muonBeam/");
   muonBeamDir->SetGuidance("Muon beam control");

   muonBeamPhaseSpaceDir = new G4UIdirectory("/gem/generation/muonBeam/phaseSpace/");
   muonBeamPhaseSpaceDir->SetGuidance("Muon beam phase space control");

   listCmd = new G4UIcmdWithoutParameter("/gem/generation/list", this);
   listCmd->SetGuidance("List available event generation types");

   evtypeCmd = new G4UIcmdWithAnInteger("/gem/generation/eventtype", this);
   evtypeCmd->SetGuidance("Set event generation type");
   evtypeCmd->SetParameterName("evtype", true);
   evtypeCmd->SetDefaultValue(1);
   evtypeCmd->AvailableForStates(G4State_Idle);

   fTGTGeometryIdCmd = new G4UIcmdWithAnInteger("/gem/customID/TGTGeometryId", this);
   fTGTGeometryIdCmd->SetGuidance("Set the custom TGTGeometry_id used to read the database");
   fTGTGeometryIdCmd->SetParameterName("id", false);
   fTGTGeometryIdCmd->AvailableForStates(G4State_PreInit);

   stoppedMuonCenterXCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/centerX", this);
   stoppedMuonCenterXCmd->SetGuidance("Set mean stopping center, x");
   stoppedMuonCenterXCmd->SetParameterName("center", false);
   stoppedMuonCenterXCmd->SetUnitCategory("Length");

   stoppedMuonSigmaXCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/sigmaX", this);
   stoppedMuonSigmaXCmd->SetGuidance("Set mean stopping sigma, x");
   stoppedMuonSigmaXCmd->SetParameterName("sigma", false);
   stoppedMuonSigmaXCmd->SetUnitCategory("Length");

   stoppedMuonCenterYCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/centerY", this);
   stoppedMuonCenterYCmd->SetGuidance("Set mean stopping center, y");
   stoppedMuonCenterYCmd->SetParameterName("center", false);
   stoppedMuonCenterYCmd->SetUnitCategory("Length");

   stoppedMuonSigmaYCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/sigmaY", this);
   stoppedMuonSigmaYCmd->SetGuidance("Set mean stopping sigma, y");
   stoppedMuonSigmaYCmd->SetParameterName("sigma", false);
   stoppedMuonSigmaYCmd->SetUnitCategory("Length");

   stoppedMuonDepthCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/depth", this);
   stoppedMuonDepthCmd->SetGuidance("Set mean stopping depth of incident muon");
   stoppedMuonDepthCmd->SetParameterName("depth", false);
   stoppedMuonDepthCmd->SetUnitCategory("Length");

   stoppedMuonDepthSigmaCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/depthSigma", this);
   stoppedMuonDepthSigmaCmd->SetGuidance("Set sigma of stopping depth");
   stoppedMuonDepthSigmaCmd->SetParameterName("sigma", false);
   stoppedMuonDepthSigmaCmd->SetUnitCategory("Length");

   stoppedMuonMaximizeGammaAngleCmd = new
   G4UIcmdWithoutParameter("/gem/generation/stoppedMuon/maximizeGammaAngle", this);
   stoppedMuonMaximizeGammaAngleCmd->SetGuidance("Set gamma generation range maximum");

   stoppedMuonMaximizePositronAngleCmd = new
   G4UIcmdWithoutParameter("/gem/generation/stoppedMuon/maximizePositronAngle", this);
   stoppedMuonMaximizePositronAngleCmd->SetGuidance("Set positron generation range maximum");

   stoppedMuonInterestRangeCmd = new G4UIcmdWithoutParameter("/gem/generation/stoppedMuon/interestRange", this);
   stoppedMuonInterestRangeCmd->SetGuidance("Set generation ranage to be \"inteterest range\"");
   stoppedMuonInterestRangeCmd->SetGuidance("    0.0    < ABS(COS(THETA)) < 0.35 &");
   stoppedMuonInterestRangeCmd->SetGuidance("   -1/3 PI <       PHI       < 1/3 PI");

   stoppedMuonExtendedRangeCmd = new G4UIcmdWithoutParameter("/gem/generation/stoppedMuon/extendedRange", this);
   stoppedMuonExtendedRangeCmd->SetGuidance("Set generation ranage to be \"inteterest range\"");
   stoppedMuonExtendedRangeCmd->SetGuidance("    0.0     < ABS(COS(THETA)) < 0.45 &");
   stoppedMuonExtendedRangeCmd->SetGuidance("   -7/18 PI <       PHI       < 7/18 PI");

   stoppedMuonCosThetaGammaCenterCmd = new G4UIcmdWithADouble("/gem/generation/stoppedMuon/cosThetaGammaCenter",
                                                              this);
   stoppedMuonCosThetaGammaCenterCmd->SetGuidance("Set center of cosThetaGamma");
   stoppedMuonCosThetaGammaCenterCmd->SetParameterName("center", false);

   stoppedMuonCosThetaGammaWidthCmd = new G4UIcmdWithADouble("/gem/generation/stoppedMuon/cosThetaGammaWidth",
                                                             this);
   stoppedMuonCosThetaGammaWidthCmd->SetGuidance("Set width of cosThetaGamma");
   stoppedMuonCosThetaGammaWidthCmd->SetParameterName("width", false);

   stoppedMuonPhiGammaCenterCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/phiGammaCenter",
                                                                this);
   stoppedMuonPhiGammaCenterCmd->SetGuidance("Set center of phiGamma");
   stoppedMuonPhiGammaCenterCmd->SetParameterName("center", false);
   stoppedMuonPhiGammaCenterCmd->SetUnitCategory("Angle");

   stoppedMuonPhiGammaWidthCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/phiGammaWidth",
                                                               this);
   stoppedMuonPhiGammaWidthCmd->SetGuidance("Set width of phiGamma");
   stoppedMuonPhiGammaWidthCmd->SetParameterName("width", false);
   stoppedMuonPhiGammaWidthCmd->SetUnitCategory("Angle");

   stoppedMuonCosThetaPositronCenterCmd = new
   G4UIcmdWithADouble("/gem/generation/stoppedMuon/cosThetaPositronCenter", this);
   stoppedMuonCosThetaPositronCenterCmd->SetGuidance("Set center of cosThetaPositron");
   stoppedMuonCosThetaPositronCenterCmd->SetParameterName("center", false);

   stoppedMuonCosThetaPositronWidthCmd = new
   G4UIcmdWithADouble("/gem/generation/stoppedMuon/cosThetaPositronWidth", this);
   stoppedMuonCosThetaPositronWidthCmd->SetGuidance("Set width of cosThetaPositron");
   stoppedMuonCosThetaPositronWidthCmd->SetParameterName("width", false);

   stoppedMuonPhiPositronCenterCmd = new
   G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/phiPositronCenter", this);
   stoppedMuonPhiPositronCenterCmd->SetGuidance("Set center of phiPositron");
   stoppedMuonPhiPositronCenterCmd->SetParameterName("center", false);
   stoppedMuonPhiPositronCenterCmd->SetUnitCategory("Angle");

   stoppedMuonPhiPositronWidthCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/stoppedMuon/phiPositronWidth",
                                                                  this);
   stoppedMuonPhiPositronWidthCmd->SetGuidance("Set width of phiPositron");
   stoppedMuonPhiPositronWidthCmd->SetParameterName("width", false);
   stoppedMuonPhiPositronWidthCmd->SetUnitCategory("Angle");

   stoppedMuonXRangeMinCmd = new G4UIcmdWithADouble("/gem/generation/muon/xRangeMin", this);
   stoppedMuonXRangeMinCmd->SetGuidance("Set minimum x (Positron energy / mu_mass * 2)");
   stoppedMuonXRangeMinCmd->SetParameterName("xmin", false);

   stoppedMuonXRangeMaxCmd = new G4UIcmdWithADouble("/gem/generation/muon/xRangeMax", this);
   stoppedMuonXRangeMaxCmd->SetGuidance("Set maximum x (Positron energy / mu_mass * 2)");
   stoppedMuonXRangeMaxCmd->SetParameterName("xmax", false);

   stoppedMuonYRangeMinCmd = new G4UIcmdWithADouble("/gem/generation/muon/yRangeMin", this);
   stoppedMuonYRangeMinCmd->SetGuidance("Set minimum y (Gamma energy / mu_mass * 2)");
   stoppedMuonYRangeMinCmd->SetParameterName("ymin", false);

   stoppedMuonYRangeMaxCmd = new G4UIcmdWithADouble("/gem/generation/muon/yRangeMax", this);
   stoppedMuonYRangeMaxCmd->SetGuidance("Set maximum y (Gamma energy / mu_mass * 2)");
   stoppedMuonYRangeMaxCmd->SetParameterName("ymax", false);

   stoppedMuonZRangeMinCmd = new G4UIcmdWithADouble("/gem/generation/muon/zRangeMin", this);
   stoppedMuonZRangeMinCmd->SetGuidance("Set minimum z (cosine opening angle)");
   stoppedMuonZRangeMinCmd->SetParameterName("zmin", false);

   stoppedMuonZRangeMaxCmd = new G4UIcmdWithADouble("/gem/generation/muon/zRangeMax", this);
   stoppedMuonZRangeMaxCmd->SetGuidance("Set maximum z (cosine opening angle)");
   stoppedMuonZRangeMaxCmd->SetParameterName("zmax", false);

   muonPolarizationCmd = new G4UIcmdWithADouble("/gem/generation/muon/polarization", this);
   muonPolarizationCmd->SetGuidance("Set muon polarization [0, 1]");
   muonPolarizationCmd->SetParameterName("polar", false);

   muonRadiativeBranchingRatioCmd = new G4UIcmdWithADouble("/gem/generation/muon/radiativeBR", this);
   muonRadiativeBranchingRatioCmd->SetGuidance("Set branching ratio of RMD in specified cuts.");
   muonRadiativeBranchingRatioCmd->SetParameterName("br", false);

   muonRadiativeMaxFuncValueCmd = new G4UIcmdWithADouble("/gem/generation/muon/radiative/maxFuncValue", this);
   muonRadiativeMaxFuncValueCmd->SetGuidance("Set maximum value of decay probability function.");
   muonRadiativeMaxFuncValueCmd->SetParameterName("val", false);

   muonRadiativeScanNBinCmd = new G4UIcmdWithAnInteger("/gem/generation/muon/radiative/scanNBin", this);
   muonRadiativeScanNBinCmd->SetGuidance("Set number of bins to scan radiative decay probability function.");
   muonRadiativeScanNBinCmd->SetParameterName("nbin", false);

   muonRadiativeGammaSpectrumLowerEdgeCmd = new
   G4UIcmdWithADoubleAndUnit("/gem/generation/muon/radiative/gammaSpectrumLowerEdge", this);
   muonRadiativeGammaSpectrumLowerEdgeCmd->SetGuidance("Set the lower edge of the gamma spectrum used for the generation.");
   muonRadiativeGammaSpectrumLowerEdgeCmd->SetGuidance("radiativeBR should be adjusted when this is changed.");
   muonRadiativeGammaSpectrumLowerEdgeCmd->SetParameterName("th", false);
   muonRadiativeGammaSpectrumLowerEdgeCmd->SetUnitCategory("Energy");

   muonRadiativeSafetyFactorCmd = new G4UIcmdWithADouble("/gem/generation/muon/radiative/safetyFactor", this);
   muonRadiativeSafetyFactorCmd->SetGuidance("Set the safety factor in RMD generation.");
   muonRadiativeSafetyFactorCmd->SetParameterName("factor", false);

   if (GEMAction->GetModule("TAR") || GEMAction->GetModule("ATAR")) {
      targetSizeCmd = new G4UIcmdWith3VectorAndUnit("/target/geom/size", this);
      targetSizeCmd->SetGuidance("Set ellipse parameters of the target, (diameter1,diameter2,dz)");
      targetSizeCmd->SetParameterName("r0", "r1", "dz", false);
      targetSizeCmd->SetUnitCategory("Length");

      targetAngleCmd = new G4UIcmdWith3VectorAndUnit("/target/geom/angle", this);
      targetAngleCmd->SetGuidance("Set slant angle Theta, Phi, Psi (Euler angles - 90deg)");
      targetAngleCmd->SetParameterName("theta", "phi", "psi", false);
      targetAngleCmd->SetUnitCategory("Angle");

      targetPositionCmd = new G4UIcmdWith3VectorAndUnit("/target/geom/position", this);
      targetPositionCmd->SetGuidance("Set X, Y, and Z position of the target");
      targetPositionCmd->SetParameterName("x", "y", "z", false);
      targetPositionCmd->SetUnitCategory("Length");

      targetMaterialCmd = new G4UIcmdWithAnInteger("/target/geom/material", this);
      targetMaterialCmd->SetGuidance("Set material, 0: Polyethylene and polyester sandwitch, 1: Scintillator, 2: Polyehtylene, 3: Silicon");
      targetMaterialCmd->SetParameterName("material", false);
   }

   muonBeamMuonDecayModeCmd = new G4UIcmdWithAnInteger("/gem/generation/muonBeam/decayChannel", this);
   muonBeamMuonDecayModeCmd->SetGuidance("Set muon decay mode. 0:SM, 1:Muegamma");
   muonBeamMuonDecayModeCmd->SetParameterName("decay", false);

   muonBeamPhaseSpaceReadFromFileCmd = new G4UIcmdWithABool("/gem/generation/muonBeam/phaseSpace/readFromFile",
                                                            this);
   muonBeamPhaseSpaceReadFromFileCmd->SetGuidance("Set flag to read phase space from a file");
   muonBeamPhaseSpaceReadFromFileCmd->SetParameterName("read", false);

   muonBeamPhaseSpaceFileNameCmd = new G4UIcmdWithAString("/gem/generation/muonBeam/phaseSpace/fileName", this);
   muonBeamPhaseSpaceFileNameCmd->SetGuidance("Set filename to read phase space");
   muonBeamPhaseSpaceFileNameCmd->SetParameterName("name", false);

   muonBeamPhaseSpaceMeanXCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/meanX", this);
   muonBeamPhaseSpaceMeanXCmd->SetGuidance("Set mean of x distribution");
   muonBeamPhaseSpaceMeanXCmd->SetParameterName("mean", false);
   muonBeamPhaseSpaceMeanXCmd->SetUnitCategory("Length");

   muonBeamPhaseSpaceSigmaXCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/sigmaX",
                                                               this);
   muonBeamPhaseSpaceSigmaXCmd->SetGuidance("Set sigma of x distribution");
   muonBeamPhaseSpaceSigmaXCmd->SetParameterName("sigma", false);
   muonBeamPhaseSpaceSigmaXCmd->SetUnitCategory("Length");

   muonBeamPhaseSpaceMeanXpCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/meanXp",
                                                               this);
   muonBeamPhaseSpaceMeanXpCmd->SetGuidance("Set mean of x' distribution");
   muonBeamPhaseSpaceMeanXpCmd->SetParameterName("mean", false);
   muonBeamPhaseSpaceMeanXpCmd->SetUnitCategory("Angle");

   muonBeamPhaseSpaceSigmaXpCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/sigmaXp",
                                                                this);
   muonBeamPhaseSpaceSigmaXpCmd->SetGuidance("Set sigma of x' distribution");
   muonBeamPhaseSpaceSigmaXpCmd->SetParameterName("sigma", false);
   muonBeamPhaseSpaceSigmaXpCmd->SetUnitCategory("Angle");

   muonBeamPhaseSpaceCorrXCmd = new G4UIcmdWithADouble("/gem/generation/muonBeam/phaseSpace/corrX", this);
   muonBeamPhaseSpaceCorrXCmd->SetGuidance("Set correlation between x and x'");
   muonBeamPhaseSpaceCorrXCmd->SetParameterName("corr", false);

   muonBeamPhaseSpaceMeanYCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/meanY", this);
   muonBeamPhaseSpaceMeanYCmd->SetGuidance("Set mean of y distribution");
   muonBeamPhaseSpaceMeanYCmd->SetParameterName("mean", false);
   muonBeamPhaseSpaceMeanYCmd->SetUnitCategory("Length");

   muonBeamPhaseSpaceSigmaYCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/sigmaY",
                                                               this);
   muonBeamPhaseSpaceSigmaYCmd->SetGuidance("Set sigma of y distribution");
   muonBeamPhaseSpaceSigmaYCmd->SetParameterName("sigma", false);
   muonBeamPhaseSpaceSigmaYCmd->SetUnitCategory("Length");

   muonBeamPhaseSpaceMeanYpCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/meanYp",
                                                               this);
   muonBeamPhaseSpaceMeanYpCmd->SetGuidance("Set mean of y' distribution");
   muonBeamPhaseSpaceMeanYpCmd->SetParameterName("mean", false);
   muonBeamPhaseSpaceMeanYpCmd->SetUnitCategory("Angle");

   muonBeamPhaseSpaceSigmaYpCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/sigmaYp",
                                                                this);
   muonBeamPhaseSpaceSigmaYpCmd->SetGuidance("Set sigma of y' distribution");
   muonBeamPhaseSpaceSigmaYpCmd->SetParameterName("sigma", false);
   muonBeamPhaseSpaceSigmaYpCmd->SetUnitCategory("Angle");

   muonBeamPhaseSpaceCorrYCmd = new G4UIcmdWithADouble("/gem/generation/muonBeam/phaseSpace/corrY", this);
   muonBeamPhaseSpaceCorrYCmd->SetGuidance("Set correlation between y and y'");
   muonBeamPhaseSpaceCorrYCmd->SetParameterName("corr", false);

   muonBeamPhaseSpaceMeanZCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/meanZ", this);
   muonBeamPhaseSpaceMeanZCmd->SetGuidance("Set mean of z distribution");
   muonBeamPhaseSpaceMeanZCmd->SetParameterName("mean", false);
   muonBeamPhaseSpaceMeanZCmd->SetUnitCategory("Length");

   muonBeamPhaseSpaceSigmaZCmd = new G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/sigmaZ",
                                                               this);
   muonBeamPhaseSpaceSigmaZCmd->SetGuidance("Set sigma of z distribution");
   muonBeamPhaseSpaceSigmaZCmd->SetParameterName("sigma", false);
   muonBeamPhaseSpaceSigmaZCmd->SetUnitCategory("Length");

   muonBeamPhaseSpaceMomentumShapeCmd = new
   G4UIcmdWithAnInteger("/gem/generation/muonBeam/phaseSpace/momentumShape", this);
   muonBeamPhaseSpaceMomentumShapeCmd->SetGuidance("Set momentum shape. 0:Gauss, 1:Truncated Gauss");
   muonBeamPhaseSpaceMomentumShapeCmd->SetParameterName("shape", false);

   muonBeamPhaseSpaceMomentumMeanCmd = new
   G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/meanMomentum", this);
   muonBeamPhaseSpaceMomentumMeanCmd->SetGuidance("Set mean of momentum distribution");
   muonBeamPhaseSpaceMomentumMeanCmd->SetParameterName("mean", false);
   muonBeamPhaseSpaceMomentumMeanCmd->SetUnitCategory("Energy");

   muonBeamPhaseSpaceMomentumSigmaCmd = new
   G4UIcmdWithADoubleAndUnit("/gem/generation/muonBeam/phaseSpace/sigmaMomentum", this);
   muonBeamPhaseSpaceMomentumSigmaCmd->SetGuidance("Set sigma of momentum distribution");
   muonBeamPhaseSpaceMomentumSigmaCmd->SetParameterName("sigma", false);
   muonBeamPhaseSpaceMomentumSigmaCmd->SetUnitCategory("Energy");

   muonBeamPhaseSpaceMomentumWidthCmd = new
   G4UIcmdWithADouble("/gem/generation/muonBeam/phaseSpace/widthMomentum", this);
   muonBeamPhaseSpaceMomentumWidthCmd->SetGuidance("Set width of distribution in sigmas (only for truncated gaussian)");
   muonBeamPhaseSpaceMomentumWidthCmd->SetParameterName("width", false);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPrimaryGeneratorMessenger::~GEMPrimaryGeneratorMessenger()
{
   delete listCmd;
   delete evtypeCmd;
   delete fTGTGeometryIdCmd;
   delete stoppedMuonCenterXCmd;
   delete stoppedMuonSigmaXCmd;
   delete stoppedMuonCenterYCmd;
   delete stoppedMuonSigmaYCmd;
   delete stoppedMuonDepthCmd;
   delete stoppedMuonDepthSigmaCmd;
   delete stoppedMuonMaximizeGammaAngleCmd;
   delete stoppedMuonMaximizePositronAngleCmd;
   delete stoppedMuonInterestRangeCmd;
   delete stoppedMuonExtendedRangeCmd;
   delete stoppedMuonCosThetaGammaCenterCmd;
   delete stoppedMuonCosThetaGammaWidthCmd;
   delete stoppedMuonPhiGammaCenterCmd;
   delete stoppedMuonPhiGammaWidthCmd;
   delete stoppedMuonCosThetaPositronCenterCmd;
   delete stoppedMuonCosThetaPositronWidthCmd;
   delete stoppedMuonPhiPositronCenterCmd;
   delete stoppedMuonPhiPositronWidthCmd;
   delete stoppedMuonXRangeMinCmd;
   delete stoppedMuonXRangeMaxCmd;
   delete stoppedMuonYRangeMinCmd;
   delete stoppedMuonYRangeMaxCmd;
   delete stoppedMuonZRangeMinCmd;
   delete stoppedMuonZRangeMaxCmd;
   delete muonPolarizationCmd;
   delete muonRadiativeBranchingRatioCmd;
   delete muonRadiativeMaxFuncValueCmd;
   delete muonRadiativeScanNBinCmd;
   delete muonRadiativeGammaSpectrumLowerEdgeCmd;
   delete muonRadiativeSafetyFactorCmd;
   delete targetSizeCmd;
   delete targetAngleCmd;
   delete targetPositionCmd;
   delete targetMaterialCmd;
   delete muonBeamMuonDecayModeCmd;
   delete muonBeamPhaseSpaceReadFromFileCmd;
   delete muonBeamPhaseSpaceFileNameCmd;
   delete muonBeamPhaseSpaceMeanXCmd;
   delete muonBeamPhaseSpaceSigmaXCmd;
   delete muonBeamPhaseSpaceMeanXpCmd;
   delete muonBeamPhaseSpaceSigmaXpCmd;
   delete muonBeamPhaseSpaceCorrXCmd;
   delete muonBeamPhaseSpaceMeanYCmd;
   delete muonBeamPhaseSpaceSigmaYCmd;
   delete muonBeamPhaseSpaceMeanYpCmd;
   delete muonBeamPhaseSpaceSigmaYpCmd;
   delete muonBeamPhaseSpaceCorrYCmd;
   delete muonBeamPhaseSpaceMeanZCmd;
   delete muonBeamPhaseSpaceSigmaZCmd;
   delete muonBeamPhaseSpaceMomentumShapeCmd;
   delete muonBeamPhaseSpaceMomentumMeanCmd;
   delete muonBeamPhaseSpaceMomentumSigmaCmd;
   delete muonBeamPhaseSpaceMomentumWidthCmd;

   delete targetDir;
   delete genDir;
   delete muonDir;
   delete radiativeDir;
   delete stoppedMuonDir;
   delete muonBeamDir;
   delete muonBeamPhaseSpaceDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command,
                                               G4String newValue)
{
   if (command == listCmd) {
      GEMAction->DumpEventTypes();

   } else if (command == evtypeCmd) {
      G4int evtype = evtypeCmd->GetNewIntValue(newValue);
      GEMAction->SetEventType(evtype);

   } else if (command == fTGTGeometryIdCmd) {
      GEMAction->SetTGTGeometryId(fTGTGeometryIdCmd->GetNewIntValue(newValue));

   } else if (stoppedMuonCenterXCmd && command == stoppedMuonCenterXCmd) {
      GEMAction->SetStoppedMuonCenterX(stoppedMuonCenterXCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonSigmaXCmd && command == stoppedMuonSigmaXCmd) {
      GEMAction->SetStoppedMuonSigmaX(stoppedMuonSigmaXCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonCenterYCmd && command == stoppedMuonCenterYCmd) {
      GEMAction->SetStoppedMuonCenterY(stoppedMuonCenterYCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonSigmaYCmd && command == stoppedMuonSigmaYCmd) {
      GEMAction->SetStoppedMuonSigmaY(stoppedMuonSigmaYCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonDepthCmd && command == stoppedMuonDepthCmd) {
      GEMAction->SetStoppedMuonDepth(stoppedMuonDepthCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonDepthSigmaCmd && command == stoppedMuonDepthSigmaCmd) {
      GEMAction->SetStoppedMuonDepthSigma(stoppedMuonDepthSigmaCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonMaximizeGammaAngleCmd && command == stoppedMuonMaximizeGammaAngleCmd) {
      GEMAction->SetStoppedMuonCosThetaGammaCenter(0);
      GEMAction->SetStoppedMuonCosThetaGammaWidth(1);
      GEMAction->SetStoppedMuonPhiGammaCenter(0);
      GEMAction->SetStoppedMuonPhiGammaWidth(pi);

   } else if (stoppedMuonMaximizePositronAngleCmd && command == stoppedMuonMaximizePositronAngleCmd) {
      GEMAction->SetStoppedMuonCosThetaPositronCenter(0);
      GEMAction->SetStoppedMuonCosThetaPositronWidth(1);
      GEMAction->SetStoppedMuonPhiPositronCenter(0);
      GEMAction->SetStoppedMuonPhiPositronWidth(pi);

   } else if (stoppedMuonInterestRangeCmd && command == stoppedMuonInterestRangeCmd) {
      GEMAction->SetStoppedMuonCosThetaGammaCenter(kInterestCTHGENG);
      GEMAction->SetStoppedMuonCosThetaGammaWidth(kInterestDCOSTHG);
      GEMAction->SetStoppedMuonPhiGammaCenter(kInterestPHIGENG);
      GEMAction->SetStoppedMuonPhiGammaWidth(kInterestDPHIG);
      GEMAction->SetStoppedMuonCosThetaPositronCenter(kInterestCTHGENP);
      GEMAction->SetStoppedMuonCosThetaPositronWidth(kInterestDCOSTHP);
      GEMAction->SetStoppedMuonPhiPositronCenter(kInterestPHIGENP);
      GEMAction->SetStoppedMuonPhiPositronWidth(kInterestDPHIP);

   } else if (stoppedMuonExtendedRangeCmd && command == stoppedMuonExtendedRangeCmd) {
      GEMAction->SetStoppedMuonCosThetaGammaCenter(kExtendedCTHGENG);
      GEMAction->SetStoppedMuonCosThetaGammaWidth(kExtendedDCOSTHG);
      GEMAction->SetStoppedMuonPhiGammaCenter(kExtendedPHIGENG);
      GEMAction->SetStoppedMuonPhiGammaWidth(kExtendedDPHIG);
      GEMAction->SetStoppedMuonCosThetaPositronCenter(kExtendedCTHGENP);
      GEMAction->SetStoppedMuonCosThetaPositronWidth(kExtendedDCOSTHP);
      GEMAction->SetStoppedMuonPhiPositronCenter(kExtendedPHIGENP);
      GEMAction->SetStoppedMuonPhiPositronWidth(kExtendedDPHIP);

   } else if (stoppedMuonCosThetaGammaCenterCmd && command == stoppedMuonCosThetaGammaCenterCmd) {
      GEMAction->SetStoppedMuonCosThetaGammaCenter(stoppedMuonCosThetaGammaCenterCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonCosThetaGammaWidthCmd && command == stoppedMuonCosThetaGammaWidthCmd) {
      GEMAction->SetStoppedMuonCosThetaGammaWidth(stoppedMuonCosThetaGammaWidthCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonPhiGammaCenterCmd && command == stoppedMuonPhiGammaCenterCmd) {
      GEMAction->SetStoppedMuonPhiGammaCenter(stoppedMuonPhiGammaCenterCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonPhiGammaWidthCmd && command == stoppedMuonPhiGammaWidthCmd) {
      GEMAction->SetStoppedMuonPhiGammaWidth(stoppedMuonPhiGammaWidthCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonCosThetaPositronCenterCmd && command == stoppedMuonCosThetaPositronCenterCmd) {
      GEMAction->SetStoppedMuonCosThetaPositronCenter(stoppedMuonCosThetaPositronCenterCmd->GetNewDoubleValue(
                                                         newValue));

   } else if (stoppedMuonCosThetaPositronWidthCmd && command == stoppedMuonCosThetaPositronWidthCmd) {
      GEMAction->SetStoppedMuonCosThetaPositronWidth(stoppedMuonCosThetaPositronWidthCmd->GetNewDoubleValue(
                                                        newValue));

   } else if (stoppedMuonPhiPositronCenterCmd && command == stoppedMuonPhiPositronCenterCmd) {
      GEMAction->SetStoppedMuonPhiPositronCenter(stoppedMuonPhiPositronCenterCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonPhiPositronWidthCmd && command == stoppedMuonPhiPositronWidthCmd) {
      GEMAction->SetStoppedMuonPhiPositronWidth(stoppedMuonPhiPositronWidthCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonXRangeMinCmd && command == stoppedMuonXRangeMinCmd) {
      GEMAction->SetXRangeMin(stoppedMuonXRangeMinCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonXRangeMaxCmd && command == stoppedMuonXRangeMaxCmd) {
      GEMAction->SetXRangeMax(stoppedMuonXRangeMaxCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonYRangeMinCmd && command == stoppedMuonYRangeMinCmd) {
      GEMAction->SetYRangeMin(stoppedMuonYRangeMinCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonYRangeMaxCmd && command == stoppedMuonYRangeMaxCmd) {
      GEMAction->SetYRangeMax(stoppedMuonYRangeMaxCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonZRangeMinCmd && command == stoppedMuonZRangeMinCmd) {
      GEMAction->SetZRangeMin(stoppedMuonZRangeMinCmd->GetNewDoubleValue(newValue));

   } else if (stoppedMuonZRangeMaxCmd && command == stoppedMuonZRangeMaxCmd) {
      GEMAction->SetZRangeMax(stoppedMuonZRangeMaxCmd->GetNewDoubleValue(newValue));

   } else if (muonPolarizationCmd && command == muonPolarizationCmd) {
      GEMAction->SetMuonPolarization(muonPolarizationCmd->GetNewDoubleValue(newValue));

   } else if (muonRadiativeBranchingRatioCmd && command == muonRadiativeBranchingRatioCmd) {
      GEMAction->SetMuonRadiativeBranchingRatio(muonRadiativeBranchingRatioCmd->GetNewDoubleValue(newValue));

   } else if (muonRadiativeMaxFuncValueCmd && command == muonRadiativeMaxFuncValueCmd) {
      GEMAction->SetMuonRadiativeMaxFuncValue(muonRadiativeMaxFuncValueCmd->GetNewDoubleValue(newValue));

   } else if (muonRadiativeScanNBinCmd && command == muonRadiativeScanNBinCmd) {
      GEMAction->SetMuonRadiativeScanNBin(muonRadiativeScanNBinCmd->GetNewIntValue(newValue));

   } else if (muonRadiativeGammaSpectrumLowerEdgeCmd && command == muonRadiativeGammaSpectrumLowerEdgeCmd) {
      GEMAction->SetMuonRadiativeGammaSpectrumLowerEdge(muonRadiativeGammaSpectrumLowerEdgeCmd->GetNewDoubleValue(
                                                           newValue));

   } else if (muonRadiativeSafetyFactorCmd && command == muonRadiativeSafetyFactorCmd) {
      GEMAction->SetMuonRadiativeSafetyFactor(muonRadiativeSafetyFactorCmd->GetNewDoubleValue(newValue));

   } else if (targetSizeCmd && command == targetSizeCmd) {
      GEMAction->SetTargetSize(targetSizeCmd->GetNew3VectorValue(newValue));

   } else if (targetAngleCmd && command == targetAngleCmd) {
      GEMAction->SetTargetAngle(targetAngleCmd->GetNew3VectorValue(newValue));

   } else if (targetPositionCmd && command == targetPositionCmd) {
      GEMAction->SetTargetPosition(targetPositionCmd->GetNew3VectorValue(newValue));

   } else if (targetMaterialCmd && command == targetMaterialCmd) {
      GEMAction->SetTargetMaterial(targetMaterialCmd->GetNewIntValue(newValue));

   } else if (muonBeamMuonDecayModeCmd && command == muonBeamMuonDecayModeCmd) {
      GEMAction->SetMuonBeamMuonDecayMode(muonBeamMuonDecayModeCmd->GetNewIntValue(newValue));

   } else if (muonBeamPhaseSpaceReadFromFileCmd && command == muonBeamPhaseSpaceReadFromFileCmd) {
      GEMAction->SetMuonBeamPhaseSpaceReadFromFile(muonBeamPhaseSpaceReadFromFileCmd->GetNewBoolValue(newValue));

   } else if (muonBeamPhaseSpaceFileNameCmd && command == muonBeamPhaseSpaceFileNameCmd) {
      GEMAction->SetMuonBeamPhaseSpaceFileName(newValue);

   } else if (muonBeamPhaseSpaceMeanXCmd && command == muonBeamPhaseSpaceMeanXCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMeanX(muonBeamPhaseSpaceMeanXCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceSigmaXCmd && command == muonBeamPhaseSpaceSigmaXCmd) {
      GEMAction->SetMuonBeamPhaseSpaceSigmaX(muonBeamPhaseSpaceSigmaXCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceMeanXpCmd && command == muonBeamPhaseSpaceMeanXpCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMeanXp(muonBeamPhaseSpaceMeanXpCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceSigmaXpCmd && command == muonBeamPhaseSpaceSigmaXpCmd) {
      GEMAction->SetMuonBeamPhaseSpaceSigmaXp(muonBeamPhaseSpaceSigmaXpCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceCorrXCmd && command == muonBeamPhaseSpaceCorrXCmd) {
      GEMAction->SetMuonBeamPhaseSpaceCorrX(muonBeamPhaseSpaceCorrXCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceMeanYCmd && command == muonBeamPhaseSpaceMeanYCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMeanY(muonBeamPhaseSpaceMeanYCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceSigmaYCmd && command == muonBeamPhaseSpaceSigmaYCmd) {
      GEMAction->SetMuonBeamPhaseSpaceSigmaY(muonBeamPhaseSpaceSigmaYCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceMeanYpCmd && command == muonBeamPhaseSpaceMeanYpCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMeanYp(muonBeamPhaseSpaceMeanYpCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceSigmaYpCmd && command == muonBeamPhaseSpaceSigmaYpCmd) {
      GEMAction->SetMuonBeamPhaseSpaceSigmaYp(muonBeamPhaseSpaceSigmaYpCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceCorrYCmd && command == muonBeamPhaseSpaceCorrYCmd) {
      GEMAction->SetMuonBeamPhaseSpaceCorrY(muonBeamPhaseSpaceCorrYCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceMeanZCmd && command == muonBeamPhaseSpaceMeanZCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMeanZ(muonBeamPhaseSpaceMeanZCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceSigmaZCmd && command == muonBeamPhaseSpaceSigmaZCmd) {
      GEMAction->SetMuonBeamPhaseSpaceSigmaZ(muonBeamPhaseSpaceSigmaZCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceMomentumShapeCmd && command == muonBeamPhaseSpaceMomentumShapeCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMomentumShape(muonBeamPhaseSpaceMomentumShapeCmd->GetNewIntValue(newValue));

   } else if (muonBeamPhaseSpaceMomentumMeanCmd && command == muonBeamPhaseSpaceMomentumMeanCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMomentumMean(muonBeamPhaseSpaceMomentumMeanCmd->GetNewDoubleValue(newValue));

   } else if (muonBeamPhaseSpaceMomentumSigmaCmd && command == muonBeamPhaseSpaceMomentumSigmaCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMomentumSigma(muonBeamPhaseSpaceMomentumSigmaCmd->GetNewDoubleValue(
                                                       newValue));

   } else if (muonBeamPhaseSpaceMomentumWidthCmd && command == muonBeamPhaseSpaceMomentumWidthCmd) {
      GEMAction->SetMuonBeamPhaseSpaceMomentumWidth(muonBeamPhaseSpaceMomentumWidthCmd->GetNewDoubleValue(
                                                       newValue));

   } else {
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
