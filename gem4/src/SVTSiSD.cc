//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#include "G4SystemOfUnits.hh"
#include "SVTSiSD.hh"
#include "SVTSiHit.hh"
#include "SVTPixelHit.hh"
#include "SVTModule.hh"
#include "GEMUserTrackInformation.hh"

#include "G4EventManager.hh"
#include "G4Event.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4VProcess.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4SDManager.hh"
#include "G4EmProcessSubType.hh"

#include "TMath.h"

SVTModule* SVTSiSD::fgModule = 0;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiSD::SVTSiSD(G4String name)
:G4VSensitiveDetector(name)
,idx(-1)
,fSiHitsCollection(0)
,fPixelHitsCollection(0)
,fPrevStepTrackID(-1)
,fPrevStepPhysicalVol(0)
,fVerbose(0)
{
   collectionName.insert("SVTSiHitsCollection");
   collectionName.insert("SVTPixelHitsCollection");
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

SVTSiSD::~SVTSiSD()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiSD::Initialize(G4HCofThisEvent* HCE)
{
   // Called once per event at the first hit in the event
   // HCE is an array of hitsCollections of this event

   fSiHitsCollection = new SVTSiHitsCollection(SensitiveDetectorName,
                                                collectionName[0]);
   fPixelHitsCollection = new SVTPixelHitsCollection(SensitiveDetectorName,
                                                     collectionName[1]);
   //A way to keep all the hits of this event in one place if needed
   static G4int siHCID = -1;
   if (siHCID < 0) {
      siHCID = GetCollectionID(0);
   }
   HCE->AddHitsCollection(siHCID, fSiHitsCollection);
   static G4int pixelHCID = -1;
   if (pixelHCID < 0) {
      pixelHCID = GetCollectionID(1);
   }
   HCE->AddHitsCollection(pixelHCID, fPixelHitsCollection);
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

G4bool SVTSiSD::ProcessHits(G4Step* aStep, G4TouchableHistory* roHist)
{

   if (!roHist) return false; // Readout geometry is NULL

   G4Track* theTrack = aStep->GetTrack();
//    if (theTrack->GetMaterial()->GetName() != "G4_Si") {
//       return false;
//    }

   // only for a charged particle
   if (theTrack->GetParticleDefinition()->GetPDGCharge() == 0.)
      return false;

   G4double edep = aStep->GetTotalEnergyDeposit();
   if (edep <= 0.) 
      return false;


   G4StepPoint *preStepPoint  = aStep->GetPreStepPoint();
   G4StepPoint *postStepPoint = aStep->GetPostStepPoint();
   G4TouchableHistory *theTouchable =
         (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());
   G4VPhysicalVolume *physVol = theTouchable->GetVolume();

   G4int historyDepth = theTouchable->GetHistoryDepth();
   G4String volname = physVol->GetName();

   G4int depthToArray, depthToColumn;
   G4bool sensitive(false);
   // depth 
   //  0: 'SVTSensor' (sensor sensitive volume)
   //  1: 'SVTWafer'  (silicon wafer volume, a chip)
   //  2: 'SVTSensorArray' (a raw of chips, a segment)
   //  3: 'FGAS'
   if (volname == "SVTSensor") {
      // historyDepth at 4
      depthToColumn = 1;
      depthToArray = 2;
      sensitive = true;
   } else if (volname == "SVTWafer") {
      // historyDepth at 3
      depthToColumn = 0;
      depthToArray = 1;
   } else {
      G4cout<<"ERROR : on different volume!!"<<G4endl;
      return false;
   }


   // Get layer, segment, and sensor-chip number
   // chip number starts from US-top to DS and then bottom (negative phi)
   G4int layerID(-1);
   G4int arrayID = theTouchable->GetVolume(depthToArray)->GetCopyNo();
   G4int rowID = arrayID;                                      // local (in a layer) array row ID
   G4int columnID = theTouchable->GetVolume(depthToColumn)->GetCopyNo(); // local array column ID 
   G4int chipID(0);
   G4int countSegment(0);
   for (G4int iLayer = 0; (countSegment <= arrayID) 
              && iLayer < fgModule->GetNLayers(); iLayer++) {
      countSegment += fgModule->GetNSensorInPhiAt(iLayer);
      layerID++;
      if (iLayer > 0) {
         rowID -= fgModule->GetNSensorInPhiAt(iLayer - 1);
         chipID += fgModule->GetNSensorInZAt(iLayer - 1) 
               * fgModule->GetNSensorInPhiAt(iLayer - 1);
      }
   }
   chipID += rowID * fgModule->GetNSensorInZAt(layerID);
   chipID += columnID;

   G4int particleID = GEMUserTrackInformation::GetG3ParticleID(theTrack->GetParticleDefinition());
   G4int trackID = theTrack->GetTrackID();
   G4int parentID = theTrack->GetParentID();

   G4double time = preStepPoint->GetGlobalTime();
   G4double slength = aStep->GetStepLength();
   G4ThreeVector xyzPre = preStepPoint->GetPosition();
   G4ThreeVector xyzPost = postStepPoint->GetPosition();
   G4ThreeVector mom = preStepPoint->GetMomentum();



   // Make new hit (GEANT hit) for hit in sensitive region
   if (sensitive) {
      G4int hitIndex = fSiHitsCollection->GetSize();

      SVTSiHit *newHit = new SVTSiHit();

      newHit->SetIndex(hitIndex);
      newHit->SetParticleID(particleID);
      newHit->SetTrackID(trackID);
      newHit->SetChipID(chipID);
      newHit->SetTime(time);
      newHit->SetXYZPre(xyzPre);
      newHit->SetMomentum(mom);
      newHit->SetXYZPost(xyzPost);
      newHit->SetEloss(edep);
      newHit->SetStepLength(slength);
      
      fSiHitsCollection->insert(newHit);
   }


   G4bool isPreStepPointBoundary = (preStepPoint->GetStepStatus() == fGeomBoundary);
   SVTPixelHit *pixelHit(0);
   G4bool newHit(false);
   // condition of a new cluster
   if (isPreStepPointBoundary
       && physVol->GetName() == "SVTWafer"
       && ((fPrevStepTrackID != trackID) 
           || (fPrevStepPhysicalVol->GetName() != "SVTSensor"))) {
      // It is the first step of hit cluster by a common track in this layer
      newHit = true;

   } else { // not boundary, inside a volume
      G4double minTime(1e10);
      G4int bestHitId(-1);
      for (size_t iHit = 0; iHit < fPixelHitsCollection->GetSize(); iHit++) {
         pixelHit = (*fPixelHitsCollection)[iHit];
         if (pixelHit->GetLayerID() == layerID
             && pixelHit->FindTrack(trackID)) {
            if (TMath::Abs(time - pixelHit->GetTime()) < minTime) {
               minTime = TMath::Abs(time - pixelHit->GetTime());
               bestHitId = iHit;
            }
         }
      }
      if (bestHitId >= 0) {
         pixelHit = (*fPixelHitsCollection)[bestHitId];         
         if (pixelHit->GetTrackID() == trackID) {
            pixelHit->AddPathLength(slength); // add path length only for main incident track
         }
      } else {
         // check parent
         for (size_t iHit = 0; iHit < fPixelHitsCollection->GetSize(); iHit++) {
            pixelHit = (*fPixelHitsCollection)[iHit];
            if (pixelHit->GetLayerID() == layerID
                && pixelHit->FindTrack(parentID)) {
               if (TMath::Abs(time - pixelHit->GetTime()) < minTime) {
                  minTime = TMath::Abs(time - pixelHit->GetTime());
                  bestHitId = iHit;
               }
            }
         }
         if (bestHitId >= 0) {
            pixelHit = (*fPixelHitsCollection)[bestHitId];
            pixelHit->InsertTrackIDSet(trackID);
         } else {
            pixelHit = 0;
         }
      }
      if (!pixelHit) {
         // It is an isolated hit via neutral particle
         newHit = true;
//          G4cout<<"ERROR: NO BELOGING PIXEL HIT "<<parentID<<" "<<trackID<<" "
//                <<physVol->GetName()<<" "<<theTrack->GetParticleDefinition()->GetParticleName()<<G4endl;
      } 
   }

   if (newHit) {
      // Make new pixel hit
      pixelHit = new SVTPixelHit();
      fPixelHitsCollection->insert(pixelHit);

      pixelHit->SetTrackID(trackID);
      pixelHit->SetLayerID(layerID);
      pixelHit->InsertTrackIDSet(trackID);
      pixelHit->SetTime(time);
      pixelHit->AddPathLength(slength);

      // Set sensor rotation angle and incident angle of the charged particle
      G4AffineTransform aTrans = roHist->GetHistory()->GetTopTransform();
      aTrans.Invert();
      pixelHit->SetRot(aTrans.NetRotation());
      G4ThreeVector norm(1.0, 0.0, 0.0);
      norm.transform(aTrans.NetRotation().inverse());
      G4double incangle = norm.angle(mom);
      pixelHit->SetIncAngle(incangle);
      
      if (fVerbose) {
         G4cout<<"New pixel hit "<<layerID<<" "<<trackID<<" "<<particleID<<" chip"<<chipID
               <<" "<<physVol->GetName()<<" "<<edep/keV<<"keV "<<parentID<<" "<<fPrevStepTrackID;
         if (fPrevStepPhysicalVol)
            G4cout<<" "<<fPrevStepPhysicalVol->GetName()<<G4endl;
         else
            G4cout<<G4endl;

//          /////////////////////////
//          G4cout<<"INCIDENT ANGLE "<<incangle/deg<<"deg "<<norm<<" phi="<<norm.phi()/deg<<" "<<norm<<G4endl;
//          G4cout<<"RRRRRRRR "<<theTouchable->GetVolume(depthToArray)->GetObjectRotationValue().getPhi()/deg
//                <<", "<<aTrans.NetRotation().phiX()/deg
//                <<", "<<theTouchable->GetHistory()->GetTopTransform().NetRotation().getPhi()/deg
//                <<G4endl;
//          G4cout<<"XXXXXXX "<<theTouchable->GetVolume(depthToArray)->GetObjectTranslation().x()
//                <<", "<<aTrans.NetTranslation().x()
//                <<", "<<xyzPre.x()
//                <<G4endl;
//          G4cout<<"YYYYYYY "<<theTouchable->GetVolume(depthToArray)->GetObjectTranslation().y()
//                <<", "<<aTrans.NetTranslation().y()
//                <<", "<<xyzPre.y()
//                <<G4endl;
      }
   }

   if (pixelHit) {

      pixelHit->AddEloss(edep);
      if (sensitive) {
         pixelHit->AddXYZMean((xyzPre + xyzPost)/2., edep);
         pixelHit->AddEdepTot(edep);

         // Get pixel information from the ReadOut volume
         G4int pixelIndexZ(-1), pixelIndexY(-1), pixelIndex(-1);

         // Get pixelIndex from readout geometry
         pixelIndexZ = roHist->GetReplicaNumber();
         pixelIndexY = roHist->GetReplicaNumber(1);
         // pixel index starts from bottom-left to right then top
         // |      ...  NPixelInZ*nPixelInY-1|
         // |                                |
         // |      ...                       |
         // |0 1 2 ...    NPixelInZ-1        |
         pixelIndex = pixelIndexY * fgModule->GetNPixelInZ() + pixelIndexZ;
            
         if (fVerbose) {
            G4cout<<theTrack->GetTrackID()<<" "<<theTrack->GetParticleDefinition()->GetParticleName()
                  <<" "<<layerID<<" "<<chipID<<"("<<rowID<<","<<columnID<<") "<<pixelIndexZ<<"*"<<pixelIndexY
                  <<" "<<pixelIndex<<" "<<volname<<" depth "<<historyDepth<<" "<<slength/um<<"um"
                  <<" "<<edep/keV<<"keV "<<preStepPoint->GetTotalEnergy()<<G4endl;
         }

         G4bool newPixel(true);
         for (size_t iPixel = 0; iPixel < pixelHit->GetPixelIndexVec().size(); iPixel++) {
            if (chipID == pixelHit->GetChipIDVec()[iPixel]
                && pixelIndex == pixelHit->GetPixelIndexVec()[iPixel]) {
               newPixel = false;
               pixelHit->AddEdepAt(iPixel, edep);
            }
         }
         if (newPixel) {
            pixelHit->GetChipIDVec().push_back(chipID);
            pixelHit->GetPixelIndexVec().push_back(pixelIndex);
            pixelHit->GetEdepVec().push_back(edep);

            G4AffineTransform aTrans = roHist->GetHistory()->GetTopTransform();
            aTrans.Invert();
            pixelHit->GetXYZPixelVec().push_back(aTrans.NetTranslation());
         }
      }
   }




   fPrevStepTrackID = trackID;
   fPrevStepPhysicalVol = physVol;

   return true;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiSD::EndOfEvent(G4HCofThisEvent*)
{
   SVTPixelHit *pixelHit(0);
   for (size_t iHit = 0; iHit < fPixelHitsCollection->GetSize(); iHit++) {
      pixelHit = (*fPixelHitsCollection)[iHit];
      pixelHit->CalculateXYZMean();
   }
   fPrevStepTrackID = -1;
   fPrevStepPhysicalVol = 0;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiSD::clear()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiSD::DrawAll()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

void SVTSiSD::PrintAll()
{
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
