//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <vector>
#include "globals.hh"
#include "GEMPhysicsList.hh"
#include "GEMPhysicsListMessenger.hh"

#include "G4Version.hh"
#include "G4VPhysicsConstructor.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"
#if G4VERSION_NUMBER >= 1040
#include "G4EmStandardPhysicsSS.hh"
#include "G4EmStandardPhysicsGS.hh"
#include "G4EmStandardPhysicsWVI.hh"
#endif
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmLowEPPhysics.hh"
#include "G4Decay.hh"
#include "G4DecayPhysics.hh"
#include "G4DecayTable.hh"
#include "G4EmSaturation.hh"
#include "G4LossTableManager.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronElasticPhysicsXS.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4HadronHElasticPhysics.hh"
#include "G4ChargeExchangePhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4NeutronCrossSectionXS.hh"
#include "G4StoppingPhysics.hh"
#include "G4IonBinaryCascadePhysics.hh"
#include "G4IonPhysics.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4HadronPhysicsFTFP_BERT_HP.hh"
#include "G4HadronPhysicsFTFP_BERT_TRV.hh"
#include "G4HadronPhysicsFTF_BIC.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4HadronPhysicsQGSP_BERT.hh"
#include "G4HadronPhysicsQGSP_BERT_HP.hh"
#include "G4HadronPhysicsQGSP_BIC.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronPhysicsQGSP_FTFP_BERT.hh"
#include "G4HadronPhysicsQGS_BIC.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "GEMMuonDecayChannelWithSpin.hh"
#include "GEMMuonRadiativeDecayChannelWithSpin.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "G4PhaseSpaceDecayChannel.hh"
#include "G4ProcessManager.hh"
#include "G4WentzelVIModel.hh"
#include "G4CoulombScattering.hh"
#include "G4hBremsstrahlung.hh"
#include "G4hPairProduction.hh"
#if G4VERSION_NUMBER < 1100
#include "G4EmProcessOptions.hh"
#endif
# if G4VERSION_NUMBER >= 1100
#include "G4OpticalParameters.hh"
#endif
#include "G4EmConfigurator.hh"
#include "G4PAIModel.hh"
#include "G4PAIPhotModel.hh"
// #include "G4PAIPhotonModel.hh"
#include "G4RegionStore.hh"
#include "G4StepLimiter.hh"
#include "G4HadronicProcessStore.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPhysicsList::GEMPhysicsList()
   : fCerenkovProcess(0)
   , fScintillationProcess(0)
   , fAbsorptionProcess(0)
   , fRayleighScatteringProcess(0)
   , fMieHGScatteringProcess(0)
   , fBoundaryProcess(0)
   , fMuonMichelChannel(0)
   , fMuonChannelWithSpin(0)
   , fMuonRadiativeChannelWithSpin(0)
   , fMuegammaChannel(0)
   , fMessenger(new GEMPhysicsListMessenger(this))
   , fParticleList(nullptr)
   , fEMPhysicsList(nullptr)
   , fEmName("")
   , fFiniteRiseTime(false)
{
   SetVerboseLevel(0);

   // Particles
   fParticleList = new G4DecayPhysics(verboseLevel);

   AddPhysicsList("emstandard"); // Geant4 standard EM process list
   // Can be changed later with command /gem/phys/addPhysics
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPhysicsList::~GEMPhysicsList()
{
   delete fMessenger;
   delete fParticleList;
   delete fEMPhysicsList;
   for (size_t i = 0; i < fHadronPhys.size(); i++) {
      delete fHadronPhys[i];
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::ConstructParticle()
{

   fParticleList->ConstructParticle();

   // add special decay processes
   G4DecayTable* table;

   // mu+
   G4ParticleDefinition* pMu = G4MuonPlus::MuonPlusDefinition();
   table = pMu->GetDecayTable();
   fMuonMichelChannel = (*table)[0];

   // decay with spin
   fMuonChannelWithSpin = new GEMMuonDecayChannelWithSpin("mu+", 0);
   table->Insert(fMuonChannelWithSpin);

   // radiative decay
   fMuonRadiativeChannelWithSpin = new GEMMuonRadiativeDecayChannelWithSpin("mu+", 0);
   table->Insert(fMuonRadiativeChannelWithSpin);

   // mu+ -> e+ + gamma signal
   fMuegammaChannel = new G4PhaseSpaceDecayChannel("mu+", 0, 2, "e+", "gamma");
   table->Insert(fMuegammaChannel);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::ConstructProcess()
{
   AddTransportation();
   // ConstructGeneral();
   fParticleList->ConstructProcess(); // decay process
   ConstructEM();
   ConstructOp();
   for (size_t i = 0; i < fHadronPhys.size(); i++) {
      fHadronPhys[i]->ConstructProcess();
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4eMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
#include "G4hMultipleScattering.hh"

#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4ionIonisation.hh"
#include "G4IonParametrisedLossModel.hh"
#include "G4NuclearStopping.hh"

#include "G4hIonisation.hh"
#include "G4EnergyLossTables.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::ConstructEM()
{
#if G4VERSION_NUMBER >= 1030
   auto theParticleIterator = GetParticleIterator();
#endif

   // Electromagnetic physics list
   // The list of models is already set by AddPhysicsList() (/gem/phys/addPhysic)
   fEMPhysicsList->ConstructProcess();

   // Step limitation seen as a process
   G4StepLimiter* stepLimiter = new G4StepLimiter();

   theParticleIterator->reset();

   while ((*theParticleIterator)()) {
      G4ParticleDefinition *particle     = theParticleIterator->value();
      G4ProcessManager     *pmanager     = particle->GetProcessManager();
      G4String              particleName = particle->GetParticleName();
      if (particleName == "e-" ||
          particleName == "e+") {
         pmanager->AddDiscreteProcess(stepLimiter);
      }
   }


#if 0
   G4EmProcessOptions opt;
   opt.SetVerbose(0);
   opt.SetPolarAngleLimit(0.2);
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::ConstructOp()
{
   fCerenkovProcess           = new G4Cerenkov("Cerenkov");
   fScintillationProcess      = new G4Scintillation("Scintillation");
   fAbsorptionProcess         = new G4OpAbsorption();
   fRayleighScatteringProcess = new G4OpRayleigh();
   fMieHGScatteringProcess    = new G4OpMieHG();
   fBoundaryProcess           = new G4OpBoundaryProcess();

   //  fCerenkovProcess->DumpPhysicsTable();
   //  fScintillationProcess->DumpPhysicsTable();
   //  fRayleighScatteringProcess->DumpPhysicsTable();

   fCerenkovProcess->SetMaxNumPhotonsPerStep(20);
   fCerenkovProcess->SetMaxBetaChangePerStep(10.0);
   fCerenkovProcess->SetTrackSecondariesFirst(false);


#if G4VERSION_NUMBER >= 1100
   G4OpticalParameters *opParams = G4OpticalParameters::Instance();
   opParams->SetScintTrackSecondariesFirst(true);
   opParams->SetScintFiniteRiseTime(fFiniteRiseTime);

   // Set particle-dependent scintillation process.
   // It is required for LXe scintillation. All the other scintillators become also particle dependent!
   opParams->SetScintByParticleType(true);
#else
   fScintillationProcess->SetTrackSecondariesFirst(false);
   fScintillationProcess->SetFiniteRiseTime(fFiniteRiseTime);
   // Set particle-dependent scintillation process for version < 11
   // For version >= 11, it is not necessary and set via MPT
   fScintillationProcess->SetScintillationYieldFactor(1.);
#endif
   
   // Use Birks Correction in the Scintillation process
#if 0
   G4EmSaturation* emSaturation = G4LossTableManager::Instance()->EmSaturation();
   fScintillationProcess->AddSaturation(emSaturation);
#endif

#if G4VERSION_NUMBER >= 1030
   auto theParticleIterator = GetParticleIterator();
#endif
   theParticleIterator->reset();
   while ((*theParticleIterator)()) {
      G4ParticleDefinition* particle = theParticleIterator->value();
      G4ProcessManager* pmanager = particle->GetProcessManager();
      G4String particleName = particle->GetParticleName();
      if (fCerenkovProcess->IsApplicable(*particle)) {
         pmanager->AddProcess(fCerenkovProcess);
         pmanager->SetProcessOrdering(fCerenkovProcess, idxPostStep);
      }
      if (fScintillationProcess->IsApplicable(*particle)) {
         pmanager->AddProcess(fScintillationProcess);
         pmanager->SetProcessOrderingToLast(fScintillationProcess, idxAtRest);
         pmanager->SetProcessOrderingToLast(fScintillationProcess, idxPostStep);
      }

      if (particleName == "opticalphoton") {
         pmanager->AddDiscreteProcess(fAbsorptionProcess);
         pmanager->AddDiscreteProcess(fRayleighScatteringProcess);
         pmanager->AddDiscreteProcess(fMieHGScatteringProcess);
         pmanager->AddDiscreteProcess(fBoundaryProcess);
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::SetVerbose(G4int verbose)
{
   if (fCerenkovProcess) {
      fCerenkovProcess->SetVerboseLevel(verbose);
   }
   if (fScintillationProcess) {
      fScintillationProcess->SetVerboseLevel(verbose);
   }
   if (fAbsorptionProcess) {
      fAbsorptionProcess->SetVerboseLevel(verbose);
   }
   if (fRayleighScatteringProcess) {
      fRayleighScatteringProcess->SetVerboseLevel(verbose);
   }
   if (fMieHGScatteringProcess) {
      fMieHGScatteringProcess->SetVerboseLevel(verbose);
   }
   if (fBoundaryProcess) {
      fBoundaryProcess->SetVerboseLevel(verbose);
   }
   for (size_t i = 0; i < fHadronPhys.size(); i++) {
      fHadronPhys[i]->SetVerboseLevel(verbose);
   }
   G4HadronicProcessStore::Instance()->SetVerbose(verbose);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::SetCuts()
{
   //  " G4VUserPhysicsList::SetCutsWithDefault" method sets
   //   the default cut value for all particle types
   //
   SetCutsWithDefault();

   if (verboseLevel > 0) {
      DumpCutValuesTable();
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::SetMuonDecayBR(G4double signalBR, G4double michelBR, G4double rmdBR)
{
   if (!fMuonMichelChannel            ||
       !fMuonChannelWithSpin          ||
       !fMuonRadiativeChannelWithSpin ||
       !fMuegammaChannel) {
      std::ostringstream o;
      o << "Muon decays are not initialized";
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }

   fMuonMichelChannel->SetBR(0);

   G4double totalBR = signalBR + michelBR + rmdBR;

   fMuegammaChannel->SetBR(signalBR / totalBR);
   fMuonChannelWithSpin->SetBR(michelBR / totalBR);
   fMuonRadiativeChannelWithSpin->SetBR(rmdBR / totalBR);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::AddPhysicsList(const G4String& name)
{
   if (verboseLevel > 1) {
      G4cout << "GEMPhysicsList::AddPhysicsList: <" << name << ">" << G4endl;
   }

   //if (name == fEmName) return;

   if (name == "emstandard" || name == "emstandard_opt0") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysics(verboseLevel);
   } else if (name == "emstandard_opt1") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysics_option1(verboseLevel);

   } else if (name == "emstandard_opt2") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysics_option2(verboseLevel);

   } else if (name == "emstandard_opt3") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysics_option3(verboseLevel);

   } else if (name == "emstandard_opt4") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysics_option4(verboseLevel);

#if G4VERSION_NUMBER >= 1040
   } else if (name == "emstandardSS") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysicsSS(verboseLevel);

   } else if (name == "emstandardGS") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysicsGS(verboseLevel);

   } else if (name == "emstandardWVI") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmStandardPhysicsWVI(verboseLevel);

#endif
   } else if (name == "emlivermore") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmLivermorePhysics(verboseLevel);

   } else if (name == "empenelope") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmPenelopePhysics(verboseLevel);

   } else if (name == "emlowenergy") {
      fEmName = name;
      delete fEMPhysicsList;
      fEMPhysicsList = new G4EmLowEPPhysics(verboseLevel);



// Copy from g4 example extended/hadronic/Hadr01/src/PhysicsList.cc

   } else if (name == "FTFP_BERT_EMV") {

      AddPhysicsList("FTFP_BERT");
      AddPhysicsList("emstandard_opt1");

   } else if (name == "FTFP_BERT_EMX") {

      AddPhysicsList("FTFP_BERT");
      AddPhysicsList("emstandard_opt2");

   } else if (name == "FTFP_BERT_EMY") {

      AddPhysicsList("FTFP_BERT");
      AddPhysicsList("emstandard_opt3");

   } else if (name == "FTFP_BERT_EMZ") {

      AddPhysicsList("FTFP_BERT");
      AddPhysicsList("emstandard_opt4");

   } else if (name == "FTFP_BERT") {

      SetBuilderList0(false);
      fHadronPhys.push_back(new G4HadronPhysicsFTFP_BERT(verboseLevel));
   } else if (name == "FTFP_BERT_TRV") {

      AddPhysicsList("emstandardGS");
      G4EmParameters::Instance()->SetMscStepLimitType(fUseSafety);

      SetBuilderList1(false);
      fHadronPhys.push_back(new G4HadronPhysicsFTFP_BERT_TRV(verboseLevel));

   } else if (name == "FTF_BIC") {

      SetBuilderList0(false);
      fHadronPhys.push_back(new G4HadronPhysicsFTF_BIC(verboseLevel));

   } else if (name == "QBBC") {

      AddPhysicsList("emstandard_opt0");
      SetBuilderList2();
      fHadronPhys.push_back(new G4HadronInelasticQBBC(verboseLevel));

   } else if (name == "QGSP_BERT") {

      SetBuilderList0(false);
      fHadronPhys.push_back(new G4HadronPhysicsQGSP_BERT(verboseLevel));

   } else if (name == "QGSP_FTFP_BERT") {

      SetBuilderList0(false);
      fHadronPhys.push_back(new G4HadronPhysicsQGSP_FTFP_BERT(verboseLevel));

   } else if (name == "QGSP_FTFP_BERT_EMV") {

      AddPhysicsList("QGSP_FTFP_BERT");
      AddPhysicsList("emstandard_opt1");

   } else if (name == "QGSP_BERT_EMV") {

      AddPhysicsList("QGSP_BERT");
      AddPhysicsList("emstandard_opt1");

   } else if (name == "QGSP_BERT_EMX") {

      AddPhysicsList("QGSP_BERT");
      AddPhysicsList("emstandard_opt2");

   } else if (name == "QGSP_BERT_HP") {

      SetBuilderList0(true);
      fHadronPhys.push_back(new G4HadronPhysicsQGSP_BERT_HP(verboseLevel));

   } else if (name == "QGSP_BIC") {

      SetBuilderList0(false);
      fHadronPhys.push_back(new G4HadronPhysicsQGSP_BIC(verboseLevel));

   } else if (name == "QGSP_BIC_EMY") {

      AddPhysicsList("QGSP_BIC");
      AddPhysicsList("emstandard_opt3");

   } else if (name == "QGS_BIC") {

      SetBuilderList0(false);
      fHadronPhys.push_back(new G4HadronPhysicsQGS_BIC(verboseLevel));

   } else if (name == "QGSP_BIC_HP") {

      SetBuilderList0(true);
      fHadronPhys.push_back(new G4HadronPhysicsQGSP_BIC_HP(verboseLevel));

   } else if (name == "RadioactiveDecay") {

      fHadronPhys.push_back(new G4RadioactiveDecayPhysics(verboseLevel));


   } else {

      G4cout << "GEMPhysicsList::AddPhysicsList: <" << name << ">"
             << " is not defined"
             << G4endl;
   }

   return;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::AddPAIModel(const G4String& modname)
{
#if G4VERSION_NUMBER >= 1030
   auto theParticleIterator = GetParticleIterator();
#endif
   theParticleIterator->reset();
   while ((*theParticleIterator)()) {
      G4ParticleDefinition* particle = theParticleIterator->value();
      G4String partname = particle->GetParticleName();
      if (partname == "e-" || partname == "e+") {
         NewPAIModel(particle, modname, "eIoni");

      } else if (partname == "mu-" || partname == "mu+") {
         NewPAIModel(particle, modname, "muIoni");

      } else if (partname == "proton" ||
                 partname == "pi+" ||
                 partname == "pi-"
                ) {
         NewPAIModel(particle, modname, "hIoni");
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPhysicsList::NewPAIModel(const G4ParticleDefinition* part,
                                 const G4String& modname,
                                 const G4String& procname)
{
//   std::vector<const char*> regionNames = {"FGASRegion"};
//   std::vector<const char*> regionNames = {"CYLDCHRegion"};
   std::vector<const char*> regionNames = {"FGASRegion", "CYLDCHRegion"};

   const G4double maxEnergy = 10 * TeV;
#if 0
   if (!G4RegionStore::GetInstance()->GetRegion(regionName, false)) {
      return;
   }
#endif

   G4String partname = part->GetParticleName();
   G4EmConfigurator *em_config = G4LossTableManager::Instance()->EmConfigurator();

   for (auto&& region : regionNames) {
      if (modname == "pai") {
         G4PAIModel* pai = new G4PAIModel(part, "PAIModel");
         em_config->SetExtraEmModel(partname, procname, pai, region,
                                    0.0, maxEnergy, pai);
      } else if (modname == "pai_photon") {
         // G4PAIPhotonModel* pai = new G4PAIPhotonModel(part,"PAIPhotModel");
         G4PAIPhotModel* pai = new G4PAIPhotModel(part, "PAIPhotModel");
         em_config->SetExtraEmModel(partname, procname, pai, region,
                                    0.0, maxEnergy, pai);
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Copy from g4 example extended/hadronic/Hadr01/src/PhysicsList.cc
void GEMPhysicsList::SetBuilderList0(G4bool flagHP)
{
   fHadronPhys.push_back(new G4EmExtraPhysics(verboseLevel));
   if (flagHP) {
      fHadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
   } else {
      fHadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
   }
   fHadronPhys.push_back(new G4StoppingPhysics(verboseLevel));
   fHadronPhys.push_back(new G4IonPhysics(verboseLevel));
   fHadronPhys.push_back(new G4NeutronTrackingCut(verboseLevel));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GEMPhysicsList::SetBuilderList1(G4bool flagHP)
{
   fHadronPhys.push_back(new G4EmExtraPhysics(verboseLevel));
   if (flagHP) {
      fHadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
   } else {
      fHadronPhys.push_back(new G4HadronHElasticPhysics(verboseLevel));
   }
   fHadronPhys.push_back(new G4StoppingPhysics(verboseLevel));
   fHadronPhys.push_back(new G4IonPhysics(verboseLevel));
   fHadronPhys.push_back(new G4NeutronTrackingCut(verboseLevel));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GEMPhysicsList::SetBuilderList2()
{
   fHadronPhys.push_back(new G4EmExtraPhysics(verboseLevel));
   fHadronPhys.push_back(new G4HadronElasticPhysicsXS(verboseLevel));
   fHadronPhys.push_back(new G4StoppingPhysics(verboseLevel));
   fHadronPhys.push_back(new G4IonPhysics(verboseLevel));
   fHadronPhys.push_back(new G4NeutronTrackingCut(verboseLevel));
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
