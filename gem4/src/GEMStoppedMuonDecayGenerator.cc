//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "Randomize.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4TransportationManager.hh"
#include "G4Navigator.hh"
#include "GEMPhysicsList.hh"
#include "G4VDecayChannel.hh"
#include "G4DecayProducts.hh"
#include "G4DynamicParticle.hh"
#include "GEMMuonDecayChannelWithSpin.hh"
#include "GEMMuonRadiativeDecayChannelWithSpin.hh"
#include "GEMAbsTargetModule.hh"

#include "GEMStoppedMuonDecayGenerator.hh"
#include "GEMPrimaryGeneratorAction.hh"
#include "GEMPrimaryGeneratorMessenger.hh"
#include "GEMConstants.hh"
#include <cmath>
#include <fstream>
#include <vector>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <iostream>
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


/*
 * Parses through csv file line by line and returns the data
 * in vector of vector of strings.
 */



GEMStoppedMuonDecayGenerator::GEMStoppedMuonDecayGenerator()
   : fGeneratorAction(nullptr)
   , fMuonCenterX(0)
   , fMuonSigmaX(0)
   , fMuonCenterY(0)
   , fMuonSigmaY(0)
   , fMuonDepth(0)
   , fMuonDepthSigma(0)
   , fTargetSize(0, 0, 0)
   , fTargetPosition(0, 0, 0)
   , fTargetVol(nullptr)
//,fTargetMat(nullptr)
   , fTargetRotation()
   , fCosThetaGammaCenter(0)
   , fCosThetaGammaWidth(0)
   , fPhiGammaCenter(0)
   , fPhiGammaWidth(0)
   , fCosThetaPositronCenter(0)
   , fCosThetaPositronWidth(0)
   , fPhiPositronCenter(0)
   , fPhiPositronWidth(0)
   , fXRangeMin(0)
   , fXRangeMax(0)
   , fYRangeMin(0)
   , fYRangeMax(0)
   , fZRangeMin(0)
   , fZRangeMax(0)
//,fDEFORM_FILE("")
   , fMuonPolarization(0)
   , fRadiativeBR(0)
{
   Init();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMStoppedMuonDecayGenerator::GEMStoppedMuonDecayGenerator(GEMPrimaryGeneratorAction *act)
   : fGeneratorAction(act)
   , fMuonCenterX(0)
   , fMuonSigmaX(0)
   , fMuonCenterY(0)
   , fMuonSigmaY(0)
   , fMuonDepth(0)
   , fMuonDepthSigma(0)
   , fTargetSize(0, 0, 0)
   , fTargetPosition(0, 0, 0)
   , fTargetVol(nullptr)
//,fTargetMat(nullptr)
   , fTargetRotation()
   , fCosThetaGammaCenter(0)
   , fCosThetaGammaWidth(0)
   , fPhiGammaCenter(0)
   , fPhiGammaWidth(0)
//,fDEFORM_FILE("")
   , fCosThetaPositronCenter(0)
   , fCosThetaPositronWidth(0)
   , fPhiPositronCenter(0)
   , fPhiPositronWidth(0)
   , fXRangeMin(0)
   , fXRangeMax(0)
   , fYRangeMin(0)
   , fYRangeMax(0)
   , fZRangeMin(0)
   , fZRangeMax(0)
   , fMuonPolarization(0)
   , fRadiativeBR(0)
{
   Init();



}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMStoppedMuonDecayGenerator::~GEMStoppedMuonDecayGenerator()
{
   fGeneratorAction = nullptr;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMStoppedMuonDecayGenerator::Init()
{
   fMuonCenterX    = kDefaultStoppedMuonCenterX;
   fMuonSigmaX     = kDefaultStoppedMuonSigmaX;
   fMuonCenterY    = kDefaultStoppedMuonCenterY;
   fMuonSigmaY     = kDefaultStoppedMuonSigmaY;
   fMuonDepth      = kDefaultStoppedMuonDepth;
   fMuonDepthSigma = kDefaultStoppedMuonDepthSigma;


   fTargetSize.set(kDefaultTargetSize[0],
                   kDefaultTargetSize[1],
                   kDefaultTargetSize[2]);
   fTargetPosition.set(kDefaultTargetPosition[0],
                       kDefaultTargetPosition[1],
                       kDefaultTargetPosition[2]);
   //fDEFORM_FILE = "";//nullptr;
   fTargetVol              = nullptr;
   //fTargetMat              = nullptr;
   fCosThetaGammaCenter    = kDefaultCTHGENG;
   fCosThetaGammaWidth     = kDefaultDCOSTHG;
   fPhiGammaCenter         = kDefaultPHIGENG;
   fPhiGammaWidth          = kDefaultDPHIG;
   fCosThetaPositronCenter = kDefaultCTHGENP;
   fCosThetaPositronWidth  = kDefaultDCOSTHP;
   fPhiPositronCenter      = kDefaultPHIGENP;
   fPhiPositronWidth       = kDefaultDPHIP;
   fXRangeMin              = kDefaultXRangeMin;
   fXRangeMax              = kDefaultXRangeMax;
   fYRangeMin              = kDefaultYRangeMin;
   fYRangeMax              = kDefaultYRangeMax;
   fZRangeMin              = kDefaultZRangeMin;
   fZRangeMax              = kDefaultZRangeMax;

   fMuonPolarization = kDefaultMuonPolarization;
   fRadiativeBR      = kDefaultMuonRadiativeBranchingRatio;

   //double param_vec[6];

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMStoppedMuonDecayGenerator::GeneratePrimaries(G4Event *anEvent, G4int decayMode,
                                                     G4bool positronOn, G4bool gammaOn)
{
   switch (decayMode) {
   case MUDECAY_SM:
      GenerateAnSMDecay(anEvent,
                        positronOn, gammaOn);
      break;
   case MUDECAY_SIGNAL_EGAMMA:
      GenerateASignal(anEvent,
                      positronOn, gammaOn);
      break;
   default:
      std::ostringstream o;
      o << "Unknown muon decay mode : " << decayMode;
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMStoppedMuonDecayGenerator::GenerateASignal(G4Event *anEvent,
                                                   G4bool positronOn, G4bool gammaOn)
{
   //-------- Make decay products -------- //

   G4VDecayChannel *signalChannel = fGeneratorAction->GetPhysicsList()->GetMuegammaChannel();

   G4DynamicParticle    *particle;
   G4DynamicParticle    *positron = 0;
   G4DynamicParticle    *gamma = 0;
   G4ParticleTable      *particleTable = G4ParticleTable::GetParticleTable();
   G4ParticleGun        *gun = fGeneratorAction->GetParticleGun();
   G4DecayProducts      *products = signalChannel->DecayIt();

   for (G4int iParticle = 0; iParticle < products->entries(); iParticle++) {
      particle = (*products)[iParticle];
      if (particle->GetParticleDefinition() == particleTable->FindParticle("e+")) {
         positron = particle;
      }
      if (particle->GetParticleDefinition() == particleTable->FindParticle("gamma")) {
         gamma = particle;
      }
   }

   //-------- DIRECTION -------- //
   G4ThreeVector gammadir;
   if (!gammaOn) {
      GeneratePositronDirection(gammadir);
      gammadir *= -1;
   } else {
      GenerateGammaDirection(gammadir);
   }

   //-------- POSITION -------- //
   G4ThreeVector pos = GeneratePositionOnTheTarget();

   //-------- Generation -------- //
   if (positron && positronOn) {
      gun->SetParticleDefinition(particleTable->FindParticle("e+"));
      gun->SetParticlePosition(pos);
      gun->SetParticleEnergy(positron->GetKineticEnergy());
      gun->SetParticleMomentumDirection(-gammadir);
      gun->GeneratePrimaryVertex(anEvent);

   }
   if (gamma && gammaOn) {
      gun->SetParticleDefinition(particleTable->FindParticle("gamma"));
      gun->SetParticlePosition(pos);
      gun->SetParticleEnergy(gamma->GetKineticEnergy());
      gun->SetParticleMomentumDirection(gammadir);
      gun->GeneratePrimaryVertex(anEvent);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMStoppedMuonDecayGenerator::GenerateAnSMDecay(G4Event *anEvent,
                                                     G4bool positronOn, G4bool gammaOn)
{
   // When 'positronOn' is true, no gamma is generated.
   // And energy or direction of gamma is not checked.
   // Same for 'gammaOn'.

   //-------- Make decay products -------- //
   GEMMuonDecayChannelWithSpin *muonChannelWithSpin =
      fGeneratorAction->GetPhysicsList()->GetMuonChannelWithSpin();
   GEMMuonRadiativeDecayChannelWithSpin *muonRadiativeChannelWithSpin =
      fGeneratorAction->GetPhysicsList()->GetMuonRadiativeChannelWithSpin();

   G4double muWithSpinBR  = (1 - fRadiativeBR);

   G4VDecayChannel   *channel;
   G4DecayProducts   *products = 0;
   G4DynamicParticle *particle;
   G4DynamicParticle *positron = 0;
   G4DynamicParticle *gamma = 0;
   G4ParticleTable   *particleTable = G4ParticleTable::GetParticleTable();
   G4ParticleGun     *gun = fGeneratorAction->GetParticleGun();

   while (1) {
      if ((gammaOn && GammaRequired()) || !positronOn) {
         // always generate gamma
         channel = muonRadiativeChannelWithSpin;
      } else if (!gammaOn) {
         // no gamma
         channel = muonChannelWithSpin;
      } else {
         G4double rn = G4UniformRand();
         if (rn < muWithSpinBR) {
            channel = muonChannelWithSpin;
         } else {
            channel = muonRadiativeChannelWithSpin;
         }
         CheckRangeCondition(positronOn, gammaOn);
      }

      G4ThreeVector polar(0, 0, -1);

      if (G4UniformRand() > fMuonPolarization) {
         // rotate 4pi
         G4double rphi   = twopi * G4UniformRand() * rad;
         G4double rtheta = std::acos(2  * G4UniformRand() - 1);
         G4double rpsi   = twopi * G4UniformRand() * rad;
         G4RotationMatrix rot;
         rot.set(rphi, rtheta, rpsi);
         polar *= rot;
      }

      if (channel == muonChannelWithSpin) {
         muonChannelWithSpin->SetPolarization(polar);
         muonChannelWithSpin->SetRange(fXRangeMin, fXRangeMax);
      }
      if (channel == muonRadiativeChannelWithSpin) {
         muonRadiativeChannelWithSpin->SetPolarization(polar);
         muonRadiativeChannelWithSpin->SetRange(fXRangeMin, fXRangeMax,
                                                fYRangeMin, fYRangeMax,
                                                fZRangeMin, fZRangeMax);
      }

      delete products;
      products = channel->DecayIt();
      positron = gamma = 0;
      for (G4int iParticle = 0; iParticle < products->entries(); iParticle++) {
         particle = (*products)[iParticle];
         if (particle->GetParticleDefinition() == particleTable->FindParticle("e+")) {
            positron = particle;
         }
         if (particle->GetParticleDefinition() == particleTable->FindParticle("gamma")) {
            gamma = particle;
         }
      }

      // rotate phi so that one particle is inside the acceptance
      if (positronOn && positron) {
         G4ThreeVector posdir, newdir;
         GeneratePositronDirection(posdir);
         newdir = positron->GetMomentumDirection();
         G4double rotphi = posdir.phi() - newdir.phi();
         newdir.rotateZ(rotphi);
         positron->SetMomentumDirection(newdir);
         if (gamma) {
            newdir = gamma->GetMomentumDirection();
            newdir.rotateZ(rotphi);
            gamma->SetMomentumDirection(newdir);
         }
      } else if (gamma) {
         G4ThreeVector gammadir, newdir;
         GenerateGammaDirection(gammadir);
         newdir = gamma->GetMomentumDirection();
         G4double rotphi = gammadir.phi() - newdir.phi();
         newdir.rotateZ(rotphi);
         gamma->SetMomentumDirection(newdir);
         if (positron) {
            newdir = positron->GetMomentumDirection();
            newdir.rotateZ(rotphi);
            positron->SetMomentumDirection(newdir);
         }
      }

      if (CheckKinematics(positron, gamma,
                          positronOn, gammaOn)) {
         break;
      }
   }


   //-------- POSITION -------- //
   G4ThreeVector pos = GeneratePositionOnTheTarget();

   //-------- Generation -------- //
   if (positron && positronOn) {
      gun->SetParticleDefinition(particleTable->FindParticle("e+"));
      gun->SetParticlePosition(pos);
      gun->SetParticleEnergy(positron->GetKineticEnergy());
      gun->SetParticleMomentumDirection(positron->GetMomentumDirection());
      gun->GeneratePrimaryVertex(anEvent);
   }
   if (gamma && gammaOn) {
      gun->SetParticleDefinition(particleTable->FindParticle("gamma"));
      gun->SetParticlePosition(pos);
      gun->SetParticleEnergy(gamma->GetKineticEnergy());
      gun->SetParticleMomentumDirection(gamma->GetMomentumDirection());
      gun->GeneratePrimaryVertex(anEvent);
   }

   delete products;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4ThreeVector GEMStoppedMuonDecayGenerator::MEG_to_TGT(G4ThreeVector pos, G4ThreeVector tgt_pos,
                                                       G4ThreeVector tgt_ang)
{
   //double *xT_a = new double[3];
   //xT_a[0] = xT(param_vec, x, y, z);
   //xT_a[1] = yT(param_vec, x, y, z);
   //xT_a[2] = zT(param_vec, x, y, z);

   //fTargetRotation = new G4RotationMatrix();
   G4ThreeVector zAxis(0, 0, 1);
   G4ThreeVector yAxis(0, 1, 0);
   G4ThreeVector t(pos.x() - tgt_pos.x(), pos.y() - tgt_pos.y(), pos.z() - tgt_pos.z());
   // Rotate Z-Y-Z (v[1]-v[0]-v[2]) - no 90 since starting at z-y plane not the x-y plane

   t.rotateZ(tgt_ang.y());
   zAxis.rotateZ(tgt_ang.y());
   yAxis.rotateZ(tgt_ang.y());

   t.rotate(tgt_ang.x(), yAxis);
   zAxis.rotate(tgt_ang.x(), yAxis);
   t.rotate(tgt_ang.z(), zAxis);

   //xT_a[0] = t.x();
   //xT_a[1] = t.y();
   //xT_a[2] = t.z();
   return t;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
std::vector<std::vector<std::string> > GEMStoppedMuonDecayGenerator::getData(std::string fileName)
{
   std::string delimeter = ",";
   std::ifstream file(fileName);
   std::vector<std::vector<std::string> > dataList;
   std::string line = "";
   // Iterate through each line and split the content using delimeter
   while (getline(file, line)) {
      std::vector<std::string> vec;
      boost::algorithm::split(vec, line, boost::is_any_of(delimeter));
      dataList.push_back(vec);
   }
   file.close();
   return dataList;
}



G4ThreeVector GEMStoppedMuonDecayGenerator::GeneratePositionOnTheTarget()
{
//----------------------------------------------------------------------------------
// Written by D.Palo
// Summary:
// 1. If a deformed file is specified, the csv file containing the information is read (read in TGT coordinates)
// 2. Vertex position is found on the flat target (Box Mueller)
// 3. If using flat target, return this vertex
//----------------------------------------------------------------------------------

//--------------- 1. ----------------------------------------------------------------
//row_count is initiallized to zero, called during first position generation
   if (row_count == 0) {
      tgt_angle = fGeneratorAction->GetTargetEulerAngles();
      in_target_flat_x.setX(0);
      in_target_flat_x.setY(0);
      in_target_flat_x.setZ(fTargetSize.x() / 2);
      in_target_flat_x = MEG_to_TGT(in_target_flat_x, fTargetPosition, tgt_angle); //getting target edge in xM

      in_target_flat_y.setX(0);
      in_target_flat_y.setY(fTargetSize.y() / 2);
      in_target_flat_y.setZ(0);
      in_target_flat_y = MEG_to_TGT(in_target_flat_y, fTargetPosition, tgt_angle); //getting target edge in yM
   }
//-----------------------------------------------------------------------------------
//--------------- 2. ----------------------------------------------------------------

   G4ThreeVector pos = fTargetPosition;
   // Norm vector
   G4ThreeVector normVec(0, 0, 1);
   normVec *= fTargetRotation;
   if (normVec.z() == 0) {
      // The target is parallel to the beam axis, not allowed
      G4cerr << __FILE__ << " " << __func__ << " " << __LINE__ << " " <<
             "Bad target angle." << G4endl;
      exit(-1);
   }
   // Center coordinate of the surface
   G4ThreeVector surfaceCenter = fTargetPosition;
   surfaceCenter -= fTargetSize.z() / 2 * normVec;
   // The target surface plane (a*x + b*y + c*z + d = 0)
   // is the plane passing surfaceCenter point with the normal vector being normVec.
   //  a = normVec.x(), b = normVec.y(), c = normVec.z()
   G4double d = -(normVec.dot(surfaceCenter));
   while (1) {
      pos.setX(G4RandGauss::shoot(fMuonCenterX, fMuonSigmaX));
      pos.setY(G4RandGauss::shoot(fMuonCenterY, fMuonSigmaY));

      G4double rn[2] = {G4UniformRand(),
                        G4UniformRand()
                       };
      // Calculate z position on the target surface plane given the (x,y) position
      G4double z1 = -1. / normVec.z() * (
                       normVec.x() * pos.x() + normVec.y() * pos.y() + d);
      // Gauss distribution by Box-Muller's method
      G4double depth = fMuonDepth +
                       sqrt(-2 * log(rn[0])) *
                       cos(twopi * rn[1]) * fMuonDepthSigma;
      pos.setZ(z1 + depth);

//--------------- 3. ----------------------------------------------------------------
      //std::cout << "size of x target: " << in_target_flat_x.x() << std::endl;
      //std::cout << "size of y target: " << in_target_flat_y.y() <<  std::endl;
      
      if ((pos.x()*pos.x()) / (in_target_flat_x.x()*in_target_flat_x.x()) + (pos.y()*pos.y()) /
          (in_target_flat_y.y()*in_target_flat_y.y()) <
          1) { //assume the angles are done properly, if inside the limits of the target, return
         if (fTargetVol) {
            // check if the position is in the target
            G4VPhysicalVolume *physVol = G4TransportationManager::GetTransportationManager()->
                                         GetNavigatorForTracking()->
                                         LocateGlobalPointAndSetup(pos, 0, false);
            if (!physVol) {
               G4cerr << __FILE__ << " " << __func__ << " " << __LINE__ << " " <<
                      "Muon position outside physical volume." << G4endl;
               exit(-1);
            }
            if (physVol->GetLogicalVolume() == fTargetVol->GetLogicalVolume()) { //check to verify vertex is in target
               return pos;
            }
         }
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool GEMStoppedMuonDecayGenerator::CheckKinematics(G4DynamicParticle *positron,
                                                     G4DynamicParticle *gamma,
                                                     G4bool positronOn, G4bool gammaOn)
{
   G4double energy;
   G4double z;
   G4double phidiff;

   if (positronOn) {
      G4ThreeVector posdir = positron->GetMomentumDirection();
      if (PositronRequired() && !positron) {
         return false;
      }

      if (positron) {
         if (fabs(posdir.cosTheta() - fCosThetaPositronCenter) > fCosThetaPositronWidth) {
            return false;
         }

         phidiff = posdir.phi() - fPhiPositronCenter;
         while (phidiff > +pi) {
            phidiff -= twopi;
         }
         while (phidiff < -pi) {
            phidiff += twopi;
         }
         if (fabs(phidiff) > fPhiPositronWidth) {
            return false;
         }

         energy = positron->GetTotalEnergy();
         if (energy / (muon_mass_c2 / 2) < fXRangeMin || energy / (muon_mass_c2 / 2) > fXRangeMax) {
            return false;
         }
      }
   }

   if (gammaOn) {
      if (GammaRequired() && !gamma) {
         return false;
      }

      if (gamma) {
         G4ThreeVector gammadir = gamma->GetMomentumDirection();
         if (fabs(gammadir.cosTheta() - fCosThetaGammaCenter) > fCosThetaGammaWidth) {
            return false;
         }

         phidiff = gammadir.phi() - fPhiGammaCenter;
         while (phidiff > +pi) {
            phidiff -= twopi;
         }
         while (phidiff < -pi) {
            phidiff += twopi;
         }
         if (fabs(phidiff) > fPhiGammaWidth) {
            return false;
         }

         energy = gamma->GetTotalEnergy();
         if (energy / (muon_mass_c2 / 2) < fYRangeMin || energy / (muon_mass_c2 / 2) > fYRangeMax) {
            return false;
         }

         if (positronOn && positron) {
            G4ThreeVector posdir =  positron->GetMomentumDirection();
            z = cos(gammadir.angle(posdir));
            if (z < fZRangeMin || z > fZRangeMax) {
               return false;
            }
         }
      }
   }

   return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool GEMStoppedMuonDecayGenerator::GammaRequired()
{
   return
      fCosThetaGammaWidth < 1    - 1e-10 ||
      fPhiGammaWidth      < 3.14 - 1e-10 ||
      fYRangeMax < kDefaultYRangeMax - 100 * keV / (muon_mass_c2 / 2) ||
      fYRangeMin > kDefaultYRangeMin + 100 * keV / (muon_mass_c2 / 2) ||
      fZRangeMin > -1 + 1e-10 ||
      fZRangeMax < +1 - 1e-10 ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool GEMStoppedMuonDecayGenerator::PositronRequired()
{
   return
      fCosThetaPositronWidth < 1    - 1e-10 ||
      fPhiPositronWidth      < 3.14 - 1e-10 ||
      fXRangeMax < kDefaultXRangeMax - 100 * keV / (muon_mass_c2 / 2) ||
      fXRangeMin > kDefaultXRangeMin + 100 * keV / (muon_mass_c2 / 2);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void GEMStoppedMuonDecayGenerator::GenerateGammaDirection(G4ThreeVector &dir)
{
   G4double cthpho, sthpho, phipho;

   //-- determine cos(theta).
   cthpho = (fCosThetaGammaCenter +
             (2 * G4UniformRand() - 1) * fCosThetaGammaWidth);
   if (fabs(cthpho) >= 1) {
      cthpho = cthpho > 0 ? 1 : -1;
      sthpho = 0;
   } else {
      sthpho = sqrt((1 - cthpho) * (1 + cthpho));
   }

   //-- determine phi.
   phipho = fPhiGammaCenter + (2 * G4UniformRand() - 1) * fPhiGammaWidth;
   if (phipho <= 0) {
      phipho = phipho + twopi;
   } else if (phipho >= twopi) {
      phipho = phipho - twopi;
   }

   //-- photon direction.
   dir.setX(sthpho * cos(phipho));
   dir.setY(sthpho * sin(phipho));
   dir.setZ(cthpho);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void GEMStoppedMuonDecayGenerator::GeneratePositronDirection(G4ThreeVector &dir)
{
   G4double cthpos, sthpos, phipos;

   //-- determine cos(theta).
   cthpos = (fCosThetaPositronCenter +
             (2 * G4UniformRand() - 1) * fCosThetaPositronWidth);
   if (fabs(cthpos) >= 1) {
      cthpos = cthpos > 0 ? 1 : -1;
      sthpos = 0;
   } else {
      sthpos = sqrt((1 - cthpos) * (1 + cthpos));
   }

   //-- determine phi.
   phipos = fPhiPositronCenter + (2 * G4UniformRand() - 1) * fPhiPositronWidth;
   if (phipos <= 0) {
      phipos = phipos + twopi;
   } else if (phipos >= twopi) {
      phipos = phipos - twopi;
   }

   //-- Positron direction.
   dir.setX(sthpos * cos(phipos));
   dir.setY(sthpos * sin(phipos));
   dir.setZ(cthpos);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void GEMStoppedMuonDecayGenerator::CheckRangeCondition(G4bool positronOn,
                                                       G4bool gammaOn)
{
   // assuming ranges are not changed in a process
   static G4bool checked = false;
   if (checked) {
      return;
   }
   checked = true;

   if ((gammaOn && GammaRequired()) || !positronOn || fRadiativeBR > 1 - 1e-10) {
      // no problem, always radiative decay is used.
      return;
   }
   if (!gammaOn || fRadiativeBR < 1e-10) {
      // no problem, always Michel decay is used.
      return;
   }

   G4bool rangeLimited =
      //     fCosThetaGammaWidth    < 1    - 1e-10 ||
      //     fPhiGammaWidth         < 3.14 - 1e-10 ||
      //     fCosThetaPositronWidth < 1    - 1e-10 ||
      //     fPhiPositronWidth      < 3.14 - 1e-10 ||
      fXRangeMin > kDefaultXRangeMax - 100 * keV / (muon_mass_c2 / 2) ||
      fXRangeMax < kDefaultXRangeMin + 100 * keV / (muon_mass_c2 / 2) ||
      //     fYRangeMax < kDefaultYRangeMax - 1e-10 ||
      //     fYRangeMin > kDefaultYRangeMin + 1e-10 ||
      //     fZRangeMax < kDefaultZRangeMax - 1e-10 ||
      //     fZRangeMin > kDefaultZRangeMin + 1e-10 ||
      0;

   if (rangeLimited && fRadiativeBR == kDefaultMuonRadiativeBranchingRatio) {
      std::ostringstream o;
      o << "It seems the default radiative BR is used while positron energy or angle is limited." << G4endl;
      o << "RMD BR may not be correct." << G4endl;
      o << "Solutions are one of followings," << G4endl;
      o << "  - adjust radiative BR for your range cuts" << G4endl;
      o << "  - disable gamma generation. (e.g. evtype=12)" << G4endl;
      o << "  - use full range of positron energy." << G4endl;
      o << "  - set radiative BR to be 0 or 1." << G4endl;
      G4Exception(__func__, "", JustWarning, o.str().c_str());
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
