//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <cassert>

#include "GEMTrackingAction.hh"
#include "GEMTrajectory.hh"

#include "G4EventManager.hh"
#include "G4TrackingManager.hh"
#include "G4Track.hh"
#include "G4Event.hh"
#include "G4ParticleTypes.hh"
#include "G4ProcessType.hh"
#include "G4EmProcessSubType.hh"
#include "GEMAbsUserTrackInformation.hh"
#include "GEMUserEventInformation.hh"
#include "GEMUserTrackInformation.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMTrackingAction::GEMTrackingAction()
   : modules()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
   //Let this be up to the user via vis.mac
   //  fpTrackingManager->SetStoreTrajectory(true);

   fpTrackingManager->SetTrajectory(new GEMTrajectory(aTrack));

   std::vector<GEMAbsModule*>::iterator mod;

   GEMUserTrackInformation *ui;
   GEMUserEventInformation* eveInfo =
      dynamic_cast<GEMUserEventInformation*>(
         G4EventManager::GetEventManager()->GetConstCurrentEvent()->
         GetUserInformation());

   if (aTrack->GetParentID() == 0 && aTrack->GetUserInformation() == 0) {
      // This is a primary track
      ui = new GEMUserTrackInformation();

      for (mod = modules.begin(); mod != modules.end(); ++mod) {
         if ((*mod)) {
            ui->AddUserTrackInformation((*mod)->MakeTrackInformation(aTrack));
         }
      }

      eveInfo->AddPrimaryInfo(aTrack);
      // assuming a primary particle is tracked just after the generation.
      ui->trackInfo.vertexid             = eveInfo->GetPrimaryInfoSize() - 1;
      ui->trackInfo.PrimaryParticleIndex = eveInfo->GetPrimaryInfoSize() - 1;
      ui->trackInfo.ParentIndex          = -1;
      ui->trackInfo.ParentID             = -1;
      ui->trackInfo.particledef          = aTrack->GetDefinition();
      ui->trackInfo.trackid              = aTrack->GetTrackID();
      ui->trackInfo.tekine               = aTrack->GetKineticEnergy();
      ui->trackInfo.momx                 = aTrack->GetMomentum().x();
      ui->trackInfo.momy                 = aTrack->GetMomentum().y();
      ui->trackInfo.momz                 = aTrack->GetMomentum().z();

      fpTrackingManager->SetUserTrackInformation(ui);
   } else {
      ui = dynamic_cast<GEMUserTrackInformation*>(aTrack->GetUserInformation());
      if (ui->trackInfo.trackid == -1) {
         // this is the first time for this track.
         ui->trackInfo.particledef = aTrack->GetDefinition();
         ui->trackInfo.trackid     = aTrack->GetTrackID();
         ui->trackInfo.tekine      = aTrack->GetKineticEnergy();
         ui->trackInfo.ParentIndex = aTrack->GetParentID();
         ui->trackInfo.ParentID    = aTrack->GetParentID();
         ui->trackInfo.momx        = aTrack->GetMomentum().x();
         ui->trackInfo.momy        = aTrack->GetMomentum().y();
         ui->trackInfo.momz        = aTrack->GetMomentum().z();
         if (IsDecayProcess(aTrack->GetCreatorProcess())){
            if (aTrack->GetParentID() == 1) {
               if (aTrack->GetDefinition() == G4Gamma::GammaDefinition() ||
                   aTrack->GetDefinition() == G4Positron::PositronDefinition() ||
                   aTrack->GetDefinition() == G4Electron::ElectronDefinition()) {
                  // so far, only gamma and positron are intersting, added electrons too (GC 11 May 13)
                  eveInfo->AddSecondaryInfo(aTrack);
               }
            }else if (IsPionZeroDecayProcess(eveInfo, aTrack)) {
               // added pi0 decay (Nov. 27)
               eveInfo->AddSecondaryInfo(aTrack);
            }
            eveInfo->timeOfInterest = aTrack->GetGlobalTime();
         }
      }
   }

   G4UserTrackingAction *act;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetTrackingAction())) {
         act->PreUserTrackingAction(aTrack);
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMTrackingAction::PostUserTrackingAction(const G4Track* aTrack)
{
   GEMUserTrackInformation *ui =
      dynamic_cast<GEMUserTrackInformation*>(
         aTrack->GetUserInformation());

   GEMUserEventInformation* eveInfo =
      dynamic_cast<GEMUserEventInformation*>(
         G4EventManager::GetEventManager()->GetConstCurrentEvent()->
         GetUserInformation());

   ui->trackInfo.tracklength = aTrack->GetTrackLength();
   G4int parentIndex = ui->trackInfo.TrackIndex;

   if (ui->trackInfo.TrackIndex == -1) {
      // this is the first time for this track.
      if (aTrack->GetCreatorProcess()) {
         G4String cname = aTrack->GetCreatorProcess()->GetProcessName();
         if (cname == "Decay") {
            ui->trackInfo.trackendmec = 0;
         } else if (cname == "muIoni") {
            ui->trackInfo.trackendmec = 1;
         } else if (cname == "eIoni") {
            ui->trackInfo.trackendmec = 2;
         } else if (cname == "annihil") {
            ui->trackInfo.trackendmec = 3;
         } else if (cname == "phot") {
            ui->trackInfo.trackendmec = 4;
         } else if (cname == "compt") {
            ui->trackInfo.trackendmec = 5;
         } else if (cname == "eBrem") {
            ui->trackInfo.trackendmec = 6;
         } else if (cname == "conv") {
            ui->trackInfo.trackendmec = 7;
         } else {
            ui->trackInfo.trackendmec = -1;
            G4cout << " trackid " << aTrack->GetTrackID() << 
               " CreatorProcess " << cname <<
               G4endl;
         }
      }
      if (aTrack->GetDefinition() !=
          G4OpticalPhoton::OpticalPhotonDefinition()) {
         ui->trackInfo.TrackIndex = eveInfo->GetTrackInfoSize();
         eveInfo->AddTrackInfo(*ui);
      } else {
         ui->trackInfo.TrackIndex = -1;
      }
   }

   // make user info for secondaries
   G4TrackVector* secondaries = fpTrackingManager->GimmeSecondaries();
   if (secondaries) {
      size_t nSeco = secondaries->size();
      for (size_t iSeco = 0; iSeco < nSeco; iSeco++) {
         GEMUserTrackInformation *uiNew = new GEMUserTrackInformation();
         std::vector<GEMAbsModule*>::iterator mod;
         for (mod = modules.begin(); mod != modules.end(); ++mod) {
            if ((*mod)) {
               uiNew->AddUserTrackInformation((*mod)->MakeTrackInformation(aTrack));
            }
         }
         uiNew->trackInfo.vertexid             = ui->trackInfo.vertexid;
         uiNew->trackInfo.PrimaryParticleIndex = ui->trackInfo.PrimaryParticleIndex;
         uiNew->trackInfo.ParentIndex          = parentIndex;
         uiNew->trackInfo.ParentID             = aTrack->GetTrackID();
         uiNew->trackInfo.annihiltrackid       = ui->trackInfo.annihiltrackid;
         uiNew->trackInfo.bremstrackid         = ui->trackInfo.bremstrackid;

         // Record annihilation and bremsstrahlung from the primary positron
         if ((aTrack->GetParentID() == 0 || // primary
              ((aTrack->GetCreatorProcess() && (aTrack->GetCreatorProcess()->GetProcessType() == fDecay)) &&
               aTrack->GetParentID() == 1)) && // from muon decay
             aTrack->GetDefinition() == G4Positron::PositronDefinition()) { // positron
            G4int type    = (*secondaries)[iSeco]->GetCreatorProcess()->GetProcessType();
            G4int subType = (*secondaries)[iSeco]->GetCreatorProcess()->GetProcessSubType();
            if (type == fElectromagnetic && (subType == fAnnihilation || subType == fBremsstrahlung)) {
               GEMUserPrimaryVertexInformation *rad = new GEMUserPrimaryVertexInformation();
               rad->vtxid = eveInfo->radiationInfos.size(); // radiation index
               rad->vtype = subType;
               rad->gcode = -1;
               rad->xvtx = (*secondaries)[iSeco]->GetPosition().x();
               rad->yvtx = (*secondaries)[iSeco]->GetPosition().y();
               rad->zvtx = (*secondaries)[iSeco]->GetPosition().z();
               rad->tvtx = (*secondaries)[iSeco]->GetGlobalTime();
               rad->xmom = (*secondaries)[iSeco]->GetMomentum().x();
               rad->ymom = (*secondaries)[iSeco]->GetMomentum().y();
               rad->zmom = (*secondaries)[iSeco]->GetMomentum().z();
               eveInfo->radiationInfos.push_back(rad);
               if (subType == fAnnihilation) {
                  uiNew->trackInfo.annihiltrackid = rad->vtxid;
                  assert(uiNew->trackInfo.bremstrackid == -1);
               } else if (subType == fBremsstrahlung) {
                  uiNew->trackInfo.bremstrackid = rad->vtxid;
                  assert(uiNew->trackInfo.annihiltrackid == -1);
               }
               uiNew->trackInfo.radx = rad->xvtx;
               uiNew->trackInfo.rady = rad->yvtx;
               uiNew->trackInfo.radz = rad->zvtx;
            }
         }
         (*secondaries)[iSeco]->SetUserInformation(uiNew);
      }
   }

   G4UserTrackingAction *act;
   std::vector<GEMAbsModule*>::iterator mod;
   for (mod = modules.begin(); mod != modules.end(); ++mod) {
      if ((*mod) && (act = (*mod)->GetTrackingAction())) {
         act->PostUserTrackingAction(aTrack);
      }
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMTrackingAction::IsDecayProcess(const G4VProcess* process)
{
   if (!process) {
      return false;
   }
   return (process->GetProcessType() == fDecay); // so far in gem4, only muon can decay.
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool GEMTrackingAction::IsPionZeroDecayProcess(GEMUserEventInformation* eveInfo, const G4Track* aTrack)
{

   bool parentIsPionZero = false;
   std::vector<GEMUserTrackInformation*>::iterator track;
   for (track = eveInfo->trackInfos.begin(); track != eveInfo->trackInfos.end(); ++track) {
      if((*track)->trackInfo.trackid == aTrack->GetParentID()){// is parent
         parentIsPionZero = ((*track)->trackInfo.particledef == G4PionZero::PionZeroDefinition());
      }
   }

   bool isInterestingParticle = (aTrack->GetDefinition() == G4Gamma::GammaDefinition() || 
                                 aTrack->GetDefinition() == G4Positron::PositronDefinition() ||
                                 aTrack->GetDefinition() == G4Electron::ElectronDefinition());

   const G4VProcess* process = aTrack->GetCreatorProcess();
   if (!process) {
      return false;
   }
   return (parentIsPionZero && isInterestingParticle && (process->GetProcessType() == fDecay));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
