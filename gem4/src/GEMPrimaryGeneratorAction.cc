//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "GEMPrimaryGeneratorAction.hh"
#include "GEMPrimaryGeneratorMessenger.hh"
#include "GEMStoppedMuonDecayGenerator.hh"
#include "GEMMuonRadiativeDecayChannelWithSpin.hh"

#include "Randomize.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4GeneralParticleSource.hh"

#include "GEMPhysicsList.hh"
#include "GEMAbsTargetModule.hh"
#include "GEMConstants.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPrimaryGeneratorAction::GEMPrimaryGeneratorAction()
   : G4VUserPrimaryGeneratorAction()
   , fParticleGun(nullptr)
   , fParticleSource(nullptr)
   , fGunMessenger(nullptr)
   , fEventTypeList(0)
   , fEventType(kDefaultEventType)
   , fModules()
   , fPhysics(nullptr)
   , fMuonDecayGenerator(nullptr)
   , fMuonBeamGenerator(nullptr)
   , fTargetModule(nullptr)
   , fTGTGeometryId(-1)
   , fTargetRotation(nullptr)
   , fTargetEulerAngles()
{
   Init();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPrimaryGeneratorAction::GEMPrimaryGeneratorAction(std::vector<GEMAbsModule*> &mod,
                                                     GEMPhysicsList *p)
   : G4VUserPrimaryGeneratorAction()
   , fParticleGun(nullptr)
   , fParticleSource(nullptr)
   , fGunMessenger(nullptr)
   , fEventTypeList(0)
   , fEventType(kDefaultEventType)
   , fModules()
   , fPhysics(p)
   , fMuonDecayGenerator(nullptr)
   , fMuonBeamGenerator(nullptr)
   , fTargetModule(nullptr)
   , fTGTGeometryId(-1)
   , fTargetRotation(nullptr)
   , fTargetEulerAngles()
{
   AddModules(mod);
   Init();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
void GEMPrimaryGeneratorAction::Init()
{
   delete fMuonDecayGenerator;
   delete fMuonBeamGenerator;

   fMuonDecayGenerator    = new GEMStoppedMuonDecayGenerator(this);
   fMuonBeamGenerator     = new GEMMuonBeamGenerator(this);

   G4int n_particle = 1;
   fParticleGun = new G4ParticleGun(n_particle);

   fParticleSource = new G4GeneralParticleSource();

   //create a messenger for this class
   fGunMessenger = new GEMPrimaryGeneratorMessenger(this);

   //default kinematic
   G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
   G4ParticleDefinition* particle = particleTable->FindParticle("geantino");
   fParticleGun->SetParticleDefinition(particle);
   fParticleGun->SetParticleTime(0 * ns);
   fParticleGun->SetParticlePosition(G4ThreeVector(0 * cm, 0 * cm, 0 * cm));
   fParticleGun->SetParticleEnergy(0 * MeV);
   fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0, 0, 1));

   // Register event types
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::ISIGNAL, "e+ gamma SIGNAL event"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IRADIATIVE, "e+ gamma from SM Decay"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IAIFAIF         , "two photons AIF and AIF"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPOSITRON2GAMMA , "mu to e+ 2gamma"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::ISIGNALPOSITRON, "e+ from SIGNAL"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IMICHEL, "e+ from MICHEL"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::INOISEE         , "e+ flat noise on spectrometer"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::ISIGNALPHOTON, "gamma from SIGNAL"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IRADIATIVEPHOTON, "gamma from RD"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IAFGAMA         , "gamma from AIF (DEPRECATED); no MAINTAINED)");
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IFLGAMA         , "gamma flat"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IAFGAMATAB      , "gamma from AIF by using pre-computed table"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IMUBEAM, "muon beam"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPOSBEAM, "positron beam"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::ICR1DIR         , "cosmic ray muon in a fixed direction"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::ICRZANG         , "cosmic muon with zenith angle distribution"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IEVELED         , "LED pulse simulation"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::INICKGA         , "gamma from Ni plates (calibration)"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IALPHSW, "alpha on wires."));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPROTCA,
   //                                           "LXe calibration events from CW accelerator + LiF"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::ICOSMIC, "CYLDCH cosmic rays"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPIZERODALITZ, "pi0->gamma+e+e-"));
//   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IEPEMGA         , "e+/e- pair from 129 MeV gamma"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPIZERO2GAMMA, "pi0->gamma+gamma"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPROCON         , "proton from CW accelerator + LiF + converter"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPROTBO, "proton from CW accelerator + Boron"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IEVMOTT, "Mott scattering for positrons"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::INEUCA          , "neutron generator calibration event"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IXRAY, "120keV Xray"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IPIBEAM, "Pion Beam"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IBMFILE         , "beam with kinematics from a file"));
   // fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IBMCARD         , "fixed kinematics from cards"));
   fEventTypeList.push_back(new GEMEventType(EGEMEventTypeID::IGSP, "GeneralParticleSource defined in macro"));

   // Init target
   G4ThreeVector v;
   v.setX(kDefaultTargetSize[0]);
   v.setY(kDefaultTargetSize[1]);
   v.setZ(kDefaultTargetSize[2]);      
   SetTargetSize(v);
   v.setX(kDefaultTargetAngle[0]);
   v.setY(kDefaultTargetAngle[1]);
   v.setZ(kDefaultTargetAngle[2]);
   SetTargetEulerAngles(v);
   SetTargetAngle(v);
   v.setX(kDefaultTargetPosition[0]);
   v.setX(kDefaultTargetPosition[1]);
   v.setX(kDefaultTargetPosition[2]);      
   SetTargetPosition(v);
   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMPrimaryGeneratorAction::~GEMPrimaryGeneratorAction()
{
   delete fParticleGun;
   delete fParticleSource;
   delete fGunMessenger;
   for (auto &&eventType : fEventTypeList) {
      delete eventType;
   }
   delete fMuonDecayGenerator;
   delete fMuonBeamGenerator;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::DumpEventTypes()
{
   std::vector<GEMEventType*>::iterator fEventTypeListIter;
   G4cout << G4endl << "____________________" << G4endl;
   for (fEventTypeListIter = fEventTypeList.begin();
        fEventTypeListIter != fEventTypeList.end();
        ++fEventTypeListIter) {
      if ((*fEventTypeListIter)->GetId() == fEventType) {
         G4cout << "* ";
      } else {
         G4cout << "  ";
      }
      G4cout << std::setw(2) << (*fEventTypeListIter)->GetId() << "   " <<
             (*fEventTypeListIter)->GetDescription() << G4endl;
   }
   G4cout << "____________________" << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent)
{
   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   G4VPhysicalVolume *targetVol = 0;
   if (tarModule) {
      targetVol = tarModule->GetTargetVolume();
      fMuonDecayGenerator->SetTargetVolume(targetVol);
   }

   switch (fEventType) {
   case EGEMEventTypeID::ISIGNAL:
      fParticleGun->SetParticleTime(0 * ns);
      fMuonDecayGenerator->GeneratePrimaries(anEvent,
                                             GEMStoppedMuonDecayGenerator::MUDECAY_SIGNAL_EGAMMA,
                                             true, true);
      break;
   case EGEMEventTypeID::IRADIATIVE:
      fParticleGun->SetParticleTime(0 * ns);
      fMuonDecayGenerator->GeneratePrimaries(anEvent,
                                             GEMStoppedMuonDecayGenerator::MUDECAY_SM,
                                             true, true);
      break;
   case EGEMEventTypeID::ISIGNALPOSITRON:
      fParticleGun->SetParticleTime(0 * ns);
      fMuonDecayGenerator->GeneratePrimaries(anEvent,
                                             GEMStoppedMuonDecayGenerator::MUDECAY_SIGNAL_EGAMMA,
                                             true, false);
      break;
   case EGEMEventTypeID::IMICHEL:
      fParticleGun->SetParticleTime(0 * ns);
      fMuonDecayGenerator->GeneratePrimaries(anEvent,
                                             GEMStoppedMuonDecayGenerator::MUDECAY_SM,
                                             true, false);
      break;
   case EGEMEventTypeID::ISIGNALPHOTON:
      fMuonDecayGenerator->GeneratePrimaries(anEvent,
                                             GEMStoppedMuonDecayGenerator::MUDECAY_SIGNAL_EGAMMA,
                                             false, true);
      break;
   case EGEMEventTypeID::IRADIATIVEPHOTON:
      fParticleGun->SetParticleTime(0 * ns);
      fMuonDecayGenerator->GeneratePrimaries(anEvent,
                                             GEMStoppedMuonDecayGenerator::MUDECAY_SM,
                                             false, true);
      break;
   case EGEMEventTypeID::IMUBEAM:
      fParticleGun->SetParticleTime(0 * ns);
      fMuonBeamGenerator->GeneratePrimaries(anEvent);
      break;
   case EGEMEventTypeID::IGSP:
      // event generation with GeneralParticeSource
      fParticleSource->SetParticleTime(0 * ns);
      fParticleSource->GeneratePrimaryVertex(anEvent);
      break;
   default:
      std::ostringstream o;
      o << "Unknown event type : " << fEventType;
      G4Exception(__func__, "", FatalException, o.str().c_str());
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GEMAbsModule *GEMPrimaryGeneratorAction::GetModule(const char* name)
{
   std::vector<GEMAbsModule*>::iterator iter;
   G4String str = name;
   for (auto&& module : fModules) {
      if (module && name == module->GetName()) {
         return module;
      }
   }
   return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetTGTGeometryId(G4int id)
{
   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   if (tarModule) {
      tarModule->SetTGTGeometryId(id);
   }
   fTGTGeometryId = id;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetTargetSize(G4ThreeVector v)
{
   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   if (tarModule) {
      tarModule->SetTargetSize(v);
   }
   if (fMuonDecayGenerator) {
      fMuonDecayGenerator->SetTargetSize(v);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetTargetAngle(G4ThreeVector v)
{
   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   CalculateTargetRotationMatrix(v);
   if (tarModule) {
      tarModule->SetTargetRotation(fTargetRotation);
   }
   if (fMuonDecayGenerator) {
      fMuonDecayGenerator->SetTargetRotation(fTargetRotation);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetTargetEulerAngles(G4ThreeVector v)
{
   //setting Target Euler Angles for GEMStoppedMuonDecayGenerator
   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   if (tarModule) {
      tarModule->SetTargetEulerAngles(v);
      fTargetEulerAngles = v;
   }
}

G4ThreeVector GEMPrimaryGeneratorAction::GetTargetEulerAngles()
{
   //setting Target Euler Angles for GEMStoppedMuonDecayGenerator
   return fTargetEulerAngles;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::CalculateTargetRotationMatrix(const G4ThreeVector v)
{
   delete fTargetRotation;
   fTargetRotation = new G4RotationMatrix();
   G4ThreeVector zAxis(0, 0, 1);
   G4ThreeVector yAxis(0, 1, 0);

   // Rotate Z-Y-Z (v[1]-(90-v[0]-v[2])

   fTargetRotation->rotateZ(v[1]);
   zAxis.rotateZ(v[1]);
   yAxis.rotateZ(v[1]);

   fTargetRotation->rotate(90 * deg - v[0], yAxis);
   zAxis.rotate(90 * deg - v[0], yAxis);
   fTargetRotation->rotate(v[2], zAxis);
   return;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetTargetPosition(G4ThreeVector v)
{

   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   if (tarModule) {
      tarModule->SetTargetPosition(v);
   }
   if (fMuonDecayGenerator) {
      fMuonDecayGenerator->SetTargetPosition(v);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetTargetMaterial(G4int i)
{

   GEMAbsTargetModule* tarModule = dynamic_cast<GEMAbsTargetModule*>(fTargetModule);
   if (tarModule) {
      tarModule->SetTargetMaterial(i);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetMuonRadiativeMaxFuncValue(G4double v)
{
   if (fPhysics) {
      fPhysics->GetMuonRadiativeChannelWithSpin()->SetMaxSom0(v);
      fPhysics->GetMuonRadiativeChannelWithSpin()->SetMaxSom0ByHand(true);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetMuonRadiativeScanNBin(G4int n)
{
   if (fPhysics) {
      fPhysics->GetMuonRadiativeChannelWithSpin()->SetScanNBin(n);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetMuonRadiativeGammaSpectrumLowerEdge(G4double th)
{
   if (fPhysics) {
      fPhysics->GetMuonRadiativeChannelWithSpin()->SetGammaSpectrumLowerEdge(th);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GEMPrimaryGeneratorAction::SetMuonRadiativeSafetyFactor(G4double v)
{
   if (fPhysics) {
      fPhysics->GetMuonRadiativeChannelWithSpin()->SetSafetyFactor(v);
   }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
