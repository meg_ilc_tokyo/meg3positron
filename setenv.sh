# setup ROOT, GenFit and Geant4
#source /meg/home/meg/pro/cern/setenv.sh


# setup user environment

# MEG3
#export MEG3SYS=/meg/home/uchiyama/MegAnalysis/meg3positron
export MEG3SYS=`pwd`
# gem4
export LD_LIBRARY_PATH=${MEG3SYS}/gem4/folders/obj:${LD_LIBRARY_PATH}

export MEGOPT='-O2'
export MEG3ANAOPTIONS=""
#export MEG3BAROPTIONS="-v -qm"


